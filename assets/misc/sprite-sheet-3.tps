<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>6</int>
        <key>texturePackerVersion</key>
        <string>7.1.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename>../images/sprite-sheets/sprite-sheet-{n}.webp</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>etc1QualityLevel</key>
        <uint>40</uint>
        <key>etc2QualityLevel</key>
        <uint>40</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>4</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>90</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">webp</enum>
        <key>borderPadding</key>
        <uint>4</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../images/sprite-sheets/sprite-sheet-{n}.json</filename>
            </struct>
        </map>
        <key>multiPackMode</key>
        <enum type="SettingsBase::MultiPackMode">MultiPackManual</enum>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../images/source/cards/astral-beings/spirit-card.png</key>
            <key type="filename">../images/source/cards/astral-beings/tulpa-card.png</key>
            <key type="filename">../images/source/cards/dwarves/commander-the-magnate.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-elite-axeman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-elite-crossbowman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-elite-maceman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-elite-spearman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-elite-swordsman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-regular-axeman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-regular-crossbowman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-regular-maceman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-regular-spearman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-regular-swordsman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-rookie-axeman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-rookie-crossbowman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-rookie-maceman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-rookie-spearman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-rookie-swordsman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-veteran-axeman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-veteran-crossbowman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-veteran-maceman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-veteran-spearman.png</key>
            <key type="filename">../images/source/cards/dwarves/dwarf-veteran-swordsman.png</key>
            <key type="filename">../images/source/cards/elves/elf-elite-slingman.png</key>
            <key type="filename">../images/source/cards/elves/elf-elite-spearman.png</key>
            <key type="filename">../images/source/cards/elves/elf-elite-swordsman.png</key>
            <key type="filename">../images/source/cards/elves/elf-regular-slingman.png</key>
            <key type="filename">../images/source/cards/elves/elf-regular-spearman.png</key>
            <key type="filename">../images/source/cards/elves/elf-regular-swordsman.png</key>
            <key type="filename">../images/source/cards/elves/elf-rookie-slingman.png</key>
            <key type="filename">../images/source/cards/elves/elf-rookie-spearman.png</key>
            <key type="filename">../images/source/cards/elves/elf-rookie-swordsman.png</key>
            <key type="filename">../images/source/cards/elves/elf-veteran-slingman.png</key>
            <key type="filename">../images/source/cards/elves/elf-veteran-spearman.png</key>
            <key type="filename">../images/source/cards/elves/elf-veteran-swordsman.png</key>
            <key type="filename">../images/source/cards/goblins/commander-wimis.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-adept-sorcerer.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-disciple-sorcerer.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-elite-axeman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-elite-crossbowman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-elite-maceman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-master-sorcerer.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-novice-sorcerer.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-regular-axeman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-regular-crossbowman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-regular-maceman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-rookie-axeman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-rookie-crossbowman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-rookie-maceman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-veteran-axeman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-veteran-crossbowman.png</key>
            <key type="filename">../images/source/cards/goblins/goblin-veteran-maceman.png</key>
            <key type="filename">../images/source/cards/humans/human-adept-sorcerer.png</key>
            <key type="filename">../images/source/cards/humans/human-adept-theurge.png</key>
            <key type="filename">../images/source/cards/humans/human-adept-warlock.png</key>
            <key type="filename">../images/source/cards/humans/human-adept-wizard.png</key>
            <key type="filename">../images/source/cards/humans/human-disciple-sorcerer.png</key>
            <key type="filename">../images/source/cards/humans/human-disciple-theurge.png</key>
            <key type="filename">../images/source/cards/humans/human-disciple-warlock.png</key>
            <key type="filename">../images/source/cards/humans/human-disciple-wizard.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-axeman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-bowman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-crossbowman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-maceman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-slingman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-spearman.png</key>
            <key type="filename">../images/source/cards/humans/human-elite-swordsman.png</key>
            <key type="filename">../images/source/cards/humans/human-master-sorcerer.png</key>
            <key type="filename">../images/source/cards/humans/human-master-theurge.png</key>
            <key type="filename">../images/source/cards/humans/human-master-warlock.png</key>
            <key type="filename">../images/source/cards/humans/human-master-wizard.png</key>
            <key type="filename">../images/source/cards/humans/human-novice-sorcerer.png</key>
            <key type="filename">../images/source/cards/humans/human-novice-theurge.png</key>
            <key type="filename">../images/source/cards/humans/human-novice-warlock.png</key>
            <key type="filename">../images/source/cards/humans/human-novice-wizard.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-axeman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-bowman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-crossbowman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-maceman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-slingman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-spearman.png</key>
            <key type="filename">../images/source/cards/humans/human-regular-swordsman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-axeman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-bowman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-crossbowman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-maceman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-slingman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-spearman.png</key>
            <key type="filename">../images/source/cards/humans/human-rookie-swordsman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-axeman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-bowman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-crossbowman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-maceman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-slingman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-spearman.png</key>
            <key type="filename">../images/source/cards/humans/human-veteran-swordsman.png</key>
            <key type="filename">../images/source/cards/ogres/commander-gamomba.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-adept-wizard.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-disciple-wizard.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-elite-axeman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-elite-maceman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-elite-slingman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-master-wizard.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-novice-wizard.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-regular-axeman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-regular-maceman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-regular-slingman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-rookie-axeman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-rookie-maceman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-rookie-slingman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-veteran-axeman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-veteran-maceman.png</key>
            <key type="filename">../images/source/cards/ogres/ogre-veteran-slingman.png</key>
            <key type="filename">../images/source/cards/orcs/commander-ixohch.png</key>
            <key type="filename">../images/source/cards/orcs/orc-adept-sorcerer.png</key>
            <key type="filename">../images/source/cards/orcs/orc-adept-warlock.png</key>
            <key type="filename">../images/source/cards/orcs/orc-disciple-sorcerer.png</key>
            <key type="filename">../images/source/cards/orcs/orc-disciple-warlock.png</key>
            <key type="filename">../images/source/cards/orcs/orc-elite-axeman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-elite-bowman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-elite-maceman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-elite-spearman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-master-sorcerer.png</key>
            <key type="filename">../images/source/cards/orcs/orc-master-warlock.png</key>
            <key type="filename">../images/source/cards/orcs/orc-novice-sorcerer.png</key>
            <key type="filename">../images/source/cards/orcs/orc-novice-warlock.png</key>
            <key type="filename">../images/source/cards/orcs/orc-regular-axeman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-regular-bowman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-regular-maceman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-regular-spearman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-rookie-axeman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-rookie-bowman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-rookie-maceman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-rookie-spearman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-veteran-axeman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-veteran-bowman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-veteran-maceman.png</key>
            <key type="filename">../images/source/cards/orcs/orc-veteran-spearman.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>75,105,150,210</rect>
                <key>scale9Paddings</key>
                <rect>75,105,150,210</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileLists</key>
        <map type="SpriteSheetMap">
            <key>3</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>../images/source/cards/goblins/goblin-adept-sorcerer.png</filename>
                    <filename>../images/source/cards/goblins/goblin-disciple-sorcerer.png</filename>
                    <filename>../images/source/cards/goblins/goblin-elite-axeman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-elite-crossbowman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-elite-maceman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-master-sorcerer.png</filename>
                    <filename>../images/source/cards/goblins/goblin-novice-sorcerer.png</filename>
                    <filename>../images/source/cards/goblins/goblin-regular-axeman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-regular-crossbowman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-regular-maceman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-rookie-axeman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-rookie-crossbowman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-rookie-maceman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-veteran-axeman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-veteran-crossbowman.png</filename>
                    <filename>../images/source/cards/goblins/goblin-veteran-maceman.png</filename>
                    <filename>../images/source/cards/ogres/commander-gamomba.png</filename>
                    <filename>../images/source/cards/ogres/ogre-adept-wizard.png</filename>
                    <filename>../images/source/cards/ogres/ogre-disciple-wizard.png</filename>
                    <filename>../images/source/cards/ogres/ogre-elite-axeman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-elite-maceman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-elite-slingman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-master-wizard.png</filename>
                    <filename>../images/source/cards/ogres/ogre-novice-wizard.png</filename>
                    <filename>../images/source/cards/ogres/ogre-regular-axeman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-regular-maceman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-regular-slingman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-rookie-axeman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-rookie-maceman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-rookie-slingman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-veteran-axeman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-veteran-maceman.png</filename>
                    <filename>../images/source/cards/ogres/ogre-veteran-slingman.png</filename>
                    <filename>../images/source/cards/orcs/commander-ixohch.png</filename>
                    <filename>../images/source/cards/orcs/orc-adept-sorcerer.png</filename>
                    <filename>../images/source/cards/orcs/orc-adept-warlock.png</filename>
                    <filename>../images/source/cards/orcs/orc-disciple-sorcerer.png</filename>
                    <filename>../images/source/cards/orcs/orc-disciple-warlock.png</filename>
                    <filename>../images/source/cards/orcs/orc-elite-axeman.png</filename>
                    <filename>../images/source/cards/orcs/orc-elite-bowman.png</filename>
                    <filename>../images/source/cards/orcs/orc-elite-maceman.png</filename>
                    <filename>../images/source/cards/orcs/orc-elite-spearman.png</filename>
                    <filename>../images/source/cards/orcs/orc-master-sorcerer.png</filename>
                    <filename>../images/source/cards/orcs/orc-master-warlock.png</filename>
                    <filename>../images/source/cards/orcs/orc-novice-sorcerer.png</filename>
                    <filename>../images/source/cards/orcs/orc-novice-warlock.png</filename>
                    <filename>../images/source/cards/orcs/orc-regular-axeman.png</filename>
                    <filename>../images/source/cards/orcs/orc-regular-bowman.png</filename>
                    <filename>../images/source/cards/orcs/orc-regular-maceman.png</filename>
                    <filename>../images/source/cards/orcs/orc-regular-spearman.png</filename>
                    <filename>../images/source/cards/orcs/orc-rookie-axeman.png</filename>
                    <filename>../images/source/cards/orcs/orc-rookie-bowman.png</filename>
                    <filename>../images/source/cards/orcs/orc-rookie-maceman.png</filename>
                    <filename>../images/source/cards/orcs/orc-rookie-spearman.png</filename>
                    <filename>../images/source/cards/orcs/orc-veteran-axeman.png</filename>
                    <filename>../images/source/cards/orcs/orc-veteran-bowman.png</filename>
                    <filename>../images/source/cards/orcs/orc-veteran-maceman.png</filename>
                    <filename>../images/source/cards/orcs/orc-veteran-spearman.png</filename>
                    <filename>../images/source/cards/goblins/commander-wimis.png</filename>
                </array>
            </struct>
            <key>4</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>../images/source/cards/dwarves/commander-the-magnate.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-elite-axeman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-elite-crossbowman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-elite-maceman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-elite-spearman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-elite-swordsman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-regular-axeman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-regular-crossbowman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-regular-maceman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-regular-spearman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-regular-swordsman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-rookie-axeman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-rookie-crossbowman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-rookie-maceman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-rookie-spearman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-rookie-swordsman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-veteran-axeman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-veteran-crossbowman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-veteran-maceman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-veteran-spearman.png</filename>
                    <filename>../images/source/cards/dwarves/dwarf-veteran-swordsman.png</filename>
                    <filename>../images/source/cards/elves/elf-elite-slingman.png</filename>
                    <filename>../images/source/cards/elves/elf-elite-spearman.png</filename>
                    <filename>../images/source/cards/elves/elf-elite-swordsman.png</filename>
                    <filename>../images/source/cards/elves/elf-regular-slingman.png</filename>
                    <filename>../images/source/cards/elves/elf-regular-spearman.png</filename>
                    <filename>../images/source/cards/elves/elf-regular-swordsman.png</filename>
                    <filename>../images/source/cards/elves/elf-rookie-slingman.png</filename>
                    <filename>../images/source/cards/elves/elf-rookie-spearman.png</filename>
                    <filename>../images/source/cards/elves/elf-rookie-swordsman.png</filename>
                    <filename>../images/source/cards/elves/elf-veteran-slingman.png</filename>
                    <filename>../images/source/cards/elves/elf-veteran-spearman.png</filename>
                    <filename>../images/source/cards/elves/elf-veteran-swordsman.png</filename>
                </array>
            </struct>
            <key>5</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>../images/source/cards/humans/human-adept-sorcerer.png</filename>
                    <filename>../images/source/cards/humans/human-adept-theurge.png</filename>
                    <filename>../images/source/cards/humans/human-adept-wizard.png</filename>
                    <filename>../images/source/cards/humans/human-disciple-sorcerer.png</filename>
                    <filename>../images/source/cards/humans/human-disciple-theurge.png</filename>
                    <filename>../images/source/cards/humans/human-disciple-wizard.png</filename>
                    <filename>../images/source/cards/humans/human-elite-axeman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-bowman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-crossbowman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-maceman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-slingman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-spearman.png</filename>
                    <filename>../images/source/cards/humans/human-elite-swordsman.png</filename>
                    <filename>../images/source/cards/humans/human-master-sorcerer.png</filename>
                    <filename>../images/source/cards/humans/human-master-theurge.png</filename>
                    <filename>../images/source/cards/humans/human-master-wizard.png</filename>
                    <filename>../images/source/cards/humans/human-novice-sorcerer.png</filename>
                    <filename>../images/source/cards/humans/human-novice-theurge.png</filename>
                    <filename>../images/source/cards/humans/human-novice-wizard.png</filename>
                    <filename>../images/source/cards/humans/human-regular-axeman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-bowman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-crossbowman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-maceman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-slingman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-spearman.png</filename>
                    <filename>../images/source/cards/humans/human-regular-swordsman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-axeman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-bowman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-crossbowman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-maceman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-slingman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-spearman.png</filename>
                    <filename>../images/source/cards/humans/human-rookie-swordsman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-axeman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-bowman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-crossbowman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-maceman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-slingman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-spearman.png</filename>
                    <filename>../images/source/cards/humans/human-veteran-swordsman.png</filename>
                    <filename>../images/source/cards/humans/human-adept-warlock.png</filename>
                    <filename>../images/source/cards/humans/human-disciple-warlock.png</filename>
                    <filename>../images/source/cards/humans/human-master-warlock.png</filename>
                    <filename>../images/source/cards/humans/human-novice-warlock.png</filename>
                    <filename>../images/source/cards/astral-beings/spirit-card.png</filename>
                    <filename>../images/source/cards/astral-beings/tulpa-card.png</filename>
                </array>
            </struct>
        </map>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>try-pro-features</string>
            <string>enable-multipack</string>
            <string>large-max-texture-size</string>
            <string>pixijs-multipack-2023-05-25</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
