<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>6</int>
        <key>texturePackerVersion</key>
        <string>7.0.2</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename>../images/sprite-sheets/sprite-sheet-2.webp</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrQualityLevel</key>
        <uint>3</uint>
        <key>astcQualityLevel</key>
        <uint>2</uint>
        <key>basisUniversalQualityLevel</key>
        <uint>2</uint>
        <key>etc1QualityLevel</key>
        <uint>70</uint>
        <key>etc2QualityLevel</key>
        <uint>70</uint>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>4</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>3</uint>
        <key>webpQualityLevel</key>
        <uint>90</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">webp</enum>
        <key>borderPadding</key>
        <uint>4</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../images/sprite-sheets/sprite-sheet-2.json</filename>
            </struct>
        </map>
        <key>multiPackMode</key>
        <enum type="SettingsBase::MultiPackMode">MultiPackOff</enum>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../images/source/icons/activities/active.png</key>
            <key type="filename">../images/source/icons/activities/ended.png</key>
            <key type="filename">../images/source/icons/activities/in-use.png</key>
            <key type="filename">../images/source/icons/activities/suspended.png</key>
            <key type="filename">../images/source/icons/activities/withdrawn.png</key>
            <key type="filename">../images/source/icons/beings/angel.png</key>
            <key type="filename">../images/source/icons/beings/bear.png</key>
            <key type="filename">../images/source/icons/beings/bee.png</key>
            <key type="filename">../images/source/icons/beings/demon.png</key>
            <key type="filename">../images/source/icons/beings/dwarf.png</key>
            <key type="filename">../images/source/icons/beings/elf.png</key>
            <key type="filename">../images/source/icons/beings/gnome.png</key>
            <key type="filename">../images/source/icons/beings/goblin.png</key>
            <key type="filename">../images/source/icons/beings/golem.png</key>
            <key type="filename">../images/source/icons/beings/halfling.png</key>
            <key type="filename">../images/source/icons/beings/human.png</key>
            <key type="filename">../images/source/icons/beings/ogre.png</key>
            <key type="filename">../images/source/icons/beings/orc.png</key>
            <key type="filename">../images/source/icons/beings/salamander.png</key>
            <key type="filename">../images/source/icons/beings/spirit.png</key>
            <key type="filename">../images/source/icons/beings/tulpa.png</key>
            <key type="filename">../images/source/icons/beings/undead.png</key>
            <key type="filename">../images/source/icons/beings/wolf.png</key>
            <key type="filename">../images/source/icons/engagement-zones/dwarf-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/elf-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/gnome-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/goblin-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/halfling-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/human-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/ogre-background.png</key>
            <key type="filename">../images/source/icons/engagement-zones/orc-background.png</key>
            <key type="filename">../images/source/icons/grades/artifact.png</key>
            <key type="filename">../images/source/icons/grades/level-0.png</key>
            <key type="filename">../images/source/icons/grades/level-1.png</key>
            <key type="filename">../images/source/icons/grades/level-2.png</key>
            <key type="filename">../images/source/icons/grades/level-3.png</key>
            <key type="filename">../images/source/icons/grades/level-4.png</key>
            <key type="filename">../images/source/icons/grades/masterpiece.png</key>
            <key type="filename">../images/source/icons/grades/ordinary.png</key>
            <key type="filename">../images/source/icons/grades/relic.png</key>
            <key type="filename">../images/source/icons/items/garment.png</key>
            <key type="filename">../images/source/icons/items/implement.png</key>
            <key type="filename">../images/source/icons/items/weapon.png</key>
            <key type="filename">../images/source/icons/misc/fate-points.png</key>
            <key type="filename">../images/source/icons/misc/moves.png</key>
            <key type="filename">../images/source/icons/primitive-types/creature.png</key>
            <key type="filename">../images/source/icons/primitive-types/item.png</key>
            <key type="filename">../images/source/icons/primitive-types/spell.png</key>
            <key type="filename">../images/source/icons/readiness/exhausted.png</key>
            <key type="filename">../images/source/icons/readiness/occupied.png</key>
            <key type="filename">../images/source/icons/readiness/prepared.png</key>
            <key type="filename">../images/source/icons/readiness/unprepared.png</key>
            <key type="filename">../images/source/icons/roles/magical.png</key>
            <key type="filename">../images/source/icons/roles/melee.png</key>
            <key type="filename">../images/source/icons/roles/ranged.png</key>
            <key type="filename">../images/source/icons/sizes/large.png</key>
            <key type="filename">../images/source/icons/sizes/medium.png</key>
            <key type="filename">../images/source/icons/sizes/null-size.png</key>
            <key type="filename">../images/source/icons/sizes/small.png</key>
            <key type="filename">../images/source/icons/spells/apolar.png</key>
            <key type="filename">../images/source/icons/spells/enoth.png</key>
            <key type="filename">../images/source/icons/spells/faoth.png</key>
            <key type="filename">../images/source/icons/spells/immediate.png</key>
            <key type="filename">../images/source/icons/spells/metoth.png</key>
            <key type="filename">../images/source/icons/spells/negative.png</key>
            <key type="filename">../images/source/icons/spells/permanent.png</key>
            <key type="filename">../images/source/icons/spells/persistent.png</key>
            <key type="filename">../images/source/icons/spells/positive.png</key>
            <key type="filename">../images/source/icons/spells/powth.png</key>
            <key type="filename">../images/source/icons/spells/sustained.png</key>
            <key type="filename">../images/source/icons/spells/voth.png</key>
            <key type="filename">../images/source/icons/stats/mana-points.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/buttons/close-button.png</key>
            <key type="filename">../images/source/icons/dices/bear-die.png</key>
            <key type="filename">../images/source/icons/dices/bee-die.png</key>
            <key type="filename">../images/source/icons/dices/demon-die.png</key>
            <key type="filename">../images/source/icons/dices/dwarf-die.png</key>
            <key type="filename">../images/source/icons/dices/elf-die.png</key>
            <key type="filename">../images/source/icons/dices/gnome-die.png</key>
            <key type="filename">../images/source/icons/dices/goblin-die.png</key>
            <key type="filename">../images/source/icons/dices/golem-die.png</key>
            <key type="filename">../images/source/icons/dices/halfling-die.png</key>
            <key type="filename">../images/source/icons/dices/human-die.png</key>
            <key type="filename">../images/source/icons/dices/ogre-die.png</key>
            <key type="filename">../images/source/icons/dices/orc-die.png</key>
            <key type="filename">../images/source/icons/dices/salamander-die.png</key>
            <key type="filename">../images/source/icons/dices/spirit-die.png</key>
            <key type="filename">../images/source/icons/dices/undead-die.png</key>
            <key type="filename">../images/source/icons/dices/wolf-die.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/buttons/round-button-gray.png</key>
            <key type="filename">../images/source/icons/buttons/round-button-red.png</key>
            <key type="filename">../images/source/icons/misc/coin.png</key>
            <key type="filename">../images/source/icons/misc/config.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/blunt.png</key>
            <key type="filename">../images/source/icons/damages/ether.png</key>
            <key type="filename">../images/source/icons/maneuvers/bite.png</key>
            <key type="filename">../images/source/icons/maneuvers/throw.png</key>
            <key type="filename">../images/source/icons/stats/range.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/fire.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/mana.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,4,16,7</rect>
                <key>scale9Paddings</key>
                <rect>8,4,16,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/pierce.png</key>
            <key type="filename">../images/source/icons/items/sword.png</key>
            <key type="filename">../images/source/icons/maneuvers/scratch.png</key>
            <key type="filename">../images/source/icons/stats/agility.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,14,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,14,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/shock.png</key>
            <key type="filename">../images/source/icons/stats/energy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,8,9,16</rect>
                <key>scale9Paddings</key>
                <rect>5,8,9,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/damages/slash.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,8,8,16</rect>
                <key>scale9Paddings</key>
                <rect>4,8,8,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/engagement-zones/magical-symbol.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,8,32,15</rect>
                <key>scale9Paddings</key>
                <rect>16,8,32,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/engagement-zones/melee-symbol.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,14,32,29</rect>
                <key>scale9Paddings</key>
                <rect>16,14,32,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/engagement-zones/ranged-symbol.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,14,32,28</rect>
                <key>scale9Paddings</key>
                <rect>16,14,32,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/axe.png</key>
            <key type="filename">../images/source/icons/items/crossbow.png</key>
            <key type="filename">../images/source/icons/maneuvers/rapture.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,13,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,13,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/bow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,8,5,16</rect>
                <key>scale9Paddings</key>
                <rect>3,8,5,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/mace.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,8,15,16</rect>
                <key>scale9Paddings</key>
                <rect>7,8,15,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/shield.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,12,16</rect>
                <key>scale9Paddings</key>
                <rect>6,8,12,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/sling.png</key>
            <key type="filename">../images/source/icons/maneuvers/shoot.png</key>
            <key type="filename">../images/source/icons/stats/will.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,14</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/spear.png</key>
            <key type="filename">../images/source/icons/misc/star.png</key>
            <key type="filename">../images/source/icons/stats/power.png</key>
            <key type="filename">../images/source/icons/stats/recovery.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/items/unarmed.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,13</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/maneuvers/swarm.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,16,11</rect>
                <key>scale9Paddings</key>
                <rect>8,6,16,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/misc/arrow-double.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/misc/arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/misc/fate-points-symbol.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,32,31</rect>
                <key>scale9Paddings</key>
                <rect>16,15,32,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/stats/bonus.png</key>
            <key type="filename">../images/source/icons/stats/penalty.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0,0</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/stats/health.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,7,16,15</rect>
                <key>scale9Paddings</key>
                <rect>8,7,16,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/stats/initiative.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,5,16,9</rect>
                <key>scale9Paddings</key>
                <rect>8,5,16,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/stats/penetration.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../images/source/icons/stats/stamina.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,6,16,13</rect>
                <key>scale9Paddings</key>
                <rect>8,6,16,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileLists</key>
        <map type="SpriteSheetMap">
            <key>default</key>
            <struct type="SpriteSheet">
                <key>files</key>
                <array>
                    <filename>../images/source/icons/buttons/round-button-gray.png</filename>
                    <filename>../images/source/icons/buttons/round-button-red.png</filename>
                    <filename>../images/source/icons/misc/arrow.png</filename>
                    <filename>../images/source/icons/misc/coin.png</filename>
                    <filename>../images/source/icons/misc/config.png</filename>
                    <filename>../images/source/icons/items/axe.png</filename>
                    <filename>../images/source/icons/items/bow.png</filename>
                    <filename>../images/source/icons/items/crossbow.png</filename>
                    <filename>../images/source/icons/items/shield.png</filename>
                    <filename>../images/source/icons/items/sling.png</filename>
                    <filename>../images/source/icons/items/spear.png</filename>
                    <filename>../images/source/icons/items/sword.png</filename>
                    <filename>../images/source/icons/items/unarmed.png</filename>
                    <filename>../images/source/icons/items/mace.png</filename>
                    <filename>../images/source/icons/items/garment.png</filename>
                    <filename>../images/source/icons/items/implement.png</filename>
                    <filename>../images/source/icons/items/weapon.png</filename>
                    <filename>../images/source/icons/damages/ether.png</filename>
                    <filename>../images/source/icons/damages/fire.png</filename>
                    <filename>../images/source/icons/damages/mana.png</filename>
                    <filename>../images/source/icons/damages/pierce.png</filename>
                    <filename>../images/source/icons/damages/shock.png</filename>
                    <filename>../images/source/icons/damages/slash.png</filename>
                    <filename>../images/source/icons/damages/blunt.png</filename>
                    <filename>../images/source/icons/spells/apolar.png</filename>
                    <filename>../images/source/icons/spells/enoth.png</filename>
                    <filename>../images/source/icons/spells/faoth.png</filename>
                    <filename>../images/source/icons/spells/immediate.png</filename>
                    <filename>../images/source/icons/spells/metoth.png</filename>
                    <filename>../images/source/icons/spells/negative.png</filename>
                    <filename>../images/source/icons/spells/permanent.png</filename>
                    <filename>../images/source/icons/spells/persistent.png</filename>
                    <filename>../images/source/icons/spells/positive.png</filename>
                    <filename>../images/source/icons/spells/powth.png</filename>
                    <filename>../images/source/icons/spells/sustained.png</filename>
                    <filename>../images/source/icons/spells/voth.png</filename>
                    <filename>../images/source/icons/grades/artifact.png</filename>
                    <filename>../images/source/icons/grades/level-1.png</filename>
                    <filename>../images/source/icons/grades/level-2.png</filename>
                    <filename>../images/source/icons/grades/level-3.png</filename>
                    <filename>../images/source/icons/grades/level-4.png</filename>
                    <filename>../images/source/icons/grades/masterpiece.png</filename>
                    <filename>../images/source/icons/grades/ordinary.png</filename>
                    <filename>../images/source/icons/grades/relic.png</filename>
                    <filename>../images/source/icons/maneuvers/bite.png</filename>
                    <filename>../images/source/icons/maneuvers/rapture.png</filename>
                    <filename>../images/source/icons/maneuvers/scratch.png</filename>
                    <filename>../images/source/icons/maneuvers/shoot.png</filename>
                    <filename>../images/source/icons/maneuvers/swarm.png</filename>
                    <filename>../images/source/icons/maneuvers/throw.png</filename>
                    <filename>../images/source/icons/misc/fate-points.png</filename>
                    <filename>../images/source/icons/sizes/large.png</filename>
                    <filename>../images/source/icons/sizes/medium.png</filename>
                    <filename>../images/source/icons/sizes/small.png</filename>
                    <filename>../images/source/icons/stats/agility.png</filename>
                    <filename>../images/source/icons/stats/energy.png</filename>
                    <filename>../images/source/icons/stats/health.png</filename>
                    <filename>../images/source/icons/stats/initiative.png</filename>
                    <filename>../images/source/icons/stats/power.png</filename>
                    <filename>../images/source/icons/stats/range.png</filename>
                    <filename>../images/source/icons/stats/recovery.png</filename>
                    <filename>../images/source/icons/stats/stamina.png</filename>
                    <filename>../images/source/icons/stats/will.png</filename>
                    <filename>../images/source/icons/stats/penetration.png</filename>
                    <filename>../images/source/icons/sizes/null-size.png</filename>
                    <filename>../images/source/icons/grades/level-0.png</filename>
                    <filename>../images/source/icons/beings/golem.png</filename>
                    <filename>../images/source/icons/roles/magical.png</filename>
                    <filename>../images/source/icons/roles/melee.png</filename>
                    <filename>../images/source/icons/roles/ranged.png</filename>
                    <filename>../images/source/icons/beings/angel.png</filename>
                    <filename>../images/source/icons/beings/bear.png</filename>
                    <filename>../images/source/icons/beings/demon.png</filename>
                    <filename>../images/source/icons/beings/wolf.png</filename>
                    <filename>../images/source/icons/beings/bee.png</filename>
                    <filename>../images/source/icons/beings/dwarf.png</filename>
                    <filename>../images/source/icons/beings/elf.png</filename>
                    <filename>../images/source/icons/beings/gnome.png</filename>
                    <filename>../images/source/icons/beings/goblin.png</filename>
                    <filename>../images/source/icons/beings/halfling.png</filename>
                    <filename>../images/source/icons/beings/human.png</filename>
                    <filename>../images/source/icons/beings/ogre.png</filename>
                    <filename>../images/source/icons/beings/orc.png</filename>
                    <filename>../images/source/icons/beings/salamander.png</filename>
                    <filename>../images/source/icons/beings/undead.png</filename>
                    <filename>../images/source/icons/primitive-types/creature.png</filename>
                    <filename>../images/source/icons/primitive-types/item.png</filename>
                    <filename>../images/source/icons/primitive-types/spell.png</filename>
                    <filename>../images/source/icons/beings/spirit.png</filename>
                    <filename>../images/source/icons/beings/tulpa.png</filename>
                    <filename>../images/source/icons/activities/active.png</filename>
                    <filename>../images/source/icons/activities/ended.png</filename>
                    <filename>../images/source/icons/activities/suspended.png</filename>
                    <filename>../images/source/icons/activities/withdrawn.png</filename>
                    <filename>../images/source/icons/activities/in-use.png</filename>
                    <filename>../images/source/icons/misc/moves.png</filename>
                    <filename>../images/source/icons/misc/star.png</filename>
                    <filename>../images/source/icons/engagement-zones/dwarf-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/elf-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/gnome-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/goblin-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/halfling-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/human-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/magical-symbol.png</filename>
                    <filename>../images/source/icons/engagement-zones/melee-symbol.png</filename>
                    <filename>../images/source/icons/engagement-zones/ogre-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/orc-background.png</filename>
                    <filename>../images/source/icons/engagement-zones/ranged-symbol.png</filename>
                    <filename>../images/source/icons/stats/mana-points.png</filename>
                    <filename>../images/source/icons/readiness/exhausted.png</filename>
                    <filename>../images/source/icons/readiness/occupied.png</filename>
                    <filename>../images/source/icons/readiness/prepared.png</filename>
                    <filename>../images/source/icons/readiness/unprepared.png</filename>
                    <filename>../images/source/icons/dices/bear-die.png</filename>
                    <filename>../images/source/icons/dices/bee-die.png</filename>
                    <filename>../images/source/icons/dices/demon-die.png</filename>
                    <filename>../images/source/icons/dices/dwarf-die.png</filename>
                    <filename>../images/source/icons/dices/elf-die.png</filename>
                    <filename>../images/source/icons/dices/gnome-die.png</filename>
                    <filename>../images/source/icons/dices/goblin-die.png</filename>
                    <filename>../images/source/icons/dices/golem-die.png</filename>
                    <filename>../images/source/icons/dices/halfling-die.png</filename>
                    <filename>../images/source/icons/dices/human-die.png</filename>
                    <filename>../images/source/icons/dices/ogre-die.png</filename>
                    <filename>../images/source/icons/dices/orc-die.png</filename>
                    <filename>../images/source/icons/dices/salamander-die.png</filename>
                    <filename>../images/source/icons/dices/spirit-die.png</filename>
                    <filename>../images/source/icons/dices/undead-die.png</filename>
                    <filename>../images/source/icons/dices/wolf-die.png</filename>
                    <filename>../images/source/icons/stats/bonus.png</filename>
                    <filename>../images/source/icons/stats/penalty.png</filename>
                    <filename>../images/source/icons/misc/fate-points-symbol.png</filename>
                    <filename>../images/source/icons/buttons/close-button.png</filename>
                    <filename>../images/source/icons/misc/arrow-double.png</filename>
                </array>
            </struct>
        </map>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>pixijs-multipack-2023-05-25</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
