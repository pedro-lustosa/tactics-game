// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const events = {};

/// Propriedades iniciais
defineProperties: {
  // Para início da alternação do modo informativo
  events.infoModeStart = {};

  // Para fim da alternação do modo informativo
  events.infoModeEnd = {};

  // Para início de mudanças de telas
  events.screenChangeStart = {};

  // Para fim de mudanças de telas
  events.screenChangeEnd = {};

  // Para início de mudanças de barra estática
  events.staticBarChangeStart = {};

  // Para fim de mudanças de barra estática
  events.staticBarChangeEnd = {};

  // Para início de mudanças de modal
  events.modalChangeStart = {};

  // Para fim de mudanças de modal
  events.modalChangeEnd = {};

  // Para início de mudanças em molduras
  events.frameChangeStart = {};

  // Para fim de mudanças em molduras
  events.frameChangeEnd = {};

  // Para início de paradas de uma partida
  events.breakStart = {};

  // Para fim de paradas de uma partida
  events.breakEnd = {};

  // Para início de mudanças no conteúdo de cartas
  events.cardContentStart = {};

  // Para fim de mudanças no conteúdo de cartas
  events.cardContentEnd = {};

  // Para início de mudanças na atividade de cartas
  events.cardActivityStart = {};

  // Para fim de mudanças na atividade de cartas
  events.cardActivityEnd = {};

  // Para início de mudanças no preparo de entes
  events.beingReadinessStart = {};

  // Para fim de mudanças no preparo de entes
  events.beingReadinessEnd = {};

  // Para início de mudanças na vinculação de componentes
  events.attachabilityChangeStart = {};

  // Para fim de mudanças na vinculação de componentes
  events.attachabilityChangeEnd = {};

  // Para início de mudanças no uso de cartas
  events.cardUseStart = {};

  // Para fim de mudanças no uso de cartas
  events.cardUseEnd = {};

  // Para início de mudanças na ativação de efeitos
  events.cardEffectStart = {};

  // Para fim de mudanças na ativação de efeitos
  events.cardEffectEnd = {};

  // Para início de mudanças em convocações
  events.summonChangeStart = {};

  // Para fim de mudanças em convocações
  events.summonChangeEnd = {};

  // Para início de mudanças em baralhos
  events.deckChangeStart = {};

  // Para fim de mudanças em baralhos
  events.deckChangeEnd = {};

  // Para início de mudanças em ocupantes de casas do campo
  events.fieldSlotCardChangeStart = {};

  // Para fim de mudanças em ocupantes de casas do campo
  events.fieldSlotCardChangeEnd = {};

  // Para início de mudanças em fluxo
  events.flowChangeStart = {};

  // Para fim de mudanças em fluxo
  events.flowChangeEnd = {};

  // Para início de mudanças em jogadas
  events.moveChangeStart = {};

  // Para fim de mudanças em jogadas
  events.moveChangeEnd = {};

  // Para início de mudanças em ações
  events.actionChangeStart = {};

  // Para fim de mudanças em ações
  events.actionChangeEnd = {};

  // Para início de mudanças em pontos de destino
  events.fatePointsChangeStart = {};

  // Para fim de mudanças em pontos de destino
  events.fatePointsChangeEnd = {};

  // Para início de mudanças de danos
  events.damageChangeStart = {};

  // Para fim de mudanças de danos
  events.damageChangeEnd = {};

  // Para início de mudanças em condições
  events.conditionChangeStart = {};

  // Para fim de mudanças em condições
  events.conditionChangeEnd = {};

  // Para início de registro de eventos de uma partida
  events.logChangeStart = {};

  // Para fim de registro de eventos de uma partida
  events.logChangeEnd = {};

  // Métodos não configuráveis de 'events'
  Object.defineProperties( events, {
    // Adiciona evento
    addEvent: {
      value: function ( eventName, target, handler, config = {} ) {
        // Identificadores
        var method = config.once ? 'once' : 'addListener';

        // Adição do evento
        return config.context ?
          target[ method ]( eventName, handler, config.context ) :
          target[ method ]( eventName, handler );
      }
    },
    // Remove evento
    removeEvent: {
      value: function ( eventName, target, handler, config = {} ) {
        // Remoção do evento
        return config.context ?
          target.removeListener( eventName, handler, config.context ) :
          target.removeListener( eventName, handler );
      }
    },
    // Emite eventos
    emitEvents: {
      value: function ( target, eventData, ...eventNames ) {
        // Apenas executa função caso o alvo possa emitir eventos
        if( !target._events ) return false;

        // Identificadores
        var offset = [ '-start-', '-end-' ].find( value => eventNames[ 0 ].includes( value ) );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( eventData.constructor == Object );
          m.oAssert( offset );
        }

        // Adiciona aos dados de evento aquele que engatilhou ativação da cadeia de eventos
        eventData.eventName = eventNames[ 0 ];

        // Adiciona aos dados de evento sua categoria
        eventData.eventCategory = eventData.eventName.slice( 0, eventData.eventName.search( offset ) );

        // Adiciona aos dados de evento sua fase
        eventData.eventPhase = offset.slice( 1, -1 );

        // Adiciona aos dados de evento seu tipo
        eventData.eventType = eventData.eventName.slice( eventData.eventName.search( offset ) + offset.length );

        // Adiciona aos dados de evento seu alvo
        eventData.eventTarget = target;

        // Emissão dos eventos passados
        for( let eventName of eventNames ) target.emit( eventName, eventData );
      }
    }
  } );

  // Atribuição de propriedades de 'infoModeStart'
  infoModeStart: {
    // Identificadores
    let { infoModeStart } = events;

    // Qualquer mudança do modo informativo
    infoModeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-start-any', target, handler, config )
    };

    // Entrada no modo informativo
    infoModeStart.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-start-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-start-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'info-mode-start-enter', 'info-mode-start-any' );
      }
    };

    // Saída do modo informativo
    infoModeStart.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-start-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-start-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'info-mode-start-leave', 'info-mode-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'infoModeEnd'
  infoModeEnd: {
    // Identificadores
    let { infoModeEnd } = events;

    // Qualquer mudança do modo informativo
    infoModeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-end-any', target, handler, config )
    };

    // Entrada no modo informativo
    infoModeEnd.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-end-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-end-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'info-mode-end-enter', 'info-mode-end-any' );
      }
    };

    // Saída do modo informativo
    infoModeEnd.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'info-mode-end-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'info-mode-end-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'info-mode-end-leave', 'info-mode-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'screenChangeStart'
  screenChangeStart: {
    // Identificadores
    let { screenChangeStart } = events;

    // Qualquer mudança de tela
    screenChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-start-any', target, handler, config )
    };

    // Entrada de tela
    screenChangeStart.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-start-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-start-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'screen-change-start-enter', 'screen-change-start-any' );
      }
    };

    // Saída de tela
    screenChangeStart.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-start-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-start-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'screen-change-start-leave', 'screen-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'screenChangeEnd'
  screenChangeEnd: {
    // Identificadores
    let { screenChangeEnd } = events;

    // Qualquer mudança de tela
    screenChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-end-any', target, handler, config )
    };

    // Entrada de tela
    screenChangeEnd.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-end-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-end-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'screen-change-end-enter', 'screen-change-end-any' );
      }
    };

    // Saída de tela
    screenChangeEnd.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'screen-change-end-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'screen-change-end-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'screen-change-end-leave', 'screen-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'staticBarChangeStart'
  staticBarChangeStart: {
    // Identificadores
    let { staticBarChangeStart } = events;

    // Qualquer mudança de barra estática
    staticBarChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-start-any', target, handler, config )
    };

    // Inserção de barra estática
    staticBarChangeStart.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-start-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-start-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'static-bar-change-start-insert', 'static-bar-change-start-any' );
      }
    };

    // Remoção de barra estática
    staticBarChangeStart.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-start-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-start-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'static-bar-change-start-remove', 'static-bar-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'staticBarChangeEnd'
  staticBarChangeEnd: {
    // Identificadores
    let { staticBarChangeEnd } = events;

    // Qualquer mudança de barra estática
    staticBarChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-end-any', target, handler, config )
    };

    // Inserção de barra estática
    staticBarChangeEnd.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-end-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-end-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'static-bar-change-end-insert', 'static-bar-change-end-any' );
      }
    };

    // Remoção de barra estática
    staticBarChangeEnd.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'static-bar-change-end-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'static-bar-change-end-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'static-bar-change-end-remove', 'static-bar-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'modalChangeStart'
  modalChangeStart: {
    // Identificadores
    let { modalChangeStart } = events;

    // Qualquer mudança de modal
    modalChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-start-any', target, handler, config )
    };

    // Inserção de modal
    modalChangeStart.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-start-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-start-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'modal-change-start-insert', 'modal-change-start-any' );
      }
    };

    // Remoção de modal
    modalChangeStart.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-start-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-start-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'modal-change-start-remove', 'modal-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'modalChangeEnd'
  modalChangeEnd: {
    // Identificadores
    let { modalChangeEnd } = events;

    // Qualquer mudança de modal
    modalChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-end-any', target, handler, config )
    };

    // Inserção de modal
    modalChangeEnd.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-end-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-end-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'modal-change-end-insert', 'modal-change-end-any' );
      }
    };

    // Remoção de modal
    modalChangeEnd.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'modal-change-end-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'modal-change-end-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'modal-change-end-remove', 'modal-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'frameChangeStart'
  frameChangeStart: {
    // Identificadores
    let { frameChangeStart } = events;

    // Qualquer mudança da moldura
    frameChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-start-any', target, handler, config )
    };

    // Mudança de contedor na moldura
    frameChangeStart.container = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-start-container', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-start-container', target, handler, config )
    };

    // Entrada de contedor na moldura
    frameChangeStart.containerIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-start-container-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-start-container-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-start-container-in', 'frame-change-start-container', 'frame-change-start-any' );
      }
    };

    // Saída de contedor da moldura
    frameChangeStart.containerOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-start-container-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-start-container-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-start-container-out', 'frame-change-start-container', 'frame-change-start-any' );
      }
    };

    // Mudança de componente na moldura
    frameChangeStart.component = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-start-component', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-start-component', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-start-component', 'frame-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'frameChangeEnd'
  frameChangeEnd: {
    // Identificadores
    let { frameChangeEnd } = events;

    // Qualquer mudança da moldura
    frameChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-end-any', target, handler, config )
    };

    // Mudança de contedor na moldura
    frameChangeEnd.container = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-end-container', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-end-container', target, handler, config )
    };

    // Entrada de contedor na moldura
    frameChangeEnd.containerIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-end-container-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-end-container-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-end-container-in', 'frame-change-end-container', 'frame-change-end-any' );
      }
    };

    // Saída de contedor da moldura
    frameChangeEnd.containerOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-end-container-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-end-container-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-end-container-out', 'frame-change-end-container', 'frame-change-end-any' );
      }
    };

    // Mudança de componente na moldura
    frameChangeEnd.component = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'frame-change-end-component', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'frame-change-end-component', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'frame-change-end-component', 'frame-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'breakStart'
  breakStart: {
    // Identificadores
    let { breakStart } = events;

    // Qualquer parada
    breakStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-start-any', target, handler, config )
    };

    // Paradas de combate
    breakStart.combat = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-start-combat', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-start-combat', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-start-combat', 'break-start-any' );
      }
    };

    // Paradas de ataques livres
    breakStart.freeAttack = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-start-free-attack', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-start-free-attack', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-start-free-attack', 'break-start-any' );
      }
    };

    // Paradas de destino
    breakStart.fate = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-start-fate', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-start-fate', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-start-fate', 'break-start-any' );
      }
    };
  };

  // Atribuição de propriedades de 'breakEnd'
  breakEnd: {
    // Identificadores
    let { breakEnd } = events;

    // Qualquer parada
    breakEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-end-any', target, handler, config )
    };

    // Paradas de combate
    breakEnd.combat = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-end-combat', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-end-combat', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-end-combat', 'break-end-any' );
      }
    };

    // Paradas de ataques livres
    breakEnd.freeAttack = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-end-free-attack', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-end-free-attack', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-end-free-attack', 'break-end-any' );
      }
    };

    // Paradas de destino
    breakEnd.fate = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'break-end-fate', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'break-end-fate', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'break-end-fate', 'break-end-any' );
      }
    };
  };

  // Atribuição de propriedades de 'cardContentStart'
  cardContentStart: {
    // Identificadores
    let { cardContentStart } = events;

    // Qualquer mudança de conteúdo
    cardContentStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-any', target, handler, config )
    };

    // Mudança de tamanho
    cardContentStart.size = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-size', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-size', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-size', 'card-content-start-any' );
      }
    };

    // Mudança de atributo
    cardContentStart.attribute = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-attribute', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-attribute', target, handler, config )
    };

    // Mudança de vida
    cardContentStart.health = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-health', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-health', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-health', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de iniciativa
    cardContentStart.initiative = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-initiative', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-initiative', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-initiative', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de vigor
    cardContentStart.stamina = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-stamina', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-stamina', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-stamina', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de agilidade
    cardContentStart.agility = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-agility', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-agility', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-agility', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de vontade
    cardContentStart.will = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-will', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-will', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-will', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de energia
    cardContentStart.energy = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-energy', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-energy', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-energy', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de poder
    cardContentStart.power = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-power', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-power', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-power', 'card-content-start-attribute', 'card-content-start-any' );
      }
    };

    // Mudança de mana
    cardContentStart.mana = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-mana', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-mana', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-mana', 'card-content-start-any' );
      }
    };

    // Mudança de custo de mana
    cardContentStart.manaCost = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-mana-cost', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-mana-cost', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-mana-cost', 'card-content-start-any' );
      }
    };

    // Mudança de custo de fluxo
    cardContentStart.flowCost = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-flow-cost', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-flow-cost', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-flow-cost', 'card-content-start-any' );
      }
    };

    // Mudança de potência
    cardContentStart.potency = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-potency', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-potency', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-potency', 'card-content-start-any' );
      }
    };

    // Mudança de efetividade
    cardContentStart.effectivity = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-effectivity', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-effectivity', target, handler, config )
    };

    // Mudança de manobras
    cardContentStart.maneuver = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-maneuver', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-maneuver', target, handler, config )
    };

    // Mudança de fonte de manobras
    cardContentStart.maneuverSource = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-maneuver-source', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-maneuver-source', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-content-start-maneuver-source', 'card-content-start-maneuver', 'card-content-start-effectivity', 'card-content-start-any'
        );
      }
    };

    // Mudança de pontos de manobra
    cardContentStart.maneuverPoints = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-maneuver-points', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-maneuver-points', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-content-start-maneuver-points', 'card-content-start-maneuver', 'card-content-start-effectivity', 'card-content-start-any'
        );
      }
    };

    // Mudança de condutividade
    cardContentStart.conductivity = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-conductivity', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-conductivity', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-conductivity', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de recuperação
    cardContentStart.recovery = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-recovery', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-recovery', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-recovery', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de dano
    cardContentStart.damage = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-damage', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-damage', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-damage', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de penetração
    cardContentStart.penetration = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-penetration', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-penetration', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-penetration', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de alcance
    cardContentStart.range = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-range', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-range', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-range', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de resistência
    cardContentStart.resistance = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-resistance', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-resistance', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-resistance', 'card-content-start-effectivity', 'card-content-start-any' );
      }
    };

    // Mudança de pontos de destino embutidos
    cardContentStart.fate = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-fate', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-fate', target, handler, config )
    };

    // Mudança para entrada de fonte de pontos de destino embutidos
    cardContentStart.fateIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-fate-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-fate-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-fate-in', 'card-content-start-fate', 'card-content-start-any' );
      }
    };

    // Mudança para uso de pontos de destino embutidos
    cardContentStart.fateSpent = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-fate-spent', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-fate-spent', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-fate-spent', 'card-content-start-fate', 'card-content-start-any' );
      }
    };

    // Mudança para saída de fonte de pontos de destino embutidos
    cardContentStart.fateOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-fate-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-fate-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-fate-out', 'card-content-start-fate', 'card-content-start-any' );
      }
    };

    // Mudança para redefinição de fontes de pontos de destino embutidos
    cardContentStart.fateReset = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-fate-reset', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-fate-reset', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-fate-reset', 'card-content-start-fate', 'card-content-start-any' );
      }
    };

    // Mudança de equipamento
    cardContentStart.item = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-item', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-item', target, handler, config )
    };

    // Mudança para entrada de equipamento
    cardContentStart.itemIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-item-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-item-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-item-in', 'card-content-start-item', 'card-content-start-any' );
      }
    };

    // Mudança para saída de equipamento
    cardContentStart.itemOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-start-item-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-start-item-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-start-item-out', 'card-content-start-item', 'card-content-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardContentEnd'
  cardContentEnd: {
    // Identificadores
    let { cardContentEnd } = events;

    // Qualquer mudança de conteúdo
    cardContentEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-any', target, handler, config )
    };

    // Mudança de tamanho
    cardContentEnd.size = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-size', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-size', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-size', 'card-content-end-any' );
      }
    };

    // Mudança de atributo
    cardContentEnd.attribute = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-attribute', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-attribute', target, handler, config )
    };

    // Mudança de vida
    cardContentEnd.health = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-health', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-health', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-health', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de iniciativa
    cardContentEnd.initiative = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-initiative', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-initiative', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-initiative', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de vigor
    cardContentEnd.stamina = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-stamina', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-stamina', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-stamina', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de agilidade
    cardContentEnd.agility = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-agility', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-agility', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-agility', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de vontade
    cardContentEnd.will = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-will', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-will', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-will', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de energia
    cardContentEnd.energy = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-energy', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-energy', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-energy', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de poder
    cardContentEnd.power = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-power', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-power', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-power', 'card-content-end-attribute', 'card-content-end-any' );
      }
    };

    // Mudança de mana
    cardContentEnd.mana = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-mana', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-mana', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-mana', 'card-content-end-any' );
      }
    };

    // Mudança de custo de mana
    cardContentEnd.manaCost = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-mana-cost', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-mana-cost', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-mana-cost', 'card-content-end-any' );
      }
    };

    // Mudança de custo de fluxo
    cardContentEnd.flowCost = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-flow-cost', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-flow-cost', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-flow-cost', 'card-content-end-any' );
      }
    };

    // Mudança de potência
    cardContentEnd.potency = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-potency', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-potency', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-potency', 'card-content-end-any' );
      }
    };

    // Mudança de efetividade
    cardContentEnd.effectivity = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-effectivity', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-effectivity', target, handler, config )
    };

    // Mudança de manobras
    cardContentEnd.maneuver = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-maneuver', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-maneuver', target, handler, config )
    };

    // Mudança de fonte de manobras
    cardContentEnd.maneuverSource = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-maneuver-source', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-maneuver-source', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-content-end-maneuver-source', 'card-content-end-maneuver', 'card-content-end-effectivity', 'card-content-end-any'
        );
      }
    };

    // Mudança de pontos de manobra
    cardContentEnd.maneuverPoints = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-maneuver-points', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-maneuver-points', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-content-end-maneuver-points', 'card-content-end-maneuver', 'card-content-end-effectivity', 'card-content-end-any'
        );
      }
    };

    // Mudança de condutividade
    cardContentEnd.conductivity = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-conductivity', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-conductivity', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-conductivity', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de recuperação
    cardContentEnd.recovery = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-recovery', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-recovery', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-recovery', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de dano
    cardContentEnd.damage = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-damage', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-damage', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-damage', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de penetração
    cardContentEnd.penetration = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-penetration', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-penetration', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-penetration', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de alcance
    cardContentEnd.range = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-range', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-range', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-range', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de resistência
    cardContentEnd.resistance = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-resistance', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-resistance', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-resistance', 'card-content-end-effectivity', 'card-content-end-any' );
      }
    };

    // Mudança de pontos de destino embutidos
    cardContentEnd.fate = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-fate', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-fate', target, handler, config )
    };

    // Mudança para entrada de fonte de pontos de destino embutidos
    cardContentEnd.fateIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-fate-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-fate-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-fate-in', 'card-content-end-fate', 'card-content-end-any' );
      }
    };

    // Mudança para uso de pontos de destino embutidos
    cardContentEnd.fateSpent = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-fate-spent', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-fate-spent', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-fate-spent', 'card-content-end-fate', 'card-content-end-any' );
      }
    };

    // Mudança para saída de fonte de pontos de destino embutidos
    cardContentEnd.fateOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-fate-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-fate-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-fate-out', 'card-content-end-fate', 'card-content-end-any' );
      }
    };

    // Mudança para redefinição de fontes de pontos de destino embutidos
    cardContentEnd.fateReset = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-fate-reset', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-fate-reset', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-fate-reset', 'card-content-end-fate', 'card-content-end-any' );
      }
    };

    // Mudança de equipamento
    cardContentEnd.item = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-item', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-item', target, handler, config )
    };

    // Mudança para entrada de equipamento
    cardContentEnd.itemIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-item-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-item-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-item-in', 'card-content-end-item', 'card-content-end-any' );
      }
    };

    // Mudança para saída de equipamento
    cardContentEnd.itemOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-content-end-item-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-content-end-item-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-content-end-item-out', 'card-content-end-item', 'card-content-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardActivityStart'
  cardActivityStart: {
    // Identificadores
    let { cardActivityStart } = events;

    // Qualquer mudança de atividade
    cardActivityStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-any', target, handler, config )
    };

    // Mudança para estado ativo
    cardActivityStart.active = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-active', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-active', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-start-active', 'card-activity-start-any' );
      }
    };

    // Mudança para estado inativo
    cardActivityStart.inactive = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-inactive', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-inactive', target, handler, config )
    };

    // Mudança para estado suspenso
    cardActivityStart.suspended = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-suspended', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-suspended', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-start-suspended', 'card-activity-start-inactive', 'card-activity-start-any' );
      }
    };

    // Mudança para estado removido
    cardActivityStart.removed = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-removed', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-removed', target, handler, config )
    };

    // Mudança para estado afastado
    cardActivityStart.withdrawn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-withdrawn', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-withdrawn', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-activity-start-withdrawn', 'card-activity-start-removed', 'card-activity-start-inactive', 'card-activity-start-any'
        );
      }
    };

    // Mudança para estado findado
    cardActivityStart.ended = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-ended', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-ended', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-activity-start-ended', 'card-activity-start-removed', 'card-activity-start-inactive', 'card-activity-start-any'
        );
      }
    };

    // Mudança para estado alheio
    cardActivityStart.unlinked = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-start-unlinked', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-start-unlinked', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-start-unlinked', 'card-activity-start-removed', 'card-activity-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardActivityEnd'
  cardActivityEnd: {
    // Identificadores
    let { cardActivityEnd } = events;

    // Qualquer mudança de atividade
    cardActivityEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-any', target, handler, config )
    };

    // Mudança para estado ativo
    cardActivityEnd.active = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-active', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-active', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-end-active', 'card-activity-end-any' );
      }
    };

    // Mudança para estado inativo
    cardActivityEnd.inactive = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-inactive', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-inactive', target, handler, config )
    };

    // Mudança para estado suspenso
    cardActivityEnd.suspended = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-suspended', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-suspended', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-end-suspended', 'card-activity-end-inactive', 'card-activity-end-any' );
      }
    };

    // Mudança para estado removido
    cardActivityEnd.removed = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-removed', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-removed', target, handler, config )
    };

    // Mudança para estado afastado
    cardActivityEnd.withdrawn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-withdrawn', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-withdrawn', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-activity-end-withdrawn', 'card-activity-end-removed', 'card-activity-end-inactive', 'card-activity-end-any'
        );
      }
    };

    // Mudança para estado findado
    cardActivityEnd.ended = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-ended', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-ended', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'card-activity-end-ended', 'card-activity-end-removed', 'card-activity-end-inactive', 'card-activity-end-any'
        );
      }
    };

    // Mudança para estado alheio
    cardActivityEnd.unlinked = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-activity-end-unlinked', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-activity-end-unlinked', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-activity-end-unlinked', 'card-activity-end-removed', 'card-activity-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'beingReadinessStart'
  beingReadinessStart: {
    // Identificadores
    let { beingReadinessStart } = events;

    // Qualquer mudança de preparo
    beingReadinessStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-any', target, handler, config )
    };

    // Mudança para estado preparado
    beingReadinessStart.prepared = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-prepared', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-prepared', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-start-prepared', 'being-readiness-start-any' );
      }
    };

    // Mudança para estado de indisponibilidade
    beingReadinessStart.unready = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-unready', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-unready', target, handler, config )
    };

    // Mudança para estado despreparado
    beingReadinessStart.unprepared = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-unprepared', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-unprepared', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-start-unprepared', 'being-readiness-start-unready', 'being-readiness-start-any' );
      }
    };

    // Mudança para estado ocupado
    beingReadinessStart.occupied = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-occupied', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-occupied', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-start-occupied', 'being-readiness-start-unready', 'being-readiness-start-any' );
      }
    };

    // Mudança para estado esgotado
    beingReadinessStart.exhausted = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-start-exhausted', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-start-exhausted', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-start-exhausted', 'being-readiness-start-unready', 'being-readiness-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'beingReadinessEnd'
  beingReadinessEnd: {
    // Identificadores
    let { beingReadinessEnd } = events;

    // Qualquer mudança de preparo
    beingReadinessEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-any', target, handler, config )
    };

    // Mudança para estado preparado
    beingReadinessEnd.prepared = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-prepared', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-prepared', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-end-prepared', 'being-readiness-end-any' );
      }
    };

    // Mudança para estado de indisponibilidade
    beingReadinessEnd.unready = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-unready', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-unready', target, handler, config )
    };

    // Mudança para estado despreparado
    beingReadinessEnd.unprepared = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-unprepared', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-unprepared', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-end-unprepared', 'being-readiness-end-unready', 'being-readiness-end-any' );
      }
    };

    // Mudança para estado ocupado
    beingReadinessEnd.occupied = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-occupied', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-occupied', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-end-occupied', 'being-readiness-end-unready', 'being-readiness-end-any' );
      }
    };

    // Mudança para estado esgotado
    beingReadinessEnd.exhausted = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'being-readiness-end-exhausted', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'being-readiness-end-exhausted', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'being-readiness-end-exhausted', 'being-readiness-end-unready', 'being-readiness-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'attachabilityChangeStart'
  attachabilityChangeStart: {
    // Identificadores
    let { attachabilityChangeStart } = events;

    // Qualquer mudança de vinculação
    attachabilityChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-any', target, handler, config )
    };

    // Mudança de vinculante
    attachabilityChangeStart.attacher = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attacher', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attacher', target, handler, config )
    };

    // Mudança para entrada em vinculante
    attachabilityChangeStart.attacherIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attacher-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attacher-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-start-attacher-in', 'attachability-change-start-attacher', 'attachability-change-start-any'
        );
      }
    };

    // Mudança para saída de vinculante
    attachabilityChangeStart.attacherOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attacher-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attacher-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-start-attacher-out', 'attachability-change-start-attacher', 'attachability-change-start-any'
        );
      }
    };

    // Mudança de vinculado
    attachabilityChangeStart.attachment = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attachment', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attachment', target, handler, config )
    };

    // Mudança para entrada de vinculado
    attachabilityChangeStart.attachmentIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attachment-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attachment-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-start-attachment-in', 'attachability-change-start-attachment', 'attachability-change-start-any'
        );
      }
    };

    // Mudança para saída de vinculado
    attachabilityChangeStart.attachmentOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-start-attachment-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-start-attachment-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-start-attachment-out', 'attachability-change-start-attachment', 'attachability-change-start-any'
        );
      }
    };
  }

  // Atribuição de propriedades de 'attachabilityChangeEnd'
  attachabilityChangeEnd: {
    // Identificadores
    let { attachabilityChangeEnd } = events;

    // Qualquer mudança de vinculação
    attachabilityChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-any', target, handler, config )
    };

    // Mudança de vinculante
    attachabilityChangeEnd.attacher = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attacher', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attacher', target, handler, config )
    };

    // Mudança para entrada em vinculante
    attachabilityChangeEnd.attacherIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attacher-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attacher-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-end-attacher-in', 'attachability-change-end-attacher', 'attachability-change-end-any'
        );
      }
    };

    // Mudança para saída de vinculante
    attachabilityChangeEnd.attacherOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attacher-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attacher-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-end-attacher-out', 'attachability-change-end-attacher', 'attachability-change-end-any'
        );
      }
    };

    // Mudança de vinculado
    attachabilityChangeEnd.attachment = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attachment', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attachment', target, handler, config )
    };

    // Mudança para entrada de vinculado
    attachabilityChangeEnd.attachmentIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attachment-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attachment-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-end-attachment-in', 'attachability-change-end-attachment', 'attachability-change-end-any'
        );
      }
    };

    // Mudança para saída de vinculado
    attachabilityChangeEnd.attachmentOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'attachability-change-end-attachment-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'attachability-change-end-attachment-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'attachability-change-end-attachment-out', 'attachability-change-end-attachment', 'attachability-change-end-any'
        );
      }
    };
  }

  // Atribuição de propriedades de 'cardUseStart'
  cardUseStart: {
    // Identificadores
    let { cardUseStart } = events;

    // Qualquer mudança de uso
    cardUseStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-start-any', target, handler, config )
    };

    // Mudança para estado usado ou desusado
    cardUseStart.useInOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-start-use-in-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-start-use-in-out', target, handler, config )
    };

    // Mudança para estado usado
    cardUseStart.useIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-start-use-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-start-use-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-start-use-in', 'card-use-start-use-in-out', 'card-use-start-any' );
      }
    };

    // Mudança para estado desusado
    cardUseStart.useOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-start-use-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-start-use-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-start-use-out', 'card-use-start-use-in-out', 'card-use-start-any' );
      }
    };

    // Para magias, aplicação de sustentação do uso
    cardUseStart.sustain = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-start-sustain', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-start-sustain', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-start-sustain', 'card-use-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardUseEnd'
  cardUseEnd: {
    // Identificadores
    let { cardUseEnd } = events;

    // Qualquer mudança de uso
    cardUseEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-end-any', target, handler, config )
    };

    // Mudança para estado usado ou desusado
    cardUseEnd.useInOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-end-use-in-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-end-use-in-out', target, handler, config )
    };

    // Mudança para estado usado
    cardUseEnd.useIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-end-use-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-end-use-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-end-use-in', 'card-use-end-use-in-out', 'card-use-end-any' );
      }
    };

    // Mudança para estado desusado
    cardUseEnd.useOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-end-use-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-end-use-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-end-use-out', 'card-use-end-use-in-out', 'card-use-end-any' );
      }
    };

    // Para magias, aplicação de sustentação do uso
    cardUseEnd.sustain = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-use-end-sustain', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-use-end-sustain', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-use-end-sustain', 'card-use-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardEffectStart'
  cardEffectStart: {
    // Identificadores
    let { cardEffectStart } = events;

    // Qualquer mudança de efeito
    cardEffectStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-start-any', target, handler, config )
    };

    // Habilitação de um efeito
    cardEffectStart.enable = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-start-enable', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-start-enable', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-effect-start-enable', 'card-effect-start-any' );
      }
    };

    // Desabilitação de um efeito
    cardEffectStart.disable = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-start-disable', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-start-disable', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-effect-start-disable', 'card-effect-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'cardEffectEnd'
  cardEffectEnd: {
    // Identificadores
    let { cardEffectEnd } = events;

    // Qualquer mudança de efeito
    cardEffectEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-end-any', target, handler, config )
    };

    // Habilitação de um efeito
    cardEffectEnd.enable = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-end-enable', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-end-enable', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-effect-end-enable', 'card-effect-end-any' );
      }
    };

    // Desabilitação de um efeito
    cardEffectEnd.disable = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'card-effect-end-disable', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'card-effect-end-disable', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'card-effect-end-disable', 'card-effect-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'summonChangeStart'
  summonChangeStart: {
    // Identificadores
    let { summonChangeStart } = events;

    // Qualquer mudança de convocação
    summonChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-start-any', target, handler, config )
    };

    // Mudança para entrada de uma convocação
    summonChangeStart.summonIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-start-summon-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-start-summon-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'summon-change-start-summon-in', 'summon-change-start-any' );
      }
    };

    // Mudança para saída de uma convocação
    summonChangeStart.summonOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-start-summon-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-start-summon-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'summon-change-start-summon-out', 'summon-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'summonChangeEnd'
  summonChangeEnd: {
    // Identificadores
    let { summonChangeEnd } = events;

    // Qualquer mudança de convocação
    summonChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-end-any', target, handler, config )
    };

    // Mudança para entrada de uma convocação
    summonChangeEnd.summonIn = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-end-summon-in', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-end-summon-in', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'summon-change-end-summon-in', 'summon-change-end-any' );
      }
    };

    // Mudança para saída de uma convocação
    summonChangeEnd.summonOut = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'summon-change-end-summon-out', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'summon-change-end-summon-out', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'summon-change-end-summon-out', 'summon-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'deckChangeStart'
  deckChangeStart: {
    // Identificadores
    let { deckChangeStart } = events;

    // Qualquer mudança de baralho
    deckChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-start-any', target, handler, config )
    };

    // Adição de cartas ao baralho
    deckChangeStart.add = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-start-add', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-start-add', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'deck-change-start-add', 'deck-change-start-any' );
      }
    };

    // Remoção de cartas do baralho
    deckChangeStart.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-start-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-start-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'deck-change-start-remove', 'deck-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'deckChangeEnd'
  deckChangeEnd: {
    // Identificadores
    let { deckChangeEnd } = events;

    // Qualquer mudança de baralho
    deckChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-end-any', target, handler, config )
    };

    // Adição de cartas ao baralho
    deckChangeEnd.add = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-end-add', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-end-add', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'deck-change-end-add', 'deck-change-end-any' );
      }
    };

    // Remoção de cartas do baralho
    deckChangeEnd.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'deck-change-end-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'deck-change-end-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'deck-change-end-remove', 'deck-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'fieldSlotCardChangeStart'
  fieldSlotCardChangeStart: {
    // Identificadores
    let { fieldSlotCardChangeStart } = events;

    // Qualquer mudança de ocupante
    fieldSlotCardChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-start-any', target, handler, config )
    };

    // Entrada de ocupante
    fieldSlotCardChangeStart.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-start-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-start-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'field-slot-card-change-start-enter', 'field-slot-card-change-start-any' );
      }
    };

    // Saída de ocupante
    fieldSlotCardChangeStart.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-start-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-start-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'field-slot-card-change-start-leave', 'field-slot-card-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'fieldSlotCardChangeEnd'
  fieldSlotCardChangeEnd: {
    // Identificadores
    let { fieldSlotCardChangeEnd } = events;

    // Qualquer mudança de ocupante
    fieldSlotCardChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-end-any', target, handler, config )
    };

    // Entrada de ocupante
    fieldSlotCardChangeEnd.enter = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-end-enter', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-end-enter', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'field-slot-card-change-end-enter', 'field-slot-card-change-end-any' );
      }
    };

    // Saída de ocupante
    fieldSlotCardChangeEnd.leave = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'field-slot-card-change-end-leave', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'field-slot-card-change-end-leave', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'field-slot-card-change-end-leave', 'field-slot-card-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'flowChangeStart'
  flowChangeStart: {
    // Identificadores
    let { flowChangeStart } = events;

    // Qualquer mudança de fluxo
    flowChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-start-any', target, handler, config )
    };

    // Para progressão de estágios
    flowChangeStart.progress = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-start-progress', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-start-progress', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-start-progress', 'flow-change-start-any' );
      }
    };

    // Entrada ou saída de estágios
    flowChangeStart.transit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-start-transit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-start-transit', target, handler, config )
    };

    // Para início de estágios
    flowChangeStart.begin = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-start-begin', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-start-begin', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-start-begin', 'flow-change-start-transit', 'flow-change-start-any' );
      }
    };

    // Para conclusão de estágios
    flowChangeStart.finish = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-start-finish', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-start-finish', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-start-finish', 'flow-change-start-transit', 'flow-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'flowChangeEnd'
  flowChangeEnd: {
    // Identificadores
    let { flowChangeEnd } = events;

    // Qualquer mudança de fluxo
    flowChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-end-any', target, handler, config )
    };

    // Para progressão de estágios
    flowChangeEnd.progress = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-end-progress', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-end-progress', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-end-progress', 'flow-change-end-any' );
      }
    };

    // Entrada ou saída de estágios
    flowChangeEnd.transit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-end-transit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-end-transit', target, handler, config )
    };

    // Para início de estágios
    flowChangeEnd.begin = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-end-begin', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-end-begin', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-end-begin', 'flow-change-end-transit', 'flow-change-end-any' );
      }
    };

    // Para conclusão de estágios
    flowChangeEnd.finish = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'flow-change-end-finish', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'flow-change-end-finish', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'flow-change-end-finish', 'flow-change-end-transit', 'flow-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'moveChangeStart'
  moveChangeStart: {
    // Identificadores
    let { moveChangeStart } = events;

    // Qualquer mudança de jogada
    moveChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-start-any', target, handler, config )
    };

    // Para a adição de jogadas
    moveChangeStart.add = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-start-add', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-start-add', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-start-add', 'move-change-start-any' );
      }
    };

    // Para o desenvolvimento de jogadas
    moveChangeStart.develop = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-start-develop', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-start-develop', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-start-develop', 'move-change-start-any' );
      }
    };

    // Para a remoção de jogadas
    moveChangeStart.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-start-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-start-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-start-remove', 'move-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'moveChangeEnd'
  moveChangeEnd: {
    // Identificadores
    let { moveChangeEnd } = events;

    // Qualquer mudança de jogada
    moveChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-end-any', target, handler, config )
    };

    // Para a adição de jogadas
    moveChangeEnd.add = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-end-add', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-end-add', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-end-add', 'move-change-end-any' );
      }
    };

    // Para o desenvolvimento de jogadas
    moveChangeEnd.develop = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-end-develop', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-end-develop', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-end-develop', 'move-change-end-any' );
      }
    };

    // Para a remoção de jogadas
    moveChangeEnd.remove = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'move-change-end-remove', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'move-change-end-remove', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'move-change-end-remove', 'move-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'actionChangeStart'
  actionChangeStart: {
    // Identificadores
    let { actionChangeStart } = events;

    // Qualquer mudança de ação
    actionChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-any', target, handler, config )
    };

    // Resolução da ação
    actionChangeStart.resolution = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-resolution', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-resolution', target, handler, config )
    };

    // Acionamento da ação
    actionChangeStart.commit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-commit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-commit', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-start-commit', 'action-change-start-resolution', 'action-change-start-any' );
      }
    };

    // Cancelamento da ação
    actionChangeStart.cancel = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-cancel', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-cancel', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-start-cancel', 'action-change-start-resolution', 'action-change-start-any' );
      }
    };

    // Execução da ação pós-acionamento
    actionChangeStart.postExecution = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-post-execution', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-post-execution', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-start-post-execution', 'action-change-start-any' );
      }
    };

    // Término da ação
    actionChangeStart.complete = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-complete', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-complete', target, handler, config )
    };

    // Conclusão da ação
    actionChangeStart.finish = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-finish', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-finish', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-start-finish', 'action-change-start-complete', 'action-change-start-any' );
      }
    };

    // Interrompimento da ação
    actionChangeStart.interrupt = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-interrupt', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-interrupt', target, handler, config )
    };

    // Abortamento da ação
    actionChangeStart.abort = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-abort', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-abort', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'action-change-start-abort', 'action-change-start-interrupt', 'action-change-start-complete', 'action-change-start-any'
        );
      }
    };

    // Impedimento da ação
    actionChangeStart.prevent = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-start-prevent', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-start-prevent', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'action-change-start-prevent', 'action-change-start-interrupt', 'action-change-start-complete', 'action-change-start-any'
        );
      }
    };
  }

  // Atribuição de propriedades de 'actionChangeEnd'
  actionChangeEnd: {
    // Identificadores
    let { actionChangeEnd } = events;

    // Qualquer mudança de ação
    actionChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-any', target, handler, config )
    };

    // Resolução da ação
    actionChangeEnd.resolution = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-resolution', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-resolution', target, handler, config )
    };

    // Acionamento da ação
    actionChangeEnd.commit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-commit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-commit', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-end-commit', 'action-change-end-resolution', 'action-change-end-any' );
      }
    };

    // Cancelamento da ação
    actionChangeEnd.cancel = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-cancel', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-cancel', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-end-cancel', 'action-change-end-resolution', 'action-change-end-any' );
      }
    };

    // Execução da ação pós-acionamento
    actionChangeEnd.postExecution = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-post-execution', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-post-execution', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-end-post-execution', 'action-change-end-any' );
      }
    };

    // Término da ação
    actionChangeEnd.complete = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-complete', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-complete', target, handler, config )
    };

    // Conclusão da ação
    actionChangeEnd.finish = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-finish', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-finish', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'action-change-end-finish', 'action-change-end-complete', 'action-change-end-any' );
      }
    };

    // Interrompimento da ação
    actionChangeEnd.interrupt = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-interrupt', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-interrupt', target, handler, config )
    };

    // Abortamento da ação
    actionChangeEnd.abort = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-abort', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-abort', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'action-change-end-abort', 'action-change-end-interrupt', 'action-change-end-complete', 'action-change-end-any'
        );
      }
    };

    // Impedimento da ação
    actionChangeEnd.prevent = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'action-change-end-prevent', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'action-change-end-prevent', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents(
          target, eventData, 'action-change-end-prevent', 'action-change-end-interrupt', 'action-change-end-complete', 'action-change-end-any'
        );
      }
    };
  }

  // Atribuição de propriedades de 'fatePointsChangeStart'
  fatePointsChangeStart: {
    // Identificadores
    let { fatePointsChangeStart } = events;

    // Qualquer mudança de pontos de destino
    fatePointsChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-start-any', target, handler, config )
    };

    // Aumento de pontos de destino
    fatePointsChangeStart.increase = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-start-increase', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-start-increase', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'fate-points-change-start-increase', 'fate-points-change-start-any' );
      }
    };

    // Redução de pontos de destino
    fatePointsChangeStart.decrease = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-start-decrease', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-start-decrease', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'fate-points-change-start-decrease', 'fate-points-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'fatePointsChangeEnd'
  fatePointsChangeEnd: {
    // Identificadores
    let { fatePointsChangeEnd } = events;

    // Qualquer mudança de pontos de destino
    fatePointsChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-end-any', target, handler, config )
    };

    // Aumento de pontos de destino
    fatePointsChangeEnd.increase = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-end-increase', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-end-increase', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'fate-points-change-end-increase', 'fate-points-change-end-any' );
      }
    };

    // Redução de pontos de destino
    fatePointsChangeEnd.decrease = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'fate-points-change-end-decrease', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'fate-points-change-end-decrease', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'fate-points-change-end-decrease', 'fate-points-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'damageChangeStart'
  damageChangeStart: {
    // Identificadores
    let { damageChangeStart } = events;

    // Qualquer mudança de dano
    damageChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-start-any', target, handler, config )
    };

    // Infligimento de dano
    damageChangeStart.inflict = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-start-inflict', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-start-inflict', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'damage-change-start-inflict', 'damage-change-start-any' );
      }
    };

    // Sofrimento de dano
    damageChangeStart.suffer = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-start-suffer', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-start-suffer', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'damage-change-start-suffer', 'damage-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'damageChangeEnd'
  damageChangeEnd: {
    // Identificadores
    let { damageChangeEnd } = events;

    // Qualquer mudança de dano
    damageChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-end-any', target, handler, config )
    };

    // Infligimento de dano
    damageChangeEnd.inflict = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-end-inflict', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-end-inflict', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'damage-change-end-inflict', 'damage-change-end-any' );
      }
    };

    // Sofrimento de dano
    damageChangeEnd.suffer = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'damage-change-end-suffer', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'damage-change-end-suffer', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'damage-change-end-suffer', 'damage-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'conditionChangeStart'
  conditionChangeStart: {
    // Identificadores
    let { conditionChangeStart } = events;

    // Qualquer mudança de condição
    conditionChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-any', target, handler, config )
    };

    // Aplicação ou remoção da condição
    conditionChangeStart.transit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-transit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-transit', target, handler, config )
    };

    // Aplicação de condição
    conditionChangeStart.apply = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-apply', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-apply', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-start-apply', 'condition-change-start-transit', 'condition-change-start-any' );
      }
    };

    // Remoção de condição
    conditionChangeStart.drop = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-drop', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-drop', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-start-drop', 'condition-change-start-transit', 'condition-change-start-any' );
      }
    };

    // Alteração em marcadores da condição
    conditionChangeStart.markers = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-markers', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-markers', target, handler, config )
    };

    // Incremento de marcadores da condição
    conditionChangeStart.increase = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-increase', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-increase', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-start-increase', 'condition-change-start-markers', 'condition-change-start-any' );
      }
    };

    // Decremento de marcadores da condição
    conditionChangeStart.decrease = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-start-decrease', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-start-decrease', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-start-decrease', 'condition-change-start-markers', 'condition-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'conditionChangeEnd'
  conditionChangeEnd: {
    // Identificadores
    let { conditionChangeEnd } = events;

    // Qualquer mudança de condição
    conditionChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-any', target, handler, config )
    };

    // Aplicação ou remoção da condição
    conditionChangeEnd.transit = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-transit', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-transit', target, handler, config )
    };

    // Aplicação de condição
    conditionChangeEnd.apply = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-apply', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-apply', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-end-apply', 'condition-change-end-transit', 'condition-change-end-any' );
      }
    };

    // Remoção de condição
    conditionChangeEnd.drop = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-drop', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-drop', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-end-drop', 'condition-change-end-transit', 'condition-change-end-any' );
      }
    };

    // Alteração em marcadores da condição
    conditionChangeEnd.markers = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-markers', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-markers', target, handler, config )
    };

    // Incremento de marcadores da condição
    conditionChangeEnd.increase = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-increase', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-increase', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-end-increase', 'condition-change-end-markers', 'condition-change-end-any' );
      }
    };

    // Decremento de marcadores da condição
    conditionChangeEnd.decrease = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'condition-change-end-decrease', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'condition-change-end-decrease', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'condition-change-end-decrease', 'condition-change-end-markers', 'condition-change-end-any' );
      }
    };
  }

  // Atribuição de propriedades de 'logChangeStart'
  logChangeStart: {
    // Identificadores
    let { logChangeStart } = events;

    // Qualquer mudança de registro
    logChangeStart.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-start-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-start-any', target, handler, config )
    };

    // Inserção de um novo registro
    logChangeStart.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-start-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-start-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'log-change-start-insert', 'log-change-start-any' );
      }
    };

    // Remoção de um novo registro
    logChangeStart.delete = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-start-delete', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-start-delete', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'log-change-start-delete', 'log-change-start-any' );
      }
    };
  }

  // Atribuição de propriedades de 'logChangeEnd'
  logChangeEnd: {
    // Identificadores
    let { logChangeEnd } = events;

    // Qualquer mudança de registro
    logChangeEnd.any = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-end-any', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-end-any', target, handler, config )
    };

    // Inserção de um novo registro
    logChangeEnd.insert = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-end-insert', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-end-insert', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'log-change-end-insert', 'log-change-end-any' );
      }
    };

    // Remoção de um novo registro
    logChangeEnd.delete = {
      // Adiciona evento
      add: ( target, handler, config ) => events.addEvent( 'log-change-end-delete', target, handler, config ),
      // Remove evento
      remove: ( target, handler, config ) => events.removeEvent( 'log-change-end-delete', target, handler, config ),
      // Emite evento
      emit: function ( target, eventData = {} ) {
        return events.emitEvents( target, eventData, 'log-change-end-delete', 'log-change-end-any' );
      }
    };
  }
}
