// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'pt' ) return;

  // Identificadores
  const cards = m.languages.cards = {};

  // Propriedades iniciais
  defineProperties: {
    // Retorna dados de localização de carta passada
    cards.getData = function ( card ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

      // Identificadores
      var cardData = {},
          getCardData = card.content.stats?.command ? getCommanderData :
                        card instanceof m.Humanoid ? getHumanoidData :
                        card instanceof m.Being ? getNonHumanoidData :
                        card instanceof m.Item ? getItemData :
                        card instanceof m.Spell ? getSpellData : null;

      // Popula dados da carta
      getCardData();

      // Retorna dados da carta
      return cardData;

      // Retorna dados de comandantes
      function getCommanderData() {
        // Identificadores
        var cardName = card.name.slice( card.name.search( '-' ) + 1 );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getRace( card.content.typeset.race );

        // Atribui à carta dados diversos em função de seu nome, sem o prefixo 'commander'
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'dummy-commander':
              return {
                title: 'Comandante de Testes',
                epigraph:
                  'Comandante de testes, visível apenas na versão de desenvolvimento deste jogo.'
              };
            case 'gaspar':
              return {
                title: 'Gaspar',
                epigraph:
                  'A serviço de uma civilização humana, Gaspar é um general de ampla experiência. ' +
                  'Em conjunto com Baltazar – um campeão de um clã anão – e Melquior – um sábio canalizador elfo –, ele foi escolhido para comandar um exército fruto de uma aliança militar entre um amalgamado de nações humanas, anãs e élficas.',
                effects:
                  'A inativação deste comandante não te derrota enquanto tiveres ao menos 1 tropa de nível 4.',
                creaturesConstraint:
                  'Obrigatórios ' + ( m.languages.keywords.getRoleType( 'melee', { person: true } ) + ' ' + m.languages.keywords.getRace( 'dwarf' ) + ' de ' +
                  m.languages.keywords.getExperienceLevel( 4 ) + ' e ' + m.languages.keywords.getRoleType( 'magical', { person: true } ) + ' ' +
                  m.languages.keywords.getRace( 'elf' ) + ' de ' + m.languages.keywords.getExperienceLevel( 4 ) ).toLowerCase() + '.'
              };
            case 'ixohch':
              return {
                title: 'Ixohch',
                epigraph:
                  'Ao [dormir/meditar], Ixohch, através do que reputa ser visões de Mllk – o deus local da purificação –, vê um futuro em que todos os orques, acuados pelo imperialismo de povos citadinos, uniram-se para reivindicar suas terras e vidas. ' +
                  'Anos professando tais visões a sua tribo o transitaram de guerreiro a profeta.\n\n' +
                  'Ele, então, conseguiu alistar tribos vizinhas para uma marcha às terras civilizadas, sob um exército alcunhado de A Grande Horda.',
                effects:
                  'Para cada humanoide do oponente que removeres, ganhas 1 ponto de destino: intransitivo, se esse humanoide for um orque, goblin ou ogro; transitivo, do contrário.\n\n' +
                  'Paradas de combate ofensivas tuas têm um modificador de ataque igual à diferença entre os pontos de destino atuais teus e do oponente.',
                creaturesConstraint:
                  'Sem criaturas de ' + m.languages.keywords.getExperienceLevel( 4 ).toLowerCase() + ' outras que ' +
                  m.languages.keywords.getRace( 'orc', { plural: true } ).toLowerCase() + '.'
              };
            case 'nathera':
              return {
                title: 'Nathera',
                epigraph:
                  'Ainda que vasta como uma grande civilização, na floresta onde vive Nathera, tudo a conhece. ' +
                  'Sem assentamento fixo – mas sempre acaudatada por animais e seguidores –, ela transita pelas comunidades élficas de lá há mais tempo do que qualquer um se lembra, com sua juventude magicamente conservada. Por onde passa ela cura enfermidades, e apregoa sobre o pertencimento de todos na floresta a uma única comunidade, que deve lutar contra seu desmatamento por povos expansionistas.',
                effects:
                  'Tão logo um ente do oponente ocupe uma zona de engajamento desocupada, esta comandante convoca 1 urso para lá.\n\n' +
                  'Tão logo o oponente afaste uma tropa tua, esta comandante convoca 1 enxame para uma zona de engajamento a tua escolha.\n\n' +
                  'Em pré-combates, esta comandante convoca 1 lobo para cada zona de engajamento disputada ou controlada por ti.'
              };
            case 'the-magnate':
              return {
                title: 'O Magnata',
                epigraph:
                  'O ditado é que o Magnata foi um dos primeiros a impulsionar relações comerciais entre a superfície e o subterrâneo, e disso fez sua fortuna. ' +
                  'Poucos conhecem seu nome, mas ainda menos conhecem outro referido como magnata.\n\n' +
                  'Afamado como o anão mais rico que já existiu, o Magnata financia guerras por capricho, e pode contratar levas de mercenários ao estalar dos dedos.',
                effects:
                  'Não podes ter um baralho coadjuvante.\n\n' +
                  'Ao início de arranjos: este comandante é colocado em tua reserva, irreversivelmente alheado; escolhes, então, um ente teu para ser o novo comandante de teu baralho principal durante a rodada atual.'
              };
            case 'dofro':
              return {
                title: 'Dofro',
                epigraph:
                  'Originalmente um pastor, Dofro foi impelido às armas por um canalizador que, passando-se por viajante, raptara-o ao visitar sua vila. ' +
                  'Segundo o canalizador, os anais cósmicos predestinam Dofro a destruir uma relíquia que obterá via um combate, ou, do contrário, uma força maligna a usará para conquistar o mundo.\n\n' +
                  'Desde então, Dofro conseguiu recrutar alguns combatentes para sua causa, e, alerta, treina para o dia do encontro com a relíquia.',
                effects:
                  'Ao fim de arranjos, deves trocar tua relíquia com uma carta do baralho principal do oponente: que não [seu comandante/uma relíquia]; de preço igual ou menor que o da relíquia. ' +
                  'Se o tamanho da relíquia trocada não for igual ao de nenhum ente de seu novo baralho, ele muda para o de seu comandante.\n\n' +
                  'Em batalhas, derrotas o oponente caso a relíquia trocada não esteja ativa.',
                creaturesConstraint:
                  'Sem ' + ( m.languages.keywords.getRoleType( 'magical', { person: true, plural: true } ) + ' ' +
                  m.languages.keywords.getRace( 'halfling', { plural: true } ) + '; obrigatório ' +
                  m.languages.keywords.getRoleType( 'magical', { person: true } ) + ' de ' + m.languages.keywords.getExperienceLevel( 4 ) ).toLowerCase() + '.'
              };
            case 'finnael':
              return {
                title: 'Finnael',
                epigraph:
                  'Finnael integra uma próspera civilização, cujos assentamentos ponteiam uma cordilheira com diversas jazidas de minerais preciosos. ' +
                  'Lá, anões e gnomos convivem de modo igualitário e simbiótico, em que os primeiros se encarregam de ofícios que demandam rigor, e os últimos, dos que demandam inventividade.\n\n' +
                  'No comando de um batalhão fronteiriço, cabe a Finnael repelir invasores que cobiçam as riquezas de assentamentos próximos.',
                effects:
                  'Em batalhas, és derrotado caso estejas sem [anões/gnomos] ativos.'
              };
            case 'wimis':
              return {
                title: 'Wimis',
                epigraph:
                  'Criado na malta de um déspota implacável, lá Wimis fingia completa subserviência para evitar sua execução ou torturas. ' +
                  'Porém, desejando uma posição de poder, ele pactuou com um demônio, que, então, prometeu-lhe o comando de um povo mais submisso caso ele, quando oportuno, exterminasse sua malta.\n\n' +
                  'Feito isso, Wimis foi levado até uma tribo orque escravista, cujo espírito guia predissera a chegada de um venerável bruxo goblin.',
                effects:
                  'Humanoides de nível 1 teus a serem afastados pelo oponente são, em vez disso, suspensos, conjuntamente com seus pertences.\n\n' +
                  'Ao serem suspensos desse modo, as estatísticas e os equipamentos embutidos desses humanoides voltam a seu estado inicial.',
                creaturesConstraint:
                  'Sem ' + ( m.languages.keywords.getRace( 'orc', { plural: true } ) + ' de ' + m.languages.keywords.getExperienceLevel( 1 ) +
                  '; sem criaturas de ' + m.languages.keywords.getExperienceLevel( 3 ) + ' outras que ' +
                  m.languages.keywords.getRace( 'orc', { plural: true } ) ).toLowerCase() + '.'
              };
            case 'gamomba':
              return {
                title: 'Gamomba',
                epigraph:
                  'Marginalizada por desejar se tornar uma guerreira a despeito de seu sexo, ainda jovem Gamomba abandonou sua aldeia ogra em busca de maior acolhimento.\n\n' +
                  'Seus vagueios acabaram quando encontrou moradia em uma vila metadília já habituada a abrigar goblines desertores de maltas próximas. Com o tempo, Gamomba se tornou a principal protetora e matriarca da vila, sendo lá alcunhada de Grande Mãe.',
                effects:
                  'Esta comandante não pode ser o alvo direto de [ações/efeitos] do oponente enquanto ocupar casas com alguma tropa tua.\n\n' +
                  'Enquanto esta comandante ocupar casas com: algum metadílio teu, atos [feitos por/contra] ela para os quais possas gastar pontos de destino os usam automática e gratuitamente; algum goblin teu, sua ação "engajar" não tem custo de fluxo, e pode mirar qualquer zona de engajamento ocupada.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }

      // Retorna dados de humanoides
      function getHumanoidData() {
        // Identificadores
        var cardRace = card.content.typeset.race,
            cardRole = card.content.typeset.role.name,
            cardExperience = card.content.typeset.experience.grade;

        // Define título da carta
        cardData.title = `${ m.languages.keywords.getRoleName( cardRole ) } ${ m.languages.keywords.getExperienceGrade( cardExperience ) }`;

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getRace( cardRace );

        // Define designação completa da carta
        cardData.fullDesignation = `${ cardData.subtitle } ${ cardData.title }`;

        // Define epígrafe da carta
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( `${ cardRace }-${ cardRole }` ) {
            case 'human-maceman':
              return {
                epigraph:
                  'A maior parte das sociedades humanas atuais não tem o costume de combater primariamente com claveiros. Lá, clavas são mais comumente empregadas como armas secundárias.'
              };
            case 'human-swordsman':
              return {
                epigraph:
                  'Espadeiros humanos costumam se dedicar, sobretudo, a duelos. Manuais de duelo, ancorados por uma tradição milenar, ensinam-nos a manejar espadas com excelência.'
              };
            case 'human-axeman':
              return {
                epigraph:
                  'Muitos nobres humanos são versados no manejo da acha. Eles a usam comumente em duelos, mas por vezes também a levam para guerras.'
              };
            case 'human-spearman':
              return {
                epigraph:
                  'Em tempos de guerra, humanos tendem a angariar levas de lanceiros. Muitos, sem experiência de luta, são munidos de lanças simples e baratas, ao passo que os com experiência geralmente usam alabardas.'
              };
            case 'human-slingman':
              return {
                epigraph:
                  'Ante a difusão de armas de disparo mais fáceis de usar, as sociedades humanas que treinam fundeiros vêm diminuindo. Alguns, mesmo assim, ainda encontram lugar nos campos de batalha.'
              };
            case 'human-bowman':
              return {
                epigraph:
                  'Desde tempos remotos, arqueiros encabeçam muitos exércitos humanos. Os que usam arcos longos desfrutam de um prestígio social notável.'
              };
            case 'human-crossbowman':
              return {
                epigraph:
                  'Por demandarem pouco tempo de treinamento, besteiros costumam constituir a maior parte dos disparadores de exércitos humanos contemporâneos.'
              };
            case 'human-theurge':
              return {
                epigraph:
                  'Humanos costumam se reunir em templos para escutarem sermões de seus teurgos, que os educam acerca de suas obrigações sociais e espirituais.'
              };
            case 'human-warlock':
              return {
                epigraph:
                  'A bruxaria é proibida na maioria das sociedades humanas, que tortura e executa quem descobre ter compactuado com seres abissais. Todavia, a cobiça por poder seduz muitos humanos a essa prática.'
              };
            case 'human-wizard':
              return {
                epigraph:
                  'Poucos canalizadores são suficientemente benquistos e inteligentes para ingressarem em academias arcanas humanas, por padrão dirigidas pelo clero. Os que ingressam aprendem lá a controlar seu mana.'
              };
            case 'human-sorcerer':
              return {
                epigraph:
                  'Quando sem lugar no clero ou em academias arcanas, humanos que queiram desenvolver sua canalização geralmente devem o fazer em sigilo. Porém, alguns são tolerados enquanto adivinhos ou mercenários.'
              };
            case 'orc-maceman':
              return {
                epigraph:
                  'Orques claveiros tradicionalmente combatem com gadas massivos, de peso superior a 20 quilos. Porém, os ainda sem muita força costumam usar porretes, e atuam mormente como batedores e sinaleiros.'
              };
            case 'orc-axeman':
              return {
                epigraph:
                  'Machados de guerra costumam ser as armas mais valorizadas por tribos orques. Muitos machadeiros dessas tribos também usam escudos, ainda que não sejam avessos a colecionarem cicatrizes de luta.'
              };
            case 'orc-spearman':
              return {
                epigraph:
                  'A lança é usada por orques sobretudo na caça, que é o principal meio de sustento de suas tribos. Lá, caçadores comumente não diferenciam bestas de humanoides que estejam pelas redondezas.'
              };
            case 'orc-bowman':
              return {
                epigraph:
                  'Embora orques careçam de destreza para serem arqueiros tão proficientes quanto humanos e elfos, sua musculatura lhes permite usar arcos mais potentes que os dessas raças.'
              };
            case 'orc-warlock':
              return {
                epigraph:
                  'Muitos orques tentam pactuar com entidades veneradas por sua tribo, mas apenas um punhado desperta o interesse de alguma. Os que conseguem infundem fascínio e respeito a seus congêneres.'
              };
            case 'orc-sorcerer':
              return {
                epigraph:
                  'Em uma tribo orque, as orcas tradicionalmente ocupam posições servis. As com mana, porém, tornam-se xamãs, sendo, por extensão, o alicerce moral e espiritual de sua tribo.'
              };
            case 'elf-swordsman':
              return {
                epigraph:
                  'Elfos que habitam florestas costumam patrulhar os arredores de sua comunidade com espadeiros. Quando necessário, esses espadeiros também são enviados para emboscadas, escaramuças, escoltas e afins.'
              };
            case 'elf-spearman':
              return {
                epigraph:
                  'Em geral, elfos apenas recrutam lanceiros para guerras. Não obstante, a destreza de sua raça lhes permite manejar lanças com facilidade, sendo comum aos mais experientes combater com uma em cada mão.'
              };
            case 'elf-slingman':
              return {
                epigraph:
                  'Muitos elfos que habitam florestas usam fundas desde pequenos, como passatempo. Lá, embora arqueiros costumem ser preferidos para combates, fundeiros com boa pontaria às vezes também são recrutados.'
              };
            case 'elf-bowman':
              return {
                epigraph:
                  'Elfos têm facilidade para disparar flechas em rápida sucessão, e sem que isso prejudique a pontaria. Contudo, por em geral terem pouca força física, o uso de arcos longos é raro entre eles.'
              };
            case 'elf-theurge':
              return {
                epigraph:
                  'Elfos teurgos zelam pela preservação de sua comunidade, e do ambiente em que ela está. Para esse fim, eles se encarregam de repelir intrusos, muitas vezes letalmente.'
              };
            case 'elf-warlock':
              return {
                epigraph:
                  'Elfos entregues à bruxaria geralmente buscam melhor estudar e experienciar o ambiente astral. Por estigma ou alvedrio, muitos se tornam eremitas, vivendo em sintonia cada vez maior com o Abismo.'
              };
            case 'elf-wizard':
              return {
                epigraph:
                  'Para a maioria dos elfos, o sucesso de um mago está no uso da magia em todas suas aplicações: da ostensividade à sutileza; do poder sobre a matéria ao poder sobre a mente.'
              };
            case 'dwarf-maceman':
              return {
                epigraph:
                  'No subterrâneo, clavas não têm lugar nas regras de guerra anãs. Por outro lado, elas são as armas mais comuns de delinquentes lá, assim como de anões autônomos que vivem na superfície.'
              };
            case 'dwarf-swordsman':
              return {
                epigraph:
                  'Outrora, legionários eram o núcleo de exércitos anões do subterrâneo. Muitos de seus descendentes ainda seguem esse cargo, mas agora eles atuam apenas como guardas da ordem civil e de Casas nobres.'
              };
            case 'dwarf-axeman':
              return {
                epigraph:
                  'Para anões do subterrâneo, machados são armas de gladiadores. Lá, os de pouco renome lutam entre si com machados de guerra, ao passo que veteranos, geralmente representando uma Casa nobre, usam achas.'
              };
            case 'dwarf-spearman':
              return {
                epigraph:
                  'Guerras atuais entre anões do subterrâneo seguem protocolos, que programam cada batalha. Em uma, formações de piqueiros se enfrentam, e a batalha é encerrada após certo número de fileiras quebradas.'
              };
            case 'dwarf-crossbowman':
              return {
                epigraph:
                  'Bestas raramente são vistas no subterrâneo, devido aos costumes anões e à estrutura de suas cidades, que não favorece combates distantes. Todavia, elas são amplamente usadas por anões na superfície.'
              };
            case 'halfling-swordsman':
              return {
                epigraph:
                  'Metadílios normalmente usam apenas espadas de madeira, para recreação. Entre os com espadas reais, em geral a finalidade destas é restrita para a defesa pessoal ou de estabelecimentos importantes.'
              };
            case 'halfling-spearman':
              return {
                epigraph:
                  'Por padrão, lanceiros metadílios previnem conflitos internos em suas vilas, e as guardam contra ameaças externas. Os mais experientes costumam ser treinados para desarmar raças maiores.'
              };
            case 'halfling-slingman':
              return {
                epigraph:
                  'Em vez de enfrentarem raças maiores diretamente, metadílios preferem desferir contra elas ataques sorrateiros. Para isso, costumam ocultar fundeiros, ou os posicionar em locais de difícil acesso.'
              };
            case 'halfling-bowman':
              return {
                epigraph:
                  'Embora seja incomum vilas metadílias treinarem arqueiros para combates, algumas incentivam a prática da arquearia organizando eventos periódicos de tiro com arco.'
              };
            case 'halfling-crossbowman':
              return {
                epigraph:
                  'Em uma vila metadília, é esperado que todos seus moradores contribuam para a defender de ataques. Lá, eles costumam aprender a usar bestas quando jovens, progredindo segundo sua vontade e aptidão.'
              };
            case 'halfling-theurge':
              return {
                epigraph:
                  'Teurgos de vilas metadílias costumam ser sábios e oráculos. Eles normalmente liberam seu mana em rogos mentais para predizer, suscitar e prevenir eventos, e aconselham seu povo ante presságios.'
              };
            case 'halfling-sorcerer':
              return {
                epigraph:
                  'A fragilidade de metadílios leva muitas de suas vilas a formarem esquadrões especiais de powthes, a participarem de combates arriscados demais para seus combatentes mundanos.'
              };
            case 'gnome-spearman':
              return {
                epigraph:
                  'Guerras são raras em sociedades gnomas. Lá, uma comuna não costuma ter mais que um punhado de combatentes mundanos – geralmente, lanceiros – para sua defesa, muitos dos que nunca estiveram em combate.'
              };
            case 'gnome-slingman':
              return {
                epigraph:
                  'Algumas comunas gnomas têm uma expressiva quantidade de fundeiros, embora isso ocorra, sobretudo, por motivações culturais, visto que muitas de suas atividades recreativas usam fundas.'
              };
            case 'gnome-crossbowman':
              return {
                epigraph:
                  'Em geral, gnomos combatem apenas com suas mentes, subjugando a vontade do adversário. Contra os mais obstinados ou contra grandes grupos inimigos, bestas também costumam ser usadas.'
              };
            case 'gnome-theurge':
              return {
                epigraph:
                  'Reza a lenda que a energia mágica inerente a todo gnomo é uma dádiva de Celéstia. Na expectativa de que ela nunca deixe de verter, gnomos teurgos frequentemente se relacionam com entes celestiais.'
              };
            case 'gnome-warlock':
              return {
                epigraph:
                  'Nenhum gnomo precisa de pactos para canalizar. Ainda assim, em algumas comunas suas existe bruxaria, mas lá ela consiste na submissão de entes abissais, a fim de que sejam usados para o bem comunal.'
              };
            case 'gnome-wizard':
              return {
                epigraph:
                  'Em sociedades gnomas, os magos costumam ser polímatas. Muitos atuam como educadores, ensinando novas gerações a controlar e a usar responsavelmente sua energia mágica.'
              };
            case 'goblin-maceman':
              return {
                epigraph:
                  'Goblines preferem lutas em que possam incapacitar rapidamente seus adversários. Um método que comumente empregam para esse fim é o uso de venenos paralisantes, untados a armas como manguais.'
              };
            case 'goblin-axeman':
              return {
                epigraph:
                  'Quando uma malta de goblines captura um humanoide vivo, ele costuma a entreter até sucumbir. Muitas das brincadeiras usam machados, via que os filhotes da malta aprendem a os manejar corretamente.'
              };
            case 'goblin-crossbowman':
              return {
                epigraph:
                  'Por terem uma visão ruim, goblines evitam usar armas de disparo. Apesar disso, por precaução muitas de suas maltas costumam manter por perto um punhado das bestas que encontram em seus saques.'
              };
            case 'goblin-sorcerer':
              return {
                epigraph:
                  'Canalizadores costumam comandar maltas goblines, eliminando sua concorrência. Quando um morre, é comum que sua malta se dissolva em menores, que lutam pelo comando até ele ser de um novo canalizador.'
              };
            case 'ogre-maceman':
              return {
                epigraph:
                  'Em aldeias ogras, claveiros são geralmente ogros de força mediana. Sobrerrepresentados em exércitos ogros, lá eles normalmente não são munidos de muito mais que porretes.'
              };
            case 'ogre-axeman':
              return {
                epigraph:
                  'Entre ogros, respeito e valor são normalmente conquistados via força bruta. Os ogros mais fortes de uma aldeia costumam assumir as posições de maior destaque, e, em guerras, são os mais bem-equipados.'
              };
            case 'ogre-slingman':
              return {
                epigraph:
                  'Ogros franzinos costumam ocupar as posições mais desprestigiadas de uma aldeia ogra. Porém, os que desejam participar de combates geralmente são aceitos enquanto fundeiros, se com boa pontaria.'
              };
            case 'ogre-wizard':
              return {
                epigraph:
                  'Por superstição, aldeias ogras costumam proibir quaisquer práticas de magia, à exceção das que possam ser usadas como um antídoto para esse veneno.'
              };
          }
        } )() );
      }

      // Retorna dados de entes que não sejam humanoides
      function getNonHumanoidData() {
        // Identificadores
        var cardRace = card.content.typeset.race,
            cardName = card.name;

        // Se ficha for de um morto-vivo, ajusta seu nome para que desconsidere seu prefixo
        if( card instanceof m.Undead ) cardName = cardName.slice( cardName.search( '-' ) + 1 );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'bear':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'Ursos-pardos têm um porte formidável. Quando em confrontos, lutam com suas garras e presas.'
              };
            case 'wolf':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'Um único lobo não costuma ser um problema para combatentes provisionados, mas, em números, podem se tornar uma ameaça considerável.'
              };
            case 'swarm':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'Um enxame de abelhas não pode ser disperso através de armas convencionais, e ataca seu alvo incessantemente.',
                effects:
                  'Sempre está levitada, e não pode ser um alvo direto.\n\n' +
                  'Seu ataque: dura 5 segmentos, em que são infligidos 2 pontos por segmento; enquanto em andamento, ocupa seu alvo; assim que infligir a seu alvo 5 pontos de dano, atordoa-o até sua conclusão; ao ser concluído, remove esta criatura.'
              };
            case 'golem':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Feitos de ferro, golens são animados através da transferência de uma alma humanoide para seu corpo. Essa alma, sem mais memórias e arbítrio, é incapaz de outro intento senão o de servir a seu dono.'
              };
            case 'golden-salamander':
              return {
                title: m.languages.keywords.getRace( 'goldenSalamander' ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'O fogo mágico, quando imbuído com intento, comumente se afigura como uma salamandra agigantada. Suas chamas são domadas de modo a se direcionarem aos inimigos de seu mestre.',
                effects:
                  'Em pós-combates, perde 12 pontos de vida.'
              };
            case 'indigo-salamander':
              return {
                title: m.languages.keywords.getRace( 'indigoSalamander' ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Um fogo imbuído com perversidade emite mais calor, mas se torna mais bravio. Alcunhado de fogo maldito, ele se sustenta com a agonia de quem consome, e é embevecido pela vontade de extinguir.',
                effects:
                  'Em pós-combates, perde 12 pontos de vida; em combates, restaura 1 ponto de vida para cada 2 pontos de dano que infligir.\n\n' +
                  'Torna-se caótica e permanentemente incontrolável tão logo seu convocante seja inativado, ou no pós-combate de um turno em cujo combate não acionou "atacar".\n\n' +
                  'Finda alvos que remover.'
              };
            case 'undead':
              return {
                title: `${ m.languages.keywords.getRace( card.sourceBeing.content.typeset.race ) } ${ m.languages.keywords.getRace( cardRace ) }`,
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Corpos de mortos são animados via o mesmo princípio que rege outras animações. Sua acessibilidade em batalhas torna seu uso para fins bélicos particularmente comum.'
              };
            case 'tulpa':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Tulpas são formas-pensamento com substância suficiente para se manifestarem fora de sua mente criadora. Quando muito fortalecidas energeticamente, às vezes servem de alimento mânico a canalizadores.',
                effects:
                  'Em pós-combates, restaura 2 pontos de energia.'
              };
            case 'spirit':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Espíritos são consciências de [desacordados/falecidos], que ambulam o plano etéreo. Ainda que eles não sejam visíveis à maioria dos entes físicos, interações mentais entre ambos são comuns.',
                effects:
                  'Seu ataque "arrebatar" apenas mira os cuja vida atual seja [igual a/menor que] metade da vida base.\n\n' +
                  'Na delonga de seu ataque "arrebatar", o ponto de destino usável pelo oponente reduz o dano desse ataque para 0, ao passo que o ponto de destino usável por ti apenas anula esse eventual efeito.'
              };
            case 'demon':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Demônios são nativos do Abismo, um plano de alta negatividade. Lá, eles escravizam espíritos, para mormente usufruírem de sua energia e os usarem em conflitos internos ou em operações no plano etéreo.',
                effects:
                  'Seu ataque "arrebatar" apenas mira os cuja vida atual seja [igual a/menor que] 3/4 da vida base.'
              };
            case 'angel':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Anjos habitam Celéstia, um plano tão sutil que não se sabe de espíritos para lá atraídos que tenham voltado. Detêm extremo poder e positividade, mas, sempre impassíveis e distantes, são inescrutáveis.',
                effects:
                  'Não pode ser um alvo direto.\n\n' +
                  'É findado tão logo uses uma magia negativa.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }

      // Retorna dados de equipamentos
      function getItemData() {
        // Identificadores
        var { equipage: cardEquipage, size: cardSize, grade: cardGrade } = card.content.typeset,
            cardName = card.name.replace( /((ordinary)|(masterpiece))(-|$)/, '' ).replace( /((small)|(medium)|(large))(-|$)/, '' ).replace( /-$/, '' );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getEquipage( cardEquipage );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'smooth-club':
              return {
                title: 'Porrete Liso',
                epigraph:
                  'Ainda que haja armas mais sofisticadas para cada situação de luta, em muitas um simples porrete basta.'
              };
            case 'spiked-club':
              return {
                title: 'Porrete Cravado',
                epigraph:
                  'Quando em dúvida se um simples porrete bastará para uma luta, povos primitivos costumam adicionar cravas a ele.'
              };
            case 'flanged-mace': {
              // Define variável para texto sobre efeito
              let maxPenetration = cardSize == 'large' ? 6 : cardSize == 'medium' ? 4 : 2;

              return {
                title: 'Maça Flangeada',
                epigraph:
                  'Maças flangeadas contrapõem, sobretudo, armaduras pesadas, pois suas flanges minimizam o risco de colisões contra essas armaduras serem defletidas.',
                effects:
                  `Em paradas de combate, a penetração do ataque "bater" desta arma se torna igual ao menor valor entre ${ maxPenetration } e a soma das resistências contra concussão de fontes do defensor que sejam: trajes de peso médio e pesado; "Pele Ferrosa"; para golens, a resistência natural.`
              };
            }
            case 'war-hammer': {
              // Define variável para texto sobre efeito
              let maxPenetration = cardSize == 'large' ? 6 : cardSize == 'medium' ? 4 : 2;

              return {
                title: 'Martelo de Guerra',
                epigraph:
                  'Martelos de guerra são projetados para destruir armaduras pesadas – e quem quer que as esteja trajando.',
                effects:
                  `Em paradas de combate, a penetração do ataque "bater" desta arma se torna igual ao menor valor entre ${ maxPenetration } e a soma das resistências contra concussão de fontes do defensor que sejam: trajes de peso médio e pesado; "Pele Ferrosa"; para golens, a resistência natural.\n\n` +
                  'Esta arma é afastada caso seu ataque "perfurar" inflija a seu alvo ao menos 1 ponto de ataque mas não o remova.'
              };
            }
            case 'gada':
              return {
                title: 'Gada',
                epigraph:
                  'Gadas, empregados desde os primórdios de muitas civilizações, continuam sendo armas eficazes, mas apenas quando usados por combatentes com a força adequada.',
                effects:
                  'Caso o ataque "bater" desta arma inflija um dano crítico, seu alvo é atordoado durante o resto da paridade em que se estiver.'
              };
            case 'flail':
              return {
                title: 'Mangual',
                epigraph:
                  'Manguais não desferem ataques precisos, mas, por os desferirem em rápida sucessão, eventualmente algum acerta seu alvo.',
                effects:
                  'O ataque "bater" desta arma não pode ser defendido com "bloquear".'
              };
            case 'smasher-maul':
              return {
                title: 'Malho Esmagador',
                epigraph:
                  'Destemido, o anão desferiu com seu malho encantado uma poderosa pancada contra o busto do inimigo. Após o impacto, de seu corpo rochoso não restou nada senão ruína e pó.',
                effects:
                  'Caso o ataque "bater" desta arma inflija um dano crítico, seu alvo: é afastado, caso esteja vinculado a "Pele Rochosa"; do contrário, é atordoado durante o resto da paridade em que se estiver.'
              };
            case 'gladius':
              return {
                title: 'Gládio',
                epigraph:
                  'Até o conhecimento metalúrgico ter possibilitado a produção de espadas maiores, gládios eram uma arma costumeira entre espadeiros. Na atualidade, eles mormente são usados para a defesa pessoal.'
              };
            case 'falchion':
              return {
                title: 'Alfange',
                epigraph:
                  'Em batalhas, alfanges costumam ser armas secundárias, usadas por os que preferem não estar em engajamentos próximos se puderem evitar.'
              };
            case 'bastard-sword':
              return {
                title: 'Espada Bastarda',
                epigraph:
                  'Chama-se de bastarda qualquer espada pequena o suficiente para o uso com uma mão, mas grande o suficiente para aumentar seu desempenho quando usada com duas mãos.'
              };
            case 'longsword':
              return {
                title: 'Espada Larga',
                epigraph:
                  'Devido a seu tamanho, o uso de espadas largas para a defesa cotidiana é impraticável. A fim de que sejam desembainhadas de forma rápida, combatentes tipicamente as carregam apoiadas a seus ombros.'
              };
            case 'radiance':
              return {
                title: 'Resplendor',
                epigraph:
                  'Quando desembainhada, da Espada que Resplandece irradia uma luminância como a de um brilhante sol. De toque suave e ardor penetrante, quem fita essa luz tem o coração aquecido, e a alma iluminada.',
                effects:
                  'Em pré-combates ou tão logo seja usada, dono e outros entes bióticos teus em sua casa ganham 3 marcadores de encorajado.\n\n' +
                  'Remove de dono e dos em sua casa a condição "possuído", e impede que eles a ganhem.\n\n' +
                  'Fichas na casa de dono: sendo pertences, são removidas; do contrário, tornam-se caótica e permanentemente incontroláveis.'
              };
            case 'battle-axe':
              return {
                title: 'Machado de Guerra',
                epigraph:
                  'Apesar de seu curto alcance, não é preciso muita destreza para que machados de guerra causem ferimentos fatais em quem atingem.'
              };
            case 'bardiche':
              return {
                title: 'Bardiche',
                epigraph:
                  'Poucos são os que enfrentam um bardicheiro desprovidos de proteção adequada. Ainda menos são os que vivem para arriscar uma segunda vez.'
              };
            case 'poleaxe':
              return {
                title: 'Acha',
                epigraph:
                  'Achas costumam ser as armas de haste mais estimadas pela nobreza. Abundam manuais que ensinam seu uso em duelos, e é comum que os campeões destes tenham desde pequenos praticado o manejo de uma.'
              };
            case 'desolator':
              return {
                title: 'Desoladora',
                epigraph:
                  'Aos montes fogem os inimigos que avistam a Foice que Desola; todos já ouviram histórias sobre como ela ultraja A Morte. Tão cruel, não se contenta só com a carne: também reclama a alma de quem degola.',
                effects:
                  'Em pré-combates ou tão logo seja usada, dono ganha 5 marcadores de temível.\n\n' +
                  'Finda alvos que remover.'
              };
            case 'spear':
              return {
                title: 'Lança',
                epigraph:
                  'Fáceis de produzir e fáceis de usar, poucos são os exércitos que não fazem de lanças sua arma branca principal.',
                effects:
                  'O ataque "arremessar" desta arma a afasta caso não remova seu alvo.'
              };
            case 'ranseur':
              return {
                title: 'Ranseur',
                epigraph:
                  'Ranseures são lanças com projeções laterais laminadas, capazes de melhor interceptar ataques e de desarmar o adversário.',
                effects:
                  'Ao fim de paradas de combate em que esta arma use a defesa "repelir" contra um ataque de uma arma, a arma atacante é afastada caso os pontos totais de ataque da parada de combate tenham sido menores que os pontos totais de defesa por 3 pontos ou mais.'
              };
            case 'halberd':
              return {
                title: 'Alabarda',
                epigraph:
                  'Alabardas costumam substituir lanças simples em exércitos bem-equipados, e são temidas tanto por sua capacidade de corte quanto pela de perfuração.'
              };
            case 'pike':
              return {
                title: 'Pique',
                epigraph:
                  'O maior benefício dos piques está na formação quadricular de seus combatentes, em que mais e mais fileiras de piqueiros desferem ataques conforme a distância entre a formação e o inimigo diminui.',
                effects:
                  'Após esta arma concluir um ataque "perfurar" dedicado ou livre provocado pelo alcance de uma arma, se houver na mesma casa outro pique teu em uso, seu dono ganha 1 ataque livre via a manobra "perfurar" do pique, contra um alvo do oponente a tua escolha. ' +
                  'Se mais de 1 ente puder desferir esse ataque livre, o atacante é determinado segundo, nesta ordem de preferência, estes critérios: quem puder atribuir ao ataque livre mais pontos; quem tiver ocupado primeiro a casa atual.'
              };
            case 'drill-spear':
              return {
                title: 'Lança-Furadeira',
                epigraph:
                  'Com o devido impulso, esta lança abre caminho até o corpo de inimigos protegidos mesmo por armaduras pesadas.',
                effects:
                  'O ataque "arremessar" desta arma a afasta caso não remova seu alvo.'
              };
            case 'buckler':
              return {
                title: 'Broquel',
                epigraph:
                  'Broquéis são pequenos escudos redondos, geralmente metálicos. Eles não conferem uma proteção elevada, mas tampouco diminuem a mobilidade do portador, e são acessíveis a maior parte dos combatentes.'
              };
            case 'rondache':
              return {
                title: 'Rodela',
                epigraph:
                  'Rodelas são escudos redondos de médio porte, geralmente metálicos. Elas balanceam proteção e mobilidade.'
              };
            case 'scutum':
              return {
                title: 'Escutum',
                epigraph:
                  'Escutuns são grandes escudos retangulares de madeira, geralmente capazes de cobrir seu portador por inteiro. Conferem ótima proteção, mas tendem a rachar ante fortes colisões.',
                effects:
                  'Em paradas de combate em que esta arma use a defesa "bloquear" contra um ataque cujo dano principal seja de corte, esta arma registra o menor valor entre os pontos de ataque e de defesa da respectiva parada de combate.\n\n' +
                  'Ao fim de uma parada de combate, esta arma é afastada caso a soma dos pontos registrados por ela ao longo de paradas de combate seja maior que 9.'
              };
            case 'aspis':
              return {
                title: 'Áspide',
                epigraph:
                  'Áspides são escudos redondos de madeira, revestidos em bronze. Oferecem boa proteção, mas à custa de uma mobilidade muito reduzida.'
              };
            case 'watchful-shield':
              return {
                title: 'Escudo Vigilante',
                epigraph:
                  'Com sua lança, ele desferiu contra mim várias estocadas em rápida sucessão, mas meu escudo, magicamente animado, propeliu-se em minha defesa mais rápido que o que meu instinto pôde acompanhar.'
              };
            case 'sling':
              return {
                title: 'Funda',
                epigraph:
                  'Normalmente, um disparador precisa de anos de prática com fundas até conseguir as usar com eficácia em combates. Porém, seus disparos em muitos casos são mais letais que os de arcos.'
              };
            case 'shortbow':
              return {
                title: 'Arco Curto',
                epigraph:
                  'Arcos são armas relativamente simples de se produzir, mas demandam força e muita prática para serem usados com eficácia em combates.'
              };
            case 'longbow':
              return {
                title: 'Arco Longo',
                epigraph:
                  'Arcos longos costumam ser quase tão altos quanto o portador. À custa de mais força, a corda de um pode ser estirada por uma distância maior que a de curtos, o que resulta em disparos mais potentes.'
              };
            case 'crossbow':
              return {
                title: 'Besta',
                epigraph:
                  'Bestas não podem ser disparadas tão rapidamente quanto arcos e fundas, mas seus disparos costumam ser mais potentes, e elas requerem pouca prática para serem usadas com eficácia.',
                effects:
                  'O ataque "disparar" desta arma: tem custo de fluxo, igual a 1; não desfere ataques livres.'
              };
            case 'gambeson':
              return {
                title: 'Jaquetão',
                epigraph:
                  'Jaquetões são vestimentas estofadas com muitas camadas de linho, lã e afins. Isso os torna particularmente bons em mitigar a força de impactos concussivos.'
              };
            case 'chain-mail':
              return {
                title: 'Cota de Malhas',
                epigraph:
                  'Cotas de malhas são formadas por argolas metálicas entrelaçadas. Eficazes mormente contra cortes, também são notáveis por sua proteção contra correntes elétricas se com elos em um circuito até o chão.'
              };
            case 'scale-armor':
              return {
                title: 'Armadura Escamada',
                epigraph:
                  'Armaduras escamadas consistem em pequenas placas metálicas sobrepostas, em um suporte de tecido ou couro. Oferecem tanto uma proteção quanto uma cobertura razoáveis.'
              };
            case 'brigandine':
              return {
                title: 'Brigantina',
                epigraph:
                  'Brigantinas são vestimentas têxteis com placas de metal internas. Usadas, sobretudo, para proteção contra cortes e perfurações, sua maior desvantagem é a cobertura, relativamente pequena.'
              };
            case 'plate-armor':
              return {
                title: 'Armadura de Placas',
                epigraph:
                  'Entre armaduras mundanas, as de placas oferecem a melhor proteção geral, com ótima cobertura. Porém, seu alto custo de produção restringe seu uso apenas aos combatentes mais valiosos.'
              };
            case 'sublime-robe':
              return {
                title: 'Manto Sublime',
                epigraph:
                  'Alguns mantos, geralmente brancos, não podem ser maculados por meios mundanos, e irradiam uma aura intensa de magnificência, capaz de fascinar quem os vê.',
                effects:
                  'Em pré-combates, dono ganha 5 marcadores de admirável.\n\n' +
                  'Este traje apenas surte efeito enquanto for o único usado por dono.'
              };
            case 'absorption-robe':
              return {
                title: 'Manto de Absorção',
                epigraph:
                  'Alguns mantos são encantados para que absorvam parte do mana liberado por seu portador, que pode facilmente o reutilizar. Esses mantos são particularmente úteis em canalizações muito demandantes.',
                effects:
                  'Dono imediatamente restaura 1/3 – arredondado para baixo – do mana que gastar em cada [canalização/sustentação] sua.\n\n' +
                  'Este traje apenas surte efeito enquanto for o único usado por dono.'
              };
            case 'martyr-shroud':
              return {
                title: 'Mortalha do Martírio',
                epigraph:
                  'Louvados são os que com a alma de mártir dedicam a vida nesta guerra, pois o sangue que tinge suas mortalhas é um testemunho aos anais da fé inquebrantável que tiveram na vitória de nossa causa.',
                effects:
                  'Tão logo dono seja afastado pelo oponente, ganhas 1 ponto de destino transitivo.\n\n' +
                  'Este traje apenas surte efeito enquanto for o único usado por dono.'
              };
            case 'maniphile-cuirass':
              return {
                title: 'Couraça Manífila',
                epigraph:
                  'Os combatentes que enfrentam powthes frequentemente se sentem mais protegidos com uma couraça de deflexão mânica do que com qualquer armadura de placas.',
                effects:
                  'Todo disparo com dano de mana que mire diretamente um ente na casa de dono outro que ele altera seu alvo para dono.\n\n' +
                  'A cobertura deste traje é considerada 8 para sua resistência contra mana.'
              };
            case 'bone-mail':
              return {
                title: 'Malha de Ossos',
                epigraph:
                  'Devido à facilidade que têm em absorver energia vital, os ossos de malhas costumam ser encantados de modo a capturarem parte dessa energia dispersa com a morte, para então a liberarem enquanto éter.',
                effects:
                  'Em pós-combates, os na casa de dono sofrem 1 ataque de 5 pontos e 1 ponto de dano de éter. A esse dano é, até que tenha 5 pontos, adicionado 1 ponto por tropa que for afastada enquanto este traje estiver em uso.\n\n' +
                  'O dano do ataque deste traje: volta a ser igual a 1, caso o traje seja inativado; quando maior que 1, em pré-combates é reduzido em 1 ponto.'
              };
            case 'robe-of-the-magi':
              return {
                title: 'Manto dos Arcanistas',
                epigraph:
                  'É dito que, em tempos imemoriais, um ancião transferiu parte de sua alma para este manto, almejando eternizar seus atos. Fato é que, com ele, mesmo a magia de noviços se confunde com a de arquimagos.',
                effects:
                  'Canalizações de dono ganham automática e gratuitamente os efeitos de "Acelerar", "Estender" e "Potencializar", magias cuja canalização, pois, torna-se desabilitada a ele.\n\n' +
                  'Este traje apenas surte efeito enquanto for o único usado por dono.'
              };
            case 'armor-of-the-heroes':
              return {
                title: 'Armadura dos Heróis',
                epigraph:
                  'Abundam lendas sobre heróis que, em defesa de seu povo, venceram lutas contra monstros primordiais. Atualmente, esta armadura parece ser a única possível reminiscência das trajadas por esses heróis.'
              };
            case 'horn': {
              // Define variável para texto sobre efeito
              let segmentsQuantity = cardSize == 'large' ? '3 segmentos' : cardSize == 'medium' ? '2 segmentos' : '1 segmento';

              return {
                title: 'Berrante',
                epigraph:
                  'Em batalhas, berrantes são usados para alertar tropas quanto ao cumprimento de certa ação ou evento.',
                effects:
                  `Em combates, dono ganha a ação parcial "preparar", que, quando concluída, abre jogadas de tropas tuas que estejam em até ${ segmentsQuantity } após o de conclusão dessa ação.`
              };
            }
            case 'pavise':
              return {
                title: 'Pavês',
                epigraph:
                  'O pavês é um escudo retangular, comumente de tamanho similar ao de seu carregador. Ele costuma ser firmado ao terreno em frente ao carregador, para lhe fornecer cobertura contra projéteis.',
                effects:
                  'Dono não pode ser o alvo de manobras "disparar" do oponente.\n\n' +
                  'Este apetrecho não surte efeito enquanto dono estiver levitado, ou em uma zona de engajamento, ou ocupado com: as ações "atacar", "engajar", "recuar"; as ações "canalizar" ou "sustentar", desde que tenham um alvo direto que não seja uma magia nem dono.'
              };
            case 'poison':
              return {
                title: 'Veneno',
                epigraph:
                  'Uma boa dose de qualquer um dos venenos mais potentes é suficiente para tombar até as maiores criaturas.',
                effects:
                  'Vinculante deve ter um ataque com dano de corte ou de perfuração.\n\n' +
                  'Ao fim de paradas de combate em que vinculante tenha infligido a um ente biótico um dano de corte ou de perfuração, esse ente ganha 1 marcador de enfermo por ponto de ataque que infligiu ao menos 1 ponto de um desses tipos de dano.'
              };
            case 'pitch':
              return {
                title: 'Piche',
                epigraph:
                  'Projéteis banhados em piche tornam-se incendiários, trocando precisão por poder de fogo.',
                effects:
                  'Vinculante deve ser capaz de atacar com "disparar", cujo dano principal deve ser de perfuração.\n\n' +
                  'Ataques "disparar" de vinculante ganham 2 pontos de dano de fogo, mas diminuem seu alcance em 1 e dobram a penalidade de ataque por alcance efetivo.'
              };
            case 'scepter-of-command':
              return {
                title: 'Cetro do Comando',
                epigraph:
                  'Supremo soberano, de o que estiver na terra a no céu, de o que estiver no físico a no astral, que este cetro assegure que nada fuja de teu poder.',
                effects:
                  'Impede que tuas fichas [sejam removidas/se tornem incontroláveis] devido à inativação do convocante.\n\n' +
                  'Ao ser desusado, fichas a que o efeito deste apetrecho foi aplicado imediatamente sofrem os efeitos que ele impediu, à exceção de as cujo convocante esteja novamente ativo.'
              };
            case 'rod-of-absorption':
              return {
                title: 'Bastão de Absorção',
                epigraph:
                  'Caminhei pela campina com o bastão, até o sentir vibrar levemente em minha mão. Ergui-o e a gema em sua ponta, ao começar a cintilar, sinalizou que estava absorvendo o mana que detectara.',
                effects:
                  'Dono ganha a manobra "raio de mana" e, em combates, a ação plena "rasgar" – que é como a magia homônima. Em ambos os casos, o mana usável é o vinculado a este apetrecho.\n\n' +
                  'Em remobilizações, caso este apetrecho não esteja com a quantidade máxima de mana: dono ganha a ação "absorver", quando aplicável; a ação "absorver" de dono vincula o mana removido a este apetrecho.'
              };
            case 'blazing-staff': {
              // Define variável para texto sobre efeito
              let attackPoints = cardSize == 'large' ? 8 : cardSize == 'medium' ? 6 : 4;

              return {
                title: 'Cajado Chamejante',
                epigraph:
                  'Este cajado armazena uma quantidade oscilante de energia, convertida em fogo ao ser disparada por sua boca. Ele atinge seu pico de energia quando fica quase quente demais ao toque.',
                effects:
                  `Dono ganha a manobra ofensiva "raio de fogo", que é como a manobra "raio de mana", com estas diferenças: seus pontos de ataque iniciais são ${ attackPoints }; pontos de ataque gastos não reduzem o eventual mana de dono; seu dano é de 4 pontos de fogo por ponto de ataque.`
              };
            }
            case 'shadow-wand': {
              // Define variável para texto sobre efeito
              let maxEnergy = cardSize == 'large' ? 8 : cardSize == 'medium' ? 6 : 4;

              return {
                title: 'Báculo do Umbral',
                epigraph:
                  'Este báculo, nomeado em alusão ao povo das sombras, é capaz de consumir espíritos. Ele também pode ejetar o éter dos espíritos consumidos, via o disparo de uma esfera explosiva.',
                effects:
                  `Em combates, dono ganha as ações plenas: "consumir", que é como a magia homônima, com a diferença de que mira alvos cuja energia atual – vinculada a este apetrecho enquanto pontos de mana – seja de até ${ maxEnergy } pontos e de que pontos de destino não são usáveis; "explosão de éter", que é um disparo de alcance R2 que mira uma casa e desfere contra seus ocupantes 1 ataque de até ${ maxEnergy } pontos e 3 pontos de dano de éter, em que 1 ponto do mana vinculado a este apetrecho é removido por ponto de ataque.`
              };
            }
            case 'shock-gauntlets': {
              // Define variável para texto sobre efeito
              let damageQuantity = cardSize == 'large' ? 3 : cardSize == 'medium' ? 2 : 1;

              return {
                title: 'Manoplas de Choque',
                epigraph:
                  'Algumas manoplas são magicamente ionizadas, de modo a lançarem uma descarga elétrica ante fortes colisões. O choque que geram, raramente letal, é, sobretudo, um meio de atordoar seu receptor.',
                effects:
                  `Ataques "desarmado" de dono ganham um dano colateral de ${ damageQuantity } ${ damageQuantity == 1 ? 'ponto' : 'pontos' } de choque.`
              };
            }
            case 'strength-bracelets':
              return {
                title: 'Braceletes da Força',
                epigraph:
                  'Estes braceletes não apenas aumentam a força de seu portador, mas também o deixam mais resistente a doenças e outras enfermidades físicas.',
                effects:
                  'Em pré-combates, dono ganha 2 marcadores de fortalecido.'
              };
            case 'resolve-diadem':
              return {
                title: 'Diadema do Aferro',
                epigraph:
                  'Este diadema apura a mente de quem o usa. Após cingido, seu portador é tomado por uma intensa presença de espírito, mesmo que antes estivesse cheio de relutância ou languidez.',
                effects:
                  'Em pré-combates, dono ganha 2 marcadores de alentado.'
              };
            case 'vigil-amulet':
              return {
                title: 'Amuleto da Vigília',
                epigraph:
                  'A morte de quem porta este amuleto não o deixa muito distante dos vivos. O espírito do morto ainda consegue exercer alguns de seus deveres terrenos, estando comprometido com eles.',
                effects:
                  'Caso dono seja afastado, ele é, em sequência, alheado, e uma carta de espírito é adicionada a teu baralho principal e suspensa na casa da banca previamente ocupada por dono.\n\n' +
                  'O espírito gerado equivale a um gerado por uma projeção de dono, sendo seus atributos definidos segundo os valores base dos de dono. Em adição, o espírito conserva a posse das magias de dono antes de seu afastamento, e, em vez de uma ficha ou comandante, é sempre considerado uma criatura de baralho.'
              };
            case 'orichalcum-pendant':
              return {
                title: 'Pingente de Oricalco',
                epigraph:
                  'O oricalco, metal hoje precioso, outrora foi empregado extensivamente. Suas propriedades antimágicas concediam a cidades um grau de estabilidade e segurança raramente alcançável nos dias atuais.',
                effects:
                  'Em combates, magias de [Enoth/Voth] com custo de mana e cujo alvo direto seja dono reduzem, no momento de uso, 1 ponto de potência por ponto de mana vinculável a este apetrecho. ' +
                  'O efeito de magias cuja potência seja desse modo reduzida a 0 é negado. Em adição, os pontos reduzidos são vinculados a este apetrecho, enquanto pontos de mana.\n\n' +
                  '[Em pré-combates/Ao ser inativado], este apetrecho remove todo mana vinculado a si.'
              };
            case 'ring-of-protection':
              return {
                title: 'Anel da Proteção',
                epigraph:
                  'O capitão nunca é visto sem aquele anel. E, afinal, por que deveria? Após sobreviver a terceira tentativa de assassinato, começamos a acreditar que ele é de fato uma espécie de talismã protetor.',
                effects:
                  'Dono ganha 1 ponto de destino.\n\n' +
                  'O ponto de destino ganho apenas pode ser usado em paradas de combate em que dono seja o defensor, ou em paradas de destino cujo desfecho possa: remover dono; reduzir a vida, o vigor ou a vontade de dono; tornar dono possuído.'
              };
            case 'flute-of-wolfmancy':
              return {
                title: 'Flauta Lupimântica',
                epigraph:
                  'Esta flauta emite um som que, inaudível a humanoides, atrai a atenção de lobos nos arredores. Quando tocada em certa melodia, ela induz lobos suficientemente próximos a obedecer quem a toca.',
                effects:
                  'Durante o uso deste apetrecho, dono se ocupa, mas pode o desusar ao acionar "abortar".\n\n' +
                  'Em pós-combates, se este apetrecho esteve em uso durante ao menos os últimos 10 segmentos do último combate, ele convoca 2 lobos para uma zona de engajamento a tua escolha.'
              };
            case 'ectoplasm-urn':
              return {
                title: 'Urna de Ectoplasma',
                epigraph:
                  'A acólita passara o dia com a urna, coletando resquícios de mana cidade adentro. À noite, retornou-a para o templo, e ao ser aberta seu ectoplasma saciou os espíritos que lá buscavam cuidados.',
                effects:
                  'Em combates, dono ganha a ação parcial "energizar", em que escolhes um [espírito/demônio] ativo teu para ganhar 1 ponto de energia por ponto de mana que removeres deste apetrecho.\n\n' +
                  'Em remobilizações, caso este apetrecho não esteja com a quantidade máxima de mana: dono ganha a ação "absorver", quando aplicável; a ação "absorver" de dono vincula o mana removido a este apetrecho.'
              };
            case 'flying-carpet':
              return {
                title: 'Tapete Voador',
                epigraph:
                  'Infatigáveis e tão rápidos quanto as melhores montarias, tapetes voadores são ótimos meios de locomoção. Contudo, não suportam muito peso, e devem ser constantemente manobrados por seu condutor.',
                effects:
                  'Dono se torna levitado.\n\n' +
                  'Em combates, dono ganha a ação "posicionar", enquanto sendo parcial e mirando uma casa de tua grade ou uma zona de engajamento ocupada.\n\n' +
                  'Este apetrecho não surte efeito enquanto a contagem de peso de dono estiver acima de 3.'
              };
            case 'boots-of-levitation': {
              // Define variável para texto sobre efeito
              let weightCount = cardSize == 'large' ? 0 : cardSize == 'medium' ? 1 : 2;

              return {
                title: 'Botas da Levitação',
                epigraph:
                  'Ao calçar as botas, meus pés escaparam do solo. Volitei até o centro da batalha, e de cima flechei muitos dos inimigos sem que eles pudessem fazer nada a respeito.',
                effects:
                  'Dono se torna levitado.\n\n' +
                  `Este apetrecho não surte efeito enquanto a contagem de peso de dono estiver acima de ${ weightCount }.`
              };
            }
            case 'awareness-potion':
              return {
                title: 'Poção da Clareza',
                epigraph:
                  'Quando bebo esta poção, fico mais atento, e imediatamente sinto saber a melhor maneira de proceder ante cada situação em que esteja.',
                effects:
                  'Alvo não deve estar adiantável.\n\n' +
                  'Alvo se torna adiantável.'
              };
            case 'nimbleness-potion':
              return {
                title: 'Poção da Ligeireza',
                epigraph:
                  'Os que tomam esta poção se tornam irrequietos, e dificilmente se fadigam. Porém, é preciso muita resistência para não ser debilitado pela exaustão corporal que marca o fim de seu efeito.',
                effects:
                  'Durante o combate seguinte ao uso deste apetrecho, alvo ganha 3 pontos de bônus em todas suas paradas de combate próximas.\n\n' +
                  'No pós-combate seguinte ao uso deste apetrecho, se o vigor de alvo for menor que 8 ele é afastado caso não gastes 1 ponto de destino incontestável.'
              };
            case 'alertness-potion':
              return {
                title: 'Poção do Alerta',
                epigraph:
                  'Usada em situações de risco, quem toma esta poção se torna capaz de vislumbrar, pouco antes que aconteça, um futuro onde sua vida chega ao fim, mas contra o qual geralmente há tempo para se precaver.',
                effects:
                  'No combate seguinte ao uso deste apetrecho, tão logo o oponente remova alvo pela primeira vez, o fluxo retrocede 5 segmentos, ou, na falta disso, até o início do combate.\n\n' +
                  'Na nova resolução dos segmentos retrocedidos, são: suprimidos efeitos deste apetrecho; desabilitados usos de "A Visão".'
              };
            case 'anticombustible-oil':
              return {
                title: 'Óleo Anticombustivo',
                epigraph:
                  'Este óleo inibe a combustão, rapidamente apagando quaisquer chamas ateadas sobre o local em que foi passado.',
                effects:
                  'Durante o combate seguinte ao uso deste apetrecho, alvo: ganha 4 pontos de resistência natural contra fogo; não pode ser incendiado, pelo que todo dano de fogo lhe destinado é infligido imediatamente.'
              };
            case 'frenzy-fragrance':
              return {
                title: 'Perfume Frenesiante',
                epigraph:
                  'O soldado que se aproximou exalava um aroma parecido com o de framboesa, que, de algum modo, ferveu meu sangue, e anuviou minha mente. Sob um estado bestial, logo tudo que pensava era em trucidá-lo.',
                effects:
                  'Durante o combate seguinte ao uso deste apetrecho, enquanto ocuparem a mesma casa de alvo e estiverem com vontade abaixo de 8, entes bióticos: ganham 3 marcadores de enfurecido; se do oponente, tornam-se incontroláveis, assim que possível sempre acionando "atacar" contra alvo. Por tropa afetada sua, o oponente pode gastar 1 ponto de destino contestável para a manter sob seu controle.'
              };
            case 'holy-water':
              return {
                title: 'Água Benta',
                epigraph:
                  'Soldado, não temas o mal extrafísico: foste ungido com uma água abençoada por um Alto Sacerdote. Enquanto seu efeito durar, nenhuma entidade astral terá poder sobre ti.',
                effects:
                  'Durante o combate seguinte ao uso deste apetrecho, alvo não pode ser diretamente mirado por entes astrais.'
              };
            case 'mask-of-concealment':
              return {
                title: 'Máscara da Ocultação',
                epigraph:
                  'Após certo tempo imóvel, quem estiver com esta máscara deixará de ser detectável por sentidos físicos: ao se olhar para o portador, será visto apenas o que estiver além de si.',
                effects:
                  'Em combates, enquanto dono estiver desocupado por mais de 5 segmentos consecutivos, ele não pode ser diretamente mirado por entes físicos.\n\n' +
                  'O efeito deste apetrecho é provocável tanto antes quanto depois de dono ter estado ocupado.'
              };
            case 'mirror-of-attunement':
              return {
                title: 'Espelho da Sintonia',
                epigraph:
                  'Após certo tempo o fitando, este espelho passa a refletir o ente vivo em que seu portador esteja pensando. À medida que o portador observa esse ente, um elo emocional se estabelece entre ambos.',
                effects:
                  'Em pré-combates, escolhes 1 tropa, a ser o alvo do efeito deste apetrecho durante o turno atual.\n\n' +
                  'Marcadores de "alentado", "desalentado", "encorajado" e "enfurecido" aplicados a dono ou a alvo são também aplicados ao outro. A inativação de um ou o desuso deste apetrecho não afeta as condições atuais do outro, e, em seu reuso, este apetrecho não surte mais efeito no turno atual.'
              };
            case 'bell-of-rest':
              return {
                title: 'Sino do Descanso',
                epigraph:
                  'O Sino do Descanso emite um tinido penetrante, que, aderindo-se à mente por horas, provoca aos que o escutam um irresistível cansaço psicológico, em que nada é mais desejado que um cochilo.',
                effects:
                  'A partir do bloco 1 de combates de turnos cuja numeração seja da mesma paridade que a tua, dono ganha a ação plena "concluir", que impede ações em andamento e desabilita suspensões do fluxo durante o resto do combate atual.'
              };
            case 'anvil-of-souls':
              return {
                title: 'Bigorna da Animação',
                epigraph:
                  'Usada comumente para produzir golens, esta bigorna captura a alma de quem morre sobre si, e a transfere para o metal lá moldado.',
                effects:
                  'Em remobilizações: dono ganha a ação "convocar", se ainda sem; dono ganha a opção "golem" em sua ação "convocar".\n\n' +
                  'Para que dono convoque um golem, deves findar uma tropa que não ele, que esteja em tua grade do campo e seja uma criatura; a ficha de golem, então, é posicionada na casa antes ocupada pela tropa findada.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title +
          ( cardSize && cardGrade != 'relic' ? ` ${ m.languages.keywords.getSize( cardSize ) }` : '' ) + ( cardGrade == 'masterpiece' ? ' (O)' : '' );
      }

      // Retorna dados de magias
      function getSpellData() {
        // Identificadores
        var cardName = card.name.slice( card.name.search( '-' ) + 1 );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getPath( card.content.typeset.path );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'shape':
              return {
                title: 'Esculpir',
                epigraph:
                  'O mana empregado por outras sendas não é mais que uma derivação do de Metoth, que está em seu estado bruto.',
                effects:
                  'Alvo é sempre dono, que escolhe 1 senda sua afora Metoth.\n\n' +
                  'Ao ser [usada/sustentada], restaura 1 ponto de mana da senda escolhida por dono.\n\n' +
                  'O mana gasto por esta magia apenas é subtraído do mana de Metoth. Em adição, esta magia é desusada tão logo o mana atual da senda escolhida por dono seja igual a seu mana base.'
              };
            case 'orchestrate':
              return {
                title: 'Orquestrar',
                epigraph:
                  'Embora sentir, expelir e absorver o mana sejam habilidades elementares para metothes, em geral eles precisam de muita prática até que consigam fazer uso do mana diretamente em seu estado livre.',
                effects:
                  'Alvo deve ser uma magia tua suspensa ou de dono, em desuso, com custo de mana e de Metoth. Em adição, a casa de dono deve ter mana.\n\n' +
                  'Inicia uma canalização de alvo em que dono pode atribuir a seu custo de mana o mana da casa em que estiver, exclusiva ou inclusivamente e sob um valor final de até seu mana base de Metoth.'
              };
            case 'weave':
              return {
                title: 'Entrelaçar',
                epigraph:
                  'Quando o mana de dois canalizadores é vinculado, ambos podem usar parcialmente o mana do outro, geralmente a fim de canalizar magias que demandem uma energia mágica superior a que teriam sozinhos.',
                effects:
                  'Ao definir [ganhos/custos] de mana de dono ou de alvo, se ao outro for aplicável até metade do processo, o máximo dessa parcela – arredondada para baixo – lhe é transferido.\n\n' +
                  'Para magias a que esse efeito é aplicado, o mana gasto ao todo é o usado por outras mecânicas, como potência ou custo de uso.\n\n' +
                  'Esta magia é desusada estando [dono/alvo] inativo ou projetado.'
              };
            case 'burn':
              return {
                title: 'Queimar',
                epigraph:
                  'Metothes experientes são capazes de usar o mana bruto no lugar do lapidado. Todavia, esse é um processo extremamente desgastante, a ponto de ser fatal se repetido em um curto intervalo de tempo.',
                effects:
                  'Alvo deve ser uma magia tua suspensa ou de dono, em desuso, com custo de mana e de uma senda de dono que não Metoth.\n\n' +
                  'Inicia uma canalização de alvo em que, tendo acabado o mana de sua senda, dono pode atribuir a seu custo de mana seu mana de Metoth, sob um valor final de até seu mana base de Metoth.\n\n' +
                  'Concluída a canalização de alvo, esta magia é findada.'
              };
            case 'attract':
              return {
                title: 'Atrair',
                epigraph:
                  'Na natureza, o mana livre se concentra em poucos locais, magicamente mais atrativos. Metothes comumente podem aumentar a atração mágica de um local, de modo a temporariamente reproduzir tal fenômeno.',
                effects:
                  'Em pós-combates, até que alvo atinja seu limite de mana vinculável transfere para ele o mana vinculado a casas adjacentes, sob esta precedência: fileiras C, B, A; grade do oponente, tua grade; colunas 4, 3, 2, 1.\n\n' +
                  'Em adição, ocupantes em alvo não podem ser o alvo direto de disparos mágicos de magias cuja potência não seja maior que o dobro da desta magia.'
              };
            case 'wither':
              return {
                title: 'Murchar',
                epigraph:
                  'Um local magicamente esterilizado inibe manifestações mágicas em seu perímetro. Canalizadores sabem quando estão em um desses locais pela dificuldade que têm em sentir lá seu próprio mana.',
                effects:
                  'Em alvo: ações “canalizar” e "sustentar” são desabilitadas e, se em andamento, impedidas; todo mana vinculado a si é removido.\n\n' +
                  'Em adição, esta magia desabilita o efeito de magias que atendam todas estas condições: não sejam disparos; não tenham uma potência maior que o dobro da desta magia; mirem alvo, seus ocupantes ou os vinculados desses ocupantes.'
              };
            case 'poke':
              return {
                title: 'Cutucar',
                epigraph:
                  'Embora normalmente isto seja apenas uma forma incômoda de chamar a atenção, uma cutucada mental costuma bastar para quebrar a concentração requerida para canalizações.',
                effects:
                  'Alvo deve estar ocupado com "canalizar" ou "sustentar".\n\n' +
                  'Impede a ação de alvo.\n\n' +
                  'Ao ser usada, o oponente pode gastar 1 ponto de destino incontestável para negar o efeito desta magia.'
              };
            case 'tear':
              return {
                title: 'Rasgar',
                epigraph:
                  'Tudo tecido por magia pode ser rasgado caso se aplique uma força tão potente quanto a que emenda os fios.',
                effects:
                  'Alvo deve ser uma magia [persistente/permanente] em uso.\n\n' +
                  'Desusa alvo. Se isso não remover alvo, podes então gastar 1 ponto de destino contestável para o afastar.\n\n' +
                  'O custo de mana desta magia é igual ao mínimo preciso para que sua potência não seja menor que a de alvo.'
              };
            case 'freeze':
              return {
                title: 'Congelar',
                epigraph:
                  'Mesmo quando um canalizador tem poder suficiente para confrontar grandes magias, às vezes ele apenas precisa que elas se silenciem durante um tempo.',
                effects:
                  'Alvo deve ser uma magia [persistente/permanente] cujo efeito esteja habilitado.\n\n' +
                  'Desabilita os efeitos de alvo, e impede que sua potência seja reduzida por seu custo de persistência, se existente.\n\n' +
                  'Esta magia persiste por apenas 1 turno, e seu custo de mana é igual ao mínimo preciso para que sua potência não seja menor que metade da de alvo.'
              };
            case 'suppress':
              return {
                title: 'Suprimir',
                epigraph:
                  'A magia do inimigo mais inócua é aquela que nunca é usada.',
                effects:
                  'Alvo deve estar ocupado com "canalizar", cuja magia mirada deve ter mana lhe atribuído.\n\n' +
                  'Impede a ação de alvo e afasta – ou finda, caso gastes 1 ponto de destino incontestável – a magia mirada por essa ação.\n\n' +
                  'O custo de mana desta magia é igual ao mínimo preciso para que sua potência não seja menor que à prevista para a magia de alvo.'
              };
            case 'consume':
              return {
                title: 'Consumir',
                epigraph:
                  'A energia espiritual é convertível em mana, à custa da absorção do portador. Porém, a consciência do absorvido não é extinta de imediato, e até isso ocorrer pode ter certo controle sobre o absorvedor.',
                effects:
                  'Alvo deve ser um espírito ou tulpa, e sua energia deve ser [menor que/igual ao] mana base de Metoth de dono.\n\n' +
                  'Dono é repreparado e restaura 1 ponto de mana de Metoth por ponto de energia de alvo, que é então findado.\n\n' +
                  'Sendo alvo um espírito, caso então não gastes 1 ponto de destino contestável dono se torna possuído pelo oponente.'
              };
            case 'scavenge':
              return {
                title: 'Reciclar',
                epigraph:
                  'Às vezes, magias encerradas deixam resíduos mânicos em seu ambiente. Esse mana residual é mais sutil e volátil que o mana livre comum, mas pode ser reaproveitado por canalizadores capazes de o sentir.',
                effects:
                  'Ao início de custos de fluxo desta magia, deves escolher 1 magia afastada, que é findada ao fim do custo de fluxo requerente.\n\n' +
                  'Ao ser [usada/sustentada], dono restaura 4 pontos de seu mana de Metoth.\n\n' +
                  'Esta magia é desusada tão logo o mana de Metoth atual de dono seja igual a seu mana base.'
              };
            case 'quicken':
              return {
                title: 'Acelerar',
                epigraph:
                  'Canalizadores que dominam o processo de canalização conseguem usar seu mana para acelerar a canalização de suas magias.',
                effects:
                  'Alvo deve ser uma magia tua canalizável por dono, com custo de mana, e com custo de fluxo para canalização.\n\n' +
                  'Inicia uma canalização de alvo em que seu custo de fluxo é reduzido em 1.\n\n' +
                  'O custo de mana desta magia é: igual à metade do para canalização de alvo; gasto ao mesmo tempo que o de alvo.'
              };
            case 'extend':
              return {
                title: 'Estender',
                epigraph:
                  'Canalizadores que dominam o processo de canalização conseguem usar seu mana para estender o alcance de suas magias.',
                effects:
                  'Alvo deve ser uma magia tua canalizável por dono, com custo de mana, e com alcance entre R1 e R4.\n\n' +
                  'Inicia uma canalização de alvo em que seu alcance aumenta em 1.\n\n' +
                  'O custo de mana desta magia é: igual à metade do para canalização de alvo; gasto ao mesmo tempo que o de alvo.'
              };
            case 'empower':
              return {
                title: 'Potencializar',
                epigraph:
                  'Canalizadores que dominam o processo de canalização conseguem canalizar magias potentes usando menos mana que o usual.',
                effects:
                  'Alvo deve ser uma magia tua canalizável por dono, e com custo de mana.\n\n' +
                  'Inicia uma canalização de alvo em que sua potência, após aferida, é dobrada, assim como seu custo de persistência, se existente.\n\n' +
                  'O custo de mana desta magia é: igual à metade do para canalização de alvo; gasto ao mesmo tempo que o de alvo.'
              };
            case 'reflect':
              return {
                title: 'Refletir',
                epigraph:
                  'Polos de magias são como faces de moedas: ambos podem ser vistos se alternada a perspectiva.',
                effects:
                  'Alvo deve estar ocupado com "canalizar", cuja magia mirada deve ser polar e de uma senda de dono.\n\n' +
                  'Dono inicia uma canalização da magia oposta à sendo canalizada por alvo, via seu mana de Metoth e sob um custo de mana igual à potência prevista para a magia de alvo.\n\n' +
                  'Concluída a canalização de dono, esta magia é findada.'
              };
            case 'strengthness':
              return {
                title: 'Fortalecimento',
                epigraph:
                  'Após a força daquele claveiro ter sido ampliada, a armadura do adversário não conseguiu absorver muito bem o impacto seguinte de seu gada, que, atingindo o abdômen, deixou-o nauseado.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de fortalecido, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo: se existente, perde 1 marcador de enfraquecido; do contrário, ganha 1 marcador de fortalecido.'
              };
            case 'regeneration':
              return {
                title: 'Regeneração',
                epigraph:
                  'Antes do combate, o chefe da tribo orque foi assistido por uma xamã, que proveu seu corpo de capacidades hiper-regenerativas. Assim, os ferimentos que sofreu apenas o fizeram lutar mais ferozmente.',
                effects:
                  'Em pós-combates, alvo restaura um percentual de sua vida base, igual a 10% por ponto de potência desta magia mais 5% por ponto de vigor de alvo.\n\n' +
                  'O percentual de vida a ser restaurada é ajustado para 90% caso ultrapasse esse valor.'
              };
            case 'body-cleansing':
              return {
                title: 'Purificação Corporal',
                epigraph:
                  'Curandeiros enothes costumam acompanhar exércitos em operações de guerra, resguardando a saúde de seus oficiais contra mazelas de origem mundana ou mágica.',
                effects:
                  'Remove de alvo as condições "enfraquecido", "desalentado", "enfermo" e "possuído", e impede que ele as ganhe.'
              };
            case 'growth':
              return {
                title: 'Crescimento',
                epigraph:
                  'Antigamente, era comum a gladiadores enfrentarem na arena ursos atrozes. Esses eram ursos com tamanho magicamente aumentado; sobre quatro patas, tinham uma altura equivalente a de 2 humanos adultos.',
                effects:
                  'Alvo deve ter tamanho, que deve ser menor que grande.\n\n' +
                  'Alvo e seus vinculados embutidos com tamanho o mudam para o acima do atual, sofrendo os ajustes pertinentes. ' +
                  'Ao ser usada ou desusada, suspende vinculados externos de alvo com tamanho.\n\n' +
                  'Esta magia pode persistir por até 3 turnos, e seu custo de mana mínimo aumenta em 1 para cada vinculado embutido de alvo com tamanho.'
              };
            case 'pain-twist':
              return {
                title: 'Dor Encaminhada',
                epigraph:
                  'Em grandes batalhas, às vezes enothes ladeiam escravos e prisioneiros de guerra, para os quais transferem os ferimentos de combatentes que retornam da vanguarda. Quando tudo dá certo.',
                effects:
                  'Dono escolhe 2 alvos distintos. ' +
                  'O primeiro alvo transfere o dano físico que até o início da canalização desta magia sofreu no combate atual ao segundo, que é afastado caso tal dano exceda sua vida, sendo a transferência interrompida nesse caso. ' +
                  'Caso dono não seja um alvo, ao ser usada o oponente pode gastar 1 ponto de destino contestável para que ele seja o segundo.\n\n' +
                  'O custo de mana desta magia é igual ao mínimo preciso para que sua potência não seja menor que 1/5 do dano a ser transferido.'
              };
            case 'weakness':
              return {
                title: 'Enfraquecimento',
                epigraph:
                  'Afligido por uma magia que debilitou seu organismo, o guerreiro não conseguiu resistir ao veneno da flecha em seu braço, que o asfixiou em pouco tempo.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de enfraquecido, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo: se existente, perde 1 marcador de fortalecido; do contrário, ganha 1 marcador de enfraquecido.'
              };
            case 'decay':
              return {
                title: 'Deterioração',
                epigraph:
                  'Gloriosamente equipado, nosso campeão vencia todas suas batalhas, até a em que na plateia estava um vil canalizador. Naquela noite, seu corpo inteiro necrosou, e nos deixou com um moribundo agonizado.',
                effects:
                  'Em pós-combates, alvo perde um percentual de sua vida base, igual a 55% mais 10% por ponto de potência desta magia e menos 5% por ponto de vigor de alvo.\n\n' +
                  'O percentual de vida a ser perdida é ajustado para 90% caso ultrapasse esse valor.'
              };
            case 'heartstop':
              return {
                title: 'Parada Cardíaca',
                epigraph:
                  'Após minha infiltração no acampamento, o capitão, pouco tempo depois que o avistei, foi acometido por uma parada cardíaca fatal. Seus subalternos acharam que foi uma morte de causas naturais.',
                effects:
                  'O vigor de alvo deve ser menor que 5.\n\n' +
                  'Afasta alvo.'
              };
            case 'shrinkage':
              return {
                title: 'Encolhimento',
                epigraph:
                  'Algumas cidades humanas usam em labutas orques, trazidos por expedições a terras selvagens. Lá, é comum que punições a um orque sejam aplicadas com ele encolhido, para atenuar o impacto de sua reação.',
                effects:
                  'Alvo deve ter tamanho, que deve ser maior que pequeno.\n\n' +
                  'Alvo e seus vinculados embutidos com tamanho o mudam para o abaixo do atual, sofrendo os ajustes pertinentes. ' +
                  'Ao ser usada ou desusada, suspende vinculados externos de alvo com tamanho.\n\n' +
                  'Esta magia pode persistir por até 3 turnos, e seu custo de mana mínimo aumenta em 1 para cada vinculado embutido de alvo com tamanho.'
              };
            case 'barkskin':
              return {
                title: 'Pele Cascosa',
                epigraph:
                  'Aos poucos, sua pele adquiriu uma cor marrom-escura, e se tornou rugosa e seca. Ao fim da transformação, ela lembrava a casca de uma árvore.',
                effects:
                  'Sendo menores, alvo muda as resistências naturais para: 3 contra concussão; 3 contra corte; 3 contra perfuração.\n\n' +
                  'Em adição, a condutividade natural de alvo se torna 0, e o valor total de danos de fogo que sofrer é dobrado antes de ser infligido.\n\n' +
                  'Esta magia disputa com "Pele Ferrosa" e "Pele Rochosa".'
              };
            case 'ironskin':
              return {
                title: 'Pele Ferrosa',
                epigraph:
                  'Aos poucos, sua pele adquiriu uma cor cinza-prata, e se tornou lisa e dura. Ao fim da transformação, seu corpo parecia ser feito de ferro.',
                effects:
                  'Sendo menores, alvo muda as resistências naturais para: 2 contra concussão; 8 contra corte; 5 contra perfuração.\n\n' +
                  'Em adição, a condutividade natural de alvo se torna 3, e as resistências contra fogo de seus trajes são reduzidas pela metade, arredondada para baixo.\n\n' +
                  'Esta magia disputa com "Pele Cascosa" e "Pele Rochosa".'
              };
            case 'stoneskin':
              return {
                title: 'Pele Rochosa',
                epigraph:
                  'Aos poucos, sua pele adquiriu uma cor cinza-crômio, e se tornou granulosa e rija. Ao fim da transformação, seu corpo mais parecia uma escultura de granito.',
                effects:
                  'Sendo menores, alvo muda as resistências naturais para: 4 contra concussão; 5 contra corte; 8 contra perfuração; 3 contra fogo.\n\n' +
                  'Em adição, alvo ganha 2 pontos de penalidade em manobras próximas, e é afastado caso um ataque lhe inflija um dano físico maior que metade de sua vida base.\n\n' +
                  'Esta magia disputa com "Pele Cascosa" e "Pele Ferrosa".'
              };
            case 'restoration':
              return {
                title: 'Restauração',
                epigraph:
                  'Quando com mana suficiente, um enothe é a panaceia para avarias.',
                effects:
                  'Alvo deve ser um equipamento mundano afastado.\n\n' +
                  'Suspende alvo.\n\n' +
                  'O custo de mana mínimo desta magia é igual a 1 mais o tamanho de alvo, enquanto convertido em: 0 para nulo; 1 para pequeno; 2 para médio; 3 para grande.'
              };
            case 'ruination':
              return {
                title: 'Arruinamento',
                epigraph:
                  'Enothes conseguem facilmente arruinar um objeto ao alterar sua estrutura molecular – deixando-o, por exemplo, mais frágil ou mole.',
                effects:
                  'Alvo deve ser um equipamento mundano em uso.\n\n' +
                  'Afasta alvo.\n\n' +
                  'O custo de mana mínimo desta magia é igual a 1 mais o tamanho de alvo, enquanto convertido em: 0 para nulo; 1 para pequeno; 2 para médio; 3 para grande.'
              };
            case 'withering-slash':
              return {
                title: 'Corte Fulminante',
                epigraph:
                  'Após o canalizador me devolver a espada, senti que emanava uma energia sinistra. Ao encostar seu gume em uma tora, a região tocada se corroeu até, em poucos segundos, formar um profundo sulco.',
                effects:
                  'Alvo deve ser capaz de atacar com "cortar".\n\n' +
                  'Tendo alvo infligido dano com "cortar": o receptor é afastado ou – se o dano já o removeria – findado; esta magia é desusada.\n\n' +
                  'O oponente pode gastar 1 ponto de destino incontestável para que, em vez de remover seu receptor, o dano do ataque "cortar" de alvo seja um crítico.'
              };
            case 'fierce-shot':
              return {
                title: 'Disparo Penetrante',
                epigraph:
                  'À medida que percorria o ar, a flecha ganhava velocidade. Ao atingir o soldado, atravessou seu corpo como se ele estivesse sem nenhuma armadura, e apenas perdeu força centenas de metros depois.',
                effects:
                  'Alvo deve ser capaz de atacar com "disparar".\n\n' +
                  'O próximo ataque "disparar" de alvo: adiciona a seus pontos de penetração a extensão de seu alcance efetivo; automaticamente tem um dano crítico se seu alcance efetivo for maior que R2.\n\n' +
                  'Esta magia é desusada após alvo ter atacado com "disparar".'
              };
            case 'sanctuary':
              return {
                title: 'Santuário',
                epigraph:
                  'Uma poderosa energia curativa circulava pelo forte em que estávamos. Quando os soldados retornavam, não importava quão ruim fosse a situação dos feridos, em pouco tempo todos eram plenamente curados.',
                effects:
                  'Entes bióticos em tua grade do campo: em pré-combates, restauram sua vida para o valor base; perdem as condições "enfraquecido", "desalentado", "enfermo" e "possuído", e são impedidos de as ganhar.\n\n' +
                  'Esta magia disputa com "Abiosfera", enquanto em uso pelo oponente.'
              };
            case 'abiosphere':
              return {
                title: 'Abiosfera',
                epigraph:
                  'Sem que os ocupantes soubessem, o acantonamento fora envolto em uma esfera mágica hostil à vida. Toda vitalidade lá foi gradualmente sugada, e quem não saiu a tempo adoeceu até ser medicado com morte.',
                effects:
                  'Em pré-combates, entes bióticos na grade do campo do oponente ganham 2 marcadores de enfermo por turno da batalha concluído desde que esta magia entrou em seu uso atual.\n\n' +
                  'Esta magia disputa com "Santuário", enquanto em uso pelo oponente.'
              };
            case 'hypergravity':
              return {
                title: 'Hipergravidade',
                epigraph:
                  'Senti uma forte pressão contra meu corpo, e cada novo passo passou a demandar mais esforço. Ao meu redor, alguns se mantinham de pé com dificuldade, e outros tombavam devido ao peso de suas armaduras.',
                effects:
                  'Para todos no campo: remove condição "levitado", e impede sua aquisição; ajusta agilidade para 3, quando acima desse valor; dobra a pontuação de peso de equipamentos. ' +
                  'Ante contagens de peso ajustadas para além do limite, remove equipamentos embutidos e suspende os externos, sob esta precedência: externo, embutido; pesado, médio; apetrecho, traje, arma.\n\n' +
                  'Esta magia disputa com "Hipogravidade".'
              };
            case 'hypogravity':
              return {
                title: 'Hipogravidade',
                epigraph:
                  'Ao correr, percebi quão fácil era saltar; ao saltar, percebi quão fácil era voar. Logo parte da batalha estava sendo travada acima do solo, sobre os menos afortunados em peso.',
                effects:
                  'No campo, tornam-se levitados todos os mortos-vivos e bestas, assim como os humanoides cuja contagem de peso atual for de até: 2, para os de tamanho pequeno; 1, para os de tamanho médio; 0, para os de tamanho grande.\n\n' +
                  'Esta magia disputa com "Hipergravidade".'
              };
            case 'magic-missile':
              return {
                title: 'Mísseis Mágicos',
                epigraph:
                  'O powthe se concentrou, e em pouco tempo materializou com seu mana projéteis ogivais e azulados. Movidos por sua vontade, eles sobrevoaram o campo de batalha até mergulharem de encontro a seus alvos.',
                effects:
                  'Dono escolhe até 4 alvos, não necessariamente distintos.\n\n' +
                  'Ao ser usada, dispara contra cada alvo escolhido por dono 1 ataque de 1 ponto e 3 pontos de dano de mana.\n\n' +
                  'O custo de mana desta magia é igual à quantidade de alvos escolhidos por dono.'
              };
            case 'mage-armor':
              return {
                title: 'Armadura Arcana',
                epigraph:
                  'Fui envolvido por uma aura azul e cintilante. Ela não me protegeria da arremetida de uma espada, mas sabia que dissiparia bem qualquer disparo mânico que a atingisse.',
                effects:
                  'Alvo ganha 4 pontos de resistência natural contra mana.'
              };
            case 'mana-disruption':
              return {
                title: 'Disrupção Mânica',
                epigraph:
                  'Quando usado para fins bélicos, o resíduo mânico de um ambiente comumente é ajuntado em uma esfera de energia, movida pela vontade de seu canalizador e a explodir após ele deixar de a estabilizar.',
                effects:
                  'Ao início de custos de fluxo desta magia, deves escolher 1 magia afastada, que é findada ao fim do custo de fluxo requerente.\n\n' +
                  'Ao ser usada, gera 1 ataque de 1 ponto e 4 pontos de dano de mana. Cada sustentação adiciona 1 ponto a esse ataque.\n\n' +
                  'Ao ser desusada, o ataque é disparado contra os ocupantes: da casa de dono, se esta magia foi impedida; de alvo, do contrário.'
              };
            case 'energy-wall':
              return {
                title: 'Muralha de Energia',
                epigraph:
                  'Enquanto circundava o acampamento, a powthe moldava com sua magia uma muralha de mana sólido. Passou-se uma tarde até que essa muralha cercasse o local, mas ela protegeu todos lá durante a noite.',
                effects:
                  'Dono deve estar em tua grade do campo. Esta magia afeta todas as casas na: fileira de dono, se ela for a A; grade de dono, do contrário.\n\n' +
                  'Entre casas afetadas e não afetadas, para entes físicos que não estejam levitados se desabilitam: disparos mágicos; as ações "atacar", "mover", "engajar", "interceptar" e "desengajar"; posicionamentos de ajuste.'
              };
            case 'voltaic-outburst':
              return {
                title: 'Erupção Voltaica',
                epigraph:
                  'Os lobos investiram contra o powthe, que os fitava com calma. Ao chegarem a poucos metros do canalizador, de súbito foram eletrocutados por vários raios que se projetaram de sua cota de malhas.',
                effects:
                  'Alvo deve compartilhar sua casa com ao menos 1 ocupante, e ambos devem ter condutividade.\n\n' +
                  'Ao ser usada, gera 1 ataque de 6 pontos e 1 ponto de dano de choque. Cada sustentação adiciona 1 ponto a esse dano.\n\n' +
                  'Ao ser desusada, se esta magia não foi impedida seu ataque mira alvo e todos em sua casa com a maior condutividade, desconsiderando alvo para determinação dessa condutividade.'
              };
            case 'charged-aura':
              return {
                title: 'Aura Eletrificante',
                epigraph:
                  'Os companheiros daquele guerreiro sabiam que não era uma boa ideia lutar ao seu lado. Os inimigos que ele enfrentou descobriram o motivo, que os chocou.',
                effects:
                  'Em paradas de combate próximo com alvo iniciadas por ataques de alcance inferior a M5, após a atribuição de seus pontos de ataque seu outro participante sofre 1 ataque de 1 ponto e dano de choque igual aos pontos de ataque da parada menos 2.\n\n' +
                  'Caso o dano desta magia atordoe seu alvo, a parada de combate é concluída e: se o alvo desta magia tiver a iniciado, o ataque é impedido; do contrário, todos os pontos de ataque são infligidos.'
              };
            case 'thunder-strike':
              return {
                title: 'Relâmpago',
                epigraph:
                  'O capitão nos motivava do palanque, sob um céu sem nuvens. Eis que um clarão repentino nos cega, e um ruído estrondoso nos ensurdece; ao nos recuperarmos, vimos seu corpo torrado, caído sobre a terra.',
                effects:
                  'Alvo deve estar sendo ocupado por ao menos 1 ente com condutividade.\n\n' +
                  '1 ataque de 5 pontos e 8 pontos de dano de choque é disparado contra o ocupante de alvo com a maior condutividade, ou, dentre estes, contra o primeiro que ocupou alvo. ' +
                  'Caso a condutividade do alvo desse ataque seja 1, seu jogador pode gastar 1 ponto de destino incontestável para o negar.'
              };
            case 'thunderstorm':
              return {
                title: 'Trovoada',
                epigraph:
                  'O exército inimigo vinha de encontro ao nosso, quando um raio atingiu um de seus soldados. E outro. E outro. E outro. Quando a batalha começou, a maioria de suas tropas já havia debandado.',
                effects:
                  'Ao ser usada, esta magia reproduz o efeito de "Relâmpago", com a diferença de que seu ataque tem 5 pontos e 6 pontos de dano de choque.\n\n' +
                  'Cada sustentação repete essa reprodução, mas dono pode diferir a casa alvo.'
              };
            case 'whirlwind':
              return {
                title: 'Redemoinho',
                epigraph:
                  'Bem, fiz um redemoinho nas redondezas, e o conduzi para o centro da batalha. E ele encerrou a luta, conforme vocês queriam; não tenho culpa de para onde seus ventos levaram alguns dos nossos.',
                effects:
                  'Os ocupantes de alvo se esgotam, e podes mover cada um para [zonas de engajamento/casas desocupadas de seu jogador] que sejam abarcadas por um alcance de escopo R e extensão máxima igual ao mana gasto com a canalização desta magia menos 1.\n\n' +
                  'Por ocupante de alvo que moveres, o oponente pode gastar 1 ponto de destino incontestável para mover um outro, sob restrições iguais.'
              };
            case 'air-cocoon':
              return {
                title: 'Casulo Eólico',
                epigraph:
                  'Fortes correntes de ar o cercaram, e o envolveram em uma esfera quase palpável. Essa esfera acompanhava seus passos – como se orbitasse seu corpo –, e defletia com facilidade flechas que chegavam.',
                effects:
                  'Impede ações em andamento de alvo que sejam "disparar" e "arremessar".\n\n' +
                  'Durante o uso desta magia, alvo não pode [atacar com/ser mirado por] "disparar" e "arremessar".'
              };
            case 'miasma':
              return {
                title: 'Miasma',
                epigraph:
                  'Alguns o descrevem como exalando um leve cheiro pútrido, mas a maioria apenas percebe que há algo de errado com o ar que respira quando é tarde demais.',
                effects:
                  'Em combates, entes bióticos ganham 1 marcador de enfermo ao fim de cada 3 segmentos consecutivos durante os que estiverem em alvo.'
              };
            case 'auspicious-winds':
              return {
                title: 'Ventos Auspiciosos',
                epigraph:
                  'O vento em si marcha conosco em direção à batalha, e impulsiona nossas tropas e flechas para a vitória.',
                effects:
                  'Durante o uso desta magia: o alcance dos ataques "disparar" de tuas tropas aumenta em 1, até R5; as tropas do oponente não podem atacar com "disparar" e "arremessar".\n\n' +
                  'Esta magia disputa com qualquer magia de mesmo título, e desusa cartas "Miasma" em uso do oponente assim que a potência atual destas for igual ou menor que o custo de uso desta magia.'
              };
            case 'flaming-darts':
              return {
                title: 'Dardos Flamejantes',
                epigraph:
                  'Distraído com a luta, o soldado não conseguiu evitar que o fogo do projétil que o acertara se alastrasse por seu flanco. Ao tentar o apagar, o adversário aproveitou a brecha para o lancear fatalmente.',
                effects:
                  'Dono escolhe até 4 alvos, não necessariamente distintos.\n\n' +
                  'Ao ser usada, dispara contra cada alvo escolhido por dono 1 ataque de 1 ponto e 3 pontos de dano de fogo.\n\n' +
                  'O custo de mana desta magia é igual à quantidade de alvos escolhidos por dono.'
              };
            case 'fire-ring':
              return {
                title: 'Anel Ígneo',
                epigraph:
                  'O goblin achava que conseguiria fugir após seu ataque sorrateiro, mas precisou arretar depois que labaredas de fogo mágico irromperam ao seu redor, cercando-o até a chegada de seus inimigos.',
                effects:
                  'Não estando levitado, desabilita de alvo as ações “engajar”, "interceptar", "desengajar" e "recuar". Entre alvo e outros entes físicos, desabilita dos não levitados a ação “atacar”.\n\n' +
                  'Afora ataques distantes, entes sem vontade ou com resistência contra fogo ainda podem acionar essas ações, após sofrerem, ao acionar a primeira, 1 ataque de 10 pontos e 2 pontos de dano de fogo. Ademais, se esse acionante for alvo, esta magia é desusada.'
              };
            case 'fireball':
              return {
                title: 'Bola de Fogo',
                epigraph:
                  'O ar se ignizou em um fogo mágico, que crescia combustando oxigênio, e sob a forma de uma bola giratória. Difícil de conter, essa bola explodiu antes de atingir o alvo, mas incendiou todos por perto.',
                effects:
                  'Alvo deve estar sendo ocupado por ao menos 1 ente físico.\n\n' +
                  '1 ataque de 2 pontos e 5 pontos de dano de fogo é disparado contra os entes físicos em alvo. Enquanto pontos de ataque, ao disparo é adicionada a extensão do alcance efetivo desta magia.\n\n' +
                  'Ao ser usada, podes gastar 1 ponto de destino contestável para alterar os pontos de dano do ataque desta magia para 8, e seu alvo para 1 ente físico, que escolhes ao início da canalização.'
              };
            case 'fiery-rain':
              return {
                title: 'Chuva Incandescente',
                epigraph:
                  'Nuvens do inferno assomaram ao campo de batalha, e despejaram sobre todos gotas de fogo. Um incêndio de corpos se formou, cujo crepitar nenhum dos gritos pôde abafar. E então, houve apenas silêncio.',
                effects:
                  'Ao ser usada, 1 ataque de 6 pontos e 1 ponto de dano de fogo é disparado contra todos os entes físicos no campo por 5 segmentos.\n\n' +
                  'Cada sustentação estende o efeito desta magia por 1 segmento.\n\n' +
                  'Esta magia disputa com qualquer magia de mesmo título.'
              };
            case 'earth-grip':
              return {
                title: 'Aperto da Terra',
                epigraph:
                  'O ogro pensou que a encurralara, mas, ao avançar contra a powthe, seus pés afundaram na terra. Enquanto ele tentava os libertar, a canalizadora, sorrindo, desejou-lhe boa sorte antes de ir embora.',
                effects:
                  'Alvo não deve estar levitado.\n\n' +
                  'Impede ações em andamento de alvo que sejam "engajar", "interceptar", "desengajar", "posicionar" e "recuar". Em adição, impede que alvo acione essas ações, assim como: torne-se levitado; defenda-se com "esquivar"; gaste sua agilidade; desfira ataques próximos, desde que não sejam provocados pelo alcance de uma arma.'
              };
            case 'earth-den':
              return {
                title: 'Reduto Telúrico',
                epigraph:
                  'Abaixo de nossos pés, o inimigo se esconde. O covarde moldou a terra para lhe tunelar um abrigo, mas sua magia não deve persistir por muito mais tempo. Em algum momento, ele terá de subir.',
                effects:
                  'Alvo deve ser dono e estar na retaguarda.\n\n' +
                  'Em batalhas, alvo: é considerado um ente astral ante posicionamentos de ajuste; não pode ser o alvo direto de [ações/efeitos]; não é afetado por ataques; apenas pode acionar "canalizar" – em que apenas pode mirar magias cujo alvo direto seja nulo ou uma magia –, "sustentar", "convocar", "manejar", "recuar" e "abortar".'
              };
            case 'fissure':
              return {
                title: 'Fissura',
                epigraph:
                  'Onde até pouco o solo era liso e hirto, uma rachadura de grandes proporções o engoliu, e o sepultou nas profundezas da terra.',
                effects:
                  'Alvo não deve estar levitado.\n\n' +
                  'Se puder, alvo gasta 2 pontos de agilidade; do contrário, é afastado.\n\n' +
                  'Ao ser usada, caso alvo possa gastar 2 pontos de agilidade mas esteja esgotado, podes gastar 1 ponto de destino contestável para que ele ainda assim seja afastado.'
              };
            case 'earthquake':
              return {
                title: 'Terremoto',
                epigraph:
                  'Eis que um terremoto irrompeu no campo de batalha. Por um longo momento, todos foram forçados a parar de se digladiar para lhe dar a devida atenção, de boa vontade ou não.',
                effects:
                  'Durante o uso desta magia, todos os entes físicos no campo que não estejam levitados se tornam indefesos.\n\n' +
                  'Esta magia disputa com qualquer magia de mesmo título.'
              };
            case 'telepathy':
              return {
                title: 'Telepatia',
                epigraph:
                  'O pensamento de outrem chega até mim como uma forte intuição, que se utiliza de meus próprios pensamentos para concepção de seu significado.',
                effects:
                  'Esta magia não surte efeito enquanto a vontade de alvo for maior que a de dono.\n\n' +
                  'Durante o uso desta magia, toda ação dedicada escolhida para alvo acionar é declarada a ti, e leva 2 segmentos para se iniciar.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'liveliness':
              return {
                title: 'Alento',
                epigraph:
                  'A vivacidade de espírito se reflete em um forte aterramento mental, e em uma disposição para realizar cada ato com presteza.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de alentado, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo: se existente, perde 1 marcador de desalentado; do contrário, ganha 1 marcador de alentado.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'bravery':
              return {
                title: 'Bravura',
                epigraph:
                  'A magia de vothes costuma ser o melhor recurso motivacional para fazer soldados ficarem afoitos para lutar até a morte.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de encorajado, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo ganha 1 marcador de encorajado.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'awe':
              return {
                title: 'Admiração',
                epigraph:
                  'Vothes podem conceder até à mais torpe das criaturas uma forte aura de sublimidade, de modo que os ao seu redor fiquem fascinados com sua presença.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de admirável.\n\n' +
                  'Ao ser usada e em pré-combates, alvo ganha 2 marcadores de admirável por ponto de potência desta magia. A quantidade de marcadores é ajustada sincronicamente com a potência.\n\n' +
                  'Esta magia disputa com "Temor". Em adição, para espíritos, canalizá-la é uma ação parcial, com alcance G1.'
              };
            case 'inspiration':
              return {
                title: 'Inspiração',
                epigraph:
                  'Alguém devotado a uma atividade encontra em si mesma toda motivação necessária para continuar a fazê-la.',
                effects:
                  'Alvo deve estar esgotado e ter 1 ou mais jogadas, todas as quais devem ter sido concluídas.\n\n' +
                  'Quando a vontade de alvo for igual a no mínimo 10 menos a quantidade de sustentações desta magia em seu uso atual, esta magia é desusada e ele se torna repreparado.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'unliveliness':
              return {
                title: 'Desalento',
                epigraph:
                  'Por que lutas? Para proteger quem não se importa contigo? Para defender uma causa abstrata, que tem sentido apenas para quem quiser dar? Larga tuas armas, e luta para melhorar tua própria vida.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de desalentado, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo: se existente, perde 1 marcador de alentado; do contrário, ganha 1 marcador de desalentado.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'rage':
              return {
                title: 'Fúria',
                epigraph:
                  'Amor e piedade não fazem nenhum bem quando teu objetivo é eliminar o inimigo. Se tu não estiveres almejando isso, estás trabalhando de modo aquém ao ideal.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de enfurecido, e esta magia é desusada tão logo isso ocorra.\n\n' +
                  'Ao ser [usada/sustentada], alvo ganha 1 marcador de enfurecido.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'fear':
              return {
                title: 'Temor',
                epigraph:
                  'Por onde o velho andarilho passava, um senso de perdição o acompanhava. Em suas andanças, ele não precisou de outra companhia para que delinquentes o deixassem em paz.',
                effects:
                  'Alvo não deve ter o máximo de marcadores de temível.\n\n' +
                  'Ao ser usada e em pré-combates, alvo ganha 2 marcadores de temível por ponto de potência desta magia. A quantidade de marcadores é ajustada sincronicamente com a potência.\n\n' +
                  'Esta magia disputa com "Admiração". Em adição, para espíritos, canalizá-la é uma ação parcial, com alcance G1.'
              };
            case 'submission':
              return {
                title: 'Submissão',
                epigraph:
                  'A maioria, pouco capaz de pensar por si mesma, leva uma vida subserviente a ideias e desejos alheios. Que, pois, façamos dessas pessoas bom uso, direcionando-as para a causa correta.',
                effects:
                  'Alvo não deve estar esgotado, e deve ter 1 ou mais jogadas.\n\n' +
                  'Impede ações de alvo, e o ocupa. Quando a vontade de alvo for igual a no máximo 1 mais a quantidade de sustentações desta magia em seu uso atual, esta magia é desusada e ele se torna possuído até o fim da terceira paridade tua após esse desuso.\n\n' +
                  'Para espíritos, canalizar esta magia é uma ação parcial, com alcance G1.'
              };
            case 'astral-projection':
              return {
                title: 'Projeção Astral',
                epigraph:
                  'O plano material é apenas um dos muitos no universo. Os outros, coletivamente chamados de astral, contém realidades pouco compreendidas, mas acessíveis por consciências fora de um corpo físico.',
                effects:
                  'Alvo é sempre dono, que não deve: ser um espírito; estar em uma zona de engajamento.\n\n' +
                  'Dono se torna projetado.'
              };
            case 'akashic-borrowing':
              return {
                title: 'Empréstimo Akáshico',
                epigraph:
                  'É dito que, nos ermos do astral, uma biblioteca armazena a história de cada partícula do universo. A cada visita, é possível vivenciar um pouco do passado e futuro, e alugar parte das experiências.',
                effects:
                  'Dono deve ser um espírito.\n\n' +
                  'Dono inicia a canalização de qualquer magia do jogo – a tua escolha – a que atenda os requisitos para tal.'
              };
            case 'cosmoenergy':
              return {
                title: 'Cosmoenergia',
                epigraph:
                  'Os que se alinham com as vibrações do universo são sempre saciados por sua energia.',
                effects:
                  'Alvo é sempre dono, que deve: ser um espírito; ter polaridade positiva em Voth.\n\n' +
                  'Ao ser [usada/sustentada], alvo restaura 2 pontos de energia.\n\n' +
                  'Esta magia é uma ação parcial, e é desusada após restaurar ao todo uma quantidade de energia igual à metade do mana base de dono em Voth.'
              };
            case 'body-takeover':
              return {
                title: 'Usurpação Corporal',
                epigraph:
                  'Cada corpo físico é um veículo para a interação de uma consciência com o plano material. Os sem escrúpulos para alijarem consciências desestimuladas a lutar pelo seu dispõem de muitos para conduzir.',
                effects:
                  'Dono deve poder mirar alvo via "possuir".\n\n' +
                  'Até seu desuso, suspende dono e alvo. Gera para ti uma cópia de alvo, na antiga casa de dono e sem pertences não embutidos. Se ela for removida, alvo também o é, e esta magia é desusada.\n\n' +
                  'Esta magia pode persistir por até 3 turnos, e seu custo de mana mínimo é igual ao vigor de alvo.'
              };
            case 'nature-call':
              return {
                title: 'Chamado da Natureza',
                epigraph:
                  'O santuário foi encantado para ser guardado por lobos de sua floresta. Quem peregrina até lá encontra uma alcateia assentada, que, porém, apenas é hostil a ladrões, vândalos, caçadores e afins.',
                effects:
                  'Em pré-combates, escolhes 1 de 3 opções: 1 urso, 3 lobos ou 1 enxame. Esta magia convoca a opção escolhida para uma zona de engajamento também a tua escolha.'
              };
            case 'celestial-appeal':
              return {
                title: 'Apelo Celestial',
                epigraph:
                  'Muitos rogam por amparo celestial, mas apenas são atendidos os cuja causa é pura, e o poder mágico, notável.',
                effects:
                  'Dono deve ter polaridade positiva em Voth, e tu não deves ter usado uma magia negativa.\n\n' +
                  'Em remobilizações, teus canalizadores com ao menos 3 níveis positivos em Voth e sem nenhum anjo ativo ganham a opção "anjo" na ação "convocar".\n\n' +
                  'Esta magia é desusada após usares uma magia negativa.'
              };
            case 'abyssal-evocation':
              return {
                title: 'Evocação Abissal',
                epigraph:
                  'Após conjurados via rituais sombrios, demônios costumam servir a seu conjurador de bom grado, pois sabem que essa subordinação se manterá no pós-vida, mas com eles tendo então poder para a inverter.',
                effects:
                  'Dono deve ter polaridade negativa em Voth.\n\n' +
                  'Em remobilizações, teus canalizadores com ao menos 2 níveis negativos em Voth ganham a opção "demônio" na ação "convocar". Essa opção é desabilitada a um canalizador enquanto ele tiver 1 demônio ativo por 2 níveis em Voth.'
              };
            case 'macabre-communion':
              return {
                title: 'Comunhão Macabra',
                epigraph:
                  'Quem controla a matéria e a mente tem pouca dificuldade em angariar o serviço dos mortos. Os que pavimentam campos de batalha costumam ser uma mão de obra barata e apreciada.',
                effects:
                  'Dono deve ter ao menos 1 nível em Enoth.\n\n' +
                  'Em pré-combates, por humanoide teu afastado, esta magia convoca 1 ficha de morto-vivo vezes metade do nível em Enoth de dono, que então distribuis em zonas de engajamento a tua escolha.'
              };
            case 'pyrotic-animation':
              return {
                title: 'Animação Pirótica',
                epigraph:
                  'O fogo é uma energia destrutiva desnorteada. Entretanto, vothes podem conceder um pouco de volição a ele, no intuito de o orientarem acerca do que destruir.',
                effects:
                  'Dono deve ter ao menos 1 nível em Powth.\n\n' +
                  'Em remobilizações, teus canalizadores powthes com ao menos 2 níveis em Voth ganham a opção "salamandra dourada" na ação "convocar". Em adição, os com também polaridade negativa em Voth ganham a opção "salamandra anilada". ' +
                  'As opções desta magia são desabilitadas a um canalizador enquanto ele tiver 1 salamandra ativa por 2 níveis em Voth.'
              };
            case 'the-champion':
              return {
                title: 'O Campeão',
                epigraph:
                  'Foi-lhe dada uma coroa, e ele saiu vencendo, e para vencer.',
                effects:
                  'Alvo deve ser uma criatura tua.\n\n' +
                  'Alvo ganha uma quantidade infinita de pontos de destino. Porém, tu perdes a rodada caso ele seja removido.\n\n' +
                  'O custo de mana desta magia aumenta em 2 por turno da batalha concluído, até ser igual a 8.'
              };
            case 'the-seeker':
              return {
                title: 'O Aspirante',
                epigraph:
                  'O destino eventualmente favorece os que perseguem suas aspirações.',
                effects:
                  'Em pré-combates, se alvo estiver ativo e sem um aspirado, ele escolhe 1 ente ativo adversário para o ser, até a inativação deste.\n\n' +
                  'Sendo aplicável a ti, atos de alvo contra o aspirado ou seus pertences usam pontos de destino automática e gratuitamente.\n\n' +
                  'O custo de mana desta magia diminui em 4 por ponto de destino que o jogador de alvo gaste via ele, até ser igual a 2.'
              };
            case 'the-prey':
              return {
                title: 'A Caça',
                epigraph:
                  'Ah, esse teu cheiro de medo...',
                effects:
                  'O oponente não pode gastar pontos de destino para atos teus contra alvo ou seus pertences.\n\n' +
                  'O custo de mana desta magia diminui em 4 por ponto de destino que o jogador de alvo gaste via ele, até ser igual a 2.'
              };
            case 'the-ruler':
              return {
                title: 'O Regente',
                epigraph:
                  'Primeiro receberam os atos de um líder, depois deram a autoridade para liderar.',
                effects:
                  'Alvo deve ser uma criatura tua.\n\n' +
                  'Enquanto alvo estiver ativo, teu comandante: não te derrota se inativo; é considerado criatura ante a ação "recuar". Enquanto teu comandante estiver inativo, alvo não pode ser suspenso, à exceção de via a magia "A Trégua".\n\n' +
                  'O custo de mana desta magia diminui em 2 por combate concluído em que alvo esteve sempre ativo, até ser igual a 2.'
              };
            case 'the-hoard':
              return {
                title: 'A Riqueza',
                epigraph:
                  'Não existe excesso de riqueza para quem pode enriquecer.',
                effects:
                  'Converte para transitivos teus pontos de destino intransitivos atuais e futuros.\n\n' +
                  'O custo de mana desta magia diminui em 2 por ponto de destino atual teu, até ser igual a 4.'
              };
            case 'the-idol':
              return {
                title: 'O Ídolo',
                epigraph:
                  'Quem não vê um criador diretamente o encontra em suas obras.',
                effects:
                  'Em pré-combates, ganhas 1 ponto de destino intransitivo por relíquia tua que estiver ativa.\n\n' +
                  'O custo de mana desta magia diminui em 4 por combate concluído em que ao menos 1 relíquia tua esteve sempre ativa, até ser igual a 4.'
              };
            case 'the-host':
              return {
                title: 'A Hóstia',
                epigraph:
                  'O sacrifício de quem está abaixo é frequentemente o que eleva quem está acima.',
                effects:
                  'Ganhas 1 ponto de destino transitivo assim que um humanoide teu for afastado, de modo também retroativo.\n\n' +
                  'O custo de mana desta magia diminui em 2 por humanoide teu que seja afastado, até ser igual a 4.'
              };
            case 'the-spring':
              return {
                title: 'A Primavera',
                epigraph:
                  'Quem semeia pelos campos celestiais colhe frutos de bem-aventurança.',
                effects:
                  'Em pré-combates, ganhas 2 pontos de destino intransitivos.\n\n' +
                  'Esta magia disputa com "O Inverno", enquanto em uso pelo oponente.\n\n' +
                  'O custo de mana desta magia diminui em 3 por ponto de destino que gastes no combate atual, até ser igual a 3.'
              };
            case 'the-winter':
              return {
                title: 'O Inverno',
                epigraph:
                  'Os que não tomam providências para a chegada do inverno morrem de frio e fome.',
                effects:
                  'O oponente não ganha mais pontos de destino.\n\n' +
                  'Esta magia disputa com "A Primavera", enquanto em uso pelo oponente.\n\n' +
                  'O custo de mana desta magia diminui em 3 por ponto de destino que o oponente gaste no combate atual, até ser igual a 3.'
              };
            case 'the-chalice':
              return {
                title: 'O Cálice',
                epigraph:
                  'Arcanistas que bebem do cálice de faothes são abençoados ao lançarem suas mais primorosas magias.',
                effects:
                  'Em combates, ganhas 1 ponto de destino intransitivo por 2 pontos de mana que gastes com uma [canalização/sustentação].\n\n' +
                  'O custo de mana desta magia diminui em 1 por 2 pontos de mana que qualquer jogador gaste com uma [canalização/sustentação] em combates, até ser igual a 3.\n\n' +
                  'Ao determinar pontos de mana gastos, esta magia inclui energia e custos de magias modificadoras, e exclui ônus de polaridades.'
              };
            case 'the-genie':
              return {
                title: 'O Gênio',
                epigraph:
                  'Para que limitares teus desejos quando podes desejar mais desejos.',
                effects:
                  '1 vez por pré-combate, podes gastar 1 ponto de destino incontestável para suspenderes 1 magia tua ativa ou afastada.\n\n' +
                  'O custo de mana desta magia diminui em 3 por canalização concluída por ti em combates, até ser igual a 3.'
              };
            case 'the-daeva':
              return {
                title: 'O Daeva',
                epigraph:
                  'O mana corrompe tão bem quanto o ouro.',
                effects:
                  'Em combates, o oponente gasta 1 ponto de destino incontestável ao fim do custo de fluxo para canalização de qualquer magia sua; caso não possa, a canalização é impedida.\n\n' +
                  'O custo de mana desta magia diminui em 3 por canalização concluída pelo oponente em combates, até ser igual a 3.'
              };
            case 'the-feast':
              return {
                title: 'O Festim',
                epigraph:
                  'A fortuna sorri para nós, e nos abarrota de fartura. Aproveitemos todo meu vinho, pois hoje ele não há de acabar.',
                effects:
                  'Em paradas de destino contestadas pelo oponente, podes gastar 1 ponto de destino adicional para negar essa contestação. ' +
                  'Porém, se o oponente tiver uma magia de mesmo título em uso, ele pode, então, recontestar a parada, via outro ponto de destino.\n\n' +
                  'O custo de mana desta magia diminui em 2 por parada de destino contestável tua resolvida em teu favor, até ser igual a 2.'
              };
            case 'the-beast':
              return {
                title: 'A Fera',
                epigraph:
                  'Há uma fera no escuro, à espreita. Ninguém nunca quer a encontrar, mas otimismo é inútil: ela está sempre faminta.',
                effects:
                  '1 vez por pós-combate, podes gastar 1 ponto de destino para afastar 1 criatura biótica ativa ou suspensa, a tua escolha. ' +
                  'Esse ponto de destino é incontestável caso o alvo seja uma besta, e contestável caso ele seja um humanoide.\n\n' +
                  'O custo de mana desta magia diminui em 2 por humanoide do oponente que seja removido na fase da batalha, até ser igual a 2.'
              };
            case 'the-gate':
              return {
                title: 'O Portão',
                epigraph:
                  'Todos precisam da aprovação do destino para passar por cada portão da vida.',
                effects:
                  'Em combates e remobilizações, o oponente deve gastar 1 ponto de destino contestável como custo para acionar "mobilizar" e "recuar". Caso o contestes, durante o resto do período atual a ação alvo se torna desabilitada ao ente que a acionaria.\n\n' +
                  'O custo de mana desta magia diminui em 2 por ação "mobilizar" e "recuar" concluída pelo oponente em combates e remobilizações, até ser igual a 4.'
              };
            case 'the-castle':
              return {
                title: 'O Castelo',
                epigraph:
                  'Mostra teu castelo aos justos que encontrar, pois vazio ele não te protegerá dos vis.',
                effects:
                  '1 vez por parada de combate defensiva tua, podes gastar 1 ponto de destino incontestável para que o defensor ganhe 3 pontos de bônus de defesa.\n\n' +
                  'O custo de mana desta magia diminui em 2 por parada de combate iniciada pelo oponente, até ser igual a 4.'
              };
            case 'the-prison':
              return {
                title: 'O Cárcere',
                epigraph:
                  'Todo soldado é um prisioneiro, a transitar por celas enquanto cumpre a pena da guerra.',
                effects:
                  'Em combates e remobilizações, o oponente deve gastar 1 ponto de destino contestável como custo para acionar "posicionar". Caso o contestes, durante o resto do período atual essa ação se torna desabilitada ao ente que a acionaria.\n\n' +
                  'O custo de mana desta magia diminui em 2 por ação "posicionar" concluída pelo oponente em combates e remobilizações, até ser igual a 4.'
              };
            case 'the-plot':
              return {
                title: 'O Enredo',
                epigraph:
                  'Personagens fictícios seguem os eventos de um narrador; personagens reais, os de um canalizador.',
                effects:
                  'Quando maior, a potência desta magia substitui a potência de tuas magias permanentes em uso atuais e futuras.\n\n' +
                  'O custo de mana mínimo desta magia diminui em 5 por magia permanente atualmente em uso por ti, até ser igual a 5.'
              };
            case 'the-vision':
              return {
                title: 'A Visão',
                epigraph:
                  'Às vezes, alguns são abduzidos por uma imaginação tão real que se questiona se o abdutor foi o tempo.',
                effects:
                  'Retrocede 5 segmentos do combate atual, ou, na falta disso, até seu início.\n\n' +
                  'Na nova resolução dos segmentos retrocedidos, são: suprimidos efeitos de "Poção do Alerta"; desabilitados usos desta magia.\n\n' +
                  'Após usada, esta magia é findada.'
              };
            case 'the-truce':
              return {
                title: 'A Trégua',
                epigraph:
                  'Na paz encontramos oportunidade para preparar novas operações de guerra.',
                effects:
                  'No pós-combate do turno de uso, conclui o turno e: suspende tropas; remove fichas, e o mana em casas; retorna ao estado inicial as estatísticas e vinculados de cartas suspensas. ' +
                  'Então, nesta ordem inicia: uma mobilização; uma alocação; o próximo turno da batalha.\n\n' +
                  'O custo de mana desta magia diminui em 2 por tropa que tenhas a mais que o oponente, até ser igual a 2. Em adição, esta magia pode ser usada apenas 1 vez por batalha, nos turnos 3 ou 4.'
              };
            case 'the-lure':
              return {
                title: 'O Engodo',
                epigraph:
                  'Os que se apressam em sua investida são os mais suscetíveis a tropeçarem de encontro ao inimigo.',
                effects:
                  'No pós-combate do turno de uso, quando possível suspende criaturas humanoides tuas em zonas de engajamento não controladas por ti. ' +
                  'Na remobilização do turno de uso: ganhas 1 jogada por ente suspenso desse modo; tuas ações "mobilizar" podem mirar zonas de engajamento ocupadas.\n\n' +
                  'O custo de mana desta magia diminui em 2 por tropa que o oponente tenha a mais que ti, até ser igual a 2. Em adição, esta magia pode ser usada por ti apenas 1 vez por batalha.'
              };
            case 'the-end':
              return {
                title: 'O Fim',
                epigraph:
                  'Quando as preces de guerra são feitas por um faothe, a vitória chega por meios que o destino é coagido a trazer.',
                effects:
                  'Conclui a rodada, e define seu vencedor como o jogador que tiver mais pontos de destino no momento da conclusão.\n\n' +
                  'Se ambos os jogadores tiverem a mesma quantidade de pontos de destino, um empate é computado.\n\n' +
                  'O custo de mana desta magia diminui em 4 por turno da batalha iniciado.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }
    }

    // Retorna dados de localização da característica passada
    cards.getTrait = function ( traitName ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( typeof traitName == 'string' );

      // Identifica característica alvo
      switch( traitName ) {
        case 'racial-mix':
          return {
            title: 'Integração Racial',
            description:
              'Em um baralho principal, o limite de humanos comandáveis, se acima de 0, aumenta em 1 por cada raça não humana cuja quantidade de criaturas no baralho exceda a mínima em 1, 3 e 5.'
          };
        case 'adaptive-tactics':
          return {
            title: 'Adaptabilidade Tática',
            description:
              'Por paridade de remobilizações, as 2 primeiras ações dedicadas acionadas via humanos são convertidas para livres.'
          };
        case 'raging-surge':
          return {
            title: 'Surto Raivoso',
            description:
              'Em combates, ao sofrerem ao menos 10 pontos de dano de uma vez, orques, nesta ordem, ganham: a cada 2 pontos de dano adicionais, 1 marcador de encorajado ou, caso o oponente gaste 1 ponto de destino contestável, de enfurecido; 1 ataque livre, contra um alvo à escolha de seu jogador.'
          };
        case 'powerful-shots':
          return {
            title: 'Disparos Poderosos',
            description:
              'A penetração e o alcance dos ataques "disparar" de arcos em uso por orques aumentam em 1, até, respectivamente, 10 e R5.'
          };
        case 'arcane-mastery':
          return {
            title: 'Maestria Arcana',
            description:
              'Cada elfo canalizador concede a seu baralho créditos, separados por senda e iguais ao nível do elfo na senda vezes 6. Esses créditos são gastos com magias de sua senda no baralho, antes de moedas.'
          };
        case 'dexterous-wielding':
          return {
            title: 'Empunhadura Exímia',
            description:
              'Elfos não sofrem penalidades de empunhadura.'
          };
        case 'ingenious-craftsmanship':
          return {
            title: 'Produção Engenhosa',
            description:
              'Cada anão concede a seu baralho 15 pontos de crédito vezes seu nível de experiência. Esses créditos são gastos com equipamentos no baralho, antes de moedas.'
          };
        case 'antimagic-essence':
          return {
            title: 'Essência Antimágica',
            description:
              'Anões não podem ser o alvo direto de magias de [Enoth/Voth] cuja potência prevista seja menor que 5.'
          };
        case 'restless-scouting':
          return {
            title: 'Batedores Irrequietos',
            description:
              'Em mobilizações, cada metadílio ativo ganha a ação "posicionar", enquanto sendo singular.'
          };
        case 'blessed-fate':
          return {
            title: 'Destino Agraciado',
            description:
              'Cada metadílio tem 1 ponto de destino.'
          };
        case 'shared-magic':
          return {
            title: 'Coletividade Mágica',
            description:
              'Magias ativadas por [gnomos/espíritos de gnomos] são suspensas após: seu desuso; sua canalização ser interrompida.'
          };
        case 'inbred-voth':
          return {
            title: 'Inatismo Vôthico',
            description:
              'Ao início de pré-combates, cada gnomo não removido restaura 2 pontos de mana de Voth.'
          };
        case 'timely-ambush':
          return {
            title: 'Emboscada Oportuna',
            description:
              'Em combates, cada goblin suspenso: gera 1 jogada; ganha a ação "mobilizar", enquanto sendo parcial e mirando uma zona de engajamento ocupada.'
          };
        case 'skittering-retreat':
          return {
            title: 'Recuo Matreiro',
            description:
              'Em combates, cada criatura goblin na zona de controle de seu jogador ganha a ação "recuar", enquanto com custo de fluxo igual a 2.'
          };
        case 'slow-pace':
          return {
            title: 'Caminhada Lenta',
            description:
              'O custo de fluxo das ações "engajar" e "desengajar" de ogros é igual a 4.'
          };
        case 'stubborn-tactics':
          return {
            title: 'Teimosia Tática',
            description:
              'Em remobilizações, ogros apenas podem acionar ações em turnos cuja numeração seja da mesma paridade que a de seu jogador.'
          };
      }
    }
  }
} )();
