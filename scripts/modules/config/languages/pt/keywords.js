// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'pt' ) return;

  // Identificadores
  const keywords = m.languages.keywords = {};

  // Propriedades iniciais
  defineProperties: {
    // Para paridades
    keywords.getParity = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'odd':
          return config.plural ? 'Ímpares' : 'Ímpar';
        case 'even':
          return config.plural ? 'Pares' : 'Par';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para atividades
    keywords.getActivity = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'inUse':
        case 'in-use':
        case 'active':
          return config.feminine && config.plural ? 'Ativas' :
                 config.feminine                  ? 'Ativa' :
                 config.plural                    ? 'Ativos' : 'Ativo';
        case 'inactive':
          return config.feminine && config.plural ? 'Inativas' :
                 config.feminine                  ? 'Inativa' :
                 config.plural                    ? 'Inativos' : 'Inativo';
        case 'suspended':
          return config.feminine && config.plural ? 'Suspensas' :
                 config.feminine                  ? 'Suspensa' :
                 config.plural                    ? 'Suspensos' : 'Suspenso';
        case 'withdrawn':
          return config.feminine && config.plural ? 'Afastadas' :
                 config.feminine                  ? 'Afastada' :
                 config.plural                    ? 'Afastados' : 'Afastado';
        case 'ended':
          return config.feminine && config.plural ? 'Findadas' :
                 config.feminine                  ? 'Findada' :
                 config.plural                    ? 'Findados' : 'Findado';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para estado de preparo
    keywords.getReadiness = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'unprepared':
          return config.feminine && config.plural ? 'Despreparadas' :
                 config.feminine                  ? 'Despreparada' :
                 config.plural                    ? 'Despreparados' : 'Despreparado';
        case 'prepared':
          return config.feminine && config.plural ? 'Preparadas' :
                 config.feminine                  ? 'Preparada' :
                 config.plural                    ? 'Preparados' : 'Preparado';
        case 'occupied':
          return config.feminine && config.plural ? 'Ocupadas' :
                 config.feminine                  ? 'Ocupada' :
                 config.plural                    ? 'Ocupados' : 'Ocupado';
        case 'exhausted':
          return config.feminine && config.plural ? 'Esgotadas' :
                 config.feminine                  ? 'Esgotada' :
                 config.plural                    ? 'Esgotados' : 'Esgotado';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos primitivos
    keywords.getPrimitiveType = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'being':
        case 'beings':
          return config.plural ? 'Entes' : 'Ente';
        case 'item':
        case 'items':
          return config.plural ? 'Equipamentos' : 'Equipamento';
        case 'spell':
        case 'spells':
          return config.plural ? 'Magias' : 'Magia';
        case 'creature':
        case 'creatures':
          return config.plural ? 'Criaturas' : 'Criatura';
        case 'asset':
        case 'assets':
          return config.plural ? 'Ativos' : 'Ativo';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos acrescidos
    keywords.getTypeset = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'environment':
          return config.plural ? 'Ambientes' : 'Ambiente';
        case 'attachability':
          return config.plural ? 'Vinculações' : 'Vinculação';
        case 'race':
          return config.plural ? 'Raças' : 'Raça';
        case 'equipage':
          return config.plural ? 'Equipagens' : 'Equipagem';
        case 'path':
          return config.plural ? 'Sendas' : 'Senda';
        case 'size':
          return config.plural ? 'Tamanhos' : 'Tamanho';
        case 'weight':
          return config.plural ? 'Pesos' : 'Peso';
        case 'polarity':
          return config.plural ? 'Polaridades' : 'Polaridade';
        case 'role':
          return config.plural ? 'Atuações' : 'Atuação';
        case 'roleType':
          return config.plural ? 'Tipos de Atuação' : 'Tipo de Atuação';
        case 'experience':
          return config.plural ? 'Experiências' : 'Experiência';
        case 'experienceLevel':
          return config.plural ? 'Níveis de Experiência' : 'Nível de Experiência';
        case 'grade':
          return config.plural ? 'Categorias' : 'Categoria';
        case 'wielding':
          return config.plural ? 'Empunhaduras' : 'Empunhadura';
        case 'usage':
          return config.plural ? 'Usos' : 'Uso';
        case 'duration':
          return config.plural ? 'Durações' : 'Duração';
        case 'availability':
          return config.plural ? 'Disponibilidades' : 'Disponibilidade';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para estatísticas diversas
    keywords.getStat = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'attributes':
          return config.plural ? 'Atributos' : 'Atributo';
        case 'fighting':
          return config.plural ? 'Lutas' : 'Luta';
        case 'channeling':
          return config.plural ? 'Canalizações' : 'Canalização';
        case 'effectivity':
          return config.plural ? 'Efetividades' : 'Efetividade';
        case 'resistances':
          return config.plural ? 'Resistências' : 'Resistência';
        case 'modifier':
          return config.plural ? 'Modificadores' : 'Modificador';
        case 'penetration':
          return config.plural ? 'Penetrações' : 'Penetração';
        case 'recovery':
          return config.plural ? 'Recuperações' : 'Recuperação';
        case 'range':
          return config.plural ? 'Alcances' : 'Alcance';
        case 'coverage':
          return config.plural ? 'Coberturas' : 'Cobertura';
        case 'conductivity':
          return config.plural ? 'Condutividades' : 'Condutividade';
        case 'mana':
          return 'Mana';
        case 'flowCost':
          return config.plural ? 'Custos de Fluxo' : 'Custo de Fluxo';
        case 'manaCost':
          return config.plural ? 'Custos de Mana' : 'Custo de Mana';
        case 'persistenceCost':
          return config.plural ? 'Custos de Persistência' : 'Custo de Persistência';
        case 'potency':
          return config.plural ? 'Potências' : 'Potência';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para atributos
    keywords.getAttribute = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'health':
          return 'Vida';
        case 'initiative':
          return 'Iniciativa';
        case 'stamina':
          return 'Vigor';
        case 'agility':
          return 'Agilidade';
        case 'will':
          return 'Vontade';
        case 'energy':
          return 'Energia';
        case 'power':
          return 'Poder';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para vinculações
    keywords.getAttachability = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case undefined:
          return 'Nulo';
        case 'Being':
          return config.plural ? 'Entes' : 'Ente';
        case 'PhysicalBeing':
          return config.plural ? 'Entes Físicos' : 'Ente Físico';
        case 'BioticBeing':
          return config.plural ? 'Entes Bióticos' : 'Ente Biótico';
        case 'Humanoid':
          return config.plural ? 'Humanoides' : 'Humanoide';
        case 'Beast':
          return config.plural ? 'Bestas' : 'Besta';
        case 'Construct':
          return config.plural ? 'Constructos' : 'Constructo';
        case 'AstralBeing':
          return config.plural ? 'Entes Astrais' : 'Ente Astral';
        case 'Item':
          return config.plural ? 'Equipamentos' : 'Equipamento';
        case 'Weapon':
          return config.plural ? 'Armas' : 'Arma';
        case 'Garment':
          return config.plural ? 'Trajes' : 'Traje';
        case 'Implement':
          return config.plural ? 'Apetrechos' : 'Apetrecho';
        case 'Spell':
          return config.plural ? 'Magias' : 'Magia';
        case 'CardSlot':
          return config.plural ? 'Casas' : 'Casa';
        case 'FieldGridSlot':
          return config.plural ? 'Casas de Grade' : 'Casa de Grade';
        case 'EngagementZone':
          return config.plural ? 'Zonas de Engajamento' : 'Zona de Engajamento';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para usos
    keywords.getUsage = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'static':
          return 'Estático';
        case 'dynamic':
          return 'Dinâmico';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para raças
    keywords.getRace = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'human':
          return config.plural ? 'Humanos' : 'Humano';
        case 'orc':
          return config.plural ? 'Orques' : 'Orque';
        case 'elf':
          return config.plural ? 'Elfos' : 'Elfo';
        case 'dwarf':
          return config.plural ? 'Anões' : 'Anão';
        case 'halfling':
          return config.plural ? 'Metadílios' : 'Metadílio';
        case 'gnome':
          return config.plural ? 'Gnomos' : 'Gnomo';
        case 'goblin':
          return config.plural ? 'Goblines' : 'Goblin';
        case 'ogre':
          return config.plural ? 'Ogros' : 'Ogro';
        case 'bear':
          return config.plural ? 'Ursos' : 'Urso';
        case 'wolf':
          return config.plural ? 'Lobos' : 'Lobo';
        case 'swarm':
          return config.plural ? 'Enxames' : 'Enxame';
        case 'undead':
          return config.plural ? 'Mortos-vivos' : 'Morto-vivo';
        case 'salamander':
          return config.plural ? 'Salamandras' : 'Salamandra';
        case 'golden-salamander':
        case 'goldenSalamander':
          return config.plural ? 'Salamandras Douradas' : 'Salamandra Dourada';
        case 'indigo-salamander':
        case 'indigoSalamander':
          return config.plural ? 'Salamandras Aniladas' : 'Salamandra Anilada';
        case 'golem':
          return config.plural ? 'Golens' : 'Golem';
        case 'tulpa':
          return config.plural ? 'Tulpas' : 'Tulpa';
        case 'spirit':
          return config.plural ? 'Espíritos' : 'Espírito';
        case 'demon':
          return config.plural ? 'Demônios' : 'Demônio';
        case 'angel':
          return config.plural ? 'Anjos' : 'Anjo';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para ambientes
    keywords.getEnvironment = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'physical':
          return 'Físico';
        case 'astral':
          return 'Astral';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para nomes de atuação
    keywords.getRoleName = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'civilian':
          return config.plural ? 'Civis' : 'Civil';
        case 'maceman':
          return config.plural ? 'Claveiros' : 'Claveiro';
        case 'swordsman':
          return config.plural ? 'Espadeiros' : 'Espadeiro';
        case 'axeman':
          return config.plural ? 'Machadeiros' : 'Machadeiro';
        case 'spearman':
          return config.plural ? 'Lanceiros' : 'Lanceiro';
        case 'slingman':
          return config.plural ? 'Fundeiros' : 'Fundeiro';
        case 'bowman':
          return config.plural ? 'Arqueiros' : 'Arqueiro';
        case 'crossbowman':
          return config.plural ? 'Besteiros' : 'Besteiro';
        case 'theurge':
          return config.plural ? 'Teurgos' : 'Teurgo';
        case 'warlock':
          return config.plural ? 'Bruxos' : 'Bruxo';
        case 'wizard':
          return config.plural ? 'Magos' : 'Mago';
        case 'sorcerer':
          return config.plural ? 'Feiticeiros' : 'Feiticeiro';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos de atuação
    keywords.getRoleType = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'melee':
          return config.person && config.plural  ? 'Combatentes Próximos' :
                 config.person                   ? 'Combatente Próximo' :
                 config.plural                   ? 'Combates Próximos' :
                                                   'Combate Próximo';
        case 'ranged':
          return config.person && config.plural  ? 'Combatentes Distantes' :
                 config.person                   ? 'Combatente Distante' :
                 config.plural                   ? 'Combates Distantes' :
                                                   'Combate Distante';
        case 'magical':
          return config.person && config.plural  ? 'Combatentes Mágicos' :
                 config.person                   ? 'Combatente Mágico' :
                 config.plural                   ? 'Combates Mágicos' :
                                                   'Combate Mágico';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para categorias de experiência
    keywords.getExperienceGrade = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'commander':
          return config.plural ? 'Comandantes' : 'Comandante';
        case 'rookie':
          return config.plural ? 'Recrutas' : 'Recruta';
        case 'regular':
          return config.plural ? 'Regulares' : 'Regular';
        case 'veteran':
          return config.plural ? 'Veteranos' : 'Veterano';
        case 'elite':
          return config.plural ? 'Elitistas' : 'Elitista';
        case 'novice':
          return config.plural ? 'Noviços' : 'Noviço';
        case 'disciple':
          return config.plural ? 'Discípulos' : 'Discípulo';
        case 'adept':
          return config.plural ? 'Adeptos' : 'Adepto';
        case 'master':
          return config.plural ? 'Mestres' : 'Mestre';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para níveis de experiência
    keywords.getExperienceLevel = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 0:
        case '0':
          return 'Nível 0';
        case 1:
        case '1':
          return 'Nível 1';
        case 2:
        case '2':
          return 'Nível 2';
        case 3:
        case '3':
          return 'Nível 3';
        case 4:
        case '4':
          return 'Nível 4';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para equipagens
    keywords.getEquipage = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'weapon':
          return config.plural ? 'Armas' : 'Arma';
        case 'garment':
          return config.plural ? 'Trajes' : 'Traje';
        case 'implement':
          return config.plural ? 'Apetrechos' : 'Apetrecho';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tamanhos
    keywords.getSize = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case null:
        case 'null':
          return 'Nulo';
        case 'small':
          return 'Pequeno';
        case 'medium':
          return 'Médio';
        case 'large':
          return 'Grande';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para pesos
    keywords.getWeight = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'light':
          return 'Leve';
        case 'medium':
          return 'Médio';
        case 'heavy':
          return 'Pesado';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para categorias de equipamentos
    keywords.getItemGrade = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'ordinary':
          return config.plural ? 'Ordinários' : 'Ordinário';
        case 'masterpiece':
          return config.plural ? 'Obras-Primas' : 'Obra-Prima';
        case 'artifact':
          return config.plural ? 'Artefatos' : 'Artefato';
        case 'relic':
          return config.plural ? 'Relíquias' : 'Relíquia';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para empunhaduras
    keywords.getWielding = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'non-handed':
          return 'Amanual';
        case 'one-handed':
          return 'Unimanual';
        case 'two-handed':
          return 'Bimanual';
        case 'multi-handed':
          return 'Multimanual';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para armas
    keywords.getWeapon = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'unarmed':
          return 'Desarmado';
        case 'mace':
          return config.plural ? 'Clavas' : 'Clava';
        case 'sword':
          return config.plural ? 'Espadas' : 'Espada';
        case 'axe':
          return config.plural ? 'Machados' : 'Machado';
        case 'spear':
          return config.plural ? 'Lanças' : 'Lança';
        case 'shield':
          return config.plural ? 'Escudos' : 'Escudo';
        case 'sling':
          return config.plural ? 'Fundas' : 'Funda';
        case 'bow':
          return config.plural ? 'Arcos' : 'Arco';
        case 'crossbow':
          return config.plural ? 'Bestas' : 'Besta';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para manobras
    keywords.getManeuver = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^maneuver-/, '' ) ) {
        case 'unarmed':
          return 'Desarmado';
        case 'bash':
          return 'Bater';
        case 'cut':
          return 'Cortar';
        case 'thrust':
          return 'Perfurar';
        case 'shoot':
          return 'Disparar';
        case 'throw':
          return 'Arremessar';
        case 'scratch':
          return 'Arranhar';
        case 'bite':
          return 'Morder';
        case 'swarm':
          return 'Enxamear';
        case 'burn':
          return 'Incendiar';
        case 'fire-bolt':
          return 'Raio de Fogo';
        case 'mana-bolt':
          return 'Raio de Mana';
        case 'rapture':
          return 'Arrebatar';
        case 'repress':
          return 'Reprimir';
        case 'dodge':
          return 'Esquivar';
        case 'block':
          return 'Bloquear';
        case 'repel':
          return 'Repelir';
        case 'cover':
          return 'Cobrir';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para danos
    keywords.getDamage = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^damage-/, '' ) ) {
        case 'blunt':
          return 'Concussão';
        case 'slash':
          return 'Corte';
        case 'pierce':
          return 'Perfuração';
        case 'fire':
          return 'Fogo';
        case 'shock':
          return 'Choque';
        case 'mana':
          return 'Mana';
        case 'raw':
          return 'Bruto';
        case 'ether':
          return 'Éter';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para sendas
    keywords.getPath = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'metoth':
          return 'Metoth';
        case 'enoth':
          return 'Enoth';
        case 'powth':
          return 'Powth';
        case 'voth':
          return 'Voth';
        case 'faoth':
          return 'Faoth';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para polaridades
    keywords.getPolarity = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'positive':
          return config.feminine ? 'Positiva' : 'Positivo';
        case 'apolar':
          return 'Apolar';
        case 'negative':
          return config.feminine ? 'Negativa' : 'Negativo';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para durações
    keywords.getDuration = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'immediate':
          return 'Imediata';
        case 'sustained':
          return 'Sustentada';
        case 'sustained-spiking':
          return 'De Pico';
        case 'sustained-constant':
          return 'Constante';
        case 'persistent':
          return 'Persistente';
        case 'permanent':
          return 'Permanente';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para baralhos
    keywords.getDeck = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'mainDeck':
        case 'main-deck':
          return config.plural ? 'Baralhos Principais' : 'Baralho Principal';
        case 'sideDeck':
        case 'side-deck':
          return config.plural ? 'Baralhos Coadjuvantes' : 'Baralho Coadjuvante';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para unidades de fluxo e estágios
    keywords.getFlow = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'round':
          return config.plural ? 'Rodadas' : 'Rodada';
        case 'setup-phase':
          return config.plural ? 'Fases do Preparo' : 'Fase do Preparo';
        case 'battle-phase':
          return config.plural ? 'Fases da Batalha' : 'Fase da Batalha';
        case 'step':
          return config.plural ? 'Etapas' : 'Etapa';
        case 'arrangement-step':
          return config.plural ? 'Etapas do Arranjo' : 'Etapa do Arranjo';
        case 'deployment-step':
          return config.plural ? 'Etapas da Mobilização' : 'Etapa da Mobilização';
        case 'allocation-step':
          return config.plural ? 'Etapas da Alocação' : 'Etapa da Alocação';
        case 'battle-step':
          return config.plural ? 'Etapas da Batalha' : 'Etapa da Batalha';
        case 'turn':
          return config.plural ? 'Turnos' : 'Turno';
        case 'arrangement-setup-turn':
          return config.plural ? 'Turnos do Arranjo' : 'Turno do Arranjo';
        case 'deployment-setup-turn':
          return config.plural ? 'Turnos da Mobilização' : 'Turno da Mobilização';
        case 'allocation-setup-turn':
          return config.plural ? 'Turnos da Alocação' : 'Turno da Alocação';
        case 'battle-turn':
          return config.plural ? 'Turnos da Batalha' : 'Turno da Batalha';
        case 'period':
          return config.plural ? 'Períodos' : 'Período';
        case 'pre-combat-break-period':
          return config.plural ? 'Períodos do Pré-combate' : 'Período do Pré-combate';
        case 'combat-period':
          return config.plural ? 'Períodos do Combate' : 'Período do Combate';
        case 'post-combat-break-period':
          return config.plural ? 'Períodos do Pós-combate' : 'Período do Pós-combate';
        case 'redeployment-period':
          return config.plural ? 'Períodos da Remobilização' : 'Período da Remobilização';
        case 'block':
          return config.plural ? 'Blocos' : 'Bloco';
        case 'being-block':
          return config.plural ? 'Blocos dos Entes' : 'Bloco dos Entes';
        case 'item-block':
          return config.plural ? 'Blocos dos Equipamentos' : 'Bloco dos Equipamentos';
        case 'spell-block':
          return config.plural ? 'Blocos das Magias' : 'Bloco das Magias';
        case 'segment':
          return config.plural ? 'Segmentos' : 'Segmento';
        case 'parity':
          return config.plural ? 'Paridades' : 'Paridade';
        case 'odd-parity':
          return config.plural ? 'Paridades Ímpares' : 'Paridade Ímpar';
        case 'even-parity':
          return config.plural ? 'Paridades Pares' : 'Paridade Par';
        case 'move':
          return config.plural ? 'Jogadas' : 'Jogada';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para ações
    keywords.getAction = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^action-/, '' ) ) {
        case 'abort':
          return 'Abortar';
        case 'absorb':
          return 'Absorver';
        case 'guard':
          return 'Amparar';
        case 'attack':
          return 'Atacar';
        case 'activate':
          return 'Ativar';
        case 'banish':
          return 'Banir';
        case 'channel':
          return 'Canalizar';
        case 'end':
          return 'Concluir';
        case 'consume':
          return 'Consumir';
        case 'summon':
          return 'Convocar';
        case 'defend':
          return 'Defender';
        case 'unsummon':
          return 'Desconvocar';
        case 'disengage':
          return 'Desengajar';
        case 'unequip':
          return 'Desequipar';
        case 'energize':
          return 'Energizar';
        case 'engage':
          return 'Engajar';
        case 'equip':
          return 'Equipar';
        case 'expel':
          return 'Expelir';
        case 'ether-blast':
          return 'Explosão de Éter';
        case 'intercept':
          return 'Interceptar';
        case 'handle':
          return 'Manejar';
        case 'deploy':
          return 'Mobilizar';
        case 'move':
          return 'Mover';
        case 'relocate':
          return 'Posicionar';
        case 'possess':
          return 'Possuir';
        case 'prepare':
          return 'Preparar';
        case 'mana-bolt':
          return 'Raio de Mana';
        case 'tear':
          return 'Rasgar';
        case 'retreat':
          return 'Recuar';
        case 'suspend':
          return 'Suspender';
        case 'sustain':
          return 'Sustentar';
        case 'transfer':
          return 'Transferir';
        case 'swap':
          return 'Trocar';
        case 'leech':
          return 'Vampirizar';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para condições
    keywords.getCondition = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^condition-/, '' ) ) {
        case 'forwarded':
          return 'Adiantado';
        case 'incapacitated':
          return 'Indefeso';
        case 'stunned':
          return 'Atordoado';
        case 'levitated':
          return 'Levitado';
        case 'guarded':
          return 'Amparado';
        case 'possessed':
          return 'Possuído';
        case 'projected':
          return 'Projetado';
        case 'ignited':
          return 'Incendiado';
        case 'strengthened':
          return 'Fortalecido';
        case 'weakened':
          return 'Enfraquecido';
        case 'enlivened':
          return 'Alentado';
        case 'unlivened':
          return 'Desalentado';
        case 'emboldened':
          return 'Encorajado';
        case 'enraged':
          return 'Enfurecido';
        case 'diseased':
          return 'Enfermo';
        case 'awed':
          return 'Admirável';
        case 'feared':
          return 'Temível';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para telas
    keywords.getScreen = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'loading-screen':
        case 'LoadingScreen':
          return 'Tela de Carregamento';
        case 'opening-screen':
        case 'OpeningScreen':
          return 'Tela de Abertura';
        case 'match-screen':
        case 'MatchScreen':
          return 'Tela de Partidas';
        case 'deck-viewing-screen':
        case 'DeckViewingScreen':
          return 'Tela de Visualização de Baralhos';
        case 'deck-building-screen':
        case 'DeckBuildingScreen':
          return 'Tela de Montagem de Baralhos';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para componentes de molduras
    keywords.getFrameComponent = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'cardData':
          return 'Dados';
        case 'cardStats':
          return 'Estatísticas';
        case 'cardCommand':
          return 'Comando';
        case 'cardEffects':
          return 'Efeitos';
        case 'cardFlavor':
          return 'Temática';
        case 'mainDeck':
          return 'Baralho Principal';
        case 'sideDeck':
          return 'Baralho Coadjuvante';
        case 'validation':
          return 'Validação';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para valores não categorizados
    keywords.getMisc = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'primitiveType':
        case 'primitiveTypes':
          return config.plural ? 'Tipos Primitivos' : 'Tipo Primitivo';
        case 'trait':
        case 'traits':
          return config.plural ? 'Características' : 'Característica';
        case 'attachment':
        case 'attachments':
          return config.plural ? 'Vinculados' : 'Vinculado';
        case 'weightCount':
          return 'Contagem de Peso';
        case 'hands':
          return 'Mãos';
        case 'inUse':
          return 'Em Uso';
        case 'inDisuse':
          return 'Em Desuso';
        case 'usageSegment':
          return 'Segmento de Uso';
        case 'target':
          return config.plural ? 'Alvos' : 'Alvo';
        case 'fatePoints':
          return config.plural ? 'Pontos de Destino' : 'Ponto de Destino';
        default:
          return m.languages.defaultFallback;
      }
    }
  }
} )();
