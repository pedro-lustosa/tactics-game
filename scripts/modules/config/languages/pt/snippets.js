// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'pt' ) return;

  // Identificadores
  const snippets = m.languages.snippets = {};

  // Propriedades iniciais
  defineProperties: {
    // Baralhos pré-montados
    snippets.getPrebuiltDeck = function ( deckName ) {
      // Identifica baralho a ser retornado a partir do nome passado
      switch( deckName.replace( /-main-deck$/, '' ) ) {
        case 'starter-deck-1':
          return 'Baralho Inicial 1';
        case 'starter-deck-2':
          return 'Baralho Inicial 2';
        default:
          return '';
      }
    }

    // Ações
    snippets.getUserInterfaceAction = function ( actionName ) {
      // Identifica ação a ser retornada a partir do nome passado
      switch( actionName ) {
        case 'acknowledge':
          return 'OK';
        case 'agree':
          return 'Sim';
        case 'disagree':
          return 'Não';
        case 'submit':
          return 'Submeter';
        case 'save':
          return 'Salvar';
        case 'edit':
          return 'Editar';
        case 'copy':
          return 'Copiar';
        case 'rename':
          return 'Renomear';
        case 'remove':
          return 'Remover';
        case 'cancel':
          return 'Cancelar';
        case 'back':
          return 'Voltar';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Ação atual que ente está acionando
    snippets.getBeingCurrentAction = function ( action ) {
      // Caso não tenha sido passada uma ação, retorna um texto vazio
      if( !action ) return '';

      // Captura adendo sobre previsão de [conclusão/sustentação] da ação alvo
      var finishmentText = getFinishmentText();

      // Identifica texto a ser retornado em função do nome da ação passada
      switch( action.name ) {
        case 'action-attack':
          return `Atacando ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        case 'action-engage':
          return `Engajando em ${ action.target.name }` + finishmentText;
        case 'action-disengage':
          return `Desengajando para ${ action.target.name.replace( /^.+-/, '' ) }` + finishmentText;
        case 'action-retreat':
          return 'Recuando' + finishmentText;
        case 'action-channel': {
          // Identificadores
          let [ spell, spellTarget ] = [ action.target, action.target.target ],
              targetSnippet = snippets.getSpellTargetSnippet( spellTarget, action.committer ).trim();

          // Para caso exista um trecho sobre o alvo
          if( targetSnippet ) {
            // Adiciona espaço ao trecho do alvo
            targetSnippet = ' ' + targetSnippet;

            // Caso alvo não seja o acionante e esteja em uma casa, adiciona essa informação
            if( spellTarget != action.committer && spellTarget.slot ) targetSnippet += ` ${ snippets.getCardPosition( spellTarget ) }`;
          }

          // Retorna texto
          return `Canalizando ${ spell.content.designation.title }` + targetSnippet + finishmentText;
        }
        case 'action-sustain': {
          // Identificadores
          let [ spell, spellTarget ] = [ action.target, action.target.target ],
              targetSnippet = snippets.getSpellTargetSnippet( spellTarget, action.committer ).trim();

          // Para caso exista um trecho sobre o alvo
          if( targetSnippet ) {
            // Adiciona espaço ao trecho do alvo
            targetSnippet = ' ' + targetSnippet;

            // Caso alvo não seja o acionante e esteja em uma casa, adiciona essa informação
            if( spellTarget != action.committer && spellTarget.slot ) targetSnippet += ` ${ snippets.getCardPosition( spellTarget ) }`;
          }

          // Retorna texto
          return `Sustentando ${ spell.content.designation.title }` + targetSnippet + finishmentText;
        }
        case 'action-expel':
          return `Expelindo ${ snippets.getCardPosition( action.committer ) }` + finishmentText;
        case 'action-leech':
          return `Vampirizando ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        case 'action-possess':
          return `Possuindo ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        default:
          return '';
      }

      // Retorna texto sobre previsão de [conclusão/sustentação] da ação alvo
      function getFinishmentText() {
        // Identificadores
        var actionPlayer = action.committer.getRelationships().controller,
            matchFlow = m.GameMatch.current.flow,
            currentParityIndex = matchFlow.segment.children.indexOf( matchFlow.parity ),
            parityModifier = 0,
            isToSustain = [ 'action-sustain', 'action-expel', 'action-leech' ].includes( action.name );

        // Caso paridade do acionante ainda esteja por vir no segmento atual, incrementa modificador de paridade
        if( currentParityIndex < matchFlow.segment.children.findIndex( parityStage => actionPlayer.parity == parityStage.parity ) ) parityModifier++;

        // Captura segmento de [conclusão/sustentação] da ação alvo
        let targetSegment = matchFlow.period.getStageFrom( matchFlow.segment, action.flowCost - action.committer.actions.spentFlowCost - parityModifier );

        // Captura paridade de [conclusão/sustentação] da ação alvo
        let targetParity = targetSegment?.getChild( `${ actionPlayer.parity }-parity` ) ?? null;

        // Retorna texto sobre previsão de [conclusão/sustentação] da ação
        return targetSegment ?
          `\n${ isToSustain ? 'Sustentação' : 'Conclusão' } na ${ targetParity.displayName.toLowerCase() } do ${ targetSegment.displayName.toLowerCase() }` :
          `\nSem previsão de ${ isToSustain ? 'sustentação' : 'conclusão' }`;
      }
    }

    // Tipo de ataque livre
    snippets.getFreeAttack = function ( freeAttackName ) {
      // Identifica texto a ser retornado em função do nome do ataque livre passado
      switch( freeAttackName.slice( 0, freeAttackName.search( /-free-attack-/ ) ) ) {
        case 'melee-range':
          return 'Ataque livre provocado pelo alcance próximo.';
        case 'rapture':
          return `Ataque livre provocado pela manobra "${ m.languages.keywords.getManeuver( 'rapture' ).toLowerCase() }".`;
        case 'engage-action':
          return `Ataque livre provocado pela ação "${ m.languages.keywords.getAction( 'engage' ).toLowerCase() }".`;
        case 'raging-surge':
          return `Ataque livre provocado pela característica "${ m.languages.cards.getTrait( 'raging-surge' ).title }".`;
        case 'weapon-pike':
          return 'Ataque livre provocado pelo efeito de um pique.';
        default:
          return 'Ataque livre.';
      }
    }

    // Tipo de parada de destino
    snippets.getFateBreak = function ( fateBreakName ) {
      // Identifica texto a ser retornado em função do nome do ataque livre passado
      switch( fateBreakName.slice( 0, fateBreakName.search( /-fate-break-/ ) ) ) {
        case 'prevent-actions':
          return 'Parada de destino relativa ao interrompimento de ações por conta do dano infligido.';
        case 'raging-surge':
          return `Parada de destino relativa à característica "${ m.languages.cards.getTrait( 'raging-surge' ).title }".`;
        case 'action-possess':
          return `Parada de destino relativa à ação "${ m.languages.keywords.getAction( 'possess' ).toLowerCase() }".`;
        case 'nimbleness-potion':
          return 'Parada de destino relativa ao apetrecho "Poção da Ligeireza".';
        case 'spell-poke':
          return 'Parada de destino relativa à magia "Cutucar".';
        case 'spell-tear':
          return 'Parada de destino relativa à magia "Rasgar".';
        case 'spell-suppress':
          return 'Parada de destino relativa à magia "Suprimir".';
        case 'spell-consume':
          return 'Parada de destino relativa à magia "Consumir".';
        case 'spell-pain-twist':
          return 'Parada de destino relativa à magia "Dor Encaminhada".';
        case 'spell-thunder-strike':
          return 'Parada de destino relativa à magia "Relâmpago".';
        case 'spell-fireball':
          return 'Parada de destino relativa à magia "Bola de Fogo".';
        case 'spell-fissure':
          return 'Parada de destino relativa à magia "Fissura".';
        case 'spell-the-beast':
          return 'Parada de destino relativa à magia "A Fera".';
        case 'condition-diseased':
          return `Parada de destino relativa à condição "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }".`;
        default:
          return 'Parada de destino.';
      }
    }

    // Origem de pontos de destino embutidos
    snippets.getEmbeddedFatePointsSource = function ( sourceName ) {
      // Identifica texto a ser retornado em função do nome da origem do ponto de destino
      switch( sourceName ) {
        case 'blessed-fate':
          return '"Destino Agraciado"';
        case 'ring-of-protection':
          return '"Anel da Proteção"';
        case 'spell-the-champion':
          return '"O Campeão"';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Trecho para alvos de magias
    snippets.getSpellTargetSnippet = function ( target, committer ) {
      // Retorna trecho tendo como referência identidade do alvo passado
      return target == committer ? ' em si' :
             target instanceof m.Item ? ` em ${ target.content.designation.title }` :
             target instanceof m.Card ? ` em ${ target.content.designation.full }` :
             target instanceof m.CardSlot ? ` em ${ target.name }` : '';
    }

    // Nome de uma jogada
    snippets.getMoveName = function ( moveId, source, cardId ) {
      // Identificadores
      var text = m.languages.keywords.getFlow( 'move' );

      // Acrescenta ao nome da jogada seu número em relação a outras jogadas da mesma fonte
      if( moveId != 1 ) text += ' ' + moveId;

      // Adiciona ente a que jogada se vincula
      text += ` de ${ source.content.designation.full }`;

      // Adiciona ao nome da jogada número do ente em relação a outros entes do mesmo jogador
      if( cardId != 1 ) text += ' ' + cardId;

      // Retorna texto
      return text;
    }

    // Tipo de Dano
    snippets.getDamageType = ( damageType ) => `Dano de ${ m.languages.keywords.getDamage( damageType ) }`;

    // Tipo de Resistência
    snippets.getResistanceType = ( resistanceType ) => `Resistência contra ${ m.languages.keywords.getDamage( resistanceType ) }`;

    // Algo em
    snippets.getSomethingIn = ( location, thing = '' ) => thing ? `${ thing } em ${ location }` : `Em ${ location }`;

    // Algo de
    snippets.getSomethingOf = ( source, thing = '' ) => thing ? `${ thing } de ${ source }` : `De ${ source }`;

    // Algo de
    snippets.getSomethingFrom = ( source, thing = '' ) => thing ? `${ thing } de ${ source }` : `De ${ source }`;

    // Casa em que carta está
    snippets.getCardPosition = function ( card, slot, isShort = false ) {
      // Identificadores
      var positionText = '',
          targetSlot = slot ?? card.slot ?? card.summoner?.slot,
          slotName = targetSlot?.name ?? '';

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

      // Caso carta não esteja em nenhuma casa, encerra função
      if( !slotName ) return positionText;

      // Caso nome abreviado da casa deva ser retornado, converte para ele seu nome completo
      if( isShort ) slotName = slotName.replace( /^.+-/g, '' );

      // Adiciona ao texto a ser retornado casa em que carta está
      positionText += `Em ${ slotName }`;

      // Caso casa seja uma zona de engajamento, adiciona em que lado carta está
      if( targetSlot instanceof m.EngagementZone )
        positionText += `; lado ${ m.languages.keywords.getParity( card.getRelationships().owner.parity ).toLowerCase() }`;

      // Coloca entre parênteses conteúdo a ser retornado
      positionText = positionText.replace( /(.*)/, ' ($1)' );

      // Retorna texto com localização da carta
      return positionText;
    }

    // Retorna controlador do ente passado
    snippets.getController = function ( being ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( m.GameMatch.current );
      }

      // Identificadores
      var playerUser = m.GamePlayer.current?.user,
          beingController = being.getRelationships().controller,
          text = 'Controlado ';

      // Acrescenta ao texto informação sobre controlador do ente
      text += m.GameMatch.current.isSimulation || playerUser != beingController.user ?
        `pelo ${ m.languages.keywords.getParity( beingController.parity ).toLowerCase() }` : 'por ti';

      // Retorna texto
      return text;
    }

    // Versão
    snippets.version = 'Versão';

    // Ordem
    snippets.order = 'Ordem';

    // Mínimo
    snippets.min = 'Mínimo';

    // Máximo
    snippets.max = 'Máximo';

    // Base
    snippets.base = 'Base';

    // Fonte
    snippets.source = 'Fonte';

    // Embutido
    snippets.embedded = 'Embutido';

    // Dinâmico
    snippets.dynamic = 'Dinâmico';

    // Moedas
    snippets.coins = 'Moedas';

    // Custo de fluxo gasto
    snippets.spentFlowCost = 'Custo de fluxo gasto';

    // Indicação de se ente está adiantável
    snippets.isForwardable = 'Está adiantável';

    // Magia escolhida
    snippets.chosenSpell = 'Magia escolhida';

    // Usuário global
    snippets.globalUser = 'Visitante';
  }
} )();
