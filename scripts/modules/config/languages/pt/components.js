// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'pt' ) return;

  // Identificadores
  const components = m.languages.components = {};

  // Propriedades iniciais
  defineProperties: {
    // Para a tela de carregamento
    components.loadingScreen = function () {
      return {
        inProgress: 'Carregando...',
        start: 'Iniciar'
      };
    }

    // Para o menu da tela de abertura
    components.openingMenu = function () {
      return {
        learn: 'Aprender',
        manual: 'Manual',
        videos: 'Vídeos',
        play: 'Jogar',
        simulate: 'Simular',
        multiplayer: 'Multijogador',
        decks: 'Baralhos',
        view: 'Visualizar',
        build: 'Montar',
        config: 'Configurações',
        audio: 'Áudio',
        shortcuts: 'Atalhos',
        languages: 'Idiomas',
        back: 'Voltar'
      };
    }

    // Para a tela de partidas
    components.matchScreen = function () {
      return {
        // Texto para ícone do menu de configurações
        configIcon: 'Configurações',
        // Texto para ícone de jogadas
        getRostrumMoves: function ( parity ) {
          return m.languages.keywords.getFlow( 'move', { plural: true } ) + ' ' + m.languages.keywords.getParity( parity, { plural: true } ).toLowerCase() +
                 ' de fontes controláveis';
        },
        // Texto sobre quantidade de jogadas não desenvolvidas
        getUndevelopedMoves: function ( moves ) {
          return 'Jogadas não desenvolvidas:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto sobre quantidade de jogadas desenvolvidas
        getDevelopedMoves: function ( moves ) {
          return 'Jogadas desenvolvidas:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto sobre quantidade de jogadas concluídas
        getFinishedMoves: function ( moves ) {
          return 'Jogadas concluídas:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto para ícones de quantidade de cartas por estado de atividade e uso
        getCardsByActivityDescription: function ( activity, usage, parity ) {
          return 'Cartas ' + m.languages.keywords.getActivity( activity, { plural: true, feminine: true } ).toLowerCase() + ( usage ? ` e ${ usage }` : '' ) +
                 ' do baralho ' + m.languages.keywords.getParity( parity ).toLowerCase();
        },
        // Texto sobre quantidade de cartas por estado de atividade e uso
        getCardsByActivityValue: function ( cards ) {
          // Identificadores
          var hoverText = '';

          // Itera por propriedades alvo
          for( let key of [ 'beings', 'items', 'spells' ] ) {
            // Identificadores
            let targetCards = cards[ key ];

            // Caso não haja cartas alvo, avança para próxima iteração
            if( !targetCards.length ) continue;

            // Atualiza texto suspenso com quantidade de cartas alvo passada
            hoverText += `${ targetCards.length } ${ m.languages.keywords.getPrimitiveType( key, { plural: targetCards.length != 1 } ) }:\n`;

            // Lista designações completas das cartas alvo
            hoverText += targetCards.reduce( ( accumulator, current ) => accumulator += `  ${ current.content.designation.full }\n`, '' );

            // Adiciona divisão entre categorias de cartas alvo
            hoverText += '\n';
          }

          // Retorna texto suspenso
          return hoverText;
        },
        // Texto para ícones de quantidade de cartas por estado de preparo
        getCardsByReadinessDescription: function ( readiness, parity ) {
          return 'Cartas ' + m.languages.keywords.getParity( parity, { plural: true } ).toLowerCase() + ' ' +
                 m.languages.keywords.getReadiness( readiness, { plural: true, feminine: true } ).toLowerCase() + ' e controláveis';
        },
        // Texto sobre quantidade de cartas por estado de preparo
        getCardsByReadinessValue: function ( cards ) {
          return cards.reduce(
            ( accumulator, current ) => accumulator += `${ current.content.designation.full }${ current.slot ? ` (Em ${ current.slot.name })` : '' }\n`, ''
          );
        },
        // Texto para ícone de pontos de destino de dado jogador
        getFatePointsDescription: function ( parity ) {
          return m.languages.keywords.getMisc( 'fatePoints' )[0] + m.languages.keywords.getMisc( 'fatePoints', { plural: true } ).slice( 1 ).toLowerCase() +
                 ' ' + m.languages.keywords.getParity( parity, { plural: true } ).toLowerCase();
        },
        // Texto sobre quantidade de pontos de destino de dado jogador
        getFatePointsValue: function ( currentFate ) {
          return `Pontos transitivos: ${ currentFate.transitive }\n` +
                 `Pontos intransitivos: ${ currentFate.intransitive }`;
        }
      };
    }

    // Para o menu de configurações da tela de partidas
    components.matchConfigMenu = function () {
      return {
        concede: 'Desistir',
        exit: 'Sair'
      };
    }

    // Para paradas de combate
    components.combatBreak = function () {
      return {
        // Título
        caption: 'Parada de Combate',
        // Para o texto suspenso do ícone da seção de ataque
        getAttackHoverText: function ( attack, maneuverSource ) {
          // Identificadores
          var hoverText = '';

          // Adiciona texto sobre ataque
          hoverText += `Ataque "${ m.languages.keywords.getManeuver( attack.name ) }"`;

          // Adiciona arma de origem do ataque, se existente
          if( maneuverSource.source ) hoverText += `, via ${ maneuverSource.source.content.designation.title }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso do ícone da seção de defesa
        getDefenseHoverText: function ( defense, maneuverSource ) {
          // Identificadores
          var hoverText = '';

          // Adiciona texto sobre defesa
          hoverText += `Defesa "${ m.languages.keywords.getManeuver( defense.name ) }"`;

          // Adiciona arma de origem da defesa, se existente
          if( maneuverSource.source ) hoverText += `, via ${ maneuverSource.source.content.designation.title }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para os textos suspensos dos ícones das seções de modificadores de combate
        getCombatModifiersHoverText: function ( modifiers ) {
          // Identificadores
          var hoverText = '',
              isAttack = modifiers.type == 'attack';

          // Adiciona cabeçalho do texto
          hoverText += `Modificadores de ${ isAttack ? 'Ataque' : 'Defesa' }`;

          // Adiciona modificador da manobra, se existente
          if( modifiers.maneuver ) hoverText += `\n  ${ modifiers.maneuver } do modificador da manobra`;

          // Adiciona modificador de recuperação, se existente
          if( modifiers.recoveryBonus ) hoverText += `\n  ${ modifiers.recoveryBonus } da recuperação`;

          // Adiciona modificador da mão inábil, se existente
          if( modifiers.secondaryHandWeapon ) hoverText += `\n  ${ modifiers.secondaryHandWeapon } da mão inábil`;

          // Adiciona modificador da arma multimanual, se existente
          if( modifiers.multiHandedWeapon ) hoverText += `\n  ${ modifiers.multiHandedWeapon } da arma multimanual`;

          // Adiciona modificador de alcance efetivo, se existente
          if( modifiers.effectiveRange ) hoverText += `\n  ${ modifiers.effectiveRange } do alcance efetivo`;

          // Adiciona modificador de 'Esquivar' ante 'Arremessar', se existente
          if( modifiers.throwPenalty ) hoverText += `\n  ${ modifiers.throwPenalty } da manobra "${ m.languages.keywords.getManeuver( 'throw' ).toLowerCase() }"`;

          // Adiciona modificador de 'encorajado', se existente
          if( modifiers.emboldened ) hoverText += `\n  ${ modifiers.emboldened } da condição "${ m.languages.keywords.getCondition( 'emboldened' ).toLowerCase() }"`;

          // Adiciona modificador de 'enfurecido', se existente
          if( modifiers.enraged ) hoverText += `\n  ${ modifiers.enraged } da condição "${ m.languages.keywords.getCondition( 'enraged' ).toLowerCase() }"`;

          // Adiciona modificador de 'enfermo', se existente
          if( modifiers.diseased ) hoverText += `\n  ${ modifiers.diseased } da condição "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }"`;

          // Adiciona modificador do efeito de 'Poção da Ligeireza', se existente
          if( modifiers.nimblenessPotion ) hoverText += `\n  ${ modifiers.nimblenessPotion } do efeito de "Poção da Ligeireza"`;

          // Adiciona modificador do efeito de Ixohch, se existente
          if( modifiers.ixohchEffect ) hoverText += `\n  ${ modifiers.ixohchEffect } do efeito de Ixohch`;

          // Adiciona modificador de agilidade, se existente
          if( modifiers.agility ) hoverText += `\n  ${ modifiers.agility } da ${ m.languages.keywords.getAttribute( 'agility' ).toLowerCase() }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso dos textos de pontos disponíveis
        availablePointsHoverText: 'Pontos disponíveis',
        // Para o texto suspenso dos textos de pontos atribuídos
        assignedPointsHoverText: 'Pontos atribuídos',
        // Para o texto suspenso dos dados de ataque
        getDieAttackHoverText: function ( attack ) {
          // Identificadores
          var hoverText = '';

          // Adiciona nome do ataque ao texto suspenso
          hoverText += m.languages.keywords.getManeuver( attack.name );

          // Adiciona danos do ataque ao texto suspenso
          for( let damageName in attack.damage )
            hoverText += `\n  ${ m.languages.snippets.getDamageType( damageName ) }: ${ attack.damage[ damageName ] }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso dos dados de defesa
        getDieDefenseHoverText: function ( defense ) {
          return m.languages.keywords.getManeuver( defense.name );
        },
        // Para o texto suspenso dos dados de bônus de ataque
        dieBonusHoverText: 'Bônus',
        // Para o texto suspenso dos dados de penalidades de ataque
        diePenaltyHoverText: 'Penalidade',
        // Para o texto suspenso dos textos de dano total
        getTotalDamageHoverText: function ( damage, isInvalid = false, being ) {
          // Identificadores
          var textEnding = '\nClica para ver sua fórmula';

          // Mensagem para caso seja previsto infligimento direto do dano
          if( !isInvalid )
            return `Dano total de ${ m.languages.keywords.getDamage( damage.name ).toLowerCase() } a ser infligido` + textEnding;

          // Para caso dano seja de fogo
          if( damage instanceof m.DamageFire ) {
            // Identificadores
            let beingSize = being.content.typeset.size,
                sizeScore = beingSize == 'large' ? 3 : beingSize == 'medium' ? 2 : 1,
                messageBody = sizeScore == 1 ? 'Este dano será infligido ao fim deste segmento' :
                  `Este dano será rateado e infligido ao longo de ${ sizeScore } segmentos`;

            // Retorna mensagem própria do dano de fogo invalidado
            return messageBody + textEnding;
          }

          // Para caso dano seja de éter
          if( damage instanceof m.DamageEther ) {
            // Identificadores
            let points = being.content.stats.attributes.current.health;

            // Retorna mensagem própria do dano de éter invalidado
            return `Este dano precisa ter no mínimo ${ points } ponto${ points == 1 ? '' : 's' } para ser infligido` + textEnding;
          }
        },
        // Para o texto suspenso da seta de retorno ao dano total
        shortDamageArrowHoverText: 'Voltar ao dano resumido',
        // Para o texto suspenso sobre os pontos de ataque
        attackPointsHoverText: 'Pontos de ataque a serem infligidos',
        // Para o texto suspenso do dano bruto
        getRawDamageHoverText: function ( damage ) {
          return `Dano bruto de ${ m.languages.keywords.getDamage( damage.name ).toLowerCase() } por ponto de ataque`;
        },
        // Para o texto suspenso da resistência natural
        naturalResistanceHoverText: 'Resistência natural do defensor',
        // Para o texto suspenso da resistência do traje primário
        getPrimaryGarmentResistanceHoverText: function ( garmentCoverage ) {
          return 'Resistência do traje primário do defensor' + ( garmentCoverage ?
                 `\nApenas aplicada ${ garmentCoverage == 1 ? 'ao primeiro ponto' : 'aos ' + garmentCoverage + ' primeiros pontos' } de ataque` : ''
          );
        },
        // Para o texto suspenso da resistência do traje secundário
        getSecondaryGarmentResistanceHoverText: function ( garmentCoverage ) {
          return 'Resistência do traje secundário do defensor' + ( garmentCoverage ?
                 `\nApenas aplicada ${ garmentCoverage == 1 ? 'ao primeiro ponto' : 'aos ' + garmentCoverage + ' primeiros pontos' } de ataque` : ''
          );
        },
        // Para o texto suspenso sobre a penetração
        penetrationHoverText: 'Penetração do ataque',
        // Para o texto suspenso sobre o modificador de destino
        fateModifierHoverText: 'Modificador de destino',
        // Para o texto suspenso sobre o dano resultante
        resultingDamageHoverText: 'Dano resultante',
        // Para o texto suspenso dos ícones de pontos de destino
        getFatePointHoverText: function ( parity, isAssigned = false, isEmbedded = false ) {
          return m.languages.keywords.getMisc( 'fatePoints' )[ 0 ] + m.languages.keywords.getMisc( 'fatePoints' ).slice( 1 ).toLowerCase() + ' ' +
                 m.languages.keywords.getParity( parity ).toLowerCase() + ': ' + ( isAssigned ? 'atribuído' : 'não atribuído' ) +
                 ( isAssigned && isEmbedded ? ' (embutido)' : '' );
        }
      };
    }

    // Para as listas de cartas
    components.cardsList = function () {
      return {
        price: 'Preço',
        mainDeckCoins: 'Moedas disponíveis para o baralho principal'
      };
    }

    // Para a tela de visualização de baralhos
    components.deckViewingScreen = function () {
      return {
        deckTable: {
          caption: 'Tabela de Baralhos Montados',
          name: 'Nome',
          created: 'Data de Criação',
          modified: 'Data de Modificação',
          matches: 'Partidas',
          wins: 'Vitórias',
          draws: 'Empates',
          losses: 'Derrotas',
          disconnects: 'Desconexões'
        },
        moveUpArrow: 'Subir fileira.',
        moveDownArrow: 'Descer fileira.',
        backArrow: 'Voltar à tela de abertura.'
      };
    }

    // Para as molduras de cartas e casas
    components.componentFrame = function ( sourceComponent ) {
      // Identificadores
      var frameData = {},
          getFrameData = sourceComponent instanceof m.Being ? getBeingData :
                         sourceComponent instanceof m.Item ? getItemData :
                         sourceComponent instanceof m.Spell ? getSpellData :
                         sourceComponent instanceof m.CardSlot ? getSlotData : null;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Card, m.CardSlot ].some( constructor => sourceComponent instanceof constructor ) );

      // Popula dados da moldura em função do tipo do componente alvo
      getFrameData();

      // Adiciona à moldura texto sobre inativação do efeito da carta
      frameData.disabledEffect = 'Efeito desabilitado';

      // Adiciona à moldura operação para popular dados dos textos suspensos sobre cartas na seção de vinculados
      frameData.getCardAttachment = function ( attachment ) {
        // Identificadores
        var attachmentText = '',
            directCardAttachments = Object.values( attachment.attachments ).flat().filter( subAttachment => subAttachment instanceof m.Card );

        // Inicia linha de texto suspenso com título de alvo
        attachmentText += attachment.content.designation.title;

        // Adiciona descritor de obra-prima, se aplicável
        if( attachment.content.typeset.grade == 'masterpiece' ) attachmentText += ` ${ m.languages.keywords.getItemGrade( 'masterpiece' ) }`;

        // Caso vinculado não esteja em uma casa e não seja um equipamento embutido, encerra função
        if( !attachment.slot && !attachment.isEmbedded ) return attachmentText;

        // Inclui adendo sobre origem de alvo
        attachmentText += attachment.slot ? ` (Em ${ attachment.slot.name }` : ' (Embutido';

        // Inclui cartas vinculadas a alvo, se existentes
        if( directCardAttachments.length ) {
          // Inicia frase sobre vinculados
          attachmentText += '; com ';

          // Acrescenta nome de vinculados
          attachmentText += directCardAttachments.map( subAttachment =>
            subAttachment.content.designation.title.toLowerCase() +
            ( subAttachment.slot ? ` em ${ subAttachment.slot.name }` : subAttachment.isEmbedded ? ' embutido' : '' )
          ).join( ', ' );
        }

        // Encerra adendo e texto sobre vinculado atual
        return attachmentText += ')\n';
      }

      // Caso componente seja uma carta, adiciona à moldura trecho que indica autoria da ilustração do componente de temática
      if( sourceComponent instanceof m.Card ) frameData.artist = `Carta ilustrada por ${ m.data.creators.getImageArtist( sourceComponent.content.image ) }`;

      // Retorna dados da moldura
      return frameData;

      // Retorna dados de entes
      function getBeingData() {
        // Identificadores
        var cardTypeset = sourceComponent.content.typeset;

        // Texto suspenso para condições
        frameData.conditions = 'Condições do Ente';

        // Texto suspenso para seção de ambientes
        frameData.environment = sourceComponent.summoner ?
          `Convocado por ${ sourceComponent.summoner.content.designation.full } em ${ sourceComponent.summoner.slot.name }` +
          ( sourceComponent.owner ? ' (Pertence)' : '' ) : '';

        // Texto suspenso para pontos gastos de manobras
        frameData.spentPoints = 'Pontos gastos';

        // Texto suspenso para seção conjunta de canalização e vinculados
        frameData.channelingAndAttachments =
          `${ m.languages.keywords.getStat( 'channeling' ) } e ${ m.languages.keywords.getMisc( 'attachment', { plural: true } ) }`;

        // Texto suspenso para valores do mana de sendas
        frameData.manaQuantity = {
          path: 'Mana da Senda', total: 'Mana Total'
        };

        // Encerra execução da função caso ente não seja um humanoide
        if( !( sourceComponent instanceof m.Humanoid ) ) return;

        // Texto suspenso para cabeçalho de raça
        frameData.traitCaption =
          `${ m.languages.keywords.getMisc( 'trait', { plural: true } ) } de ${ m.languages.keywords.getRace( cardTypeset.race, { plural: true } ) }`;

        // Texto suspenso para ocupação de mãos
        frameData.hands = {
          primary: 'Mão hábil com ',
          secondary: 'Mão inábil com ',
          all: 'Ambas as mãos com '
        };
      }

      // Retorna dados de equipamentos
      function getItemData() {
        // Define texto suspenso para seção de alvo
        frameData.target = ( function () {
          // Para caso componente não seja vinculável
          if( !sourceComponent.isToAttach ) return 'Invinculável';

          // Para caso componente não tenha um vinculante
          if( !sourceComponent.attacher ) return 'Vinculável';

          // Captura dados sobre dono e vinculante do equipamento
          let text = '',
              [ sourceOwner, sourceAttacher ] = [ sourceComponent.owner, sourceComponent.attacher ],
              ownerSlotSnippet = sourceOwner.slot ? ` em ${ sourceOwner.slot.name }` : '',
              attacherSlotSnippet = sourceAttacher.slot ? ` em ${ sourceAttacher.slot.name }` : ownerSlotSnippet;

          // Acrescenta informação sobre dono do equipamento
          text += `Pertencido a ${ sourceOwner.content.designation.full }${ ownerSlotSnippet }`;

          // Caso o vinculante seja diferente do dono, acrescenta informação sobre o vinculante
          if( sourceAttacher != sourceOwner ) text += `\nVinculado a ${ sourceAttacher.content.designation.title }${ attacherSlotSnippet }`;

          // Retorna texto
          return text;
        } )();
      }

      // Retorna dados de magias
      function getSpellData() {
        // Define texto suspenso para seção de alvo
        frameData.target = ( function () {
          // Identificadores
          var text = '',
              sourceOwner = sourceComponent.owner,
              ownerSlotSnippet = sourceOwner?.slot ? ` em ${ sourceOwner.slot.name }` : '';

          // Caso magia tenha um dono, acrescenta essa informação
          if( sourceOwner ) text += `Pertencido a ${ sourceOwner.content.designation.full }${ ownerSlotSnippet }\n`;

          // Caso componente não seja vinculável, adiciona essa informação e retorna texto
          if( !sourceComponent.isToAttach ) return text += 'Invinculável';

          // Caso componente não tenha um vinculante, adiciona essa informação e retorna texto
          if( !sourceComponent.attacher ) return text += 'Vinculável';

          // Caso vinculante do componente seja uma casa, adiciona essa informação e retorna texto
          if( sourceComponent.attacher instanceof m.CardSlot ) return text += `Vinculado a ${ sourceComponent.attacher.name }`;

          // Captura dados do vinculante
          let sourceAttacher = sourceComponent.attacher,
              attacherName = sourceAttacher.content.designation[ sourceAttacher instanceof m.Being ? 'full' : 'title' ],
              cardSlot = sourceAttacher.slot ?? sourceAttacher.owner?.slot ?? null,
              attacherSlotSnippet = cardSlot ? ` em ${ cardSlot.name }` : '';

          // Inclui informação sobre vinculante, e retorna texto
          return text += `Vinculado a ${ attacherName }${ attacherSlotSnippet }`;
        } )();

        // Texto suspenso para seção de polaridade
        frameData.oppositeSpell = sourceComponent.oppositeSpell ? `Magia oposta: ${ new sourceComponent.oppositeSpell().content.designation.title }` : '';

        // Textos suspensos para seção do custo de fluxo
        frameData.flowCosts = {
          channeling: 'Segmentos para canalização', sustaining: 'Segmentos para sustentação'
        };

        // Textos suspensos para seção do custo de mana
        frameData.manaCosts = {
          minChanneling: 'Custo mínimo para canalização', maxChanneling: 'Custo máximo para canalização',
          sustainingCost: 'Custo para sustentação', maxUsageCost: 'Custo máximo de uso'
        };
      }

      // Retorna dados de casas
      function getSlotData() {
        // Definição do título da moldura
        frameData.title = `${ m.languages.keywords.getAttachability( 'CardSlot' ) } ${ sourceComponent.name }`;
      }
    }

    // Para as molduras de baralhos
    components.deckFrame = function () {
      return {
        getSubtitle: ( minCards, maxCards ) => minCards ? `escolhe entre ${ minCards } e ${ maxCards } cartas` : `escolhe até ${ maxCards } cartas`,
        getCreditsSource: function ( deckCredits ) {
          // Identificadores
          var creditsText = '';

          // Adiciona créditos de equipamentos, se aplicável
          if( deckCredits.items ) creditsText += `  ${ m.languages.keywords.getPrimitiveType( 'item', { plural: true } ) }: ${ deckCredits.items }\n`;

          // Adiciona créditos de magias, quando aplicável
          for( let path of [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ] )
            if( deckCredits.spells[ path ] ) creditsText += `  ${ m.languages.keywords.getPath( path ) }: ${ deckCredits.spells[ path ] }\n`;

          // Caso haja ao menos uma fonte de crédito, adiciona cabeçalho
          if( creditsText ) creditsText = 'Créditos:\n' + creditsText;

          // Retorna texto sobre créditos
          return creditsText.trimEnd();
        },
        availableCoins: 'Moedas disponíveis',
        totalCoins: 'Moedas totais',
        spentCoins: 'Moedas gastas',
        spentCredits: 'Créditos gastos',
        invalidDeck: 'Baralho inválido',
        validation: {
          getIsValidText: ( isDeckSet ) => ( isDeckSet ? 'Conjunto de baralhos' : 'Baralho' ) + ' válido.',
          getMinDeckCards: function ( invalidEntries ) {
            // Identificadores
            var validationText = 'Falta incluir ao menos ';

            // Itera por entradas inválidas
            for( let entry of invalidEntries ) {
              // Identificadores
              let [ deckType, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ number == 1 ? 'carta' : 'cartas' } no ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } e `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / e $/, '.' );
          },
          getMaxDeckCards: function ( invalidEntries ) {
            // Identificadores
            var validationText = 'Há ';

            // Itera por entradas inválidas
            for( let entry of invalidEntries ) {
              // Identificadores
              let [ deckType, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText +=
                `${ number } ${ number == 1 ? 'carta excedente' : 'cartas excedentes' } no ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } e `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / e $/, '.' );
          },
          getMaxCoinsSpent: function ( invalidKeys ) {
            // Identificadores
            var validationText = 'A quantidade de moedas gastas ';

            // Itera por chaves inválidas
            for( let key of invalidKeys ) {
              // Identificadores
              let deckType = key;

              // Ajusta texto de validação
              validationText += `com o ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } e `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / e $/, ' é maior que a máxima permitida.' );
          },
          getMaxSameCards: () => 'Uma ou mais cartas excederam seu limite de disponibilidade.',
          getCommandCards: function ( validationType, invalidData ) {
            // Identificadores
            var validationText = validationType == 'minCommandCards' ? 'Falta incluir no baralho principal ao menos: ' : 'Falta remover: ',
                { race = {}, roleType = {}, experienceLevel = {}, grade = {}, constraints = [] } = invalidData[ validationType ],
                [ raceEntries, roleEntries, experienceEntries, gradeEntries ] = [
                  Object.entries( race ), Object.entries( roleType ), Object.entries( experienceLevel ), Object.entries( grade )
                ];

            // Itera por tipos primitivos inválidos
            for( let primitiveType of [ 'creature', 'item', 'spell' ] ) {
              // Identificadores
              let number = invalidData[ validationType ][ primitiveType ];

              // Filtra tipos primitivos válidos
              if( !number ) continue;

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getPrimitiveType( primitiveType, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de raças inválidas
            for( let entry of raceEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getRace( target, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de tipos de atuação inválidas
            for( let entry of roleEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getRoleType( target, { person: true, plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de níveis de experiência inválidos
            for( let entry of experienceEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText +=
                `${ number } ${ m.languages.keywords.getPrimitiveType( 'creature', { plural: number != 1 } ).toLowerCase() } de ` +
                `${ m.languages.keywords.getExperienceLevel( target ).toLowerCase() }; `;
            }

            // Itera por entradas de categorias de equipamento inválidas
            for( let entry of gradeEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getItemGrade( target, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por superescopos com restrições especiais inválidas
            for( let superScope of constraints ) {
              // Ajusta texto de validação
              validationText +=
                `cartas de ${ m.languages.keywords.getPrimitiveType( superScope, { plural: true } ).toLowerCase() } que transgridem restrições especiais; `;
            }

            // Remove pontuação final, e retorna texto resultante
            return validationText = validationText.replace( /; $/, '.' );
          }
        }
      };
    }

    // Para a moldura de registro de partidas
    components.logFrame = function () {
      return {
        getTitle: function ( stageName, stageNumber ) {
          // Identificadores
          var stageText = m.languages.keywords.getFlow( stageName.replace( /(^|-)\d+?(-|$)/g, '' ) ),
              preposition = [ 'Turno', 'Período', 'Bloco', 'Segmento' ].some( flowUnit => stageText.startsWith( flowUnit ) ) ? 'do' : 'da';

          // Caso estágio seja uma etapa e seu número seja menor que 2, ajusta-o para que seja omitido
          if( stageName.includes( '-step-' ) && stageNumber < 2 ) stageNumber = 0;

          // Retorna texto do cabeçalho do registro de eventos
          return `Eventos ${ preposition } ${ stageText }${ stageNumber ? ' ' + stageNumber : '' }`;
        },
        beginArrow: 'Ver estágio com eventos mais anteriores',
        previousArrow: 'Ver eventos anteriores',
        nextArrow: 'Ver eventos posteriores',
        endArrow: 'Ver estágio com eventos mais posteriores'
      };
    }

    // Para a moldura de escolha do comandante
    components.commanderFrame = function () {
      return {
        title: 'Escolhe o Comandante',
        subtitle: 'com um duplo clique'
      };
    }

    // Para a modal de áudio
    components.audioModal = function () {
      return {
        soundTracks: 'Volume da Música',
        soundEffects: 'Volume dos Efeitos'
      };
    }

    // Tabela de atalhos de uma tela
    components.shortcutsTable = function ( screenName ) {
      return {
        caption: `Atalhos da ${ m.languages.keywords.getScreen( screenName ) }`,
        operation: 'Operação',
        trigger: 'Gatilho',
        combinations: 'Combinações',
        body: this.screenShortcuts()[ screenName ] ?? []
      };
    }

    // Descrição dos atalhos de telas
    components.screenShortcuts = function () {
      return {
        // Atalhos da tela de partidas
        'match-screen': [
          {
            description: 'Liberar fluxo',
            trigger: 'Tecla "Enter"',
            combinations: []
          }, {
            description: 'Suspender fluxo',
            trigger: 'Tecla "Espaço"',
            combinations: []
          }, {
            description: 'Mostrar estágio acima do atual',
            trigger: 'Tecla "Seta para cima"',
            combinations: []
          }, {
            description: 'Mostrar estágio abaixo do atual',
            trigger: 'Tecla "Seta para baixo"',
            combinations: []
          }, {
            description: 'Manipular moldura esquerda',
            trigger: 'Tecla "1"',
            combinations: [
              { description: 'Alternar visibilidade' },
              { description: 'Exibir registros', ctrlKey: true, altKey: true },
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura direita',
            trigger: 'Tecla "2"',
            combinations: [
              { description: 'Alternar visibilidade' },
              { description: 'Exibir registros', ctrlKey: true, altKey: true },
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura central superior',
            trigger: 'Tecla "3"',
            combinations: [
              { description: 'Alternar visibilidade' },
              { description: 'Exibir registros', ctrlKey: true, altKey: true },
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura central inferior',
            trigger: 'Tecla "4"',
            combinations: [
              { description: 'Alternar visibilidade' },
              { description: 'Exibir registros', ctrlKey: true, altKey: true },
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Filtrar cartas por atividade',
            trigger: 'Tecla "5" (Pressionada)',
            combinations: [
              { description: 'Suspensas' },
              { description: 'Ativas', ctrlKey: true },
              { description: 'Afastadas', shiftKey: true },
              { description: 'Findadas', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Filtrar entes por preparo',
            trigger: 'Tecla "6" (Pressionada)',
            combinations: [
              { description: 'Despreparados' },
              { description: 'Preparados', ctrlKey: true },
              { description: 'Ocupados', shiftKey: true },
              { description: 'Esgotados', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Mostrar apenas cartas relacionadas a em que cursor pairar',
            trigger: 'Tecla "7" (Pressionada)',
            combinations: [
              { description: 'Pertencidas' },
              { description: 'Vinculadas', ctrlKey: true },
              { description: 'Em uso', shiftKey: true }
            ]
          }, {
            description: 'Alternar visibilidade da designação de cartas',
            trigger: 'Tecla "9"',
            combinations: [
              { description: 'Nos róis' },
              { description: 'No campo', ctrlKey: true },
              { description: 'Nas molduras', shiftKey: true },
              { description: 'Em todos os componentes', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular configurações da tela',
            trigger: 'Tecla "0"',
            combinations: [
              { description: 'Exibir atalhos' },
              { description: 'Alternar visibilidade de seções de atividade e preparo', shiftKey: true },
              { description: 'Alternar modo informativo', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Sair da tela',
            trigger: 'Tecla "Esc"',
            combinations: []
          }
        ],
        // Atalhos da tela de montagem de baralhos
        'deck-building-screen': [
          {
            description: 'Manipular barra de filtros visível',
            trigger: 'Clique primário',
            combinations: [
              { description: 'Alternar filtro clicado' },
              { description: 'Ativar filtros do conjunto do clicado que não ele', ctrlKey: true }
            ]
          }, {
            description: 'Navegar por lista de cartas visível',
            trigger: 'Tecla "Seta para esquerda"',
            combinations: [
              { description: 'Mostrar 1 carta anterior' },
              { description: 'Mostrar 6 cartas anteriores', ctrlKey: true }
            ]
          }, {
            description: 'Navegar por lista de cartas visível',
            trigger: 'Tecla "Seta para direita"',
            combinations: [
              { description: 'Mostrar 1 carta posterior' },
              { description: 'Mostrar 6 cartas posteriores', ctrlKey: true }
            ]
          }, {
            description: 'Manipular moldura esquerda de cartas',
            trigger: 'Tecla "1"',
            combinations: [
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura de baralhos',
            trigger: 'Tecla "2"',
            combinations: [
              { description: 'Alternar baralho coadjuvante', ctrlKey: true },
              { description: 'Alternar validação', shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura direita de cartas',
            trigger: 'Tecla "3"',
            combinations: [
              { description: 'Alternar foco', shiftKey: true, altKey: true },
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Limpar filtros da barra visível',
            trigger: 'Tecla "4"',
            combinations: [
              { description: 'Todos' },
              { description: 'Primeiro conjunto', ctrlKey: true },
              { description: 'Segundo conjunto', shiftKey: true },
              { description: 'Terceiro conjunto', altKey: true }
            ]
          }, {
            description: 'Alternar visibilidade da designação de cartas',
            trigger: 'Tecla "9"',
            combinations: [
              { description: 'Nas listas', ctrlKey: true },
              { description: 'Nas molduras', shiftKey: true },
              { description: 'Em todos os componentes', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular configurações da tela',
            trigger: 'Tecla "0"',
            combinations: [
              { description: 'Exibir atalhos' },
              { description: 'Alternar layout', ctrlKey: true },
              { description: 'Alternar modo informativo', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Sair da tela',
            trigger: 'Tecla "Esc"',
            combinations: []
          }
        ],
        // Atalhos da tela de visualização de baralhos
        'deck-viewing-screen': [
          {
            description: 'Modificar baralho selecionado',
            trigger: 'Tecla "Enter"',
            combinations: [
              { description: 'Editar' },
              { description: 'Copiar', ctrlKey: true }
            ]
          }, {
            description: 'Modificar baralho selecionado',
            trigger: 'Tecla "Delete"',
            combinations: [
              { description: 'Remover' },
              { description: 'Renomear', ctrlKey: true }
            ]
          }, {
            description: 'Manipular fileiras da tabela',
            trigger: 'Tecla "Seta para cima"',
            combinations: [
              { description: 'Selecionar fileira acima da selecionada' },
              { description: 'Mover fileira selecionada para cima', ctrlKey: true }
            ]
          }, {
            description: 'Manipular fileiras da tabela',
            trigger: 'Tecla "Seta para baixo"',
            combinations: [
              { description: 'Selecionar fileira abaixo da selecionada' },
              { description: 'Mover fileira selecionada para baixo', ctrlKey: true }
            ]
          }, {
            description: 'Manipular primeira fileira da tabela',
            trigger: 'Tecla "Home"',
            combinations: [
              { description: 'Selecionar fileira' },
              { description: 'Mover fileira selecionada para essa posição', ctrlKey: true }
            ]
          }, {
            description: 'Manipular última fileira da tabela',
            trigger: 'Tecla "End"',
            combinations: [
              { description: 'Selecionar fileira' },
              { description: 'Mover fileira selecionada para essa posição', ctrlKey: true }
            ]
          }, {
            description: 'Manipular moldura de cartas',
            trigger: 'Tecla "1"',
            combinations: [
              { description: 'Alternar estatísticas', ctrlKey: true },
              { description: 'Alternar efeitos', shiftKey: true },
              { description: 'Alternar comando', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipular moldura de baralhos',
            trigger: 'Tecla "2"',
            combinations: [
              { description: 'Alternar baralho coadjuvante', ctrlKey: true }
            ]
          }, {
            description: 'Alternar visibilidade da designação de cartas',
            trigger: 'Tecla "9"',
            combinations: [
              { description: 'Nas molduras', shiftKey: true }
            ]
          }, {
            description: 'Manipular configurações da tela',
            trigger: 'Tecla "0"',
            combinations: [
              { description: 'Exibir atalhos' },
              { description: 'Alternar modo informativo', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Sair da tela',
            trigger: 'Tecla "Esc"',
            combinations: []
          }
        ]
      };
    }
  }
} )();
