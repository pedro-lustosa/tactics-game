// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'pt' ) return;

  // Identificadores
  const notices = m.languages.notices = {};

  // Para textos diversos em modais
  notices.getModalText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'only-users-allowed':
        return 'Deves estar logado para realizar essa operação.';
      case 'multiplayer-not-available':
        return 'O modo multijogador está indisponível no momento.';
      case 'select-defense-for-attack': {
        // Identificadores
        let attackName = m.languages.keywords.getManeuver( config.attack.name ),
            attackPoints = config.maneuverSource.currentPoints,
            sourceName = config.maneuverSource.source?.content.designation.title,
            attackerName = config.attacker.content.designation.full;

        // Retorna texto para a modal
        return `Seleciona uma defesa para "${ attackName }" (${ attackPoints }P), por ${ attackerName + ( sourceName ? ' via ' + sourceName : '' ) }.`;
      };
      case 'confirm-free-attack-by-defender': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full,
            defenderName = config.defender.content.designation.full,
            attackName = m.languages.keywords.getManeuver( config.attack.name ),
            attackPoints = config.maneuverSource.currentPoints,
            sourceName = config.maneuverSource.source?.content.designation.title;

        // Retorna texto para a modal
        return `${ attackerName } atacará ${ defenderName } com "${ attackName }" (${ attackPoints }P)${ sourceName ? ', via ' + sourceName : '' }. Desejas antes lhe desferir um ataque livre?`;
      };
      case 'confirm-free-attack-by-engage-action': {
        // Identificadores
        let engagerName = config.engager.content.designation.full,
            attackerName = config.attacker.content.designation.full,
            cardSlotName = config.slot.name;

        // Retorna texto para a modal
        return `${ engagerName } acaba de engajar em ${ cardSlotName }. Desejas lhe desferir um ataque livre com ${ attackerName }?`;
      };
      case 'confirm-free-attack-by-raging-surge-trait': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full,
            traitName = m.languages.cards.getTrait( 'raging-surge' ).title;

        // Retorna texto para a modal
        return `${ attackerName } teve o efeito de "${ traitName }" ativado. Desejas desferir 1 ataque livre com ele?`;
      };
      case 'confirm-free-attack-by-pike': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full;

        // Retorna texto para a modal
        return `Desejas desferir 1 ataque livre com ${ attackerName }, via a manobra "perfurar" de seu pique em uso?`;
      };
      case 'awed-free-attack-addendum': {
        // Identificadores
        let { target, requiredPoints } = config,
            targetName = target.content.designation.full,
            fatePointsSnippet = requiredPoints == 1 ? '1 ponto de destino será gasto' : `${ requiredPoints } pontos de destino serão gastos`;

        // Retorna texto para a modal
        return ` (${ fatePointsSnippet } devido à condição "admirável" em ${ targetName })`;
      };
      case 'confirm-fate-point-spent-for-awed-target': {
        // Identificadores
        let { target, requiredPoints } = config,
            targetName = target.content.designation.full,
            fatePointsSnippet = requiredPoints == 1 ? '1 ponto de destino será gasto' : `${ requiredPoints } pontos de destino serão gastos`;

        // Retorna texto para a modal
        return `Devido à condição "admirável" em ${ targetName }, ${ fatePointsSnippet } para que esse ente seja o alvo desta ação. Desejas proceder?`;
      };
      case 'confirm-fate-point-spent-by-feared': {
        // Identificadores
        let { slot, requiredPoints } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? 'de um ente do oponente' : 'do ente do oponente',
            fatePointsSnippet = requiredPoints == 1 ? '1 ponto de destino será gasto' : `${ requiredPoints } pontos de destino serão gastos`;

        // Retorna texto para a modal
        return `Devido à condição "temível" ${ beingSnippet } em ${ slotName }, ${ fatePointsSnippet } para que esta ação seja acionada. Desejas proceder?`;
      };
      case 'select-maneuver-for-free-attack':
        return 'Seleciona uma manobra para o ataque livre.';
      case 'select-embedded-item':
        return 'Seleciona o equipamento embutido alvo.';
      case 'select-item-attacher':
        return 'Seleciona o vinculante para o equipamento alvo.';
      case 'select-spell-path-for-spell-effect':
        return 'Seleciona a senda a sofrer o efeito da magia.';
      case 'assign-mana-cost': {
        // Identificadores
        let { polarityBurden, isEnergy } = config,
            modalText = `Atribui ${ isEnergy ? 'a energia' : 'o mana' } para a canalização da magia`;

        // Adiciona adendo sobre ônus de polaridade, se aplicável
        if( polarityBurden ) modalText += ', desconsiderando o ônus de polaridade';

        // Ajusta final do texto
        modalText += '.';

        // Retorna texto
        return modalText;
      };
      case 'assign-mana-cost-placeholder':
        return config.isPersistent ?
          `Apenas múltiplos de ${ config.minMana }, até ${ config.maxMana }` : `Apenas dígitos, entre ${ config.minMana } e ${ config.maxMana }`;
      case 'confirm-mana-cost-after-polarity-burden':
        return `Serão gastos ${ config.manaPoints } pontos de ${ config.isEnergy ? 'energia' : 'mana' } incluindo o ônus de polaridade. Desejas proceder?`;
      case 'confirm-mana-cost-with-adjunct-spells': {
        // Identificadores
        let { manaPoints, adjunctSpells, isEnergy } = config,
            resource = isEnergy ? 'energia' : 'mana de Metoth',
            modalText = '';

        // Adiciona ao texto da modal pontos de mana que serão gastos
        modalText += manaPoints == 1 ? `${ manaPoints } ponto de ${ resource } será gasto ` : `${ manaPoints } pontos de ${ resource } serão gastos `;

        // Adiciona ao texto da modal origem do uso dos pontos de mana
        modalText += adjunctSpells.length == 1 ?
          `pela magia "${ adjunctSpells[ 0 ].content.designation.title }".` : `ao todo pelas magias modificando a magia principal.`;

        // Adiciona ao texto solicitação de confirmação
        modalText += ' Desejas proceder?';

        // Retorna texto
        return modalText;
      }
      case 'assign-mana-to-absorb':
        return 'Atribui o mana a ser absorvido.';
      case 'assign-mana-to-transfer':
        return 'Atribui o mana a ser transferido.';
      case 'assign-mana-placeholder':
        return `Até ${ config.manaPoints } ${ config.manaPoints == 1 ? 'ponto' : 'pontos' }`;
      case 'set-number-of-attacks':
        return `Define quantos ataques serão gerados.`;
      case 'number-of-attacks-placeholder':
        return `Apenas dígitos, entre 1 e ${ config.maxNumber }.`;
      case 'confirm-spell-mana-cost':
        return `Ao ser usada, esta magia gastará ${ config.manaPoints } ponto${ config.manaPoints == 1 ? '' : 's' } de ${ config.isEnergy ? 'energia' : 'mana' }. Desejas proceder?`;
      case 'confirm-spell-mana-cost-and-inflicted-damage':
        return `Ao ser usada, esta magia gastará ${ config.manaPoints } ponto${ config.manaPoints == 1 ? '' : 's' } de ${ config.isEnergy ? 'energia' : 'mana' } e transferirá até ${ config.damagePoints } ponto${ config.damagePoints == 1 ? '' : 's' } de dano. Desejas proceder?`;
      case 'change-target-of-fireball-spell':
        return 'Desejas que o ataque desta magia mire apenas 1 ente físico?';
      case 'notify-round-winner-self':
        return 'Parabéns. Esta rodada foi concluída, e tu és o vencedor.';
      case 'notify-round-winner-opponent':
        return 'Esta rodada foi concluída, e teu oponente é o vencedor.';
      case 'notify-round-winner-other':
        return `Esta rodada foi concluída. Seu vencedor é o ${ m.languages.keywords.getParity( config.winner.parity ).toLowerCase() }.`;
      case 'notify-round-draw':
        return 'Esta rodada foi concluída, tendo terminado em empate.';
      case 'leave-match':
        return 'Realmente desejas sair da partida atual?';
      case 'concede-match':
        return 'Ao desistires, esta partida será concluída, e seu resultado será computado como derrota para ti e vitória para teu oponente. Realmente desejas realizar essa ação?';
      case 'win-by-concession-player':
        return 'Teu oponente acaba de desistir desta partida. Por conta disso, tu és o vencedor.';
      case 'win-by-concession-other':
        return `O ${ m.languages.keywords.getParity( config.loser.parity ).toLowerCase() } acaba de desistir desta partida. Por conta disso, o ${ m.languages.keywords.getParity( config.winner.parity ).toLowerCase() } é o vencedor.`;
      case 'name-deck-set':
        return `Nomeia este ${ config.isDeckSet ? 'conjunto de baralhos' : 'baralho' }.`;
      case 'name-deck-set-placeholder':
        return 'Apenas letras latinas e dígitos';
      case 'alert-deck-set-editing':
        return 'Ao salvar a edição de um baralho, suas estatísticas sobre partidas são zeradas. Caso não desejes isso, deves cancelar esta edição e editar uma cópia desse baralho.';
      case 'confirm-deck-set-saving': {
        // Identificadores
        let { decksSet, isNewDeck } = config,
            confirmationText = 'Ainda tens ';

        // Itera por conjunto de baralhos
        for( let deck of decksSet ) {
          // Identificadores
          let [ cardsNumber, coinsNumber ] = [ deck.constructor.maxCards - deck.cards.length, deck.coins.max - deck.coins.spent ];

          // Filtra baralhos que não atendem aos critérios de confirmação
          if( deck.cards.length >= deck.constructor.maxCards || coinsNumber < 50 ) continue;

          // Ajusta texto de confirmação
          confirmationText +=
            `${ coinsNumber } moedas no ${ m.languages.keywords.getDeck( deck.type ).toLowerCase() } para gastar com ` +
            `${ ( cardsNumber == 1 ? '' : 'até ' ) + cardsNumber + ( cardsNumber == 1 ? ' carta' : ' cartas' ) } e `;
        }

        // Ajusta final do texto de confirmação
        confirmationText = confirmationText.replace( / e $/, `. Realmente desejas concluir a ${ isNewDeck ? 'montagem' : 'edição' }?` );

        // Retorna texto de confirmação
        return confirmationText;
      };
      case 'deck-set-saved': {
        // Identificadores
        let modalText = config.isDeckSet ? 'Conjunto de baralhos ' : 'Baralho ';

        // Acrescenta corpo do texto a ser retornado
        modalText += config.isNewDeck ?
          'salvo. Doravante podes o encontrar na tela de criação de partidas e na tela de visualização de baralhos.' : 'editado.';

        // Retorna texto
        return modalText;
      };
      case 'not-enough-valid-decks-for-simulate':
        return 'Deves ter ao menos 2 baralhos válidos para que possas acessar o modo de simulação.';
      case 'not-enough-valid-decks-for-multiplayer':
        return 'Deves ter ao menos 1 baralho válido para que possas acessar o modo de multijogador.';
      case 'no-access-while-searching-for-match':
        return 'Não podes acessar outras telas enquanto estiveres procurando uma partida. Para não procurares mais uma partida, recarrega a tela.';
      case 'deck-set-limit-reached':
        return 'Alcançaste o limite de baralhos montados. Para montares um baralho novo, deves primeiro remover um baralho atual teu.';
      case 'confirm-deck-set-removal':
        return 'Realmente desejas remover esse baralho de teus baralhos montados?';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para textos sobre invalidações diversas
  notices.getInvalidationText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'invalid-user':
        return 'Usuário inválido.';
      case 'only-1-match-at-a-time':
        return 'Cada usuário pode [buscar/estar em] apenas 1 partida por vez.';
      case 'flow-must-be-redeployment':
        return 'O fluxo deve estar no período da remobilização.';
      case 'allocation-step-invalid-flow-cost':
        return 'O custo de fluxo da ação somado ao já gasto nesta etapa pelo acionante não deve ultrapassar 5.';
      case 'combat-period-invalid-flow-cost':
        return 'O custo de fluxo da ação deve ser menor que os segmentos restantes do período atual.';
      case 'invalid-target':
        return 'O alvo escolhido é inválido.';
      case 'target-must-be-player-component':
        return 'O alvo deve ser um componente teu.';
      case 'target-must-not-be-opponent-component':
        return 'O alvo não deve ser um componente do oponente.';
      case 'target-must-not-be-committer':
        return 'O alvo não deve ser o acionante.';
      case 'target-in-player-grid':
        return 'O alvo deve estar em tua grade do campo.';
      case 'target-must-be-card-slot':
        return 'O alvo deve ser uma casa.';
      case 'target-must-not-be-committer-card-slot':
        return 'O alvo não deve ser a casa do acionante.';
      case 'target-must-be-field-grid-slot':
        return 'O alvo deve ser uma casa de uma grade do campo.';
      case 'target-must-be-unoccupied-card-slot':
        return 'O alvo deve ser uma casa desocupada.';
      case 'target-must-be-from-committer-field-grid':
        return 'O alvo deve ser uma casa da grade do campo de acionante.';
      case 'target-must-be-engagement-zone':
        return 'O alvo deve ser uma zona de engajamento.';
      case 'target-must-be-adjacent-engagement-zone':
        return 'O alvo deve ser uma zona de engajamento adjacente.';
      case 'target-must-be-empty-engagement-zone':
        return 'O alvo deve ser uma zona de engajamento vazia.';
      case 'target-must-be-occupied-engagement-zone':
        return 'O alvo deve ser uma zona de engajamento ocupada.';
      case 'engagement-zone-target-must-be-occupied':
        return 'Sendo uma zona de engajamento, o alvo deve ter 1 ou mais ocupantes.';
      case 'engagement-zone-target-must-not-be-with-astral-being':
        return 'Sendo o alvo uma zona de engajamento, o acionante não deve ser um ente astral.';
      case 'target-must-not-have-max-player-occupants':
        return 'O alvo não deve ter atingido o limite de ocupantes teus.';
      case 'target-must-be-card':
        return 'O alvo deve ser uma carta.';
      case 'target-must-be-from-player':
        return 'O alvo deve ser uma carta tua.';
      case 'target-must-be-from-opponent':
        return 'O alvo deve ser uma carta do oponente.';
      case 'target-must-be-from-side-deck':
        return 'O alvo deve ser uma carta de teu baralho coadjuvante.';
      case 'target-must-be-from-committer-deck':
        return 'O alvo deve ser uma carta do baralho de acionante.';
      case 'target-must-be-active':
        return 'O alvo deve estar ativo.';
      case 'target-must-be-suspended':
        return 'O alvo deve estar suspenso.';
      case 'target-must-be-active-or-suspended':
        return 'O alvo deve estar ativo ou suspenso.';
      case 'target-must-be-in-use':
        return 'O alvo deve estar em uso.';
      case 'target-must-be-in-disuse':
        return 'O alvo não deve estar em uso.';
      case 'target-must-be-exhausted':
        return 'O alvo deve estar esgotado.';
      case 'target-must-not-be-exhausted':
        return 'O alvo não deve estar esgotado.';
      case 'target-must-have-moves':
        return 'O alvo deve ter jogadas.';
      case 'target-must-have-finished-all-moves':
        return 'O alvo deve ter concluído todas as suas jogadas.';
      case 'target-effect-must-be-enabled':
        return 'O efeito do alvo deve estar habilitado.';
      case 'target-usage-must-be-dynamic':
        return 'O uso do alvo deve ser dinâmico.';
      case 'target-must-not-be-commander':
        return 'O alvo não deve ser um comandante.';
      case 'target-must-be-being':
        return 'O alvo deve ser um ente.';
      case 'target-must-be-biotic-being':
        return 'O alvo deve ser um ente biótico.';
      case 'target-must-be-humanoid':
        return 'O alvo deve ser um humanoide.';
      case 'target-must-be-spirit-or-demon':
        return 'O alvo deve ser um espírito ou demônio.';
      case 'target-must-be-demon-or-spell':
        return 'O alvo deve ser um demônio ou uma magia.';
      case 'target-energy-must-be-lower-than-base':
        return 'A energia atual do alvo deve estar abaixo de sua energia base.';
      case 'target-must-have-less-will-than-committer':
        return 'A vontade do acionante deve ser maior que a do alvo.';
      case 'target-must-have-less-stamina-than-committer-energy':
        return 'A energia do acionante deve ser maior que o dobro do vigor do alvo.';
      case 'target-must-have-less-experience-than-committer-power':
        return 'O poder do acionante deve ser maior que o nível de experiência do alvo.';
      case 'being-is-already-guarded':
        return 'Esse ente já está sendo amparado.';
      case 'target-must-not-be-guarded':
        return 'O alvo não deve estar sendo amparado.';
      case 'target-must-be-owned-by-committer':
        return 'O alvo deve ser um pertence do acionante.';
      case 'target-attacher-must-be-in-use':
        return 'O vinculante do alvo deve estar em uso.';
      case 'target-must-be-item-or-committer':
        return 'O alvo deve ser um equipamento ou o acionante.';
      case 'target-must-be-item':
        return 'O alvo deve ser um equipamento.';
      case 'item-invalid-target-type':
        return `Esse equipamento apenas pode mirar ${ config.targetType }.`;
      case 'no-valid-embedded-item':
        return 'O acionante não tem equipamentos embutidos válidos.';
      case 'target-must-be-external-item':
        return 'O alvo deve ser um equipamento externo.';
      case 'item-size-must-match':
        return 'O equipamento deve ter o mesmo tamanho do vinculante, ou não ter tamanho.';
      case 'item-over-weight-count':
        return 'A pontuação de peso do equipamento somada à contagem de peso do vinculante não deve ultrapassar 5.';
      case 'garment-weight-must-be-distinct':
        return 'O traje deve ter um peso distinto daquele do traje já vinculado ao vinculante.';
      case 'implement-with-same-title-already-attached':
        return 'Um apetrecho com o mesmo título não deve estar já vinculado ao vinculante.';
      case 'consumable-already-used-by-being':
        return `Um consumível com o mesmo título já foi usado por este ente ${ m.GameMatch.current?.flow.period ? 'no período' : 'na etapa' } atual.`;
      case 'item-already-attached':
        return 'O equipamento não deve já estar vinculado ao vinculante.';
      case 'item-over-max-attachments':
        return 'O vinculante já alcançou seu limite de vinculados para a equipagem do equipamento.';
      case 'no-valid-attachers':
        return 'Não há vinculantes válidos para o equipamento alvo.';
      case 'target-must-not-be-forwardable':
        return 'O ente alvo não deve estar adiantável.';
      case 'attacher-invalid-attack':
        return `O vinculante deve ter o ataque "${ config.attack }".`;
      case 'shoot-invalid-damage':
        return 'Ataque "disparar" do vinculante deve ter um dano principal de perfuração.';
      case 'attacher-must-have-slash-or-pierce-damage':
        return 'O vinculante deve ter ao menos um ataque com dano de corte ou de perfuração.';
      case 'target-must-be-spell':
        return 'O alvo deve ser uma magia.';
      case 'if-spell-target-must-be-negative':
        return 'Sendo uma magia, o alvo deve ter polaridade negativa.';
      case 'spell-invalid-target-type':
        return `Essa magia apenas pode mirar ${ config.targetType }.`;
      case 'target-must-be-spell-suspended-or-owned':
        return 'O alvo deve ser uma magia suspensa ou pertencida ao acionante.';
      case 'allocation-step-spell-targets':
        return 'O alvo deve ser uma magia persistente ou adjunta.';
      case 'spell-already-in-use':
        return 'A magia não deve já estar em uso.';
      case 'target-must-be-in-channeler-paths':
        return 'O alvo deve ser de uma senda na que o acionante tenha ao menos 1 nível.';
      case 'target-mana-cost-must-be-paid': {
        // Identificadores
        let { isEnergy, isWithAdjunct, polarityBurden } = config,
            resource = isEnergy ? 'energia' : 'mana',
            noticeText = `O acionante deve ter ${ resource } suficiente para pagar ao menos o custo mínimo `;

        // Adiciona identificação da magia alvo
        noticeText += isWithAdjunct ? 'da magia principal' : 'do alvo';

        // Adiciona adendo sobre ônus de polaridade, se aplicável
        if( polarityBurden ) noticeText += ', incluindo o ônus de polaridade';

        // Ajusta final do texto
        noticeText += '.';

        // Retorna texto
        return noticeText;
      };
      case 'adjunct-spells-mana-cost-must-be-paid': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return isEnergy ?
          'O custo mínimo de energia requerido pelas magias adjuntas não deve ser maior que o mana base de Metoth do acionante.' :
          'O acionante deve ter mana de Metoth suficiente para pagar ao menos o custo mínimo requerido pelas magias adjuntas.';
      }
      case 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-metoth-mana': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return isEnergy ?
          'A soma do custo de energia requerido pelas magias da cadeia não deve ser maior que o mana base de Metoth do acionante.' :
          'O acionante deve ter mana de Metoth suficiente para pagar o custo da magia principal da cadeia somado ao custo das magias adjuntas.';
      }
      case 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-total-mana': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return `O acionante deve ter ${ isEnergy ? 'energia' : 'mana total' } suficiente para pagar o custo da magia principal da cadeia somado ao custo das magias adjuntas.`;
      }
      case 'target-invalid-by-antimagic-essence':
        return `Sendo o alvo da magia um anão, a potência prevista da magia deve poder ser ao menos ${ config.minPotency }.`;
      case 'spell-over-max-channeling-flow-cost':
        return 'O custo de fluxo de canalização atual da magia não deve ultrapassar 5.';
      case 'spell-invalid-range':
        return `O alvo deve ser abarcado pelo alcance da magia.`;
      case 'spell-target-must-be-an-attacher':
        return 'O alvo deve ser capaz de ter magias vinculadas.';
      case 'spell-over-max-attachments':
        return 'O vinculante já alcançou seu limite de magias vinculadas.';
      case 'must-be-powthe-committer':
        return 'O acionante deve ter ao menos 1 nível em Powth.';
      case 'must-be-powthe-summoner':
        return 'O convocante deve ter ao menos 1 nível em Powth.';
      case 'spell-wrong-voth-polarity-committer':
        return `O acionante deve ter polaridade ${ config.polarity } em Voth.`;
      case 'spell-wrong-voth-polarity-summoner':
        return `O convocante deve ter polaridade ${ config.polarity } em Voth.`;
      case 'voth-greater-min-level-summoner':
        return `O nível em Voth do convocante não deve ser menor que ${ config.level }.`;
      case 'demons-above-summoner-limit':
        return 'O convocante não deve exceder o limite de demônios seus atualmente ativos.';
      case 'angels-above-summoner-limit':
        return 'O convocante já tem um anjo ativo.';
      case 'salamanders-above-summoner-limit':
        return 'O convocante não deve exceder o limite de salamandras suas atualmente ativas.';
      case 'leech-must-target-unleeched-target':
        return 'O alvo não deve estar sendo mirado por outra ação "vampirizar".';
      case 'possess-must-target-unpossessed-target':
        return 'O alvo não deve estar sendo mirado por outra ação "possuir".';
      case 'must-not-be-with-possess-condition':
        return 'O alvo não deve estar com a condição "possuído".';
      case 'attack-invalid-target-type':
        return `Esse ataque apenas pode mirar ${ config.targetType }.`;
      case 'attack-invalid-range':
        return `O alvo deve ser abarcado pelo alcance do ataque.`;
      case 'being-cannot-be-direct-target-of-actions':
        return `Esse ente não pode ser um alvo direto de ações.`;
      case 'being-cannot-be-direct-target-of-attacks':
        return `Esse ente não pode ser um alvo direto de ataques.`;
      case 'being-cannot-be-direct-target-of-spells':
        return `Esse ente não pode ser um alvo direto de magias.`;
      case 'attack-rapture-too-high-health':
        return `Esse ente precisa estar com no máximo ${ config.maxHealth } ponto${ config.maxHealth == 1 ? '' : 's' } de vida para ser mirável por essa manobra.`;
      case 'attack-prevented-by-removed-item':
        return `O ataque de ${ config.attacker.content.designation.full } foi impedido devido ao meio de ataque ter sido removido.`;
      case 'attack-prevented-by-lack-of-points':
        return `O ataque de ${ config.attacker.content.designation.full } foi impedido devido à falta de pontos para seu acionamento.`;
      case 'target-occupant-able-to-commit-relocate':
        return 'O ocupante atual do alvo deve ser capaz de acionar "posicionar".';
      case 'target-occupant-able-to-do-distinct-move':
        return 'O ocupante atual do alvo deve ser capaz de desenvolver uma jogada distinta da do acionante.';
      case 'target-must-be-commiting-channel-or-sustain':
        return 'O alvo deve estar ocupado com "canalizar" ou "sustentar".';
      case 'target-must-be-persistent-or-permanent-spell':
        return 'O alvo deve ser uma magia persistente ou permanente.';
      case 'target-must-be-channeling-spell':
        return 'O alvo deve estar canalizando uma magia.';
      case 'target-must-be-channeling-spell-with-assigned-mana':
        return 'A magia sendo canalizada por alvo deve ter mana lhe atribuído.';
      case 'spell-must-be-able-to-have-no-less-potency-than-target':
        return 'O acionante não tem mana suficiente para que a potência desta magia não seja menor que a de alvo.';
      case 'spell-must-be-able-to-have-no-less-potency-than-half-target':
        return 'O acionante não tem mana suficiente para que a potência desta magia não seja menor que metade da de alvo.';
      case 'spell-must-be-able-to-have-no-less-potency-than-target-spell':
        return 'O acionante não tem mana suficiente para que a potência desta magia não seja menor que a prevista da magia de alvo.';
      case 'committer-must-have-paths-other-than-metoth':
        return 'O acionante precisa ter níveis em outras sendas além de Metoth.';
      case 'committer-must-have-paths-with-restorable-mana':
        return 'O acionante precisa ter sendas que não Metoth cujo mana atual esteja abaixo do base.';
      case 'owner-current-metoth-mana-must-be-less-than-base':
        return 'O mana atual de Metoth de acionante deve ser menor que seu mana base.';
      case 'target-must-be-tulpa-or-spirit':
        return 'O alvo deve ser uma tulpa ou um espírito.';
      case 'target-energy-must-not-be-greater-than-committer-metoth-mana':
        return 'A energia de alvo não deve ser maior que o mana base de Metoth de acionante.';
      case 'adjunct-spell-must-be-distinct':
        return 'A magia escolhida não pode ser uma já escolhida nesta cadeia de magias.';
      case 'committer-must-not-be-with-robe-of-the-magi':
        return 'O acionante não deve estar com o efeito de "Manto dos Arcanistas" ativo.';
      case 'target-must-have-channeling-mana-cost':
        return 'O alvo deve ter custo de mana para canalização.';
      case 'target-must-have-channeling-flow-cost':
        return 'O alvo deve ter custo de fluxo para canalização.';
      case 'target-must-have-r1-to-r4-range':
        return 'O alcance de alvo deve ser entre R1 e R4.';
      case 'target-must-not-have-max-strengthened-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de fortalecido.';
      case 'target-must-not-have-max-enlivened-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de alentado.';
      case 'target-must-not-have-max-weakened-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de enfraquecido.';
      case 'target-must-not-have-max-unlivened-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de desalentado.';
      case 'target-must-not-have-max-emboldened-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de encorajado.';
      case 'target-must-not-have-max-enraged-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de enfurecido.';
      case 'target-must-not-have-max-awed-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de admirável.';
      case 'target-must-not-have-max-feared-markers':
        return 'O alvo não deve ter a quantidade máxima de marcadores de temível.';
      case 'target-must-have-less-than-5-stamina':
        return 'O vigor de alvo deve ser menor que 5.';
      case 'damage-giver-must-have-suffered-physical-damage':
        return 'O alvo deve ter sofrido dano físico no combate atual.';
      case 'damage-giver-has-not-enough-damage-to-assign':
        return 'O dano físico atualmente sofrido pelo alvo deve ser [igual a/maior que] o dano físico a ser transferido.';
      case 'damage-receiver-must-be-different-from-damage-giver':
        return 'O alvo a receber o dano deve ser distinto do a transferir o dano.';
      case 'target-must-have-size':
        return 'O alvo deve ter tamanho.';
      case 'target-size-must-be-smaller-than-large':
        return 'O tamanho de alvo deve ser menor que grande.';
      case 'target-size-must-be-larger-than-small':
        return 'O tamanho de alvo deve ser maior que pequeno.';
      case 'target-must-have-less-attachments-with-size-for-spell':
        return 'O alvo deve ter menos vinculados embutidos com tamanho, para que o acionante possa pagar o mana mínimo requerido pela magia.';
      case 'target-must-have-conductivity':
        return 'O alvo deve ter condutividade.';
      case 'target-must-be-with-being-with-conductivity':
        return 'O alvo deve estar em uma casa com ao menos mais 1 ente que tenha condutividade.';
      case 'target-must-have-a-being-with-conductivity':
        return 'O alvo deve estar sendo ocupado por ao menos 1 ente com condutividade.';
      case 'target-must-have-a-physical-being':
        return 'O alvo deve estar sendo ocupado por ao menos 1 ente físico.';
      case 'target-must-not-be-with-air-cocoon':
        return 'O alvo não deve estar vinculado à magia "Casulo Eólico".';
      case 'target-must-not-be-with-pavise':
        return 'O alvo não deve estar com um pavês em uso.';
      case 'player-cannot-use-celestial-appeal':
        return 'Esta magia não pode ser usada por ti, por já teres usado uma magia negativa.';
      case 'committer-must-not-be-astral-being':
        return 'O acionante não deve ser um ente astral.';
      case 'committer-must-not-be-in-engagement-zone':
        return 'O acionante não deve estar em uma zona de engajamento.';
      case 'spirit-spell-cannot-have-extend':
        return 'Sendo um espírito, o acionante não pode canalizar esta magia em conjunto com "Estender".';
      case 'awed-prevents-attack-target':
        return 'O alvo não deve ter mais marcadores de "admirável" do que a vontade de acionante.';
      case 'feared-prevents-slot-choice': {
        // Identificadores
        let { slot } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? `Nenhum ente do oponente em ${ slotName }` : `O ente do oponente em ${ slotName } não`;

        // Retorna texto
        return `${ beingSnippet } deve ter mais marcadores de "temível" do que a vontade de acionante.`;
      }
      case 'no-fate-points-to-attack-awed-target':
        return 'Devido aos marcadores de "admirável" em alvo, para o mirar deves gastar uma quantidade de pontos de destino que não tens.';
      case 'no-fate-points-to-move-to-target-with-feared': {
        // Identificadores
        let { slot } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? 'de um ente do oponente' : 'do ente do oponente';

        // Retorna texto
        return `Devido aos marcadores de "temível" ${ beingSnippet } em ${ slotName }, para acionar esta ação deves gastar uma quantidade de pontos de destino que não tens.`;
      }
      case 'not-enough-mana-for-given-target':
        return `O acionante não tem mana suficiente para a escolha desse alvo, que requer ${ config.manaPoints } pontos.`;
      case 'spell-already-used-in-battle':
        return 'Esta magia não pode mais ser usada na batalha atual.';
      case 'spell-already-used-by-you-in-battle':
        return 'Esta magia não pode mais ser usada por ti na batalha atual.';
      case 'spell-only-usable-in-middle-battle':
        return 'Esta magia apenas pode ser usada nos turnos da batalha 3 ou 4.';
      case 'swap-over-max-coins': {
        // Identificadores
        let { invalidDeckTypes } = config,
            noticeText = 'Essa troca está acima do limite de moedas ';

        // Itera por baralhos cuja troca excederia o limite de moedas
        for( let deckType in invalidDeckTypes ) {
          // Identificadores
          let coinsDifference = invalidDeckTypes[ deckType ];

          // Acrescenta texto sobre invalidação do baralho alvo
          noticeText += `do ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } por ${ coinsDifference } moeda${ coinsDifference == 1 ? '' : 's' } e `;
        }

        // Remove conjunção final
        noticeText = noticeText.replace( / e $/, '.' );

        // Retorna texto
        return noticeText;
      };
      case 'swap-invalid-command':
        return 'Essa troca tornaria o comando do baralho principal inválido.';
      case 'swap-invalid':
        return 'Essa troca tornaria o conjunto de baralhos inválido.';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre escolhas
  notices.getChoiceText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'choose-target-card':
        return 'Escolhe a carta alvo, com um duplo clique.';
      case 'choose-target-card-slot':
        return 'Escolhe a casa alvo, com um duplo clique e pressionando "Ctrl".';
      case 'choose-target-being':
        return 'Escolhe o ente alvo, com um duplo clique.';
      case 'choose-target-item':
        return 'Escolhe o equipamento alvo, com um duplo clique.';
      case 'choose-target-spell':
        return 'Escolhe a magia alvo, com um duplo clique.';
      case 'choose-target-for-spell':
        return 'Escolhe o alvo da magia, com um duplo clique e, para casas, pressionando "Ctrl".';
      case 'choose-target-for-spell-chain':
        return 'Escolhe a próxima magia da cadeia de magias.';
      case 'choose-target-item-or-committer':
        return 'Escolhe o equipamento alvo ou – para equipamentos embutidos – o acionante, com um duplo clique.';
      case 'choose-new-owner':
        return 'Escolhe o novo dono do equipamento alvo, com um duplo clique.';
      case 'choose-card-slot-for-token':
        return 'Escolhe a casa da ficha, com um duplo clique e pressionando "Ctrl".';
      case 'choose-target-for-golem':
        return 'Escolhe a tropa a ser findada para a convocação, com um duplo clique.';
      case 'choose-target-for-give-damage':
        return 'Escolhe o alvo do qual transferir pontos de dano.';
      case 'choose-target-for-receive-damage':
        return 'Escolhe o alvo para o qual transferir pontos de dano.';
      case 'choose-attack-target':
        return 'Escolhe o alvo do ataque, com um duplo clique.';
      case 'choose-attack-target-1':
        return 'Escolhe o alvo do primeiro ataque, com um duplo clique.';
      case 'choose-attack-target-2':
        return 'Escolhe o alvo do segundo ataque, com um duplo clique.';
      case 'choose-attack-target-3':
        return 'Escolhe o alvo do terceiro ataque, com um duplo clique.';
      case 'choose-attack-target-4':
        return 'Escolhe o alvo do quarto ataque, com um duplo clique.';
      case 'choose-attack-points-player':
        return 'Define os pontos totais de ataque da parada de combate.';
      case 'choose-attack-points-other':
        return `O ${ m.languages.keywords.getParity( config.player.parity ).toLowerCase() } está definindo os pontos totais de ataque da parada de combate.`;
      case 'choose-defense-points-player':
        return 'Define os pontos totais de defesa da parada de combate.';
      case 'choose-defense-points-other':
        return `O ${ m.languages.keywords.getParity( config.player.parity ).toLowerCase() } está definindo os pontos totais de defesa da parada de combate.`;
      case 'choose-assign-fate-point':
        return 'Escolhe se desejas atribuir 1 ponto de destino à parada de combate.';
      case 'opponent-choosing-assign-fate-point':
        return 'O oponente está escolhendo se deseja atribuir 1 ponto de destino à parada de combate.';
      case 'players-choosing-assign-fate-point':
        return 'Os jogadores estão escolhendo se desejam atribuir 1 ponto de destino à parada de combate.';
      case 'choosing-attack-target': {
        // Identificadores
        let { playerParity, sourceCard } = config,
            player = m.languages.keywords.getParity( playerParity ).toLowerCase(),
            cardName = sourceCard instanceof m.Being ? sourceCard.content.designation.full : `"${ sourceCard.content.designation.title }"`;

        // Retorna texto para a modal
        return `O ${ player } está escolhendo um alvo para o ataque de ${ cardName }.`;
      }
      case 'choosing-attack-defense': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            committer = config.action.committer.content.designation.full;

        // Retorna texto para a modal
        return `O ${ player } está escolhendo uma defesa para o ataque de ${ committer }.`;
      }
      case 'canceling-action': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            actionName = m.languages.keywords.getAction( config.action.name ).toLowerCase(),
            committer = config.action.committer.content.designation.full;

        // Retorno do texto
        return `O ${ player } cancelou a ação "${ actionName }" de ${ committer }.`;
      }
      case 'choosing-free-attack': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            [ committer, defender ] = [ config.committer.content.designation.full, config.defender?.content.designation.full ];

        // Retorna texto para a modal
        return `O ${ player } está avaliando desferir um ataque livre com ${ committer }${ defender ? ' contra ' + defender : '' }.`;
      }
      case 'free-attack-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            decision = config.isToAttack ? 'atacar' : 'não atacar';

        // Retorno do texto
        return `O ${ player } escolheu ${ decision }.`;
      }
      case 'choosing-free-attack-maneuver': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            [ committer ] = [ config.committer.content.designation.full ];

        // Retorna texto para a modal
        return `O ${ player } está escolhendo uma manobra para o ataque livre de ${ committer }.`;
      }
      case 'choosing-fate-break-target': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorna texto para a modal
        return `O ${ player } está escolhendo um alvo para o efeito da parada de destino.`;
      }
      case 'chose-fate-break-target': {
        // Identificadores
        let targetName = config.target instanceof m.Being ? config.target.content.designation.full : config.target.content.designation.title;

        // Retorna texto para a modal
        return `${ targetName } foi escolhido como alvo para o efeito da parada de destino.`;
      }
      case 'canceling-fate-break-target': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorno do texto
        return `O ${ player } cancelou o efeito da parada de destino.`;
      }
      case 'fate-break-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            decision = config.isToUse ? 'usar' : 'não usar';

        // Retorno do texto
        return `O ${ player } escolheu ${ decision } pontos de destino.`;
      }
      case 'fate-break-contestation-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            decision = config.isToUse ? 'contestar' : 'não contestar';

        // Retorno do texto
        return `O ${ player } escolheu ${ decision } a parada de destino.`;
      }
      case 'choosing-deny-fate-break-contestation': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorno do texto
        return `O ${ player } está avaliando negar a contestação da parada de destino, via "O Festim".`;
      }
      case 'deny-fate-break-contestation-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            decision = config.isToDeny ? 'negar' : 'não negar';

        // Retorno do texto
        return `O ${ player } escolheu ${ decision } a contestação da parada de destino.`;
      }
      case 'choosing-contest-fate-break-denying': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorno do texto
        return `O ${ player } está avaliando contestar a negação de sua contestação, via "O Festim".`;
      }
      case 'contest-fate-break-denying-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase(),
            decision = config.isToContest ? 'contestar' : 'não contestar';

        // Retorno do texto
        return `O ${ player } escolheu ${ decision } a negação de sua contestação.`;
      }
      case 'awaiting-player-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorno do texto
        return `Aguardando decisão do ${ player }.`;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre registros de ações
  notices.getActionLog = function ( textName, config = {} ) {
    // Identificadores
    var { action, isFull = false, eventType = '' } = config;

    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'action-abort': {
        // Identificadores
        let { committer, actionToAbort } = action,
            committerName = committer.content.designation.full,
            actionToAbortName = m.languages.keywords.getAction( actionToAbort.name ).toLowerCase();

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } abortou "${ actionToAbortName }".`;
      }
      case 'action-absorb': {
        // Identificadores
        let { committer, manaToAbsorb, manaReceiver } = action,
            committerName = committer.content.designation.full,
            manaToAbsorbSnippet = `${ manaToAbsorb } ponto${ manaToAbsorb == 1 ? '' : 's' } de mana`,
            receiverSnippet = committer == manaReceiver ? '' : ` via ${ manaReceiver.content.designation.title }`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Caso alvo a receber o mana não seja acionante, indica em que casa está
          if( committer != manaReceiver ) receiverSnippet += m.languages.snippets.getCardPosition( manaReceiver );
        }

        // Retorno do texto
        return `${ committerName } absorveu ${ manaToAbsorbSnippet }${ receiverSnippet }.`;
      }
      case 'action-attack': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            freeAttackSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          // Caso ataque seja livre, aponta isso e o que o causou
          if( action.commitment.type == 'free'  ) freeAttackSnippet +=
            `, via um ${ m.languages.snippets.getFreeAttack( m.ActionAttack.freeAttackExecution?.name ?? '' ).toLowerCase() }`.replace( /\.$/, '' );
        }

        // Retorno do texto
        return `${ committerName } atacará ${ targetName }${ freeAttackSnippet }.`;
      }
      case 'action-banish': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target instanceof m.Being ? target.content.designation.full : target.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } baniu ${ targetName }.`;
      }
      case 'action-channel': {
        // Identificadores
        let matchFlow = m.GameMatch.current.flow,
            { committer, target: spell, manaToAssign, adjunctManaToAssign, adjunctSpells } = action,
            { target: spellTarget } = spell,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.full,
            resource = committer instanceof m.AstralBeing ? 'energia' : 'mana',
            tenseSnippet = matchFlow.period instanceof m.CombatPeriod && action.flowCost && !action.isCommitted ? 'canalizará' : 'canalizou',
            spellTargetSnippet = m.languages.snippets.getSpellTargetSnippet( spellTarget, committer ),
            spellExtraTargetsSnippet = '',
            endSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Complementa nome da magia com indicação de em que casa está
          spellName += m.languages.snippets.getCardPosition( spell );

          // Para caso alvo da magia seja uma carta que não o acionante
          if( spellTarget instanceof m.Card && spellTarget != committer ) {
            // Para entes e equipamentos embutidos, complementa nome do alvo da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
            if( spellTarget instanceof m.Being || spellTarget.isEmbedded ) spellTargetSnippet += spellTarget.content.designation.getIndex( true );

            // Complementa nome do alvo da magia com indicação de em que casa está
            spellTargetSnippet += m.languages.snippets.getCardPosition( spellTarget );
          }

          // Itera por eventuais alvos adicionais da magia
          for( let i = 0; i < spell.extraTargets.length; i++ ) {
            // Identificadores
            let extraTarget = spell.extraTargets[ i ],
                extraTargetName = m.languages.snippets.getSpellTargetSnippet( extraTarget, committer );

            // Filtra alvos que sejam iguais ao alvo principal
            if( extraTarget == spellTarget ) continue;

            // Filtra alvos que já tenham sido relevados
            if( spell.extraTargets.indexOf( extraTarget ) != i ) continue;

            // Para caso alvo adicional da magia seja uma carta que não o acionante
            if( extraTarget instanceof m.Card && extraTarget != committer ) {
              // Para entes e equipamentos embutidos, complementa nome do alvo adicional da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
              if( extraTarget instanceof m.Being || extraTarget.isEmbedded ) extraTargetName += extraTarget.content.designation.getIndex( true );

              // Complementa nome do alvo adicional da magia com indicação de em que casa está
              extraTargetName += m.languages.snippets.getCardPosition( extraTarget );
            }

            // Adiciona ao trecho de alvos adicionais informação sobre o alvo adicional da magia
            spellExtraTargetsSnippet += extraTargetName + ', ';
          }

          // Ajusta separador final do eventual trecho sobre alvos adicionais
          spellExtraTargetsSnippet = spellExtraTargetsSnippet.replace( /, $/, ' e ' );
        }

        // Formula trecho final
        setEndSnippet: {
          // Apenas executa bloco caso registro seja o completo
          if( !isFull ) break setEndSnippet;

          // Identificadores
          let manaToAssignSnippet = !manaToAssign ?
                '' : `via ${ manaToAssign } ponto${ manaToAssign == 1 ? '' : 's' } de ${ resource } atribuído${ manaToAssign == 1 ? '' : 's' }`,
              adjunctSpellsSnippet = !adjunctSpells.length ?
                '' : `também gastando ${ adjunctManaToAssign } ponto${ adjunctManaToAssign == 1 ? '' : 's' } de ${ resource }`;

          // Apenas executa bloco caso haja ao menos 1 informação a constituir trecho final
          if( [ manaToAssignSnippet, adjunctSpellsSnippet ].every( snippet => !snippet ) ) break setEndSnippet;

          // Adiciona a trecho final separador
          endSnippet += ', ';

          // Para caso haja mana atribuído
          if( manaToAssignSnippet ) {
            // Identificadores
            let manaWithPolarityBurden = spell.applyPolarityBurden( manaToAssign );

            // Adiciona a trecho final mana atribuído
            endSnippet += manaToAssignSnippet;

            // Quando houver ônus de polaridade, adiciona essa informação
            if( manaToAssign != manaWithPolarityBurden ) endSnippet += ` (${ manaWithPolarityBurden } pontos gastos, com o ônus de polaridade)`;
          }

          // Encerra bloco caso não exista trecho sobre magias adjuntas
          if( !adjunctSpellsSnippet ) break setEndSnippet;

          // Caso exista conteúdo no trecho sobre mana atribuído, adiciona a trecho final separador
          if( manaToAssignSnippet ) endSnippet += ' e ';

          // Adiciona a trecho final mana gasto com magias adjuntas
          endSnippet += adjunctSpellsSnippet;

          // Adiciona a trecho final informações sobre magias adjuntas
          endSnippet += adjunctSpells.length == 1 ? ` com a magia ` : ` ao todo com as magias: `;

          // Adiciona ao trecho final nome das magias adjuntas
          for( let adjunctSpell of adjunctSpells ) endSnippet += `"${ adjunctSpell.content.designation.title }", `;

          // Remove separador final sobre magias adjuntas
          endSnippet = endSnippet.replace( /, $/, '' );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ spellName }${ spellExtraTargetsSnippet }${ spellTargetSnippet }${ endSnippet }.`;
      }
      case 'action-deploy': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Caso registro seja o completo, complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
        if( isFull ) committerName += committer.content.designation.getIndex( true );

        // Retorno do texto
        return `${ committerName } mobilizou-se em ${ targetName }.`;
      }
      case 'action-disengage': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            tenseSnippet = action.flowCost && !action.isCommitted ? 'desengajará' : 'desengajou';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } para ${ targetName }.`;
      }
      case 'action-energize': {
        // Identificadores
        let { committer, target, manaToTransfer } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            manaToTransferSnippet = `${ manaToTransfer } ponto${ manaToTransfer == 1 ? '' : 's' } de mana`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } energizou ${ targetName } via ${ manaToTransferSnippet }.`;
      }
      case 'action-engage': {
        // Identificadores
        let { committer, target, formerSlot } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            tenseSnippet = action.flowCost && !action.isCommitted ? 'engajará' : 'engajou';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer, formerSlot );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } em ${ targetName }.`;
      }
      case 'action-equip': {
        // Identificadores
        let { committer, target, itemAttacher } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            verbSnippet = target.isToAttach ? 'equipou' : 'usou',
            itemAttacherSnippet = itemAttacher == committer ? '' : ' a ' + itemAttacher.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o vinculante do alvo
          if( itemAttacher.isEmbedded ) itemAttacherSnippet += itemAttacher.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          /// Para o vinculante do alvo
          itemAttacherSnippet += itemAttacher == committer ? '' : m.languages.snippets.getCardPosition( itemAttacher );
        }

        // Retorno do texto
        return `${ committerName } ${ verbSnippet } ${ targetName }${ itemAttacherSnippet }.`;
      }
      case 'action-expel': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'expelirá mana' : 'expeliu 1 ponto de mana';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Caso ação tenha sido concluída, indica essa informação
          if( eventType == 'finish' ) tenseSnippet += ', e concluiu essa ação';
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet }.`;
      }
      case 'action-guard': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } está amparando ${ targetName }.`;
      }
      case 'action-handle': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            operationText = target.isInUse ? 'desuso' : 'uso';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o alvo
          if( target.isEmbedded ) targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } colocou em ${ operationText } ${ targetName }.`;
      }
      case 'action-intercept': {
        // Identificadores
        let { committer, target, interceptedBeing } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            interceptedBeingName = interceptedBeing.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o ente interceptado
          interceptedBeingName += interceptedBeing.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o ente interceptado
          interceptedBeingName += m.languages.snippets.getCardPosition( interceptedBeing );
        }

        // Retorno do texto
        return `${ committerName } interceptou ${ interceptedBeingName } em ${ targetName }.`;
      }
      case 'action-leech': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'vampirizará' : eventType == 'finish' ? 'terminou de vampirizar' : 'vampirizou',
            energyPoints = Math.round( target.content.stats.attributes.current.stamina * .5 ),
            finalSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          // Caso ação tenha sido concluída, adiciona essa informação
          if( eventType == 'finish' )
            finalSnippet += ', tendo restaurado sua energia para o valor base'

          // Caso ação tenha sido acionada, adiciona quantidade de pontos de energia restaurados
          else if( action.isCommitted )
            finalSnippet += `, restaurando ${ energyPoints } ponto${ energyPoints == 1 ? '' : 's' } de energia`;
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ targetName }${ finalSnippet }.`;
      }
      case 'action-possess': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'possuirá' : 'possuiu';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ targetName }.`;
      }
      case 'action-prepare': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            committerSize = committer.content.typeset.size,
            segmentsQuantity = committerSize == 'large' ? '3 segmentos' : committerSize == 'medium' ? '2 segmentos' : '1 segmento';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } abriu jogadas de seu jogador em até ${ segmentsQuantity }.`;
      }
      case 'action-relocate': {
        // Identificadores
        let { committer, target, formerSlot } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer, formerSlot );
        }

        // Retorno do texto
        return `${ committerName } posicionou-se em ${ targetName }.`;
      }
      case 'action-retreat': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'recuará' : 'recuou';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet }.`;
      }
      case 'action-summon': {
        // Identificadores
        let { committer, target, token } = action,
            committerName = committer.content.designation.full,
            tokenName = token.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para a ficha
          tokenName += token.content.designation.getIndex( true, committer.deck );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } convocou ${ tokenName } em ${ targetName }.`;
      }
      case 'action-sustain': {
        // Identificadores
        let { committer, target: spell, manaToAssign } = action,
            { target: spellTarget } = spell,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.full,
            spellTargetSnippet = m.languages.snippets.getSpellTargetSnippet( spellTarget, committer ),
            resource = committer instanceof m.AstralBeing ? 'energia' : 'mana',
            tenseSnippet = eventType == 'finish' ? 'terminou de sustentar' : 'sustentou',
            manaToAssignSnippet = isFull && manaToAssign && ( eventType != 'finish' || spell.content.typeset.durationSubtype != 'constant' ) ?
              `, via ${ manaToAssign } ponto${ manaToAssign == 1 ? '' : 's' } de ${ resource } atribuído${ manaToAssign == 1 ? '' : 's' }` : '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Complementa nome da magia com indicação de em que casa está
          spellName += m.languages.snippets.getCardPosition( spell );

          // Para caso alvo da magia seja uma carta que não o acionante
          if( spellTarget instanceof m.Card && spellTarget != committer ) {
            // Para entes e equipamentos embutidos, complementa nome do alvo da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
            if( spellTarget instanceof m.Being || spellTarget.isEmbedded ) spellTargetSnippet += spellTarget.content.designation.getIndex( true );

            // Complementa nome do alvo da magia com indicação de em que casa está
            spellTargetSnippet += m.languages.snippets.getCardPosition( spellTarget );
          }

          // Para caso haja mana atribuído
          if( manaToAssignSnippet ) {
            // Identificadores
            let manaWithPolarityBurden = spell.applyPolarityBurden( manaToAssign );

            // Quando aplicável, adiciona a trecho de mana atribuído o custo com o ônus de polaridade
            if( manaToAssign != manaWithPolarityBurden ) manaToAssignSnippet += ` (${ manaWithPolarityBurden } pontos gastos, com o ônus de polaridade)`;
          }
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ spellName }${ spellTargetSnippet }${ manaToAssignSnippet }.`;
      }
      case 'action-swap': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full;

        // Retorno do texto
        return `Feita troca entre "${ committerName }" e "${ targetName }".`;
      }
      case 'action-transfer': {
        // Identificadores
        let { committer, target, itemOwner, itemAttacher } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            ownerName = itemOwner.content.designation.full,
            itemAttacherSnippet = itemOwner == itemAttacher ? '' : `${ itemAttacher.content.designation.title }`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o novo dono do alvo
          ownerName += itemOwner.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o novo vinculante do alvo
          if( itemAttacher.isEmbedded ) itemAttacherSnippet += itemAttacher.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          /// Para o novo dono do alvo
          ownerName += m.languages.snippets.getCardPosition( itemOwner );

          /// Para o novo vinculante do alvo, se diferente do novo dono
          if( itemOwner != itemAttacher ) itemAttacherSnippet += m.languages.snippets.getCardPosition( itemAttacher );
        }

        // Caso haja conteúdo no trecho sobre vinculante, concatena a ele separador
        if( itemAttacherSnippet ) itemAttacherSnippet += ' de ';

        // Retorno do texto
        return `${ committerName } transferiu ${ targetName } para ${ itemAttacherSnippet }${ ownerName }.`;
      }
      case 'action-unequip': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o alvo
          if( target.isEmbedded ) targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } desequipou ${ targetName }.`;
      }
      case 'action-unsummon': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } foi desconvocado.`;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre resultados de eventos
  notices.getOutcome = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'combat-break': {
        // Identificadores
        let { combatBreak, isFull = false } = config,
            { attack, defense, attacker, defender } = combatBreak,
            attackerName = attacker.content.designation.full,
            defenderName = defender.content.designation.full,
            attackName = `"${ m.languages.keywords.getManeuver( attack.maneuver.name ).toLowerCase() }"`,
            defenseSnippet = defense ? `, contra "${ m.languages.keywords.getManeuver( defense.maneuver.name ).toLowerCase() }"` : '',
            inflictedPoints = combatBreak.gaugeInflictedAttack(),
            inflictedPointsSnippet =
              inflictedPoints ? `${ inflictedPoints } ponto${ inflictedPoints == 1 ? '' : 's' } de ataque infligido${ inflictedPoints == 1 ? '' : 's' }` :
                                'nenhum ponto de ataque infligido',
            outcomeSnippet = '',
            fatePointsSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Identificadores
          let assignedPoints = combatBreak.getAssignedPoints(),
              attackModifiers = Object.values( assignedPoints.attackModifiers ).reduce( ( accumulator, current ) => accumulator += current, 0 ),
              defenseModifiers = Object.values( assignedPoints.defenseModifiers ).reduce( ( accumulator, current ) => accumulator += current, 0 ),
              dynamicModifiers = {
                attack: attackModifiers - assignedPoints.attackModifiers.base,
                defense: defenseModifiers - assignedPoints.defenseModifiers.base
              },
              totalPoints = {
                attack: assignedPoints.attack + attackModifiers,
                defense: assignedPoints.defense + defenseModifiers
              },
              fateChoices = combatBreak.getFateChoices();

          // Complementa nome das cartas com índice de suas posições no baralho ante outras cartas com a mesma designação

          /// Para o atacante
          attackerName += attacker.content.designation.getIndex( true );

          /// Para o defensor
          defenderName += defender.content.designation.getIndex( true );

          // Complementa nome das cartas com indicação de em que casa estão

          /// Para o atacante
          attackerName += m.languages.snippets.getCardPosition( attacker );

          /// Para o defensor
          defenderName += m.languages.snippets.getCardPosition( defender );

          // Define cômputo de pontos totais
          for( let name of [ 'attack', 'defense' ] )
            totalPoints[ name + 'Sum' ] = `(${ assignedPoints[ name ] }P + ${ assignedPoints[ name + 'Modifiers' ].base }ME + ${ dynamicModifiers[ name ] }MD)`;

          // Define trecho do resultado do ataque

          /// Início
          outcomeSnippet += `Os pontos totais foram `;

          /// Pontos de ataque
          outcomeSnippet += `${ totalPoints.attack } de ataque ${ totalPoints.attackSum }`;

          /// Pontos de defesa, se existentes
          if( defense ) outcomeSnippet += ` e ${ totalPoints.defense } de defesa ${ totalPoints.defenseSum }`;

          /// Final
          outcomeSnippet += `, resultando em ${ inflictedPointsSnippet }`;

          // Define trecho dos pontos de destino gastos

          /// Para caso ambos os jogadores tenham gasto pontos de destino
          if( fateChoices.attacker && fateChoices.defender ) fatePointsSnippet += ` Ambos os jogadores gastaram 1 ponto de destino.`

          /// Para caso o jogador do atacante tenha gasto 1 ponto de destino
          else if( fateChoices.attacker ) fatePointsSnippet += ` O jogador do atacante gastou 1 ponto de destino.`

          /// Para caso o jogador do defensor tenha gasto 1 ponto de destino
          else if( fateChoices.defender ) fatePointsSnippet += ` O jogador do defensor gastou 1 ponto de destino.`;
        }

        // Do contrário
        else {
          // Define trecho do resultado do ataque
          outcomeSnippet = `Isso resultou em ${ inflictedPointsSnippet }`;
        }

        // Retorno do texto
        return `${ attackerName } atacou ${ defenderName } com ${ attackName }${ defenseSnippet }. ${ outcomeSnippet }.${ fatePointsSnippet }`;
      }
      case 'combat-break-result':
        return 'Este é o resultado desta parada de combate.';
      case 'inflicted-damage': {
        // Identificadores
        let { damages, damageTarget: being, isToBeRemoved = false, isFull = false } = config,
            beingName = being.content.designation.full,
            damageTypeSnippet = damages[ 0 ].isCritical ? 'crítico ' : damages[ 0 ].isGraze ? 'escoriativo ' : '',
            damageText = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );
        }

        // Define início do texto sobre danos sofridos
        damageText += `${ beingName } sofreu um dano ${ damageTypeSnippet }de `;

        // Itera por danos infligidos
        for( let damage of damages ) {
          // Identificadores
          let { type, totalPoints } = damage,
              damageSnippet = type != 'raw' ? `de ${ m.languages.keywords.getDamage( damage.name ).toLowerCase() }` :
                              damage.maneuver ? `via "${ m.languages.keywords.getManeuver( damage.maneuver.name ).toLowerCase() }"` :
                              damage.source ? `via "${ damage.source.content.designation.title }"` : 'de dano bruto';

          // Adiciona pontos de dano alvo ao texto sobre danos infligidos
          damageText += `${ totalPoints } ponto${ totalPoints == 1 ? '' : 's' } ${ damageSnippet } e `;
        }

        // Remove separador final do dano infligido
        damageText = damageText.replace( / e $/, '' );

        // Adiciona informação sobre se ente foi removido por conta do dano
        if( isToBeRemoved ) damageText += ', tendo sido removido';

        // Finaliza sentença
        damageText += `.`;

        // Retorno do texto
        return damageText;
      }
      case 'healed-damage': {
        // Identificadores
        let { healedDamage, healTarget: being, source, isFull = false } = config,
            beingName = being.content.designation.full,
            sourceName = source.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );
        }

        // Retorno do texto
        return `${ beingName } restaurou ${ healedDamage } ponto${ healedDamage == 1 ? '' : 's' } de vida via "${ sourceName }".`;
      }
      case 'fate-break': {
        // Identificadores
        let { fateBreakName, fateBreakResult, decidingPlayer, isContestable, requiredPoints } = config,
            playerName = m.languages.keywords.getParity( decidingPlayer.parity ).toLowerCase(),
            headingSnippet = `O ${ playerName } resolveu uma parada de destino`,
            contestableSnippet = isContestable ? ' contestável' : isContestable === false ? ' incontestável' : '',
            fateBreakSnippet = m.languages.snippets.getFateBreak( fateBreakName ).replace( /\.$/, '' ).replace( /^Parada de destino ?/i, '' ),
            useSnippet = fateBreakResult.player ? 'usou ' : 'não usou ',
            fatePointsSnippet = `${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino`,
            contestationSnippet = fateBreakResult.opponent ? ', e foi contestado' : '';

        // Retorno do texto
        return `${ headingSnippet }${ contestableSnippet } ${ fateBreakSnippet }. Ele ${ useSnippet }${ fatePointsSnippet }${ contestationSnippet }.`;
      }
      case 'condition-gained': {
        // Identificadores
        let { condition, being, markers } = config,
            beingName = being.content.designation.full,
            conditionName = m.languages.keywords.getCondition( condition.name ).toLowerCase(),
            [ markersSnippet, finalSnippet ] = [ '', '' ];

        // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
        beingName += being.content.designation.getIndex( true );

        // Complementa nome do ente com indicação de em que casa está
        beingName += m.languages.snippets.getCardPosition( being );

        // Para caso condição seja gradativa
        if( condition instanceof m.GradedCondition ) {
          // Complementa texto com marcadores da condição ganhos
          markersSnippet += `${ markers } marcador${ markers == 1 ? '' : 'es' } d`;
        }

        // Para caso condição seja binária
        else if( condition instanceof m.BinaryCondition ) {
          // Inicia texto do trecho de finalização
          finalSnippet += ', a expirar até o ';

          // Conclui texto do trecho de finalização segundo estágio de expiração
          finalSnippet += ( function () {
            // Identificadores
            var expireStage = condition.expireStage ?? m.GameMatch.current.flow.turn.getChild( condition.maxExpireStageName ),
                expireStageName = m.languages.keywords.getFlow( expireStage.name ).toLowerCase();

            // Retorno do final do trecho para caso estágio seja um período intersticial
            if( expireStage instanceof m.BreakPeriod ) return `início do próximo ${ expireStageName }`;

            // Retorno do final do trecho para caso estágio seja um período do combate
            if( expireStage instanceof m.CombatPeriod ) return `fim do ${ expireStageName }`;

            // Retorno do final do trecho para caso estágio seja um bloco
            if( expireStage instanceof m.FlowBlock ) return `fim do ${ expireStage.displayName.toLowerCase() }`;

            // Captura nome do bloco em que estágio de expiração possa estar
            let blockName = expireStage.getParent( 'block' )?.displayName.toLowerCase();

            // Retorno do final do trecho para caso estágio seja um segmento
            if( expireStage instanceof m.FlowSegment ) return `fim do ${ expireStage.displayName.toLowerCase() }${ blockName ? ` do ${ blockName }` : '' }`;

            // Captura nome do segmento em que estágio de expiração está
            let segmentName = expireStage.getParent( 'segment' ).displayName.toLowerCase();

            // Retorno do final do trecho para caso estágio seja uma paridade
            if( expireStage instanceof m.FlowParity ) return `fim da ${ expireStageName } do ${ segmentName }${ blockName ? ` do ${ blockName }` : '' }`;
          } )();
        }

        // Retorno do texto
        return `${ beingName } ganhou ${ markersSnippet }a condição "${ conditionName }"${ finalSnippet }.`;
      }
      case 'condition-removed': {
        // Identificadores
        let { condition, being, markers } = config,
            beingName = being.content.designation.full,
            conditionName = m.languages.keywords.getCondition( condition.name ).toLowerCase(),
            markersSnippet = '';

        // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
        beingName += being.content.designation.getIndex( true );

        // Complementa nome do ente com indicação de em que casa está
        beingName += m.languages.snippets.getCardPosition( being );

        // Para caso condição seja gradativa e ainda haja ao menos um marcador na condição
        if( condition instanceof m.GradedCondition && condition.markers ) {
          // Complementa texto com marcadores da condição perdidos
          markersSnippet += `${ markers } marcador${ markers == 1 ? '' : 'es' } d`;
        }

        // Retorno do texto
        return `${ beingName } perdeu ${ markersSnippet }a condição "${ conditionName }".`;
      }
      case 'action-prevented': {
        // Identificadores
        let { action, action: { committer }, isFull = false, isInvalid = false } = config,
            actionName = m.languages.keywords.getAction( action.name ).toLowerCase(),
            committerName = committer.content.designation.full,
            validitySnippet = isInvalid ? ', por não ser mais válida' : '';

        // Para caso descrição deva ser uma completa
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `A ação "${ actionName }" de ${ committerName } foi impedida${ validitySnippet }.`;
      }
      case 'orichalcum-pendant-spell-prevented':
        return `O ${ config.item.content.designation.title } de ${ config.item.owner.content.designation.full } negou o efeito da magia "${ config.spell.content.designation.title }".`;
      case 'magic-shot-prevented-by-attract': {
        // Identificadores
        let spellName = config.spell.content.designation.full,
            targetName = config.target.content.designation.full,
            preventTense = config.presentTense ? 'impede' : 'impediu',
            affectTense = config.presentTense ? 'afete' : 'afetasse';

        // Retorno do texto
        return `A magia "Atrair" ${ preventTense } que a magia "${ spellName }" ${ affectTense } ${ targetName }.`;
      }
      case 'condition-prevented-by-body-cleasing': {
        // Identificadores
        let conditionName = m.languages.keywords.getCondition( config.conditionName ).toLowerCase(),
            targetName = config.target.content.designation.full,
            snippetTense = config.presentTense ? 'impede' : 'impediu';

        // Retorno do texto
        return `A magia "Purificação Corporal" ${ snippetTense } a aplicação da condição "${ conditionName }" em ${ targetName }.`;
      }
      case 'condition-prevented-by-sanctuary': {
        // Identificadores
        let conditionName = m.languages.keywords.getCondition( config.conditionName ).toLowerCase(),
            targetName = config.target.content.designation.full,
            snippetTense = config.presentTense ? 'impede' : 'impediu';

        // Retorno do texto
        return `A magia "Santuário" ${ snippetTense } a aplicação da condição "${ conditionName }" em ${ targetName }.`;
      }
      case 'voltaic-outburst-attack-prevented': {
        // Identificadores
        let { action: { committer, target: spell } } = config,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.title;

        // Retorno do texto
        return `O ataque de ${ committerName } via "${ spellName }" não foi desferido, por não ser mais válido.`;
      }
      case 'weapon-withdrawn-by-its-effect': {
        // Identificadores
        let { owner, isFull = false } = config,
            ownerName = owner.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do dono com índice de sua posição no baralho ante outras cartas com a mesma designação
          ownerName += owner.content.designation.getIndex( true );

          // Complementa nome do dono com indicação de em que casa está
          ownerName += m.languages.snippets.getCardPosition( owner );
        }

        // Retorno do texto
        return `A arma usada por ${ ownerName } na última parada de combate foi afastada, devido a seu efeito.`;
      }
      case 'weapon-ranseur-effect': {
        // Identificadores
        let { owner, isFull = false } = config,
            ownerName = owner.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do dono com índice de sua posição no baralho ante outras cartas com a mesma designação
          ownerName += owner.content.designation.getIndex( true );

          // Complementa nome do dono com indicação de em que casa está
          ownerName += m.languages.snippets.getCardPosition( owner );
        }

        // Retorno do texto
        return `A arma usada por ${ ownerName } na última parada de combate foi afastada, devido ao efeito do ranseur do defensor.`;
      }
      case 'spell-attract-effect': {
        // Identificadores
        let { card: spell, transferredMana } = config,
            headingText = `O efeito da magia "${ spell.content.designation.title }" transferiu para ${ spell.target.name }: `,
            transferredManaSnippet = '';

        // Itera por casas que tiveram pontos de mana transferidos para o alvo da magia
        for( let slotName in transferredMana ) {
          // Identificadores
          let manaPoints = transferredMana[ slotName ];

          // Adiciona pontos de mana transferidos da casa alvo ao texto sobre mana transferido
          transferredManaSnippet += `${ manaPoints } ponto${ manaPoints == 1 ? '' : 's' } de mana de ${ slotName }; `;
        }

        // Trata final do trecho sobre mana transferido
        transferredManaSnippet = transferredManaSnippet.replace( /; $/, '.' );

        // Retorno do texto
        return headingText + transferredManaSnippet;
      }
      case 'spell-the-truce-effect':
        return `O efeito da magia "${ config.card.content.designation.title }" foi provocado, concluindo prematuramente o pós-combate e seu turno da batalha.`;
      case 'spell-the-lure-effect': {
        // Identificadores
        let { card: spell, beings } = config,
            headingText = `O efeito da magia "${ spell.content.designation.title }" foi provocado, `,
            beingSnippet = `tendo suspendido ${ beings.length } ente${ beings.length == 1 ? '' : 's' }: `;

        // Para caso nenhum ente tenha sido suspenso
        if( !beings.length ) return headingText + 'mas nenhum ente foi suspenso.';

        // Itera por entes afetados
        for( let being of beings ) {
          // Identificadores
          let beingName = being.content.designation.full;

          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );

          // Adiciona ente ao trecho sobre entes afetados
          beingSnippet += beingName + '; ';
        }

        // Trata final do trecho sobre entes afetados
        beingSnippet = beingSnippet.replace( /; $/, '.' );

        // Retorno do texto
        return headingText + beingSnippet;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para textos sobre paradas de destino
  notices.getFateBreakText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'fate-break-heading-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints, isContestable } = config,
            headingSnippet = `Desejas gastar ${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino`,
            contestableSnippet = isContestable === null ? '' : ` ${ isContestable ? 'contestáve' : 'incontestáve' }${ requiredPoints == 1 ? 'l' : 'is' }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embutido${ embeddedPoints == 1 ? '' : 's' })`;

        // Retorno do texto
        return `${ headingSnippet }${ contestableSnippet }${ embeddedSnippet } para que `;
      }
      case 'fate-break-heading-other': {
        // Identificadores
        let { player, requiredPoints, embeddedPoints, isContestable } = config,
            playerParity = m.languages.keywords.getParity( player.parity ).toLowerCase(),
            headingSnippet = `O ${ playerParity } está avaliando gastar ${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino`,
            contestableSnippet = isContestable === null ? '' : ` ${ isContestable ? 'contestáve' : 'incontestáve' }${ requiredPoints == 1 ? 'l' : 'is' }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embutido${ embeddedPoints == 1 ? '' : 's' })`;

        // Retorno do texto
        return `${ headingSnippet }${ contestableSnippet }${ embeddedSnippet } para que `;
      }
      case 'fate-break-contestation-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints, effect } = config,
            headingSnippet = `O oponente gastou ${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino para que ${ effect }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } ${ embeddedPoints == 1 ? 'ponto embutido disponível' : 'pontos embutidos disponíveis' })`;

        // Retorno do texto
        return `${ headingSnippet }. Desejas o contestar?${ embeddedSnippet }`;
      }
      case 'fate-break-contestation-other': {
        // Identificadores
        let { player, requiredPoints, embeddedPoints } = config,
            playerParity = m.languages.keywords.getParity( player.parity ).toLowerCase(),
            usableEmbeddedPoints = Math.min( requiredPoints, embeddedPoints ),
            headingSnippet = `O ${ playerParity } está avaliando contestar o uso ${ requiredPoints == 1 ? 'do ponto' : 'dos pontos' } de destino`,
            embeddedSnippet = !embeddedPoints ? '' : `, com ${ usableEmbeddedPoints } ${ usableEmbeddedPoints == 1 ? 'ponto embutido usável' : 'pontos embutidos usáveis' }`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }.`;
      }
      case 'the-feast-contestation-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints } = config,
            headingSnippet = `Desejas gastar ${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino para negar a contestação do oponente, via "O Festim"?`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } ${ embeddedPoints == 1 ? 'ponto embutido disponível' : 'pontos embutidos disponíveis' })`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }`;
      }
      case 'the-feast-contestation-opponent': {
        // Identificadores
        let { requiredPoints, embeddedPoints } = config,
            headingSnippet = `Desejas gastar ${ requiredPoints } ponto${ requiredPoints == 1 ? '' : 's' } de destino para contestar a negação do oponente, via "O Festim"?`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } ${ embeddedPoints == 1 ? 'ponto embutido disponível' : 'pontos embutidos disponíveis' })`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }`;
      }
      case 'fate-break-prevent-actions':
        return `${ config.committer.content.designation.full } não tenha sua ação em andamento impedida`;
      case 'fate-break-raging-surge-gain-enraged-prevent-emboldened':
        return `${ config.orc.content.designation.full } ganhe ${ config.markers } ${ config.markers == 1 ? 'marcador' : 'marcadores' } de enfurecido, em vez de encorajado`;
      case 'fate-break-raging-surge-gain-enraged':
        return `${ config.orc.content.designation.full } ganhe ${ config.markers } ${ config.markers == 1 ? 'marcador' : 'marcadores' } de enfurecido`;
      case 'fate-break-raging-surge-prevent-emboldened':
        return `${ config.orc.content.designation.full } não ganhe ${ config.markers } ${ config.markers == 1 ? 'marcador' : 'marcadores' } de encorajado`;
      case 'fate-break-action-possess':
        return `a ação "${ m.languages.keywords.getAction( config.action.name ).toLowerCase() }" de ${ config.action.committer.content.designation.full } contra ${ config.action.target.content.designation.full } seja impedida`;
      case 'fate-break-nimbleness-potion':
        return `${ config.being.content.designation.full } não seja afastado via "${ config.item.content.designation.title }"`;
      case 'fate-break-spell-poke':
        return `o efeito da magia "${ config.spell.content.designation.title }" de ${ config.spell.owner.content.designation.full } contra ${ config.spell.target.content.designation.full } seja negado`;
      case 'fate-break-spell-tear':
        return `a magia "${ config.spell.content.designation.title }" de ${ config.spell.owner.content.designation.full } seja afastada`;
      case 'fate-break-spell-suppress':
        return `a magia "${ config.spell.content.designation.title }" de ${ config.spell.owner.content.designation.full } seja findada`;
      case 'fate-break-spell-consume':
        return `${ config.spell.owner.content.designation.full } não se torne possuído`;
      case 'fate-break-spell-pain-twist':
        return `o ente a receber até ${ config.spell.damageToAssign } ponto${ config.spell.damageToAssign == 1 ? '' : 's' } de dano via "${ config.spell.content.designation.title }" seja ${ config.spell.owner.content.designation.full }, em vez de ${ config.spell.target.content.designation.full }`;
      case 'fate-break-spell-thunder-strike':
        return `o ataque de "${ config.spell.content.designation.title }" contra ${ config.attackTarget.content.designation.full } seja negado`;
      case 'fate-break-spell-fireball':
        return `o ataque de "${ config.spell.content.designation.title }" tenha 8 pontos de dano, e mire apenas ${ config.spell.target.content.designation.full }, em vez de todos os entes físicos em ${ config.spell.target.slot.name }`;
      case 'fate-break-spell-fissure':
        return `${ config.spell.target.content.designation.full } seja afastado via "${ config.spell.content.designation.title }"`;
      case 'fate-break-spell-the-beast': {
        // Identificadores
        let { spell } = config,
            { fateBreakTarget } = spell.content.effects;

        // Retorno do texto
        return `${ fateBreakTarget?.content.designation.full ?? 'um ente biótico' } seja afastado via "${ spell.content.designation.title }"`;
      }
      case 'fate-break-condition-diseased':
        return `${ config.being.content.designation.full } seja afastado pela condição "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }"`;
    }
  }

  // Para notificações sobre montagens e validações de baralhos
  notices.getDeckText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'deck-building-no-card-to-filter':
        return 'Não há cartas abarcadas pelo filtro alvo que sejam usáveis com o comandante escolhido.';
      case 'deck-building-no-active-deck':
        return 'Muda o componente visível na moldura de baralhos para o do baralho em que desejas adicionar essa carta.';
      case 'deck-building-over-max-cards':
        return 'O baralho alvo já alcançou sua quantidade máxima de cartas.';
      case 'deck-building-over-max-coins':
        return 'O preço dessa carta está acima da quantidade de moedas disponível no baralho alvo.';
      case 'deck-building-over-availability':
        return 'Essa carta já atingiu seu limite de disponibilidade.';
      case 'deck-building-command-invalid':
        return 'O comando do baralho principal já alcançou a quantidade máxima de cartas afins a essa.';
      case 'deck-building-no-commander-removal':
        return 'Para removeres o comandante, deves reiniciar a montagem do baralho, clicando no botão "Voltar".';
      case 'deck-editing-no-commander-removal':
        return 'Não podes remover o comandante de um baralho já montado.';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre o fluxo e jogadas obrigatórias
  notices.getFlowText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'flow-released':
        return 'Fluxo liberado.';
      case 'flow-suspended':
        return 'Fluxo suspenso.';
      case 'flow-release-failure':
        return 'O fluxo não pôde ser liberado.';
      case 'flow-suspension-failure':
        return 'O fluxo não pôde ser suspenso.';
      case 'flow-advanced':
        return 'O fluxo avançou.';
      case 'synchronizing-flow':
        return 'Sincronizando o fluxo.';
      case 'flow-intermission':
        return `Intermissão do ${ m.languages.keywords.getParity( config.parity ).toLowerCase() }.`;
      case 'finish-required-move':
        return 'Tens 1 ou mais jogadas obrigatórias para concluir.';
      case 'must-deploy-commander':
        return 'Deves mobilizar teu comandante.';
      case 'must-deploy-commander-or-ruler':
        return 'Deves mobilizar teu comandante, ou teu ente com "O Regente".';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações relacionadas à conectividade e afins
  notices.getNetworkText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'failed-request':
        return 'Um erro de rede ocorreu. Por favor, verifique sua conexão com a Internet ou tente novamente mais tarde.';
      case 'server-shut-down-manual':
        return 'O servidor do jogo foi desligado. Por conta disso, tuas atividades atuais serão perdidas.';
      case 'server-shut-down-error':
        return 'O servidor do jogo foi desligado, por ter encontrado um erro. Ele está sendo reiniciado ou deverá o ser em breve, mas não é possível reenviar a ele tuas atividades atuais, pelo que o jogo será reiniciado.';
      case 'removed-from-match-by-disconnect':
        return 'Por conta de problemas de conexão, foi preciso te remover da partida em que estavas.';
      case 'match-disconnect-player':
        return 'Tua conexão com o servidor foi perdida. Se não conseguirmos a restaurar em até 1 minuto, esta partida será abortada.';
      case 'match-disconnect-other':
        return `O ${ m.languages.keywords.getParity( config.playerParity ).toLowerCase() } se desconectou do servidor. Caso ele não se reconecte em até 1 minuto, esta partida será abortada.`;
      case 'match-reconnect-player':
        return `Foste reconectado ao servidor.`;
      case 'match-reconnect-other':
        return `O ${ m.languages.keywords.getParity( config.playerParity ).toLowerCase() } foi reconectado ao servidor.`;
      case 'abort-match-by-disconnect-player':
        return 'Não está sendo possível te reconectar ao servidor. Por conta disso, a partida em que estavas foi abortada.';
      case 'abort-match-by-disconnect-other':
        return `Não foi possível reconectar o ${ m.languages.keywords.getParity( config.playerParity ).toLowerCase() } ao servidor. Por conta disso, a partida em que estavas foi abortada.`;
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações diversas
  notices.getMiscText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'small-screen':
        return 'Este jogo requer uma tela de exibição de ao menos 1240 píxeis de largura e 620 píxeis de altura.';
      case 'no-webgl':
        return 'Este jogo requer um navegador que suporte WebGL.';
      case 'player-parity':
        return `Tu és o jogador ${ m.languages.keywords.getParity( config.parity ).toLowerCase() }.`;
      case 'deck-viewing-no-selected-row':
        return 'Não há uma fileira selecionada para sofrer essa operação.';
      default:
        return m.languages.defaultFallback;
    }
  }
} )();
