// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const languages = {};

/// Propriedades iniciais
defineProperties: {
  // Línguas suportadas pelo jogo
  languages.all = [ 'en', 'pt' ];

  // Língua atual do jogo
  languages.current = languages.all.includes( document.documentElement.lang ) ? document.documentElement.lang : 'en';

  // Texto padrão a ser exibido caso texto desejado não tenha sido encontrado
  languages.defaultFallback = '?';

  // Métodos não configuráveis de 'languages'
  Object.defineProperties( languages, {
    // Altera língua do jogo
    change: {
      value: function ( language = '' ) {
        // Não executa função caso língua passada não seja suportada pelo jogo
        if( !languages.all.includes( language ) ) return false;

        // Não executa função caso língua passada seja igual à atual
        if( languages.current == language ) return false;

        // Envia requisição para alteração do idioma
        return m.app.sendHttpRequest( {
          path: `/actions/change-language${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
          body: JSON.stringify( { language: language } ),
          isToBlock: true,
          onLoad: () => location.reload()
        } );
      }
    }
  } );
}
