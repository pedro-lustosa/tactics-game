// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'en' ) return;

  // Identificadores
  const components = m.languages.components = {};

  // Propriedades iniciais
  defineProperties: {
    // Para a tela de carregamento
    components.loadingScreen = function () {
      return {
        inProgress: 'Loading...',
        start: 'Start'
      };
    }

    // Para o menu da tela de abertura
    components.openingMenu = function () {
      return {
        learn: 'Learn',
        manual: 'Manual',
        videos: 'Videos',
        play: 'Play',
        simulate: 'Simulate',
        multiplayer: 'Multiplayer',
        decks: 'Decks',
        view: 'View',
        build: 'Build',
        config: 'Settings',
        audio: 'Audio',
        shortcuts: 'Shortcuts',
        languages: 'Languages',
        back: 'Back'
      };
    }

    // Para a tela de partidas
    components.matchScreen = function () {
      return {
        // Texto para ícone do menu de configurações
        configIcon: 'Settings',
        // Texto para ícone de jogadas
        getRostrumMoves: function ( parity ) {
          return m.languages.keywords.getParity( parity ) + ' ' + m.languages.keywords.getFlow( 'move', { plural: true } ).toLowerCase() +
                 ' from controllable sources';
        },
        // Texto sobre quantidade de jogadas não desenvolvidas
        getUndevelopedMoves: function ( moves ) {
          return 'Undeveloped moves:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto sobre quantidade de jogadas desenvolvidas
        getDevelopedMoves: function ( moves ) {
          return 'Developed moves:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto sobre quantidade de jogadas concluídas
        getFinishedMoves: function ( moves ) {
          return 'Finished moves:\n' + moves.reduce( ( accumulator, current ) => accumulator += `  ${ current.displayName }\n`, '' );
        },
        // Texto para ícones de quantidade de cartas por estado de atividade e uso
        getCardsByActivityDescription: function ( activity, usage, parity ) {
          return m.languages.keywords.getActivity( activity ) + ( usage ? ` and ${ usage }` : '' ) + ' cards from the ' +
                 m.languages.keywords.getParity( parity ).toLowerCase() + ' deck';
        },
        // Texto sobre quantidade de cartas por estado de atividade e uso
        getCardsByActivityValue: function ( cards ) {
          // Identificadores
          var hoverText = '';

          // Itera por propriedades alvo
          for( let key of [ 'beings', 'items', 'spells' ] ) {
            // Identificadores
            let targetCards = cards[ key ];

            // Caso não haja cartas alvo, avança para próxima iteração
            if( !targetCards.length ) continue;

            // Atualiza texto suspenso com quantidade de cartas alvo passada
            hoverText += `${ targetCards.length } ${ m.languages.keywords.getPrimitiveType( key, { plural: targetCards.length != 1 } ) }:\n`;

            // Lista designações completas das cartas alvo
            hoverText += targetCards.reduce( ( accumulator, current ) => accumulator += `  ${ current.content.designation.full }\n`, '' );

            // Adiciona divisão entre categorias de cartas alvo
            hoverText += '\n';
          }

          // Retorna texto suspenso
          return hoverText;
        },
        // Texto para ícones de quantidade de cartas por estado de preparo
        getCardsByReadinessDescription: function ( readiness, parity ) {
          return m.languages.keywords.getParity( parity ) + ' cards ' + m.languages.keywords.getReadiness( readiness ).toLowerCase() + ' and controllable';
        },
        // Texto sobre quantidade de cartas por estado de preparo
        getCardsByReadinessValue: function ( cards ) {
          return cards.reduce(
            ( accumulator, current ) => accumulator += `${ current.content.designation.full }${ current.slot ? ` (In ${ current.slot.name })` : '' }\n`, ''
          );
        },
        // Texto para ícone de pontos de destino de dado jogador
        getFatePointsDescription: function ( parity ) {
          return m.languages.keywords.getParity( parity ) + ' ' + m.languages.keywords.getMisc( 'fatePoints', { plural: true } ).toLowerCase();
        },
        // Texto sobre quantidade de pontos de destino de dado jogador
        getFatePointsValue: function ( currentFate ) {
          return `Transitive points: ${ currentFate.transitive }\n` +
                 `Intransitive points: ${ currentFate.intransitive }`;
        }
      };
    }

    // Para o menu de configurações da tela de partidas
    components.matchConfigMenu = function () {
      return {
        concede: 'Concede',
        exit: 'Exit'
      };
    }

    // Para paradas de combate
    components.combatBreak = function () {
      return {
        // Título
        caption: 'Combat Break',
        // Para o texto suspenso do ícone da seção de ataque
        getAttackHoverText: function ( attack, maneuverSource ) {
          // Identificadores
          var hoverText = '';

          // Adiciona texto sobre ataque
          hoverText += `"${ m.languages.keywords.getManeuver( attack.name ) }" attack`;

          // Adiciona arma de origem do ataque, se existente
          if( maneuverSource.source ) hoverText += `, via ${ maneuverSource.source.content.designation.title }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso do ícone da seção de defesa
        getDefenseHoverText: function ( defense, maneuverSource ) {
          // Identificadores
          var hoverText = '';

          // Adiciona texto sobre defesa
          hoverText += `"${ m.languages.keywords.getManeuver( defense.name ) }" defense`;

          // Adiciona arma de origem da defesa, se existente
          if( maneuverSource.source ) hoverText += `, via ${ maneuverSource.source.content.designation.title }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para os textos suspensos dos ícones das seções de modificadores de combate
        getCombatModifiersHoverText: function ( modifiers ) {
          // Identificadores
          var hoverText = '',
              isAttack = modifiers.type == 'attack';

          // Adiciona cabeçalho do texto
          hoverText += `${ isAttack ? 'Attack' : 'Defense' } Modifiers`;

          // Adiciona modificador da manobra, se existente
          if( modifiers.maneuver ) hoverText += `\n  ${ modifiers.maneuver } from the maneuver\'s modifier`;

          // Adiciona modificador de recuperação, se existente
          if( modifiers.recoveryBonus ) hoverText += `\n  ${ modifiers.recoveryBonus } from recovery`;

          // Adiciona modificador da mão inábil, se existente
          if( modifiers.secondaryHandWeapon ) hoverText += `\n  ${ modifiers.secondaryHandWeapon } from secondary hand`;

          // Adiciona modificador da arma multimanual, se existente
          if( modifiers.multiHandedWeapon ) hoverText += `\n  ${ modifiers.multiHandedWeapon } from multi-handed weapon`;

          // Adiciona modificador de alcance efetivo, se existente
          if( modifiers.effectiveRange ) hoverText += `\n  ${ modifiers.effectiveRange } from effective range`;

          // Adiciona modificador de 'Esquivar' ante 'Arremessar', se existente
          if( modifiers.throwPenalty ) hoverText += `\n  ${ modifiers.throwPenalty } from "${ m.languages.keywords.getManeuver( 'throw' ).toLowerCase() }" maneuver`;

          // Adiciona modificador de 'encorajado', se existente
          if( modifiers.emboldened ) hoverText += `\n  ${ modifiers.emboldened } from "${ m.languages.keywords.getCondition( 'emboldened' ).toLowerCase() }" condition`;

          // Adiciona modificador de 'enfurecido', se existente
          if( modifiers.enraged ) hoverText += `\n  ${ modifiers.enraged } from "${ m.languages.keywords.getCondition( 'enraged' ).toLowerCase() }" condition`;

          // Adiciona modificador de 'enfermo', se existente
          if( modifiers.diseased ) hoverText += `\n  ${ modifiers.diseased } from "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }" condition`;

          // Adiciona modificador do efeito de 'Poção da Ligeireza', se existente
          if( modifiers.nimblenessPotion ) hoverText += `\n  ${ modifiers.nimblenessPotion } from the effect of "Nimbleness Potion"`;

          // Adiciona modificador do efeito de Ixohch, se existente
          if( modifiers.ixohchEffect ) hoverText += `\n  ${ modifiers.ixohchEffect } from the effect of Ixohch`;

          // Adiciona modificador de agilidade, se existente
          if( modifiers.agility ) hoverText += `\n  ${ modifiers.agility } from ${ m.languages.keywords.getAttribute( 'agility' ).toLowerCase() }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso dos textos de pontos disponíveis
        availablePointsHoverText: 'Available Points',
        // Para o texto suspenso dos textos de pontos atribuídos
        assignedPointsHoverText: 'Assigned Points',
        // Para o texto suspenso dos dados de ataque
        getDieAttackHoverText: function ( attack ) {
          // Identificadores
          var hoverText = '';

          // Adiciona nome do ataque ao texto suspenso
          hoverText += m.languages.keywords.getManeuver( attack.name );

          // Adiciona danos do ataque ao texto suspenso
          for( let damageName in attack.damage )
            hoverText += `\n  ${ m.languages.snippets.getDamageType( damageName ) }: ${ attack.damage[ damageName ] }`;

          // Retorna texto suspenso
          return hoverText;
        },
        // Para o texto suspenso dos dados de defesa
        getDieDefenseHoverText: function ( defense ) {
          return m.languages.keywords.getManeuver( defense.name );
        },
        // Para o texto suspenso dos dados de bônus de ataque
        dieBonusHoverText: 'Bonus',
        // Para o texto suspenso dos dados de penalidades de ataque
        diePenaltyHoverText: 'Penalty',
        // Para o texto suspenso dos textos de dano total
        getTotalDamageHoverText: function ( damage, isInvalid = false, being ) {
          // Identificadores
          var textEnding = '\nClick on it to see the formula';

          // Mensagem para caso seja previsto infligimento direto do dano
          if( !isInvalid )
            return `Total ${ m.languages.keywords.getDamage( damage.name ).toLowerCase() } damage to be inflicted` + textEnding;

          // Para caso dano seja de fogo
          if( damage instanceof m.DamageFire ) {
            // Identificadores
            let beingSize = being.content.typeset.size,
                sizeScore = beingSize == 'large' ? 3 : beingSize == 'medium' ? 2 : 1,
                messageBody = sizeScore == 1 ? 'This damage will be inflicted at this segment\'s end' :
                  `This damage will be prorated and inflicted over ${ sizeScore } segments`;

            // Retorna mensagem própria do dano de fogo invalidado
            return messageBody + textEnding;
          }

          // Para caso dano seja de éter
          if( damage instanceof m.DamageEther ) {
            // Identificadores
            let points = being.content.stats.attributes.current.health;

            // Retorna mensagem própria do dano de éter invalidado
            return `This damage must have at least ${ points } point${ points == 1 ? '' : 's' } to be inflicted` + textEnding;
          }
        },
        // Para o texto suspenso da seta de retorno ao dano total
        shortDamageArrowHoverText: 'Back to compressed damage',
        // Para o texto suspenso sobre os pontos de ataque
        attackPointsHoverText: 'Attack points to be inflicted',
        // Para o texto suspenso do dano bruto
        getRawDamageHoverText: function ( damage ) {
          return `Raw ${ m.languages.keywords.getDamage( damage.name ).toLowerCase() } damage per attack point`;
        },
        // Para o texto suspenso da resistência natural
        naturalResistanceHoverText: 'Defender\'s natural resistance',
        // Para o texto suspenso da resistência do traje primário
        getPrimaryGarmentResistanceHoverText: function ( garmentCoverage ) {
          return 'Defender\'s primary garment resistance' + ( garmentCoverage ?
                 `\nOnly applied to the first ${ garmentCoverage == 1 ? 'attack point' : garmentCoverage + ' attack points' }` : ''
          );
        },
        // Para o texto suspenso da resistência do traje secundário
        getSecondaryGarmentResistanceHoverText: function ( garmentCoverage ) {
          return 'Defender\'s secondary garment resistance' + ( garmentCoverage ?
                 `\nOnly applied to the first ${ garmentCoverage == 1 ? 'attack point' : garmentCoverage + ' attack points' }` : ''
          );
        },
        // Para o texto suspenso sobre a penetração
        penetrationHoverText: 'Attack\'s penetration',
        // Para o texto suspenso sobre o modificador de destino
        fateModifierHoverText: 'Fate modifier',
        // Para o texto suspenso sobre o dano resultante
        resultingDamageHoverText: 'Resulting damage',
        // Para o texto suspenso dos ícones de pontos de destino
        getFatePointHoverText: function ( parity, isAssigned = false, isEmbedded = false ) {
          return m.languages.keywords.getParity( parity ) + ' ' + m.languages.keywords.getMisc( 'fatePoints' ).toLowerCase() + ': ' +
                ( isAssigned ? 'assigned' : 'not assigned' ) + ( isAssigned && isEmbedded ? ' (embedded)' : '' );
        }
      };
    }

    // Para as listas de cartas
    components.cardsList = function () {
      return {
        price: 'Price',
        mainDeckCoins: 'Available coins for the main deck'
      };
    }

    // Para a tela de visualização de baralhos
    components.deckViewingScreen = function () {
      return {
        deckTable: {
          caption: 'Table of Built Decks',
          name: 'Name',
          created: 'Created Date',
          modified: 'Modified Date',
          matches: 'Matches',
          wins: 'Wins',
          draws: 'Draws',
          losses: 'Losses',
          disconnects: 'Disconnects'
        },
        moveUpArrow: 'Move up row.',
        moveDownArrow: 'Move down row.',
        backArrow: 'Exit to the opening screen.'
      };
    }

    // Para as molduras de cartas e casas
    components.componentFrame = function ( sourceComponent ) {
      // Identificadores
      var frameData = {},
          getFrameData = sourceComponent instanceof m.Being ? getBeingData :
                         sourceComponent instanceof m.Item ? getItemData :
                         sourceComponent instanceof m.Spell ? getSpellData :
                         sourceComponent instanceof m.CardSlot ? getSlotData : null;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Card, m.CardSlot ].some( constructor => sourceComponent instanceof constructor ) );

      // Popula dados da moldura em função do tipo do componente alvo
      getFrameData();

      // Adiciona à moldura texto sobre inativação do efeito da carta
      frameData.disabledEffect = 'Disabled effect';

      // Adiciona à moldura operação para popular dados dos textos suspensos sobre cartas na seção de vinculados
      frameData.getCardAttachment = function ( attachment ) {
        // Identificadores
        var attachmentText = '',
            directCardAttachments = Object.values( attachment.attachments ).flat().filter( subAttachment => subAttachment instanceof m.Card );

        // Inicia linha de texto suspenso com descritor de obra-prima, se aplicável
        if( attachment.content.typeset.grade == 'masterpiece' ) attachmentText += `${ m.languages.keywords.getItemGrade( 'masterpiece' ) } `;

        // Adiciona título de alvo
        attachmentText += attachment.content.designation.title;

        // Caso vinculado não esteja em uma casa e não seja um equipamento embutido, encerra função
        if( !attachment.slot && !attachment.isEmbedded ) return attachmentText;

        // Inclui adendo sobre origem de alvo
        attachmentText += attachment.slot ? ` (In ${ attachment.slot.name }` : ' (Embedded';

        // Inclui cartas vinculadas a alvo, se existentes
        if( directCardAttachments.length ) {
          // Inicia frase sobre vinculados
          attachmentText += '; with ';

          // Acrescenta nome de vinculados
          attachmentText += directCardAttachments.map( subAttachment =>
            ( subAttachment.isEmbedded ? 'embedded ' : '' ) +
            subAttachment.content.designation.title.toLowerCase() +
            ( subAttachment.isEmbedded ? '' : subAttachment.slot ? ` in ${ subAttachment.slot.name }` : '' )
          ).join( ', ' );
        }

        // Encerra adendo e texto sobre vinculado atual
        return attachmentText += ')\n';
      }

      // Caso componente seja uma carta, adiciona à moldura trecho que indica autoria da ilustração do componente de temática
      if( sourceComponent instanceof m.Card ) frameData.artist = `Illustrated by ${ m.data.creators.getImageArtist( sourceComponent.content.image ) }`;

      // Retorna dados da moldura
      return frameData;

      // Retorna dados de entes
      function getBeingData() {
        // Identificadores
        var cardTypeset = sourceComponent.content.typeset;

        // Texto suspenso para condições
        frameData.conditions = 'Being\'s Conditions';

        // Texto suspenso para seção de ambientes
        frameData.environment = sourceComponent.summoner ?
          `Summoned by ${ sourceComponent.summoner.content.designation.full } in ${ sourceComponent.summoner.slot.name }` +
          ( sourceComponent.owner ? ' (Owned)' : '' ) : '';

        // Texto suspenso para pontos gastos de manobras
        frameData.spentPoints = 'Spent points';

        // Texto suspenso para seção conjunta de canalização e vinculados
        frameData.channelingAndAttachments =
          `${ m.languages.keywords.getStat( 'channeling' ) } & ${ m.languages.keywords.getMisc( 'attachment', { plural: true } ) }`;

        // Texto suspenso para valores do mana de sendas
        frameData.manaQuantity = {
          path: 'Path Mana', total: 'Total Mana'
        };

        // Encerra execução da função caso ente não seja um humanoide
        if( !( sourceComponent instanceof m.Humanoid ) ) return;

        // Texto suspenso para cabeçalho de raça
        frameData.traitCaption =
          `${ m.languages.keywords.getRace( cardTypeset.race ) } ${ m.languages.keywords.getMisc( 'trait', { plural: true } ) }`;

        // Texto suspenso para ocupação de mãos
        frameData.hands = {
          primary: 'Primary hand with ',
          secondary: 'Secondary hand with ',
          all: 'Both hands with '
        };
      }

      // Retorna dados de equipamentos
      function getItemData() {
        // Define texto suspenso para seção de alvo
        frameData.target = ( function () {
          // Para caso componente não seja vinculável
          if( !sourceComponent.isToAttach ) return 'Unattachable';

          // Para caso componente não tenha um vinculante
          if( !sourceComponent.attacher ) return 'Attachable';

          // Captura dados sobre dono e vinculante do equipamento
          let text = '',
              [ sourceOwner, sourceAttacher ] = [ sourceComponent.owner, sourceComponent.attacher ],
              ownerSlotSnippet = sourceOwner.slot ? ` in ${ sourceOwner.slot.name }` : '',
              attacherSlotSnippet = sourceAttacher.slot ? ` in ${ sourceAttacher.slot.name }` : ownerSlotSnippet;

          // Acrescenta informação sobre dono do equipamento
          text += `Owned by ${ sourceOwner.content.designation.full }${ ownerSlotSnippet }`;

          // Caso o vinculante seja diferente do dono, acrescenta informação sobre o vinculante
          if( sourceAttacher != sourceOwner ) text += `\nAttached to ${ sourceAttacher.content.designation.title }${ attacherSlotSnippet }`;

          // Retorna texto
          return text;
        } )();
      }

      // Retorna dados de magias
      function getSpellData() {
        // Define texto suspenso para seção de alvo
        frameData.target = ( function () {
          // Identificadores
          var text = '',
              sourceOwner = sourceComponent.owner,
              ownerSlotSnippet = sourceOwner?.slot ? ` in ${ sourceOwner.slot.name }` : '';

          // Caso magia tenha um dono, acrescenta essa informação
          if( sourceOwner ) text += `Owned by ${ sourceOwner.content.designation.full }${ ownerSlotSnippet }\n`;

          // Caso componente não seja vinculável, adiciona essa informação e retorna texto
          if( !sourceComponent.isToAttach ) return text += 'Unattachable';

          // Caso componente não tenha um vinculante, adiciona essa informação e retorna texto
          if( !sourceComponent.attacher ) return text += 'Attachable';

          // Caso vinculante do componente seja uma casa, adiciona essa informação e retorna texto
          if( sourceComponent.attacher instanceof m.CardSlot ) return text += `Attached to ${ sourceComponent.attacher.name }`;

          // Captura dados do vinculante
          let sourceAttacher = sourceComponent.attacher,
              attacherName = sourceAttacher.content.designation[ sourceAttacher instanceof m.Being ? 'full' : 'title' ],
              cardSlot = sourceAttacher.slot ?? sourceAttacher.owner?.slot ?? null,
              attacherSlotSnippet = cardSlot ? ` in ${ cardSlot.name }` : '';

          // Inclui informação sobre vinculante, e retorna texto
          return text += `Attached to ${ attacherName }${ attacherSlotSnippet }`;
        } )();

        // Texto suspenso para seção de polaridade
        frameData.oppositeSpell = sourceComponent.oppositeSpell ? `Opposite spell: ${ new sourceComponent.oppositeSpell().content.designation.title }` : '';

        // Textos suspensos para seção do custo de fluxo
        frameData.flowCosts = {
          channeling: 'Segments for channeling', sustaining: 'Segments for sustaining'
        };

        // Textos suspensos para seção do custo de mana
        frameData.manaCosts = {
          minChanneling: 'Minimum channeling cost', maxChanneling: 'Maximum channeling cost',
          sustainingCost: 'Sustaining cost', maxUsageCost: 'Maximum usage cost'
        };
      }

      // Retorna dados de casas
      function getSlotData() {
        // Definição do título da moldura
        frameData.title = `${ sourceComponent.name } ${ m.languages.keywords.getAttachability( 'CardSlot' ) }`;
      }
    }

    // Para as molduras de baralhos
    components.deckFrame = function () {
      return {
        getSubtitle: ( minCards, maxCards ) => minCards ? `choose between ${ minCards } and ${ maxCards } cards` : `choose up to ${ maxCards } cards`,
        getCreditsSource: function ( deckCredits ) {
          // Identificadores
          var creditsText = '';

          // Adiciona créditos de equipamentos, se aplicável
          if( deckCredits.items ) creditsText += `  ${ m.languages.keywords.getPrimitiveType( 'item', { plural: true } ) }: ${ deckCredits.items }\n`;

          // Adiciona créditos de magias, quando aplicável
          for( let path of [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ] )
            if( deckCredits.spells[ path ] ) creditsText += `  ${ m.languages.keywords.getPath( path ) }: ${ deckCredits.spells[ path ] }\n`;

          // Caso haja ao menos uma fonte de crédito, adiciona cabeçalho
          if( creditsText ) creditsText = 'Credits:\n' + creditsText;

          // Retorna texto sobre créditos
          return creditsText.trimEnd();
        },
        availableCoins: 'Available coins',
        totalCoins: 'Total coins',
        spentCoins: 'Spent coins',
        spentCredits: 'Spent credits',
        invalidDeck: 'Invalid deck',
        validation: {
          getIsValidText: ( isDeckSet ) => 'The ' + ( isDeckSet ? 'deck set' : 'deck' ) + ' is valid.',
          getMinDeckCards: function ( invalidEntries ) {
            // Identificadores
            var validationText = 'It is required to add at least ';

            // Itera por entradas inválidas
            for( let entry of invalidEntries ) {
              // Identificadores
              let [ deckType, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ number == 1 ? 'card' : 'cards' } to the ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } and `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / and $/, '.' );
          },
          getMaxDeckCards: function ( invalidEntries ) {
            // Identificadores
            var validationText = 'There ';

            // Itera por entradas inválidas
            for( let entry of invalidEntries ) {
              // Identificadores
              let [ deckType, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText +=
                `${ number == 1 ? 'is' : 'are' } ${ number } exceeding ${ number == 1 ? 'card' : 'cards' } in the ` +
                `${ m.languages.keywords.getDeck( deckType ).toLowerCase() } and `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / and $/, '.' );
          },
          getMaxCoinsSpent: function ( invalidKeys ) {
            // Identificadores
            var validationText = 'The number of coins spent ';

            // Itera por chaves inválidas
            for( let key of invalidKeys ) {
              // Identificadores
              let deckType = key;

              // Ajusta texto de validação
              validationText += `on the ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } and `;
            }

            // Remove conjunção final, e retorna texto resultante
            return validationText = validationText.replace( / and $/, ' is greater than the maximum allowed.' );
          },
          getMaxSameCards: () => 'One or more cards have exceeded their availability limit.',
          getCommandCards: function ( validationType, invalidData ) {
            // Identificadores
            var validationText = validationType == 'minCommandCards' ? 'It is required to add to the main deck at least: ' : 'It is required to remove: ',
                { race = {}, roleType = {}, experienceLevel = {}, grade = {}, constraints = [] } = invalidData[ validationType ],
                [ raceEntries, roleEntries, experienceEntries, gradeEntries ] = [
                  Object.entries( race ), Object.entries( roleType ), Object.entries( experienceLevel ), Object.entries( grade )
                ];

            // Itera por tipos primitivos inválidos
            for( let primitiveType of [ 'creature', 'item', 'spell' ] ) {
              // Identificadores
              let number = invalidData[ validationType ][ primitiveType ];

              // Filtra tipos primitivos válidos
              if( !number ) continue;

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getPrimitiveType( primitiveType, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de raças inválidas
            for( let entry of raceEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getRace( target, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de tipos de atuação inválidas
            for( let entry of roleEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getRoleType( target, { person: true, plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de níveis de experiência inválidos
            for( let entry of experienceEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText +=
                `${ number } ${ m.languages.keywords.getExperienceLevel( target ).toLowerCase() } ` +
                `${ m.languages.keywords.getPrimitiveType( 'creature', { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por entradas de categorias de equipamento inválidas
            for( let entry of gradeEntries ) {
              // Identificadores
              let [ target, number ] = [ entry[ 0 ], entry[ 1 ] ];

              // Ajusta texto de validação
              validationText += `${ number } ${ m.languages.keywords.getItemGrade( target, { plural: number != 1 } ).toLowerCase() }; `;
            }

            // Itera por superescopos com restrições especiais inválidas
            for( let superScope of constraints ) {
              // Ajusta texto de validação
              validationText +=
                `${ m.languages.keywords.getPrimitiveType( superScope ).toLowerCase() } cards that violate special restrictions; `;
            }

            // Remove pontuação final, e retorna texto resultante
            return validationText = validationText.replace( /; $/, '.' );
          }
        }
      };
    }

    // Para a moldura de registro de partidas
    components.logFrame = function () {
      return {
        getTitle: function ( stageName, stageNumber ) {
          // Identificadores
          var stageText = m.languages.keywords.getFlow( stageName.replace( /(^|-)\d+?(-|$)/g, '' ) );

          // Caso estágio seja uma etapa e seu número seja menor que 2, ajusta-o para que seja omitido
          if( stageName.includes( '-step-' ) && stageNumber < 2 ) stageNumber = 0;

          // Retorna texto do cabeçalho do registro de eventos
          return `${ stageText }${ stageNumber ? ' ' + stageNumber : '' } Log`;
        },
        beginArrow: 'View stage with the most previous events',
        previousArrow: 'View previous events',
        nextArrow: 'View later events',
        endArrow: 'View stage with the most later events'
      };
    }

    // Para a moldura de escolha do comandante
    components.commanderFrame = function () {
      return {
        title: 'Choose the Commander',
        subtitle: 'with a double-click'
      };
    }

    // Para a modal de áudio
    components.audioModal = function () {
      return {
        soundTracks: 'Music Volume',
        soundEffects: 'Effect Volume'
      };
    }

    // Tabela de atalhos de uma tela
    components.shortcutsTable = function ( screenName ) {
      return {
        caption: `${ m.languages.keywords.getScreen( screenName ) }'s Shortcuts`,
        operation: 'Operation',
        trigger: 'Trigger',
        combinations: 'Combinations',
        body: this.screenShortcuts()[ screenName ] ?? []
      };
    }

    // Descrição dos atalhos de telas
    components.screenShortcuts = function () {
      return {
        // Atalhos da tela de partidas
        'match-screen': [
          {
            description: 'Release the flow',
            trigger: '"Enter" key',
            combinations: []
          }, {
            description: 'Suspend the flow',
            trigger: '"Space" key',
            combinations: []
          }, {
            description: 'Show stage above the current one',
            trigger: '"Up Arrow" key',
            combinations: []
          }, {
            description: 'Show stage below the current one',
            trigger: '"Down Arrow" key',
            combinations: []
          }, {
            description: 'Manipulate left frame',
            trigger: '"1" key',
            combinations: [
              { description: 'Toggle visibility' },
              { description: 'Show logs', ctrlKey: true, altKey: true },
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate right frame',
            trigger: '"2" key',
            combinations: [
              { description: 'Toggle visibility' },
              { description: 'Show logs', ctrlKey: true, altKey: true },
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate top-middle frame',
            trigger: '"3" key',
            combinations: [
              { description: 'Toggle visibility' },
              { description: 'Show logs', ctrlKey: true, altKey: true },
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate bottom-middle frame',
            trigger: '"4" key',
            combinations: [
              { description: 'Toggle visibility' },
              { description: 'Show logs', ctrlKey: true, altKey: true },
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Filter cards by activity',
            trigger: '"5" key (Holding down)',
            combinations: [
              { description: 'Suspended' },
              { description: 'Active', ctrlKey: true },
              { description: 'Withdrawn', shiftKey: true },
              { description: 'Ended', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Filter beings by readiness',
            trigger: '"6" key (Holding down)',
            combinations: [
              { description: 'Unprepared' },
              { description: 'Prepared', ctrlKey: true },
              { description: 'Occupied', shiftKey: true },
              { description: 'Exhausted', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Show only cards related to the one over which the cursor is',
            trigger: '"7" key (Holding down)',
            combinations: [
              { description: 'Owned' },
              { description: 'Attached', ctrlKey: true },
              { description: 'In use', shiftKey: true }
            ]
          }, {
            description: 'Toggle card designations\' visibility',
            trigger: '"9" key',
            combinations: [
              { description: 'In the pools' },
              { description: 'In the field', ctrlKey: true },
              { description: 'In the frames', shiftKey: true },
              { description: 'In all components', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate screen settings',
            trigger: '"0" key',
            combinations: [
              { description: 'Show shortcuts' },
              { description: 'Toggle activity and readiness sections\' visibility', shiftKey: true },
              { description: 'Toggle information mode', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Exit screen',
            trigger: '"Esc" key',
            combinations: []
          }
        ],
        // Atalhos da tela de montagem de baralhos
        'deck-building-screen': [
          {
            description: 'Manipulate visible filter bar',
            trigger: 'Left click',
            combinations: [
              { description: 'Toggle clicked filter' },
              { description: 'Activate filters from the clicked filter\'s set other than it', ctrlKey: true }
            ]
          }, {
            description: 'Browse visible card list',
            trigger: '"Left Arrow" key',
            combinations: [
              { description: 'Show 1 previous card' },
              { description: 'Show 6 previous cards', ctrlKey: true }
            ]
          }, {
            description: 'Browse visible card list',
            trigger: '"Right Arrow" key',
            combinations: [
              { description: 'Show 1 following card' },
              { description: 'Show 6 following cards', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate left card frame',
            trigger: '"1" key',
            combinations: [
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate deck frame',
            trigger: '"2" key',
            combinations: [
              { description: 'Toggle side deck', ctrlKey: true },
              { description: 'Toggle validation', shiftKey: true }
            ]
          }, {
            description: 'Manipulate right card frame',
            trigger: '"3" key',
            combinations: [
              { description: 'Toggle focus', shiftKey: true, altKey: true },
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Clear filters from the visible bar',
            trigger: '"4" key',
            combinations: [
              { description: 'All' },
              { description: 'First set', ctrlKey: true },
              { description: 'Second set', shiftKey: true },
              { description: 'Third set', altKey: true }
            ]
          }, {
            description: 'Toggle card designations\' visibility',
            trigger: '"9" key',
            combinations: [
              { description: 'In the lists', ctrlKey: true },
              { description: 'In the frames', shiftKey: true },
              { description: 'In all components', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate screen settings',
            trigger: '"0" key',
            combinations: [
              { description: 'Show shortcuts' },
              { description: 'Change layout', ctrlKey: true },
              { description: 'Toggle information mode', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Exit screen',
            trigger: '"Esc" key',
            combinations: []
          }
        ],
        // Atalhos da tela de visualização de baralhos
        'deck-viewing-screen': [
          {
            description: 'Modify selected deck',
            trigger: '"Enter" key',
            combinations: [
              { description: 'Edit' },
              { description: 'Copy', ctrlKey: true }
            ]
          }, {
            description: 'Modify selected deck',
            trigger: '"Delete" key',
            combinations: [
              { description: 'Remove' },
              { description: 'Rename', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate table rows',
            trigger: '"Up Arrow" key',
            combinations: [
              { description: 'Select row above the selected one' },
              { description: 'Move selected row up', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate table rows',
            trigger: '"Down Arrow" key',
            combinations: [
              { description: 'Select row below the selected one' },
              { description: 'Move selected row down', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate first table row',
            trigger: '"Home" key',
            combinations: [
              { description: 'Select row' },
              { description: 'Move selected row to that position', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate last table row',
            trigger: '"End" key',
            combinations: [
              { description: 'Select row' },
              { description: 'Move selected row to that position', ctrlKey: true }
            ]
          }, {
            description: 'Manipulate card frame',
            trigger: '"1" key',
            combinations: [
              { description: 'Toggle stats', ctrlKey: true },
              { description: 'Toggle effects', shiftKey: true },
              { description: 'Toggle command', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Manipulate deck frame',
            trigger: '"2" key',
            combinations: [
              { description: 'Toggle side deck', ctrlKey: true }
            ]
          }, {
            description: 'Toggle card designations\' visibility',
            trigger: '"9" key',
            combinations: [
              { description: 'In the frames', shiftKey: true }
            ]
          }, {
            description: 'Manipulate screen settings',
            trigger: '"0" key',
            combinations: [
              { description: 'Show shortcuts' },
              { description: 'Toggle information mode', ctrlKey: true, shiftKey: true }
            ]
          }, {
            description: 'Exit screen',
            trigger: '"Esc" key',
            combinations: []
          }
        ]
      };
    }
  }
} )();
