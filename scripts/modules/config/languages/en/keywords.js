// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'en' ) return;

  // Identificadores
  const keywords = m.languages.keywords = {};

  // Propriedades iniciais
  defineProperties: {
    // Para paridades
    keywords.getParity = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'odd':
          return 'Odd';
        case 'even':
          return 'Even';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para atividades
    keywords.getActivity = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'inUse':
        case 'in-use':
        case 'active':
          return 'Active';
        case 'inactive':
          return 'Inactive';
        case 'suspended':
          return 'Suspended';
        case 'withdrawn':
          return 'Withdrawn';
        case 'ended':
          return 'Ended';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para estado de preparo
    keywords.getReadiness = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'unprepared':
          return 'Unprepared';
        case 'prepared':
          return 'Prepared';
        case 'occupied':
          return 'Occupied';
        case 'exhausted':
          return 'Exhausted';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos primitivos
    keywords.getPrimitiveType = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'being':
        case 'beings':
          return config.plural ? 'Beings' : 'Being';
        case 'item':
        case 'items':
          return config.plural ? 'Items' : 'Item';
        case 'spell':
        case 'spells':
          return config.plural ? 'Spells' : 'Spell';
        case 'creature':
        case 'creatures':
          return config.plural ? 'Creatures' : 'Creature';
        case 'asset':
        case 'assets':
          return config.plural ? 'Assets' : 'Asset';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos acrescidos
    keywords.getTypeset = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'environment':
          return config.plural ? 'Environments' : 'Environment';
        case 'attachability':
          return config.plural ? 'Attachabilities' : 'Attachability';
        case 'race':
          return config.plural ? 'Races' : 'Race';
        case 'equipage':
          return config.plural ? 'Equipages' : 'Equipage';
        case 'path':
          return config.plural ? 'Paths' : 'Path';
        case 'size':
          return config.plural ? 'Sizes' : 'Size';
        case 'weight':
          return config.plural ? 'Weights' : 'Weight';
        case 'polarity':
          return config.plural ? 'Polarities' : 'Polarity';
        case 'role':
          return config.plural ? 'Roles' : 'Role';
        case 'roleType':
          return config.plural ? 'Role Types' : 'Role Type';
        case 'experience':
          return config.plural ? 'Experiences' : 'Experience';
        case 'experienceLevel':
          return config.plural ? 'Experience Levels' : 'Experience Level';
        case 'grade':
          return config.plural ? 'Grades' : 'Grade';
        case 'wielding':
          return config.plural ? 'Wieldings' : 'Wielding';
        case 'usage':
          return config.plural ? 'Usages' : 'Usage';
        case 'duration':
          return config.plural ? 'Durations' : 'Duration';
        case 'availability':
          return config.plural ? 'Availabilities' : 'Availability';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para estatísticas diversas
    keywords.getStat = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'attributes':
          return config.plural ? 'Attributes' : 'Attribute';
        case 'fighting':
          return config.plural ? 'Fightings' : 'Fighting';
        case 'channeling':
          return config.plural ? 'Channelings' : 'Channeling';
        case 'effectivity':
          return config.plural ? 'Effectivities' : 'Effectivity';
        case 'resistances':
          return config.plural ? 'Resistances' : 'Resistance';
        case 'modifier':
          return config.plural ? 'Modifiers' : 'Modifier';
        case 'penetration':
          return config.plural ? 'Penetrations' : 'Penetration';
        case 'recovery':
          return config.plural ? 'Recoveries' : 'Recovery';
        case 'range':
          return config.plural ? 'Ranges' : 'Range';
        case 'coverage':
          return config.plural ? 'Coverages' : 'Coverage';
        case 'conductivity':
          return config.plural ? 'Conductivities' : 'Conductivity';
        case 'mana':
          return 'Mana';
        case 'flowCost':
          return config.plural ? 'Flow Costs' : 'Flow Cost';
        case 'manaCost':
          return config.plural ? 'Mana Costs' : 'Mana Cost';
        case 'persistenceCost':
          return config.plural ? 'Persistence Costs' : 'Persistence Cost';
        case 'potency':
          return config.plural ? 'Potencies' : 'Potency';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para atributos
    keywords.getAttribute = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'health':
          return 'Health';
        case 'initiative':
          return 'Initiative';
        case 'stamina':
          return 'Stamina';
        case 'agility':
          return 'Agility';
        case 'will':
          return 'Will';
        case 'energy':
          return 'Energy';
        case 'power':
          return 'Power';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para vinculações
    keywords.getAttachability = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case undefined:
          return 'Null';
        case 'Being':
          return config.plural ? 'Beings' : 'Being';
        case 'PhysicalBeing':
          return config.plural ? 'Physical Beings' : 'Physical Being';
        case 'BioticBeing':
          return config.plural ? 'Biotic Beings' : 'Biotic Being';
        case 'Humanoid':
          return config.plural ? 'Humanoids' : 'Humanoid';
        case 'Beast':
          return config.plural ? 'Beasts' : 'Beast';
        case 'Construct':
          return config.plural ? 'Constructs' : 'Construct';
        case 'AstralBeing':
          return config.plural ? 'Astral Beings' : 'Astral Being';
        case 'Item':
          return config.plural ? 'Items' : 'Item';
        case 'Weapon':
          return config.plural ? 'Weapons' : 'Weapon';
        case 'Garment':
          return config.plural ? 'Garments' : 'Garment';
        case 'Implement':
          return config.plural ? 'Implements' : 'Implement';
        case 'Spell':
          return config.plural ? 'Spells' : 'Spell';
        case 'CardSlot':
          return config.plural ? 'Card Slots' : 'Card Slot';
        case 'FieldGridSlot':
          return config.plural ? 'Grid Card Slots' : 'Grid Card Slot';
        case 'EngagementZone':
          return config.plural ? 'Engagement Zones' : 'Engagement Zone';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para usos
    keywords.getUsage = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'static':
          return 'Static';
        case 'dynamic':
          return 'Dynamic';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para raças
    keywords.getRace = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'human':
          return config.plural ? 'Humans' : 'Human';
        case 'orc':
          return config.plural ? 'Orcs' : 'Orc';
        case 'elf':
          return config.plural ? 'Elves' : 'Elf';
        case 'dwarf':
          return config.plural ? 'Dwarves' : 'Dwarf';
        case 'halfling':
          return config.plural ? 'Halflings' : 'Halfling';
        case 'gnome':
          return config.plural ? 'Gnomes' : 'Gnome';
        case 'goblin':
          return config.plural ? 'Goblins' : 'Goblin';
        case 'ogre':
          return config.plural ? 'Ogres' : 'Ogre';
        case 'bear':
          return config.plural ? 'Bears' : 'Bear';
        case 'wolf':
          return config.plural ? 'Wolves' : 'Wolf';
        case 'swarm':
          return config.plural ? 'Swarms' : 'Swarm';
        case 'undead':
          return config.plural ? 'Undeads' : 'Undead';
        case 'salamander':
          return config.plural ? 'Salamanders' : 'Salamander';
        case 'golden-salamander':
        case 'goldenSalamander':
          return config.plural ? 'Golden Salamanders' : 'Golden Salamander';
        case 'indigo-salamander':
        case 'indigoSalamander':
          return config.plural ? 'Indigo Salamanders' : 'Indigo Salamander';
        case 'golem':
          return config.plural ? 'Golems' : 'Golem';
        case 'tulpa':
          return config.plural ? 'Tulpas' : 'Tulpa';
        case 'spirit':
          return config.plural ? 'Spirits' : 'Spirit';
        case 'demon':
          return config.plural ? 'Demons' : 'Demon';
        case 'angel':
          return config.plural ? 'Angels' : 'Angel';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para ambientes
    keywords.getEnvironment = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'physical':
          return 'Physical';
        case 'astral':
          return 'Astral';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para nomes de atuação
    keywords.getRoleName = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'civilian':
          return config.plural ? 'Civilians' : 'Civilian';
        case 'maceman':
          return config.plural ? 'Macemen' : 'Maceman';
        case 'swordsman':
          return config.plural ? 'Swordsmen' : 'Swordsman';
        case 'axeman':
          return config.plural ? 'Axemen' : 'Axeman';
        case 'spearman':
          return config.plural ? 'Spearmen' : 'Spearman';
        case 'slingman':
          return config.plural ? 'Slingmen' : 'Slingman';
        case 'bowman':
          return config.plural ? 'Bowmen' : 'Bowman';
        case 'crossbowman':
          return config.plural ? 'Crossbowmen' : 'Crossbowman';
        case 'theurge':
          return config.plural ? 'Theurges' : 'Theurge';
        case 'warlock':
          return config.plural ? 'Warlocks' : 'Warlock';
        case 'wizard':
          return config.plural ? 'Wizards' : 'Wizard';
        case 'sorcerer':
          return config.plural ? 'Sorcerers' : 'Sorcerer';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tipos de atuação
    keywords.getRoleType = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'melee':
          return config.person && config.plural  ? 'Melee Combatants' :
                 config.person                   ? 'Melee Combatant' :
                 config.plural                   ? 'Melee Combats' :
                                                   'Melee Combat';
        case 'ranged':
          return config.person && config.plural  ? 'Ranged Combatants' :
                 config.person                   ? 'Ranged Combatant' :
                 config.plural                   ? 'Ranged Combats' :
                                                   'Ranged Combat';
        case 'magical':
          return config.person && config.plural  ? 'Magical Combatants' :
                 config.person                   ? 'Magical Combatant' :
                 config.plural                   ? 'Magical Combats' :
                                                   'Magical Combat';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para categorias de experiência
    keywords.getExperienceGrade = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'commander':
          return config.plural ? 'Commanders' : 'Commander';
        case 'rookie':
          return config.plural ? 'Rookies' : 'Rookie';
        case 'regular':
          return config.plural ? 'Regulars' : 'Regular';
        case 'veteran':
          return config.plural ? 'Veterans' : 'Veteran';
        case 'elite':
          return config.plural ? 'Elites' : 'Elite';
        case 'novice':
          return config.plural ? 'Novices' : 'Novice';
        case 'disciple':
          return config.plural ? 'Disciples' : 'Disciple';
        case 'adept':
          return config.plural ? 'Adepts' : 'Adept';
        case 'master':
          return config.plural ? 'Masters' : 'Master';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para níveis de experiência
    keywords.getExperienceLevel = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 0:
        case '0':
          return 'Level 0';
        case 1:
        case '1':
          return 'Level 1';
        case 2:
        case '2':
          return 'Level 2';
        case 3:
        case '3':
          return 'Level 3';
        case 4:
        case '4':
          return 'Level 4';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para equipagens
    keywords.getEquipage = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'weapon':
          return config.plural ? 'Weapons' : 'Weapon';
        case 'garment':
          return config.plural ? 'Garments' : 'Garment';
        case 'implement':
          return config.plural ? 'Implements' : 'Implement';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para tamanhos
    keywords.getSize = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case null:
        case 'null':
          return 'Null';
        case 'small':
          return 'Small';
        case 'medium':
          return 'Medium';
        case 'large':
          return 'Large';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para pesos
    keywords.getWeight = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'light':
          return 'Light';
        case 'medium':
          return 'Medium';
        case 'heavy':
          return 'Heavy';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para categorias de equipamentos
    keywords.getItemGrade = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'ordinary':
          return config.plural ? 'Ordinaries' : 'Ordinary';
        case 'masterpiece':
          return config.plural ? 'Masterpieces' : 'Masterpiece';
        case 'artifact':
          return config.plural ? 'Artifacts' : 'Artifact';
        case 'relic':
          return config.plural ? 'Relics' : 'Relic';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para empunhaduras
    keywords.getWielding = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'non-handed':
          return 'Non-Handed';
        case 'one-handed':
          return 'One-Handed';
        case 'two-handed':
          return 'Two-Handed';
        case 'multi-handed':
          return 'Multi-Handed';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para armas
    keywords.getWeapon = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'unarmed':
          return 'Unarmed';
        case 'mace':
          return config.plural ? 'Maces' : 'Mace';
        case 'sword':
          return config.plural ? 'Swords' : 'Sword';
        case 'axe':
          return config.plural ? 'Axes' : 'Axe';
        case 'spear':
          return config.plural ? 'Spears' : 'Spear';
        case 'shield':
          return config.plural ? 'Shields' : 'Shield';
        case 'sling':
          return config.plural ? 'Slings' : 'Sling';
        case 'bow':
          return config.plural ? 'Bows' : 'Bow';
        case 'crossbow':
          return config.plural ? 'Crossbows' : 'Crossbow';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para manobras
    keywords.getManeuver = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^maneuver-/, '' ) ) {
        case 'unarmed':
          return 'Unarmed';
        case 'bash':
          return 'Bash';
        case 'cut':
          return 'Cut';
        case 'thrust':
          return 'Thrust';
        case 'shoot':
          return 'Shoot';
        case 'throw':
          return 'Throw';
        case 'scratch':
          return 'Scratch';
        case 'bite':
          return 'Bite';
        case 'swarm':
          return 'Swarm';
        case 'burn':
          return 'Burn';
        case 'fire-bolt':
          return 'Fire Bolt';
        case 'mana-bolt':
          return 'Mana Bolt';
        case 'rapture':
          return 'Rapture';
        case 'repress':
          return 'Repress';
        case 'dodge':
          return 'Dodge';
        case 'block':
          return 'Block';
        case 'repel':
          return 'Repel';
        case 'cover':
          return 'Cover';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para danos
    keywords.getDamage = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^damage-/, '' ) ) {
        case 'blunt':
          return 'Blunt';
        case 'slash':
          return 'Slash';
        case 'pierce':
          return 'Pierce';
        case 'fire':
          return 'Fire';
        case 'shock':
          return 'Shock';
        case 'mana':
          return 'Mana';
        case 'raw':
          return 'Raw';
        case 'ether':
          return 'Ether';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para sendas
    keywords.getPath = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'metoth':
          return 'Metoth';
        case 'enoth':
          return 'Enoth';
        case 'powth':
          return 'Powth';
        case 'voth':
          return 'Voth';
        case 'faoth':
          return 'Faoth';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para polaridades
    keywords.getPolarity = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'positive':
          return 'Positive';
        case 'apolar':
          return 'Apolar';
        case 'negative':
          return 'Negative';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para durações
    keywords.getDuration = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'immediate':
          return 'Immediate';
        case 'sustained':
          return 'Sustained';
        case 'sustained-spiking':
          return 'Spiking';
        case 'sustained-constant':
          return 'Constant';
        case 'persistent':
          return 'Persistent';
        case 'permanent':
          return 'Permanent';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para baralhos
    keywords.getDeck = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'mainDeck':
        case 'main-deck':
          return config.plural ? 'Main Decks' : 'Main Deck';
        case 'sideDeck':
        case 'side-deck':
          return config.plural ? 'Side Decks' : 'Side Deck';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para unidades de fluxo e estágios
    keywords.getFlow = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'round':
          return config.plural ? 'Rounds' : 'Round';
        case 'setup-phase':
          return config.plural ? 'Setup Phases' : 'Setup Phase';
        case 'battle-phase':
          return config.plural ? 'Battle Phases' : 'Battle Phase';
        case 'step':
          return config.plural ? 'Steps' : 'Step';
        case 'arrangement-step':
          return config.plural ? 'Arrangement Steps' : 'Arrangement Step';
        case 'deployment-step':
          return config.plural ? 'Deployment Steps' : 'Deployment Step';
        case 'allocation-step':
          return config.plural ? 'Allocation Steps' : 'Allocation Step';
        case 'battle-step':
          return config.plural ? 'Battle Steps' : 'Battle Step';
        case 'turn':
          return config.plural ? 'Turns' : 'Turn';
        case 'arrangement-setup-turn':
          return config.plural ? 'Arrangement Turns' : 'Arrangement Turn';
        case 'deployment-setup-turn':
          return config.plural ? 'Deployment Turns' : 'Deployment Turn';
        case 'allocation-setup-turn':
          return config.plural ? 'Allocation Turns' : 'Allocation Turn';
        case 'battle-turn':
          return config.plural ? 'Battle Turns' : 'Battle Turn';
        case 'period':
          return config.plural ? 'Periods' : 'Period';
        case 'pre-combat-break-period':
          return config.plural ? 'Pre-combat Periods' : 'Pre-combat Period';
        case 'combat-period':
          return config.plural ? 'Combat Periods' : 'Combat Period';
        case 'post-combat-break-period':
          return config.plural ? 'Post-combat Periods' : 'Post-combat Period';
        case 'redeployment-period':
          return config.plural ? 'Redeployment Periods' : 'Redeployment Period';
        case 'block':
          return config.plural ? 'Blocks' : 'Block';
        case 'being-block':
          return config.plural ? 'Being Blocks' : 'Being Block';
        case 'item-block':
          return config.plural ? 'Item Blocks' : 'Item Block';
        case 'spell-block':
          return config.plural ? 'Spell Blocks' : 'Spell Block';
        case 'segment':
          return config.plural ? 'Segments' : 'Segment';
        case 'parity':
          return config.plural ? 'Parities' : 'Parity';
        case 'odd-parity':
          return config.plural ? 'Odd Parities' : 'Odd Parity';
        case 'even-parity':
          return config.plural ? 'Even Parities' : 'Even Parity';
        case 'move':
          return config.plural ? 'Moves' : 'Move';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para ações
    keywords.getAction = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^action-/, '' ) ) {
        case 'abort':
          return 'Abort';
        case 'absorb':
          return 'Absorb';
        case 'activate':
          return 'Activate';
        case 'attack':
          return 'Attack';
        case 'banish':
          return 'Banish';
        case 'channel':
          return 'Channel';
        case 'consume':
          return 'Consume';
        case 'defend':
          return 'Defend';
        case 'deploy':
          return 'Deploy';
        case 'disengage':
          return 'Disengage';
        case 'end':
          return 'End';
        case 'energize':
          return 'Energize';
        case 'engage':
          return 'Engage';
        case 'equip':
          return 'Equip';
        case 'ether-blast':
          return 'Ether Blast';
        case 'expel':
          return 'Expel';
        case 'guard':
          return 'Guard';
        case 'handle':
          return 'Handle';
        case 'intercept':
          return 'Intercept';
        case 'leech':
          return 'Leech';
        case 'mana-bolt':
          return 'Mana Bolt';
        case 'move':
          return 'Move';
        case 'possess':
          return 'Possess';
        case 'prepare':
          return 'Prepare';
        case 'relocate':
          return 'Relocate';
        case 'retreat':
          return 'Retreat';
        case 'summon':
          return 'Summon';
        case 'suspend':
          return 'Suspend';
        case 'sustain':
          return 'Sustain';
        case 'swap':
          return 'Swap';
        case 'tear':
          return 'Tear';
        case 'transfer':
          return 'Transfer';
        case 'unequip':
          return 'Unequip';
        case 'unsummon':
          return 'Unsummon';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para condições
    keywords.getCondition = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string?.replace( /^condition-/, '' ) ) {
        case 'forwarded':
          return 'Forwarded';
        case 'incapacitated':
          return 'Incapacitated';
        case 'stunned':
          return 'Stunned';
        case 'levitated':
          return 'Levitated';
        case 'guarded':
          return 'Guarded';
        case 'possessed':
          return 'Possessed';
        case 'projected':
          return 'Projected';
        case 'ignited':
          return 'Ignited';
        case 'strengthened':
          return 'Strengthened';
        case 'weakened':
          return 'Weakened';
        case 'enlivened':
          return 'Enlivened';
        case 'unlivened':
          return 'Unlivened';
        case 'emboldened':
          return 'Emboldened';
        case 'enraged':
          return 'Enraged';
        case 'diseased':
          return 'Diseased';
        case 'awed':
          return 'Awed';
        case 'feared':
          return 'Feared';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para telas
    keywords.getScreen = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'loading-screen':
        case 'LoadingScreen':
          return 'Loading Screen';
        case 'opening-screen':
        case 'OpeningScreen':
          return 'Opening Screen';
        case 'match-screen':
        case 'MatchScreen':
          return 'Match Screen';
        case 'deck-viewing-screen':
        case 'DeckViewingScreen':
          return 'Deck Viewing Screen';
        case 'deck-building-screen':
        case 'DeckBuildingScreen':
          return 'Deck Building Screen';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para componentes de molduras
    keywords.getFrameComponent = function ( string ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'cardData':
          return 'Data';
        case 'cardStats':
          return 'Stats';
        case 'cardCommand':
          return 'Command';
        case 'cardEffects':
          return 'Effects';
        case 'cardFlavor':
          return 'Flavor';
        case 'mainDeck':
          return 'Main Deck';
        case 'sideDeck':
          return 'Side Deck';
        case 'validation':
          return 'Validation';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Para valores não categorizados
    keywords.getMisc = function ( string, config = {} ) {
      // Retorna texto equivalente a 'string'
      switch( string ) {
        case 'primitiveType':
        case 'primitiveTypes':
          return config.plural ? 'Primitive Types' : 'Primitive Type';
        case 'trait':
        case 'traits':
          return config.plural ? 'Traits' : 'Trait';
        case 'attachment':
        case 'attachments':
          return config.plural ? 'Attachments' : 'Attachment';
        case 'weightCount':
          return 'Weight Count';
        case 'hands':
          return 'Hands';
        case 'inUse':
          return 'In Use';
        case 'inDisuse':
          return 'In Disuse';
        case 'usageSegment':
          return 'Usage Segment';
        case 'target':
          return config.plural ? 'Targets' : 'Target';
        case 'fatePoints':
          return config.plural ? 'Fate Points' : 'Fate Point';
        default:
          return m.languages.defaultFallback;
      }
    }
  }
} )();
