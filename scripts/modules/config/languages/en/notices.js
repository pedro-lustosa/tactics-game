// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'en' ) return;

  // Identificadores
  const notices = m.languages.notices = {};

  // Para textos diversos em modais
  notices.getModalText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'only-users-allowed':
        return 'You must be logged in to perform this operation.';
      case 'multiplayer-not-available':
        return 'Multiplayer mode is currently unavailable.';
      case 'select-defense-for-attack': {
        // Identificadores
        let attackName = m.languages.keywords.getManeuver( config.attack.name ),
            attackPoints = config.maneuverSource.currentPoints,
            sourceName = config.maneuverSource.source?.content.designation.title,
            attackerName = config.attacker.content.designation.full;

        // Retorna texto para a modal
        return `Select a defense for "${ attackName }" (${ attackPoints }P), by ${ attackerName + ( sourceName ? ' via ' + sourceName : '' ) }.`;
      };
      case 'confirm-free-attack-by-defender': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full,
            defenderName = config.defender.content.designation.full,
            attackName = m.languages.keywords.getManeuver( config.attack.name ),
            attackPoints = config.maneuverSource.currentPoints,
            sourceName = config.maneuverSource.source?.content.designation.title;

        // Retorna texto para a modal
        return `${ attackerName } will attack ${ defenderName } with "${ attackName }" (${ attackPoints }P)${ sourceName ? ', via ' + sourceName : '' }. Do you want to make a free attack against them before?`;
      };
      case 'confirm-free-attack-by-engage-action': {
        // Identificadores
        let engagerName = config.engager.content.designation.full,
            attackerName = config.attacker.content.designation.full,
            cardSlotName = config.slot.name;

        // Retorna texto para a modal
        return `${ engagerName } has just engaged in ${ cardSlotName }. Do you want to have ${ attackerName } make a free attack against them?`;
      };
      case 'confirm-free-attack-by-raging-surge-trait': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full,
            traitName = m.languages.cards.getTrait( 'raging-surge' ).title;

        // Retorna texto para a modal
        return `${ attackerName } had the "${ traitName }" effect activated. Do you want to make 1 free attack with them?`;
      };
      case 'confirm-free-attack-by-pike': {
        // Identificadores
        let attackerName = config.attacker.content.designation.full;

        // Retorna texto para a modal
        return `Do you want to make 1 free attack with ${ attackerName }, via the "thrust" maneuver of their pike in use?`;
      };
      case 'awed-free-attack-addendum': {
        // Identificadores
        let { target, requiredPoints } = config,
            targetName = target.content.designation.full,
            fatePointsSnippet = requiredPoints == 1 ? '1 fate point' : `${ requiredPoints } fate points`;

        // Retorna texto para a modal
        return ` (${ fatePointsSnippet } will be spent due to the "awed" condition on ${ targetName })`;
      };
      case 'confirm-fate-point-spent-for-awed-target': {
        // Identificadores
        let { target, requiredPoints } = config,
            targetName = target.content.designation.full,
            fatePointsSnippet = requiredPoints == 1 ? '1 fate point' : `${ requiredPoints } fate points`;

        // Retorna texto para a modal
        return `Due to the "awed" condition on ${ targetName }, ${ fatePointsSnippet } will be spent to make them the target of this action. Do you wish to proceed?`;
      };
      case 'confirm-fate-point-spent-by-feared': {
        // Identificadores
        let { slot, requiredPoints } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? 'an opponent\'s being' : 'the opponent\'s being',
            fatePointsSnippet = requiredPoints == 1 ? '1 fate point' : `${ requiredPoints } fate points`;

        // Retorna texto para a modal
        return `Due to the "feared" condition of ${ beingSnippet } in ${ slotName }, ${ fatePointsSnippet } will be spent for this action to be committed. Do you wish to proceed?`;
      };
      case 'select-maneuver-for-free-attack':
        return 'Select a maneuver for the free attack.';
      case 'select-embedded-item':
        return 'Select the target embedded item.';
      case 'select-item-attacher':
        return 'Select the attacher for the target item.';
      case 'select-spell-path-for-spell-effect':
        return 'Select the path to be affected by the spell.';
      case 'assign-mana-cost': {
        // Identificadores
        let { polarityBurden, isEnergy } = config,
            modalText = `Assign the ${ isEnergy ? 'energy' : 'mana' } for the spell\'s channeling`;

        // Adiciona adendo sobre ônus de polaridade, se aplicável
        if( polarityBurden ) modalText += ', disregarding the polarity burden';

        // Ajusta final do texto
        modalText += '.';

        // Retorna texto
        return modalText;
      };
      case 'assign-mana-cost-placeholder':
        return config.isPersistent ?
          `Multiples of ${ config.minMana } only, up to ${ config.maxMana }` : `Digits only, between ${ config.minMana } and ${ config.maxMana }`;
      case 'confirm-mana-cost-after-polarity-burden':
        return `${ config.manaPoints } ${ config.isEnergy ? 'energy' : 'mana' } points will be spent including the polarity burden. Do you wish to proceed?`;
      case 'confirm-mana-cost-with-adjunct-spells': {
        // Identificadores
        let { manaPoints, adjunctSpells, isEnergy } = config,
            resource = isEnergy ? 'energy' : 'Metoth mana',
            modalText = '';

        // Adiciona ao texto da modal pontos de mana que serão gastos
        modalText += manaPoints == 1 ? `${ manaPoints } ${ resource } point will be spent ` : `${ manaPoints } ${ resource } points will be spent `;

        // Adiciona ao texto da modal origem do uso dos pontos de mana
        modalText += adjunctSpells.length == 1 ?
          `by the "${ adjunctSpells[ 0 ].content.designation.title }" spell.` : `in total by the spells modifying the main spell.`;

        // Adiciona ao texto solicitação de confirmação
        modalText += ' Do you wish to proceed?';

        // Retorna texto
        return modalText;
      }
      case 'assign-mana-to-absorb':
        return 'Assign the mana to be absorbed.';
      case 'assign-mana-to-transfer':
        return 'Assign the mana to be transferred.';
      case 'assign-mana-placeholder':
        return `Up to ${ config.manaPoints } ${ config.manaPoints == 1 ? 'point' : 'points' }`;
      case 'set-number-of-attacks':
        return `Set how many attacks will be generated.`;
      case 'number-of-attacks-placeholder':
        return `Digits only, between 1 and ${ config.maxNumber }.`;
      case 'confirm-spell-mana-cost':
        return `When used, this spell will spend ${ config.manaPoints } ${ config.isEnergy ? 'energy' : 'mana' } point${ config.manaPoints == 1 ? '' : 's' }. Do you wish to proceed?`;
      case 'confirm-spell-mana-cost-and-inflicted-damage':
        return `When used, this spell will spend ${ config.manaPoints } ${ config.isEnergy ? 'energy' : 'mana' } point${ config.manaPoints == 1 ? '' : 's' } and will transfer up to ${ config.damagePoints } damage point${ config.damagePoints == 1 ? '' : 's' }. Do you wish to proceed?`;
      case 'change-target-of-fireball-spell':
        return 'Do you want this spell\'s attack to target only 1 physical being?';
      case 'notify-round-winner-self':
        return 'Congratulations. This round has ended, and you are the winner.';
      case 'notify-round-winner-opponent':
        return 'This round has ended, and your opponent is the winner.';
      case 'notify-round-winner-other':
        return `This round was finished. Its winner is the ${ m.languages.keywords.getParity( config.winner.parity ).toLowerCase() } player.`;
      case 'notify-round-draw':
        return 'This round was finished, having ended in a draw.';
      case 'leave-match':
        return 'Do you really want to leave from the current match?';
      case 'concede-match':
        return 'If you concede, this match will be finished, and its result will be computed as a loss for you and a win for your opponent. Do you really want to do this action?';
      case 'win-by-concession-player':
        return 'Your opponent has just conceded this match. Because of this, you are the winner.';
      case 'win-by-concession-other':
        return `${ m.languages.keywords.getParity( config.loser.parity ) } has just conceded this match. Because of this, ${ m.languages.keywords.getParity( config.winner.parity ).toLowerCase() } is the winner.`;
      case 'name-deck-set':
        return `Name this ${ config.isDeckSet ? 'deck set' : 'deck' }.`;
      case 'name-deck-set-placeholder':
        return 'Latin letters and digits only';
      case 'alert-deck-set-editing':
        return 'When saving the edition of a deck, its statistics about matches are reset to zero. If you do not want that, you must cancel this edition and edit a copy of the deck.';
      case 'confirm-deck-set-saving': {
        // Identificadores
        let { decksSet, isNewDeck } = config,
            confirmationText = 'You still have ';

        // Itera por conjunto de baralhos
        for( let deck of decksSet ) {
          // Identificadores
          let [ cardsNumber, coinsNumber ] = [ deck.constructor.maxCards - deck.cards.length, deck.coins.max - deck.coins.spent ];

          // Filtra baralhos que não atendem aos critérios de confirmação
          if( deck.cards.length >= deck.constructor.maxCards || coinsNumber < 50 ) continue;

          // Ajusta texto de confirmação
          confirmationText +=
            `${ coinsNumber } coins in the ${ m.languages.keywords.getDeck( deck.type ).toLowerCase() } to spend with ` +
            `${ ( cardsNumber == 1 ? '' : 'up to ' ) + cardsNumber + ( cardsNumber == 1 ? ' card' : ' cards' ) } and `;
        }

        // Ajusta final do texto de confirmação
        confirmationText = confirmationText.replace( / and $/, `. Do you really want to finish?` );

        // Retorna texto de confirmação
        return confirmationText;
      };
      case 'deck-set-saved': {
        // Identificadores
        let modalText = config.isDeckSet ? 'Deck set ' : 'Deck ';

        // Acrescenta corpo do texto a ser retornado
        modalText += config.isNewDeck ?
          'saved. From now on you can find it on the matchmaking screen and on the deck viewing screen.' : 'edited.';

        // Retorna texto
        return modalText;
      };
      case 'not-enough-valid-decks-for-simulate':
        return 'You must have at least 2 valid decks in order to access the simulation mode.';
      case 'not-enough-valid-decks-for-multiplayer':
        return 'You must have at least 1 valid deck in order to access the multiplayer mode.';
      case 'no-access-while-searching-for-match':
        return 'You cannot access any other screen while you are searching for a match. To stop searching for a match, reload this screen.';
      case 'deck-set-limit-reached':
        return 'You have reached the limit of decks you can have. To build a new deck, you must first remove a current deck of yours.';
      case 'confirm-deck-set-removal':
        return 'Do you really want to remove this deck from your built decks?';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para textos sobre invalidações diversas
  notices.getInvalidationText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'invalid-user':
        return 'Invalid user.';
      case 'only-1-match-at-a-time':
        return 'Each user can only [search/be in] 1 match at a time.';
      case 'flow-must-be-redeployment':
        return 'The flow must be in the redeployment period.';
      case 'allocation-step-invalid-flow-cost':
        return 'The sum of the action\'s flow cost and the flow cost already spent in this step by the committer must not exceed 5.';
      case 'combat-period-invalid-flow-cost':
        return 'The action\'s flow cost must be less than the remaining segments of the current period.';
      case 'invalid-target':
        return 'The chosen target is invalid.';
      case 'target-must-be-player-component':
        return 'The target must be a component of yours.';
      case 'target-must-not-be-opponent-component':
        return 'The target must not be an opponent\'s component.';
      case 'target-must-not-be-committer':
        return 'The target must not be the committer.';
      case 'target-in-player-grid':
        return 'The target must be in your field grid.';
      case 'target-must-be-card-slot':
        return 'The target must be a card slot.';
      case 'target-must-not-be-committer-card-slot':
        return 'The target must not be the committer\'s card slot.';
      case 'target-must-be-field-grid-slot':
        return 'The target must be a card slot in a field grid.';
      case 'target-must-be-unoccupied-card-slot':
        return 'The target must be an unoccupied card slot.';
      case 'target-must-be-from-committer-field-grid':
        return 'The target must be a card slot from the committer\'s field grid.';
      case 'target-must-be-engagement-zone':
        return 'The target must be an engagement zone.';
      case 'target-must-be-adjacent-engagement-zone':
        return 'The target must be an adjacent engagement zone.';
      case 'target-must-be-empty-engagement-zone':
        return 'The target must be an empty engagement zone.';
      case 'target-must-be-occupied-engagement-zone':
        return 'The target must be an occupied engagement zone.';
      case 'engagement-zone-target-must-be-occupied':
        return 'As an engagement zone, the target must have 1 or more occupants.';
      case 'engagement-zone-target-must-not-be-with-astral-being':
        return 'If the target is an engagement zone, the committer must not be an astral being.';
      case 'target-must-not-have-max-player-occupants':
        return 'The target must not have reached the limit of your occupants.';
      case 'target-must-be-card':
        return 'The target must be a card.';
      case 'target-must-be-from-player':
        return 'The target must be a card of yours.';
      case 'target-must-be-from-opponent':
        return 'The target must be an opponent\'s card.';
      case 'target-must-be-from-side-deck':
        return 'The target must be a card of your side deck.';
      case 'target-must-be-from-committer-deck':
        return 'The target must be a card from the committer\'s deck.';
      case 'target-must-be-active':
        return 'The target must be active.';
      case 'target-must-be-suspended':
        return 'The target must be suspended.';
      case 'target-must-be-active-or-suspended':
        return 'The target must be active or suspended.';
      case 'target-must-be-in-use':
        return 'The target must be in use.';
      case 'target-must-be-in-disuse':
        return 'The target must not be in use.';
      case 'target-must-be-exhausted':
        return 'The target must be exhausted.';
      case 'target-must-not-be-exhausted':
        return 'The target must not be exhausted.';
      case 'target-must-have-moves':
        return 'The target must have moves.';
      case 'target-must-have-finished-all-moves':
        return 'The target must have finished all their moves.';
      case 'target-effect-must-be-enabled':
        return 'The target\'s effect must be enabled.';
      case 'target-usage-must-be-dynamic':
        return 'The target\'s usage must be dynamic.';
      case 'target-must-not-be-commander':
        return 'The target must not be a commander.';
      case 'target-must-be-being':
        return 'The target must be a being.';
      case 'target-must-be-biotic-being':
        return 'The target must be a biotic being.';
      case 'target-must-be-humanoid':
        return 'The target must be a humanoid.';
      case 'target-must-be-spirit-or-demon':
        return 'The target must be a spirit or a demon.';
      case 'target-must-be-demon-or-spell':
        return 'The target must be a demon or a spell.';
      case 'target-energy-must-be-lower-than-base':
        return 'The target\'s current energy must be below their base energy.';
      case 'target-must-have-less-will-than-committer':
        return 'The committer\'s will must be greater than that of the target.';
      case 'target-must-have-less-stamina-than-committer-energy':
        return 'The committer\'s energy must be greater than twice target\'s stamina.';
      case 'target-must-have-less-experience-than-committer-power':
        return 'The committer\'s power must be greater than target\'s experience level.';
      case 'being-is-already-guarded':
        return 'This being is already being guarded.';
      case 'target-must-not-be-guarded':
        return 'The target must not be being guarded.';
      case 'target-must-be-owned-by-committer':
        return 'The target must be owned by the committer.';
      case 'target-attacher-must-be-in-use':
        return 'The target\'s attacher must be in use.';
      case 'target-must-be-item-or-committer':
        return 'The target must be an item or the committer.';
      case 'target-must-be-item':
        return 'The target must be an item.';
      case 'item-invalid-target-type':
        return `This item can only target ${ config.targetType }.`;
      case 'no-valid-embedded-item':
        return 'The committer has no valid embedded items.';
      case 'target-must-be-external-item':
        return 'The target must be an external item.';
      case 'item-size-must-match':
        return 'The item\'s size must be either null or equal to the attacher\'s.';
      case 'item-over-weight-count':
        return 'The sum of the item\'s weight score and the attacher\'s weight count must not exceed 5.';
      case 'garment-weight-must-be-distinct':
        return 'The garment must have a weight distinct from that of the garment already attached to the attacher.';
      case 'implement-with-same-title-already-attached':
        return 'An implement with the same title must not already be attached to the attacher.';
      case 'consumable-already-used-by-being':
        return `A consumable with the same title has already been used by this being in the current ${ m.GameMatch.current?.flow.period ? 'period' : 'step' }.`;
      case 'item-already-attached':
        return 'The item must not already be attached to the attacher.';
      case 'item-over-max-attachments':
        return 'The attacher has already reached their attachment limit for the item\'s equipage.';
      case 'no-valid-attachers':
        return 'There are no valid attachers for the target item.';
      case 'target-must-not-be-forwardable':
        return 'The target being must not be forwardable.';
      case 'attacher-invalid-attack':
        return `The attacher must have the attack "${ config.attack }".`;
      case 'shoot-invalid-damage':
        return 'The main damage of the attacher\'s "shoot" attack must be pierce.';
      case 'attacher-must-have-slash-or-pierce-damage':
        return 'The attacher must have at least one attack with slash or pierce damage.';
      case 'target-must-be-spell':
        return 'The target must be a spell.';
      case 'if-spell-target-must-be-negative':
        return 'As a spell, the target must have negative polarity.';
      case 'spell-invalid-target-type':
        return `This spell can only target ${ config.targetType }.`;
      case 'target-must-be-spell-suspended-or-owned':
        return 'The target must be a spell suspended or owned by the committer.';
      case 'allocation-step-spell-targets':
        return 'The target must be a persistent or adjunct spell.';
      case 'spell-already-in-use':
        return 'The spell must not already be in use.';
      case 'target-must-be-in-channeler-paths':
        return 'The target must be from a path in which the committer has at least 1 level.';
      case 'target-mana-cost-must-be-paid': {
        // Identificadores
        let { isEnergy, isWithAdjunct, polarityBurden } = config,
            resource = isEnergy ? 'energy' : 'mana',
            noticeText = `The committer must have enough ${ resource } to pay at least the minimum cost of `;

        // Adiciona identificação da magia alvo
        noticeText += isWithAdjunct ? 'the main spell' : 'the target';

        // Adiciona adendo sobre ônus de polaridade, se aplicável
        if( polarityBurden ) noticeText += ', including the polarity burden';

        // Ajusta final do texto
        noticeText += '.';

        // Retorna texto
        return noticeText;
      };
      case 'adjunct-spells-mana-cost-must-be-paid': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return isEnergy ?
          'The minimum energy cost required by adjunct spells must not be greater than the committer\'s base Metoth mana.' :
          'The committer must have enough Metoth mana to pay at least the minimum cost required by adjunct spells.';
      }
      case 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-metoth-mana': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return isEnergy ?
          'The sum of the energy cost required by the spells in the chain must not be greater than the committer\'s Metoth base mana.' :
          'The committer must have enough Metoth mana to pay the spell chain\'s main spell cost plus the cost required by adjunct spells.';
      }
      case 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-total-mana': {
        // Identificadores
        let { isEnergy } = config;

        // Retorna texto
        return `The committer must have enough ${ isEnergy ? 'energy' : 'total mana' } to pay the spell chain\'s main spell cost plus the cost required by adjunct spells.`;
      }
      case 'target-invalid-by-antimagic-essence':
        return `When the spell's target is a dwarf, the spell's predicted potency must be able to be at least ${ config.minPotency }.`;
      case 'spell-over-max-channeling-flow-cost':
        return 'The spell\'s current channeling flow cost must not exceed 5.';
      case 'spell-invalid-range':
        return `The target must be within the spell's range.`;
      case 'spell-target-must-be-an-attacher':
        return 'The target must be able to have attached spells.';
      case 'spell-over-max-attachments':
        return 'The attacher has already reached their limit of attached spells.';
      case 'must-be-powthe-committer':
        return 'The committer must have at least 1 level in Powth.';
      case 'must-be-powthe-summoner':
        return 'The summoner must have at least 1 level in Powth.';
      case 'spell-wrong-voth-polarity-committer':
        return `The committer must have ${ config.polarity } polarity in Voth.`;
      case 'spell-wrong-voth-polarity-summoner':
        return `The summoner must have ${ config.polarity } polarity in Voth.`;
      case 'voth-greater-min-level-summoner':
        return `The summoner's Voth level must not be less than ${ config.level }.`;
      case 'demons-above-summoner-limit':
        return 'The summoner must not exceed the limit of their currently active demons.';
      case 'angels-above-summoner-limit':
        return 'The summoner already has an active angel.';
      case 'salamanders-above-summoner-limit':
        return 'The summoner must not exceed the limit of their currently active salamanders.';
      case 'leech-must-target-unleeched-target':
        return 'The target must not be being targeted by another "leech" action.';
      case 'possess-must-target-unpossessed-target':
        return 'The target must not be being targeted by another "possess" action.';
      case 'must-not-be-with-possess-condition':
        return 'The target must not have the "possessed" condition.';
      case 'attack-invalid-target-type':
        return `This attack can only target ${ config.targetType }.`;
      case 'attack-invalid-range':
        return `The target must be within the attack's range.`;
      case 'being-cannot-be-direct-target-of-actions':
        return `This being cannot be a direct target of actions.`;
      case 'being-cannot-be-direct-target-of-attacks':
        return `This being cannot be a direct target of attacks.`;
      case 'being-cannot-be-direct-target-of-spells':
        return `This being cannot be a direct target of spells.`;
      case 'attack-rapture-too-high-health':
        return `This being needs to have at most ${ config.maxHealth } health point${ config.maxHealth == 1 ? '' : 's' } to be targetable by this maneuver.`;
      case 'attack-prevented-by-removed-item':
        return `The attack of ${ config.attacker.content.designation.full } was prevented due to the means of attack having been removed.`;
      case 'attack-prevented-by-lack-of-points':
        return `The attack of ${ config.attacker.content.designation.full } was prevented due to the lack of points for its commitment.`;
      case 'target-occupant-able-to-commit-relocate':
        return 'The target\'s current occupant must be able to commit "relocate".';
      case 'target-occupant-able-to-do-distinct-move':
        return 'The target\'s current occupant must be able to develop a move distinct from the committer\'s.';
      case 'target-must-be-commiting-channel-or-sustain':
        return 'The target must be occupied with "channel" or "sustain".';
      case 'target-must-be-persistent-or-permanent-spell':
        return 'The target must be a [persistent/permanent] spell.';
      case 'target-must-be-channeling-spell':
        return 'The target must be channeling a spell.';
      case 'target-must-be-channeling-spell-with-assigned-mana':
        return 'The spell being channeled by the target must have mana assigned to it.';
      case 'spell-must-be-able-to-have-no-less-potency-than-target':
        return 'The committer does not have enough mana for this spell\'s potency to be no less than the target\'s.';
      case 'spell-must-be-able-to-have-no-less-potency-than-half-target':
        return 'The committer does not have enough mana for this spell\'s potency to be no less than half of the target\'s.';
      case 'spell-must-be-able-to-have-no-less-potency-than-target-spell':
        return 'The committer does not have enough mana for this spell\'s potency to be no less than the predicted potency of target\'s spell.';
      case 'committer-must-have-paths-other-than-metoth':
        return 'The committer must have levels in other paths besides Metoth.';
      case 'committer-must-have-paths-with-restorable-mana':
        return 'The committer needs to have non-Metoth paths whose current mana is below the base value.';
      case 'owner-current-metoth-mana-must-be-less-than-base':
        return 'The committer\'s current Metoth mana must be less than their base mana.';
      case 'target-must-be-tulpa-or-spirit':
        return 'The target must be a tulpa or spirit.';
      case 'target-energy-must-not-be-greater-than-committer-metoth-mana':
        return 'The target\'s energy must not be greater than the committer\'s base Metoth mana.';
      case 'adjunct-spell-must-be-distinct':
        return 'The chosen spell cannot be one already chosen in this spell chain.';
      case 'committer-must-not-be-with-robe-of-the-magi':
        return 'The committer must not have active the effect of "Robe of the Magi".';
      case 'target-must-have-channeling-mana-cost':
        return 'The target must have channeling mana cost.';
      case 'target-must-have-channeling-flow-cost':
        return 'The target must have channeling flow cost.';
      case 'target-must-have-r1-to-r4-range':
        return 'The target\'s range must be between R1 and R4.';
      case 'target-must-not-have-max-strengthened-markers':
        return 'The target must not have the maximum number of strengthened markers.';
      case 'target-must-not-have-max-enlivened-markers':
        return 'The target must not have the maximum number of enlivened markers.';
      case 'target-must-not-have-max-weakened-markers':
        return 'The target must not have the maximum number of weakened markers.';
      case 'target-must-not-have-max-unlivened-markers':
        return 'The target must not have the maximum number of unlivened markers.';
      case 'target-must-not-have-max-emboldened-markers':
        return 'The target must not have the maximum number of emboldened markers.';
      case 'target-must-not-have-max-enraged-markers':
        return 'The target must not have the maximum number of enraged markers.';
      case 'target-must-not-have-max-awed-markers':
        return 'The target must not have the maximum number of awed markers.';
      case 'target-must-not-have-max-feared-markers':
        return 'The target must not have the maximum number of feared markers.';
      case 'target-must-have-less-than-5-stamina':
        return 'The target\'s stamina must be less than 5.';
      case 'damage-giver-must-have-suffered-physical-damage':
        return 'The target must have suffered physical damage in the current combat.';
      case 'damage-giver-has-not-enough-damage-to-assign':
        return 'The physical damage currently suffered by the target must be [equal to/greater than] the physical damage to be transferred.';
      case 'damage-receiver-must-be-different-from-damage-giver':
        return 'The target receiving the damage must be different from the one transferring the damage.';
      case 'target-must-have-size':
        return 'The target must have size.';
      case 'target-size-must-be-smaller-than-large':
        return 'The target\'s size must be smaller than large.';
      case 'target-size-must-be-larger-than-small':
        return 'The target\'s size must be larger than small.';
      case 'target-must-have-less-attachments-with-size-for-spell':
        return 'The target must have less embedded attachments with size, so that the committer can pay the minimum mana required by the spell.';
      case 'target-must-have-conductivity':
        return 'The target must have conductivity.';
      case 'target-must-be-with-being-with-conductivity':
        return 'The target must be in a card slot with at least 1 other being that has conductivity.';
      case 'target-must-have-a-being-with-conductivity':
        return 'The target must be occupied by at least 1 being with conductivity.';
      case 'target-must-have-a-physical-being':
        return 'The target must be occupied by at least 1 physical being.';
      case 'target-must-not-be-with-air-cocoon':
        return 'The target must not be attached to the "Air Cocoon" spell.';
      case 'target-must-not-be-with-pavise':
        return 'The target must not be with a pavise in use.';
      case 'player-cannot-use-celestial-appeal':
        return 'This spell cannot be used by you, as you have already used a negative spell.';
      case 'committer-must-not-be-astral-being':
        return 'The committer must not be an astral being.';
      case 'committer-must-not-be-in-engagement-zone':
        return 'The committer must not be in an engagement zone.';
      case 'spirit-spell-cannot-have-extend':
        return 'As a spirit, the committer cannot channel this spell together with "Extend".';
      case 'awed-prevents-attack-target':
        return 'The target must not have more "awed" markers than the committer\'s will.';
      case 'feared-prevents-slot-choice': {
        // Identificadores
        let { slot } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? `No opponent's being in ${ slotName } must` : `The opponent's being in ${ slotName } must not`;

        // Retorna texto
        return `${ beingSnippet } have more "feared" markers than the committer\'s will.`;
      }
      case 'no-fate-points-to-attack-awed-target':
        return 'Due to the "awed" markers on the target, to target them you must spend a number of fate points that you do not have.';
      case 'no-fate-points-to-move-to-target-with-feared': {
        // Identificadores
        let { slot } = config,
            slotName = slot.name.replace( /^.+-/g, '' ),
            beingSnippet = slot instanceof m.EngagementZone ? 'an opponent\'s being' : 'the opponent\'s being';

        // Retorna texto
        return `Due to the "feared" markers of ${ beingSnippet } in ${ slotName }, to commit this action you must spend a number of fate points that you do not have.`;
      }
      case 'not-enough-mana-for-given-target':
        return `The committer does not have enough mana to choose this target, which requires ${ config.manaPoints } points.`;
      case 'spell-already-used-in-battle':
        return 'This spell no longer can be used in the current battle.';
      case 'spell-already-used-by-you-in-battle':
        return 'This spell no longer can be used by you in the current battle.';
      case 'spell-only-usable-in-middle-battle':
        return 'This spell can only be used on battle turns 3 or 4.';
      case 'swap-over-max-coins': {
        // Identificadores
        let { invalidDeckTypes } = config,
            noticeText = 'This swap is above the coin limit ';

        // Itera por baralhos cuja troca excederia o limite de moedas
        for( let deckType in invalidDeckTypes ) {
          // Identificadores
          let coinsDifference = invalidDeckTypes[ deckType ];

          // Acrescenta texto sobre invalidação do baralho alvo
          noticeText +=
            `of the ${ m.languages.keywords.getDeck( deckType ).toLowerCase() } by ${ coinsDifference } coin${ coinsDifference == 1 ? '' : 's' } and `;
        }

        // Remove conjunção final
        noticeText = noticeText.replace( / and $/, '.' );

        // Retorna texto
        return noticeText;
      };
      case 'swap-invalid-command':
        return 'This swap would invalidate the main deck\'s command.';
      case 'swap-invalid':
        return 'This swap would invalidate the deck set.';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre escolhas
  notices.getChoiceText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'choose-target-card':
        return 'Choose the target card, with a double-click.';
      case 'choose-target-card-slot':
        return 'Choose the target card slot, with a double-click while holding down "Ctrl".';
      case 'choose-target-being':
        return 'Choose the target being, with a double-click.';
      case 'choose-target-item':
        return 'Choose the target item, with a double-click.';
      case 'choose-target-spell':
        return 'Choose the target spell, with a double-click.';
      case 'choose-target-for-spell':
        return 'Choose the spell\'s target, with a double-click, and for card slots while holding down "Ctrl".';
      case 'choose-target-for-spell-chain':
        return 'Choose the next spell of the spell chain.';
      case 'choose-target-item-or-committer':
        return 'Choose the target item or – for embedded items – the committer, with a double-click.';
      case 'choose-new-owner':
        return 'Choose the new owner of the target item, with a double-click.';
      case 'choose-card-slot-for-token':
        return 'Choose the token\'s card slot, with a double-click while holding down "Ctrl".';
      case 'choose-target-for-golem':
        return 'Choose the troop to be ended for the summoning, with a double-click.';
      case 'choose-target-for-give-damage':
        return 'Choose the target to transfer damage points from.';
      case 'choose-target-for-receive-damage':
        return 'Choose the target to transfer damage points to.';
      case 'choose-attack-target':
        return 'Choose the target of the attack, with a double click.';
      case 'choose-attack-target-1':
        return 'Choose the target of the first attack, with a double click.';
      case 'choose-attack-target-2':
        return 'Choose the target of the second attack, with a double click.';
      case 'choose-attack-target-3':
        return 'Choose the target of the third attack, with a double click.';
      case 'choose-attack-target-4':
        return 'Choose the target of the fourth attack, with a double click.';
      case 'choose-attack-points-player':
        return 'Set the combat break\'s total attack points.';
      case 'choose-attack-points-other':
        return `${ m.languages.keywords.getParity( config.player.parity ) } is setting the combat break\'s total attack points.`;
      case 'choose-defense-points-player':
        return 'Set the combat break\'s total defense points.';
      case 'choose-defense-points-other':
        return `${ m.languages.keywords.getParity( config.player.parity ) } is setting the combat break\'s total defense points.`;
      case 'choose-assign-fate-point':
        return 'Choose whether you want to assign 1 fate point to the combat break.';
      case 'opponent-choosing-assign-fate-point':
        return 'The opponent is choosing whether they want to assign 1 fate point to the combat break.';
      case 'players-choosing-assign-fate-point':
        return 'The players are choosing whether they want to assign 1 fate point to the combat break.';
      case 'choosing-attack-target': {
        // Identificadores
        let { playerParity, sourceCard } = config,
            player = m.languages.keywords.getParity( playerParity ),
            cardName = sourceCard instanceof m.Being ? sourceCard.content.designation.full : `"${ sourceCard.content.designation.title }"`;

        // Retorna texto para a modal
        return `${ player } is choosing a target for the attack of ${ cardName }.`;
      }
      case 'choosing-attack-defense': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            committer = config.action.committer.content.designation.full;

        // Retorna texto para a modal
        return `${ player } is choosing a defense for the attack of ${ committer }.`;
      }
      case 'canceling-action': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            actionName = m.languages.keywords.getAction( config.action.name ).toLowerCase(),
            committer = config.action.committer.content.designation.full;

        // Retorno do texto
        return `${ player } canceled the "${ actionName }" action of ${ committer }.`;
      }
      case 'choosing-free-attack': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            [ committer, defender ] = [ config.committer.content.designation.full, config.defender?.content.designation.full ];

        // Retorna texto para a modal
        return `${ player } is considering making a free attack with ${ committer }${ defender ? ' against ' + defender : '' }.`;
      }
      case 'free-attack-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            decision = config.isToAttack ? 'to attack' : 'not to attack';

        // Retorno do texto
        return `${ player } chose ${ decision }.`;
      }
      case 'choosing-free-attack-maneuver': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            [ committer ] = [ config.committer.content.designation.full ];

        // Retorna texto para a modal
        return `${ player } is choosing a maneuver for the free attack of ${ committer }.`;
      }
      case 'choosing-fate-break-target': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity );

        // Retorna texto para a modal
        return `${ player } is choosing a target for the effect of the fate break.`;
      }
      case 'chose-fate-break-target': {
        // Identificadores
        let targetName = config.target instanceof m.Being ? config.target.content.designation.full : config.target.content.designation.title;

        // Retorna texto para a modal
        return `${ targetName } was chosen as the target for the effect of the fate break.`;
      }
      case 'canceling-fate-break-target': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity );

        // Retorno do texto
        return `${ player } canceled the effect of the fate break.`;
      }
      case 'fate-break-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            decision = config.isToUse ? 'to use' : 'not to use';

        // Retorno do texto
        return `${ player } chose ${ decision } fate points.`;
      }
      case 'fate-break-contestation-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            decision = config.isToUse ? 'to contest' : 'not to contest';

        // Retorno do texto
        return `${ player } chose ${ decision } the fate break.`;
      }
      case 'choosing-deny-fate-break-contestation': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity );

        // Retorno do texto
        return `${ player } is considering denying the fate break's contestation, via "The Feast".`;
      }
      case 'deny-fate-break-contestation-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            decision = config.isToDeny ? 'to negate' : 'to not negate';

        // Retorno do texto
        return `${ player } chose ${ decision } the fate break's contestation.`;
      }
      case 'choosing-contest-fate-break-denying': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity );

        // Retorno do texto
        return `${ player } is considering to contest the denying of their contestation, via "The Feast".`;
      }
      case 'contest-fate-break-denying-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ),
            decision = config.isToContest ? 'to contest' : 'to not contest';

        // Retorno do texto
        return `${ player } chose ${ decision } the denying of their contestation.`;
      }
      case 'awaiting-player-choice': {
        // Identificadores
        let player = m.languages.keywords.getParity( config.playerParity ).toLowerCase();

        // Retorno do texto
        return `Awaiting ${ player }'s decision.`;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre registros de ações
  notices.getActionLog = function ( textName, config = {} ) {
    // Identificadores
    var { action, isFull = false, eventType = '' } = config;

    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'action-abort': {
        // Identificadores
        let { committer, actionToAbort } = action,
            committerName = committer.content.designation.full,
            actionToAbortName = m.languages.keywords.getAction( actionToAbort.name ).toLowerCase();

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } aborted "${ actionToAbortName }".`;
      }
      case 'action-absorb': {
        // Identificadores
        let { committer, manaToAbsorb, manaReceiver } = action,
            committerName = committer.content.designation.full,
            manaToAbsorbSnippet = `${ manaToAbsorb } mana point${ manaToAbsorb == 1 ? '' : 's' }`,
            receiverSnippet = committer == manaReceiver ? '' : ` via ${ manaReceiver.content.designation.title }`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Caso alvo a receber o mana não seja acionante, indica em que casa está
          if( committer != manaReceiver ) receiverSnippet += m.languages.snippets.getCardPosition( manaReceiver );
        }

        // Retorno do texto
        return `${ committerName } absorbed ${ manaToAbsorbSnippet }${ receiverSnippet }.`;
      }
      case 'action-attack': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            freeAttackSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          // Caso ataque seja livre, aponta isso e o que o causou
          if( action.commitment.type == 'free'  ) freeAttackSnippet +=
            `, via a ${ m.languages.snippets.getFreeAttack( m.ActionAttack.freeAttackExecution?.name ?? '' ).toLowerCase() }`.replace( /\.$/, '' );
        }

        // Retorno do texto
        return `${ committerName } will attack ${ targetName }${ freeAttackSnippet }.`;
      }
      case 'action-banish': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target instanceof m.Being ? target.content.designation.full : target.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } banished ${ targetName }.`;
      }
      case 'action-channel': {
        // Identificadores
        let matchFlow = m.GameMatch.current.flow,
            { committer, target: spell, manaToAssign, adjunctManaToAssign, adjunctSpells } = action,
            { target: spellTarget } = spell,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.full,
            resource = committer instanceof m.AstralBeing ? 'energy' : 'mana',
            tenseSnippet = matchFlow.period instanceof m.CombatPeriod && action.flowCost && !action.isCommitted ? 'will channel' : 'channeled',
            spellTargetSnippet = m.languages.snippets.getSpellTargetSnippet( spellTarget, committer ),
            spellExtraTargetsSnippet = '',
            endSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Complementa nome da magia com indicação de em que casa está
          spellName += m.languages.snippets.getCardPosition( spell );

          // Para caso alvo da magia seja uma carta que não o acionante
          if( spellTarget instanceof m.Card && spellTarget != committer ) {
            // Para entes e equipamentos embutidos, complementa nome do alvo da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
            if( spellTarget instanceof m.Being || spellTarget.isEmbedded ) spellTargetSnippet += spellTarget.content.designation.getIndex( true );

            // Complementa nome do alvo da magia com indicação de em que casa está
            spellTargetSnippet += m.languages.snippets.getCardPosition( spellTarget );
          }

          // Itera por eventuais alvos adicionais da magia
          for( let i = 0; i < spell.extraTargets.length; i++ ) {
            // Identificadores
            let extraTarget = spell.extraTargets[ i ],
                extraTargetName = m.languages.snippets.getSpellTargetSnippet( extraTarget, committer );

            // Filtra alvos que sejam iguais ao alvo principal
            if( extraTarget == spellTarget ) continue;

            // Filtra alvos que já tenham sido relevados
            if( spell.extraTargets.indexOf( extraTarget ) != i ) continue;

            // Para caso alvo adicional da magia seja uma carta que não o acionante
            if( extraTarget instanceof m.Card && extraTarget != committer ) {
              // Para entes e equipamentos embutidos, complementa nome do alvo adicional da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
              if( extraTarget instanceof m.Being || extraTarget.isEmbedded ) extraTargetName += extraTarget.content.designation.getIndex( true );

              // Complementa nome do alvo adicional da magia com indicação de em que casa está
              extraTargetName += m.languages.snippets.getCardPosition( extraTarget );
            }

            // Adiciona ao trecho de alvos adicionais informação sobre o alvo adicional da magia
            spellExtraTargetsSnippet += extraTargetName + ', ';
          }

          // Ajusta separador final do eventual trecho sobre alvos adicionais
          spellExtraTargetsSnippet = spellExtraTargetsSnippet.replace( /, $/, ' and ' );
        }

        // Formula trecho final
        setEndSnippet: {
          // Apenas executa bloco caso registro seja o completo
          if( !isFull ) break setEndSnippet;

          // Identificadores
          let manaToAssignSnippet = !manaToAssign ?
                '' : `via ${ manaToAssign } assigned ${ resource } point${ manaToAssign == 1 ? '' : 's' }`,
              adjunctSpellsSnippet = !adjunctSpells.length ?
                '' : `also spending ${ adjunctManaToAssign } ${ resource } point${ adjunctManaToAssign == 1 ? '' : 's' }`;

          // Apenas executa bloco caso haja ao menos 1 informação a constituir trecho final
          if( [ manaToAssignSnippet, adjunctSpellsSnippet ].every( snippet => !snippet ) ) break setEndSnippet;

          // Adiciona a trecho final separador
          endSnippet += ', ';

          // Para caso haja mana atribuído
          if( manaToAssignSnippet ) {
            // Identificadores
            let manaWithPolarityBurden = spell.applyPolarityBurden( manaToAssign );

            // Adiciona a trecho final mana atribuído
            endSnippet += manaToAssignSnippet;

            // Quando houver ônus de polaridade, adiciona essa informação
            if( manaToAssign != manaWithPolarityBurden ) endSnippet += ` (${ manaWithPolarityBurden } spent points, with the polarity burden)`;
          }

          // Encerra bloco caso não exista trecho sobre magias adjuntas
          if( !adjunctSpellsSnippet ) break setEndSnippet;

          // Caso exista conteúdo no trecho sobre mana atribuído, adiciona a trecho final separador
          if( manaToAssignSnippet ) endSnippet += ' and ';

          // Adiciona a trecho final mana gasto com magias adjuntas
          endSnippet += adjunctSpellsSnippet;

          // Adiciona a trecho final informações sobre magias adjuntas
          endSnippet += adjunctSpells.length == 1 ? ` with the spell ` : ` in total with the spells: `;

          // Adiciona ao trecho final nome das magias adjuntas
          for( let adjunctSpell of adjunctSpells ) endSnippet += `"${ adjunctSpell.content.designation.title }", `;

          // Remove separador final sobre magias adjuntas
          endSnippet = endSnippet.replace( /, $/, '' );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ spellName }${ spellExtraTargetsSnippet }${ spellTargetSnippet }${ endSnippet }.`;
      }
      case 'action-deploy': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Caso registro seja o completo, complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
        if( isFull ) committerName += committer.content.designation.getIndex( true );

        // Retorno do texto
        return `${ committerName } was deployed to ${ targetName }.`;
      }
      case 'action-disengage': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will disengage' : 'disengaged';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } to ${ targetName }.`;
      }
      case 'action-energize': {
        // Identificadores
        let { committer, target, manaToTransfer } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            manaToTransferSnippet = `${ manaToTransfer } mana point${ manaToTransfer == 1 ? '' : 's' }`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } energized ${ targetName } via ${ manaToTransferSnippet }.`;
      }
      case 'action-engage': {
        // Identificadores
        let { committer, target, formerSlot } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will engage' : 'engaged';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer, formerSlot );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } in ${ targetName }.`;
      }
      case 'action-equip': {
        // Identificadores
        let { committer, target, itemAttacher } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            verbSnippet = target.isToAttach ? 'equipped' : 'used',
            itemAttacherSnippet = itemAttacher == committer ? '' : ' to ' + itemAttacher.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o vinculante do alvo
          if( itemAttacher.isEmbedded ) itemAttacherSnippet += itemAttacher.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          /// Para o vinculante do alvo
          itemAttacherSnippet += itemAttacher == committer ? '' : m.languages.snippets.getCardPosition( itemAttacher );
        }

        // Retorno do texto
        return `${ committerName } ${ verbSnippet } ${ targetName }${ itemAttacherSnippet }.`;
      }
      case 'action-expel': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will expel mana' : 'expelled 1 mana point';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Caso ação tenha sido concluída, indica essa informação
          if( eventType == 'finish' ) tenseSnippet += ', and finished this action';
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet }.`;
      }
      case 'action-guard': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } is guarding ${ targetName }.`;
      }
      case 'action-handle': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            operationText = target.isInUse ? 'disuse' : 'use';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o alvo
          if( target.isEmbedded ) targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } put to ${ operationText } ${ targetName }.`;
      }
      case 'action-intercept': {
        // Identificadores
        let { committer, target, interceptedBeing } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' ),
            interceptedBeingName = interceptedBeing.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o ente interceptado
          interceptedBeingName += interceptedBeing.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o ente interceptado
          interceptedBeingName += m.languages.snippets.getCardPosition( interceptedBeing );
        }

        // Retorno do texto
        return `${ committerName } intercepted ${ interceptedBeingName } in ${ targetName }.`;
      }
      case 'action-leech': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will leech' : eventType == 'finish' ? 'finished leeching' : 'leeched',
            energyPoints = Math.round( target.content.stats.attributes.current.stamina * .5 ),
            finalSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          // Caso ação tenha sido concluída, adiciona essa informação
          if( eventType == 'finish' )
            finalSnippet += ', having restored their energy to the base value'

          // Caso ação tenha sido acionada, adiciona quantidade de pontos de energia restaurados
          else if( action.isCommitted )
            finalSnippet += `, restoring ${ energyPoints } energy point${ energyPoints == 1 ? '' : 's' }`;
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ targetName }${ finalSnippet }.`;
      }
      case 'action-possess': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will possess' : 'possessed';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o alvo
          targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ targetName }.`;
      }
      case 'action-prepare': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            committerSize = committer.content.typeset.size,
            segmentsQuantity = committerSize == 'large' ? '3 segments' : committerSize == 'medium' ? '2 segments' : '1 segment';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } opened moves from their player within ${ segmentsQuantity }.`;
      }
      case 'action-relocate': {
        // Identificadores
        let { committer, target, formerSlot } = action,
            committerName = committer.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer, formerSlot );
        }

        // Retorno do texto
        return `${ committerName } was relocated to ${ targetName }.`;
      }
      case 'action-retreat': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full,
            tenseSnippet = action.flowCost && !action.isCommitted ? 'will retreat' : 'retreated';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet }.`;
      }
      case 'action-summon': {
        // Identificadores
        let { committer, target, token } = action,
            committerName = committer.content.designation.full,
            tokenName = token.content.designation.full,
            targetName = isFull ? target.name : target.name.replace( /^.+-/g, '' );

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para a ficha
          tokenName += token.content.designation.getIndex( true, committer.deck );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } summoned ${ tokenName } in ${ targetName }.`;
      }
      case 'action-sustain': {
        // Identificadores
        let { committer, target: spell, manaToAssign } = action,
            { target: spellTarget } = spell,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.full,
            spellTargetSnippet = m.languages.snippets.getSpellTargetSnippet( spellTarget, committer ),
            resource = committer instanceof m.AstralBeing ? 'energy' : 'mana',
            tenseSnippet = eventType == 'finish' ? 'finished sustaining' : 'sustained',
            manaToAssignSnippet = isFull && manaToAssign && ( eventType != 'finish' || spell.content.typeset.durationSubtype != 'constant' ) ?
              `, via ${ manaToAssign } assigned ${ resource } point${ manaToAssign == 1 ? '' : 's' }` : '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );

          // Complementa nome da magia com indicação de em que casa está
          spellName += m.languages.snippets.getCardPosition( spell );

          // Para caso alvo da magia seja uma carta que não o acionante
          if( spellTarget instanceof m.Card && spellTarget != committer ) {
            // Para entes e equipamentos embutidos, complementa nome do alvo da magia com índice de sua posição no baralho ante outras cartas com a mesma designação
            if( spellTarget instanceof m.Being || spellTarget.isEmbedded ) spellTargetSnippet += spellTarget.content.designation.getIndex( true );

            // Complementa nome do alvo da magia com indicação de em que casa está
            spellTargetSnippet += m.languages.snippets.getCardPosition( spellTarget );
          }

          // Para caso haja mana atribuído
          if( manaToAssignSnippet ) {
            // Identificadores
            let manaWithPolarityBurden = spell.applyPolarityBurden( manaToAssign );

            // Quando aplicável, adiciona a trecho de mana atribuído o custo com o ônus de polaridade
            if( manaToAssign != manaWithPolarityBurden ) manaToAssignSnippet += ` (${ manaWithPolarityBurden } spent points, with the polarity burden)`;
          }
        }

        // Retorno do texto
        return `${ committerName } ${ tenseSnippet } ${ spellName }${ spellTargetSnippet }${ manaToAssignSnippet }.`;
      }
      case 'action-swap': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.full;

        // Retorno do texto
        return `"${ committerName }" was swapped with "${ targetName }".`;
      }
      case 'action-transfer': {
        // Identificadores
        let { committer, target, itemOwner, itemAttacher } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title,
            ownerName = itemOwner.content.designation.full,
            itemAttacherSnippet = itemOwner == itemAttacher ? '' : `${ itemAttacher.content.designation.title }`;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Para o novo dono do alvo
          ownerName += itemOwner.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o novo vinculante do alvo
          if( itemAttacher.isEmbedded ) itemAttacherSnippet += itemAttacher.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );

          /// Para o novo dono do alvo
          ownerName += m.languages.snippets.getCardPosition( itemOwner );

          /// Para o novo vinculante do alvo, se diferente do novo dono
          if( itemOwner != itemAttacher ) itemAttacherSnippet += m.languages.snippets.getCardPosition( itemAttacher );
        }

        // Caso haja conteúdo no trecho sobre vinculante, concatena a ele separador
        if( itemAttacherSnippet ) itemAttacherSnippet += ' of ';

        // Retorno do texto
        return `${ committerName } transferred ${ targetName } to ${ itemAttacherSnippet }${ ownerName }.`;
      }
      case 'action-unequip': {
        // Identificadores
        let { committer, target } = action,
            committerName = committer.content.designation.full,
            targetName = target.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome de cartas com índice de sua posição no baralho ante outras cartas com a mesma designação

          /// Para o acionante
          committerName += committer.content.designation.getIndex( true );

          /// Quando um equipamento embutido, para o alvo
          if( target.isEmbedded ) targetName += target.content.designation.getIndex( true );

          // Complementa nome de cartas com indicação de em que casa estão

          /// Para o acionante
          committerName += m.languages.snippets.getCardPosition( committer );

          /// Para o alvo
          targetName += m.languages.snippets.getCardPosition( target );
        }

        // Retorno do texto
        return `${ committerName } unequipped ${ targetName }.`;
      }
      case 'action-unsummon': {
        // Identificadores
        let { committer } = action,
            committerName = committer.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `${ committerName } was unsummoned.`;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre resultados de eventos
  notices.getOutcome = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'combat-break': {
        // Identificadores
        let { combatBreak, isFull = false } = config,
            { attack, defense, attacker, defender } = combatBreak,
            attackerName = attacker.content.designation.full,
            defenderName = defender.content.designation.full,
            attackName = `"${ m.languages.keywords.getManeuver( attack.maneuver.name ).toLowerCase() }"`,
            defenseSnippet = defense ? `, against "${ m.languages.keywords.getManeuver( defense.maneuver.name ).toLowerCase() }"` : '',
            inflictedPoints = combatBreak.gaugeInflictedAttack(),
            inflictedPointsSnippet =
              inflictedPoints ? `${ inflictedPoints } attack point${ inflictedPoints == 1 ? '' : 's' } inflicted` :
                                'no attack points inflicted',
            outcomeSnippet = '',
            fatePointsSnippet = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Identificadores
          let assignedPoints = combatBreak.getAssignedPoints(),
              attackModifiers = Object.values( assignedPoints.attackModifiers ).reduce( ( accumulator, current ) => accumulator += current, 0 ),
              defenseModifiers = Object.values( assignedPoints.defenseModifiers ).reduce( ( accumulator, current ) => accumulator += current, 0 ),
              dynamicModifiers = {
                attack: attackModifiers - assignedPoints.attackModifiers.base,
                defense: defenseModifiers - assignedPoints.defenseModifiers.base
              },
              totalPoints = {
                attack: assignedPoints.attack + attackModifiers,
                defense: assignedPoints.defense + defenseModifiers
              },
              fateChoices = combatBreak.getFateChoices();

          // Complementa nome das cartas com índice de suas posições no baralho ante outras cartas com a mesma designação

          /// Para o atacante
          attackerName += attacker.content.designation.getIndex( true );

          /// Para o defensor
          defenderName += defender.content.designation.getIndex( true );

          // Complementa nome das cartas com indicação de em que casa estão

          /// Para o atacante
          attackerName += m.languages.snippets.getCardPosition( attacker );

          /// Para o defensor
          defenderName += m.languages.snippets.getCardPosition( defender );

          // Define cômputo de pontos totais
          for( let name of [ 'attack', 'defense' ] )
            totalPoints[ name + 'Sum' ] = `(${ assignedPoints[ name ] }P + ${ assignedPoints[ name + 'Modifiers' ].base }SM + ${ dynamicModifiers[ name ] }DM)`;

          // Define trecho do resultado do ataque

          /// Início
          outcomeSnippet += `The total points were `;

          /// Pontos de ataque
          outcomeSnippet += `${ totalPoints.attack } of attack ${ totalPoints.attackSum }`;

          /// Pontos de defesa, se existentes
          if( defense ) outcomeSnippet += ` and ${ totalPoints.defense } of defense ${ totalPoints.defenseSum }`;

          /// Final
          outcomeSnippet += `, resulting in ${ inflictedPointsSnippet }`;

          // Define trecho dos pontos de destino gastos

          /// Para caso ambos os jogadores tenham gasto pontos de destino
          if( fateChoices.attacker && fateChoices.defender ) fatePointsSnippet += ` Both players spent 1 fate point.`

          /// Para caso o jogador do atacante tenha gasto 1 ponto de destino
          else if( fateChoices.attacker ) fatePointsSnippet += ` The attacking player spent 1 fate point.`

          /// Para caso o jogador do defensor tenha gasto 1 ponto de destino
          else if( fateChoices.defender ) fatePointsSnippet += ` The defending player spent 1 fate point.`;
        }

        // Do contrário
        else {
          // Define trecho do resultado do ataque
          outcomeSnippet = `This resulted in ${ inflictedPointsSnippet }`;
        }

        // Retorno do texto
        return `${ attackerName } attacked ${ defenderName } with ${ attackName }${ defenseSnippet }. ${ outcomeSnippet }.${ fatePointsSnippet }`;
      }
      case 'combat-break-result':
        return 'This is the result of the combat break.';
      case 'inflicted-damage': {
        // Identificadores
        let { damages, damageTarget: being, isToBeRemoved = false, isFull = false } = config,
            beingName = being.content.designation.full,
            damageTypeSnippet = damages[ 0 ].isCritical ? 'critical ' : damages[ 0 ].isGraze ? 'graze ' : '',
            damageText = '';

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );
        }

        // Define início do texto sobre danos sofridos
        damageText += `${ beingName } suffered a ${ damageTypeSnippet }damage of `;

        // Itera por danos infligidos
        for( let damage of damages ) {
          // Identificadores
          let { type, totalPoints } = damage,
              damageSnippet = type != 'raw' ? m.languages.keywords.getDamage( damage.name ).toLowerCase() :
                              damage.maneuver ? `"${ m.languages.keywords.getManeuver( damage.maneuver.name ).toLowerCase() }"` :
                              damage.source ? `"${ damage.source.content.designation.title }"` : 'raw damage';

          // Adiciona pontos de dano alvo ao texto sobre danos infligidos
          damageText += `${ totalPoints } point${ totalPoints == 1 ? '' : 's' } from ${ damageSnippet }, and `;
        }

        // Remove separador final do dano infligido
        damageText = damageText.replace( /, and $/, '' );

        // Adiciona informação sobre se ente foi removido por conta do dano
        if( isToBeRemoved ) damageText += ', having been removed';

        // Finaliza sentença
        damageText += `.`;

        // Retorno do texto
        return damageText;
      }
      case 'healed-damage': {
        // Identificadores
        let { healedDamage, healTarget: being, source, isFull = false } = config,
            beingName = being.content.designation.full,
            sourceName = source.content.designation.title;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );
        }

        // Retorno do texto
        return `${ beingName } restored ${ healedDamage } health point${ healedDamage == 1 ? '' : 's' } via "${ sourceName }".`;
      }
      case 'fate-break': {
        // Identificadores
        let { fateBreakName, fateBreakResult, decidingPlayer, isContestable, requiredPoints } = config,
            playerName = m.languages.keywords.getParity( decidingPlayer.parity ),
            headingSnippet = `${ playerName } resolved`,
            contestableSnippet = isContestable ? 'a contestable' : isContestable === false ? 'an incontestable' : 'a',
            fateBreakSnippet = m.languages.snippets.getFateBreak( fateBreakName ).replace( /\.$/, '' ).replace( /^F/, 'f' ),
            useSnippet = fateBreakResult.player ? 'used ' : 'did not use ',
            fatePointsSnippet = `${ requiredPoints } fate point${ requiredPoints == 1 ? '' : 's' }`,
            contestationSnippet = fateBreakResult.opponent ? ', and was contested' : '';

        // Retorno do texto
        return `${ headingSnippet } ${ contestableSnippet } ${ fateBreakSnippet }. That player ${ useSnippet }${ fatePointsSnippet }${ contestationSnippet }.`;
      }
      case 'condition-gained': {
        // Identificadores
        let { condition, being, markers } = config,
            beingName = being.content.designation.full,
            conditionName = m.languages.keywords.getCondition( condition.name ).toLowerCase(),
            [ markersSnippet, finalSnippet ] = [ '', '' ];

        // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
        beingName += being.content.designation.getIndex( true );

        // Complementa nome do ente com indicação de em que casa está
        beingName += m.languages.snippets.getCardPosition( being );

        // Para caso condição seja gradativa
        if( condition instanceof m.GradedCondition ) {
          // Complementa texto com marcadores da condição ganhos
          markersSnippet += `${ markers } marker${ markers == 1 ? '' : 's' } of `;
        }

        // Para caso condição seja binária
        else if( condition instanceof m.BinaryCondition ) {
          // Inicia texto do trecho de finalização
          finalSnippet += ', which will expire by the ';

          // Conclui texto do trecho de finalização segundo estágio de expiração
          finalSnippet += ( function () {
            // Identificadores
            var expireStage = condition.expireStage ?? m.GameMatch.current.flow.turn.getChild( condition.maxExpireStageName ),
                expireStageName = m.languages.keywords.getFlow( expireStage.name ).toLowerCase();

            // Retorno do final do trecho para caso estágio seja um período intersticial
            if( expireStage instanceof m.BreakPeriod ) return `start of the next ${ expireStageName }`;

            // Retorno do final do trecho para caso estágio seja um período do combate
            if( expireStage instanceof m.CombatPeriod ) return `end of the ${ expireStageName }`;

            // Retorno do final do trecho para caso estágio seja um bloco
            if( expireStage instanceof m.FlowBlock ) return `end of ${ expireStage.displayName.toLowerCase() }`;

            // Captura nome do bloco em que estágio de expiração possa estar
            let blockName = expireStage.getParent( 'block' )?.displayName.toLowerCase();

            // Retorno do final do trecho para caso estágio seja um segmento
            if( expireStage instanceof m.FlowSegment ) return `end of ${ expireStage.displayName.toLowerCase() }${ blockName ? ` of ${ blockName }` : '' }`;

            // Captura nome do segmento em que estágio de expiração está
            let segmentName = expireStage.getParent( 'segment' ).displayName.toLowerCase();

            // Retorno do final do trecho para caso estágio seja uma paridade
            if( expireStage instanceof m.FlowParity ) return `end of ${ expireStageName } of ${ segmentName }${ blockName ? ` of ${ blockName }` : '' }`;
          } )();
        }

        // Retorno do texto
        return `${ beingName } gained ${ markersSnippet }the "${ conditionName }" condition${ finalSnippet }.`;
      }
      case 'condition-removed': {
        // Identificadores
        let { condition, being, markers } = config,
            beingName = being.content.designation.full,
            conditionName = m.languages.keywords.getCondition( condition.name ).toLowerCase(),
            markersSnippet = '';

        // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
        beingName += being.content.designation.getIndex( true );

        // Complementa nome do ente com indicação de em que casa está
        beingName += m.languages.snippets.getCardPosition( being );

        // Para caso condição seja gradativa e ainda haja ao menos um marcador na condição
        if( condition instanceof m.GradedCondition && condition.markers ) {
          // Complementa texto com marcadores da condição perdidos
          markersSnippet += `${ markers } marker${ markers == 1 ? '' : 's' } of `;
        }

        // Retorno do texto
        return `${ beingName } lost ${ markersSnippet }the "${ conditionName }" condition.`;
      }
      case 'action-prevented': {
        // Identificadores
        let { action, action: { committer }, isFull = false, isInvalid = false } = config,
            actionName = m.languages.keywords.getAction( action.name ).toLowerCase(),
            committerName = committer.content.designation.full,
            validitySnippet = isInvalid ? ', as it is no longer valid' : '';

        // Para caso descrição deva ser uma completa
        if( isFull ) {
          // Complementa nome do acionante com índice de sua posição no baralho ante outras cartas com a mesma designação
          committerName += committer.content.designation.getIndex( true );

          // Complementa nome do acionante com indicação de em que casa está
          committerName += m.languages.snippets.getCardPosition( committer );
        }

        // Retorno do texto
        return `The "${ actionName }" action of ${ committerName } was prevented${ validitySnippet }.`;
      }
      case 'orichalcum-pendant-spell-prevented':
        return `The ${ config.item.content.designation.title } of ${ config.item.owner.content.designation.full } negated the effect of the "${ config.spell.content.designation.title }" spell.`;
      case 'magic-shot-prevented-by-attract': {
        // Identificadores
        let spellName = config.spell.content.designation.full,
            targetName = config.target.content.designation.full,
            snippetTense = config.presentTense ? 'prevents' : 'prevented';

        // Retorno do texto
        return `The "Attract" spell ${ snippetTense } "${ spellName }" from affecting ${ targetName }.`;
      }
      case 'condition-prevented-by-body-cleasing': {
        // Identificadores
        let conditionName = m.languages.keywords.getCondition( config.conditionName ).toLowerCase(),
            targetName = config.target.content.designation.full,
            snippetTense = config.presentTense ? 'prevents' : 'prevented';

        // Retorno do texto
        return `The "Body Cleansing" spell ${ snippetTense } ${ targetName } from receiving the "${ conditionName }" condition.`;
      }
      case 'condition-prevented-by-sanctuary': {
        // Identificadores
        let conditionName = m.languages.keywords.getCondition( config.conditionName ).toLowerCase(),
            targetName = config.target.content.designation.full,
            snippetTense = config.presentTense ? 'prevents' : 'prevented';

        // Retorno do texto
        return `The "Sanctuary" spell ${ snippetTense } ${ targetName } from receiving the "${ conditionName }" condition.`;
      }
      case 'voltaic-outburst-attack-prevented': {
        // Identificadores
        let { action: { committer, target: spell } } = config,
            committerName = committer.content.designation.full,
            spellName = spell.content.designation.title;

        // Retorno do texto
        return `The attack of ${ committerName } via "${ spellName }" was not made, because it was no longer valid.`;
      }
      case 'weapon-withdrawn-by-its-effect': {
        // Identificadores
        let { owner, isFull = false } = config,
            ownerName = owner.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do dono com índice de sua posição no baralho ante outras cartas com a mesma designação
          ownerName += owner.content.designation.getIndex( true );

          // Complementa nome do dono com indicação de em que casa está
          ownerName += m.languages.snippets.getCardPosition( owner );
        }

        // Retorno do texto
        return `The weapon used by ${ ownerName } in the last combat break was withdrawn, due to its effect.`;
      }
      case 'weapon-ranseur-effect': {
        // Identificadores
        let { owner, isFull = false } = config,
            ownerName = owner.content.designation.full;

        // Para caso registro seja o completo
        if( isFull ) {
          // Complementa nome do dono com índice de sua posição no baralho ante outras cartas com a mesma designação
          ownerName += owner.content.designation.getIndex( true );

          // Complementa nome do dono com indicação de em que casa está
          ownerName += m.languages.snippets.getCardPosition( owner );
        }

        // Retorno do texto
        return `The weapon used by ${ ownerName } in the last combat break was withdrawn, due to the effect of the defender's ranseur.`;
      }
      case 'spell-attract-effect': {
        // Identificadores
        let { card: spell, transferredMana } = config,
            headingText = `The effect of the "${ spell.content.designation.title }" spell transferred to ${ spell.target.name }: `,
            transferredManaSnippet = '';

        // Itera por casas que tiveram pontos de mana transferidos para o alvo da magia
        for( let slotName in transferredMana ) {
          // Identificadores
          let manaPoints = transferredMana[ slotName ];

          // Adiciona pontos de mana transferidos da casa alvo ao texto sobre mana transferido
          transferredManaSnippet += `${ manaPoints } mana point${ manaPoints == 1 ? '' : 's' } from ${ slotName }; `;
        }

        // Trata final do trecho sobre mana transferido
        transferredManaSnippet = transferredManaSnippet.replace( /; $/, '.' );

        // Retorno do texto
        return headingText + transferredManaSnippet;
      }
      case 'spell-the-truce-effect':
        return `The effect of the spell "The Truce" was triggered, prematurely ending the post-combat and its battle turn.`;
      case 'spell-the-lure-effect': {
        // Identificadores
        let { card: spell, beings } = config,
            headingText = `The effect of the spell "${ spell.content.designation.title }" was triggered, `,
            beingSnippet = `and has suspended ${ beings.length } being${ beings.length == 1 ? '' : 's' }: `;

        // Para caso nenhum ente tenha sido suspenso
        if( !beings.length ) return headingText + 'but no being was suspended.';

        // Itera por entes afetados
        for( let being of beings ) {
          // Identificadores
          let beingName = being.content.designation.full;

          // Complementa nome do ente com índice de sua posição no baralho ante outras cartas com a mesma designação
          beingName += being.content.designation.getIndex( true );

          // Complementa nome do ente com indicação de em que casa está
          beingName += m.languages.snippets.getCardPosition( being );

          // Adiciona ente ao trecho sobre entes afetados
          beingSnippet += beingName + '; ';
        }

        // Trata final do trecho sobre entes afetados
        beingSnippet = beingSnippet.replace( /; $/, '.' );

        // Retorno do texto
        return headingText + beingSnippet;
      }
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para textos sobre paradas de destino
  notices.getFateBreakText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'fate-break-heading-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints, isContestable } = config,
            contestableSnippet = isContestable ? ' contestable' : isContestable === false ? ' incontestable' : '',
            headingSnippet = `Do you want to spend ${ requiredPoints }${ contestableSnippet } fate point${ requiredPoints == 1 ? '' : 's' }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embedded)`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet } to `;
      }
      case 'fate-break-heading-other': {
        // Identificadores
        let { player, requiredPoints, embeddedPoints, isContestable } = config,
            playerParity = m.languages.keywords.getParity( player.parity ),
            contestableSnippet = isContestable ? ' contestable' : isContestable === false ? ' incontestable' : '',
            headingSnippet = `${ playerParity } is considering spending ${ requiredPoints }${ contestableSnippet } fate point${ requiredPoints == 1 ? '' : 's' }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embedded)`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet } to `;
      }
      case 'fate-break-contestation-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints, effect } = config,
            headingSnippet = `The opponent has spent ${ requiredPoints } fate point${ requiredPoints == 1 ? '' : 's' } to ${ effect }`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embedded point${ embeddedPoints == 1 ? '' : 's' } available)`;

        // Retorno do texto
        return `${ headingSnippet }. Do you want to contest the fate break?${ embeddedSnippet }`;
      }
      case 'fate-break-contestation-other': {
        // Identificadores
        let { player, requiredPoints, embeddedPoints } = config,
            playerParity = m.languages.keywords.getParity( player.parity ),
            usableEmbeddedPoints = Math.min( requiredPoints, embeddedPoints ),
            headingSnippet = `${ playerParity } is considering contesting the fate break`,
            embeddedSnippet = !embeddedPoints ? '' : `, with ${ usableEmbeddedPoints } embedded point${ usableEmbeddedPoints == 1 ? '' : 's' }`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }.`;
      }
      case 'the-feast-contestation-player': {
        // Identificadores
        let { requiredPoints, embeddedPoints } = config,
            headingSnippet = `Do you want to spend ${ requiredPoints } fate point${ requiredPoints == 1 ? '' : 's' } to negate the opponent's contestation, via "The Feast"?`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embedded point${ embeddedPoints == 1 ? '' : 's' } available)`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }`;
      }
      case 'the-feast-contestation-opponent': {
        // Identificadores
        let { requiredPoints, embeddedPoints } = config,
            headingSnippet = `Do you want to spend ${ requiredPoints } fate point${ requiredPoints == 1 ? '' : 's' } to contest the opponent's denying, via "The Feast"?`,
            embeddedSnippet = !embeddedPoints ? '' : ` (${ embeddedPoints == Infinity ? '∞' : embeddedPoints } embedded point${ embeddedPoints == 1 ? '' : 's' } available)`;

        // Retorno do texto
        return `${ headingSnippet }${ embeddedSnippet }`;
      }
      case 'fate-break-prevent-actions':
        return `not have the action in progress of ${ config.committer.content.designation.full } prevented`;
      case 'fate-break-raging-surge-gain-enraged-prevent-emboldened':
        return `have ${ config.orc.content.designation.full } gain ${ config.markers } enraged ${ config.markers == 1 ? 'marker' : 'markers' }, instead of emboldened`;
      case 'fate-break-raging-surge-gain-enraged':
        return `have ${ config.orc.content.designation.full } gain ${ config.markers } enraged ${ config.markers == 1 ? 'marker' : 'markers' }`;
      case 'fate-break-raging-surge-prevent-emboldened':
        return `not have ${ config.orc.content.designation.full } gain ${ config.markers } emboldened ${ config.markers == 1 ? 'marker' : 'markers' }`;
      case 'fate-break-action-possess':
        return `have the "${ m.languages.keywords.getAction( config.action.name ).toLowerCase() }" action of ${ config.action.committer.content.designation.full } against ${ config.action.target.content.designation.full } prevented`;
      case 'fate-break-nimbleness-potion':
        return `not have ${ config.being.content.designation.full } withdrawn via "${ config.item.content.designation.title }"`;
      case 'fate-break-spell-poke':
        return `have the effect of the "${ config.spell.content.designation.title }" spell of ${ config.spell.owner.content.designation.full } against ${ config.spell.target.content.designation.full } negated`;
      case 'fate-break-spell-tear':
        return `have the "${ config.spell.content.designation.title }" spell of ${ config.spell.owner.content.designation.full } withdrawn`;
      case 'fate-break-spell-suppress':
        return `have the "${ config.spell.content.designation.title }" spell of ${ config.spell.owner.content.designation.full } ended`;
      case 'fate-break-spell-consume':
        return `not have ${ config.spell.owner.content.designation.full } becoming possessed`;
      case 'fate-break-spell-pain-twist':
        return `change from ${ config.spell.target.content.designation.full } to ${ config.spell.owner.content.designation.full } the being receiving up to ${ config.spell.damageToAssign } damage point${ config.spell.damageToAssign == 1 ? '' : 's' } via "${ config.spell.content.designation.title }"`;
      case 'fate-break-spell-thunder-strike':
        return `have the attack from "${ config.spell.content.designation.title }" against ${ config.attackTarget.content.designation.full } negated`;
      case 'fate-break-spell-fireball':
        return `have the attack from "${ config.spell.content.designation.title }" deal 8 damage points, and target only ${ config.spell.target.content.designation.full } instead of all physical beings in ${ config.spell.target.slot.name }`;
      case 'fate-break-spell-fissure':
        return `have ${ config.spell.target.content.designation.full } withdrawn via "${ config.spell.content.designation.title }"`;
      case 'fate-break-spell-the-beast': {
        // Identificadores
        let { spell } = config,
            { fateBreakTarget } = spell.content.effects;

        // Retorno do texto
        return `have ${ fateBreakTarget?.content.designation.full ?? 'a biotic creature' } withdrawn via "${ config.spell.content.designation.title }"`;
      }
      case 'fate-break-condition-diseased':
        return `have ${ config.being.content.designation.full } withdrawn by the "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }" condition`;
    }
  }

  // Para notificações sobre montagens e validações de baralhos
  notices.getDeckText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'deck-building-no-card-to-filter':
        return 'There are no cards covered by the target filter that are usable with the chosen commander.';
      case 'deck-building-no-active-deck':
        return 'Change the deck frame\'s visible component to that of the deck to which you want to add this card.';
      case 'deck-building-over-max-cards':
        return 'The target deck has already reached its maximum number of cards.';
      case 'deck-building-over-max-coins':
        return 'This card\'s price is above the number of coins available in the target deck.';
      case 'deck-building-over-availability':
        return 'This card has already reached its availability limit.';
      case 'deck-building-command-invalid':
        return 'The main deck\'s command has already reached the maximum number of cards related to this one.';
      case 'deck-building-no-commander-removal':
        return 'To remove the commander, you must restart the building from scratch, clicking on the "Back" button.';
      case 'deck-editing-no-commander-removal':
        return 'You cannot remove the commander of an already built deck.';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações sobre o fluxo e jogadas obrigatórias
  notices.getFlowText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'flow-released':
        return 'Flow released.';
      case 'flow-suspended':
        return 'Flow suspended.';
      case 'flow-release-failure':
        return 'The flow could not be released.';
      case 'flow-suspension-failure':
        return 'The flow could not be suspended.';
      case 'flow-advanced':
        return 'The flow has advanced.';
      case 'synchronizing-flow':
        return 'Synchronizing the flow.';
      case 'flow-intermission':
        return `${ m.languages.keywords.getParity( config.parity ) }'s intermission.`;
      case 'finish-required-move':
        return 'You have 1 or more required moves to finish.';
      case 'must-deploy-commander':
        return 'You must deploy your commander.';
      case 'must-deploy-commander-or-ruler':
        return 'You must deploy your commander, or your being with "The Ruler".';
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações relacionadas à conectividade e afins
  notices.getNetworkText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'failed-request':
        return 'A network error has occurred. Please, check your Internet connection or try again later.';
      case 'server-shut-down-manual':
        return 'The game server has been shut down. Because of this, your current activities will be lost.';
      case 'server-shut-down-error':
        return 'The game server has been shut down, because it encountered an error. The server is restarting or should be soon, but it is not possible to resend to it your current activities, thus the game will be restarted.';
      case 'removed-from-match-by-disconnect':
        return 'Due to connection problems, it was necessary to remove you from the match you were in.';
      case 'match-disconnect-player':
        return 'Your connection to the server has been lost. If we fail to restore it within 1 minute, this match will be aborted.';
      case 'match-disconnect-other':
        return `${ m.languages.keywords.getParity( config.playerParity ) } has disconnected from the server. If they do not reconnect within 1 minute, this match will be aborted.`;
      case 'match-reconnect-player':
        return `You have been reconnected to the server.`;
      case 'match-reconnect-other':
        return `${ m.languages.keywords.getParity( config.playerParity ) } has been reconnected to the server.`;
      case 'abort-match-by-disconnect-player':
        return 'It is not being possible to reconnect you to the server. Because of this, the match you were in was aborted.';
      case 'abort-match-by-disconnect-other':
        return `It was not possible to reconnect the ${ m.languages.keywords.getParity( config.playerParity ).toLowerCase() } player to the server. Because of this, the match you were in was aborted.`;
      default:
        return m.languages.defaultFallback;
    }
  }

  // Para notificações diversas
  notices.getMiscText = function ( textName, config = {} ) {
    // Retorna texto relacionado ao nome passado
    switch( textName ) {
      case 'small-screen':
        return 'This game requires a display screen that is at least 1240 pixels wide and 620 pixels high.';
      case 'no-webgl':
        return 'This game requires a browser that supports WebGL.';
      case 'player-parity':
        return `You are the ${ m.languages.keywords.getParity( config.parity ).toLowerCase() } player.`;
      case 'deck-viewing-no-selected-row':
        return 'There is no row selected for this operation.';
      default:
        return m.languages.defaultFallback;
    }
  }
} )();
