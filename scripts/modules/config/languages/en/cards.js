// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'en' ) return;

  // Identificadores
  const cards = m.languages.cards = {};

  // Propriedades iniciais
  defineProperties: {
    // Retorna dados de localização de carta passada
    cards.getData = function ( card ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

      // Identificadores
      var cardData = {},
          getCardData = card.content.stats?.command ? getCommanderData :
                        card instanceof m.Humanoid ? getHumanoidData :
                        card instanceof m.Being ? getNonHumanoidData :
                        card instanceof m.Item ? getItemData :
                        card instanceof m.Spell ? getSpellData : null;

      // Popula dados da carta
      getCardData();

      // Retorna dados da carta
      return cardData;

      // Retorna dados de comandantes
      function getCommanderData() {
        // Identificadores
        var cardName = card.name.slice( card.name.search( '-' ) + 1 );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getRace( card.content.typeset.race );

        // Atribui à carta dados diversos em função de seu nome, sem o prefixo 'commander'
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'dummy-commander':
              return {
                title: 'Dummy Commander',
                epigraph:
                  'A commander for testing, visible only in this game\'s development version.'
              };
            case 'gaspar':
              return {
                title: 'Gaspar',
                epigraph:
                  'In the service of a human civilization, Gaspar is a general with extensive experience. ' +
                  'Together with Balthasar – a champion of a dwarven clan – and Melchior – a wise elf channeler –, he was chosen to command an army that is the result of a military alliance between a mix of human, dwarven and elven nations.',
                effects:
                  'This commander\'s inactivation does not defeat you as long as you have at least 1 level 4 troop.',
                creaturesConstraint:
                  'Mandatory ' + ( m.languages.keywords.getExperienceLevel( 4 ) + ' ' + m.languages.keywords.getRoleType( 'melee', { person: true } ) + ' ' +
                  m.languages.keywords.getRace( 'dwarf' ) + ' and ' + m.languages.keywords.getExperienceLevel( 4 ) + ' ' +
                  m.languages.keywords.getRoleType( 'magical', { person: true } ) + ' ' + m.languages.keywords.getRace( 'elf' ) ).toLowerCase() + '.'
              };
            case 'ixohch':
              return {
                title: 'Ixohch',
                epigraph:
                  'While [sleeping/meditating], Ixohch, through what he deems to be visions of Mllk – the local god of purification –, sees a future in which all orcs, cornered by the city-dwellers\' imperialism, have united to claim their lands and lives. ' +
                  'Years of professing such visions to his tribe transitioned him from warrior to prophet.\n\n' +
                  'He then managed to enlist neighboring tribes for a march into civilized lands, under an army dubbed The Great Horde.',
                effects:
                  'For each opponent\'s humanoid you remove, you gain 1 fate point: intransitive, if that humanoid is an orc, goblin, or ogre; transitive, otherwise.\n\n' +
                  'Your offensive combat breaks have an attack modifier equal to the difference between your and the opponent\'s current fate points.',
                creaturesConstraint:
                  'No ' + m.languages.keywords.getExperienceLevel( 4 ).toLowerCase() + ' creatures other than ' +
                  m.languages.keywords.getRace( 'orc', { plural: true } ).toLowerCase() + '.'
              };
            case 'nathera':
              return {
                title: 'Nathera',
                epigraph:
                  'Despite being as vast as a great civilization, in the forest where Nathera lives, everything knows her. ' +
                  'With no fixed settlement – but always joined by animals and followers –, she has roamed the elven communities there for longer than anyone can remember, with her youth magically preserved. Wherever she goes, she cures illnesses, and preaches about the belonging of everyone in the forest to a single community, which must fight against its deforestation by expansionist peoples.',
                effects:
                  'Once an opponent\'s being occupies an unoccupied engagement zone, this commander summons 1 bear there.\n\n' +
                  'Once the opponent withdraws a troop of yours, this commander summons 1 swarm to an engagement zone chosen by you.\n\n' +
                  'In pre-combats, this commander summons 1 wolf to each engagement zone contested or controlled by you.',
              };
            case 'the-magnate':
              return {
                title: 'The Magnate',
                epigraph:
                  'The saying is that the Magnate was one of the first to boost trade relations between the surface and the underground, and from this he made his fortune. ' +
                  'Few know his name, but even fewer know anyone else referred to as a magnate.\n\n' +
                  'Renowned as the richest dwarf that ever lived, the Magnate finances wars on a whim, and can hire waves of mercenaries in the blink of an eye.',
                effects:
                  'You cannot have a side deck.\n\n' +
                  'At the beginning of arrangements: this commander is placed in your reserve, irreversibly unlinked; you then choose one of your beings to be the new commander of your main deck for the current round.'
              };
            case 'dofro':
              return {
                title: 'Dofro',
                epigraph:
                  'Originally a shepherd, Dofro was driven to arms by a channeler who, posing as a traveler, kidnapped him while visiting his town. ' +
                  'According to the channeler, the cosmic annals predestine Dofro to destroy a relic that he will obtain through combat, or else an evil force will use it to conquer the world.\n\n' +
                  'Since then, Dofro managed to recruit some combatants for his cause, and, alert, trains for the day of his encounter with the relic.',
                effects:
                  'At the end of arrangements, you must swap your relic with a card from the opponent\'s main deck: other than [their commander/a relic]; the price of which is not greater than the relic\'s. ' +
                  'If no being in the relic\'s new deck has a size equal to its own, the relic\'s size changes to that of its new commander.\n\n' +
                  'In battles, you defeat the opponent if the swapped relic is not active.',
                creaturesConstraint:
                  'No ' + ( m.languages.keywords.getRoleType( 'magical', { person: true } ) + ' ' +
                  m.languages.keywords.getRace( 'halfling', { plural: true } ) + '; mandatory ' + m.languages.keywords.getExperienceLevel( 4 ) + ' ' +
                  m.languages.keywords.getRoleType( 'magical', { person: true } ) ).toLowerCase() + '.'
              };
            case 'finnael':
              return {
                title: 'Finnael',
                epigraph:
                  'Finnael lives in a thriving civilization, whose settlements dot a mountain range with several deposits of precious minerals. ' +
                  'There, dwarves and gnomes live together in an egalitarian and symbiotic way, with the former in charge of tasks that demand rigor, and the latter, of those that demand inventiveness.\n\n' +
                  'As the commander of a frontier battalion, Finnael\'s duty is to repel invaders who covet the riches of nearby settlements.',
                effects:
                  'In battles, you are defeated if you have no active dwarves or gnomes.'
              };
            case 'wimis':
              return {
                title: 'Wimis',
                epigraph:
                  'Raised in the pack of a ruthless despot, Wimis feigned absolute subservience to avoid execution or torture. ' +
                  'However, craving a position of power, he made a pact with a demon, who then promised him the command of a more submissive people if he, when the opportunity arose, exterminated his pack.\n\n' +
                  'Having done so, Wimis was directed to an orc slaver tribe, whose spirit guide had foretold the arrival of a venerable goblin warlock.',
                effects:
                  'Your level 1 humanoids to be withdrawn by the opponent are instead suspended, along with their possessions.\n\n' +
                  'When suspended in this way, these humanoids\' stats and embedded items are reset to the initial state.',
                creaturesConstraint:
                  'No ' + ( m.languages.keywords.getExperienceLevel( 1 ) + ' ' + m.languages.keywords.getRace( 'orc', { plural: true } ) +
                  '; no ' + m.languages.keywords.getExperienceLevel( 3 ) + ' creatures other than ' +
                  m.languages.keywords.getRace( 'orc', { plural: true } ) ).toLowerCase() + '.'
              };
            case 'gamomba':
              return {
                title: 'Gamomba',
                epigraph:
                  'Marginalized for wanting to become a warrior despite her sex, at a young age Gamomba left her ogre village in search of greater acceptance.\n\n' +
                  'Her wanderings came to an end when she found housing in a halfling village already used to sheltering goblin deserters from nearby packs. Over time, Gamomba became the main protector and matriarch of the village, which nicknamed her Great Mother.',
                effects:
                  'While this commander is in card slots with any of your troops, she cannot be the direct target of the opponent\'s [actions/effects].\n\n' +
                  'While this commander is in card slots with: any of your halflings, acts made [by/against] her for which you can spend fate points use them automatically and for free; any of your goblins, her "engage" action has no flow cost, and can target any occupied engagement zone.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }

      // Retorna dados de humanoides
      function getHumanoidData() {
        // Identificadores
        var cardRace = card.content.typeset.race,
            cardRole = card.content.typeset.role.name,
            cardExperience = card.content.typeset.experience.grade;

        // Define título da carta
        cardData.title = `${ m.languages.keywords.getExperienceGrade( cardExperience ) } ${ m.languages.keywords.getRoleName( cardRole ) }`;

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getRace( cardRace );

        // Define designação completa da carta
        cardData.fullDesignation = `${ cardData.subtitle } ${ cardData.title }`;

        // Define epígrafe da carta
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( `${ cardRace }-${ cardRole }` ) {
            case 'human-maceman':
              return {
                epigraph:
                  'Most current human societies are not in the habit of fighting primarily with macemen. Instead, they typically employ maces as sidearms.'
              };
            case 'human-swordsman':
              return {
                epigraph:
                  'Human swordsmen usually dedicate themselves to dueling. Dueling manuals, backed by an ancient tradition, teach them how to handle swords with excellence.'
              };
            case 'human-axeman':
              return {
                epigraph:
                  'Plenty of human nobles are well versed in the handling of the poleaxe. They commonly use it in duels, but sometimes also take it to wars.'
              };
            case 'human-spearman':
              return {
                epigraph:
                  'In time of war, humans tend to raise waves of spearmen. Many, with no fighting experience, carry simple, inexpensive spears, whereas those with experience often use halberds.'
              };
            case 'human-slingman':
              return {
                epigraph:
                  'With the spread of easier-to-use ranged weapons, human societies that train slingmen have been dwindling. Some slingmen, nevertheless, still find a place on the battlefields.'
              };
            case 'human-bowman':
              return {
                epigraph:
                  'Since ancient times, bowmen have fronted many human armies. Those who use longbows enjoy remarkable social prestige.'
              };
            case 'human-crossbowman':
              return {
                epigraph:
                  'Crossbowmen usually make up the bulk of the ranged combatants in contemporary human armies, as they require little training time.'
              };
            case 'human-theurge':
              return {
                epigraph:
                  'Humans often gather in temples to hear sermons from their theurges, who educate them about their social and spiritual duties.'
              };
            case 'human-warlock':
              return {
                epigraph:
                  'Warlockry is forbidden in most human societies, which torture and execute anyone found to have consorted with abyssal beings. Nevertheless, lust for power seduces many humans into this practice.'
              };
            case 'human-wizard':
              return {
                epigraph:
                  'Few channelers are well-liked and intelligent enough to enroll in human arcane academies, by default run by the clergy. Those who are admitted learn there how to control their mana.'
              };
            case 'human-sorcerer':
              return {
                epigraph:
                  'When out of place in the clergy or in arcane academies, humans who want to develop their channeling must usually do so in secrecy. However, some are tolerated as diviners or mercenaries.'
              };
            case 'orc-maceman':
              return {
                epigraph:
                  'Orc macemen traditionally fight with massive gadas, weighing more than 20 kilograms. However, those without much strength yet usually use clubs, and mainly act as scouts and signallers.'
              };
            case 'orc-axeman':
              return {
                epigraph:
                  'Battle axes are often the most valued weapons of orc tribes. Many axemen from these tribes also use shields, although they are not averse to amassing battle scars.'
              };
            case 'orc-spearman':
              return {
                epigraph:
                  'Spears are used by orcs mainly for hunting, which is the main way of feeding their tribes. Hunters from these tribes often do not distinguish beasts from humanoids that are in the vicinity.'
              };
            case 'orc-bowman':
              return {
                epigraph:
                  'Although orcs lack the dexterity to be bowmen as proficient as humans and elves, their musculature allows them to wield more powerful bows than the ones of these races.'
              };
            case 'orc-warlock':
              return {
                epigraph:
                  'Many orcs try to make pacts with entities worshipped by their tribe, but only a handful arouse the interest of any. Those who succeed instill fascination and respect in their peers.'
              };
            case 'orc-sorcerer':
              return {
                epigraph:
                  'In an orc tribe, female orcs traditionally hold servile positions. Those with mana, however, become shamans, and by extension are the moral and spiritual foundation of their tribe.'
              };
            case 'elf-swordsman':
              return {
                epigraph:
                  'Elves who inhabit forests often patrol the outskirts of their community with swordsmen. When necessary, these swordsmen are also sent out for ambushes, skirmishes, escorts, and the like.'
              };
            case 'elf-spearman':
              return {
                epigraph:
                  'Usually, elves only recruit spearmen for warfare. Nevertheless, their race\'s dexterity allows them to handle spears with ease, and it is common for the more experienced to fight with one in each hand.'
              };
            case 'elf-slingman':
              return {
                epigraph:
                  'Many forest-dwelling elves have used slings since they were young, as a hobby. There, although bowmen are often preferred for combat, slingmen with good aim are sometimes recruited as well.'
              };
            case 'elf-bowman':
              return {
                epigraph:
                  'Elves can easily shoot arrows in rapid succession, and without impairing their aim. However, the use of longbows is rare among them, as they generally have little physical strength.'
              };
            case 'elf-theurge':
              return {
                epigraph:
                  'Elf theurges care for the preservation of their community, and the environment in which it is located. To this end, they take it upon themselves to repel intruders, often lethally.'
              };
            case 'elf-warlock':
              return {
                epigraph:
                  'Elves given over to warlockry usually seek to better study and experience the astral environment. Out of stigma or whim, many become hermits, living in ever greater attunement with the Abyss.'
              };
            case 'elf-wizard':
              return {
                epigraph:
                  'For most elves, a wizard\'s success lies in the use of magic in all its forms: from ostentation to subtlety; from power over matter to power over mind.'
              };
            case 'dwarf-maceman':
              return {
                epigraph:
                  'In the underground, maces have no place in the dwarven rules of war. On the other hand, they are the most common weapons of criminals there, as well as of autonomous dwarves living on the surface.'
              };
            case 'dwarf-swordsman':
              return {
                epigraph:
                  'In the old days, legionaries were the core of the dwarven armies in the underground. Many of their descendants still hold this position, but now only as guards of the civil order and of noble Houses.'
              };
            case 'dwarf-axeman':
              return {
                epigraph:
                  'For underground dwarves, axes are gladiator weapons. There, those of little renown fight each other with battle axes, whereas veterans, who usually represent a noble House, wield poleaxes.'
              };
            case 'dwarf-spearman':
              return {
                epigraph:
                  'Current wars between underground dwarves follow protocols, which schedule each battle. In one, formations of pikemen face each other, and the battle is ended after a certain number of broken ranks.'
              };
            case 'dwarf-crossbowman':
              return {
                epigraph:
                  'Crossbows are rarely seen in the underground, due to the dwarves\' customs and their cities\' structure, which does not favor ranged weapons. However, they are widely used by dwarves on the surface.'
              };
            case 'halfling-swordsman':
              return {
                epigraph:
                  'Halflings normally use only wooden swords, for recreation. Among those with real swords, their purpose is generally restricted to self-defense or the defense of major establishments.'
              };
            case 'halfling-spearman':
              return {
                epigraph:
                  'By default, halfling spearmen prevent internal conflicts in their towns, and guard them against external threats. The more experienced ones are often trained to disarm larger races.'
              };
            case 'halfling-slingman':
              return {
                epigraph:
                  'Instead of confronting larger races directly, halflings prefer to launch sneak attacks against them. To this end, they usually hide slingmen, or position them in hard-to-reach places.'
              };
            case 'halfling-bowman':
              return {
                epigraph:
                  'While it is uncommon for halfling towns to train bowmen for warfare, some encourage the practice of archery by organizing periodic bow-shooting events.'
              };
            case 'halfling-crossbowman':
              return {
                epigraph:
                  'In a halfling town, all residents are expected to contribute to defending it from raids. Thus, they usually learn how to use crossbows at a young age, and progress according to their skills and will.'
              };
            case 'halfling-theurge':
              return {
                epigraph:
                  'Theurges of halfling towns are often sages and oracles. They typically release their mana in mental pleas to foretell, induce and prevent events, and advise their people upon omens.'
              };
            case 'halfling-sorcerer':
              return {
                epigraph:
                  'The frailness of halflings prompts many of their towns to form special squads of powthes, meant to engage in fights too risky for their mundane combatants.'
              };
            case 'gnome-spearman':
              return {
                epigraph:
                  'Wars are rare in gnome societies. Their communes usually have no more than a handful of mundane combatants — usually spearmen — for defense, many of whom have never been in combat.'
              };
            case 'gnome-slingman':
              return {
                epigraph:
                  'Some gnome communes have a significant number of slingmen, although this is mostly for cultural reasons, as many of their recreational activities use slings.'
              };
            case 'gnome-crossbowman':
              return {
                epigraph:
                  'In general, gnomes fight only with their minds, subduing their opponent\'s will. Against the most willful or against large enemy groups, crossbows are also often used.'
              };
            case 'gnome-theurge':
              return {
                epigraph:
                  'Legend has it that the magical energy inherent in every gnome is a gift from Celestia. In the hope that it will never stop flowing, gnome theurges often establish relationships with celestial beings.'
              };
            case 'gnome-warlock':
              return {
                epigraph:
                  'No gnomes need pacts to channel. Even so, in some of their communes there is warlockry, but while consisting in submitting abyssal beings so as to have them used for the communal good.'
              };
            case 'gnome-wizard':
              return {
                epigraph:
                  'In gnome societies, wizards are often polymaths. Many serve as educators, teaching new generations how to control and responsibly use their magical energy.'
              };
            case 'goblin-maceman':
              return {
                epigraph:
                  'Goblins prefer fights in which they can quickly disable their opponents. One method they commonly employ to this end is the use of paralyzing poisons, coated on weapons such as flails.'
              };
            case 'goblin-axeman':
              return {
                epigraph:
                  'A humanoid captured alive by a pack of goblins usually entertains the pack until perishing. Many of the games use axes, via which the pack\'s offspring learn how to handle them properly.'
              };
            case 'goblin-crossbowman':
              return {
                epigraph:
                  'Due to their poor eyesight, goblins avoid using ranged weapons. Despite this, as a precaution many of their packs tend to keep a handful of the crossbows they find in their looting close by.'
              };
            case 'goblin-sorcerer':
              return {
                epigraph:
                  'Channelers often command goblin packs, eliminating their competition. When one dies, it is common for their pack to disband into smaller ones, which fight for command until it goes to a new channeler.'
              };
            case 'ogre-maceman':
              return {
                epigraph:
                  'In ogre villages, macemen are often ogres with average strength. Overrepresented in ogre armies, they are usually not armed with much more than clubs.'
              };
            case 'ogre-axeman':
              return {
                epigraph:
                  'Among ogres, respect and worthiness are typically earned through brute force. The strongest ogres in a village usually take the most prominent positions, and are the best-equipped in warfare.'
              };
            case 'ogre-slingman':
              return {
                epigraph:
                  'Skinny ogres tend to fill the lowest positions in an ogre village. However, those who wish to take part in combat are generally accepted as slingmen, provided they have good aim.'
              };
            case 'ogre-wizard':
              return {
                epigraph:
                  'Out of superstition, ogre villages often forbid all practices of magic, except those that can be used as an antidote to such poison.'
              };
          }
        } )() );
      }

      // Retorna dados de entes que não sejam humanoides
      function getNonHumanoidData() {
        // Identificadores
        var cardRace = card.content.typeset.race,
            cardName = card.name;

        // Se ficha for de um morto-vivo, ajusta seu nome para que desconsidere seu prefixo
        if( card instanceof m.Undead ) cardName = cardName.slice( cardName.search( '-' ) + 1 );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'bear':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'Brown bears have a formidable size. When fighting, they use their claws and fangs.'
              };
            case 'wolf':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'A single wolf is not usually a problem for provisioned fighters, but in numbers they can become a significant threat.'
              };
            case 'swarm':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Beast' ),
                epigraph:
                  'A swarm of bees cannot be dispersed by conventional weapons, and attacks its target relentlessly.',
                effects:
                  'Is always levitated, and cannot be a direct target.\n\n' +
                  'Its attack: lasts 5 segments, in which 2 points are inflicted per segment; while in progress, occupies its target; once inflicts on its target 5 damage points, stuns them until it finishes; when finished, removes this creature.'
              };
            case 'golem':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Made of iron, golems are animated by transferring a humanoid soul into their body. This soul, no longer having memories and free will, is incapable of anything other than serving its master.'
              };
            case 'golden-salamander':
              return {
                title: m.languages.keywords.getRace( 'goldenSalamander' ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Magical fire, when imbued with intent, typically resembles a gigantic salamander. Its flames are tamed in such a way as to be directed at its master\'s enemies.',
                effects:
                  'In post-combats, loses 12 health points.'
              };
            case 'indigo-salamander':
              return {
                title: m.languages.keywords.getRace( 'indigoSalamander' ),
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'A fire imbued with wickedness emits more heat, but becomes wilder. Known as a fiendfyre, it sustains itself through the agony of those it consumes, and is intoxicated by a desire to extinguish.',
                effects:
                  'In post-combats, loses 12 health points; in combats, restores 1 health point for every 2 damage points it inflicts.\n\n' +
                  'Becomes chaotic and permanently uncontrollable once its summoner is inactivated, or in the post-combat of a turn in whose combat it did not commit "attack".\n\n' +
                  'Ends any target it removes.'
              };
            case 'undead':
              return {
                title: `${ m.languages.keywords.getRace( card.sourceBeing.content.typeset.race ) } ${ m.languages.keywords.getRace( cardRace ) }`,
                subtitle: m.languages.keywords.getAttachability( 'Construct' ),
                epigraph:
                  'Dead bodies are animated via the same principle that governs other animations. Their accessibility in battle makes their use for war purposes particularly common.'
              };
            case 'tulpa':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Tulpas are thought-forms with enough substance to manifest outside of their creator\'s mind. When very energy-strengthened, they are sometimes consumed to restore a channeler\'s mana.',
                effects:
                  'In post-combats, restores 2 energy points.'
              };
            case 'spirit':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Spirits are the consciousnesses of the [unawakened/deceased], who wander the ethereal plane. Even though they are not visible to most physical beings, mental interactions between the two are common.',
                effects:
                  'Its "rapture" attack only targets those whose current health is [equal to/less than] half of their base health.\n\n' +
                  'In the overtime of its "rapture" attack, the fate point usable by the opponent reduces this attack\'s damage to 0, whereas the fate point usable by you only negates this possible effect.'
              };
            case 'demon':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Demons are native to the Abyss, a plane of high negativity. There, they enslave spirits, mainly to enjoy their energy and use them in internal conflicts or in operations on the ethereal plane.',
                effects:
                  'Its "rapture" attack only targets those whose current health is [equal to/less than] 3/4 of their base health.'
              };
            case 'angel':
              return {
                title: m.languages.keywords.getRace( cardRace ),
                subtitle: m.languages.keywords.getAttachability( 'AstralBeing' ),
                epigraph:
                  'Angels inhabit Celestia, a plane so subtle that no spirits drawn there are known to have returned. They hold extreme power and positivity, but, always impassive and distant, they are inscrutable.',
                effects:
                  'Cannot be a direct target.\n\n' +
                  'Is ended as soon as you use a negative spell.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }

      // Retorna dados de equipamentos
      function getItemData() {
        // Identificadores
        var { equipage: cardEquipage, size: cardSize, grade: cardGrade } = card.content.typeset,
            cardName = card.name.replace( /((ordinary)|(masterpiece))(-|$)/, '' ).replace( /((small)|(medium)|(large))(-|$)/, '' ).replace( /-$/, '' );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getEquipage( cardEquipage );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'smooth-club':
              return {
                title: 'Smooth Club',
                epigraph:
                  'Although there are more sophisticated weapons for every fighting situation, in many cases a simple club is enough.'
              };
            case 'spiked-club':
              return {
                title: 'Spiked Club',
                epigraph:
                  'When in doubt whether a simple club will be enough for a fight, primitive peoples often add spikes to it.'
              };
            case 'flanged-mace': {
              // Define variável para texto sobre efeito
              let maxPenetration = cardSize == 'large' ? 6 : cardSize == 'medium' ? 4 : 2;

              return {
                title: 'Flanged Mace',
                epigraph:
                  'Flanged maces counter heavy armor in particular, as their flanges minimize the risk of collisions against it being deflected.',
                effects:
                  `In combat breaks, the penetration of this weapon's "bash" attack becomes equal to the lesser of ${ maxPenetration } and the sum of the defender's blunt resistances from: garments whose weight is medium and heavy; "Ironskin"; for golems, the natural resistance.`
              };
            }
            case 'war-hammer': {
              // Define variável para texto sobre efeito
              let maxPenetration = cardSize == 'large' ? 6 : cardSize == 'medium' ? 4 : 2;

              return {
                title: 'War Hammer',
                epigraph:
                  'War hammers are designed to destroy heavy armor – and whoever is wearing it.',
                effects:
                  `In combat breaks, the penetration of this weapon's "bash" attack becomes equal to the lesser of ${ maxPenetration } and the sum of the defender's blunt resistances from: garments whose weight is medium and heavy; "Ironskin"; for golems, the natural resistance.\n\n` +
                  'This weapon is withdrawn if its "thrust" attack inflicts on its target at least 1 attack point but does not remove them.'
              };
            }
            case 'gada':
              return {
                title: 'Gada',
                epigraph:
                  'Gadas, employed since the dawn of many civilizations, are still effective weapons, but only when used by combatants with adequate strength.',
                effects:
                  'If this weapon\'s "bash" attack inflicts critical damage, its target is stunned for the remainder of the current parity.'
              };
            case 'flail':
              return {
                title: 'Flail',
                epigraph:
                  'Flails do not make accurate attacks, but since their attacks are made in quick succession, eventually one hits its target.',
                effects:
                  'This weapon\'s "bash" attack cannot be defended with "block".'
              };
            case 'smasher-maul':
              return {
                title: 'Smasher Maul',
                epigraph:
                  'Undaunted, the dwarf struck with his enchanted maul a powerful blow against the enemy\'s chest. Upon impact, nothing remained of his rocky body but ruin and dust.',
                effects:
                  'If this weapon\'s "bash" attack inflicts critical damage, its target: is withdrawn, if they are attached to "Stoneskin"; otherwise, is stunned for the remainder of the current parity.'
              };
            case 'gladius':
              return {
                title: 'Gladius',
                epigraph:
                  'Until metallurgical knowledge made it possible to produce larger swords, gladiuses were a customary weapon among swordsmen. Nowadays, they are mostly used for self-defense.'
              };
            case 'falchion':
              return {
                title: 'Falchion',
                epigraph:
                  'In battles, falchions are often sidearms, used by those who would rather not be in melee combat if they can avoid it.'
              };
            case 'bastard-sword':
              return {
                title: 'Bastard Sword',
                epigraph:
                  'A bastard sword is any sword small enough for one-handed use, but large enough to increase its performance when used with two hands.'
              };
            case 'longsword':
              return {
                title: 'Longsword',
                epigraph:
                  'Due to their size, the use of longswords for everyday defense is impractical. In order to draw them quickly, combatants typically carry them slung over their shoulders.'
              };
            case 'radiance':
              return {
                title: 'Radiance',
                epigraph:
                  'When drawn, from the Shining Sword radiates a luminance like that of a sun. With a soft touch and penetrating ardor, those who look at this light have their hearts warmed, and their souls illuminated.',
                effects:
                  'In pre-combats or as soon as it is used, owner and your other biotic beings in their card slot gain 3 emboldened markers.\n\n' +
                  'Removes from owner and those in their card slot the "possessed" condition, and prevents them from gaining it.\n\n' +
                  'Tokens in owner\'s card slot: are removed, if they are owned; otherwise, become chaotic and permanently uncontrollable.'
              };
            case 'battle-axe':
              return {
                title: 'Battle Axe',
                epigraph:
                  'Despite their short range, it does not take much dexterity for battle axes to inflict fatal wounds on those they hit.'
              };
            case 'bardiche':
              return {
                title: 'Bardiche',
                epigraph:
                  'Few are those who face a bardicher without adequate protection. Even fewer are those who live to risk it a second time.'
              };
            case 'poleaxe':
              return {
                title: 'Poleaxe',
                epigraph:
                  'Poleaxes are usually the polearms most prized by the nobility. Manuals teaching how to use them in duels are plentiful, and it is common for dueling champions to have been using them since childhood.'
              };
            case 'desolator':
              return {
                title: 'Desolator',
                epigraph:
                  'Enemies flee in droves at the sight of the Desolating Scythe; everyone has heard about how it outrages Death. So cruel, just taking the flesh is not enough: it also claims the soul of whom it catches.',
                effects:
                  'In pre-combats or as soon as it is used, owner gains 5 feared markers.\n\n' +
                  'Ends any target it removes.'
              };
            case 'spear':
              return {
                title: 'Spear',
                epigraph:
                  'Easy to produce and easy to use, few are the armies that do not make spears their main melee weapon.',
                effects:
                  'This weapon\'s "throw" attack withdraws the weapon if it does not remove its target.'
              };
            case 'ranseur':
              return {
                title: 'Ranseur',
                epigraph:
                  'Ranseurs are spears with laminated side projections, capable of better intercepting attacks and disarming the opponent.',
                effects:
                  'At the end of combat breaks in which this weapon uses the "repel" defense against a weapon\'s attack, the attacking weapon is withdrawn if the combat break\'s total attack points were less than the total defense points by 3 points or more.'
              };
            case 'halberd':
              return {
                title: 'Halberd',
                epigraph:
                  'Halberds often replace plain spears in well-equipped armies, and are feared as much for their slashing capability as for their piercing capability.'
              };
            case 'pike':
              return {
                title: 'Pike',
                epigraph:
                  'The greatest benefit of pikes lies in the square formation of their combatants, in which more and more ranks of pikemen make attacks as the distance between the formation and the enemy decreases.',
                effects:
                  'Once this weapon finishes a "thrust" attack whose commitment is dedicated or which was triggered by a weapon\'s range, if there is another pike of yours in use on the same card slot, its owner gains 1 free attack via the pike\'s " thrust " maneuver, against an opponent\'s target chosen by you. ' +
                  'If more than 1 being can make this free attack, the attacker is chosen according to the following criteria, in order of preference: whoever can assign the most points to the attack; whoever occupied the current card slot first.'
              };
            case 'drill-spear':
              return {
                title: 'Drill Spear',
                epigraph:
                  'If thrust properly, this spear makes its way into the body of enemies protected even by heavy armor.',
                effects:
                  'This weapon\'s "throw" attack withdraws the weapon if it does not remove its target.'
              };
            case 'buckler':
              return {
                title: 'Buckler',
                epigraph:
                  'Bucklers are small round shields, usually made of metal. They do not provide high protection, but neither do they decrease the bearer\'s mobility, and they are accessible to most combatants.'
              };
            case 'rondache':
              return {
                title: 'Rondache',
                epigraph:
                  'Rondaches are medium-sized round shields, usually made of metal. They balance protection and mobility.'
              };
            case 'scutum':
              return {
                title: 'Scutum',
                epigraph:
                  'Scutums are large rectangular wooden shields, usually capable of covering their bearer entirely. They provide great protection, but tend to crack when subjected to strong collisions.',
                effects:
                  'In combat breaks in which this weapon uses the "block" defense against an attack whose main damage is slashing, this weapon registers the lowest value between the respective combat break\'s attack and defense points.\n\n' +
                  'At the end of a combat break, this weapon is withdrawn if the sum of the points registered by it over the course of combat breaks is greater than 9.'
              };
            case 'aspis':
              return {
                title: 'Aspis',
                epigraph:
                  'Aspis are round wooden shields, coated in bronze. They offer good protection, but at the expense of greatly reduced mobility.'
              };
            case 'watchful-shield':
              return {
                title: 'Watchful Shield',
                epigraph:
                  'With his spear, he thrust several blows at me in quick succession, but my shield, magically animated, propelled itself to my defense faster than my instincts could keep up.'
              };
            case 'sling':
              return {
                title: 'Sling',
                epigraph:
                  'Typically, a shooter needs years of practice with slings in order to be able to use them effectively in combat. However, their shots are in many cases more lethal than those from bows.'
              };
            case 'shortbow':
              return {
                title: 'Shortbow',
                epigraph:
                  'Bows are relatively simple weapons to craft, but they require strength and a lot of practice for effective use in combat.'
              };
            case 'longbow':
              return {
                title: 'Longbow',
                epigraph:
                  'Longbows are usually almost as tall as their bearer. At the expense of greater strength, their bowstring can be stretched further than that of shortbows, resulting in more powerful shots.'
              };
            case 'crossbow':
              return {
                title: 'Crossbow',
                epigraph:
                  'Crossbows cannot be fired as quickly as bows and slings, but their shots are usually more powerful, and they require little practice to be used effectively.',
                effects:
                  'This weapon\'s "shoot" attack: has a flow cost of 1; does not make free attacks.'
              };
            case 'gambeson':
              return {
                title: 'Gambeson',
                epigraph:
                  'Gambesons are garments padded with many layers of linen, wool, and the like. This makes them particularly good at mitigating the force of blunt impacts.'
              };
            case 'chain-mail':
              return {
                title: 'Chain Mail',
                epigraph:
                  'Chain mail is made up of interlocking metal rings. They are mainly effective against cuts, and are also notable for protecting against electric currents if with links in a circuit down to the ground.'
              };
            case 'scale-armor':
              return {
                title: 'Scale Armor',
                epigraph:
                  'Scale armor consists of small overlapping metal plates on a fabric or leather backing. They offer both reasonable protection and coverage.'
              };
            case 'brigandine':
              return {
                title: 'Brigandine',
                epigraph:
                  'Brigandines are textile garments with internal metal plates. Used especially for slashing and piercing protection, their main disadvantage is their relatively small coverage.'
              };
            case 'plate-armor':
              return {
                title: 'Plate Armor',
                epigraph:
                  'Among mundane armor, plate armor offers the best overall protection, and with great coverage. However, their high production cost restricts their use to only the most valuable combatants.'
              };
            case 'sublime-robe':
              return {
                title: 'Sublime Robe',
                epigraph:
                  'Some robes, usually white, cannot be stained by mundane means, and radiate an intense aura of magnificence, capable of fascinating those who see them.',
                effects:
                  'In pre-combats, owner gains 5 awed markers.\n\n' +
                  'This garment only has effect while it is the only one used by owner.'
              };
            case 'absorption-robe':
              return {
                title: 'Absorption Robe',
                epigraph:
                  'Some robes are enchanted in such a way as to absorb part of the mana released by the wearer, who can easily reuse it. Such robes are particularly useful in very demanding channellings.',
                effects:
                  'Owner immediately restores 1/3 – rounded down – of the mana they spend on each [channeling/sustaining] of theirs.\n\n' +
                  'This garment only has effect while it is the only one used by owner.'
              };
            case 'martyr-shroud':
              return {
                title: 'Martyr\'s Shroud',
                epigraph:
                  'Praised are those who with martyr\'s souls dedicate their lives in this war, for the blood that dyes their shrouds is a testimony to the annals of the unbreakable faith they had in our cause\'s victory.',
                effects:
                  'Once owner is withdrawn by the opponent, you gain 1 transitive fate point.\n\n' +
                  'This garment only has effect while it is the only one used by owner.'
              };
            case 'maniphile-cuirass':
              return {
                title: 'Maniphile Cuirass',
                epigraph:
                  'Combatants facing powthes often feel more protected with a mana-deflecting cuirass than with any plate armor.',
                effects:
                  'Every shot with mana damage that directly targets a being in owner\'s card slot other than them changes its target to owner.\n\n' +
                  'This garment\'s coverage is considered to be 8 for its mana resistance.'
              };
            case 'bone-mail':
              return {
                title: 'Bone Mail',
                epigraph:
                  'Due to their facility in absorbing life energy, the bones of mail are usually enchanted in such a way as to capture some of the life energy dispersed upon death, and then release it as ether.',
                effects:
                  'In post-combats, those in owner\'s card slot suffer 1 attack of 5 points and 1 ether damage point. This damage is, until it has 5 points, increased by 1 point per troop that is withdrawn while this garment is in use.\n\n' +
                  'This garment\'s attack damage: resets to 1 once the garment is inactivated; when greater than 1, in pre-combats is reduced by 1 point.'
              };
            case 'robe-of-the-magi':
              return {
                title: 'Robe of the Magi',
                epigraph:
                  'In ancient times, an elder is said to have passed part of his soul into this robe, wishing to eternize his deeds. With it, the fact is that even the magic of novices is mistaken for that of archmages.',
                effects:
                  'Owner\'s channelings gain automatically and for free the effects of "Quicken", "Extend" and "Empower", spells whose channeling, therefore, becomes disabled for owner.\n\n' +
                  'This garment only has effect while it is the only one used by owner.'
              };
            case 'armor-of-the-heroes':
              return {
                title: 'Armor of the Heroes',
                epigraph:
                  'Legends abound about heroes who, in defense of their people, won fights against primordial monsters. Nowadays, this armor seems to be the only possible remnant of those worn by these heroes.'
              };
            case 'horn': {
              // Define variável para texto sobre efeito
              let segmentsQuantity = cardSize == 'large' ? '3 segments' : cardSize == 'medium' ? '2 segments' : '1 segment';

              return {
                title: 'Horn',
                epigraph:
                  'In battles, horns are used to signal troops about the completion of a certain action or event.',
                effects:
                  `In combats, owner gains the partial action "prepare", which, once finished, opens moves from your troops that are within ${ segmentsQuantity } following the one in which this action is finished.`
              };
            }
            case 'pavise':
              return {
                title: 'Pavise',
                epigraph:
                  'The pavise is a rectangular shield, typically similar in size to its bearer. It is usually fixed to the ground in front of the bearer to provide them with cover against projectiles.',
                effects:
                  'Owner cannot be the target of the opponent\'s "shoot" maneuvers.\n\n' +
                  'This implement has no effect while owner is levitated, or in an engagement zone, or occupied with: the actions "attack", "engage", "retreat"; the actions "channel" or "sustain", provided they have a direct target that is neither owner nor a spell.'
              };
            case 'poison':
              return {
                title: 'Poison',
                epigraph:
                  'A good dose of any of the most potent poisons is enough to tumble even the largest creatures.',
                effects:
                  'Attacher must have an attack with slashing or piercing damage.\n\n' +
                  'At the end of combat breaks in which attacher has inflicted slashing or piercing damage on a biotic being, that being gains 1 diseased marker per attack point that inflicted at least 1 point of one of these damage types.'
              };
            case 'pitch':
              return {
                title: 'Pitch',
                epigraph:
                  'Projectiles dipped in pitch become incendiary, trading accuracy for firepower.',
                effects:
                  'Attacher must be able to attack with "shoot", whose main damage must be piercing.\n\n' +
                  'Attacher\'s "shoot" attacks gain 2 fire damage points, but decrease their range by 1 and double the attack penalty by effective range.'
              };
            case 'scepter-of-command':
              return {
                title: 'Scepter of Command',
                epigraph:
                  'Supreme Sovereign, from what is upon earth to what is in the sky, from what is in the physical to what is in the astral, let this scepter ensure that nothing escapes your power.',
                effects:
                  'Prevents your tokens from [being removed/becoming uncontrollable] due to the summoner\'s inactivation.\n\n' +
                  'When disused, tokens to which this implement\'s effect was applied immediately suffer the effects it prevented, with the exception of those whose summoner is active again.'
              };
            case 'rod-of-absorption':
              return {
                title: 'Rod of Absorption',
                epigraph:
                  'I walked across the meadow with the rod, until I felt it vibrate slightly in my hand. I lifted it and the gem on its tip, as it began to glow, signaled that it was absorbing the mana it had detected.',
                effects:
                  'Owner gains the "mana bolt" maneuver and, in combats, the "tear" full action – which is like the homonymous spell. In both cases, the mana spent is the one attached to this implement.\n\n' +
                  'In redeployments, if this implement is not with the maximum number of attached mana points: owner gains the "absorb" action, if applicable; owner\'s "absorb" action attaches the removed mana to this implement.'
              };
            case 'blazing-staff': {
              // Define variável para texto sobre efeito
              let attackPoints = cardSize == 'large' ? 8 : cardSize == 'medium' ? 6 : 4;

              return {
                title: 'Blazing Staff',
                epigraph:
                  'This staff stores an oscillating amount of energy, which is converted into fire when shot from its mouth. The staff reaches its peak energy when it becomes almost too hot to touch.',
                effects:
                  `Owner gains the "fire bolt" offensive maneuver, which is like the "mana bolt" maneuver, with these differences: its starting attack points are ${ attackPoints }; spent attack points do not reduce the possible mana of owner; its damage is 4 fire points per attack point.`
              };
            }
            case 'shadow-wand': {
              // Define variável para texto sobre efeito
              let maxEnergy = cardSize == 'large' ? 8 : cardSize == 'medium' ? 6 : 4;

              return {
                title: 'Shadow Wand',
                epigraph:
                  'This wand, named after the shadow people, is capable of consuming spirits. It can also eject the ether from consumed spirits, via the shot of an explosive sphere.',
                effects:
                  `In combats, owner gains the full actions: "consume", which is like the homonymous spell, with the difference that it targets those whose current energy – attached to this implement as mana points – is at most ${ maxEnergy } points and that fate points are not usable; "ether blast", which is a shot of R2 range that targets a card slot and makes against its occupants 1 attack of up to ${ maxEnergy } points and 3 ether damage points, in which 1 point of the mana attached to this implement is removed per attack point.`
              };
            }
            case 'shock-gauntlets': {
              // Define variável para texto sobre efeito
              let damageQuantity = cardSize == 'large' ? 3 : cardSize == 'medium' ? 2 : 1;

              return {
                title: 'Shock Gauntlets',
                epigraph:
                  'Some gauntlets are magically ionized, such that they release an electric discharge upon strong collisions. The shock they generate, rarely lethal, is mostly a means of stunning the receiver.',
                effects:
                  `Owner\'s "unarmed" attacks gain a collateral shock damage of ${ damageQuantity } ${ damageQuantity == 1 ? 'point' : 'points' }.`
              };
            }
            case 'strength-bracelets':
              return {
                title: 'Strength Bracelets',
                epigraph:
                  'These bracelets not only increase the wearer\'s strength, but also make them more resistant to disease and other physical ailments.',
                effects:
                  'In pre-combats, owner gains 2 strengthened markers.'
              };
            case 'resolve-diadem':
              return {
                title: 'Resolve Diadem',
                epigraph:
                  'This diadem sharpens the mind of the wearer. Once girded, its wearer is seized by an intense presence of mind, even if before they were full of reluctance or languor.',
                effects:
                  'In pre-combats, owner gains 2 enlivened markers.'
              };
            case 'vigil-amulet':
              return {
                title: 'Vigil Amulet',
                epigraph:
                  'The death of whoever wears this amulet does not leave them too far from the living. The spirit of the dead is still able to perform some of their earthly duties, provided they are committed to them.',
                effects:
                  'If owner is withdrawn, they are then unlinked, and a spirit card is added to your main deck and suspended in your table\'s card slot previously occupied by owner.\n\n' +
                  'The created spirit equals one from a projection of owner, and their attributes are set according to the base values of owner\'s attributes. In addition, the spirit retains possession of the spells of owner prior to owner\'s removal, and, instead of as a token or commander, is always regarded as a creature from a deck.'
              };
            case 'orichalcum-pendant':
              return {
                title: 'Orichalcum Pendant',
                epigraph:
                  'Orichalcum, a now precious metal, was once used extensively. Its anti-magic properties gave cities a degree of stability and security rarely attainable nowadays.',
                effects:
                  'In combats, [Enoth/Voth] spells with a mana cost and whose direct target is owner reduce, at the time of use, 1 potency point per mana point attachable to this implement. ' +
                  'The effect of spells whose potency is in this way reduced to 0 is negated. In addition, the reduced potency points are attached to this implement, as mana points.\n\n' +
                  '[In pre-combats/When inactivated], this implement removes all mana attached to it.'
              };
            case 'ring-of-protection':
              return {
                title: 'Ring of Protection',
                epigraph:
                  'The captain is never seen without that ring. And, after all, why should he? After surviving the third assassination attempt, we begin to believe that the ring is indeed a kind of protective talisman.',
                effects:
                  'Owner gains 1 fate point.\n\n' +
                  'The fate point gained can only be used in combat breaks in which owner is the defender, or in fate breaks whose outcome could: remove owner; reduce owner\'s health, stamina, or will; make owner possessed.'
              };
            case 'flute-of-wolfmancy':
              return {
                title: 'Flute of Wolfmancy',
                epigraph:
                  'This flute emits a sound that, inaudible to humanoids, attracts the attention of wolves in the vicinity. When it plays a certain tune, it induces wolves close enough to obey whoever is playing it.',
                effects:
                  'While using this implement, owner is occupied, but they can disuse it by committing "abort".\n\n' +
                  'In post-combats, if this implement was in use during at least the last 10 segments of the last combat, it summons 2 wolves to an engagement zone chosen by you.'
              };
            case 'ectoplasm-urn':
              return {
                title: 'Ectoplasm Urn',
                epigraph:
                  'The acolyte spent the day with the urn, collecting remnants of the city\'s mana. At night she returned it to the temple, and when it was opened, its ectoplasm quenched the needs of the spirits there.',
                effects:
                  'In combats, owner gains the "energize" partial action, whereby you choose an active [spirit/demon] of yours to gain 1 energy point per mana point you remove from this implement.\n\n' +
                  'In redeployments, if this implement is not with the maximum number of attached mana points: owner gains the "absorb" action, if applicable; owner\'s "absorb" action attaches the removed mana to this implement.'
              };
            case 'flying-carpet':
              return {
                title: 'Flying Carpet',
                epigraph:
                  'Untiring and as fast as the best mounts, flying carpets are a great means of locomotion. However, they cannot carry much weight, and must be constantly maneuvered by their rider.',
                effects:
                  'Owner becomes levitated.\n\n' +
                  'In combats, owner gains the "relocate" action, as a partial action and targeting a card slot in your grid or an occupied engagement zone.\n\n' +
                  'This implement has no effect while owner\'s weight count is above 3.'
              };
            case 'boots-of-levitation': {
              // Define variável para texto sobre efeito
              let weightCount = cardSize == 'large' ? 0 : cardSize == 'medium' ? 1 : 2;

              return {
                title: 'Boots of Levitation',
                epigraph:
                  'As I put on the boots, my feet slipped off the ground. I floated to the center of the battle, and from above I shot many of the enemies without them being able to do anything about it.',
                effects:
                  'Owner becomes levitated.\n\n' +
                  `This implement has no effect while owner\'s weight count is above ${ weightCount }.`
              };
            }
            case 'awareness-potion':
              return {
                title: 'Awareness Potion',
                epigraph:
                  'When I drink this potion, I become more attentive, and immediately feel like I know the best way to proceed in every situation I find myself in.',
                effects:
                  'Target must not be forwardable.\n\n' +
                  'Target becomes forwardable.'
              };
            case 'nimbleness-potion':
              return {
                title: 'Nimbleness Potion',
                epigraph:
                  'Those who drink this potion become restless and hardly get tired. However, it takes a lot of stamina to not be impaired by the bodily exhaustion that marks the end of its effect.',
                effects:
                  'During the combat following this implement\'s use, target gains 3 bonus points in all their melee combat breaks.\n\n' +
                  'In the post-combat following this implement\'s use, if target\'s stamina is less than 8, they are withdrawn if you do not spend 1 incontestable fate point.'
              };
            case 'alertness-potion':
              return {
                title: 'Alertness Potion',
                epigraph:
                  'Used in risky situations, whoever drinks this potion is able to glimpse, just before it happens, a future where their life comes to an end, but against which there is usually time to take precautions.',
                effects:
                  'In the combat following this implement\'s use, as soon as the opponent removes target for the first time, the flow regresses 5 segments, or, lacking that quantity, back to the beginning of the combat.\n\n' +
                  'In the new resolution of the regressed segments: the use of "The Vision" is disabled; this implement\'s effects are suppressed.'
              };
            case 'anticombustible-oil':
              return {
                title: 'Anticombustible Oil',
                epigraph:
                  'This oil inhibits combustion, quickly extinguishing any flames set over the spot on which it was passed.',
                effects:
                  'During the combat following this implement\'s use, target: gains 4 points of natural fire resistance; cannot become ignited, thus all fire damage directed to them is inflicted immediately.'
              };
            case 'frenzy-fragrance':
              return {
                title: 'Frenzy Fragrance',
                epigraph:
                  'The approaching soldier exuded a raspberry-like aroma, which somehow boiled my blood, and clouded my mind. Under a bestial state, soon all I could think of was slaughtering him.',
                effects:
                  'During the combat following this implement\'s use, while occupying target\'s card slot and having will below 8, biotic beings: gain 3 enraged markers; if from the opponent, become uncontrollable, and as soon as possible always commit "attack" against target. Per affected troop of the opponent, they can spend 1 contestable fate point to keep the troop under their control.'
              };
            case 'holy-water':
              return {
                title: 'Holy Water',
                epigraph:
                  'Soldier, do not fear any extraphysical evil: you have been anointed with water blessed by a High Priest. As long as its effect lasts, no astral entity will have power over you.',
                effects:
                  'During the combat following this implement\'s use, target cannot be directly targeted by astral beings.'
              };
            case 'mask-of-concealment':
              return {
                title: 'Mask of Concealment',
                epigraph:
                  'After a certain amount of time immobile, whoever is wearing this mask will no longer be detectable by physical senses: when anyone looks at the wearer, only what is beyond them will be seen.',
                effects:
                  'In combats, while owner is unoccupied for more than 5 consecutive segments, they cannot be directly targeted by physical beings.\n\n' +
                  'This implement\'s effect can be triggered both before and after owner has been occupied.'
              };
            case 'mirror-of-attunement':
              return {
                title: 'Mirror of Attunement',
                epigraph:
                  'After staring at it for a while, this mirror begins to reflect the living being that the bearer is thinking about. As the bearer watches this being, an emotional link is established between them.',
                effects:
                  'In pre-combats, you choose 1 troop, to be the target of this implement\'s effect during the current turn.\n\n' +
                  '"Enlivened", "unlivened", "emboldened", and "enraged" markers applied to owner or target are also applied to the other. The inactivation of one or the disuse of this implement does not affect the current conditions of the other, and, on its reuse, this implement has no longer any effect on the current turn.'
              };
            case 'bell-of-rest':
              return {
                title: 'Bell of Rest',
                epigraph:
                  'The Bell of Rest emits a penetrating tinkle, which, sticking to the mind for hours, causes those who hear it an irresistible psychological fatigue, in which nothing is more desired than a nap.',
                effects:
                  'Starting on block 1 of combats on turns whose number\'s parity equals yours, owner gains the "end" full action, which prevents actions in progress and disables flow suspensions for the remainder of the current combat.'
              };
            case 'anvil-of-souls':
              return {
                title: 'Anvil of Souls',
                epigraph:
                  'Commonly used to produce golems, this anvil captures the soul of those who die upon it, and transfers it to the metal it casts.',
                effects:
                  'In redeployments: owner gains the "summon" action, if not already available; owner\'s "summon" action gains the "golem" option.\n\n' +
                  'For owner to summon a golem, you must end a troop other than them, which must be a creature in your field grid; the golem token is then placed in the card slot previously occupied by the ended troop.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation =
          ( cardSize && cardGrade != 'relic' ? `${ m.languages.keywords.getSize( cardSize ) } ` : '' ) + cardData.title +
          ( cardGrade == 'masterpiece' ? ' (M)' : '' );
      }

      // Retorna dados de magias
      function getSpellData() {
        // Identificadores
        var cardName = card.name.slice( card.name.search( '-' ) + 1 );

        // Define subtítulo da carta
        cardData.subtitle = m.languages.keywords.getPath( card.content.typeset.path );

        // Atribui à carta dados diversos em função de seu nome
        Object.assign( cardData, ( function () {
          // Identificação da carta
          switch( cardName ) {
            case 'shape':
              return {
                title: 'Shape',
                epigraph:
                  'The mana employed by other paths is nothing more than a derivation of that from Metoth, which is in its raw state.',
                effects:
                  'Target is always owner, who chooses 1 path of theirs other than Metoth.\n\n' +
                  'When [used/sustained], restores 1 mana point of the path chosen by owner.\n\n' +
                  'The mana spent by this spell only decreases the mana from Metoth. In addition, this spell is disused once the current mana of the path chosen by owner equals its base mana.'
              };
            case 'orchestrate':
              return {
                title: 'Orchestrate',
                epigraph:
                  'Although sensing, expelling, and absorbing mana are elementary skills for metothes, in general they need a lot of practice until they can make use of mana directly in its free state.',
                effects:
                  'Target must be a spell: of yours suspended or of owner; in disuse; with mana cost; from Metoth. In addition, owner\'s card slot must have mana.\n\n' +
                  'Starts a channeling of target in which owner can assign to its mana cost the mana in their card slot, exclusively or inclusively and under a final value of up to their base Metoth mana.'
              };
            case 'weave':
              return {
                title: 'Weave',
                epigraph:
                  'When the mana of two channelers is linked, both can partially use the other\'s mana, usually in order to channel spells that require more magical energy than they would have alone.',
                effects:
                  'When setting mana [gains/costs] of owner or target, if up to half of the process applies to the other, the maximum of that portion – rounded down – is transferred to them.\n\n' +
                  'For spells to which this effect is applied, the total mana spent is the one used by other mechanics, such as potency or usage cost.\n\n' +
                  'This spell is disused if [owner/target] is inactive or projected.'
              };
            case 'burn':
              return {
                title: 'Burn',
                epigraph:
                  'Experienced metothes are able to use raw mana in place of polished mana. However, this is an extremely grueling process, to the point of being fatal if repeated in a short period of time.',
                effects:
                  'Target must be a spell: of yours suspended or of owner; in disuse; with mana cost; from an owner\'s path other than Metoth.\n\n' +
                  'Starts a channeling of target in which, once its path\'s mana has run out, owner can assign to its mana cost their Metoth mana, under a final value of up to their base Metoth mana.\n\n' +
                  'Once the channeling of target is finished, this spell is ended.'
              };
            case 'attract':
              return {
                title: 'Attract',
                epigraph:
                  'In nature, free mana is concentrated in a few, magically more attractive places. Metothes can commonly increase the magic attraction of a place, so as to temporarily reproduce this phenomenon.',
                effects:
                  'In post-combats, until target reaches its attachable mana limit, transfers to it the mana attached to adjacent card slots, under this precedence: rows C, B, A; opponent\'s grid, your grid; columns 4, 3, 2, 1.\n\n' +
                  'In addition, occupants in target cannot be directly targeted by magic shots from spells whose potency is no greater than twice that of this spell.'
              };
            case 'wither':
              return {
                title: 'Wither',
                epigraph:
                  'A magically barren place inhibits magical manifestations within its perimeter. Channelers know when they are in such a place by the difficulty they have in sensing their own mana there.',
                effects:
                  'In target: the “channel” and “sustain” actions are disabled and, if in progress, prevented; all mana attached to it is removed.\n\n' +
                  'In addition, this spell disables the effect of spells that meet all of these conditions: are not shots; do not have a potency greater than twice that of this spell; are directed to target, its occupants, or the occupants\' attachments.'
              };
            case 'poke':
              return {
                title: 'Poke',
                epigraph:
                  'Although this is usually just an annoying way to get attention, a mental poke is often enough to break the concentration required for channeling.',
                effects:
                  'Target must be occupied with "channel" or "sustain".\n\n' +
                  'Prevents target\'s action.\n\n' +
                  'When used, the opponent can spend 1 incontestable fate point to negate this spell\'s effect.'
              };
            case 'tear':
              return {
                title: 'Tear',
                epigraph:
                  'Anything woven by magic can be torn apart if a force as potent as that which splices the threads is applied.',
                effects:
                  'Target must be a [persistent/permanent] spell in use.\n\n' +
                  'Disuses target. If that does not remove target, you can then spend 1 contestable fate point to withdraw target.\n\n' +
                  'This spell\'s mana cost is equal to the minimum needed for its potency to be no less than that of the target.'
              };
            case 'freeze':
              return {
                title: 'Freeze',
                epigraph:
                  'Even when a channeler has enough power to confront mighty spells, sometimes the channeler just needs them to be silent for a while.',
                effects:
                  'Target must be a [persistent/permanent] spell whose effect is enabled.\n\n' +
                  'Disables target\'s effects, and prevents its potency from being reduced by its persistence cost, if present.\n\n' +
                  'This spell persists for only 1 turn, and its mana cost is equal to the minimum needed for its potency to be no less than half that of the target.'
              };
            case 'suppress':
              return {
                title: 'Suppress',
                epigraph:
                  'The most harmless spell of the enemy is the one that is never used.',
                effects:
                  'Target must be occupied with "channel", whose targeted spell must have mana assigned to it.\n\n' +
                  'Prevents target\'s action and withdraws – or ends, if you spend 1 incontestable fate point – the spell targeted by that action.\n\n' +
                  'This spell\'s mana cost is equal to the minimum needed for its potency to be no less than the predicted of target\'s spell.'
              };
            case 'consume':
              return {
                title: 'Consume',
                epigraph:
                  'Spiritual energy is convertible into mana, at the cost of the spirit\'s absorption. However, the spirit\'s consciousness lingers for a while, and can exert a certain amount of control over the absorber.',
                effects:
                  'Target must be a tulpa or spirit, and their energy must be [less than/equal to] owner\'s base Metoth mana.\n\n' +
                  'Owner is reprepared and restores 1 Metoth mana point per target\'s energy point, which is then ended.\n\n' +
                  'If target is a spirit, in case you then do not spend 1 contestable fate point owner becomes possessed by the opponent.'
              };
            case 'scavenge':
              return {
                title: 'Scavenge',
                epigraph:
                  'Finished spells sometimes leave residual mana in their environment. This residue is more subtle and volatile than ordinary free mana, but can be reused by channelers capable of sensing it.',
                effects:
                  'At the beginning of this spell\'s flow costs, you must choose 1 withdrawn spell, which is ended at the requesting flow cost\'s end.\n\n' +
                  'When [used/sustained], owner restores 4 Metoth mana points.\n\n' +
                  'This spell is disused once owner\'s current Metoth mana equals its base mana.'
              };
            case 'quicken':
              return {
                title: 'Quicken',
                epigraph:
                  'Channelers who have mastered the channeling process are able to use their mana to quicken their spells\' channeling.',
                effects:
                  'Target must be a spell of yours channelable by owner, with mana cost, and with channeling flow cost.\n\n' +
                  'Starts a channeling of target in which its flow cost is reduced by 1.\n\n' +
                  'This spell\'s mana cost is: equal to half of that for channeling target; spent at the same time as target\'s mana cost.'
              };
            case 'extend':
              return {
                title: 'Extend',
                epigraph:
                  'Channelers who have mastered the channeling process are able to use their mana to extend their spells\' range.',
                effects:
                  'Target must be a spell of yours channelable by owner, with mana cost, and with between R1 and R4 range.\n\n' +
                  'Starts a channeling of target in which its range increases by 1.\n\n' +
                  'This spell\'s mana cost is: equal to half of that for channeling target; spent at the same time as target\'s mana cost.'
              };
            case 'empower':
              return {
                title: 'Empower',
                epigraph:
                  'Channelers who have mastered the channeling process are able to channel potent spells using less mana than usual.',
                effects:
                  'Target must be a spell of yours channelable by owner, and with mana cost.\n\n' +
                  'Starts a channeling of target in which its potency, once set, is doubled, as well as its persistence cost, if any.\n\n' +
                  'This spell\'s mana cost is: equal to half of that for channeling target; spent at the same time as target\'s mana cost.'
              };
            case 'reflect':
              return {
                title: 'Reflect',
                epigraph:
                  'Spell poles are like coin faces: both can be seen if you switch the perspective.',
                effects:
                  'Target must be occupied with "channel", whose targeted spell must be polar and from an owner\'s path.\n\n' +
                  'Owner starts a channeling of the spell opposite to that being channeled by target, via their Metoth mana and under a mana cost equal to the predicted potency of target\'s spell.\n\n' +
                  'Once the channeling of owner is finished, this spell is ended.'
              };
            case 'strengthness':
              return {
                title: 'Strengthness',
                epigraph:
                  'After that maceman\'s strength was magnified, his opponent\'s armor could not absorb very well his gada\'s next impact, which, hitting the abdomen, left the opponent nauseated.',
                effects:
                  'Target must not have the maximum number of strengthened markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target: if present, loses 1 weakened marker; otherwise, gains 1 strengthened marker.'
              };
            case 'regeneration':
              return {
                title: 'Regeneration',
                epigraph:
                  'Before the fight, the orc chieftain was assisted by a shaman, who provided his body with hyper-regenerative capabilities. Thus, the injuries he suffered only made him fight more fiercely.',
                effects:
                  'In post-combats, target restores a percentage of their base health, equal to 10% per point of this spell\'s potency plus 5% per point of target\'s stamina.\n\n' +
                  'The percentage of health to be restored is adjusted to 90% if it exceeds this value.'
              };
            case 'body-cleansing':
              return {
                title: 'Body Cleansing',
                epigraph:
                  'Enothe healers often accompany armies on war operations, safeguarding the health of their officers against maladies of mundane or magical origin.',
                effects:
                  'Removes from target the "weakened", "unlivened", "diseased", and "possessed" conditions, and prevents target from gaining them.'
              };
            case 'growth':
              return {
                title: 'Growth',
                epigraph:
                  'In the old days, it was common for gladiators to face dire bears in the arena. These were bears with magically increased size; on four legs, they had a height equivalent to that of 2 adult humans.',
                effects:
                  'Target must have size, which must be smaller than large.\n\n' +
                  'Target and their embedded attachments with size change it to that above the current one, undergoing the related adjustments. ' +
                  'Upon [use/disuse], this spell suspends external attachments of target with size.\n\n' +
                  'This spell can persist for up to 3 turns, and its minimum mana cost increases by 1 per embedded attachment of target with size.'
              };
            case 'pain-twist':
              return {
                title: 'Pain Twist',
                epigraph:
                  'In large battles, enothes sometimes flank slaves and war prisoners, to whom they transfer the wounds of combatants returning from the vanguard. When all goes well.',
                effects:
                  'Owner chooses 2 distinct targets. ' +
                  'The first target transfers the physical damage they have suffered in the current combat until the start of this spell\'s channeling to the second target, which is withdrawn if the damage exceeds their health, in which case the transfer is interrupted. ' +
                  'When used, if owner is not a target, they can be the second if the opponent spend 1 contestable fate point.\n\n' +
                  'This spell\'s mana cost is equal to the minimum needed for its potency to be no less than 1/5 of the damage to be transferred.'
              };
            case 'weakness':
              return {
                title: 'Weakness',
                epigraph:
                  'Afflicted by a spell that weakened his body, the warrior could not resist the poison of the arrow in his arm, which asphyxiated him in a short time.',
                effects:
                  'Target must not have the maximum number of weakened markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target: if present, loses 1 strengthened marker; otherwise, gains 1 weakened marker.'
              };
            case 'decay':
              return {
                title: 'Decay',
                epigraph:
                  'Gloriously equipped, our champion won all his battles, until the one in which a villainous channeler was in the audience. That night, his entire body necrosed, and left us with a dying man in agony.',
                effects:
                  'In post-combats, target loses a percentage of their base health, equal to 55% plus 10% per point of this spell\'s potency and minus 5% per point of target\'s stamina.\n\n' +
                  'The percentage of health to be lost is adjusted to 90% if it exceeds this value.'
              };
            case 'heartstop':
              return {
                title: 'Heartstop',
                epigraph:
                  'After my infiltration of the camp, the captain, shortly after I spotted him, was stricken with a fatal cardiac arrest. His subordinates thought it was a death of natural causes.',
                effects:
                  'Target\'s stamina must be less than 5.\n\n' +
                  'Withdraws target.'
              };
            case 'shrinkage':
              return {
                title: 'Shrinkage',
                epigraph:
                  'Some human cities use in their toils orcs, brought by expeditions to wildlands. There, it is common for punishment to be meted out to an orc while they are shrunk, to soften their reaction.',
                effects:
                  'Target must have size, which must be larger than small.\n\n' +
                  'Target and their embedded attachments with size change it to that below the current one, undergoing the related adjustments. ' +
                  'Upon [use/disuse], this spell suspends external attachments of target with size.\n\n' +
                  'This spell can persist for up to 3 turns, and its minimum mana cost increases by 1 per embedded attachment of target with size.'
              };
            case 'barkskin':
              return {
                title: 'Barkskin',
                epigraph:
                  'Gradually, her skin took on a dark brown color, and became wrinkled and dry. At the end of the transformation, it resembled the bark of a tree.',
                effects:
                  'When lower, target changes their natural resistances to: 3 against blunt; 3 against slash; 3 against pierce.\n\n' +
                  'In addition, target\'s natural conductivity becomes 0, and the total fire damage they suffer is doubled before it is inflicted.\n\n' +
                  'This spell clashes with "Ironskin" and "Stoneskin".'
              };
            case 'ironskin':
              return {
                title: 'Ironskin',
                epigraph:
                  'Gradually, her skin took on a silver-gray color, and became smooth and hard. At the end of the transformation, her body seemed to be made of iron.',
                effects:
                  'When lower, target changes their natural resistances to: 2 against blunt; 8 against slash; 5 against pierce.\n\n' +
                  'In addition, target\'s natural conductivity becomes 3, and their garments\' fire resistances are halved, rounded down.\n\n' +
                  'This spell clashes with "Barkskin" and "Stoneskin".'
              };
            case 'stoneskin':
              return {
                title: 'Stoneskin',
                epigraph:
                  'Gradually, her skin took on a chrome-gray color, and became grainy and stiff. At the end of the transformation, her body looked more like a granite sculpture.',
                effects:
                  'When lower, target changes their natural resistances to: 4 against blunt; 5 against slash; 8 against pierce; 3 against fire.\n\n' +
                  'In addition, target gains 2 penalty points on melee maneuvers, and is withdrawn if an attack inflicts on them physical damage greater than half their base health.\n\n' +
                  'This spell clashes with "Barkskin" and "Ironskin".'
              };
            case 'restoration':
              return {
                title: 'Restoration',
                epigraph:
                  'When with enough mana, an enothe is the panacea for damaged equipment.',
                effects:
                  'Target must be a mundane item withdrawn.\n\n' +
                  'Suspends target.\n\n' +
                  'This spell\'s minimum mana cost is equal to 1 plus target\'s size, while converted to: 0 for null; 1 for small; 2 for medium; 3 for large.'
              };
            case 'ruination':
              return {
                title: 'Ruination',
                epigraph:
                  'Enothes can easily ruin an object by changing its molecular structure – making it, for example, more brittle or soft.',
                effects:
                  'Target must be a mundane item in use.\n\n' +
                  'Withdraws target.\n\n' +
                  'This spell\'s minimum mana cost is equal to 1 plus target\'s size, while converted to: 0 for null; 1 for small; 2 for medium; 3 for large.'
              };
            case 'withering-slash':
              return {
                title: 'Withering Slash',
                epigraph:
                  'After the channeler handed me back the sword, I felt a sinister energy emanating from it. When I touched its edge on a log, the touched area corroded until, in a few seconds, a deep groove formed.',
                effects:
                  'Target must be able to attack with "cut".\n\n' +
                  'When target inflicts damage with "cut": the receiver is withdrawn, or – if the damage would already remove them – is ended; this spell is disused.\n\n' +
                  'The opponent can spend 1 incontestable fate point for this attack to deal critical damage instead of removing its receiver.'
              };
            case 'fierce-shot':
              return {
                title: 'Fierce Shot',
                epigraph:
                  'As it traveled through the air, the arrow gained speed. When it hit the soldier, it went through his body as if he had no armor, and only lost power hundreds of meters later.',
                effects:
                  'Target must be able to attack with "shoot".\n\n' +
                  'Target\'s next "shoot" attack: adds to its penetration points the extent of its effective range; automatically deals critical damage if its effective range is greater than R2.\n\n' +
                  'This spell is disused after target has attacked with "shoot".'
              };
            case 'sanctuary':
              return {
                title: 'Sanctuary',
                epigraph:
                  'A powerful healing energy circulated through the fort we were in. When the soldiers returned, no matter how bad the condition of the wounded was, within a short time they were all fully healed.',
                effects:
                  'Biotic beings in your field grid: in pre-combats, restore their health to the base value; lose the "weakened", "unlivened", "diseased", and "possessed" conditions, and are prevented from gaining them.\n\n' +
                  'This spell clashes with "Abiosphere" while in use by the opponent.'
              };
            case 'abiosphere':
              return {
                title: 'Abiosphere',
                epigraph:
                  'Unbeknown to the occupants, the cantonment had been enveloped in a magic sphere hostile to life. All vitality there was slowly sucked out, and those who did not leave in time were taken by illness.',
                effects:
                  'In pre-combats, biotic beings in the opponent\'s field grid gain 2 diseased markers per battle turn ended since this spell came into its current use.\n\n' +
                  'This spell clashes with "Sanctuary" while in use by the opponent.'
              };
            case 'hypergravity':
              return {
                title: 'Hypergravity',
                epigraph:
                  'I felt a strong pressure against my body, and each new step took more of my effort. All around me, some were struggling to stand, and others were falling over due to the weight of their armor.',
                effects:
                  'For everyone on the field: removes "levitated" condition, and prevents its acquisition; sets agility to 3, when above this value; doubles the weight score of items. ' +
                  'When weight counts are adjusted beyond the limit, removes embedded items and suspends external ones, under this precedence: external, embedded; heavy, medium; implement, garment, weapon.\n\n' +
                  'This spell clashes with "Hypogravity".'
              };
            case 'hypogravity':
              return {
                title: 'Hypogravity',
                epigraph:
                  'As I ran, I realized how easy it was to jump; as I jumped, I realized how easy it was to fly. Soon part of the battle was being fought above ground, over those less fortunate in weight.',
                effects:
                  'On the field, all undead and beasts become levitated, as well as humanoids whose current weight count is up to: 2, for those whose size is small; 1, for those whose size is medium; 0, for those whose size is large.\n\n' +
                  'This spell clashes with "Hypergravity".'
              };
            case 'magic-missile':
              return {
                title: 'Magic Missile',
                epigraph:
                  'The powthe concentrated, and in a short time materialized with his mana blue, ogival projectiles. Driven by his will, the projectiles flew over the battlefield until they plunged into their targets.',
                effects:
                  'Owner chooses up to 4 targets, not necessarily distinct.\n\n' +
                  'When used, shoots at each target chosen by owner 1 attack of 1 point and 3 mana damage points.\n\n' +
                  'This spell\'s mana cost is equal to the number of targets chosen by owner.'
              };
            case 'mage-armor':
              return {
                title: 'Mage Armor',
                epigraph:
                  'I was enveloped in a blue, shimmering aura. It would not protect me from a sword strike, but I knew that it would dissipate well any shot of mana that hit it.',
                effects:
                  'Target gains 4 points of natural mana resistance.'
              };
            case 'mana-disruption':
              return {
                title: 'Mana Disruption',
                epigraph:
                  'When used for warfare, the residual mana in an environment is typically gathered together into an orb of energy, driven by its channeler\'s will, and meant to explode after they cease to stabilize it.',
                effects:
                  'At the beginning of this spell\'s flow costs, you must choose 1 withdrawn spell, which is ended at the requesting flow cost\'s end.\n\n' +
                  'When used, generates 1 attack of 1 point and 4 mana damage points. Each sustaining adds 1 point to this attack.\n\n' +
                  'When disused, the attack is shot at the occupants of: owner\'s card slot, if this spell was prevented; target, otherwise.'
              };
            case 'energy-wall':
              return {
                title: 'Energy Wall',
                epigraph:
                  'As she encircled the camp, the powthe molded with her magic a wall of solid mana. It took an afternoon before this wall surrounded the place, but it protected everyone there during the night.',
                effects:
                  'Owner must be in your field grid. This spell affects all card slots on: owner\'s row, if it is A; owner\'s grid, otherwise.\n\n' +
                  'Between affected and unaffected card slots, for physical beings that are not levitated the following are disabled: magic shots; the "attack", "move", "engage", "intercept", and "disengage" actions; adjustment placements.'
              };
            case 'voltaic-outburst':
              return {
                title: 'Voltaic Outburst',
                epigraph:
                  'The wolves charged at the powthe, who stared at them calmly. As they got within a few feet of the channeler, they were suddenly electrocuted by several lightning bolts that came out of his chain mail.',
                effects:
                  'Target\'s card slot must have at least 1 other occupant, and both must have conductivity.\n\n' +
                  'When used, generates 1 attack of 6 points and 1 shock damage point. Each sustaining adds 1 point to this attack\'s damage.\n\n' +
                  'When disused, if this spell was not prevented its attack is directed to target and all in their card slot with the highest conductivity, disregarding target for determining this conductivity.'
              };
            case 'charged-aura':
              return {
                title: 'Charged Aura',
                epigraph:
                  'That warrior\'s companions knew that it was not a good idea to fight alongside him. The enemies he faced found out the reason, which shocked them.',
                effects:
                  'In melee combat breaks with target initiated by attacks whose range is less than M5, after its attack points are assigned its other participant suffers 1 attack of 1 point and shock damage equal to the combat break\'s attack points minus 2.\n\n' +
                  'If this spell\'s damage stuns its target, the combat break ends, and: if this spell\'s target has initiated it, the attack is prevented; otherwise, all attack points are inflicted.'
              };
            case 'thunder-strike':
              return {
                title: 'Thunder Strike',
                epigraph:
                  'The captain was motivating us from the podium, under a cloudless sky. Then a sudden flash blinded us, and a thunderous noise deafened us; as we recovered, we saw his charred body, lying on the ground.',
                effects:
                  'Target must be occupied by at least 1 being with conductivity.\n\n' +
                  '1 attack of 5 points and 8 shock damage points is shot at the occupant of target with the highest conductivity, or, among these, at the first one who occupied target. ' +
                  'If the conductivity of this attack\'s target is 1, their player can spend 1 incontestable fate point to negate the attack.'
              };
            case 'thunderstorm':
              return {
                title: 'Thunderstorm',
                epigraph:
                  'The enemy army was coming toward ours, when a bolt of lightning struck one of their soldiers. And another. And another. And another. When the battle started, most of their troops had already fled.',
                effects:
                  'When used, this spell reproduces the effect of "Thunder Strike", with the difference that its attack has 5 points and 6 shock damage points.\n\n' +
                  'Each sustaining repeats this reproduction, but owner can differ the target card slot.'
              };
            case 'whirlwind':
              return {
                title: 'Whirlwind',
                epigraph:
                  'Well, I made a whirlwind nearby, and led it into the center of the battle. And it ended the fight, just as you wanted; it\'s not my fault to where the winds took some of ours.',
                effects:
                  'Target\'s occupants are exhausted, and you can move each one to [engagement zones/their player\'s unoccupied card slots] that are encompassed by a range of R scope and maximum extension equal to the mana spent on this spell\'s channeling minus 1.\n\n' +
                  'Per target\'s occupant you move, the opponent can spend 1 incontestable fate point to move another, under equal restrictions.'
              };
            case 'air-cocoon':
              return {
                title: 'Air Cocoon',
                epigraph:
                  'Strong currents of air surrounded him, and enveloped his body in an almost palpable sphere. This sphere followed his steps – as if orbiting him –, and easily deflected incoming arrows.',
                effects:
                  'Prevents target\'s actions in progress that are "shoot" and "throw".\n\n' +
                  'During this spell\'s use, target cannot [attack with/be targeted by] "shoot" and "throw".'
              };
            case 'miasma':
              return {
                title: 'Miasma',
                epigraph:
                  'Some describe it as exuding a faint putrid smell, but most only realize that there is something wrong with the air they breathe when it is too late.',
                effects:
                  'In combats, biotic beings gain 1 diseased marker at the end of every 3 consecutive segments during which they are in target.'
              };
            case 'auspicious-winds':
              return {
                title: 'Auspicious Winds',
                epigraph:
                  'The wind itself marches with us toward battle, and propels our troops and arrows to victory.',
                effects:
                  'During this spell\'s use: the range of your troops\' "shoot" attacks increases by 1, up to R5; the opponent\'s troops cannot attack with "shoot" and "throw".\n\n' +
                  'This spell clashes with any spell of the same title, and disuses the opponent\'s "Miasma" cards in use as soon as their current potency is equal to or less than this spell\'s usage cost.'
              };
            case 'flaming-darts':
              return {
                title: 'Flaming Darts',
                epigraph:
                  'Distracted by the fight, the soldier could not prevent the fire from the projectile that had hit him from spreading across his flank. As he tried to put it out, his opponent fatally speared him.',
                effects:
                  'Owner chooses up to 4 targets, not necessarily distinct.\n\n' +
                  'When used, shoots at each target chosen by owner 1 attack of 1 point and 3 fire damage points.\n\n' +
                  'This spell\'s mana cost is equal to the number of targets chosen by owner.'
              };
            case 'fire-ring':
              return {
                title: 'Fire Ring',
                epigraph:
                  'The goblin thought that he would be able to run away after his sneak attack, but he had to halt after flames of magical fire erupted around him, surrounding him until his enemies\' arrival.',
                effects:
                  'If not levitated, disables from target the "retreat", "engage", "intercept", and "disengage" actions. Between target and physical beings, disables from those not levitated the “attack” action.\n\n' +
                  'Aside from ranged attacks, beings without will or with fire resistance can still commit these actions, after suffering 1 attack of 10 points and 2 fire damage points when they commit the first. Moreover, if the committer is target, this spell is disused.'
              };
            case 'fireball':
              return {
                title: 'Fireball',
                epigraph:
                  'The air ignited into a magical fire, which grew by burning oxygen, and in the form of a spinning ball. Hard to control, this ball exploded before hitting its target, but set everyone nearby on fire.',
                effects:
                  'Target must be occupied by at least 1 physical being.\n\n' +
                  '1 attack of 2 points and 5 fire damage points is shot at the physical beings in target. As attack points, the extension of this spell\'s effective range is added to the shot.\n\n' +
                  'When used, you can spend 1 contestable fate point to have this spell\'s attack change its damage points to 8, and its target to 1 physical being, which you choose at the channeling\'s start.'
              };
            case 'fiery-rain':
              return {
                title: 'Fiery Rain',
                epigraph:
                  'Clouds from hell loomed over the battlefield, and showered on it drops of fire. Everyone was engulfed in flames, the crackling of which no scream could muffle. And then there was only silence.',
                effects:
                  'When used, 1 attack of 6 points and 1 fire damage point is shot at all physical beings on the field for 5 segments.\n\n' +
                  'Each sustaining extends this spell\'s effect by 1 segment.\n\n' +
                  'This spell clashes with any spell of the same title.'
              };
            case 'earth-grip':
              return {
                title: 'Earth Grip',
                epigraph:
                  'The ogre thought he had her cornered, but as he advanced toward the powthe, his feet sank into the earth. As he tried to free them, the channeler, smiling, wished him good luck before walking away.',
                effects:
                  'Target must not be levitated.\n\n' +
                  'Prevents target\'s actions in progress that are "engage", "intercept", "disengage", "relocate", and "retreat". In addition, prevents target from committing these actions, as well as: becoming levitated; defending with "dodge"; spending their agility; making melee attacks, as long as they are not triggered by a weapon\'s range.'
              };
            case 'earth-den':
              return {
                title: 'Earth Den',
                epigraph:
                  'Beneath our feet, the enemy hides. The coward has shaped the earth to burrow into a shelter, but his magic cannot persist for much longer. At some point, he will have to come up.',
                effects:
                  'Target must: be owner; be in the rearguard.\n\n' +
                  'In battles, target: for adjustment placements, is regarded as an astral being; cannot be the direct target of [actions/effects]; is not affected by attacks; can only commit "channel" – whereby can only target spells whose direct target is null or a spell –, "sustain", "summon", "handle", "retreat", and "abort".'
              };
            case 'fissure':
              return {
                title: 'Fissure',
                epigraph:
                  'Where until recently the ground had been smooth and stiff, a crack of great proportions swallowed him up, and buried his body in the depths of the earth.',
                effects:
                  'Target must not be levitated.\n\n' +
                  'If target can, they spend 2 agility points; otherwise, they are withdrawn.\n\n' +
                  'When used, if target can spend 2 agility points but is exhausted, you can spend 1 contestable fate point to still have them withdrawn.'
              };
            case 'earthquake':
              return {
                title: 'Earthquake',
                epigraph:
                  'Suddenly an earthquake erupted on the battlefield. For a long moment, everyone was forced to stop fighting to give it their due attention, willingly or not.',
                effects:
                  'During this spell\'s use, all physical beings on the field that are not levitated become incapacitated.\n\n' +
                  'This spell clashes with any spell of the same title.'
              };
            case 'telepathy':
              return {
                title: 'Telepathy',
                epigraph:
                  'The thought of other people comes to me as a strong intuition, which uses my own thoughts to conceive its meaning.',
                effects:
                  'This spell has no effect while target\'s will is greater than owner\'s.\n\n' +
                  'During this spell\'s use, every dedicated action chosen for target to commit is declared to you, and takes 2 segments to start.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'liveliness':
              return {
                title: 'Liveliness',
                epigraph:
                  'The vivacity of spirit is reflected in a strong mental grounding, and in a willingness to perform every act with promptness.',
                effects:
                  'Target must not have the maximum number of enlivened markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target: if present, loses 1 unlivened marker; otherwise, gains 1 enlivened marker.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'bravery':
              return {
                title: 'Bravery',
                epigraph:
                  'The magic of vothes is usually the best motivational resource for making soldiers eager to fight to the death.',
                effects:
                  'Target must not have the maximum number of emboldened markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target gains 1 emboldened marker.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'awe':
              return {
                title: 'Awe',
                epigraph:
                  'Vothes can grant even the vilest of creatures a strong aura of sublimity, such that those around them become fascinated by their presence.',
                effects:
                  'Target must not have the maximum number of awed markers.\n\n' +
                  'When used and in pre-combats, target gains 2 awed markers per point of this spell\'s potency. The number of markers is adjusted synchronously with the potency.\n\n' +
                  'This spell clashes with "Fear". In addition, for spirits, channeling the spell is a partial action, with G1 range.'
              };
            case 'inspiration':
              return {
                title: 'Inspiration',
                epigraph:
                  'When someone is devoted to an activity, they find in itself all the motivation necessary to keep doing it.',
                effects:
                  'Target must be exhausted and have 1 or more moves, all of which must have been finished.\n\n' +
                  'When target\'s will equals at least 10 minus the number of this spell\'s sustainings in its current use, this spell is disused, and they become reprepared.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'unliveliness':
              return {
                title: 'Unliveliness',
                epigraph:
                  'Why are you fighting? To protect those who don\'t care about you? To defend an abstract cause, which has meaning only for those who want to give? Drop your weapons, and fight to improve your own life.',
                effects:
                  'Target must not have the maximum number of unlivened markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target: if present, loses 1 enlivened marker; otherwise, gains 1 unlivened marker.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'rage':
              return {
                title: 'Rage',
                epigraph:
                  'Love and pity do no good when your goal is to eliminate the enemy. If you are not striving for this, you are not fighting to your full potential.',
                effects:
                  'Target must not have the maximum number of enraged markers, and this spell is disused once this occurs.\n\n' +
                  'When [used/sustained], target gains 1 enraged marker.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'fear':
              return {
                title: 'Fear',
                epigraph:
                  'Wherever the old wanderer went, a sense of doom accompanied him. In his travels, he needed no other company for thugs to leave him alone.',
                effects:
                  'Target must not have the maximum number of feared markers.\n\n' +
                  'When used and in pre-combats, target gains 2 feared markers per point of this spell\'s potency. The number of markers is adjusted synchronously with the potency.\n\n' +
                  'This spell clashes with "Awe". In addition, for spirits, channeling the spell is a partial action, with G1 range.'
              };
            case 'submission':
              return {
                title: 'Submission',
                epigraph:
                  'The majority, poorly capable of thinking for themselves, lead a life subservient to the ideas and desires of others. Let us thus put these people to good use by directing them to the right cause.',
                effects:
                  'Target must not be exhausted, and must have 1 or more moves.\n\n' +
                  'Prevents target\'s actions, and occupies them. When target\'s will equals at most 1 plus the number of this spell\'s sustainings in its current use, this spell is disused, and they become possessed until the end of your third parity after this spell\'s disuse.\n\n' +
                  'For spirits, channeling this spell is a partial action, with G1 range.'
              };
            case 'astral-projection':
              return {
                title: 'Astral Projection',
                epigraph:
                  'The material plane is only one of many in the universe. The others, collectively known as the astral, contain realities little understood, but accessible by consciousnesses outside a physical body.',
                effects:
                  'Target is always owner, who must not: be a spirit; be in an engagement zone.\n\n' +
                  'Owner becomes projected.'
              };
            case 'akashic-borrowing':
              return {
                title: 'Akashic Borrowing',
                epigraph:
                  'It is said that, deep in the astral, a library stores the history of every particle in the universe. With each visit, one can see a bit of the past and future, and borrow some of the experiences.',
                effects:
                  'Owner must be a spirit.\n\n' +
                  'Owner starts the channeling of any spell in the game – chosen by you – that they meet the requirements for.'
              };
            case 'cosmoenergy':
              return {
                title: 'Cosmoenergy',
                epigraph:
                  'Those who align themselves with the vibrations of the universe are always satiated by its energy.',
                effects:
                  'Target is always owner, who must: be a spirit; have positive polarity in Voth.\n\n' +
                  'When [used/sustained], target restores 2 energy points.\n\n' +
                  'This spell is a partial action, and is disused after restoring in total a quantity of energy equal to half owner\'s base mana in Voth.'
              };
            case 'body-takeover':
              return {
                title: 'Body Takeover',
                epigraph:
                  'Each physical body is a vehicle for a consciousness to interact with the material plane. Those with no scruples to throw away consciousnesses discouraged to fight for theirs dispose of many to drive.',
                effects:
                  'Owner must be able to commit "possess" targeting target.\n\n' +
                  'Until disused, suspends owner and target. Creates for you a copy of target, in owner\'s former card slot and with only embedded possessions. If it is removed, so is target, and this spell is disused.\n\n' +
                  'This spell can persist for up to 3 turns, and its minimum mana cost is equal to target\'s stamina.'
              };
            case 'nature-call':
              return {
                title: 'Nature\'s Call',
                epigraph:
                  'The sanctuary was enchanted to be guarded by wolves from its forest. Those who make a pilgrimage there find a settled pack, which, however, is only hostile to thieves, vandals, hunters, and the like.',
                effects:
                  'In pre-combats, you choose 1 of 3 options: 1 bear, 3 wolves, or 1 swarm. This spell summons the chosen option to an engagement zone also chosen by you.'
              };
            case 'celestial-appeal':
              return {
                title: 'Celestial Appeal',
                epigraph:
                  'Many pray for celestial help, but the answer comes only to those whose cause is pure, and whose magical power is remarkable.',
                effects:
                  'Owner must have positive polarity in Voth, and you must not have used a negative spell.\n\n' +
                  'In redeployments, your channelers with at least 3 positive levels in Voth and no active angel gain the "angel" option in the "summon" action.\n\n' +
                  'This spell is disused after you use a negative spell.'
              };
            case 'abyssal-evocation':
              return {
                title: 'Abyssal Evocation',
                epigraph:
                  'After being summoned via dark rituals, demons tend to serve their summoner willingly, for they know that this subordination will continue in the afterlife, but with them having power to reverse it.',
                effects:
                  'Owner must have negative polarity in Voth.\n\n' +
                  'In redeployments, your channelers with at least 2 negative levels in Voth gain the "demon" option in the "summon" action. This option is disabled for a channeler while they have 1 active demon per 2 levels in Voth.'
              };
            case 'macabre-communion':
              return {
                title: 'Macabre Communion',
                epigraph:
                  'Those who control both matter and mind have little difficulty in enlisting the service of the dead. The ones who pave battlefields are usually a cheap and valued labor force.',
                effects:
                  'Owner must have at least 1 level in Enoth.\n\n' +
                  'In pre-combats, per withdrawn humanoid of yours, this spell summons 1 undead token times half owner\'s level in Enoth, which you then distribute into engagement zones chosen by you.'
              };
            case 'pyrotic-animation':
              return {
                title: 'Pyrotic Animation',
                epigraph:
                  'Fire is a reckless destructive energy. However, vothes can grant it some volition, in order to guide it on what to destroy.',
                effects:
                  'Owner must have at least 1 level in Powth.\n\n' +
                  'In redeployments, your powthe channelers with at least 2 levels in Voth gain the "golden salamander" option in the "summon" action. In addition, those with also negative polarity in Voth gain the "indigo salamander" option. ' +
                  'This spell\'s options are disabled for a channeler while they have 1 active salamander per 2 levels in Voth.'
              };
            case 'the-champion':
              return {
                title: 'The Champion',
                epigraph:
                  'A crown was given to him, and he went out conquering and to conquer.',
                effects:
                  'Target must be a creature of yours.\n\n' +
                  'Target gains an infinite number of fate points. However, you lose the round if they are removed.\n\n' +
                  'This spell\'s mana cost increases by 2 per battle turn ended, until it equals 8.'
              };
            case 'the-seeker':
              return {
                title: 'The Seeker',
                epigraph:
                  'Fate eventually favors those who seek their aspirations.',
                effects:
                  'In pre-combats, if target is active and without an aspiration, they choose 1 enemy being active to be, until this being\'s inactivation.\n\n' +
                  'When applicable to you, target\'s acts against the aspiration or their possessions use fate points automatically and for free.\n\n' +
                  'This spell\'s mana cost decreases by 4 per fate point target\'s player spends via them, until it equals 2.'
              };
            case 'the-prey':
              return {
                title: 'The Prey',
                epigraph:
                  'Ah, you have such a smell of fear...',
                effects:
                  'The opponent cannot spend fate points for your acts against target or their possessions.\n\n' +
                  'This spell\'s mana cost decreases by 4 per fate point target\'s player spends via them, until it equals 2.'
              };
            case 'the-ruler':
              return {
                title: 'The Ruler',
                epigraph:
                  'First they got the deeds of a leader, then they gave the authority to lead.',
                effects:
                  'Target must be a creature of yours.\n\n' +
                  'While target is active, your commander: does not defeat you if inactive; is regarded as a creature by the "retreat" action. While your commander is inactive, target cannot be suspended, except via the spell "The Truce".\n\n' +
                  'This spell\'s mana cost decreases by 2 per ended combat in which target was always active, until it equals 2.'
              };
            case 'the-hoard':
              return {
                title: 'The Hoard',
                epigraph:
                  'There is no excess of wealth for those who can get wealthier.',
                effects:
                  'Converts your current and future intransitive fate points to transitive.\n\n' +
                  'This spell\'s mana cost decreases by 2 per current fate point of yours, until it equals 4.'
              };
            case 'the-idol':
              return {
                title: 'The Idol',
                epigraph:
                  'Anyone who does not see a creator directly finds them in their works.',
                effects:
                  'In pre-combats, you gain 1 intransitive fate point per active relic of yours.\n\n' +
                  'This spell\'s mana cost decreases by 4 per ended combat in which at least 1 of your relics was always active, until it equals 4.'
              };
            case 'the-host':
              return {
                title: 'The Host',
                epigraph:
                  'The sacrifice of those below is often what raises those above.',
                effects:
                  'You gain 1 transitive fate point as soon as a humanoid of yours is withdrawn, also retroactively.\n\n' +
                  'This spell\'s mana cost decreases by 2 per humanoid of yours that is withdrawn, until it equals 4.'
              };
            case 'the-spring':
              return {
                title: 'The Spring',
                epigraph:
                  'Whoever sows in the celestial fields reaps the fruits of bliss.',
                effects:
                  'In pre-combats, you gain 2 intransitive fate points.\n\n' +
                  'This spell clashes with "The Winter" while in use by the opponent.\n\n' +
                  'This spell\'s mana cost decreases by 3 per fate point you spend in the current combat, until it equals 3.'
              };
            case 'the-winter':
              return {
                title: 'The Winter',
                epigraph:
                  'Those who do not make provisions for the arrival of winter perish from cold and hunger.',
                effects:
                  'The opponent no longer gains fate points.\n\n' +
                  'This spell clashes with "The Spring" while in use by the opponent.\n\n' +
                  'This spell\'s mana cost decreases by 3 per fate point the opponent spends in the current combat, until it equals 3.'
              };
            case 'the-chalice':
              return {
                title: 'The Chalice',
                epigraph:
                  'Magi who drink from the chalice of faothes are blessed when casting their finest spells.',
                effects:
                  'In combats, you gain 1 intransitive fate point per 2 mana points you spend on a [channeling/sustaining].\n\n' +
                  'This spell\'s mana cost decreases by 1 per 2 mana points that any player spends on a [channeling/sustaining] in combats, until it equals 3.\n\n' +
                  'When determining spent mana points, this spell includes energy and modifier spell costs, and excludes polarity burdens.'
              };
            case 'the-genie':
              return {
                title: 'The Genie',
                epigraph:
                  'Why limit your desires when you can desire more desires.',
                effects:
                  'Once per pre-combat, you can spend 1 incontestable fate point to suspend 1 of your [active/withdrawn] spells.\n\n' +
                  'This spell\'s mana cost decreases by 3 per channeling finished by you in combats, until it equals 3.'
              };
            case 'the-daeva':
              return {
                title: 'The Daeva',
                epigraph:
                  'Mana corrupts just as well as gold.',
                effects:
                  'In combats, the opponent spends 1 incontestable fate point at the end of the flow cost to channel any of their spells; if they cannot, the channeling is prevented.\n\n' +
                  'This spell\'s mana cost decreases by 3 per channeling finished by the opponent in combats, until it equals 3.'
              };
            case 'the-feast':
              return {
                title: 'The Feast',
                epigraph:
                  'Fortune smiles upon us, and fills us with plenty. Let us enjoy all my wine, because today it will not end.',
                effects:
                  'In fate breaks contested by the opponent, you can spend 1 additional fate point to negate the contestation. ' +
                  'However, if the opponent has a spell of the same title in use, they can then recontest the fate break, via another fate point.\n\n' +
                  'This spell\'s mana cost decreases by 2 per contestable fate break of yours resolved in your favor, until it equals 2.'
              };
            case 'the-beast':
              return {
                title: 'The Beast',
                epigraph:
                  'There is a beast lurking in the dark. Nobody ever wants to meet it, but optimism is useless: the beast is always hungry.',
                effects:
                  'Once per post-combat, you can spend 1 fate point to withdraw 1 active or suspended biotic creature, chosen by you. ' +
                  'This fate point is incontestable if the target is a beast, and contestable if they are a humanoid.\n\n' +
                  'This spell\'s mana cost decreases by 2 per humanoid of the opponent that is removed in the battle phase, until it equals 2.'
              };
            case 'the-gate':
              return {
                title: 'The Gate',
                epigraph:
                  'Everyone needs fate\'s approval to pass through each gate in life.',
                effects:
                  'In combats and redeployments, the opponent must spend 1 contestable fate point as a cost to commit "deploy" and "retreat". If you contest them, for the remainder of the current period the target action is disabled for the being that would commit it.\n\n' +
                  'This spell\'s mana cost decreases by 2 per action "deploy" and "retreat" finished by the opponent in combats and redeployments, until it equals 4.'
              };
            case 'the-castle':
              return {
                title: 'The Castle',
                epigraph:
                  'Let into your castle the righteous you meet, for empty it will not protect you from the vile.',
                effects:
                  'Once per defensive combat break of yours, you can spend 1 incontestable fate point to have the defender gain 3 defense bonus points.\n\n' +
                  'This spell\'s mana cost decreases by 2 per combat break initiated by the opponent, until it equals 4.'
              };
            case 'the-prison':
              return {
                title: 'The Prison',
                epigraph:
                  'Every soldier is a prisoner, moving through cells while serving their war sentence.',
                effects:
                  'In combats and redeployments, the opponent must spend 1 contestable fate point as a cost to commit "relocate". If you contest them, for the remainder of the current period this action is disabled for the being that would commit it.\n\n' +
                  'This spell\'s mana cost decreases by 2 per action "relocate" finished by the opponent in combats and redeployments, until it equals 4.'
              };
            case 'the-plot':
              return {
                title: 'The Plot',
                epigraph:
                  'Fictional characters follow the events of a narrator; real characters follow those of a channeler.',
                effects:
                  'When greater, this spell\'s potency replaces the potency of your current and future permanent spells in use.\n\n' +
                  'This spell\'s minimum mana cost decreases by 5 per permanent spell currently in use by you, until it equals 5.'
              };
            case 'the-vision':
              return {
                title: 'The Vision',
                epigraph:
                  'At times, some are abducted by an imagination so real that one wonders if the abductor was the time.',
                effects:
                  'Regresses 5 segments from the current combat, or, lacking that quantity, back to its beginning.\n\n' +
                  'In the new resolution of the regressed segments: the effects from "Alertness Potion" are suppressed; this spell\'s use is disabled.\n\n' +
                  'Once used, this spell is ended.'
              };
            case 'the-truce':
              return {
                title: 'The Truce',
                epigraph:
                  'In peace we find the opportunity to prepare new war operations.',
                effects:
                  'In the post-combat of the turn of use, ends the turn and: suspends troops; removes tokens, and mana in card slots; resets the stats and attachments of suspended cards to their initial state. ' +
                  'Then, in the following order starts: a deployment; an allocation; the next battle turn.\n\n' +
                  'This spell\'s mana cost decreases by 2 per troop you have more than the opponent, until it equals 2. In addition, this spell can only be used once per battle, on turns 3 or 4.'
              };
            case 'the-lure':
              return {
                title: 'The Lure',
                epigraph:
                  'Those who rush into their onslaught are the most likely to stumble toward the enemy.',
                effects:
                  'In the post-combat of the turn of use, suspends your humanoid creatures in engagement zones not controlled by you, when possible. ' +
                  'In the redeployment of the turn of use: you gain 1 move per being suspended in that way; your "deploy" actions can target occupied engagement zones.\n\n' +
                  'This spell\'s mana cost decreases by 2 per troop the opponent has more than you, until it equals 2. In addition, this spell can only be used by you once per battle.'
              };
            case 'the-end':
              return {
                title: 'The End',
                epigraph:
                  'When war prayers are made by a faothe, victory comes by means that fate is coerced to bring.',
                effects:
                  'Ends the round, and sets its winner as the player with the most fate points at the time of the ending.\n\n' +
                  'If both players have the same number of fate points, a draw is computed.\n\n' +
                  'This spell\'s mana cost decreases by 4 per battle turn started.'
              };
          }
        } )() );

        // Define designação completa da carta
        cardData.fullDesignation = cardData.title;
      }
    }

    // Retorna dados de localização da característica passada
    cards.getTrait = function ( traitName ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( typeof traitName == 'string' );

      // Identifica característica alvo
      switch( traitName ) {
        case 'racial-mix':
          return {
            title: 'Racial Mix',
            description:
              'In a main deck, the limit of commandable humans, if above 0, increases by 1 for each non-human race whose number of creatures in the deck exceeds the minimum by 1, 3, and 5.'
          };
        case 'adaptive-tactics':
          return {
            title: 'Adaptive Tactics',
            description:
              'By parity of redeployments, the first 2 dedicated actions committed via humans are converted to free.'
          };
        case 'raging-surge':
          return {
            title: 'Raging Surge',
            description:
              'In combats, when suffering at least 10 damage points at once, orcs, in this order, gain: for every 2 extra damage points, 1 emboldened marker or, if the opponent spends 1 contestable fate point, 1 enraged marker; 1 free attack, against a target chosen by their player.'
          };
        case 'powerful-shots':
          return {
            title: 'Powerful Shots',
            description:
              'The penetration and range of "shoot" attacks from bows in use by orcs increase by 1, up to, respectively, 10 and R5.'
          };
        case 'arcane-mastery':
          return {
            title: 'Arcane Mastery',
            description:
              'Each channeler elf grants their deck credits, separated by path and equal to the elf\'s level in the path times 6. These credits are spent on the deck\'s spells of their path, before coins.'
          };
        case 'dexterous-wielding':
          return {
            title: 'Dexterous Wielding',
            description:
              'Elves suffer no wielding penalties.'
          };
        case 'ingenious-craftsmanship':
          return {
            title: 'Ingenious Craftsmanship',
            description:
              'Each dwarf grants their deck 15 credit points times their experience level. These credits are spent on the deck\'s items, before coins.'
          };
        case 'antimagic-essence':
          return {
            title: 'Antimagic Essence',
            description:
              'Dwarves cannot be the direct target of [Enoth/Voth] spells whose predicted potency is less than 5.'
          };
        case 'restless-scouting':
          return {
            title: 'Restless Scouting',
            description:
              'In deployments, each active halfling gains the "relocate" action, as a single action.'
          };
        case 'blessed-fate':
          return {
            title: 'Blessed Fate',
            description:
              'Each halfling has 1 fate point.'
          };
        case 'shared-magic':
          return {
            title: 'Shared Magic',
            description:
              'Spells activated by [gnomes/gnome spirits] are suspended once: they are disused; their channeling is interrupted.'
          };
        case 'inbred-voth':
          return {
            title: 'Inbred Voth',
            description:
              'At the beginning of pre-combats, each non-removed gnome restores 2 Voth mana points.'
          };
        case 'timely-ambush':
          return {
            title: 'Timely Ambush',
            description:
              'In combats, each suspended goblin: generates 1 move; gains the "deploy" action, as a partial action and targeting an occupied engagement zone.'
          };
        case 'skittering-retreat':
          return {
            title: 'Skittering Retreat',
            description:
              'In combats, each goblin creature in their player\'s control zone gains the "retreat" action, while having a flow cost equal to 2.'
          };
        case 'slow-pace':
          return {
            title: 'Slow Pace',
            description:
              'The flow cost of ogres\' "engage" and "disengage" actions is equal to 4.'
          };
        case 'stubborn-tactics':
          return {
            title: 'Stubborn Tactics',
            description:
              'In redeployments, ogres can only commit actions on turns whose number\'s parity equals their player\'s.'
          };
      }
    }
  }
} )();
