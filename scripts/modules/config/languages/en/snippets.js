// Módulos
import * as m from '../../../../modules.js';

( function () {
  // Apenas executa módulo caso sua língua seja a do jogo
  if( m.languages.current != 'en' ) return;

  // Identificadores
  const snippets = m.languages.snippets = {};

  // Propriedades iniciais
  defineProperties: {
    // Baralhos pré-montados
    snippets.getPrebuiltDeck = function ( deckName ) {
      // Identifica baralho a ser retornado a partir do nome passado
      switch( deckName.replace( /-main-deck$/, '' ) ) {
        case 'starter-deck-1':
          return 'Starter Deck 1';
        case 'starter-deck-2':
          return 'Starter Deck 2';
        default:
          return '';
      }
    }

    // Ações
    snippets.getUserInterfaceAction = function ( actionName ) {
      // Identifica ação a ser retornada a partir do nome passado
      switch( actionName ) {
        case 'acknowledge':
          return 'OK';
        case 'agree':
          return 'Yes';
        case 'disagree':
          return 'No';
        case 'submit':
          return 'Submit';
        case 'save':
          return 'Save';
        case 'edit':
          return 'Edit';
        case 'copy':
          return 'Copy';
        case 'rename':
          return 'Rename';
        case 'remove':
          return 'Remove';
        case 'cancel':
          return 'Cancel';
        case 'back':
          return 'Back';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Ação atual que ente está acionando
    snippets.getBeingCurrentAction = function ( action ) {
      // Caso não tenha sido passada uma ação, retorna um texto vazio
      if( !action ) return '';

      // Captura adendo sobre previsão de [conclusão/sustentação] da ação alvo
      var finishmentText = getFinishmentText();

      // Identifica texto a ser retornado em função do nome da ação passada
      switch( action.name ) {
        case 'action-attack':
          return `Attacking ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        case 'action-engage':
          return `Engaging in ${ action.target.name }` + finishmentText;
        case 'action-disengage':
          return `Disengaging to ${ action.target.name.replace( /^.+-/, '' ) }` + finishmentText;
        case 'action-retreat':
          return 'Retreating' + finishmentText;
        case 'action-channel': {
          // Identificadores
          let [ spell, spellTarget ] = [ action.target, action.target.target ],
              targetSnippet = snippets.getSpellTargetSnippet( spellTarget, action.committer ).trim();

          // Para caso exista um trecho sobre o alvo
          if( targetSnippet ) {
            // Adiciona espaço ao trecho do alvo
            targetSnippet = ' ' + targetSnippet;

            // Caso alvo não seja o acionante e esteja em uma casa, adiciona essa informação
            if( spellTarget != action.committer && spellTarget.slot ) targetSnippet += ` ${ snippets.getCardPosition( spellTarget ) }`;
          }

          // Retorna texto
          return `Channeling ${ spell.content.designation.title }` + targetSnippet + finishmentText;
        }
        case 'action-sustain': {
          // Identificadores
          let [ spell, spellTarget ] = [ action.target, action.target.target ],
              targetSnippet = snippets.getSpellTargetSnippet( spellTarget, action.committer ).trim();

          // Para caso exista um trecho sobre o alvo
          if( targetSnippet ) {
            // Adiciona espaço ao trecho do alvo
            targetSnippet = ' ' + targetSnippet;

            // Caso alvo não seja o acionante e esteja em uma casa, adiciona essa informação
            if( spellTarget != action.committer && spellTarget.slot ) targetSnippet += ` ${ snippets.getCardPosition( spellTarget ) }`;
          }

          // Retorna texto
          return `Sustaining ${ spell.content.designation.title }` + targetSnippet + finishmentText;
        }
        case 'action-expel':
          return `Expelling ${ snippets.getCardPosition( action.committer ) }` + finishmentText;
        case 'action-leech':
          return `Leeching ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        case 'action-possess':
          return `Possessing ${ action.target.content.designation.full } ${ snippets.getCardPosition( action.target ) }` + finishmentText;
        default:
          return '';
      }

      // Retorna texto sobre previsão de [conclusão/sustentação] da ação alvo
      function getFinishmentText() {
        // Identificadores
        var actionPlayer = action.committer.getRelationships().controller,
            matchFlow = m.GameMatch.current.flow,
            currentParityIndex = matchFlow.segment.children.indexOf( matchFlow.parity ),
            parityModifier = 0,
            isToSustain = [ 'action-sustain', 'action-expel', 'action-leech' ].includes( action.name );

        // Caso paridade do acionante ainda esteja por vir no segmento atual, incrementa modificador de paridade
        if( currentParityIndex < matchFlow.segment.children.findIndex( parityStage => actionPlayer.parity == parityStage.parity ) ) parityModifier++;

        // Captura segmento de [conclusão/sustentação] da ação alvo
        let targetSegment = matchFlow.period.getStageFrom( matchFlow.segment, action.flowCost - action.committer.actions.spentFlowCost - parityModifier );

        // Captura paridade de [conclusão/sustentação] da ação alvo
        let targetParity = targetSegment?.getChild( `${ actionPlayer.parity }-parity` ) ?? null;

        // Retorna texto sobre previsão de [conclusão/sustentação] da ação
        return targetSegment ?
          `\n${ isToSustain ? 'Will sustain' : 'Will finish' } at ${ targetParity.displayName.toLowerCase() } of ${ targetSegment.displayName.toLowerCase() }` :
          `\nNo ${ isToSustain ? 'sustaining' : 'conclusion' } foreseen`;
      }
    }

    // Tipo de ataque livre
    snippets.getFreeAttack = function ( freeAttackName ) {
      // Identifica texto a ser retornado em função do nome do ataque livre passado
      switch( freeAttackName.slice( 0, freeAttackName.search( /-free-attack-/ ) ) ) {
        case 'melee-range':
          return 'Free attack triggered by melee range.';
        case 'rapture':
          return `Free attack triggered by the "${ m.languages.keywords.getManeuver( 'rapture' ).toLowerCase() }" maneuver.`;
        case 'engage-action':
          return `Free attack triggered by the "${ m.languages.keywords.getAction( 'engage' ).toLowerCase() }" action.`;
        case 'raging-surge':
          return `Free attack triggered by the "${ m.languages.cards.getTrait( 'raging-surge' ).title }" trait.`;
        case 'weapon-pike':
          return 'Free attack triggered by a pike\'s effect.';
        default:
          return 'Free attack.';
      }
    }

    // Tipo de parada de destino
    snippets.getFateBreak = function ( fateBreakName ) {
      // Identifica texto a ser retornado em função do nome do ataque livre passado
      switch( fateBreakName.slice( 0, fateBreakName.search( /-fate-break-/ ) ) ) {
        case 'prevent-actions':
          return 'Fate break regarding the interruption of actions due to the damage inflicted.';
        case 'raging-surge':
          return `Fate break regarding the "${ m.languages.cards.getTrait( 'raging-surge' ).title }" trait.`;
        case 'action-possess':
          return `Fate break regarding the "${ m.languages.keywords.getAction( 'possess' ).toLowerCase() }" action.`;
        case 'nimbleness-potion':
          return 'Fate break regarding the "Nimbleness Potion" implement.';
        case 'spell-poke':
          return 'Fate break regarding the "Poke" spell.';
        case 'spell-tear':
          return 'Fate break regarding the "Tear" spell.';
        case 'spell-suppress':
          return 'Fate break regarding the "Suppress" spell.';
        case 'spell-consume':
          return 'Fate break regarding the "Consume" spell.';
        case 'spell-pain-twist':
          return 'Fate break regarding the "Pain Twist" spell.';
        case 'spell-thunder-strike':
          return 'Fate break regarding the "Thunder Strike" spell.';
        case 'spell-fireball':
          return 'Fate break regarding the "Fireball" spell.';
        case 'spell-fissure':
          return 'Fate break regarding the "Fissure" spell.';
        case 'spell-the-beast':
          return 'Fate break regarding the "The Beast" spell.';
        case 'condition-diseased':
          return `Fate break regarding the "${ m.languages.keywords.getCondition( 'diseased' ).toLowerCase() }" condition.`;
        default:
          return 'Fate break.';
      }
    }

    // Origem de pontos de destino embutidos
    snippets.getEmbeddedFatePointsSource = function ( sourceName ) {
      // Identifica texto a ser retornado em função do nome da origem do ponto de destino
      switch( sourceName ) {
        case 'blessed-fate':
          return '"Blessed Fate"';
        case 'ring-of-protection':
          return '"Ring of Protection"';
        case 'spell-the-champion':
          return '"The Champion"';
        default:
          return m.languages.defaultFallback;
      }
    }

    // Trecho para alvos de magias
    snippets.getSpellTargetSnippet = function ( target, committer ) {
      // Retorna trecho tendo como referência identidade do alvo passado
      return target == committer ? ' on themself' :
             target instanceof m.Item ? ` on ${ target.content.designation.title }` :
             target instanceof m.Card ? ` on ${ target.content.designation.full }` :
             target instanceof m.CardSlot ? ` on ${ target.name }` : '';
    }

    // Nome de uma jogada
    snippets.getMoveName = function ( moveId, source, cardId ) {
      // Identificadores
      var text = m.languages.keywords.getFlow( 'move' );

      // Acrescenta ao nome da jogada seu número em relação a outras jogadas da mesma fonte
      if( moveId != 1 ) text += ' ' + moveId;

      // Adiciona ente a que jogada se vincula
      text += ` of ${ source.content.designation.full }`;

      // Adiciona ao nome da jogada número do ente em relação a outros entes do mesmo jogador
      if( cardId != 1 ) text += ' ' + cardId;

      // Retorna texto
      return text;
    }

    // Tipo de Dano
    snippets.getDamageType = ( damageType ) => `${ m.languages.keywords.getDamage( damageType ) } Damage`;

    // Tipo de Resistência
    snippets.getResistanceType = ( resistanceType ) => `${ m.languages.keywords.getDamage( resistanceType ) } Resistance`;

    // Algo em
    snippets.getSomethingIn = ( location, thing = '' ) => thing ? `${ thing } in ${ location }` : `In ${ location }`;

    // Algo de
    snippets.getSomethingOf = ( source, thing = '' ) => thing ? `${ thing } of ${ source }` : `Of ${ source }`;

    // Algo de
    snippets.getSomethingFrom = ( source, thing = '' ) => thing ? `${ thing } from ${ source }` : `From ${ source }`;

    // Casa em que carta está
    snippets.getCardPosition = function ( card, slot, isShort = false ) {
      // Identificadores
      var positionText = '',
          targetSlot = slot ?? card.slot ?? card.summoner?.slot,
          slotName = targetSlot?.name ?? '';

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

      // Caso carta não esteja em nenhuma casa, encerra função
      if( !slotName ) return positionText;

      // Caso nome abreviado da casa deva ser retornado, converte para ele seu nome completo
      if( isShort ) slotName = slotName.replace( /^.+-/g, '' );

      // Adiciona ao texto a ser retornado casa em que carta está
      positionText += `In ${ slotName }`;

      // Caso casa seja uma zona de engajamento, adiciona em que lado carta está
      if( targetSlot instanceof m.EngagementZone )
        positionText += `; ${ m.languages.keywords.getParity( card.getRelationships().owner.parity ).toLowerCase() } side`;

      // Coloca entre parênteses conteúdo a ser retornado
      positionText = positionText.replace( /(.*)/, ' ($1)' );

      // Retorna texto com localização da carta
      return positionText;
    }

    // Retorna controlador do ente passado
    snippets.getController = function ( being ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( m.GameMatch.current );
      }

      // Identificadores
      var playerUser = m.GamePlayer.current?.user,
          beingController = being.getRelationships().controller,
          text = 'Controlled by ';

      // Acrescenta ao texto informação sobre controlador do ente
      text += m.GameMatch.current.isSimulation || playerUser != beingController.user ?
        `the ${ m.languages.keywords.getParity( beingController.parity ).toLowerCase() }` : 'you';

      // Retorna texto
      return text;
    }

    // Versão
    snippets.version = 'Version';

    // Ordem
    snippets.order = 'Order';

    // Mínimo
    snippets.min = 'Minimum';

    // Máximo
    snippets.max = 'Maximum';

    // Base
    snippets.base = 'Base';

    // Fonte
    snippets.source = 'Source';

    // Embutido
    snippets.embedded = 'Embedded';

    // Dinâmico
    snippets.dynamic = 'Dynamic';

    // Moedas
    snippets.coins = 'Coins';

    // Custo de fluxo gasto
    snippets.spentFlowCost = 'Spent flow cost';

    // Indicação de se ente está adiantável
    snippets.isForwardable = 'Is forwardable';

    // Magia escolhida
    snippets.chosenSpell = 'Chosen spell';

    // Usuário global
    snippets.globalUser = 'Guest';
  }
} )();
