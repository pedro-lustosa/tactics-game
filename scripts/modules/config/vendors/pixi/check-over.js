// Módulos
import * as m from '../../../../modules.js';

// Adição de propriedades de identificação de sobreposição de elementos a 'DisplayObject.prototype'
Object.defineProperties( PIXI.DisplayObject.prototype, {
  // Verifica se cursor está sobre o elemento alvo
  oCheckCursorOver: {
    value: function () {
      // Identificadores
      var mousePositions = m.app.pixi.renderer.plugins.interaction.mouse.global,
          elementBounds = this.getBounds();

      // Identifica se cursor está sobre o elemento alvo
      return ( mousePositions.x >= elementBounds.x && mousePositions.x <= elementBounds.x + elementBounds.width ) &&
             ( mousePositions.y >= elementBounds.y && mousePositions.y <= elementBounds.y + elementBounds.height );
    }
  },
  // Verifica se elemento passado está sobre o elemento alvo
  oCheckElementOver: {
    value: function ( element ) {
      // Validação
      if( !( element instanceof PIXI.DisplayObject ) ) throw new Error( 'Argument "element" must be an instance of "PIXI.DisplayObject".' );

      // Identificadores
      var ownElementBounds = this.getBounds(),
          targetElementBounds = element.getBounds();

      // Identifica se elementos estão compartilhando o mesmo espaço
      return ( targetElementBounds.x >= ownElementBounds.x && targetElementBounds.x <= ownElementBounds.x + ownElementBounds.width ) &&
             ( targetElementBounds.y >= ownElementBounds.y && targetElementBounds.y <= ownElementBounds.y + ownElementBounds.height );
    }
  }
} );
