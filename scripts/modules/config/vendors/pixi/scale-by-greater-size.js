// Módulos
import * as m from '../../../../modules.js';

// Adição de método 'oScaleByGreaterSize' a 'DisplayObject.prototype'
Object.defineProperty( PIXI.DisplayObject.prototype, 'oScaleByGreaterSize', {
  // Proporcionalmente redimensiona objeto para uma medida fixa, ao passar o valor para o qual a maior medida se redimensionará
  value: function ( value ) {
    // Identificadores
    var largestSize = this.width >= this.height ? 'width' : 'height',
        smallestSize = largestSize == 'width' ? 'height' : 'width';

    // Validação
    if( typeof value != 'number' ) throw new Error( 'Argument "value" must be a number.' );

    // Altera menor medida do objeto para valor proporcional ao novo da maior medida
    this[ smallestSize ] *= value / this[ largestSize ];

    // Altera maior medida do objeto para valor passado
    this[ largestSize ] = value;
  }
} );
