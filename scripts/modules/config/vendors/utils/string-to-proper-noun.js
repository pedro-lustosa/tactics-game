// Adição de método 'oToProperNoun' a 'String.prototype'
Object.defineProperty( String.prototype, 'oToProperNoun', {
  // Converte uma string em um nome próprio
  value: function () {
    // Identificadores
    var newString = this.toLowerCase().replace( /-/g, ' ' ).replace( /\s+/g, ' ' ).trim(),
        matchExecution = newString.matchAll( /^.|(?<= )./g );

    // Itera por instâncias de hífenes no texto alvo
    for( let currentExecution = matchExecution.next(); !currentExecution.done; currentExecution = matchExecution.next() ) {
      // Identificadores
      let currentMatch = currentExecution.value;

      // Capitaliza caractere alvo
      newString = newString.slice( 0, currentMatch.index ) + newString[ currentMatch.index ].toUpperCase() + newString.slice( currentMatch.index + 1 );
    }

    // Retorna texto convertido
    return newString;
  }
} );
