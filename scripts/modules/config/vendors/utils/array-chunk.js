// Adição de método 'oChunk' a 'Array.prototype'
Object.defineProperty( Array.prototype, 'oChunk', {
  // Segmenta arranjo em sub-arranjos, delimitados em função do retorno de 'action'
  value: function ( action, itToTrim = false ) {
    // Identificadores
    var [ chunkedArray, startIndex ] = [ [], 0 ];

    // Validação
    if( typeof action != 'function' ) throw new Error( 'Argument "action" must be a function.' );

    // Itera por arranjo que chamou a função
    this.forEach( function ( value, index, array ) {
      // Caso índice não seja o primeiro e caso função passada retorne um valor verdadeiro
      if( index && action( value, index, array ) ) {
        // Cria um novo sub-arranjo, composto pelos elementos do arranjo original entre o fim do último sub-arranjo criado e 'index'
        chunkedArray.push( array.slice( startIndex, index ) );

        // Atualiza índice de início do próximo sub-arranjo a ser criado
        startIndex = index;
      }
    } );

    // Adiciona ao arranjo de sub-arranjos os últimos componentes do arranjo original
    chunkedArray.push( this.slice( startIndex ) );

    // Se aplicável, remove do arranjo de sub-arranjos primeiro arranjo, caso seu primeiro componente não seja validado pela função passada
    if( itToTrim && !action( chunkedArray[0][0] ) ) chunkedArray.shift();

    // Retorna arranjo de sub-arranjos
    return chunkedArray;
  }
} );
