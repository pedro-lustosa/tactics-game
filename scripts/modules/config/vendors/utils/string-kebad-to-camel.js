// Adição de método 'oKebadToCamel' a 'String.prototype'
Object.defineProperty( String.prototype, 'oKebadToCamel', {
  // Converte uma string sob o caso 'kebad' para que use o caso 'camel'
  value: function () {
    // Identificadores
    var newString = this.trim().toLowerCase().replace( /^-+/, '' ).replace( /-+$/, '' ).replace( /(?<=-)-/g, '' ),
        matchExecution = newString.matchAll( /-/g );

    // Itera por instâncias de hífenes no texto alvo
    for( let currentExecution = matchExecution.next(); !currentExecution.done; currentExecution = matchExecution.next() ) {
      // Identificadores
      let currentMatch = currentExecution.value;

      // Capitaliza caractere após hífen atual
      newString = newString.slice( 0, currentMatch.index + 1 ) + newString[ currentMatch.index + 1 ].toUpperCase() + newString.slice( currentMatch.index + 2 );
    }

    // Retira hífenes do texto
    newString = newString.replace( /-/g, '' );

    // Retorna texto convertido
    return newString;
  }
} );
