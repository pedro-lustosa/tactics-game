// Adição de método 'oAssignByCopy' a 'Object'
Object.defineProperty( Object, 'oAssignByCopy', {
  // Efetivamente torna objetos e arranjos do alvo cópias, em vez de referências
  value: function ( receiver, giver, config = {} ) {
    // Identificadores
    var { only = [], never = [] } = config,
        giverKeys = Object.keys( giver );

    // Caso apenas algumas propriedades devam ser alvo da função, executa esse filtro
    if( only.length ) giverKeys = giverKeys.filter( key => only.includes( key ) );

    // Itera por propriedades a serem concedidas
    for( let key of giverKeys ) {
      // Filtra propriedades que nunca devem ser alvo da função
      if( never.includes( key ) ) continue;

      // Para caso propriedade alvo aponte um arranjo
      if( Array.isArray( giver[ key ] ) ) {
        // Atribui ao objeto destinatário uma cópia do arranjo
        receiver[ key ] = giver[ key ].slice();

        // Realiza recursão por propriedades do remetente
        Object.oAssignByCopy( receiver[ key ], giver[ key ], { never: never } );
      }

      // Para caso propriedade alvo seja um objeto
      else if( giver[ key ] && typeof giver[ key ] == 'object' ) {
        // Atribui ao objeto destinatário propriedades do remetente
        receiver[ key ] = Object.assign( Object.create( Object.getPrototypeOf( giver[ key ] ) ), giver[ key ] );

        // Realiza recursão por propriedades do remetente
        Object.oAssignByCopy( receiver[ key ], giver[ key ], { never: never } );
      }

      // Para os demais casos
      else {
        // Atribuição normal de propriedades entre destinatário e remetente
        receiver[ key ] = giver[ key ];
      }
    }

    // Retorna objeto alvo
    return receiver;
  }
} );
