// Adição de método 'oCycle' a 'Array.prototype'
Object.defineProperty( Array.prototype, 'oCycle', {
  // Se o argumento for negativo ou [igual a/maior que] o tamanho do arranjo, ajusta-o para retornar o valor de um índice do arranjo
  value: function ( integer = 0 ) {
    // Validação
    if( !Number.isInteger( integer ) ) throw new Error( 'Argument "integer" must be an integer.' );

    // Não executa função caso arranjo esteja vazio
    if( !this.length ) return;

    // Caso o número passado seja maior ou igual ao tamanho do arranjo, retorna módulo de si pelo tamanho do arranjo
    if( integer >= this.length ) return this[ integer % this.length ];

    // Enquanto o número passado for um valor negativo, acrescenta a ele tamanho do arranjo
    while( integer < 0 ) integer += this.length;

    // Retorna elemento no arranjo de índice equivalente ao valor ajustado do número passado
    return this[ integer ];
  }
} );
