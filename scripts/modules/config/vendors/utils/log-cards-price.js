// Módulos
import * as m from '../../../../modules.js';

// Exibe o preço das cartas que atenderem aos argumentos passados
window.logCardsPrice = function ( primitiveType, condition ) {
  // Validação

  /// De 'primitiveType'
  m.oAssert( [ 'all', 'creatures', 'items', 'spells' ].includes( primitiveType ) );

  /// De 'condition'
  m.oAssert( !condition || typeof condition == 'function' );

  // Identificadores
  var cardsToShowPrice = [],
      priceText = '';

  // Quando aplicável, captura preço de criaturas
  if( [ 'all', 'creatures' ].includes( primitiveType ) ) getCreaturePrices();

  // Quando aplicável, captura preço de equipamentos
  if( [ 'all', 'items' ].includes( primitiveType ) ) getItemPrices();

  // Quando aplicável, captura preço de magias
  if( [ 'all', 'spells' ].includes( primitiveType ) ) getSpellPrices();

  // Caso cartas devam ser filtradas, realiza esse filtro
  if( condition ) cardsToShowPrice = cardsToShowPrice.filter( condition );

  // Popula texto sobre preço das cartas alvo
  cardsToShowPrice.forEach( card => priceText += `${ card.content.designation.full }: ${ card.coins.base }\n` );

  // Exibe preço das cartas desejadas
  return console.log( priceText.trimEnd() );

  // Funções

  /// Adiciona ao arranjo de cartas a exibirem seu preço as de criatura
  function getCreaturePrices() {
    // Identificadores
    var creatureCards = [],
        levelArray = [ 1, 2, 3, 4 ];

    // Itera por construtores de criaturas
    for( let constructor of m.data.cards.creatures ) {
      // Filtra construtores cujo protótipo não seja uma instância de humanoides
      if( !( constructor.prototype instanceof m.Humanoid ) ) continue;

      // Identificadores
      let sampleCard = new constructor();

      // Filtra fichas
      if( sampleCard.isToken ) continue;

      // Adiciona cartas do humanoide alvo ao arranjo de cartas pertinentes
      creatureCards.push( ...levelArray.map( value => new constructor( { level: value } ) ) );
    }

    // Adiciona cartas de criatura alvo ao arranjo de cartas pertinente
    cardsToShowPrice = cardsToShowPrice.concat( creatureCards );
  }

  /// Adiciona ao arranjo de cartas a exibirem seu preço as de equipamento
  function getItemPrices() {
    // Identificadores
    var itemCards = [],
        gradeArray = [ 'ordinary', 'masterpiece' ];

    // Itera por construtores de equipamentos
    for( let constructor of m.data.cards.items ) {
      // Identificadores
      let sampleCard = new constructor(),
          sizeArray = sampleCard instanceof m.Weapon && [ 'sword', 'bow', 'crossbow' ].includes( sampleCard.content.typeset.role.name ) ?
            [ 'small', 'medium' ] : [ 'small', 'medium', 'large' ];

      // Adição de relíquias ao arranjo de cartas
      if( sampleCard.content.typeset.grade == 'relic' )
        itemCards.push( sampleCard );

      // Adição de armas mundanas e ordinárias ao arranjo de cartas
      else if( sampleCard instanceof m.Weapon && gradeArray.includes( sampleCard.content.typeset.grade ) )
        itemCards.push( ...sizeArray.flatMap( size => gradeArray.map( grade => new constructor( { size: size, grade: grade } ) ) ) );

      // Adição de equipamentos com tamanho ao arranjo de cartas
      else if( sampleCard.content.typeset.size )
        itemCards.push( ...sizeArray.map( size => new constructor( { size: size } ) ) );

      // Adição de demais equipamentos ao arranjo de cartas
      else
        itemCards.push( sampleCard );
    }

    // Adiciona cartas de equipamento alvo ao arranjo de cartas pertinente
    cardsToShowPrice = cardsToShowPrice.concat( itemCards );
  }

  /// Adiciona ao arranjo de cartas a exibirem seu preço as de magias
  function getSpellPrices() {
    // Identificadores
    var spellCards = m.data.cards.spells.map( constructor => new constructor() );

    // Adiciona cartas de magia alvo ao arranjo de cartas pertinente
    cardsToShowPrice = cardsToShowPrice.concat( spellCards );
  }
}
