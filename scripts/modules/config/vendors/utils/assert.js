// Retorna um erro se expressão passada retornar um valor falso
export const oAssert = function ( expression, message = 'Assertion failed.', errorType = Error ) {
  // Validação

  /// De 'message'
  if( typeof message != 'string' ) throw new Error( 'Argument "message" must be a string.' );

  /// De 'errorType'
  if( errorType != Error && !( errorType.prototype instanceof Error ) ) throw new Error( 'Argument "errorType" must be an Error object or an instance of it.' );

  // Lança erro caso expressão tenha retornado um valor falso
  if( !expression ) throw new errorType( message );
};
