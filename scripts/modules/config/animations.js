// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const animations = {};

/// Propriedades iniciais
defineProperties: {
  // Destaca momentaneamente carta passada
  animations.highlightCard = function ( card, config = {} ) {
    // Identificadores pré-validação
    var { color: highlightColor = 'white', floatingText = '', floatingTextColor = 'black' } = config;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Card );
      m.oAssert( typeof floatingText == 'string' );
      m.oAssert( [ highlightColor, floatingTextColor ].every( color => color in m.data.colors ) );
    }

    // Retorna promessa a ser cumprida com a conclusão da animação
    return new Promise( function ( resolve, reject ) {
      // Identificadores
      var cardSizes = card.getLocalBounds(),
          highlightEffect = card.addChild( new PIXI.Graphics() ),
          isInEngagementZone = card.slot instanceof m.EngagementZone;

      // Confecção do objeto para animação

      /// Cor de preenchimento
      highlightEffect.beginFill( m.data.colors[ highlightColor ] );

      /// Renderização
      isInEngagementZone ? highlightEffect.drawCircle( 0, 0, cardSizes.width * .5 ) : highlightEffect.drawRect( 0, 0, cardSizes.width, cardSizes.height );

      /// Encerramento
      highlightEffect.endFill();

      // Caso carta esteja em uma zona de engajamento, ajusta posicionamento do objeto de destaque
      if( isInEngagementZone )
        highlightEffect.position.set( card.body.symbol.x + highlightEffect.width * .5, card.body.symbol.y + highlightEffect.height * .5 );

      // Para caso tenha sido passado um texto a ser revelado
      if( floatingText ) {
        // Gera objeto de texto, e o inclui ao objeto de animação da carta
        let sizeMultiplier = isInEngagementZone ? .4 : .25,
            textStyle = new PIXI.TextStyle( {
              fontFamily: m.assets.fonts.sourceSansPro.default.name,
              fontSize: Math.round( highlightEffect.height * sizeMultiplier ),
              padding: m.assets.fonts.padding,
              fill: m.data.colors[ floatingTextColor ]
            } ),
            floatingBitmap = highlightEffect.addChild( new PIXI.Text( floatingText, textStyle ) );

        // Posiciona objeto de texto
        floatingBitmap.position.set(
          isInEngagementZone ? floatingBitmap.x - floatingBitmap.width * .5 : highlightEffect.width * .5 - floatingBitmap.width * .5,
          isInEngagementZone ? floatingBitmap.y - floatingBitmap.height * .5 : highlightEffect.height * .5 - floatingBitmap.height * .5
        );
      }

      // Animação do objeto
      gsap.from( highlightEffect, { pixi: { alpha: 0 }, duration: 1, repeat: 1, yoyo: true, ease: "power1.inOut", onComplete: completeAnimation } );

      // Funções

      /// Operações a serem executadas após conclusão da animação
      function completeAnimation() {
        // Destrói objeto de animação
        card.removeChild( highlightEffect ).destroy( { children: true } );

        // Indica que promessa foi concluída
        return resolve();
      }
    } );
  }
}
