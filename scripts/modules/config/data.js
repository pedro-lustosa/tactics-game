// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const data = {};

/// Propriedades iniciais
defineProperties: {
  // Criadores do jogo
  data.creators = {
    // Programadores
    programmers: [ {
      name: 'Pedro Lustosa'
    } ],
    // Artistas
    artists: [ {
      name: 'Rael Dionisio',
      cardImages: [
        'human-rookie-maceman', 'human-regular-maceman', 'human-veteran-maceman', 'human-elite-maceman',
        'human-rookie-axeman', 'human-regular-axeman', 'human-veteran-axeman', 'human-elite-axeman',
        'human-rookie-swordsman', 'human-regular-swordsman', 'human-veteran-swordsman', 'human-elite-swordsman',
        'human-rookie-spearman', 'human-regular-spearman', 'human-veteran-spearman', 'human-elite-spearman',
        'human-rookie-slingman', 'human-regular-slingman', 'human-veteran-slingman', 'human-elite-slingman',
        'human-rookie-bowman', 'human-regular-bowman', 'human-veteran-bowman', 'human-elite-bowman',
        'human-rookie-crossbowman', 'human-regular-crossbowman', 'human-veteran-crossbowman', 'human-elite-crossbowman',
        'human-novice-theurge', 'human-disciple-theurge', 'human-adept-theurge', 'human-master-theurge',
        'human-novice-warlock', 'human-disciple-warlock', 'human-adept-warlock', 'human-master-warlock',
        'human-novice-wizard', 'human-disciple-wizard', 'human-adept-wizard', 'human-master-wizard',
        'human-novice-sorcerer', 'human-disciple-sorcerer', 'human-adept-sorcerer', 'human-master-sorcerer'
      ]
    }, {
      name: 'Bryan F. Rosado',
      cardImages: [
        'orc-rookie-maceman', 'orc-regular-maceman', 'orc-veteran-maceman', 'orc-elite-maceman',
        'orc-rookie-axeman', 'orc-regular-axeman', 'orc-veteran-axeman', 'orc-elite-axeman',
        'orc-rookie-spearman', 'orc-regular-spearman', 'orc-veteran-spearman', 'orc-elite-spearman',
        'orc-rookie-bowman', 'orc-regular-bowman', 'orc-veteran-bowman', 'orc-elite-bowman',
        'orc-novice-sorcerer', 'orc-disciple-sorcerer', 'orc-adept-sorcerer', 'orc-master-sorcerer',
        'orc-novice-warlock', 'orc-disciple-warlock', 'orc-adept-warlock', 'orc-master-warlock',
        'commander-ixohch'
      ]
    }, {
      name: 'Diego Cunha',
      cardImages: [
        'elf-rookie-swordsman', 'elf-regular-swordsman', 'elf-veteran-swordsman', 'elf-elite-swordsman',
        'elf-rookie-spearman', 'elf-regular-spearman', 'elf-veteran-spearman', 'elf-elite-spearman',
        'elf-rookie-slingman', 'elf-regular-slingman', 'elf-veteran-slingman', 'elf-elite-slingman'
      ]
    }, {
      name: 'Júlio Rocha',
      cardImages: [
        'dwarf-rookie-maceman', 'dwarf-regular-maceman', 'dwarf-veteran-maceman', 'dwarf-elite-maceman',
        'dwarf-rookie-swordsman', 'dwarf-regular-swordsman', 'dwarf-veteran-swordsman', 'dwarf-elite-swordsman',
        'dwarf-rookie-axeman', 'dwarf-regular-axeman', 'dwarf-veteran-axeman', 'dwarf-elite-axeman',
        'dwarf-rookie-spearman', 'dwarf-regular-spearman', 'dwarf-veteran-spearman', 'dwarf-elite-spearman',
        'dwarf-rookie-crossbowman', 'dwarf-regular-crossbowman', 'dwarf-veteran-crossbowman', 'dwarf-elite-crossbowman',
        'commander-the-magnate',
        'tulpa-card', 'spirit-card'
      ]
    }, {
      name: 'Israel Botelho',
      cardImages: [
        'goblin-rookie-maceman', 'goblin-regular-maceman', 'goblin-veteran-maceman', 'goblin-elite-maceman',
        'goblin-rookie-axeman', 'goblin-regular-axeman', 'goblin-veteran-axeman', 'goblin-elite-axeman',
        'goblin-rookie-crossbowman', 'goblin-regular-crossbowman', 'goblin-veteran-crossbowman', 'goblin-elite-crossbowman',
        'goblin-novice-sorcerer', 'goblin-disciple-sorcerer', 'goblin-adept-sorcerer', 'goblin-master-sorcerer',
        'commander-wimis'
      ]
    }, {
      name: 'Guilherme Menezes',
      cardImages: [
        'ogre-rookie-maceman', 'ogre-regular-maceman', 'ogre-veteran-maceman', 'ogre-elite-maceman',
        'ogre-rookie-axeman', 'ogre-regular-axeman', 'ogre-veteran-axeman', 'ogre-elite-axeman',
        'ogre-rookie-slingman', 'ogre-regular-slingman', 'ogre-veteran-slingman', 'ogre-elite-slingman',
        'ogre-novice-wizard', 'ogre-disciple-wizard', 'ogre-adept-wizard', 'ogre-master-wizard',
        'commander-gamomba'
      ]
    } ]
  };

  // Métodos não configuráveis de 'data.creators'
  Object.defineProperties( data.creators, {
    // Retorna nome do artista de uma imagem
    getImageArtist: {
      value: function( imageName, imageType = 'cardImages' ) {
        // Caso artista alvo tenha feito imagem passada, retorna seu nome
        for( let artist of this.artists )
          if( artist[ imageType ].includes( imageName ) ) return artist.name;

        // Retorna ausência de um nome de artista
        return '';
      }
    }
  } );

  // Cores usadas pelos objetos gráficos do jogo
  data.colors = {
    // Preto
    black: 0x000000, // hsl( 0deg, 0%, 0% )
    // Semipreto 1
    semiblack1: 0x0f0f0f, // hsl( 0deg, 0%, 6% )
    // Semipreto 2
    semiblack2: 0x1f1f1f, // hsl( 0deg, 0%, 12% )
    // Semipreto 3
    semiblack3: 0x2e2e2e, // hsl( 0deg, 0%, 18% )
    // Semipreto 4
    semiblack4: 0x3d3d3d, // hsl( 0deg, 0%, 24% )
    // Semipreto 5
    semiblack5: 0x4d4d4d, // hsl( 0deg, 0%, 30% )
    // Cinza
    gray: 0x808080, // hsl( 0deg, 0%, 50% )
    // Branco
    white: 0xffffff, // hsl( 0deg, 0%, 100% )
    // Semibranco 1
    semiwhite1: 0xe6e6e6, // hsl( 0deg, 0%, 90% )
    // Semibranco 2
    semiwhite2: 0xcccccc, // hsl( 0deg, 0%, 80% )
    // Semibranco 3
    semiwhite3: 0xb2b2b2, // hsl( 0deg, 0%, 70% )
    // Semibranco 4
    semiwhite4: 0x999999, // hsl( 0deg, 0%, 60% )
    // Vermelho
    red: 0xff0000, // hsl( 0deg, 100%, 50% )
    // Vermelho claro
    lightRed: 0xff6666, // hsl( 0deg, 100%, 70% )
    // Vermelho desbotado
    fadedRed: 0xe06c6c, // hsl( 0deg, 65%, 65% )
    // Vermelho escuro
    darkRed: 0xb30000, // hsl( 0deg, 100%, 35% )
    // Vermelho escuro vinho
    darkWineRed: 0x790606, // hsl( 0deg, 90%, 25% )
    // Bege
    beige: 0xd9bf8c, // hsl( 40deg, 50%, 70% )
    // Bege desbotado
    fadedBeige: 0xcdbb98, // hsl( 40deg, 35%, 70% )
    // Laranja
    orange: 0xc2660a, // hsl( 30deg, 90%, 40% )
    // Laranja claro
    lightOrange: 0xffb366, // hsl( 30deg, 100%, 70% )
    // Amarelo
    yellow: 0xffff00, // hsl( 60deg, 100%, 50% )
    // Amarelo claro
    lightYellow: 0xffff66, // hsl( 60deg, 100%, 70% )
    // Amarelo escuro
    darkYellow: 0x808000, // hsl( 60deg, 100%, 25% )
    // Verde
    green: 0x00ff00, // hsl( 120deg, 100%, 50% )
    // Verde claro
    lightGreen: 0x66ff66, // hsl( 120deg, 100%, 70% )
    // Verde escuro
    darkGreen: 0x008000, // hsl( 120deg, 100%, 25% )
    // Verde escuro desbotado
    darkFadedGreen: 0x364936, // hsl( 120deg, 15%, 25% )
    // Azul
    blue: 0x0000ff, // hsl( 240deg, 100%, 50% )
    // Azul claro
    lightBlue: 0x6666ff, // hsl( 240deg, 100%, 70% )
    // Azul escuro
    darkBlue: 0x000099, // hsl( 240deg, 100%, 30% )
    // Azul escuro desbotado
    darkFadedBlue: 0x363663 // hsl( 240deg, 30%, 30% )
  };

  // Métodos não configuráveis de 'data.colors'
  Object.defineProperties( data.colors, {
    // Retorna uma cor que contrasta com a passada
    contrastWith: {
      value: function( targetColor, lightColor = data.colors.white, darkColor = data.colors.black ) {
        // Identificadores
        var colors = Object.values( data.colors );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( Array.from( arguments ).every( color => colors.includes( color ) ) );
          m.oAssert( PIXI.utils.hex2rgb( lightColor ).some( value => value >= .5 ) );
          m.oAssert( PIXI.utils.hex2rgb( darkColor ).every( value => value < .5 ) );
        }

        // Identifica e retorna qual cor contrasta com a passada
        return PIXI.utils.hex2rgb( targetColor ).some( value => value >= .5 ) ? darkColor : lightColor;
      }
    }
  } );

  // Medidas usadas por componentes do jogo
  data.sizes = {
    // Botões
    buttons: {
      // Preenchimento horizontal
      paddingX: 60,
      // Preenchimento vertical
      paddingY: 4,
      // Arredondamento dos cantos
      roundedCorners: 10,
      // Tamanho da fonte
      fontSize: -19
    }
  };

  // Cartas do jogo
  data.cards = {
    commanders: [],
    creatures: [],
    items: [],
    spells: []
  };

  // Ações do jogo
  data.actions = [];

  // Retorna um componente do jogo a partir de seu identificador na partida atual
  data.getFromMatchId = function ( matchId ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( m.GameMatch.current );
      m.oAssert( matchId && typeof matchId == 'string' );
    }

    // Delega identificação do componente em função de se ele é uma carta ou casa
    return [ 'M', 'S' ].some( letter => matchId.startsWith( letter ) ) ? m.Card.getFromMatchId( matchId ) : m.FieldSlot.getFromMatchId( matchId );
  }
}
