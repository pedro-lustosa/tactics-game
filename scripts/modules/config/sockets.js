// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const sockets = {};

/// Propriedades iniciais
defineProperties: {
  // Socket para conexão com o servidor
  sockets.current = null;

  // Cria e configura socket de conexão do usuário com o servidor
  sockets.init = function () {
    // Criação do socket
    this.current = io( '/game' );

    // Evento para, após eventuais reconexões, restaurar no servidor dados perdidos ante uma desconexão
    this.current.addEventListener( 'connect', sockets.resendData );

    // Evento para tratar desconexões do socket com o servidor
    this.current.addEventListener( 'disconnect', sockets.handleDisconnection );

    // Evento para exibir mensagens de notificreconnect-to-matchação acionadas pelo servidor
    this.current.addEventListener( 'notify-user', sockets.notifyUser );

    // Evento para se gerar uma partida após o encontro de um oponente adequado
    this.current.addEventListener( 'create-match', sockets.createMatch );

    // Evento para preparar usuário na partida para seu fim programado, devido à desconexão de um jogador
    this.current.addEventListener( 'schedule-match-abort-by-disconnect', sockets.scheduleMatchAbortByDisconnect );

    // Evento para tratar reconexões de um jogador à partida em que se estiver
    this.current.addEventListener( 'handle-match-reconnections', sockets.handleMatchReconnections );

    // Evento para abortar uma partida, por força da desconexão de um jogador
    this.current.addEventListener( 'abort-match', sockets.abortMatch );

    // Evento para concluir uma partida, por força da desistência de um jogador
    this.current.addEventListener( 'concede-match', sockets.concedeMatch );

    // Evento para se acionar uma ação propagada
    this.current.addEventListener( 'execute-action', sockets.executeAction );

    // Evento para a progressão de uma ação
    this.current.addEventListener( 'progress-action', sockets.progressAction );

    // Evento para o cancelamento de uma ação
    this.current.addEventListener( 'cancel-action', sockets.cancelAction );

    // Evento para se controlar o fluxo de uma partida
    this.current.addEventListener( 'control-flow-progress', sockets.controlFlowProgress );

    // Evento para avançar o fluxo de uma partida, após sincronização de todos seus participantes
    this.current.addEventListener( 'progress-flow', sockets.progressFlow );

    // Evento para a progressão do efeito de uma carta
    this.current.addEventListener( 'progress-effect', sockets.progressEffect );

    // Evento para a progressão de uma parada de ataque livre
    this.current.addEventListener( 'progress-free-attack-break', sockets.progressFreeAttackBreak );

    // Evento para a progressão das etapas principais de uma parada de combate
    this.current.addEventListener( 'progress-combat-break-body', sockets.progressCombatBreakBody );

    // Evento para a progressão da delonga de uma parada de combate
    this.current.addEventListener( 'progress-combat-break-overtime', sockets.progressCombatBreakOvertime );

    // Evento para o cancelamento de uma parada de combate
    this.current.addEventListener( 'cancel-combat-break', sockets.cancelCombatBreak );

    // Evento para a progressão de uma parada de destino
    this.current.addEventListener( 'progress-fate-break', sockets.progressFateBreak );

    // Evento para encerrar atividades em caso de desligamento do servidor
    this.current.addEventListener( 'shut-down', sockets.shutDown );
  }

  // Atualiza dados do usuário acessando o jogo no socket do servidor
  sockets.updateUserData = async function () {
    // Captura dos dados do usuário acessando o jogo
    var userData = await m.GameUser.loadData();

    // Vincula ao socket do servidor dados pertinentes do usuário alvo
    await sockets.current.emitWithAck( 'set-data', { 'user.name': userData.name, 'user.ranking': userData.ranking, 'user.matches': userData.matches } );

    // Retorna dados do usuário
    return userData;
  }

  // Após eventuais reconexões, restaura no servidor dados perdidos ante uma desconexão
  sockets.resendData = async function () {
    // Caso não exista uma partida real em andamento, desconsidera reconexão
    if( !m.GameMatch.current || m.GameMatch.current.isSimulation ) return;

    // Identificadores
    var playerParity = m.GamePlayer.current?.parity;

    // Reatualiza socket do servidor com dados do usuário atual
    await sockets.updateUserData();

    // Reconecta socket à sala da partida
    sockets.current.emit( 'reconnect-to-match', { matchName: m.GameMatch.current.name, playerParity: playerParity ?? '' } );

    // Caso usuário seja um jogador, trata sua reconexão à partida
    if( playerParity ) sockets.handleMatchReconnections( playerParity );
  }

  // Trata desconexões do socket com o servidor
  sockets.handleDisconnection = function () {
    // Caso não exista uma partida real em andamento e não se esteja procurando uma, desconsidera desconexão
    if( !m.GameMatch.isInMatchmaking && ( !m.GameMatch.current || m.GameMatch.current.isSimulation ) ) return;

    // Para caso se esteja procurando uma partida
    if( m.GameMatch.isInMatchmaking ) {
      // Indica que uma partida não está mais sendo procurada
      m.GameMatch.isInMatchmaking = false;

      // Notifica a usuário sobre problemas de rede
      return m.noticeBar.show( m.languages.notices.getNetworkText( 'failed-request' ), 'lifted' );
    }

    // Para caso usuário não seja um jogador da partida em andamento
    if( !m.GamePlayer.current ) {
      // Programa notificação sobre remoção da partida
      setTimeout( function () {
        return new m.AlertModal( {
          text: m.languages.notices.getNetworkText( 'removed-from-match-by-disconnect' )
        } ).insert();
      }, 5 );

      // Altera tela do usuário para a de abertura
      return m.GameScreen.change( new m.OpeningScreen() );
    }

    // Prepara usuário para fim programado da partida em andamento
    sockets.scheduleMatchAbortByDisconnect( m.GamePlayer.current.parity );
  }

  // Exibe mensagens de notificação acionadas pelo servidor
  sockets.notifyUser = function ( messageName, messageConfig = {}, classNames = [] ) {
    // Exibição da mensagem
    return m.noticeBar.show( m.languages.notices.getInvalidationText( messageName, messageConfig ), ...classNames );
  }

  // Inicia procura por uma partida
  sockets.enterMatchmaking = async function () {
    // Não executa função caso socket não esteja conectado
    if( !sockets.current.connected ) return m.noticeBar.show( m.languages.notices.getNetworkText( 'failed-request' ), 'lifted' );

    // Envia a socket dados atualizados do usuário
    await sockets.updateUserData();

    // Indica que uma partida está sendo procurada
    m.GameMatch.isInMatchmaking = true;

    // Emite evento para entrada na sala de procura por partidas
    m.sockets.current.emit( 'enter-matchmaking' )
  }

  // Gera uma nova partida
  sockets.createMatch = function ( usersData, matchData ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( Array.isArray( usersData ) && usersData.length == 2 );
      m.oAssert( usersData.every( userData => userData.constructor == Object && userData.name ) );
      m.oAssert( matchData.constructor == Object );
      m.oAssert( matchData.name && typeof matchData.name == 'string' );
      m.oAssert( [ 'odd', 'even' ].every( parity => usersData.some( userData => userData.name == matchData.players?.[ parity ]?.name ) ) );
    }

    // Identificadores
    var matchUsers = usersData.map( userData => userData.name == m.GameUser.current.name ? m.GameUser.current : new m.GameUser( { userData: userData } ) );

    // Se necessário, ajusta ordem dos usuários no arranjo, para que esteja alinhada com suas paridades
    if( matchUsers[ 0 ].name != matchData.players.odd.name ) matchUsers.push( matchUsers.shift() );

    // Indica que uma partida não está mais sendo procurada
    m.GameMatch.isInMatchmaking = false;

    // Navega para a tela de partidas
    return m.GameScreen.change( new m.MatchScreen( {
      matchName: matchData.name,
      matchUsers: matchUsers,
      matchDecks: matchUsers.map( user => user.decks.find( deck => deck.validate( { shortCircuit: true } ).isValid ) )
    } ) );
  }

  // Prepara usuário na partida para seu fim programado, devido à desconexão de um jogador
  sockets.scheduleMatchAbortByDisconnect = function ( playerParity ) {
    // Identificadores
    var isSelf = m.GamePlayer.current?.parity == playerParity;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );

    // Programa evento para abortar partida
    m.GameMatch.current.abortTimeout = setTimeout( sockets.abortMatch.bind( sockets ), 1000 * 60, playerParity );

    // Bloqueia interação com o jogo
    m.app.blockInteraction();

    // Notifica usuário sobre problema de conectividade
    m.noticeBar.show( m.languages.notices.getNetworkText( 'match-disconnect-' + ( isSelf ? 'player' : 'other' ), { playerParity: playerParity } ) );
  }

  // Trata reconexões de um jogador à partida em que se estiver
  sockets.handleMatchReconnections = function ( playerParity ) {
    // Identificadores
    var isSelf = m.GamePlayer.current?.parity == playerParity;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );

    // Cancela eventual evento para a finalização da partida
    m.GameMatch.current.abortTimeout &&= clearTimeout( m.GameMatch.current.abortTimeout ) ?? null;

    // Notifica sobre reconexão bem-sucedida do usuário alvo
    m.noticeBar.show(
      m.languages.notices.getNetworkText( 'match-reconnect-' + ( isSelf ? 'player' : 'other' ), { playerParity: playerParity } ), 'green', 'fast'
    );

    // Desbloqueia interação com o jogo
    m.app.unblockInteraction();
  }

  // Aborta uma partida, por força da desconexão de um jogador
  sockets.abortMatch = function ( playerParity ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );

    // Identificadores
    var modalText = 'abort-match-by-disconnect-' + ( m.GamePlayer.current?.parity == playerParity ? 'player' : 'other' );

    // Desbloqueia interação com o jogo
    m.app.unblockInteraction();

    // Caso usuário seja o jogador não desconectado, atualiza no servidor dados da partida
    if( m.GamePlayer.current && m.GamePlayer.current.parity != playerParity ) m.GameMatch.current.updateServer();

    // Programa notificação sobre encerramento da partida por desconexão de um jogador
    setTimeout( function () {
      return new m.AlertModal( {
        text: m.languages.notices.getNetworkText( modalText, { playerParity: playerParity } )
      } ).insert();
    }, 5 );

    // Altera tela do usuário para a de abertura
    m.GameScreen.change( new m.OpeningScreen() );
  }

  // Conclui uma partida, por força da desistência de um jogador
  sockets.concedeMatch = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { playerParity } = configData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );

    // Identificadores pós-validação
    var loser = m.GameMatch.current.players[ playerParity ],
        winner = m.GameMatch.current.players[ playerParity == 'odd' ? 'even' : 'odd' ],
        modalText = 'win-by-concession-' + ( m.GamePlayer.current ? 'player' : 'other' );

    // Indica que jogador que desistiu da partida deve perder
    loser.isToWin = false;

    // Indica que jogador que permaneceu na partida deve ganhar
    winner.isToWin = true;

    // Indica que partida foi concluída
    m.GameMatch.current.stats.isFinished = true;

    // Notifica usuário sobre conclusão da partida por desistência de um jogador
    return new m.AlertModal( {
      text: m.languages.notices.getModalText( modalText, { winner: winner, loser: loser } ),
      action: () => m.GameScreen.change( new m.OpeningScreen() )
    } ).replace();
  }

  // Propaga uma ação para o oponente de uma partida
  sockets.propagateAction = function ( eventData = {} ) {
    // Não executa função caso ação já seja uma propagada
    if( eventData.eventTarget.isPropagated ) return;

    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: action, actionCommitter, actionTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'action-change' && eventType == 'commit' );
      m.oAssert( action instanceof m.GameAction );
    }

    // Identificadores pós-validação
    var gameMatch = m.GameMatch.current,
        actionObject = action.toObject(),
        flowChain = gameMatch.flow.toChain();

    // Emite evento para propagação da ação
    sockets.current.emit( 'propagate-action', { matchName: gameMatch.name, flowChain: flowChain, actionObject: actionObject } );
  }

  // Executa uma ação propagada
  sockets.executeAction = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { flowChain, actionObject } = config,
        { constructorName, committer: committerId, target: targetId = '' } = actionObject;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( actionObject.constructor == Object );
      m.oAssert( [ constructorName, committerId ].every( value => value && typeof value == 'string' ) );
    }

    // Identificadores pós-validação
    var { progressQueue, executionQueue } = m.GameAction,
        lastQueuedAction = progressQueue.oCycle( -1 ) ?? null,
        topmostAction = m.GameAction.currents[ 0 ],
        delayExecution = () => setTimeout( sockets.executeAction.bind( sockets, config ), m.app.timeoutDelay.average ),
        actionParameters = {};

    // Caso ainda não esteja lá, adiciona objeto da ação ao arranjo de ações a serem executadas
    if( executionQueue.indexOf( actionObject ) == -1 ) executionQueue.push( actionObject );

    // Caso ainda haja ações de início do estágio sendo executadas, reprograma execução desta função para depois do fim da execução da última ação lá
    if( lastQueuedAction ) return m.events.actionChangeEnd.postExecution.add( lastQueuedAction, delayExecution, { once: true } );

    // Caso já haja uma ação propagada sendo acionada, reprograma execução desta função para depois da resolução desta ação
    if( topmostAction?.isPropagated && !topmostAction.isCommitted )
      return m.events.actionChangeEnd.resolution.add( topmostAction, delayExecution, { once: true } );

    // Caso partida esteja em uma parada, reprograma execução desta função para depois da resolução da parada
    if( m.GameMatch.getActiveBreaks().length ) return m.events.breakEnd.any.add( m.GameMatch.current, delayExecution, { once: true } );

    // Captura acionante da ação
    actionParameters.committer = m.data.getFromMatchId( committerId );

    // Se existente, captura alvo da ação
    if( targetId ) actionParameters.target = m.data.getFromMatchId( targetId );

    // Indica que ação é uma propagada
    actionParameters.isPropagated = true;

    // Gera ação
    let actionConstructor = m.data.actions.find( action => action.name == constructorName ),
        action = new actionConstructor( actionParameters );

    // Quando aplicável, ajusta ação segundo operações que variam por ação
    action.prepareExecution?.( actionObject );

    // Adiciona evento para realizar ajustes à ação após o fim de sua resolução
    m.events.actionChangeEnd.resolution.add( action, adjustExecutionEnd, { context: sockets, once: true } );

    // Aciona ação
    return ( action.currentExecution = action.execute() ).next();

    // Funções

    /// Ajustes para o fim do acionamento da ação propagada
    function adjustExecutionEnd() {
      // Caso objeto da ação esteja no arranjo de ações a serem executadas, retira-o de lá
      if( executionQueue.indexOf( actionObject ) != -1 ) executionQueue.splice( executionQueue.indexOf( actionObject ), 1 );
    }
  }

  // Progride uma ação
  sockets.progressAction = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { actionName, actionCommitterId, actionIndex, value, isMatchId = false } = configData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ actionName, actionCommitterId ].every( value => value && typeof value == 'string' ) );
      m.oAssert( Number.isInteger( actionIndex ) && actionIndex >= 0 );
      m.oAssert( !isMatchId || value && typeof value == 'string' );
    }

    // Identificadores pós-validação
    var action = m.GameAction.currents[ actionIndex ];

    // Caso ação capturada não seja a esperada, executa esta função novamente após certo tempo
    if( action?.name != actionName || action?.committer != m.Card.getFromMatchId( actionCommitterId ) )
      return setTimeout( sockets.progressAction.bind( sockets, config ), m.app.timeoutDelay.long );

    // Caso valor a ser passado para a progressão da ação seja um Id da partida, converte-o para seu respectivo componente
    if( isMatchId ) value = m.data.getFromMatchId( value );

    // Progride ação
    action.currentExecution.next( value );
  }

  // Cancela uma ação
  sockets.cancelAction = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { playerParity, actionName, actionCommitterId, actionIndex } = configData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );
      m.oAssert( [ actionName, actionCommitterId ].every( value => value && typeof value == 'string' ) );
      m.oAssert( Number.isInteger( actionIndex ) && actionIndex >= 0 );
    }

    // Identificadores pós-validação
    var action = m.GameAction.currents[ actionIndex ];

    // Caso ação capturada não seja a esperada, executa esta função novamente após certo tempo
    if( action?.name != actionName || action?.committer != m.Card.getFromMatchId( actionCommitterId ) )
      return setTimeout( sockets.cancelAction.bind( sockets, config ), m.app.timeoutDelay.long );

    // Notifica usuário sobre cancelamento da ação
    m.noticeBar.show( m.languages.notices.getChoiceText( 'canceling-action', {
      playerParity: playerParity,
      action: action
    } ) );

    // Cancela ação
    action.cancel();
  }

  // Controla o fluxo de uma partida
  sockets.controlFlowProgress = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { flowChain, method } = configData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( flowChain && typeof flowChain == 'string' );
      m.oAssert( [ 'progress', 'suspend' ].includes( method ) );
    }

    // Desconsidera comandos de cadeias de fluxo desatualizadas
    if( m.GameMatch.current.flow.toChain() != flowChain ) return false;

    // Identificadores pós-validação
    var intermissionBar = m.GameScreen.current.children.find( child => child.name == 'intermission-static-bar' );

    // Remove barra de intermissão, quando existente
    intermissionBar?.remove();

    // Tenta controlar o fluxo da partida, e em caso de fracasso torna a executar esta função após certo tempo
    if( !m.GameMatch.current.flow[ method ]() )
      setTimeout( sockets.controlFlowProgress.bind( sockets, config ), m.app.timeoutDelay.long );
  }

  // Avança o fluxo de uma partida, após sincronização de todos seus participantes
  sockets.progressFlow = function () {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores
    var synchronizingBar = m.GameScreen.current.children.find( child => child.name == 'synchronizing-flow-static-bar' );

    // Remove barra de sincronização do fluxo, quando existente
    synchronizingBar?.remove();

    // Avança fluxo da partida
    m.GameMatch.current.flow.advance();
  }

  // Progride um efeito de uma carta
  sockets.progressEffect = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores
    var { data: configData = {} } = config,
        { cardId, value } = configData,
        card = m.Card.getFromMatchId( cardId );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Card );
      m.oAssert( 'currentExecution' in card.content.effects );
    }

    // Caso efeito da carta passada não esteja sendo no momento executado, executa esta função novamente após certo tempo
    if( !card.content.effects.currentExecution )
      return setTimeout( sockets.progressEffect.bind( sockets, config ), m.app.timeoutDelay.long );

    // Progride efeito da carta
    card.content.effects.currentExecution.next( value );
  }

  // Progride uma parada de ataque livre
  sockets.progressFreeAttackBreak = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores
    var { data: configData = {} } = config,
        { freeAttackName, value } = configData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( freeAttackName && typeof freeAttackName == 'string' );

    // Caso não haja uma parada de ataque livre com o mesmo nome da passada, re-executa função quando uma nova parada de ataque livre for gerada
    if( m.ActionAttack.freeAttackExecution?.name != freeAttackName )
      return m.events.breakStart.freeAttack.add( m.GameMatch.current, () =>
        setTimeout( sockets.progressFreeAttackBreak.bind( sockets, config ) ),
      { once: true } );

    // Progride parada de ataque livre
    m.ActionAttack.freeAttackExecution.next( value );
  }

  // Progride as etapas principais de uma parada de combate
  sockets.progressCombatBreakBody = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { combatBreakName = '', assignedPoints: newAssignedPoints } = configData,
        combatBreak = m.app.pixi.stage.getChildByName( combatBreakName, 2 );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( combatBreakName && typeof combatBreakName == 'string' );

    // Caso ainda não haja uma parada de combate com o nome passado, re-executa função quando uma nova parada de combate for gerada
    if( !combatBreak )
      return m.events.breakStart.combat.add( m.GameMatch.current, () =>
        setTimeout( sockets.progressCombatBreakBody.bind( sockets, config ) ),
      { once: true } );

    // Identificadores pós-validação
    var activeSections = Object.values( combatBreak.body.sections ).filter( section => section.isActive );

    // Itera por seções atualmente ativas da parada de combate
    for( let i = 0; i < activeSections.length; i++ ) {
      // Identificadores
      let activeSection = activeSections[ i ],
          sectionTitle = activeSection.title,
          currentAssignedPoints = Number( activeSection.body.assignedPoints.text ),
          pointsToAssign = newAssignedPoints[ i ] - currentAssignedPoints;

      // Habilita temporariamente seção alvo
      activeSection.isEnabled = true;

      // Atribui pontos passados à seção alvo
      for( let point = 0; point < Math.abs( pointsToAssign ); point++ )
        pointsToAssign > 0 ? combatBreak.body.addPoint( activeSection ) : combatBreak.body.removePoint( activeSection );

      // Se aplicável, atualiza texto suspenso do título da seção
      if( activeSection.updateTitleHoverText ) sectionTitle.hoverText = activeSection.updateTitleHoverText();

      // Desabilita seção alvo
      activeSection.isEnabled = false;
    }

    // Atualiza visualizador de dano visível
    combatBreak.updateDamagePreview();

    // Progride parada de combate
    combatBreak.currentExecution.next();
  }

  // Progride a delonga de uma parada de combate
  sockets.progressCombatBreakOvertime = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { combatBreakName = '' } = configData,
        combatBreak = m.app.pixi.stage.getChildByName( combatBreakName, 2 );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( combatBreakName && typeof combatBreakName == 'string' );

    // Caso ainda não haja uma parada de combate com o nome passado, re-executa função quando uma nova parada de combate for gerada
    if( !combatBreak )
      return m.events.breakStart.combat.add( m.GameMatch.current, () =>
        setTimeout( sockets.progressCombatBreakOvertime.bind( sockets, config ) ),
      { once: true } );

    // Identificadores pós-validação
    var { fateIcons } = combatBreak.footer,
        overtimeBar = m.app.pixi.stage.children.find( child => child.name == 'combat-break-overtime-awaiting-static-bar' );

    // Remove barra sobre espera na delonga, caso exista
    overtimeBar?.remove();

    // Reabilita rodapé da parada de combate
    combatBreak.footer.isEnabled = true;

    // Itera por paridades
    for( let parity of [ 'odd', 'even' ] ) {
      // Identificadores
      let isToUseFatePoint = Boolean( configData[ parity ] ),
          fateIcon = fateIcons[ parity ];

      // Se aplicável, atualiza indicador de se jogador alvo deve ou não usar pontos de destino
      if( isToUseFatePoint != fateIcon.isToUseFatePoint ) combatBreak.toggleFatePointUsage( fateIcon, true );
    }

    // Progride parada de combate
    combatBreak.currentExecution.next();
  }

  // Cancela uma parada de combate
  sockets.cancelCombatBreak = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores pré-validação
    var { data: configData = {} } = config,
        { combatBreakName = '', playerParity, actionIndex } = configData,
        combatBreak = m.app.pixi.stage.getChildByName( combatBreakName, 2 );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( combatBreakName && typeof combatBreakName == 'string' );
      m.oAssert( [ 'odd', 'even' ].includes( playerParity ) );
      m.oAssert( Number.isInteger( actionIndex ) && actionIndex >= 0 );
    }

    // Caso ainda não haja uma parada de combate com o nome passado, re-executa função quando uma nova parada de combate for gerada
    if( !combatBreak )
      return m.events.breakStart.combat.add( m.GameMatch.current, () =>
        setTimeout( sockets.cancelCombatBreak.bind( sockets, config ) ),
      { once: true } );

    // Identificadores pós-validação
    var attackAction = m.GameAction.currents[ actionIndex ];

    // Notifica usuário sobre decisão de cancelar o ataque
    m.noticeBar.show( m.languages.notices.getChoiceText( 'canceling-action', {
      playerParity: playerParity,
      action: attackAction
    } ) );

    // Remove parada de combate
    combatBreak.remove();

    // Cancela ataque
    attackAction.cancel();
  }

  // Progride uma parada de destino
  sockets.progressFateBreak = function ( config = {} ) {
    // Não executa função caso não exista mais uma partida ativa
    if( !m.GameMatch.current ) return;

    // Identificadores
    var { data: configData = {} } = config,
        { fateBreakName, value } = configData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( fateBreakName && typeof fateBreakName == 'string' );

    // Caso não haja uma parada de destino com o mesmo nome da passada, re-executa função quando uma nova parada de destino for gerada
    if( m.GameMatch.fateBreakExecution?.name != fateBreakName )
      return m.events.breakStart.fate.add( m.GameMatch.current, () =>
        setTimeout( sockets.progressFateBreak.bind( sockets, config ) ),
      { once: true } );

    // Progride parada de destino
    m.GameMatch.fateBreakExecution.next( value );
  }

  // Encerra atividades em caso de desligamento do servidor
  sockets.shutDown = function ( shutDownReason = 'error' ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ 'manual', 'error' ].includes( shutDownReason ) );

    // Não executa função caso usuário não esteja em uma partida real, e não esteja procurando uma
    if( !m.GameMatch.isInMatchmaking && ( !m.GameMatch.current || m.GameMatch.current.isSimulation ) ) return;

    // Força indicação de que usuário não está mais buscando uma partida
    m.GameMatch.isInMatchmaking = false;

    // Nulifica eventual partida atual
    m.GameMatch.current = null;

    // Notifica a usuário sobre encerramento das atividades, e recarrega a tela após seu consentimento
    new m.AlertModal( {
      text: m.languages.notices.getNetworkText( `server-shut-down-${ shutDownReason }` ),
      action: () => location.reload()
    } ).replace();
  }
}
