// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const assets = {};

/// Propriedades iniciais
defineProperties: {
  // Fontes
  assets.fonts = {
    // Source Sans Pro
    sourceSansPro: {
      // Tamanho padrão
      default: {
        // Nome da fonte
        name: 'Source Sans Pro',
        // Arquivo da fonte
        file: m.uri.game.fonts + 'source-sans-pro/source-sans-pro-16.fnt'
      },
      // Tamanho grande
      large: {
        // Nome da fonte
        name: 'Source Sans Pro Large',
        // Arquivo da fonte
        file: m.uri.game.fonts + 'source-sans-pro/source-sans-pro-22.fnt'
      }
    },
    // Preenchimento extra padrão nas extremidades de fontes
    padding: 2
  };

  // Atlas de texturas
  assets.textureAtlases = ( function () {
    // Identificadores
    var textureAtlases = [];

    // Define atlas de texturas existentes
    for( let i = 1; i <= 5; i++ ) {
      let textureAtlas = {};

      // Configurações do atlas de textura alvo

      /// Nome
      textureAtlas.name = `sprite-sheet-${i}`;

      /// Endereço
      textureAtlas.url = m.uri.game.textureAtlases + textureAtlas.name + '.json';

      // Inseri atlas alvo no conjunto de atlas de texturas
      textureAtlases.push( textureAtlas );
    }

    // Retorna conjunto de atlas de texturas
    return textureAtlases;
  } )();

  // Imagens
  assets.images = {
    // Ilustrações
    illustrations: {

    },
    // Ícones
    icons: {
      // De atividades
      activities: {
        inUse: 'in-use',
        active: 'active',
        suspended: 'suspended',
        withdrawn: 'withdrawn',
        ended: 'ended'
      },
      // De estados de preparo
      readiness: {
        unprepared: 'unprepared',
        prepared: 'prepared',
        occupied: 'occupied',
        exhausted: 'exhausted'
      },
      // De tipos primitivos
      primitiveTypes: {
        creature: 'creature',
        item: 'item',
        spell: 'spell'
      },
      // De estatísticas diversas
      stats: {
        modifier: 'bonus',
        penaltyModifier: 'penalty',
        penetration: 'penetration',
        recovery: 'recovery',
        range: 'range',
        conductivity: 'shock',
        mana: 'mana-points'
      },
      // De atributos
      attributes: {
        health: 'health',
        initiative: 'initiative',
        stamina: 'stamina',
        agility: 'agility',
        will: 'will',
        energy: 'energy',
        power: 'power'
      },
      // De raças
      races: {
        human: 'human',
        orc: 'orc',
        elf: 'elf',
        dwarf: 'dwarf',
        halfling: 'halfling',
        gnome: 'gnome',
        goblin: 'goblin',
        ogre: 'ogre',
        bear: 'bear',
        wolf: 'wolf',
        swarm: 'bee',
        undead: 'undead',
        salamander: 'salamander',
        golem: 'golem',
        tulpa: 'tulpa',
        spirit: 'spirit',
        demon: 'demon',
        angel: 'angel'
      },
      // De tipos de atuação
      roleTypes: {
        melee: 'melee',
        ranged: 'ranged',
        magical: 'magical'
      },
      // De níveis de experiência
      experienceLevels: {
        0: 'level-0',
        1: 'level-1',
        2: 'level-2',
        3: 'level-3',
        4: 'level-4'
      },
      // De equipagens
      equipages: {
        weapon: 'weapon',
        garment: 'garment',
        implement: 'implement'
      },
      // De tamanhos
      sizes: {
        null: 'null-size',
        small: 'small',
        medium: 'medium',
        large: 'large'
      },
      // De categorias de equipamentos
      itemsGrade: {
        ordinary: 'ordinary',
        masterpiece: 'masterpiece',
        artifact: 'artifact',
        relic: 'relic'
      },
      // De armas
      weapons: {
        unarmed: 'unarmed',
        mace: 'mace',
        sword: 'sword',
        axe: 'axe',
        spear: 'spear',
        shield: 'shield',
        sling: 'sling',
        bow: 'bow',
        crossbow: 'crossbow'
      },
      // De manobras
      maneuvers: {
        unarmed: 'unarmed',
        bash: 'blunt',
        cut: 'slash',
        thrust: 'pierce',
        shoot: 'shoot',
        throw: 'throw',
        scratch: 'scratch',
        bite: 'bite',
        swarm: 'swarm',
        burn: 'fire',
        fireBolt: 'fire',
        manaBolt: 'mana',
        rapture: 'rapture',
        repress: 'ether',
        dodge: 'agility',
        block: 'shield',
        repel: 'shield',
        cover: 'shield'
      },
      // De danos
      damages: {
        blunt: 'blunt',
        slash: 'slash',
        pierce: 'pierce',
        fire: 'fire',
        shock: 'shock',
        mana: 'mana',
        ether: 'ether'
      },
      // De sendas
      paths: {
        metoth: 'metoth',
        enoth: 'enoth',
        powth: 'powth',
        voth: 'voth',
        faoth: 'faoth'
      },
      // De polaridades
      polarities: {
        negative: 'negative',
        apolar: 'apolar',
        positive: 'positive'
      },
      // De durações
      durations: {
        immediate: 'immediate',
        sustained: 'sustained',
        persistent: 'persistent',
        permanent: 'permanent'
      },
      // De zonas de engajamento
      engagementZones: {
        human: 'human-background',
        orc: 'orc-background',
        elf: 'elf-background',
        dwarf: 'dwarf-background',
        halfling: 'halfling-background',
        gnome: 'gnome-background',
        goblin: 'goblin-background',
        ogre: 'ogre-background',
        melee: 'melee-symbol',
        ranged: 'ranged-symbol',
        magical: 'magical-symbol'
      },
      // De dados
      dices: {
        human: 'human-die',
        orc: 'orc-die',
        elf: 'elf-die',
        dwarf: 'dwarf-die',
        halfling: 'halfling-die',
        gnome: 'gnome-die',
        goblin: 'goblin-die',
        ogre: 'ogre-die',
        bear: 'bear-die',
        wolf: 'wolf-die',
        swarm: 'bee-die',
        undead: 'undead-die',
        salamander: 'salamander-die',
        golem: 'golem-die',
        spirit: 'spirit-die',
        demon: 'demon-die',
      },
      // De botões
      buttons: {
        roundGray: 'round-button-gray',
        roundRed: 'round-button-red',
        closeButton: 'close-button'
      },
      // Miscelânia
      misc: {
        arrow: 'arrow',
        arrowDouble: 'arrow-double',
        star: 'star',
        coin: 'coin',
        moves: 'moves',
        config: 'config',
        fatePoints: 'fate-points',
        fatePointsSymbol: 'fate-points-symbol'
      }
    }
  };

  // Áudios
  assets.audios = {
    // Músicas
    soundTracks: {},
    // Efeitos sonoros
    soundEffects: {},
    // Define volume inicial de áudios
    setStartingVolume: function ( audioVolume ) {
      // Identificadores
      var audioTypes = [ 'soundTracks', 'soundEffects' ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( audioTypes.every( key => audioVolume[ key ] >= 0 ) );

      // Itera por tipos de áudio
      for( let type of audioTypes ) {
        // Identificadores
        let targetAudios = Object.values( this[ type ] );

        // Atribui a áudio seu volume inicial
        for( let audio of targetAudios ) audio.volume = audioVolume[ type ];
      }
    }
  };

  // Captura músicas e efeitos sonoros
  ( function () {
    // Identificadores
    var audioTypes = [ 'track', 'effect' ];

    // Itera por tipos de áudio
    for( let type of audioTypes ) {
      // Identificadores
      let targetAudios = document.querySelectorAll( `audio[ data-type="${ type }" ]` ),
          targetProperty = 'sound' + type[ 0 ].toUpperCase() + type.slice( 1 ) + 's';

      // Registra áudios no objetivo de seu respectivo tipo
      for( let audio of targetAudios ) assets.audios[ targetProperty ][ audio.dataset.name ] = audio;
    }
  } )();
}
