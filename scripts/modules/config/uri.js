// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const uri = {};

/// Propriedades iniciais
defineProperties: {
  // Diretórios do jogo
  uri.game = {}

  // Propriedades de 'uri.game'
  gameURIs: {
    // Identificadores
    let gameURIs = uri.game;

    // Diretório raiz
    gameURIs.root = `${ location.protocol }//${ location.host }/game/`;

    // Diretório de fontes
    gameURIs.fonts = gameURIs.root + 'assets/fonts/bitmap/';

    // Diretório de atlas de texturas
    gameURIs.textureAtlases = gameURIs.root + 'assets/images/sprite-sheets/';
  }
}
