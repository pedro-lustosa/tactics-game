// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const app = {};

/// Propriedades iniciais
defineProperties: {
  // Canvas do jogo
  app.view = document.getElementById( 'game-canvas' );

  // Versão atual do jogo
  app.version = '0.1.2';

  // Medidas preferenciais do canvas do jogo
  app.defaultViewSize = {
    width: 1920, height: 960
  };

  // Identifica se jogo está em ambiente de desenvolvimento
  app.isInDevelopment = location.hostname.endsWith( '.local' );

  // Identifica se jogo está no modo informativo
  app.isInInfoMode = false;

  // Identifica se jogo deve omitir exibição na tela de mensagens sobre invalidações e afins
  app.isQuiet = false;

  // Indica se interatividade com o jogo deve ser bloqueada
  app.blockInteractionCount = {
    all: 0, keydown: 0
  };

  // Identifica teclas de controle pressionadas
  app.keysPressed = {
    ctrl: false, shift: false, alt: false
  };

  // Para controle de funções a serem executadas após certo intervalo
  app.timeoutDelay = {
    short: 25, average: 50, long: 500
  };

  // Indica se existe alguma operação programada para exibir um texto suspenso
  app.hoverTextTimeout = null;

  // Inicia Pixi do jogo
  app.init = function () {
    // Validação para execução do jogo

    /// Verifica se tamanho de tela do dispositivo atende requisitos mínimos
    if( screen.availWidth - ( window.outerWidth - window.innerWidth ) < 1240 || screen.availHeight - ( window.outerHeight - window.innerHeight ) < 620 ) {
      alert( m.languages.notices.getMiscText( 'small-screen' ) );
      throw new Error( 'User device does not have enough screen size.' );
    }

    /// Verifica se navegador suporta WebGL
    if( !PIXI.utils.isWebGLSupported() ) {
      alert( m.languages.notices.getMiscText( 'no-webgl' ) );
      throw new Error( 'User-agent lacks support for WebGL.' );
    }

    // Impede que mensagem sobre as configurações do Pixi seja exibida no console
    PIXI.utils.skipHello();

    // Inicia o Pixi
    app.pixi = new PIXI.Application( {
      view: app.view,
      width: app.defaultViewSize.width,
      height: app.defaultViewSize.height,
      resolution: window.devicePixelRatio,
      antialias: true
    } );

    // Define largura e altura máximas do canvas
    [ app.view.style.maxWidth, app.view.style.maxHeight ] = [ app.defaultViewSize.width * 1.25 + 'px', app.defaultViewSize.height * 1.25 + 'px' ];

    // Ajusta tamanho do canvas
    app.adjustViewSize();

    // Adiciona evento para ajuste do tamanho do canvas
    window.addEventListener( 'resize', app.adjustViewSize );

    // Adiciona eventos para atualizar estado do uso de teclas de controle
    for( let eventName of [ 'keydown', 'keyup' ] ) window.addEventListener( eventName, app.updateKeysPressed );

    // Adiciona evento para tratar configurações do aplicativo segundo sua visibilidade
    document.addEventListener( 'visibilitychange', app.handleVisibilityChange );

    // Adiciona evento para tratar configurações do aplicativo segundo perda de foco
    window.addEventListener( 'blur', app.handleBlurChange );

    // Impede propagação de eventos 'keydown', quando isso for desejado
    window.addEventListener( 'keydown', app.stopKeydownPropagation );

    // Configura Gsap

    /// Registro do plugin do Pixi
    gsap.registerPlugin( PixiPlugin );

    /// Registro do objeto do Pixi no plugin do Gsap
    PixiPlugin.registerPIXI( PIXI );

    // Executa configurações do socket a conectar usuário com o servidor
    m.sockets.init();

    // Executa configurações da barra de notificações
    m.noticeBar.init();

    // Caso ambiente do jogo seja o de desenvolvimento, concede acesso global a dados de configurações
    if( app.isInDevelopment )
      [ window.gameApp, window.gameData, window.gameAssets, window.gameEvents, window.gameLanguage ] = [ app, m.data, m.assets, m.events, m.languages ];

    // Renderiza primeira tela do jogo
    return m.GameScreen.begin();
  }

  // Ajusta tamanho do canvas
  app.adjustViewSize = function () {
    // A partir da relação entre a proporção da janela e do canvas, determina uma medida deste para ser igual a da janela e ajusta sua outra proporcionalmente
    [ app.view.style.width, app.view.style.height ] = window.innerWidth / window.innerHeight > app.view.width / app.view.height ?
                                                      [ 'auto', '100vh' ] : [ '100vw', 'auto' ];
  }

  // Mostra texto suspenso de elemento alvo
  app.showHoverText = function ( eventData ) {
    // Apenas executa função caso alvo tenha um texto suspenso
    if( !this.hoverText ) return false;

    // Programa montagem do texto suspenso
    var hoverTextTimeout = app.hoverTextTimeout = setTimeout( buildHoverText, 500, this );

    // Caso cursor tenha saído do elemento alvo, cancela montagem do texto suspenso
    this.once( 'mouseout', () => clearTimeout( hoverTextTimeout ) );

    // Monta texto suspenso de elemento alvo
    function buildHoverText( element ) {
      // Apenas executa função se elemento alvo não foi destruído
      if( element._destroyed ) return false;

      // Apenas executa função se elemento alvo ainda estiver visível
      for( let target = element; target; target = target.parent )
        if( !target.visible ) return false;

      // Identificadores
      var textContainer = new PIXI.Graphics(),
          textStyle = {
            fontName: m.assets.fonts.sourceSansPro.large.name,
            fontSize: element.hoverTextFontSize ?? -11,
            tint: m.data.colors.black,
            maxWidth: 300
          },
          hoverText = textContainer.bitmap = new PIXI.BitmapText( element.hoverText, textStyle ),
          hoverTextContainerParent = ( function () {
            // Caso elemento esteja na tela, define-a como seu ascendente
            for( let elementParent = element.parent; elementParent; elementParent = elementParent.parent )
              if( elementParent == m.GameScreen.current ) return m.GameScreen.current;

            // Do contrário, define estágio como ascendente
            return app.pixi.stage;
          } )();

      // Nomeação do contedor do texto suspenso
      textContainer.name = element.hoverTextName || 'hover-text';

      // Inserção de elementos

      /// Do texto suspenso em seu contedor
      textContainer.addChild( hoverText );

      /// Do contedor do texto suspenso em seu ascendente apropriado
      hoverTextContainerParent.addChild( textContainer );

      // Geração do plano de fundo do contedor
      textContainer.beginFill( m.data.colors.beige );
      textContainer.drawRect( 0, 0, textContainer.width + 8, textContainer.height + 4 );
      textContainer.endFill();

      // Posicionamentos

      /// Do texto
      hoverText.position.set( textContainer.width * .5 - hoverText.width * .5, textContainer.height * .5 - hoverText.height * .5 );

      /// Do contedor
      textContainerPosition: {
        // Captura delimitações globais de elemento detentor do texto suspenso
        let elementBounds = element.getBounds(),
            marginY = 4;

        // Posiciona contedor do texto suspenso
        textContainer.position.set(
          elementBounds.x + element.width * .5 - textContainer.width * .5, elementBounds.y - ( textContainer.height + marginY )
        );

        // Ajusta posição do contedor do texto suspenso
        positionAdjustment: {
          // Captura delimitações globais do contedor do texto
          let textContainerBounds = textContainer.getBounds();

          // Para caso contedor do texto suspenso seja horizontalmente truncado pelo início janela
          if( textContainerBounds.x < app.pixi.screen.x ) textContainer.x = app.pixi.screen.x

          // Para caso contedor do texto suspenso seja horizontalmente truncado pelo fim janela
          else if( textContainerBounds.x + textContainer.width > app.pixi.screen.width ) textContainer.x = app.pixi.screen.width - textContainer.width;

          // Para caso contedor do texto suspenso seja verticalmente truncado pelo início janela
          if( textContainerBounds.y < app.pixi.screen.y ) textContainer.y = elementBounds.y + element.height + marginY;
        }
      }

      // Evento para retirada do texto suspenso
      element.once( 'mouseout', app.removeHoverText.bind( app, textContainer ) );

      // Retorna contedor do texto suspenso
      return textContainer;
    }
  }

  // Remove texto suspenso alvo
  app.removeHoverText = function ( hoverTextContainer ) {
    // Retorna função caso texto suspenso alvo já tenha sido removido
    if( !hoverTextContainer.parent ) return false;

    // Remoção do texto suspenso, e sua destruição
    hoverTextContainer.parent.removeChild( hoverTextContainer ).destroy( { children: true } );
  }

  // Entra no modo informativo
  app.enterInfoMode = function () {
    // Validação
    if( this.isInDevelopment ) m.oAssert( !this.isInInfoMode );

    // Sinaliza início da entrada no modo informativo
    m.events.infoModeStart.enter.emit( m.GameScreen.current );

    // Indica que jogo está no modo informativo
    this.isInInfoMode = true;

    // Altera cursor para indicar modo informativo
    this.view.parentElement.style.cursor = 'help';

    // Sinaliza fim da entrada no modo informativo
    m.events.infoModeEnd.enter.emit( m.GameScreen.current );
  }

  // Abre seção do manual, caso se esteja no modo informativo
  app.openManualSection = function ( sectionName ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( sectionName && typeof sectionName == 'string' );

    // Apenas executa função caso se esteja no modo informativo
    if( !this.isInInfoMode ) return false;

    // Abre aba com seção do manual passada
    return window.open( `/manual/${ location.search }#${ sectionName }`, 'tactics-manual' );
  }

  // Sai do modo informativo
  app.leaveInfoMode = function () {
    // Validação
    if( this.isInDevelopment ) m.oAssert( this.isInInfoMode );

    // Sinaliza início da saída do modo informativo
    m.events.infoModeStart.leave.emit( m.GameScreen.current );

    // Restaura aparência do cursor
    this.view.parentElement.style.cursor = '';

    // Caso canvas em si tenha alterado o cursor para o do modo informativo, realtera-o para o de interação
    if( this.view.style.cursor == 'help' ) this.view.style.cursor = 'pointer';

    // Indica que jogo não está mais no modo informativo
    this.isInInfoMode = false;

    // Sinaliza fim da saída do modo informativo
    m.events.infoModeEnd.leave.emit( m.GameScreen.current );
  }

  // Bloqueia interação com o jogo
  app.blockInteraction = function () {
    // Validação
    if( this.isInDevelopment ) m.oAssert( this.blockInteractionCount.all >= 0 );

    // Indica que interatividade com o jogo está bloqueada
    this.blockInteractionCount.all++;

    // Altera cursor para indicar bloqueio, e desabilita eventos de clique
    [ this.view.parentElement.style.cursor, this.view.style.pointerEvents ] = [ 'wait', 'none' ];
  }

  // Desbloqueia interação com o jogo
  app.unblockInteraction = function () {
    // Validação
    if( this.isInDevelopment ) m.oAssert( this.blockInteractionCount.all > 0 );

    // Reverte uma instância indicando que interatividade com o jogo está bloqueada
    this.blockInteractionCount.all--;

    // Para caso não haja outros bloqueios para interação com o jogo
    if( !this.blockInteractionCount.all ) {
      // Restaura aparência do cursor
      this.view.parentElement.style.cursor = this.isInInfoMode ? 'help' : '';

      // Restaura interatividade com o canvas
      this.view.style.pointerEvents = '';
    };
  }

  // Envia requisição de Http
  app.sendHttpRequest = function ( config = {} ) {
    // Identificadores
    var { path, method = 'GET', headers = [], body = '', isToBlock = false, isSilentError = false, onEnd = null, onLoad = null, onError = null } = config;

    // Validação
    if( app.isInDevelopment ) {
      m.oAssert( path && typeof path == 'string' );
      m.oAssert( [ 'HEAD', 'GET', 'POST', 'PUT', 'PATCH' ].includes( method ) );
      m.oAssert( Array.isArray( headers ) );
      m.oAssert( headers.every( header =>
        Array.isArray( header ) && header.length == 2 && header.every( value => value && typeof value == 'string' )
      ) );
      m.oAssert( typeof body == 'string' );
      m.oAssert( [ isToBlock, isSilentError ].every( value => typeof value == 'boolean' ) );
      m.oAssert( [ onEnd, onLoad, onError ].every( value => !value || typeof value == 'function' ) );
    }

    // Caso ações do usuário devam ser bloqueadas durante requisição, realiza bloqueio
    if( isToBlock ) app.blockInteraction();

    // Retorna promessa com execução da requisição
    return new Promise( function ( resolve, reject ) {
      // Identificadores
      var request = new XMLHttpRequest();

      // Abre requisição
      request.open( method, path );

      // Adiciona eventuais cabeçalhos
      for( let header of headers ) request.setRequestHeader( header[ 0 ], header[ 1 ] );

      // Após conclusão da requisição, cumpre promessa, retornando, quando aplicável, função de conclusão passada
      request.addEventListener( 'load', () => resolve( onLoad?.( request ) ?? request ) );

      // Caso tenha ocorrido um erro na requisição, rejeita promessa, retornando, quando aplicável, função de erro passada
      request.addEventListener( 'error', () => reject( onError?.( request ) ?? request ) );

      // Caso tenha sido passada uma ação a se fazer ao fim de uma requisição bem-sucedida ou não, adiciona-a
      if( onEnd ) request.addEventListener( 'loadend', () => onEnd( request ) );

      // Caso tela tenha sido bloqueada, adiciona evento para a desbloquear
      if( isToBlock ) request.addEventListener( 'loadend', () => app.unblockInteraction() );

      // Caso requisição deva notificar usuário sobre eventual erro de rede, adiciona essa operação
      if( !isSilentError )
        request.addEventListener( 'error', () => m.noticeBar.show( m.languages.notices.getNetworkText( 'failed-request' ), 'lifted' ) );

      // Envia requisição
      request.send( body );
    } );
  }

  // Métodos não configuráveis de 'app'
  Object.defineProperties( app, {
    // Atualiza estado do uso de teclas de controle
    updateKeysPressed: {
      value: function ( event ) {
        // Identifica tecla de controle alvo – se alguma –, e retorna seu estado de uso
        switch( event.key ) {
          case 'Control':
            return app.keysPressed.ctrl = event.type == 'keydown' ? true : false;
          case 'Shift':
            return app.keysPressed.shift = event.type == 'keydown' ? true : false;
          case 'Alt':
            return app.keysPressed.alt = event.type == 'keydown' ? true : false;
        }
      }
    },
    // Trata configurações do aplicativo segundo sua visibilidade
    handleVisibilityChange: {
      value: function ( event ) {
        // Apenas executa função quando aplicativo tiver sido ocultado
        if( document.visibilityState != "hidden" ) return;

        // Identificadores
        var playingSoundTrack = Object.values( m.GameScreen.current.soundTracks ).find( audio => !audio.paused );

        // Para caso exista uma música sendo tocada
        if( playingSoundTrack ) {
          // Interrompe música
          playingSoundTrack.pause();

          // Adiciona evento para retomar música após aplicativo estar novamente visível
          document.addEventListener( 'visibilitychange', () => playingSoundTrack.play(), { once: true } );
        }

        // Indica que todas as teclas de controle não estão mais sendo pressionadas
        for( let key in app.keysPressed ) app.keysPressed[ key ] = false;
      }
    },
    // Trata configurações do aplicativo segundo perda de foco
    handleBlurChange: {
      value: function ( event ) {
        // Indica que todas as teclas de controle não estão mais sendo pressionadas
        for( let key in app.keysPressed ) app.keysPressed[ key ] = false;
      }
    },
    // Impedi propagação imediata de eventos 'keydown', quando isso for desejado
    stopKeydownPropagation: {
      value: function ( event ) {
        // Validação
        if( app.isInDevelopment ) m.oAssert( event.type == 'keydown' );

        // Bloqueio de eventos
        if( [ 'all', 'keydown' ].some( key => app.blockInteractionCount[ key ] ) ) return event.stopImmediatePropagation();
      }
    },
    // Controla exibição do menu de contexto do navegador, para que não conflite com a exibição de menus de contexto do jogo
    controlBrowserContextMenu: {
      value: function ( eventData = {} ) {
        // Identificadores
        var { type: eventType } = eventData;

        // Validação
        if( app.isInDevelopment ) m.oAssert( [ 'mouseover', 'mouseout' ].includes( eventType ) );

        // Identifica tipo do evento
        switch( eventType ) {
          case 'mouseover':
            // Havendo uma partida, impede exibição do menu de contexto do navegador
            if( m.GameMatch.current ) window.addEventListener( 'contextmenu', app.blockBrowserEvents );

            // Encerra operação
            break;
          case 'mouseout':
            // Remove evento para bloqueio do menu de contexto
            window.removeEventListener( 'contextmenu', app.blockBrowserEvents );
        }
      }
    },
    // Bloqueia eventos padrão do navegador
    blockBrowserEvents: {
      value: function ( event ) {
        // Bloqueio dos eventos padrão do navegador
        return event.preventDefault();
      }
    }
  } );
}
