// Módulos
import * as m from '../modules.js';

// Identificadores

/// Base do módulo
export const GameUser = function ( config = {} ) {
  // Iniciação de propriedades
  GameUser.init( this );

  // Identificadores
  var { userData } = config,
      { account } = this;

  // Configurações gerais

  /// Atribuição do nome
  this.name = userData.name;

  /// Atribuição da classificação
  this.rating = userData.ranking?.rating ?? this.rating;

  /// Atribuição do nome de exibição
  account.displayName = this.name == GameUser.globalUser ? m.languages.snippets.globalUser : this.name.oToProperNoun();

  /// Atribuição da foto de perfil do usuário
  account.photo = userData.photo;

  // Itera por dados de baralho do usuário
  for( let deckData of userData.decks ) {
    // Converte dados do baralho alvo em um baralho efetivo
    let deck = m.Deck.load( this, deckData );

    // Caso baralho seja um inicial, ajusta o idioma de seu nome de exibição
    if( deck.isStarter ) deck.displayName = m.languages.snippets.getPrebuiltDeck( deck.name ) || deck.displayName;

    // Adiciona baralho ao arranjo de baralhos do usuário
    this.decks.push( deck );
  }

  // Atribuição dos dados sobre partidas
  this.matches = userData.matches ?? this.matches;
}

/// Propriedades do construtor
defineProperties: {
  // Nome de usuário a apontar usuários não autenticados
  GameUser.globalUser = 'guest';

  // Usuário atual
  GameUser.current = null;

  // Iniciação de propriedades
  GameUser.init = function ( user ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( user instanceof GameUser );

    // Atribuição de propriedades iniciais

    /// Nome
    user.name = '';

    /// Indicador da habilidade do usuário
    user.rating = 1500;

    /// Baralhos
    user.decks = [];

    /// Informações da conta
    user.account = {};

    /// Informações sobre resultados de partidas do usuário
    user.matches = {};

    // Atribuição de propriedades de 'account'
    let account = user.account;

    /// Nome de exibição
    account.displayName = '';

    /// Foto de perfil
    account.photo = '';

    // Atribuição de propriedades de 'matches'
    let matches = user.matches;

    /// Total
    matches.total = 0;

    /// Vitórias
    matches.wins = 0;

    /// Empates
    matches.draws = 0;

    /// Derrotas
    matches.losses = 0;

    /// Incompletudes
    matches.unfinished = 0;
  }

  // Carrega dados do usuário acessando o jogo
  GameUser.loadData = async function () {
    // Carregamento dos dados do usuário
    var userData = await m.app.sendHttpRequest( {
      path: `/actions/load-user-data${ location.search }`,
      method: 'PATCH',
      headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
      isSilentError: true,
      onLoad: ( request ) => JSON.parse( request.response )
    } );

    // Retorno dos dados do usuário
    return userData;
  }
}
