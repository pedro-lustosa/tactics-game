// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ToggleableMenu = function ( config = {} ) {
  // Iniciação de propriedades
  ToggleableMenu.init( this );

  // Identificadores
  var { triggers = [], isSilent = false } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( triggers.length );
    m.oAssert( triggers.every( trigger => trigger instanceof PIXI.DisplayObject ) );
  }

  // Superconstrutor
  m.GameMenu.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos gatilhos de exibição do menu
  this.triggers = triggers;

  /// Interatividade do menu
  menuInteraction: {
    // Habilita cursor de botão para opções do menu
    for( let option in this.options ) this.options[ option ].buttonMode = true;

    // Itera por gatilhos do menu
    for( let trigger of this.triggers ) {
      // Ao se clicar no gatilho alvo, alterna exibição do menu
      trigger.addListener( 'click', this.toggle, this );

      // Se não explicitado o contrário, emite som de clique ao se clicar no gatilho alvo
      if( !isSilent ) trigger.addListener( 'click', () => m.assets.audios.soundEffects[ this.visible ? 'click-next' : 'click-previous' ].play(), this );
    }
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ToggleableMenu.init = function ( menu ) {
    // Chama 'init' ascendente
    m.GameMenu.init( menu );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( menu instanceof ToggleableMenu );

    // Atribuição de propriedades iniciais

    /// Nome
    menu.name = 'toggleable-menu';

    /// Gatilhos para alternar visibilidade do menu
    menu.triggers = [];

    /// Alterna exibição do menu
    menu.toggle = function () {
      // Alternação da exibição
      return this.visible = !this.visible;
    }

    // Oculta menu caso tenha se clicado fora de si e de seus gatilhos
    menu.disposeOnBlur = function ( eventData = {} ) {
      // Apenas executa função se menu estiver visível
      if( !this.visible ) return;

      // Identificadores
      var { target: currentTarget } = eventData,
          { triggers } = this,
          hoverText = m.GameScreen.current.children.find( child => child.name?.endsWith( this.name + '-hover-text' ) );

      // Itera por alvos clicados
      while( currentTarget ) {
        // Caso objeto clicado esteja no menu ou seja um de seus gatilhos, encerra função
        if( currentTarget == this || triggers.includes( currentTarget ) ) return;

        // Atualiza alvo atual
        currentTarget = currentTarget.parent;
      }

      // Remove texto suspenso do menu, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Oculta menu
      this.visible = false;
    }
  }
}

/// Propriedades do protótipo
ToggleableMenu.prototype = Object.create( m.GameMenu.prototype, {
  // Construtor
  constructor: { value: ToggleableMenu }
} );
