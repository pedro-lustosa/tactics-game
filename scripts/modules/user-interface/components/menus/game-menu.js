// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const GameMenu = function ( config = {} ) {
  // Identificadores
  var { name, options = {}, styleData = {}, isSubMenu = false } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( name || this.name );
    m.oAssert( Object.values( options ).every( option => typeof option.text == 'string' ) );
    m.oAssert( Object.values( options ).every( option =>
      typeof option.action == 'function' || !option.action && option.subMenu instanceof GameMenu
    ) );
    m.oAssert( Object.values( options ).every( option => !option.adornment || option.adornment instanceof PIXI.DisplayObject ) );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome da modal
  if( name ) this.name = name.endsWith( '-menu' ) ? name : name + '-menu';

  /// Ajusta configurações gerais do estilo do menu, se aplicável
  for( let key in styleData )
    if( key in this.styleData ) this.styleData[ key ] = styleData[ key ];

  /// Atribuição da identificação de se alvo é um submenu
  this.isSubMenu = Boolean( isSubMenu );

  /// Conteúdo do menu
  menuContent: {
    // Iteração pelas opções do menu
    for( let option in options ) {
      // Atribuição da opção alvo
      let currentOption = this.options[ option ] = this.addChild( new PIXI.Graphics() );

      // Identifica se opção está destacada
      currentOption.isHighlighted = false;

      // Atribuição do texto da opção
      currentOption.bitmap = currentOption.addChild( new PIXI.BitmapText( options[ option ].text, this.styleData.optionsText ) );

      // Para caso opção alvo tenha um adereço
      if( options[ option ].adornment ) {
        // Captura do adereço
        let optionAdornment = currentOption.adornment = options[ option ].adornment;

        // Renderização inicial do adereço
        this.drawAdornment( currentOption );

        // Ajuste da posição horizontal do adereço, para que aumente tamanho inato de sua opção
        optionAdornment.x = currentOption.width;

        // Inserção do adereço na opção alvo
        currentOption.addChild( optionAdornment );
      }

      // Para caso opção alvo tenha um submenu
      if( options[ option ].subMenu ) {
        // Identificadores
        let optionSubmenu = currentOption.subMenu = options[ option ].subMenu,
            optionArrow = currentOption.arrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ),
            colorFilter = new PIXI.filters.ColorMatrixFilter();

        // Vincula ao submenu opção que o revela
        optionSubmenu.triggerOption = currentOption;

        // Aplicação da cor preta ao filtro de cor
        colorFilter.brightness( 0 );

        // Configurações da seta indicadora do submenu

        /// Medidas
        optionArrow.oScaleByGreaterSize( Math.round( this.styleData.optionsText.fontSize * .6 ) );

        /// Iniciação do arranjo de filtros
        optionArrow.filters = [];

        // Adição do filtro de cor ao arranjo de filtros da seta
        optionArrow.filters.push( colorFilter );

        /// Ajuste da posição vertical da seta, para que não aumente tamanho de sua opção
        optionArrow.y = optionArrow.height;

        /// Inserção da seta na opção alvo
        currentOption.addChild( optionArrow );
      }
    }
  }

  /// Formatação do menu
  menuStyle: {
    // Identificadores
    let options = Object.values( this.options ),
        arrowMarginX = options.some( option => option.arrow ) ? this.styleData.optionArrowMarginX : 0,
        adornmentMarginX = options.some( option => option.adornment ) ? this.styleData.optionAdornmentMarginX : 0,
        menuContentWidth = this.styleData.menuContentWidth ||= arrowMarginX + adornmentMarginX + options.reduce(
          ( accumulator, current ) => accumulator.width < current.width ? current : accumulator, { width: 0 }
        ).width;

    // Itera por opções do menu
    for( let i = 0, previousOption; i < options.length; i++, previousOption = options[ i - 1 ] ) {
      // Renderiza opção alvo
      this.drawOption( options[ i ], false, menuContentWidth );

      // Posiciona verticalmente opção alvo
      options[ i ].y = previousOption ? previousOption.y + previousOption.height : 0;
    }

    // Por padrão, deixa menu oculto
    this.visible = false;
  }

  /// Interatividade do menu
  menuInteraction: {
    // Iteração pelas opções do menu
    for( let option in options ) {
      // Identificadores
      let currentOption = this.options[ option ];

      // Define opção como interativa
      currentOption.interactive = true;

      // Eventos

      /// Para caso opção tenha uma ação de clique vinculada
      if( options[ option ].action ) {
        // Atribui ação passada
        currentOption.addListener( 'click', options[ option ].action );

        // Se não explicitado o contrário, oculta menu após execução de sua ação
        if( !options[ option ].isToKeepAfterAction )
          currentOption.addListener( 'click', function () {
            // Oculta menu
            this.visible = false;

            // Desfaz destaque de sua opção
            this.drawOption( currentOption );
          }, this );

        // Se não explicitado o contrário, emite som ao clicar na opção alvo
        if( !options[ option ].isSilent )
          currentOption.addListener( 'click', () => m.assets.audios.soundEffects[ this.visible ? 'click-deny' : 'click-next' ].play(), this );
      }

      /// Para caso opção esteja vinculada a um submenu
      if( currentOption.subMenu ) {
        // Identificadores
        let subMenuOptions = Object.values( currentOption.subMenu.options );

        // Adiciona evento para mostrar o submenu
        currentOption.addListener( 'mouseover', this.showSubmenu.bind( this, currentOption ) );

        // Adiciona evento para ocultar o submenu
        for( let element of [ currentOption, ...subMenuOptions ] ) element.addListener( 'mouseout', this.hideSubmenu.bind( this, currentOption ) );
      }

      /// Para caso opção tenha um texto suspenso
      if( options[ option ].hoverText ) {
        // Atribui texto suspenso à opção
        currentOption.hoverText = options[ option ].hoverText;

        // Atribui nome do texto suspenso à opção
        currentOption.hoverTextName = this.name + '-hover-text';

        // Adiciona evento para exibição do texto suspenso da opção
        currentOption.addListener( 'mouseover', m.app.showHoverText );
      }

      /// Para destacar opção alvo, ao pairar cursor sobre ela
      currentOption.addListener( 'mouseover', () => this.drawOption( currentOption, true ), this );

      /// Para retirar destaque da opção alvo
      currentOption.addListener( 'mouseout', () => this.drawOption( currentOption ), this );
    }

    // No caso de supermenus, adiciona evento para remoção ou ocultamento do menu e de seus submenus, caso se tenha clicado em um objeto não lhe pertinente
    if( !isSubMenu ) m.GameScreen.current.addListener( 'click', this.disposeOnBlur, this );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GameMenu.init = function ( menu ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( menu instanceof GameMenu );

    // Atribuição de propriedades iniciais

    /// Nome
    menu.name = '';

    /// Opções
    menu.options = {};

    /// Dados de estilo
    menu.styleData = {};

    /// Indica se menu é um submenu
    menu.isSubMenu = false;

    /// Para submenus, opção do supermenu que o revela
    menu.triggerOption = null;

    /// Renderiza todo o menu
    menu.draw = function () {
      // Renderiza opções do menu
      for( let option in this.options ) this.drawOption( options[ option ] );
    }

    /// Renderiza uma opção do menu
    menu.drawOption = function ( targetOption, toHighlight = false, contentWidth = this.styleData.menuContentWidth ) {
      // Identificadores
      var [ optionText, optionAdornment, optionArrow ] = [ targetOption.bitmap, targetOption.adornment, targetOption.arrow ],
          { optionPaddingX, optionPaddingY } = this.styleData,
          backgroundColor = this.styleData[ toHighlight ? 'optionHighlightedBackgroundColor' : 'optionDefaultBackgroundColor' ],
          tintColor = m.data.colors.contrastWith( backgroundColor );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( this.options ).includes( targetOption ) );

      // Limpa renderização atual
      targetOption.clear();

      // Define novas medidas da opção
      let [ optionWidth, optionHeight ] = [ contentWidth + optionPaddingX, targetOption.height + optionPaddingY ];

      // Renderiza opção

      /// Confecção
      targetOption.beginFill( backgroundColor );

      /// Corporificação
      targetOption.drawRect( 0, 0, optionWidth, optionHeight );

      /// Encerramento
      targetOption.endFill();

      // Identifica se opção está destacada
      targetOption.isHighlighted = toHighlight;

      // Configura texto
      configText: {
        // Identificadores
        let { isToCenterTexts } = this.styleData,
            adornmentSpace = optionAdornment ? optionAdornment.width + this.styleData.optionAdornmentMarginX : 0;

        // Posiciona texto horizontalmente
        optionText.x = isToCenterTexts ? targetOption.width * .5 - optionText.width * .5 :
                                         optionPaddingX * .5 + adornmentSpace;

        // Posiciona texto verticalmente
        optionText.y = targetOption.height * .5 - optionText.height * .5;

        // Ajusta cor do texto
        optionText.tint = tintColor;
      }

      // Configura adereço
      configAdornment: {
        // Apenas executa bloco caso exista um adereço
        if( !optionAdornment ) break configAdornment;

        // Atualiza renderização do adereço
        this.drawAdornment( targetOption );

        // Posiciona adereço horizontalmente
        optionAdornment.x = optionText.x - optionAdornment.width - this.styleData.optionAdornmentMarginX;

        // Posiciona adereço verticalmente
        optionAdornment.y = targetOption.height * .5 - optionAdornment.height * .5;
      }

      // Configura seta
      configArrow: {
        // Apenas executa bloco caso exista uma seta
        if( !optionArrow ) break configArrow;

        // Define ativação do filtro de cor da seta segundo cor de contraste com plano de fundo
        optionArrow.filters[ 0 ].enabled = tintColor == m.data.colors.black;

        // Posiciona seta horizontalmente
        optionArrow.x = optionText.x + optionText.width + optionArrow.width + this.styleData.optionArrowMarginX;

        // Posiciona seta verticalmente
        optionArrow.y = targetOption.height * .5 + optionArrow.height * .5;
      }

      // Retorna opção
      return targetOption;
    }

    /// Mostra submenu da opção passada
    menu.showSubmenu = function ( triggerOption, eventData = {} ) {
      // Identificadores
      var { subMenu } = triggerOption,
          optionPosition = triggerOption.getGlobalPosition();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.visible );
        m.oAssert( subMenu instanceof GameMenu );
      }

      // Posiciona submenu

      /// Horizontalmente
      subMenu.position.x = ( subMenu.width < m.GameScreen.current.width - ( optionPosition.x + triggerOption.width ) ) ?
        optionPosition.x + triggerOption.width : optionPosition.x - subMenu.width;

      /// Verticalmente
      subMenu.position.y = ( subMenu.height < m.GameScreen.current.height - optionPosition.y ) ?
        optionPosition.y : m.GameScreen.current.height - ( subMenu.height + ( m.GameScreen.current.height - optionPosition.y ) );

      // Exibe submenu
      subMenu.visible = true;

      // Retorna submenu
      return subMenu;
    }

    /// Oculta submenu da opção passada
    menu.hideSubmenu = function ( triggerOption, eventData = {} ) {
      // Identificadores
      var { subMenu } = triggerOption,
          hoverText = m.GameScreen.current.children.find( child => child.name == subMenu.name + '-hover-text' );

      // Apenas executa função se submenu estiver visível
      if( !subMenu.visible ) return subMenu;

      // Apenas executa função caso cursor não esteja em uma posição na tela relevante para o submenu
      if( [ triggerOption, subMenu ].some( element => element.oCheckCursorOver() ) ) return false;

      // Remove texto suspenso do submenu, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Oculta submenu
      subMenu.visible = false;

      // Retorna submenu
      return subMenu;
    }

    // Atribuição de propriedades de 'styleData'
    let styleData = menu.styleData;

    /// Largura do conteúdo do menu
    styleData.menuContentWidth = 0;

    /// Preenchimento horizontal de opções
    styleData.optionPaddingX = 12;

    /// Preenchimento vertical de opções
    styleData.optionPaddingY = 8;

    /// Margem para adereço da opção do menu, quando existente
    styleData.optionAdornmentMarginX = 4;

    /// Margem para seta indicadora de submenu, para quando largura do menu for dinâmica e houver submenus
    styleData.optionArrowMarginX = 2;

    /// Cor padrão do plano de fundo de opções do menu
    styleData.optionDefaultBackgroundColor = m.data.colors.semiblack3;

    /// Cor de destaque do plano de fundo de opções do menu
    styleData.optionHighlightedBackgroundColor = m.data.colors.semiwhite1;

    /// Textos das opções
    styleData.optionsText = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -19 };

    /// Alinhamento horizontal dos textos do menu
    styleData.isToCenterTexts = true;
  }
}

/// Propriedades do protótipo
GameMenu.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: GameMenu }
} );
