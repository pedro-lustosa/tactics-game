// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionsMenu = function ( config = {} ) {
  // Iniciação de propriedades
  ActionsMenu.init( this );

  // Identificadores
  var { name, actions = [], options = {} } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( actions.length || Object.values( options ).length );
    m.oAssert( actions.every( action => action instanceof m.GameAction ) );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  if( name ) config.name = this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;

  /// Preparação das ações para serem inseridas no menu de ações
  setupMenuOptions: {
    // Cria objeto de opções de 'config', caso ainda não exista
    config.options ??= {};

    // Alfabeta ações
    actions.sort( ( a, b ) => m.languages.keywords.getAction( a.name ).localeCompare( m.languages.keywords.getAction( b.name ), m.languages.current ) );

    // Iteração pelas ações
    for( let action of actions ) {
      // Atribui ação atual ao objeto de opções
      let actionObject = config.options[ action.name.slice( action.name.indexOf( '-' ) + 1 ) ] = {};

      // Para caso ação seja 'atacar'
      if( action instanceof m.ActionAttack ) {
        // Identificadores
        let enabledAttacks = action.getEnabledAttacks();

        // Para caso não haja ataques disponíveis
        if( Object.values( enabledAttacks ).every( attacks => !attacks.length ) ) {
          // Remove ação alvo das opções de configuração
          delete config.options.attack;

          // Filtra ação alvo
          continue;
        }

        // Monta submenu de ataques habilitados para acionante
        actionObject.subMenu = new ActionsMenu( {
          name: 'attacks',
          options: ActionsMenu.setAttackOptions( this, action, enabledAttacks ),
          isSubMenu: true
        } );
      }

      // Para caso ação seja 'convocar'
      else if( action instanceof m.ActionSummon ) {
        // Identificadores
        let enabledSummons = action.getEnabledSummons();

        // Para caso não haja convocações disponíveis
        if( !Object.keys( enabledSummons ).length ) {
          // Remove ação alvo das opções de configuração
          delete config.options.summon;

          // Filtra ação alvo
          continue;
        }

        // Monta submenu de convocações habilitadas para acionante
        actionObject.subMenu = new ActionsMenu( {
          name: 'summons',
          options: ActionsMenu.setSummonOptions( this, action, enabledSummons ),
          isSubMenu: true
        } );
      }

      // Atribui texto da opção
      actionObject.text = m.languages.keywords.getAction( action.name );

      // Acrescenta adereço da opção
      actionObject.adornment = new PIXI.Graphics();

      // Registra forma do adereço
      actionObject.adornment.shapeName = this.getAdornmentShape( action );

      // Atribui função a ser executada ao se clicar na opção
      actionObject.action = this.executeAction.bind( this, action );

      // Indica que menu não deve ser ocultado automaticamente após executar sua ação
      actionObject.isToKeepAfterAction = true;

      // Indica que som de clique não deve ser emitido automaticamente após se clicar em uma opção
      actionObject.isSilent = true;

      // Monta texto suspenso da opção
      buildHoverText: {
        // Não executa bloco caso ação não tenha custo de fluxo e não seja 'canalizar'
        if( !action.flowCost && !( action instanceof m.ActionChannel ) ) break buildHoverText;

        // Identificadores
        let flowCostText = m.languages.keywords.getStat( 'flowCost' ),
            hoverText = flowCostText[ 0 ] + flowCostText.slice( 1 ).toLowerCase() + ': ';

        // Para caso ação seja 'canalizar'
        if( action instanceof m.ActionChannel ) {
          // Indica que custo de fluxo varia de acordo com magia escolhida
          hoverText += m.languages.snippets.chosenSpell.toLowerCase();

          // Captura eventual modificador final de custo de fluxo oriundo das condições do acionante
          let conditionModifiers = action.committer.conditions.map( condition => condition.getFlowCostModifier?.() ).filter( modifier => modifier ),
              finalModifier = conditionModifiers.reduce( ( accumulator, current ) => accumulator += current, 0 );

          // Caso haja um modificador de custo de fluxo, acrescenta essa informação
          if( finalModifier ) hoverText += ` + ${ finalModifier }`;
        }

        // Caso ação não seja 'canalizar', adiciona ao texto suspenso custo de fluxo da ação
        else hoverText += action.flowCost;

        // Caso ação seja uma sustentada, adiciona essa informação
        if( [ m.ActionExpel, m.ActionLeech ].some( constructor => action instanceof constructor ) )
          hoverText += ` (${ m.languages.keywords.getDuration( 'sustained' ).toLowerCase() })`;

        // Atribui texto suspenso para a opção
        actionObject.hoverText = hoverText;
      }
    }
  }

  // Superconstrutor
  m.GameMenu.call( this, config );

  // Adiciona menu à tela
  m.GameScreen.current.addChild( this );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionsMenu.init = function ( menu ) {
    // Chama 'init' ascendente
    m.GameMenu.init( menu );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( menu instanceof ActionsMenu );

    // Atribuição de propriedades iniciais

    /// Nome
    menu.name = 'actions-menu';

    /// Exibe menu de ações em posição específica
    menu.showAt = function ( position = {} ) {
      // Apenas executa função caso menu tenha uma ou mais opções
      if( !Object.keys( this.options ).length ) return false;

      // Posiciona menu

      /// Horizontalmente
      this.position.x = ( this.width < m.GameScreen.current.width - position.x ) ?
        position.x : m.GameScreen.current.width - this.width;

      /// Verticalmente
      this.position.y = ( this.height < m.GameScreen.current.height - position.y ) ?
        position.y : m.GameScreen.current.height - ( this.height + ( m.GameScreen.current.height - position.y ) );

      // Exibe menu
      this.visible = true;

      // Retorna menu
      return this;
    }

    /// Oculta menu de ações
    menu.hide = function () {
      // Identificadores
      var subMenuOptions = Object.values( this.options ).filter( option => option.subMenu ),
          hoverText = m.GameScreen.current.children.find( child => child.name?.endsWith( 'actions-menu-hover-text' ) );

      // Remove texto suspenso do menu ou de um de seus submenus, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Oculta submenus
      for( let option of subMenuOptions ) option.subMenu.visible = false;

      // Oculta menu
      this.visible = false;

      // Retorna menu
      return this;
    }

    /// Remove menu de ações caso se tenha clicado em um objeto que não suas opções com submenus
    menu.disposeOnBlur = function ( eventData = {} ) {
      // Identificadores
      var { target: currentTarget, type: eventType } = eventData,
          subMenuOptions = Object.values( this.options ).filter( option => option.subMenu ),
          hoverText = m.GameScreen.current.children.find( child => child.name?.endsWith( 'actions-menu-hover-text' ) );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'click', 'rightclick' ].includes( eventType ) );

      // Para caso haja opções com submenus e evento seja de clique
      if( subMenuOptions.length && eventType == 'click' ) {
        // Itera por alvos clicados
        while( currentTarget ) {
          // Caso objeto clicado seja uma opção do menu com um submenu, encerra função
          if( subMenuOptions.includes( currentTarget ) ) return;

          // Atualiza alvo atual
          currentTarget = currentTarget.parent;
        }
      }

      // Remove texto suspenso do menu ou de um de seus submenus, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Remove submenus da tela, e os destrói
      for( let option of subMenuOptions ) m.GameScreen.current.removeChild( option.subMenu ).destroy( { children: true } );

      // Remove evento de remoção do menu
      m.GameScreen.current.removeListener( 'click', this.disposeOnBlur, this );

      // Remove menu da tela, e o destrói
      m.GameScreen.current.removeChild( this ).destroy( { children: true } );
    }

    // Renderiza adereço relativo à opção passada
    menu.drawAdornment = function ( option ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.values( this.options ).some( menuOption => menuOption == option ) );
        m.oAssert( option.adornment instanceof PIXI.DisplayObject );
        m.oAssert( /^((circle-)|(square-))((empty)|(half)|(full))$/.test( option.adornment.shapeName ) );
      }

      // Identificadores
      var { adornment, adornment: { shapeName } } = option,
          color = m.data.colors[ option.isHighlighted ? 'white' : 'black' ],
          size = Math.abs( this.styleData.optionsText.fontSize * .6 );

      // Limpa renderização atual
      adornment.clear();

      // Renderiza adereço

      /// Caso adereço deva ser renderizado sem preenchimento, adiciona suas bordas; do contrário, adiciona seu preenchimento
      shapeName.endsWith( 'empty' ) ? adornment.lineStyle( 1, color, 1, 0 ) : adornment.beginFill( color );

      /// Corporifica forma segundo aparência indicada
      switch( shapeName ) {
        case 'circle-empty':
        case 'circle-full': {
          // Renderiza círculo
          adornment.drawCircle( 0, 0, size * .5 );

          // Ajusta pivô do círculo
          adornment.pivot.set( size * .5 * -1 );

          // Encerra operação
          break;
        }
        case 'square-empty':
        case 'square-full': {
          // Renderiza quadrado
          adornment.drawRect( 0, 0, size, size );

          // Encerra operação
          break;
        }
        case 'circle-half': {
          // Renderiza arco
          adornment.arc( 0, 0, size * .5, 0, Math.PI );

          // Ajusta pivô do arco
          adornment.pivot.set( size * .5 * -1, 0 );

          // Encerra operação
          break;
        }
        case 'square-half': {
          // Renderiza retângulo
          adornment.drawRect( 0, 0, size * .5, size );

          // Encerra operação
          break;
        }
      }

      /// Caso adereço deva ser renderizado sem preenchimento, finaliza-o
      if( !shapeName.endsWith( 'empty' ) ) adornment.endFill();

      // Retorna adereço
      return adornment;
    }

    // Retorna nome da forma através da que montar o adereço
    menu.getAdornmentShape = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.GameAction );

      // Identificadores
      var shapeName = '';

      // Inicia nome da forma segundo iteratividade da ação
      shapeName += action.iterativity == 'iterative' ? 'circle-' : 'square-';

      // Finaliza nome da forma
      shapeName += ( function () {
        // Identificadores
        var { committer, commitment } = action,
            matchFlow = m.GameMatch.current.flow;

        // Forma para caso período seja o da remobilização, acionante seja um humano, e menos de duas jogadas de humanos tenham sido feitas na paridade atual
        if( matchFlow.period instanceof m.RedeploymentPeriod && committer instanceof m.Human && matchFlow.parity.humanMovesCount < 2 ) return 'empty';

        // Define a forma segundo o acionamento da ação
        return commitment.type == 'free' ? 'empty' : commitment.subtype == 'partial' ? 'half' : 'full';
      } )();

      // Retorna nome da forma
      return shapeName;
    }

    // Executa uma ação do menu
    menu.executeAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.GameAction );
        m.oAssert( this.options[ action.name.replace( 'action-', '' ) ] );
      }

      // Identificadores
      var menuOption = this.options[ action.name.replace( 'action-', '' ) ];

      // Caso ação tenha um submenu e não se esteja no modo informativo, encerra função
      if( menuOption.subMenu && !m.app.isInInfoMode ) return;

      // Emite som de clique
      m.assets.audios.soundEffects[ 'click-next' ].play();

      // Caso modo seja o informativo, abre uma página no manual relativa à ação e encerra função
      if( m.app.isInInfoMode ) return m.app.openManualSection( action.name );

      // Executa ação
      ( action.currentExecution = action.execute() ).next();

      // Oculta menu
      return this.hide();
    }

    // Atribuição de propriedades de 'styleData'
    let styleData = menu.styleData;

    /// Largura do conteúdo do menu
    styleData.menuContentWidth = 0;

    /// Preenchimento horizontal de opções
    styleData.optionPaddingX = 12;

    /// Preenchimento vertical de opções
    styleData.optionPaddingY = 8;

    /// Margem para adereço da opção do menu, quando existente
    styleData.optionAdornmentMarginX = 4;

    /// Margem para seta indicadora de submenu, para quando largura do menu for dinâmica e houver submenus
    styleData.optionArrowMarginX = 2;

    /// Cor padrão do plano de fundo de opções do menu
    styleData.optionDefaultBackgroundColor = m.data.colors.semiwhite1;

    /// Cor de destaque do plano de fundo de opções do menu
    styleData.optionHighlightedBackgroundColor = m.data.colors.darkWineRed;

    /// Textos das opções
    styleData.optionsText = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -11 };

    /// Alinhamento horizontal dos textos do menu
    styleData.isToCenterTexts = false;
  }

  // Monta menu de ações
  ActionsMenu.build = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { target: card, type: eventType } = eventData,
        match = m.GameMatch.current;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventType == 'rightclick' );

    // Não executa função caso não haja uma partida atual
    if( !match ) return;

    // Não executa função caso fluxo não esteja suspenso
    if( !match.flow.isSuspended ) return;

    // Não executa função caso usuário não seja o controlador do fluxo
    if( m.GamePlayer.current != match.players.get( 'flow-controller' ) ) return;

    // Não executa função caso haja uma parada em andamento
    if( m.GameMatch.getActiveBreaks().length ) return;

    // Não executa função caso haja uma barra estática sendo exibida
    if( m.GameScreen.current.children.some( child => child.name?.endsWith( '-static-bar' ) && child.visible ) ) return;

    // Identificadores pós-validação
    var mousePositions = m.app.pixi.renderer.plugins.interaction.mouse.global,
        currentMenu = m.GameScreen.current.children.find( child => child.name?.endsWith( '-actions-menu' ) && !child.isSubMenu );

    // Caso já exista um menu de ações na tela, remove-o
    if( currentMenu ) currentMenu.disposeOnBlur( { target: card, type: eventType } );

    // Não continua função caso carta alvo não seja controlada pelo jogador alvo
    if( m.GamePlayer.current != card.getRelationships().controller ) return;

    // Não continua função caso carta alvo não tenha ações
    if( !card.actions.length ) return;

    // Monta e mostra menu de ações da carta alvo
    return new ActionsMenu( {
      name: card.name,
      actions: card.actions
    } ).showAt( mousePositions );
  }
}

/// Propriedades do protótipo
ActionsMenu.prototype = Object.create( m.GameMenu.prototype, {
  // Construtor
  constructor: { value: ActionsMenu }
} );

// Métodos não configuráveis de 'ActionsMenu'
Object.defineProperties( ActionsMenu, {
  // Define opções do submenu da ação 'atacar'
  setAttackOptions: {
    value: function ( menu, action, enabledAttacks ) {
      // Identificadores
      var menuOptions = {};

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( menu instanceof this );
        m.oAssert( action instanceof m.ActionAttack );
      }

      // Itera por fontes de manobras
      for( let rank in enabledAttacks ) {
        // Itera por manobras da fonte alvo
        for( let maneuver of enabledAttacks[ rank ] ) {
          // Identificadores
          let optionName = `${ maneuver.name }-${ rank }`,
              maneuverOrigin = enabledAttacks[ rank ].source;

          // Montagem da opção

          /// Criação
          menuOptions[ optionName ] = {};

          /// Definição do texto
          menuOptions[ optionName ].text = m.languages.keywords.getManeuver( maneuver.name );

          /// Definição da ação
          menuOptions[ optionName ].action = makeAttack.bind( this, menu, action, maneuver, action.committer.content.stats.effectivity.maneuvers[ rank ] );

          /// Definição de se menu deve ser ocultado automaticamente após a execução de sua ação
          menuOptions[ optionName ].isToKeepAfterAction = true;

          /// Definição de se som de clique deve ser emitido automaticamente após se clicar na opção
          menuOptions[ optionName ].isSilent = true;

          /// Havendo uma origem para a manobra, definição do texto suspenso
          if( maneuverOrigin ) menuOptions[ optionName ].hoverText =
            maneuverOrigin.content.designation.title + ' (' + (
              maneuverOrigin.isEmbedded ? m.languages.snippets.embedded : m.languages.snippets.getSomethingIn( maneuverOrigin.slot.name.replace( /^.+-/, '' ) )
            ) + ')';
        }
      }

      // Retorna opções do menu
      return menuOptions;

      // Executa ação 'atacar' via uma das opções do menu
      function makeAttack( menu, action, maneuver, maneuverSource ) {
        // Emite som de clique
        m.assets.audios.soundEffects[ 'click-next' ].play();

        // Caso modo seja o informativo, abre uma página no manual relativa à manobra e encerra função
        if( m.app.isInInfoMode ) return m.app.openManualSection( maneuver.name );

        // Define ataque da ação 'atacar'
        action.attack = {
          maneuver: maneuver, maneuverSource: maneuverSource
        };

        // Executa ação 'atacar'
        ( action.currentExecution = action.execute() ).next();

        // Oculta menu
        return menu.hide();
      }
    }
  },
  // Define opções do submenu da ação 'convocar'
  setSummonOptions: {
    value: function ( menu, action, enabledSummons ) {
      // Identificadores
      var menuOptions = {};

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( menu instanceof this );
        m.oAssert( action instanceof m.ActionSummon );
      }

      // Itera por convocações habilitadas
      for( let summonName in enabledSummons ) {
        // Identificadores
        let summonConstructor = enabledSummons[ summonName ];

        // Abre objeto da convocação
        menuOptions[ summonName ] = {};

        // Define texto da opção
        menuOptions[ summonName ].text = m.languages.keywords.getRace( summonName );

        // Define ação da opção
        menuOptions[ summonName ].action = summonTarget.bind( this, menu, action, summonConstructor );

        /// Define se menu deve ser ocultado automaticamente após a execução de sua ação
        menuOptions[ summonName ].isToKeepAfterAction = true;

        /// Define se som de clique deve ser emitido automaticamente após se clicar na opção
        menuOptions[ summonName ].isSilent = true;
      }

      // Retorna opções do menu
      return menuOptions;

      // Executa ação de convocação tendo como alvo construtor passado
      function summonTarget( menu, action, summonConstructor ) {
        // Emite som de clique
        m.assets.audios.soundEffects[ 'click-next' ].play();

        // Caso modo seja o informativo, abre uma página no manual relativa à ficha e encerra função
        if( m.app.isInInfoMode ) return m.app.openManualSection( getManualSectionId() );

        // Define ficha da ação 'convocar'
        action.token = new summonConstructor( { summoner: action.committer } );

        // Executa ação 'convocar'
        ( action.currentExecution = action.execute() ).next();

        // Oculta menu
        return menu.hide();

        // Retorna id da seção do manual relativa a uma ficha do construtor alvo
        function getManualSectionId() {
          // Retorna id da seção com base no nome do construtor
          switch( summonConstructor.name ) {
            case 'Bear':
            case 'Wolf':
            case 'Swarm':
              return 'base-stats-of-beasts';
            case 'Golem':
              return 'base-stats-of-golems';
            case 'GoldenSalamander':
            case 'IndigoSalamander':
              return 'base-stats-of-salamanders';
            case 'Undead':
              return 'base-stats-of-undeads';
            case 'Tulpa':
            case 'Spirit':
            case 'Demon':
            case 'Angel':
              return 'base-stats-of-astral-beings';
            default:
              return '';
          }
        }
      }
    }
  }
} );
