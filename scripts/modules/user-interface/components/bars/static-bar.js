// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const StaticBar = function ( config = {} ) {
  // Iniciação de propriedades
  StaticBar.init( this );

  // Identificadores
  var { name, text: barText, positionY = 44, isStageChild = false, isRemovable = false, removeHoverText, removeAction } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( !name || typeof name == 'string' );
    m.oAssert( barText && typeof barText == 'string' );
    m.oAssert( typeof positionY == 'number' && positionY >= 0 );
    m.oAssert( [ isStageChild, isRemovable ].every( value => typeof value == 'boolean' ) );
    m.oAssert( !removeHoverText || isRemovable && typeof removeHoverText == 'string' );
    m.oAssert( !removeAction || isRemovable && typeof removeAction == 'function' );
  }

  // Superconstrutor
  PIXI.Graphics.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  if( name ) this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;

  /// Atribuição do indicador de localização da barra
  this.isStageChild = isStageChild;

  /// Montagem da barra
  barBuilding: {
    // Identificadores
    let barTextStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16, maxWidth: 620 },
        bitmap = this.bitmap = new PIXI.BitmapText( barText, barTextStyle ),
        removeButton = this.removeButton = isRemovable ? new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.buttons.closeButton ] ) : null,
        components = [ bitmap, removeButton ].filter( component => component );

    // Inserção dos componentes
    this.addChild( ...components );

    // Encerra bloco caso não exista um botão de remoção
    if( !removeButton ) break barBuilding;

    // Configurações do botão de remoção

    /// Redimensionamento
    removeButton.oScaleByGreaterSize( 16 );

    /// Interatividade
    removeButton.buttonMode = removeButton.interactive = true;

    /// Para caso exista um texto suspenso
    if( removeHoverText ) {
      // Registra texto suspenso
      removeButton.hoverText = removeHoverText;

      // Nomeia texto suspenso
      removeButton.hoverTextName = 'static-bar-hover-text';

      // Adiciona evento para exibição do texto suspenso
      removeButton.addListener( 'mouseover', m.app.showHoverText );
    }

    /// Eventos

    //// Para emissão do som de clique
    removeButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

    //// Para remoção da barra
    removeButton.addListener( 'click', this.remove, this );

    //// Para execução da ação passada, se existente
    if( removeAction ) removeButton.addListener( 'click', removeAction );
  }

  /// Renderização da barra
  barDrawing: {
    // Identificadores
    let { bitmap, removeButton } = this,
        [ paddingX, paddingY ] = [ 8, 4 ],
        componentsMargin = 6,
        barWidth = this.bitmap.width + ( this.removeButton?.width ?? 0 ) + componentsMargin + paddingX * 2,
        barHeight = Math.max( this.bitmap.height, this.removeButton?.height ?? 0 ) + paddingY * 2;

    // Renderização do cabeçalho

    /// Confecção
    this.beginFill( m.data.colors.semiblack4 );

    /// Corporificação
    this.drawRoundedRect( 0, 0, barWidth, barHeight, 10 );

    /// Encerramento
    this.endFill();

    // Posicionamentos

    /// Do texto
    bitmap.position.set( paddingX, this.height * .5 - bitmap.height * .5 );

    /// Do botão de remoção, se existente
    removeButton?.position.set( bitmap.x + bitmap.width + componentsMargin, paddingY );

    /// Da barra
    this.position.set( m.app.pixi.screen.width * .5 - this.width * .5, positionY + 4 );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  StaticBar.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof StaticBar );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'static-bar';

    /// Texto
    component.bitmap = null;

    /// Botão de remoção da barra
    component.removeButton = null;

    /// Indica se barra deve ser inserida no estágio do Pixi ou na tela
    component.isStageChild = false;

    /// Inseri barra
    component.insert = function () {
      // Apenas executa função se barra não foi destruída
      if( this._destroyed ) return false;

      // Não executa função caso barra já esteja inserida
      if( this.parent ) return this;

      // Sinaliza início da inserção da barra
      m.events.staticBarChangeStart.insert.emit( this );

      // Identificadores
      var parentComponent = this.isStageChild ? m.app.pixi.stage : m.GameScreen.current,
          currentStaticBar = parentComponent.children.find( child => child.name?.endsWith( '-static-bar' ) && child.visible );

      // Caso já exista uma barra estática visível na tela, oculta-a
      if( currentStaticBar ) currentStaticBar.visible = false;

      // Inseri barra atual no ascendente alvo
      parentComponent.addChild( this );

      // Sinaliza fim da inserção da barra
      m.events.staticBarChangeEnd.insert.emit( this );

      // Retorna barra
      return this;
    }

    /// Remove barra
    component.remove = function () {
      // Apenas executa função se barra não foi destruída
      if( this._destroyed ) return false;

      // Não executa função caso barra não esteja inserida
      if( !this.parent ) return false;

      // Identificadores
      var wasVisible = this.visible,
          parentComponent = this.parent,
          hoverText = parentComponent.children.find( child => child.name == 'static-bar-hover-text' ),
          nextStaticBar = parentComponent.children.findLast( child => child.name?.endsWith( '-static-bar' ) && child != this );

      // Sinaliza início da remoção da barra
      m.events.staticBarChangeStart.remove.emit( this );

      // Remove texto suspenso da barra, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Remove barra, e a destrói
      parentComponent.removeChild( this ).destroy( { children: true } );

      // Sinaliza fim da remoção da barra
      m.events.staticBarChangeEnd.remove.emit( this );

      // Caso barra removida tenha estado visível e exista ao menos uma outra barra estática, revela-a
      if( wasVisible && nextStaticBar ) nextStaticBar.visible = true;
    }
  }
}

/// Propriedades do protótipo
StaticBar.prototype = Object.create( PIXI.Graphics.prototype, {
  // Construtor
  constructor: { value: StaticBar }
} );
