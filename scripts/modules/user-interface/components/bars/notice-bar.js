// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const noticeBar = document.getElementById( 'notice-bar' );

/// Propriedades iniciais
defineProperties: {
  // Etiqueta de texto da barra de notificações
  noticeBar.textTag = noticeBar.querySelector( '.text' );

  // Indica se existe alguma operação programada para ocultar barra de notificações
  noticeBar.hidingTimeout = null;

  // Indica classes aplicáveis para adição à barra de notificações
  noticeBar.allowedClasses = [ 'lifted', 'red', 'ocher', 'green', 'fast' ];

  // Configurações iniciais da barra de notificações
  noticeBar.init = function () {
    // Adiciona evento para cancelar uma eventual operação programada para ocultar barra de notificações
    noticeBar.addEventListener( 'animationstart', noticeBar.cancelHidingAnimation );

    // Adiciona evento para remover animação da barra de notificações após ela ter ocorrido
    noticeBar.addEventListener( 'animationend', noticeBar.removeAnimation );

    // Adiciona evento para programar ocultamento da barra de notificações
    noticeBar.addEventListener( 'animationend', noticeBar.scheduleHidingAnimation );
  }

  // Mostra barra de notificações
  noticeBar.show = function ( text, ...classNames ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( typeof text == 'string' );
      m.oAssert( classNames.every( className => noticeBar.allowedClasses.includes( className ) ) );
    }

    // Não executa função se configuração 'isQuiet' estiver ativada
    if( m.app.isQuiet ) return false;

    // Redefine barra de notificações
    noticeBar.reset();

    // Atribui à barra texto passado
    noticeBar.textTag.textContent = text;

    // Atribui à barra classes passadas
    noticeBar.classList.add( ...classNames );

    // Força eventual reinício da animação
    void noticeBar.offsetWidth;

    // Executa animação
    noticeBar.classList.add( 'fade-in' );
  }

  // Oculta barra de notificações
  noticeBar.hide = function ( isToAnimate = false ) {
    // Não executa função caso barra já esteja oculta
    if( window.getComputedStyle( noticeBar ).opacity == 0 ) return false;

    // Para caso ocultamento deva ocorrer imediatamente, redefine barra de notificações
    if( !isToAnimate ) noticeBar.reset()

    // Do contrário
    else {
      // Define direção da próxima animação
      noticeBar.style.animationDirection = 'reverse';

      // Executa animação
      noticeBar.classList.add( 'fade-in' );
    }
  }

  // Redefine barra de notificações
  noticeBar.reset = function () {
    // Encerra animação que possa estar em curso
    noticeBar.classList.remove( 'fade-in' );

    // Redefine propriedades de estilo da barra
    for( let property of [ 'opacity', 'animationDirection', 'zIndex' ] ) noticeBar.style[ property ] = '';

    // Remove classes aplicáveis à barra
    noticeBar.classList.remove( ...noticeBar.allowedClasses );
  }

  // Após certo período, oculta barra de notificações
  noticeBar.scheduleHidingAnimation = function () {
    // Identificadores
    var timer = noticeBar.classList.contains( 'fast' ) ? 500 : 3000;

    // Não executa função caso barra já esteja oculta
    if( window.getComputedStyle( noticeBar ).opacity == 0 ) return false;

    // Programa ocultamento da barra
    return noticeBar.hidingTimeout = setTimeout( noticeBar.hide, timer, true );
  }

  // Cancela operação programada para ocultar barra de notificações
  noticeBar.cancelHidingAnimation = function () {
    // Apenas executa função caso exista uma operação de ocultamento programada
    if( !noticeBar.hidingTimeout ) return;

    // Cancela operação programada
    clearTimeout( noticeBar.hidingTimeout );

    // Nulifica operação programada
    noticeBar.hidingTimeout = null;
  }

  // Remove animação da barra de notificações
  noticeBar.removeAnimation = function () {
    // Salva opacidade e índice Z acarretados pela animação
    for( let property of [ 'opacity', 'zIndex' ] ) noticeBar.style[ property ] = window.getComputedStyle( noticeBar )[ property ];

    // Remove classe de animação
    noticeBar.classList.remove( 'fade-in' );
  }
}
