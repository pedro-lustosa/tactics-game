// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManualProgressBar = function ( config = {} ) {
  // Iniciação de propriedades
  ManualProgressBar.init( this );

  // Identificadores
  var { startDegree = 1, action } = config,
      { container: barContainer, container: { bar: barBody, button: barButton } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( typeof action == 'function' );

  // Superconstrutor
  m.ProgressBar.call( this, config );

  // Configurações pós-superconstrutor

  /// Formatação da barra
  barStyle: {
    // Confecção do botão da barra

    /// Cor de preenchimento
    barButton.beginFill( this.styleData.buttonColor );

    /// Renderização
    barButton.drawCircle( 0, 0, this.styleData.barHeight * .5 );

    /// Encerramento
    barButton.endFill();

    // Define pontos a partir do que barra deve, respectivamente, estar completamente vazia ou cheia
    [ barBody.leftOffset, barBody.rightOffset ] = [ barButton.width * .5, this.styleData.barWidth - barButton.width * .5 ];

    // Captura preenchimento ajustado da barra
    let fillWidth = startDegree * ( barBody.rightOffset - barBody.leftOffset ) + barBody.leftOffset;

    // Ajusta largura do preenchimento para que esteja dentro dos limites de sua barra

    /// Limite esquerdo
    if( fillWidth <= barBody.leftOffset ) fillWidth = 0;

    /// Limite direito
    if( fillWidth >= barBody.rightOffset ) fillWidth = this.styleData.barWidth;

    // Renderiza novamente barra, para que leve em consideração limites de renderização
    barBody.draw( fillWidth, barBody.degree );

    // Posiciona botão
    barButton.position.set( Math.max( Math.min( barBody.x + fillWidth, barBody.rightOffset ), barBody.leftOffset ), barButton.height * .5 );
  }

  /// Interatividade da barra
  barInteraction: {
    // Torna botão interativo
    barButton.buttonMode = barButton.interactive = true;

    // Eventos

    /// Sinalizador de início do deslocamento do botão
    barButton.addListener( 'mousedown', () => barButton.isMoving = true );

    /// Atualização da renderização da barra
    barButton.addListener( 'mousemove', barContainer.updateDrawing, barContainer );

    /// Ação desejada
    barButton.addListener( 'mousemove', eventData => barButton.isMoving ? action( barBody.degree, eventData ) : null );

    /// Sinalizador de fim do deslocamento do botão
    for( let eventName of [ 'mouseup', 'mouseout' ] ) barButton.addListener( eventName, () => barButton.isMoving = false );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManualProgressBar.init = function ( component ) {
    // Chama 'init' ascendente
    m.ProgressBar.init( component );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof ManualProgressBar );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'manual-progress-bar';

    // Atribuição de propriedades de 'container'
    let container = component.container;

    /// Botão de controle do progresso
    container.button = container.addChild( new PIXI.Graphics() );

    /// Atualiza renderização da barra
    container.updateDrawing = function ( event ) {
      // Apenas executa função caso botão da barra esteja sendo movido
      if( !this.button.isMoving ) return;

      // Apenas executa função caso botão da barra esteja sendo movido horizontalmente
      if( !event.data.originalEvent.movementX ) return;

      // Identificadores
      var fillWidth = this.button.x + event.data.originalEvent.movementX,
          { leftOffset, rightOffset } = this.bar;

      // Ajusta largura do preenchimento para que esteja dentro dos limites de sua barra

      /// Limite esquerdo
      if( fillWidth <= leftOffset ) fillWidth = 0;

      /// Limite direito
      if( fillWidth >= rightOffset ) fillWidth = component.styleData.barWidth;

      // Atualiza posição horizontal do botão
      this.button.x = Math.max( Math.min( this.bar.x + fillWidth, rightOffset ), leftOffset );

      // Atualiza renderização do corpo da barra de progresso
      return this.bar.draw( fillWidth, ( this.button.x - leftOffset ) / ( rightOffset - leftOffset ) );
    }

    // Atribuição de propriedades de 'container.bar'
    let bar = container.bar;

    /// Ponto a partir do que barra deve estar completamente vazia, para que se harmonize com seu botão
    bar.leftOffset = 0;

    /// Ponto a partir do que barra deve estar completamente cheia, para que se harmonize com seu botão
    bar.rightOffset = 0;

    // Atribuição de propriedades de 'container.button'
    let button = container.button;

    /// Indica se botão está sendo movido ou não
    button.isMoving = false;

    // Atribuição de propriedades de 'styleData'
    let styleData = component.styleData;

    /// Cor do botão da barra de progresso
    styleData.buttonColor = m.data.colors.darkRed;
  }
}

/// Propriedades do protótipo
ManualProgressBar.prototype = Object.create( m.ProgressBar.prototype, {
  // Construtor
  constructor: { value: ManualProgressBar }
} );
