// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const AutomaticProgressBar = function ( config = {} ) {
  // Iniciação de propriedades
  AutomaticProgressBar.init( this );

  // Superconstrutor
  m.ProgressBar.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  AutomaticProgressBar.init = function ( component ) {
    // Chama 'init' ascendente
    m.ProgressBar.init( component );
    
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof AutomaticProgressBar );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'automatic-progress-bar';
  }
}

/// Propriedades do protótipo
AutomaticProgressBar.prototype = Object.create( m.ProgressBar.prototype, {
  // Construtor
  constructor: { value: AutomaticProgressBar }
} );
