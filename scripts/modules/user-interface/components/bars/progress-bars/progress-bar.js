// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ProgressBar = function ( config = {} ) {
  // Identificadores
  var { name, caption, startDegree = 1, isToShowDegree = false, styleData = {} } = config,
      { container: barContainer, container: { bar: barBody } } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ name, caption ].every( value => !value || typeof value == 'string' ) );
    m.oAssert( !isToShowDegree || caption );
    m.oAssert( startDegree >= 0 && startDegree <= 1 );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  if( name ) this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;

  /// Atribuição do indicativo de se grau de progresso da barra deve ser mostrado
  barBody.isToShowDegree = Boolean( isToShowDegree );

  /// Ajusta configurações do estilo da barra, se aplicável
  for( let key in styleData )
    if( key in this.styleData ) this.styleData[ key ] = styleData[ key ];

  /// Conteúdo da barra
  barContent: {
    // Caso tenha sido definido um título, gera-o e o adiciona à barra
    if( caption ) this.caption = this.addChild( new PIXI.BitmapText( caption, this.styleData.textStyle ) );

    // Inserção do contedor da barra
    this.addChild( barContainer );
  }

  /// Formatação da barra
  barStyle: {
    // Identificadores
    let { caption: barCaption } = this;

    // Renderização inicial da barra
    barBody.draw( startDegree * this.styleData.barWidth );

    // Caso haja um título, ajusta posição vertical do contedor da barra
    if( barCaption ) barContainer.y = barCaption.y + barCaption.height + 8;

    // Posicionamento horizontal do título e da barra de progresso
    for( let component of [ barCaption, barBody ] )
      if( component ) component.x = barContainer.width * .5 - component.width * .5;
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ProgressBar.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof ProgressBar );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'progress-bar';

    /// Título
    component.caption = null;

    /// Contedor da barra de progresso
    component.container = new PIXI.Container();

    /// Dados de estilo
    component.styleData = {};

    // Atribuição de propriedades de 'container'
    let container = component.container;

    /// Barra de progresso
    container.bar = container.addChild( new PIXI.Graphics() );

    // Atribuição de propriedades de 'container.bar'
    let bar = container.bar;

    /// Grau de progresso
    bar.degree = 0;

    /// Indica se grau de progresso deve ser mostrado
    bar.isToShowDegree = false;

    /// Renderiza corpo da barra de progresso
    bar.draw = function( fillWidth = 0, newDegree ) {
      // Identificadores
      var { barWidth, barHeight, barFillColor, barEmptyColor, barBorderColor, barBorderRadius } = component.styleData,
          borderSize = 1;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( fillWidth >= 0 && fillWidth <= barWidth );
        m.oAssert( newDegree >= 0 && newDegree <= 1 || typeof newDegree == 'undefined' );
      }

      // Limpa renderização atual da barra
      this.clear();

      // Confecção da nova barra

      /// Bordas

      //// Estilo
      this.lineStyle( borderSize, barBorderColor, 1, 1 );

      //// Renderização
      this.drawRoundedRect( 0, 0, barWidth, barHeight, barBorderRadius );

      //// Zeramento, para que não sejam usadas também na renderização dos preenchimentos
      this.lineStyle();

      /// Espaço preenchido

      //// Plano de fundo
      this.beginFill( barFillColor );

      //// Renderização
      this.drawRoundedRect( 0, 0, fillWidth, barHeight, barBorderRadius );

      //// Encerramento
      this.endFill();

      /// Espaço vazio

      //// Plano de fundo
      this.beginFill( barEmptyColor );

      //// Renderização
      this.drawRoundedRect( fillWidth, 0, barWidth - fillWidth, barHeight, barBorderRadius );

      //// Encerramento
      this.endFill();

      // Caso novo grau de progresso não tenha sido passado, define-o de modo que corresponda com área preenchida da barra
      newDegree ??= fillWidth / barWidth;

      // Atualiza grau de progresso da barra
      this.setDegree( newDegree );

      // Retorna barra
      return this;
    }

    /// Define grau de progresso da barra
    bar.setDegree = function ( newDegree = 0 ) {
      // Identificadores
      var { caption: barCaption } = component,
          { barWidth } = component.styleData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( newDegree >= 0 && newDegree <= 1 );

      // Atualiza grau da barra
      this.degree = newDegree;

      // Se aplicável, mostra valor atual do grau de progresso
      if( barCaption && this.isToShowDegree ) barCaption.text = barCaption.text.replace( /: \d+$/, '' ) + `: ${ Math.round( this.degree * 100 ) }`;

      // Retorna barra
      return this;
    }

    // Atribuição de propriedades de 'styleData'
    let styleData = component.styleData;

    /// Estilo do título da barra de progresso
    styleData.textStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 };

    /// Largura da barra de progresso
    styleData.barWidth = 120;

    /// Altura da barra de progresso
    styleData.barHeight = 12;

    /// Cor da parte preenchida da barra de progresso
    styleData.barFillColor = m.data.colors.orange;

    /// Cor da parte vazia da barra de progresso
    styleData.barEmptyColor = m.data.colors.black;

    /// Cor da borda da barra de progresso
    styleData.barBorderColor = m.data.colors.semiwhite2;

    /// Grau de arredondamento da borda da barra de progresso
    styleData.barBorderRadius = 0;
  }
}

/// Propriedades do protótipo
ProgressBar.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: ProgressBar }
} );
