// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const CardsList = function ( config = {} ) {
  // Iniciação de propriedades
  CardsList.init( this );

  // Identificadores
  var { name, type, commander } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'command', 'creature', 'item', 'spell' ].includes( type ) );
    m.oAssert( type == 'command' || commander?.content?.stats?.command );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  this.name = ( name ?? '' ) + `${ type }-cards-list`;

  /// Atribuição do tipo
  this.type = type;

  /// Geração das cartas segundo tipo de lista
  buildCards: {
    // Identificadores
    let scopes = commander?.content.stats.command.scopes,
        scopeRaces = scopes?.creature.total.max ?
          Object.keys( scopes.creature.race ).filter( race => scopes.creature.race[ race ].max ) : [],
        scopeExperienceLevels = scopes?.creature.total.max ?
          Object.keys( scopes.creature.experienceLevel ).filter( level => scopes.creature.experienceLevel[ level ].max ).map( level => Number( level ) ) : [];

    // Identificação do tipo de lista
    switch( type ) {
      case 'command': {
        // Identificadores
        let supportedCommanders = [ m.CommanderGaspar, m.CommanderIxohch ];

        // Itera por construtores de comandantes
        for( let cardConstructor of m.data.cards.commanders ) {
          // Caso ambiente não seja o de desenvolvimento, filtra cartas que ainda não foram implementadas
          if( !m.app.isInDevelopment && !supportedCommanders.includes( cardConstructor ) ) continue;

          // Adiciona carta à lista
          this.cards.push( new cardConstructor() );
        }

        // Encerra operação
        break;
      }
      case 'creature': {
        // Apenas adiciona cartas de criaturas caso escopo as abarque
        if( !scopes.creature.total.max ) break;

        // Itera por construtores de criaturas
        for( let cardConstructor of m.data.cards.creatures ) {
          // Filtra construtores cujo protótipo não seja uma instância de humanoides
          if( !( cardConstructor.prototype instanceof m.Humanoid ) ) continue;

          // Identificadores
          let sampleCard = new cardConstructor(),
              [ cardRace, cardRoleType ] = [ sampleCard.content.typeset.race, sampleCard.content.typeset.role?.type ];

          // Caso ambiente não seja o de desenvolvimento, filtra cartas que ainda não foram implementadas
          if( !m.app.isInDevelopment && sampleCard.coins.base >= 9999 ) continue;

          // Filtra fichas
          if( sampleCard.isToken ) continue;

          // Filtra raças fora do escopo
          if( !scopeRaces.includes( cardRace ) ) continue;

          // Filtra atuações fora do escopo
          if( !scopes.creature.roleType[ cardRoleType ].max ) continue;

          // Itera por níveis de experiência abarcados pelo escopo
          for( let level of scopeExperienceLevels ) {
            // Identificadores
            let currentCard = new cardConstructor( { level: level } );

            // Filtra cartas que não atendem restrições especiais, se existentes
            if( scopes.creature.total.constraint.description && !scopes.creature.total.constraint.check( currentCard ) ) continue;

            // Adiciona carta à lista
            this.cards.push( currentCard );
          }
        }

        // Encerra operação
        break;
      }
      case 'item': {
        // Apenas adiciona cartas de equipamentos caso escopo os abarque
        if( !scopes.item.total.max ) break;

        // Identificadores
        let [ scopeSizes, commanderSize ] = [ [], commander.content.typeset.size ],
            sizesByRace = {
              small: [ 'halfling', 'gnome', 'goblin' ],
              medium: [ 'human', 'orc', 'elf', 'dwarf' ],
              large: [ 'ogre' ]
            };

        // Identifica tamanhos suportados pelo escopo

        /// Pelo das raças
        for( let size in sizesByRace ) {
          if( scopeRaces.some( race => sizesByRace[ size ].includes( race ) ) ) scopeSizes.push( size );
        }

        /// Pelo do comandante
        if( commanderSize && !scopeSizes.includes( commanderSize ) ) scopeSizes.push( commanderSize );

        /// Ordena tamanhos
        scopeSizes.sort( function ( a, b ) {
          let aSize = a == 'small' ? 1 : a == 'medium' ? 2 : 3,
              bSize = b == 'small' ? 1 : b == 'medium' ? 2 : 3;

          return aSize - bSize;
        } );

        // Itera por construtores de equipamentos
        for( let cardConstructor of m.data.cards.items ) {
          // Identificadores
          let sampleCard = new cardConstructor(),
              { equipage: cardEquipage, size: cardSize, grade: cardGrade } = sampleCard.content.typeset;

          // Caso ambiente não seja o de desenvolvimento, filtra cartas que ainda não foram implementadas
          if( !m.app.isInDevelopment && sampleCard.coins.base >= 9999 ) continue;

          // Filtra categorias fora do escopo, se não advirem de armas mundanas
          if( ( cardEquipage != 'weapon' || [ 'artifact', 'relic' ].includes( cardGrade ) ) && !scopes.item.grade[ cardGrade ].max ) continue;

          // Para equipamentos sem tamanho
          if( !cardSize ) {
            // Filtra cartas que não atendem restrições especiais, se existentes
            if( scopes.item.total.constraint.description && !scopes.item.total.constraint.check( sampleCard ) ) continue;

            // Adiciona carta à lista
            this.cards.push( sampleCard );
          }
          // Para equipamentos com tamanho
          else {
            // Para relíquias
            if( cardGrade == 'relic' ) {
              // Filtra tamanhos não abarcados pelo escopo
              if( !scopeSizes.includes( cardSize ) ) continue;

              // Filtra cartas que não atendem restrições especiais, se existentes
              if( scopes.item.total.constraint.description && !scopes.item.total.constraint.check( sampleCard ) ) continue;

              // Adiciona carta à lista
              this.cards.push( sampleCard );
            }
            // Para armas mundanas
            else if( cardEquipage == 'weapon' && [ 'ordinary', 'masterpiece' ].includes( cardGrade ) ) {
              // Itera por tamanhos do escopo
              for( let size of scopeSizes ) {
                // Filtra armas largas não existentes
                if( size == 'large' && [ 'sword', 'bow', 'crossbow' ].includes( sampleCard.content.typeset.role.name ) ) continue;

                // Itera por categorias mundanas
                for( let grade of [ 'ordinary', 'masterpiece' ] ) {
                  // Filtra categorias não abarcadas pelo escopo
                  if( !scopes.item.grade[ grade ].max ) continue;

                  // Gera carta com categoria e tamanho alvos
                  let currentCard = new cardConstructor( { size: size, grade: grade } );

                  // Filtra cartas que não atendem restrições especiais, se existentes
                  if( scopes.item.total.constraint.description && !scopes.item.total.constraint.check( currentCard ) ) continue;

                  // Adiciona carta à lista
                  this.cards.push( currentCard );
                }
              }
            }
            // Para os demais equipamentos
            else {
              // Itera por tamanhos do escopo
              for( let size of scopeSizes ) {
                // Filtra armas largas não existentes
                if( size == 'large' && [ 'sword', 'bow', 'crossbow' ].includes( sampleCard.content.typeset.role?.name ) ) continue;

                // Gera carta com tamanho alvo
                let currentCard = new cardConstructor( { size: size } );

                // Filtra cartas que não atendem restrições especiais, se existentes
                if( scopes.item.total.constraint.description && !scopes.item.total.constraint.check( currentCard ) ) continue;

                // Adiciona carta à lista
                this.cards.push( currentCard );
              }
            }
          }
        }

        // Encerra operação
        break;
      }
      case 'spell': {
        // Apenas adiciona cartas de magias caso escopo as abarque
        if( !scopes.spell.total.max ) break;

        // Identificadores
        let [ scopePaths, commanderPaths ] = [ [], commander.content.stats.channeling?.paths ?? {} ],
            pathsByRaceAndLevel = {
              metoth: {
                human: 1, elf: 1, halfling: 1, gnome: 1, goblin: 3, ogre: 1
              },
              enoth: {
                human: 1, orc: 1, elf: 1, gnome: 1, goblin: 2
              },
              powth: {
                human: 1, elf: 1, halfling: 1, goblin: 1
              },
              voth: {
                human: 1, orc: 1, elf: 1, gnome: 1
              },
              faoth: {
                human: 1, orc: 1, elf: 1, halfling: 1, gnome: 1
              }
            };

        // Identifica sendas suportadas pelo escopo

        /// Por raça e experiência
        forPath: for( let path in pathsByRaceAndLevel ) {
          forRace: for( let race in pathsByRaceAndLevel[ path ] ) {
            // Para caso raça alvo seja suportada pelo escopo enquanto com um nível de experiência em que canalizadores da raça tenham acesso à senda alvo
            if( scopeRaces.includes( race ) && scopeExperienceLevels.some( level => level >= pathsByRaceAndLevel[ path ][ race ] ) ) {
              // Adiciona senda alvo ao arranjo de sendas suportadas pelo escopo
              scopePaths.push( path );

              // Avança para próxima senda
              continue forPath;
            }
          }
        }

        /// Por habilidades do comandante
        for( let path in commanderPaths )
          if( commanderPaths[ path ].skill && !scopePaths.includes( path ) ) scopePaths.push( path );

        // Itera por construtores de magias
        for( let cardConstructor of m.data.cards.spells ) {
          // Identificadores
          let sampleCard = new cardConstructor();

          // Caso ambiente não seja o de desenvolvimento, filtra cartas que ainda não foram implementadas
          if( !m.app.isInDevelopment && sampleCard.coins.base >= 9999 ) continue;

          // Filtra magias cuja senda esteja fora do escopo
          if( !scopePaths.includes( sampleCard.content.typeset.path ) ) continue;

          // Filtra cartas que não atendem restrições especiais, se existentes
          if( scopes.spell.total.constraint.description && !scopes.spell.total.constraint.check( sampleCard ) ) continue;

          // Adiciona carta à lista
          this.cards.push( sampleCard );
        }
      }
    }
  }

  /// Geração das entradas de cartas
  buildEntries: {
    // Identificadores
    let cardsListTexts = m.languages.components.cardsList(),
        coinsIconHoverText = cardsListTexts[ type == 'command' ? 'mainDeckCoins' : 'price' ];

    // Itera por cartas da lista
    for( let card of this.cards ) {
      // Gera entrada, e a inseri na tela
      let entry = this.addChild( new PIXI.Container() );

      // Inserção da entrada no arranjo de entradas
      this.entries.push( entry );

      // Inserção da carta na entrada, e sua captura
      entry.card = entry.addChild( card );

      // Identificadores do contedor de moedas
      let coinsContainer = entry.coins = entry.addChild( new PIXI.Container() ),
          coinsIcon = coinsContainer.icon = coinsContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.coin ] ) ),
          textStyle = { fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22 },
          coinsText = coinsContainer.bitmap = coinsContainer.addChild( new PIXI.BitmapText( card.coins.current.toString(), textStyle ) );

      // Redimensionamento do ícone de moedas
      coinsIcon.oScaleByGreaterSize( 26 );

      // Definição do texto suspenso do ícone de moedas
      coinsIcon.hoverText = coinsIconHoverText;

      // Configurações do texto sobre moedas
      this.entries.setCoinsText( entry );

      // Itera por componentes do contedor de moedas
      for( let component of [ coinsText, coinsIcon ] ) {
        // Torna componente interativo
        component.interactive = true;

        // Nomeia texto suspenso do componente
        component.hoverTextName = `${ this.name }-hover-text`;

        // Adiciona evento para exibição do texto suspenso
        component.addListener( 'mouseover', m.app.showHoverText );

        // Adiciona evento para abrir seção do manual sobre preço de cartas ao se clicar no componente
        component.addListener( 'click', () => m.app.openManualSection( 'card-price' ) );
      }
    }
  }

  /// Configura entradas
  this.entries.config();
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CardsList.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof CardsList );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = '';

    /// Tipo
    component.type = '';

    /// Entradas
    component.entries = [];

    /// Cartas
    component.cards = [];

    /// Fim da área visível da lista de cartas
    component.displayOffset = 1600;

    // Atribuição de propriedades de 'entries'
    let entries = component.entries;

    // Configurações iniciais para as entradas da lista
    entries.config = function () {
      // Posiciona as entradas da lista
      this.arrange();

      // Itera por cartas das entradas
      for( let card of component.cards ) {
        // Define visibilidade padrão das seções de designações da carta
        for( let section of [ 'heading', 'footer' ] ) card.body.front[ section ].visible = m.Card.designationVisibility.list;
      }
    }

    // Posiciona as entradas da lista
    entries.arrange = function ( startIndex = 0 ) {
      // Identificadores
      var [ marginX, previousEntry ] = [ 32 ];

      // Posiciona entradas a partir da do índice inicial
      for( let i = startIndex; i < this.length; i++ ) {
        // Captura entrada atual
        let currentEntry = this[ i ];

        // Filtra entradas que não estejam inseridas na lista
        if( currentEntry.parent != component ) continue;

        // Posiciona entrada atual
        currentEntry.x = previousEntry ? previousEntry.x + previousEntry.width + marginX : 0;

        // Define visibilidade da entrada
        currentEntry.visible = currentEntry.x + currentEntry.width <= component.displayOffset ? true : false;

        // Atualiza entrada anterior
        previousEntry = currentEntry;
      }

      // Posiciona entradas até a do índice inicial
      for( let i = 0; i < startIndex; i++ ) {
        // Captura entrada atual
        let currentEntry = this[ i ];

        // Filtra entradas que não estejam inseridas na lista
        if( currentEntry.parent != component ) continue;

        // Posiciona entrada atual
        currentEntry.x = previousEntry ? previousEntry.x + previousEntry.width + marginX : 0;

        // Define visibilidade da entrada
        currentEntry.visible = currentEntry.x + currentEntry.width <= component.displayOffset ? true : false;

        // Atualiza entrada anterior
        previousEntry = currentEntry;
      }
    }

    // Define texto sobre moedas da carta da entrada passada
    entries.setCoinsText = function ( entry ) {
      // Identificadores
      var { card, coins: coinsContainer } = entry,
          { icon: coinsIcon, bitmap: coinsText } = coinsContainer;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( entries.includes( entry ) );

      // Definição do texto sobre moedas da carta da entrada alvo, em função de se ela é ou não um comandante
      coinsText.text = ( !card.content.stats.command ? card.coins.current : card.content.stats.command.availableCoins.mainDeck ).toString();

      // Atualização do texto suspenso sobre o preço da carta da entrada alvo, se ela não for um comandante
      if( !card.content.stats.command )
        coinsText.hoverText = card.coins.base != card.coins.current ? `${ m.languages.snippets.base }: ${ card.coins.base }` : '';

      // Posicionamentos

      /// Do ícone de moedas
      coinsIcon.position.set( coinsText.x + coinsText.width + 6, coinsText.height * .5 - coinsIcon.height * .5 );

      /// Do contedor de moedas
      coinsContainer.position.set( card.width * .5 - coinsContainer.width * .5, card.height + 12 );
    }
  }
}

/// Propriedades do protótipo
CardsList.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: CardsList }
} );
