// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const CardsFilterElement = function ( config = {} ) {
  // Iniciação de propriedades
  CardsFilterElement.init( this );

  // Identificadores
  var { name, type } = config,
      { heading: filterHeading, body: filterBody } = this.sections,
      headingPaddingX = 6;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'command', 'creature', 'item', 'spell' ].includes( type ) );

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  this.name = ( name ?? '' ) + `${ type }-cards-filter`;

  /// Atribuição do tipo
  this.type = type;

  /// Configurações das seções da barra

  //// Cabeçalho
  configHeading: {
    // Identificadores
    let headingCaption = type == 'command' ?
          m.languages.keywords.getFrameComponent( 'cardCommand' ) : m.languages.keywords.getPrimitiveType( type, { plural: true } ),
        headingText = new PIXI.BitmapText( headingCaption, {
          fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22
        } ),
        sectionsSelector = type == 'command' ? null : new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ),
        sectionsSelectorMarginX = 12;

    // Texto

    /// Inclusão
    filterHeading.caption = filterHeading.addChild( headingText );

    /// Posicionamento
    filterHeading.caption.position.set( headingPaddingX, this.defaultHeight * .5 - filterHeading.caption.height * .5 );

    // Seletor de seções, se aplicável
    if( sectionsSelector ) {
      // Inclusão
      filterHeading.selector = filterHeading.addChild( sectionsSelector );

      // Medidas
      sectionsSelector.scale.set( .19 );

      // Âncoras
      sectionsSelector.anchor.set( .5 );

      // Rotação
      sectionsSelector.angle = 270;

      // Posicionamento
      sectionsSelector.position.set( headingText.width + sectionsSelector.width * .5 + sectionsSelectorMarginX, this.defaultHeight * .5 + 2 );

      // Interatividade
      filterHeading.buttonMode = filterHeading.interactive = true;
    }
  }

  //// Corpo
  configBody: {
    switch( type ) {
      case 'command':
      case 'creature':
        // Filtros de raça
        filterBody.sections.race = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: m.Humanoid.raceNames,
          iconsKey: 'races',
          hoverTextMethod: 'getRace'
        } ) );
        // Filtros de atuações
        filterBody.sections.roleType = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'melee', 'ranged', 'magical' ],
          iconsKey: 'roleTypes',
          hoverTextMethod: 'getRoleType',
          hoverTextConfig: { plural: true, person: true }
        } ) );
        // Filtros de experiência
        filterBody.sections.experienceLevel = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ '1', '2', '3', '4' ],
          iconsKey: 'experienceLevels',
          hoverTextMethod: 'getExperienceLevel',
          last: true
        } ) );
        break;
      case 'item':
        // Filtros de equipagem
        filterBody.sections.equipage = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'weapon', 'garment', 'implement' ],
          iconsKey: 'equipages',
          hoverTextMethod: 'getEquipage'
        } ) );
        // Filtros de tamanho
        filterBody.sections.size = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'null', 'small', 'medium', 'large' ],
          iconsKey: 'sizes',
          hoverTextMethod: 'getSize'
        } ) );
        // Filtros de categoria de equipamentos
        filterBody.sections.grade = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'ordinary', 'masterpiece', 'artifact', 'relic' ],
          iconsKey: 'itemsGrade',
          hoverTextMethod: 'getItemGrade',
          last: true
        } ) );
        break;
      case 'spell':
        // Filtros de senda
        filterBody.sections.path = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ],
          iconsKey: 'paths',
          hoverTextMethod: 'getPath'
        } ) );
        // Filtros de polaridade
        filterBody.sections.polarity = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'negative', 'apolar', 'positive' ],
          iconsKey: 'polarities',
          hoverTextMethod: 'getPolarity',
          hoverTextConfig: { feminine: true }
        } ) );
        // Filtros de duração
        filterBody.sections.duration = filterBody.addChild( CardsFilterElement.buildFiltersSet.call( this, {
          components: [ 'immediate', 'sustained', 'persistent', 'permanent' ],
          iconsKey: 'durations',
          hoverTextMethod: 'getDuration',
          last: true
        } ) );
    }
  }

  /// Renderização dos elementos
  this.draw( filterHeading.width + headingPaddingX * 2 );

  /// Inserção de elementos
  this.addChild( filterHeading, filterBody );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CardsFilterElement.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof CardsFilterElement );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = '';

    /// Tipo
    component.type = '';

    /// Altura padrão
    component.defaultHeight = 40;

    /// Seções
    component.sections = {};

    /// Renderiza barra do filtro
    component.draw = function( headingWidth ) {
      // Identificadores
      var { heading: filterHeading, body: filterBody } = component.sections;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( headingWidth );

      // Renderização do cabeçalho

      /// Confecção
      filterHeading.beginFill( m.data.colors.semiblack3 );

      /// Corporificação
      filterHeading.drawRect( 0, 0, headingWidth, this.defaultHeight );

      /// Encerramento
      filterHeading.endFill();

      /// Ajusta posição horizontal do seletor de menu, se aplicável
      if( filterHeading.selector )
        filterHeading.selector.x = filterHeading.x + filterHeading.width - filterHeading.selector.width * .5 - filterHeading.caption.x;

      // Renderização do corpo

      /// Confecção
      filterBody.beginFill( m.data.colors.semiblack4 );

      /// Corporificação
      filterBody.drawRect( 0, 0, m.app.pixi.screen.width - filterHeading.width, this.defaultHeight );

      /// Encerramento
      filterBody.endFill();

      // Posicionamento horizontal do corpo
      filterBody.x = filterHeading.width;
    }

    // Atribuição de propriedades de 'sections'
    let rootSections = component.sections;

    /// Cabeçalho
    rootSections.heading = new PIXI.Graphics();

    /// Corpo
    rootSections.body = new PIXI.Graphics();

    // Atribuição de propriedades de 'sections.heading'
    let heading = rootSections.heading;

    /// Legenda
    heading.caption = undefined;

    /// Seletor de seções
    heading.selector = null;

    // Atribuição de propriedades de 'sections.body'
    let body = rootSections.body;

    /// Seções
    body.sections = {};

    /// Arranjo com filtros ativos
    body.activeFilters = [];

    /// Alterna atividade de filtro passado
    body.toggleFilter = function ( targetFilter, eventData ) {
      // Não executa função caso ela tenha sido acionada por um evento de clique e a tecla 'ctrl' esteja pressionada
      if( eventData?.type == 'click' && m.app.keysPressed.ctrl ) return false;

      // Alternação da atividade do filtro
      return targetFilter.isActive ? this.inactivateFilter( targetFilter ) : this.activateFilter( targetFilter );
    }

    /// Ativa filtro passado
    body.activateFilter = function ( targetFilter ) {
      // Não executa função caso filtro já esteja ativo ou esteja bloqueado
      if( targetFilter.isActive || targetFilter.isBlocked ) return false;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Gera filtro de cor de ativação
      var colorFilter = new PIXI.filters.ColorMatrixFilter();

      // Configura filtro de cor

      /// Nomeação do filtro
      colorFilter.name = 'active-filter';

      /// Aplicação de escala de cinza
      colorFilter.greyscale( .5 );

      // Adiciona filtro de cor ao filtro
      targetFilter.filters.push( colorFilter );

      // Adiciona filtro ao arranjo de filtros ativos
      this.activeFilters.push( targetFilter );

      // Indica que filtro está ativo
      return targetFilter.isActive = true;
    }

    /// Inativa filtro passado
    body.inactivateFilter = function ( targetFilter ) {
      // Não executa função caso filtro já esteja inativo ou esteja bloqueado
      if( !targetFilter.isActive || targetFilter.isBlocked ) return false;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Captura índice do filtro de cor de atividade
      var activeFilterIndex = this.activeFilters.indexOf( targetFilter ),
          activeColorFilterIndex = targetFilter.filters.findIndex( filter => filter.name == 'active-filter' );

      // Remove filtro do arranjo de filtros ativos
      if( activeFilterIndex != -1 ) this.activeFilters.splice( activeFilterIndex, 1 );

      // Remove filtro de cor de atividade
      if( activeColorFilterIndex != -1 ) targetFilter.filters.splice( activeColorFilterIndex, 1 ).pop().destroy( { children: true } );

      // Indica que filtro está inativo
      return targetFilter.isActive = false;
    }

    /// Inativa filtro alvo, e ativa todos os outros de sua seção
    body.activateUntargetedFilters = function ( targetFilter, eventData ) {
      // Não executa função caso filtro alvo esteja bloqueado
      if( targetFilter.isBlocked ) return false;

      // Não executa função caso ela tenha sido acionada por um evento de clique e a tecla 'ctrl' não esteja pressionada
      if( eventData?.type == 'click' && !m.app.keysPressed.ctrl ) return false;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Identificadores
      var siblingFilters = targetFilter.parent.filtersArray.filter( filter => filter != targetFilter );

      // Inativa filtro alvo
      this.inactivateFilter( targetFilter );

      // Ativa demais filtros
      for( let filter of siblingFilters ) this.activateFilter( filter );
    }

    /// Bloqueia uso do filtro
    body.blockFilter = function ( targetFilter ) {
      // Não executa função caso filtro esteja ativo ou já esteja bloqueado
      if( targetFilter.isActive || targetFilter.isBlocked ) return false;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Gera filtro de cor de bloqueio
      var colorFilter = new PIXI.filters.ColorMatrixFilter();

      // Configura filtro de cor

      /// Nomeação do filtro
      colorFilter.name = 'blocked-filter';

      /// Aplicação de escala de cinza
      colorFilter.greyscale( .30 );

      // Adiciona filtro de cor ao filtro
      targetFilter.filters.push( colorFilter );

      // Indica que filtro está bloqueado
      return targetFilter.isBlocked = true;
    }

    /// Desbloqueia uso do filtro
    body.unblockFilter = function ( targetFilter ) {
      // Não executa função caso filtro já esteja desbloqueado
      if( !targetFilter.isBlocked ) return false;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Captura índice do filtro de cor de bloqueio
      var blockedColorFilterIndex = targetFilter.filters.findIndex( filter => filter.name == 'blocked-filter' );

      // Remove filtro de cor de bloqueio
      if( blockedColorFilterIndex != -1 ) targetFilter.filters.splice( blockedColorFilterIndex, 1 ).pop().destroy( { children: true } );

      // Indica que filtro está desbloqueado
      return targetFilter.isBlocked = false;
    }
  }
}

/// Propriedades do protótipo
CardsFilterElement.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: CardsFilterElement }
} );

// Métodos não configuráveis de 'CardsFilterElement'
Object.defineProperties( CardsFilterElement, {
  // Monta conjunto de filtros
  buildFiltersSet: {
    value: function ( config ) {
      // Identificadores
      var { components, iconsKey, hoverTextMethod, hoverTextConfig } = config,
          filtersContainer = new PIXI.Container(),
          filtersArray = filtersContainer.filtersArray = [],
          filters = components.map( component => new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons[ iconsKey ][ component ] ] ) ),
          [ filterMarginX, separatorMarginX ] = [ 12, 24 ],
          previousFiltersSet = this.sections.body.children[ this.sections.body.children.length - 1 ];

      // Itera por componentes
      for( let i = 0; i < components.length; i++ ) {
        // Identificadores
        let [ component, filter, previousFilter ] = [ components[ i ], filters[ i ], filters[ i - 1 ] ];

        // Adiciona filtro a seu contedor
        filtersContainer.addChild( filter );

        // Redimensiona filtro
        filter.oScaleByGreaterSize( 30 );

        // Posiciona filtro
        filter.position.set(
          previousFilter ? previousFilter.x + previousFilter.width + filterMarginX : 0,
          this.defaultHeight * .5 - filter.height * .5
        );

        // Nomeia filtro
        filter.name = component;

        // Indica se filtro está bloqueado
        filter.isBlocked = false;

        // Indica se filtro está ativo
        filter.isActive = false;

        // Cartas afetadas pelo filtro
        filter.targetCards = [];

        // Adiciona texto suspenso ao filtro
        filter.hoverText = m.languages.keywords[ hoverTextMethod ]( component, hoverTextConfig ?? { plural: true } );

        // Nomeia texto suspenso
        filter.hoverTextName = `cards-filter-hover-text`;

        // Concede interatividade ao filtro
        filter.buttonMode = filter.interactive = true;

        // Inicia arranjo de filtros do filtro
        filter.filters = [];

        // Adiciona filtro ao arranjo de filtros de sua seção
        filtersArray.push( filter );

        // Eventos

        /// Para alternar atividade do filtro
        filter.addListener( 'click', this.sections.body.toggleFilter.bind( this.sections.body, filter ) );

        /// Para inativar filtro alvo, e ativar todos os outros de sua seção
        filter.addListener( 'click', this.sections.body.activateUntargetedFilters.bind( this.sections.body, filter ) );

        /// Para emissão de som ao clicar em filtro
        filter.addListener( 'click', () => CardsFilterElement.playFilterClickSound( filter ) );

        /// Para exibição de texto suspenso
        filter.addListener( 'mouseover', m.app.showHoverText );
      }

      // Posiciona horizontalmente contedor
      filtersContainer.x = previousFiltersSet ? previousFiltersSet.x + previousFiltersSet.width + separatorMarginX : separatorMarginX;

      // Retorna contedor do filtro, ou, caso não seja o último, adiciona a ele separador de conjunto de filtros
      return config.last ? filtersContainer : CardsFilterElement.buildSeparator.call( this, filtersContainer );
    }
  },
  // Adiciona separador
  buildSeparator: {
    value: function ( filtersContainer ) {
      // Identificadores
      var separator = new PIXI.Graphics(),
          lastFilter = filtersContainer.children[ filtersContainer.children.length - 1 ],
          marginX = 24;

      // Inserção do separador em seu contedor
      filtersContainer.addChild( separator );

      // Nomeia separador
      separator.name = 'separator';

      // Renderiza separador

      /// Confecção
      separator.beginFill( m.data.colors.black );

      /// Corporificação
      separator.drawRect( 0, 0, 1, this.defaultHeight );

      /// Encerramento
      separator.endFill();

      // Diminui opacidade do separador
      separator.alpha = .25;

      // Posicionamento horizontal do separador
      separator.position.set(
        lastFilter.x + lastFilter.width + marginX,
        this.defaultHeight * .5 - separator.height * .5
      );

      // Retorna contedor do filtro
      return filtersContainer;
    }
  },
  // Emite som ao clicar em filtro
  playFilterClickSound: {
    value: function ( targetFilter ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFilter instanceof PIXI.Sprite && 'isActive' in targetFilter );

      // Caso filtro esteja bloqueado, emite som de bloqueio
      if( targetFilter.isBlocked ) return m.assets.audios.soundEffects[ 'click-deny' ].play();

      // Caso filtro esteja ativo, emite som de ativação
      if( targetFilter.isActive ) return m.assets.audios.soundEffects[ 'click-next' ].play();

      // Do contrário, emite som de inativação
      return m.assets.audios.soundEffects[ 'click-previous' ].play();
    }
  }
} );
