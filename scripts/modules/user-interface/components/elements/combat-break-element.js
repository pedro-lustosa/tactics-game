// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const CombatBreakElement = function ( config = {} ) {
  // Iniciação de propriedades
  CombatBreakElement.init( this );

  // Identificadores
  var { name, attacker, defender, attack, defense } = config,
      [ attackerAttributes, defenderAttributes ] = [ attacker.content.stats.attributes.current, defender.content.stats.attributes.current ],
      [ attackerManeuvers, defenderManeuvers ] = [ attacker.content.stats.effectivity.maneuvers, defender.content.stats.effectivity.maneuvers ],
      isDodgeDefense = defense?.maneuver instanceof m.ManeuverDodge,
      [ elementHeading, elementBody, elementFooter ] = [ this.heading, this.body, this.footer ],
      bodySections = elementBody.sections;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( name && typeof name == 'string' );
    m.oAssert( [ attacker, defender ].every( being => being instanceof m.Being ) );
    m.oAssert( ![ m.Tulpa, m.Angel ].some( constructor => attacker instanceof constructor ) );
    m.oAssert( attack.constructor == Object );
    m.oAssert( !defense || defense.constructor == Object );
    m.oAssert( attack.maneuver instanceof m.OffensiveManeuver );
    m.oAssert( [ 'primary', 'secondary' ].some( rank => attackerManeuvers[ rank ] == attack.maneuverSource ) );

    // Para caso haja uma defesa
    if( defense ) {
      m.oAssert( defense.maneuver instanceof m.DefensiveManeuver );
      m.oAssert(
        defense.maneuver instanceof m.ManeuverDodge || [ 'primary', 'secondary' ].some( rank => defenderManeuvers[ rank ] == defense.maneuverSource )
      );
    }
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Atribuição do nome
  this.name = name.endsWith( '-combat-break' ) ? name : name + '-combat-break';

  // Atribuição de propriedades sobre ações e manobras à parada
  for( let property of [ 'attacker', 'defender', 'attack', 'defense' ] ) this[ property ] = config[ property ];

  // Geração e captura dos objetos de dano da parada
  this.damages = Object.entries( attack.maneuver.damage ).map( function ( entry ) {
    // Identifica construtor alvo
    var targetConstructor = [ m.DamageBlunt, m.DamageSlash, m.DamagePierce, m.DamageFire, m.DamageShock, m.DamageMana, m.DamageEther ].find(
          constructor => constructor.name.toLowerCase() == 'damage' + entry[ 0 ]
        );

    // Retorna objeto de dano
    return new targetConstructor( { singlePoints: entry[ 1 ], maneuver: attack.maneuver, source: attack.maneuver.source } );
  } );

  // Conteúdo do elemento
  elementContent: {
    // Identificadores
    let combatBreakTexts = m.languages.components.combatBreak();

    // Cabeçalho
    heading: {
      // Identificadores
      let captionStyle = {
            fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22
          };

      // Geração e inserção do título
      elementHeading.caption = elementHeading.addChild( new PIXI.BitmapText( combatBreakTexts.caption, captionStyle ) );
    }

    // Corpo
    body: {
      // Identificadores
      let isAttackerWithEarthGrip = attacker.getAllCardAttachments().some( card => card instanceof m.SpellEarthGrip && card.content.effects.isEnabled ),
          isDefenderWithEarthGrip = defender.getAllCardAttachments().some( card => card instanceof m.SpellEarthGrip && card.content.effects.isEnabled ),
          titleStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -18
          },
          bodyStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16
          },
          sectionObjects = {
            attack: {
              titleIcon: m.assets.images.icons.maneuvers[ attack.maneuver.name.replace( /^maneuver-/, '' ).oKebadToCamel() ],
              titleHoverText: combatBreakTexts.getAttackHoverText( attack.maneuver, attack.maneuverSource ),
              dieName: m.assets.images.icons.dices[ attacker.content.typeset.race ],
              dieIconName: m.assets.images.icons.maneuvers[ attack.maneuver.name.replace( /^maneuver-/, '' ).oKebadToCamel() ],
              dieHoverText: combatBreakTexts.getDieAttackHoverText( attack.maneuver ),
              availablePoints: attack.maneuverSource.currentPoints - 1,
              assignedPoints: 1
            },
            attackModifiers: {
              titleIcon: m.assets.images.icons.stats.modifier,
              titleHoverText: combatBreakTexts.getCombatModifiersHoverText( this.getCombatModifiers( attacker ) ),
              updateTitleHoverText: () => combatBreakTexts.getCombatModifiersHoverText( this.getCombatModifiers( attacker ) ),
              dieName: m.assets.images.icons.dices[ attacker.content.typeset.race ],
              dieIconName: m.assets.images.icons.maneuvers.dodge,
              dieHoverText: m.languages.keywords.getAttribute( 'agility' ),
              availablePoints: attack.maneuver.range.startsWith( 'M' ) && !isAttackerWithEarthGrip ?
                Math.min( attackerAttributes.agility, 5 ) : 0,
              assignedPoints: this.getCombatModifiers( attacker ).final
            },
            defense: {
              titleIcon: m.assets.images.icons.maneuvers[ defense?.maneuver.name.replace( /^maneuver-/, '' ) ?? 'dodge' ],
              titleHoverText: defense ? combatBreakTexts.getDefenseHoverText( defense.maneuver, defense.maneuverSource ) : '',
              dieName: m.assets.images.icons.dices[ defender.content.typeset.race ],
              dieIconName: m.assets.images.icons.maneuvers[ defense?.maneuver.name.replace( /^maneuver-/, '' ) ?? 'dodge' ],
              dieHoverText: defense ? combatBreakTexts.getDieDefenseHoverText( defense.maneuver ) : '',
              availablePoints: isDodgeDefense ? defenderAttributes.agility : defense?.maneuverSource.currentPoints ?? 0,
              assignedPoints: 0,
              isOutsideBody: !defense
            },
            defenseModifiers: {
              titleIcon: m.assets.images.icons.stats.modifier,
              titleHoverText: combatBreakTexts.getCombatModifiersHoverText( this.getCombatModifiers( defender ) ),
              updateTitleHoverText: () => combatBreakTexts.getCombatModifiersHoverText( this.getCombatModifiers( defender ) ),
              dieName: m.assets.images.icons.dices[ defender.content.typeset.race ],
              dieIconName: m.assets.images.icons.maneuvers.dodge,
              dieHoverText: m.languages.keywords.getAttribute( 'agility' ),
              availablePoints: !attack.maneuver.range.startsWith( 'M' ) || !defense || isDodgeDefense || isDefenderWithEarthGrip ?
                0 : Math.min( defenderAttributes.agility, 5 ),
              assignedPoints: defense ? this.getCombatModifiers( defender ).final : 0,
              isOutsideBody: !defense
            }
          };

      // Itera por seções do corpo
      for( let sectionName in sectionObjects ) {
        // Identificadores
        let sectionObject = sectionObjects[ sectionName ],
            bodySection = bodySections[ sectionName ] = sectionObject.isOutsideBody ? new PIXI.Container() : elementBody.addChild( new PIXI.Container() ),
            sectionTitle = bodySection.title = bodySection.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ sectionObject.titleIcon ] ) ),
            sectionBody = bodySection.body = bodySection.addChild( new PIXI.Container() ),
            availablePoints = sectionBody.availablePoints = sectionBody.addChild( new PIXI.BitmapText( sectionObject.availablePoints.toString(), bodyStyle ) ),
            minusArrow = sectionBody.minusArrow = sectionBody.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) ),
            pointsDisplayer = sectionBody.pointsDisplayer = sectionBody.addChild( new PIXI.Graphics() ),
            plusArrow = sectionBody.plusArrow = sectionBody.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) ),
            assignedPoints = sectionBody.assignedPoints = sectionBody.addChild( new PIXI.BitmapText( sectionObject.assignedPoints.toString(), bodyStyle ) ),
            dices = pointsDisplayer.dices = [],
            pointSources = pointsDisplayer.pointSources = {};

        // Define os pontos mínimos e máximos que podem haver no visualizador
        [ bodySection.minPoints, bodySection.maxPoints ] = [ Number( assignedPoints.text ), 10 ];

        // Itera por propriedades a serem registradas
        for( let key of [ 'dieName', 'dieIconName', 'dieHoverText', 'updateTitleHoverText' ] ) {
          // Caso propriedade exista no objeto da seção, transfere-a para o corpo
          if( sectionObject[ key ] ) bodySection[ key ] = sectionObject[ key ];
        }

        // Adiciona dados iniciais ao visualizador de pontos
        for( let i = 0; i < Math.abs( bodySection.minPoints ); i++ )
          dices.push( pointsDisplayer.addChild( elementBody.buildDie( bodySection, [ 'attackModifiers', 'defenseModifiers' ].includes( sectionName ) ) ) );

        // Registra quantidade de pontos que provém dos pontos iniciais no visualizador de pontos
        pointSources.base = bodySection.minPoints;

        // Atribuição de textos suspensos

        /// Do título da seção
        sectionTitle.hoverText = sectionObject.titleHoverText;

        /// Dos pontos disponíveis e atribuídos
        for( let key of [ 'availablePoints', 'assignedPoints' ] ) sectionBody[ key ].hoverText = combatBreakTexts[ key + 'HoverText' ];
      }
    }

    // Rodapé
    footer: {
      // Identificadores
      let fateIcons = elementFooter.fateIcons = {},
          damagePreview = elementFooter.damagePreview = {},
          shortPreview = damagePreview.short = elementFooter.addChild( new PIXI.Container() ),
          longPreview = damagePreview.long = elementFooter.addChild( new PIXI.Container() ),
          shortPreviewStyle = {
            fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -21
          },
          longPreviewStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -18
          };

      // Itera por paridades
      for( let parity of [ 'odd', 'even' ] ) {
        // Gera ícone de ponto de destino alvo
        let fateIcon = fateIcons[ parity ] = elementFooter.addChild( new PIXI.Sprite(PIXI.utils.TextureCache[ m.assets.images.icons.misc.fatePointsSymbol ]) );

        // Define texto suspenso do ícone
        fateIcon.hoverText = combatBreakTexts.getFatePointHoverText( parity );

        // Adiciona propriedade para armazenamento de filtros
        fateIcon.filters = [];
      }

      // Atribui arranjo de danos ao contedor do dano previsto resumido
      shortPreview.damages = [];

      // Atribui arranjo de modificadores ao contedor do dano previsto extenso
      longPreview.modifiers = [];

      // Atribui arranjo de separadores aos contedores do dano previsto
      for( let preview of [ shortPreview, longPreview ] ) preview.separators = [];

      // Itera por danos da parada, para gerar visualizador resumido do dano previsto
      for( let i = 0; i < this.damages.length; i++ ) {
        // Caso dano atual não seja o primeiro, cria um separador
        if( i ) shortPreview.separators.push( shortPreview.addChild( new PIXI.BitmapText( ' + ', shortPreviewStyle ) ) );

        // Gera um indicador de dano
        shortPreview.damages.push( shortPreview.addChild( new PIXI.BitmapText( '0', shortPreviewStyle ) ) );

        // Registra dano de referência no indicador alvo
        shortPreview.damages[ i ].source = this.damages[ i ];

        // Captura texto suspenso do dano alvo
        shortPreview.damages[ i ].hoverText = combatBreakTexts.getTotalDamageHoverText( this.damages[ i ] );
      }

      // Geração do visualizador extenso do dano previsto

      /// Seta para retornar ao dano resumido
      longPreview.modifiers.push(
        longPreview.modifiers.backArrow = longPreview.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' ', longPreviewStyle ) ) );

      /// Pontos de ataque
      longPreview.modifiers.push(
        longPreview.modifiers.attackPoints = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' * ( ', longPreviewStyle ) ) );

      /// Dano bruto
      longPreview.modifiers.push(
        longPreview.modifiers.rawDamage = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' - ( ', longPreviewStyle ) ) );

      /// Resistência natural
      longPreview.modifiers.push(
        longPreview.modifiers.naturalResistance = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' + ', longPreviewStyle ) ) );

      /// Resistência do traje primário
      longPreview.modifiers.push(
        longPreview.modifiers.primaryGarmentResistance = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' + ', longPreviewStyle ) ) );

      /// Resistência do traje secundário
      longPreview.modifiers.push(
        longPreview.modifiers.secondaryGarmentResistance = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' - ', longPreviewStyle ) ) );

      /// Penetração
      longPreview.modifiers.push(
        longPreview.modifiers.penetration = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' ) ) * ', longPreviewStyle ) ) );

      /// Modificador de destino
      longPreview.modifiers.push(
        longPreview.modifiers.fateModifier = longPreview.addChild( new PIXI.BitmapText( '1', longPreviewStyle ) )
      );

      /// Separador
      longPreview.separators.push( longPreview.addChild( new PIXI.BitmapText( ' = ', longPreviewStyle ) ) );

      /// Dano resultante
      longPreview.modifiers.push(
        longPreview.modifiers.resultingDamage = longPreview.addChild( new PIXI.BitmapText( '0', longPreviewStyle ) )
      );

      // Definição dos textos suspensos do visualizador extenso do dano previsto

      /// Da seta para retornar ao dano resumido
      longPreview.modifiers.backArrow.hoverText = combatBreakTexts.shortDamageArrowHoverText;

      /// Dos pontos de ataque
      longPreview.modifiers.attackPoints.hoverText = combatBreakTexts.attackPointsHoverText;

      /// Do dano bruto
      longPreview.modifiers.rawDamage.hoverText = combatBreakTexts.getRawDamageHoverText( this.damages[ 0 ] );

      /// Da resistência natural
      longPreview.modifiers.naturalResistance.hoverText = combatBreakTexts.naturalResistanceHoverText;

      /// Da resistência do traje primário
      longPreview.modifiers.primaryGarmentResistance.hoverText =
        combatBreakTexts.getPrimaryGarmentResistanceHoverText( defender.garments?.primary?.coverage ?? 0 );

      /// Da resistência do traje secundário
      longPreview.modifiers.secondaryGarmentResistance.hoverText =
        combatBreakTexts.getSecondaryGarmentResistanceHoverText( defender.garments?.secondary?.coverage ?? 0 );

      /// Da penetração
      longPreview.modifiers.penetration.hoverText = combatBreakTexts.penetrationHoverText;

      /// Do modificador de destino
      longPreview.modifiers.fateModifier.hoverText = combatBreakTexts.fateModifierHoverText;

      /// Do dano resultante
      longPreview.modifiers.resultingDamage.hoverText = combatBreakTexts.resultingDamageHoverText;
    }

    // Inserção dos componentes do elemento
    this.addChild( elementHeading, elementBody, elementFooter );
  }

  /// Formatação do elemento
  elementStyle: {
    // Identificadores
    let { styleData } = elementBody,
        { marginTop: bodyMarginTop, marginBottom: bodyMarginBottom, sectionTitleSize, sectionTitleMarginRight, arrowSize } = styleData,
        componentsMarginLeft = 8,
        largestAvailablePointsWidth = Object.values( bodySections ).filter( section => section.parent ).map( section => section.body.availablePoints ).reduce(
          ( accumulator, current ) => accumulator = accumulator.width >= current.width ? accumulator : current
        ).width,
        { fateIcons, damagePreview, damagePreview: { short: shortPreview, long: longPreview } } = elementFooter;

    // Itera por seções do corpo
    for( let sectionName in bodySections ) {
      // Filtra seções que não estejam inseridas no corpo
      if( !elementBody.children.includes( bodySections[ sectionName ] ) ) continue;

      // Identificadores
      let section = bodySections[ sectionName ],
          { title: sectionTitle, body: sectionBody } = section,
          { availablePoints, minusArrow, pointsDisplayer, plusArrow, assignedPoints } = sectionBody;

      // Redimensiona título da seção
      sectionTitle.oScaleByGreaterSize( sectionTitleSize );

      // Configurações das setas de controle de pontos
      for( let arrow of [ minusArrow, plusArrow ] ) {
        // Redimensionamento
        arrow.oScaleByGreaterSize( arrowSize );

        // Habilitação do modo de botão
        arrow.buttonMode = true;
      }

      // Confecciona visualizador de pontos
      elementBody.buildPointsDisplayer( section );

      // Posicionamentos

      /// Do título da seção
      sectionTitle.position.set( 0, section.height * .5 - sectionTitle.height * .5 );

      /// Do corpo da seção
      sectionBody.position.set( sectionTitle.x + sectionTitleSize + sectionTitleMarginRight, section.height * .5 - sectionBody.height * .5 );

      /// Dos componentes do corpo da seção
      for( let i = 0; i < sectionBody.children.length; i++ ) {
        // Identificadores
        let [ currentComponent, previousComponent ] = [ sectionBody.children[ i ], sectionBody.children[ i - 1 ] ],
            previousComponentWidth = previousComponent == availablePoints ? largestAvailablePointsWidth : previousComponent?.width;

        // Posicionamento do componente alvo
        currentComponent.position.set(
          previousComponent ? previousComponent.x + previousComponentWidth + componentsMarginLeft : 0, sectionBody.height * .5 - currentComponent.height * .5
        );
      }

      // Configurações da seta de adição de pontos

      /// Rotação
      plusArrow.angle = 180;

      /// Ajuste do ponto âncora
      plusArrow.anchor.set( 1 );
    }

    // Configurações dos ícones de pontos de destino
    for( let icon of [ fateIcons.odd, fateIcons.even ] ) {
      // Redimensionamento
      icon.oScaleByGreaterSize( 22 );

      // Habilitação do modo de botão
      icon.buttonMode = true;

      // Aplica ao ícone alvo filtro para escurecimento
      this.toggleFateIconFilter( icon );
    }

    // Configuração da seta do visualizador de dano previsto extenso

    /// Redimensionamento
    longPreview.modifiers.backArrow.oScaleByGreaterSize( arrowSize );

    /// Modo de botão
    longPreview.modifiers.backArrow.buttonMode = true;

    // Posiciona cabeçalho da parada
    elementHeading.position.set( this.width * .5 - elementHeading.width * .5, 0 );

    // Posiciona corpo da parada

    /// Posicionamento do corpo inteiro
    elementBody.position.set( this.width * .5 - elementBody.width * .5, elementHeading.y + elementHeading.height + bodyMarginTop );

    /// Posicionamento entre as seções do corpo
    for( let i = 0; i < elementBody.children.length; i++ ) {
      // Identificadores
      let [ currentSection, previousSection ] = [ elementBody.children[ i ], elementBody.children[ i - 1 ] ],
          sectionMarginTop = styleData[ currentSection == bodySections.defense ? 'defenseSectionMarginTop' : 'modifierSectionMarginTop' ];

      // Posicionamento do componente alvo
      currentSection.position.set( 0, previousSection ? previousSection.y + previousSection.height + sectionMarginTop : 0 );
    }

    // Posiciona rodapé da parada
    elementFooter.position.set( 0, elementBody.y + elementBody.height + bodyMarginBottom );

    // Centraliza verticalmente componentes do rodapé
    for( let child of elementFooter.children ) child.y = elementFooter.height * .5 - child.height * .5;

    // No visualizador extenso de dano previsto, centraliza verticalmente seta de retorno ao dano total
    longPreview.modifiers.backArrow.y = longPreview.height * .5 - longPreview.modifiers.backArrow.height * .5;

    // Posiciona horizontalmente ícone sobre ponto de destino par
    fateIcons.even.x = elementBody.width - fateIcons.even.width;

    // Por padrão, oculta visualizador extenso de dano previsto
    longPreview.visible = false;
  }

  /// Interatividade do elemento
  elementInteraction: {
    // Corpo
    body: {
      // Itera por seções do corpo
      for( let sectionName in bodySections ) {
        // Filtra seções que não estejam inseridas no corpo
        if( !elementBody.children.includes( bodySections[ sectionName ] ) ) continue;

        // Identificadores
        let section = bodySections[ sectionName ],
            { title: sectionTitle, body: sectionBody, footer: sectionFooter } = section,
            { availablePoints, minusArrow, pointsDisplayer, plusArrow, assignedPoints } = sectionBody;

        // Itera por componentes da seção
        for( let component of [ sectionTitle, availablePoints, minusArrow, plusArrow, assignedPoints ] ) {
          // Torna componente interativo
          component.interactive = true;

          // Para componentes com texto suspenso
          if( component.hoverText ) {
            // Atribui um nome ao texto suspenso
            component.hoverTextName = 'combat-break-body-hover-text';

            // Adiciona evento para exibição do texto suspenso
            component.addListener( 'mouseover', m.app.showHoverText );
          }
        }

        // Por padrão, indica que seção alvo não pode ser modificada no momento
        section.isEnabled = false;

        // Eventos para as setas de pontos

        /// Evento para adicionar pontos a uma seção, e emitir som de clique segundo seu sucesso
        plusArrow.addListener( 'click', () => m.assets.audios.soundEffects[ elementBody.addPoint( section ) ? 'click-next' : 'click-deny' ].play() );

        /// Evento para remover pontos de uma seção, e emitir som de clique segundo seu sucesso
        minusArrow.addListener( 'click', () => m.assets.audios.soundEffects[ elementBody.removePoint( section ) ? 'click-previous' : 'click-deny' ].play() );

        /// Evento para atualizar visualização do dano previsto a ser infligido
        for( let arrow of [ plusArrow, minusArrow ] ) arrow.addListener( 'click', () => this.updateDamagePreview( section ) );

        /// Para seções com função de atualização do título
        if( section.updateTitleHoverText ) {
          // Evento para atualizar texto suspenso do título da seção
          for( let arrow of [ plusArrow, minusArrow ] ) arrow.addListener( 'click', () => sectionTitle.hoverText = section.updateTitleHoverText() );
        }
      }
    }

    // Rodapé
    footer: {
      // Identificadores
      let { fateIcons: { odd: oddFateIcon, even: evenFateIcon }, damagePreview: { short: shortPreview, long: longPreview } } = elementFooter;

      // Por padrão, indica que rodapé não pode ser modificado no momento
      elementFooter.isEnabled = false;

      // Itera por componentes do rodapé
      for( let component of [ oddFateIcon, evenFateIcon, ...shortPreview.damages, ...longPreview.modifiers ] ) {
        // Torna componente interativo
        component.interactive = true;

        // Para componentes com texto suspenso
        if( component.hoverText ) {
          // Atribui um nome ao texto suspenso
          component.hoverTextName = 'combat-break-footer-hover-text';

          // Adiciona evento para exibição do texto suspenso
          component.addListener( 'mouseover', m.app.showHoverText );
        }
      }

      // Itera por danos do visualizador de dano resumido, e por seta do visualizador de dano extenso
      for( let component of [ ...shortPreview.damages, longPreview.modifiers.backArrow ] ) {
        // Adiciona som de clique ao componente alvo, em função do visualizador em que está
        component.addListener( 'click', eventData =>
          m.assets.audios.soundEffects[ eventData.currentTarget == longPreview.modifiers.backArrow ? 'click-previous' : 'click-next' ].play()
        );

        // Adiciona evento para para exibir o visualizador de dano atualmente oculto
        component.addListener( 'click', this.toggleVisibleDamagePreview, this );
      }

      // Itera por ícones de ponto de destino
      for( let icon of [ oddFateIcon, evenFateIcon ] ) {
        // Adiciona propriedade para identificar se ponto de destino vinculado ao ícone alvo será ou não usado
        icon.isToUseFatePoint = false;

        // Adiciona a ícone alvo evento para alternar uso de um ponto de destino de seu jogador, e emitir som de clique segundo habilitação da operação
        icon.addListener( 'click', eventData =>
          m.assets.audios.soundEffects[ this.toggleFatePointUsage( eventData.currentTarget ) ? 'click-next' : 'click-deny' ].play()
        );
      }
    }
  }

  // Define visualização inicial do dano previsto
  this.updateDamagePreview();
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CombatBreakElement.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof CombatBreakElement );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'combat-break';

    /// Cabeçalho da parada de combate
    component.heading = new PIXI.Container();

    /// Corpo da parada de combate
    component.body = new PIXI.Container();

    /// Rodapé da parada de combate
    component.footer = new PIXI.Container();

    /// Execução da parada de combate
    component.currentExecution = null;

    /// Inseri parada de combate
    component.insert = function ( progressAction, isValid = false ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( typeof progressAction == 'function' );
        m.oAssert( typeof isValid == 'boolean' );
      }

      // Gera modal da parada de combate
      new m.InteractionModal( {
        name: 'combat-break',
        component: this,
        action: progressAction,
        isToKeepAfterAction: true,
        isValid: isValid
      } ).replace();

      // Sinaliza início da parada de combate
      m.events.breakStart.combat.emit( m.GameMatch.current, { combatBreak: this } );

      // Retorna parada de combate
      return this;
    }

    /// Retorna modificadores de ataque ou defesa da parada
    component.getCombatModifiers = function ( being ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'attacker', 'defender' ].some( key => this[ key ] == being ) );

      // Identificadores
      var { attacker, defender } = this,
          { maneuver, maneuverSource } = being == attacker ? this.attack : ( this.defense ?? {} ),
          { pointSources } = this.body.sections[ being == attacker ? 'attackModifiers' : 'defenseModifiers' ]?.body.pointsDisplayer ?? {},
          { controller: beingController, opponent: beingOpponent } = being.getRelationships(),
          beingManeuvers = being.content.stats.effectivity.maneuvers,
          maneuverRank = [ 'primary', 'secondary' ].find( rank => beingManeuvers[ rank ] == maneuverSource ),
          modifiers = {};

      // Captura modificador da manobra, quando existente
      modifiers.maneuver = maneuver?.modifier ?? 0;

      // Captura bônus de recuperação, quando existente
      modifiers.recoveryBonus = maneuverSource?.recoveryBonus ?? 0;

      // Penalidades relativas à empunhadura
      wieldingPenalties: {
        // Não executa bloco caso manobra alvo não provenha de uma arma
        if( !( maneuverSource?.source instanceof m.Weapon ) ) break wieldingPenalties;

        // Não executa bloco caso ente alvo seja um elfo
        if( being instanceof m.Elf ) break wieldingPenalties;

        // Captura penalidade da mão inábil, quando aplicável
        if( maneuverRank == 'secondary' ) modifiers.secondaryHandWeapon = -2;

        // Captura penalidade da arma multimanual, quando aplicável
        if( maneuverSource.source.content.typeset.wielding == 'multi-handed' && !being.hands.available ) modifiers.multiHandedWeapon = -2;
      }

      // Penalidade relativa à condição 'enfermo'
      diseasedPenalty: {
        // Identificadores
        let diseasedCondition = being.conditions.find( condition => condition instanceof m.ConditionDiseased );

        // Caso ente esteja enfermo, captura modificador da condição
        if( diseasedCondition ) modifiers.diseased = diseasedCondition.getManeuverModifier();
      }

      // Para caso ataque seja próximo
      if( this.attack.maneuver.range.startsWith( 'M' ) ) {
        // Para caso ente seja o atacante
        if( being == attacker ) {
          // Identificadores
          let emboldenedCondition = being.conditions.find( condition => condition instanceof m.ConditionEmboldened );

          // Caso ente esteja encorajado, captura modificador da condição
          if( emboldenedCondition ) modifiers.emboldened = emboldenedCondition.getAttackModifier();
        }

        // Para caso ente seja o defensor
        else if( being == defender ) {
          // Identificadores
          let enragedCondition = being.conditions.find( condition => condition instanceof m.ConditionEnraged );

          // Caso ente esteja enfurecido, captura modificador da condição
          if( enragedCondition ) modifiers.enraged = enragedCondition.getDefenseModifier();
        }

        // Caso ente esteja no arranjo de entes afetados pelo efeito de um apetrecho 'Poção da Ligeireza', adiciona modificador pertinente
        if( m.ImplementNimblenessPotion.affectedBeings.includes( being ) ) modifiers.nimblenessPotion = 3;
      }

      // Para caso ataque seja distante
      else if( this.attack.maneuver.range.startsWith( 'R' ) ) {
        // Para caso ente seja o atacante
        if( being == attacker ) {
          // Identificadores
          let isWeaponWithPitch = maneuver.source instanceof m.Weapon &&
                                  maneuver.source.getAllCardAttachments().some( card => card instanceof m.ImplementPitch && card.isInUse );

          // Captura penalidade de alcance efetivo
          modifiers.effectiveRange = ( function () {
            // Caso defensor seja um ente astral acionando uma ação que mire o atacante, retorna sempre 0
            if( defender instanceof m.AstralBeing && m.GameAction.currents.some( action => action.committer == defender && action.target == attacker ) )
              return 0;

            // Do contrário, retorna distância esperada
            return m.FieldSlot.getDistanceFrom( attacker.slot, defender.slot ) * -1;
          } )();

          // Caso manobra distante seja 'Disparar' e sua origem seja uma arma com um piche em uso, dobra penalidade de alcance efetivo
          if( maneuver instanceof m.ManeuverShoot && isWeaponWithPitch ) modifiers.effectiveRange *= 2;
        }
      }

      // Caso manobra seja 'Esquivar' e ela esteja sendo usada contra ataque 'Arremessar', captura penalidade atribuída por 'Arremessar'
      if( maneuver instanceof m.ManeuverDodge && this.attack.maneuver instanceof m.ManeuverThrow )
        modifiers.throwPenalty = -2;

      // Caso ente seja o atacante e comandante de seu controlador seja o Ixohch, adiciona um modificador igual à diferença entre os pontos de destino dos jogadores
      if( being == attacker && beingController.gameDeck.cards.commander instanceof m.CommanderIxohch )
        modifiers.ixohchEffect = beingController.fatePoints.current.get() - beingOpponent.fatePoints.current.get();

      // Itera por origem dos pontos de modificadores
      for( let pointSource in pointSources ) {
        // Filtra pontos inicialmente atribuídos
        if( pointSource == 'base' ) continue;

        // Atribui pontos da origem alvo ao objeto de modificadores
        modifiers[ pointSource ] = pointSources[ pointSource ];
      }

      // Trata modificadores para que estejam dentro de suas delimitações mínima e máxima
      for( let modifier in modifiers ) modifiers[ modifier ] = Math.max( -5, Math.min( 5, modifiers[ modifier ] ) );

      // Calcula e captura modificador final
      modifiers.final = Math.max( -10, Math.min( 10, Object.values( modifiers ).reduce( ( accumulator, current ) => accumulator += current, 0 ) ) );

      // Adiciona informação sobre se modificadores são relativos à ataque ou defesa
      modifiers.type = being == attacker ? 'attack' : 'defense';

      // Retorna modificadores da manobra
      return modifiers;
    }

    /// Retorna pontos atribuídos à parada
    component.getAssignedPoints = function () {
      // Identificadores
      var bodySections = this.body.sections,
          assignedPoints = {};

      // Itera por propriedades de ataque e defesa
      for( let key of [ 'attack', 'defense' ] ) {
        // Captura pontos de ataque e de defesa atribuídos
        assignedPoints[ key ] = Number( bodySections[ key ].body.assignedPoints.text );

        // Captura pontos de modificadores atribuídos
        assignedPoints[ key + 'Modifiers' ] = bodySections[ key + 'Modifiers' ].body.pointsDisplayer.pointSources;
      }

      // Retorna pontos atribuídos
      return assignedPoints;
    }

    /// Calcula pontos de ataque a serem infligidos ao fim da parada
    component.gaugeInflictedAttack = function ( isUnboundedPoints = false ) {
      // Identificadores
      var { body } = this,
          { sections: bodySections } = body,
          { attack, attackModifiers, defense, defenseModifiers } = bodySections;

      // Captura soma dos pontos de ataque e modificadores de ataque
      let totalAttackPoints = Math.max( Number( attack.body.assignedPoints.text ) + Number( attackModifiers.body.assignedPoints.text ), 0 );

      // Captura soma dos pontos de defesa e modificadores de defesa
      let totalDefensePoints = Math.max( Number( defense.body.assignedPoints.text ) + Number( defenseModifiers.body.assignedPoints.text ), 0 );

      // Subtrai pontos totais de defesa dos pontos totais de ataque
      let inflictedPoints = totalAttackPoints - totalDefensePoints;

      // Caso pontos finais da parada devam ser retornados sob seus limites mínimo e máximo, realiza essa delimitação
      if( !isUnboundedPoints ) inflictedPoints = Math.min( Number( attack.body.assignedPoints.text ), Math.max( 0, inflictedPoints ) );

      // Retorna pontos de ataque a serem infligidos
      return inflictedPoints;
    }

    /// Alterna visualização de componentes, entre os visualizadores de dano resumido e extenso
    component.toggleVisibleDamagePreview = function ( eventData = {} ) {
      // Identificadores
      var clickedComponent = eventData.currentTarget,
          elementFooter = this.footer,
          { damagePreview: { short: shortPreview, long: longPreview } } = elementFooter,
          hoverText = m.app.pixi.stage.children.find( child => child.name == 'combat-break-footer-hover-text' );

      // Caso exista um texto suspenso do rodapé da parada de combate, remove-o
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Alterna visualização dos visualizadores de dano
      [ shortPreview.visible, longPreview.visible ] = [ !shortPreview.visible, !longPreview.visible ];

      // Caso visualizador extenso de dano seja o agora visível, define dano de referência do visualizador como igual ao que foi clicado
      if( longPreview.visible ) longPreview.sourceDamage = clickedComponent.source;

      // Atualiza dano a ser mostrado
      return this.updateDamagePreview();
    }

    /// Atualiza visualizador de dano visível
    component.updateDamagePreview = function ( bodySection ) {
      // Não executa função caso uma seção tenha sido passada e ela esteja desabilitada
      if( bodySection && !bodySection.isEnabled ) return false;

      // Identificadores
      var { attack: { maneuver }, damages, defender, footer: elementFooter } = this,
          attackPoints = this.gaugeInflictedAttack(),
          fateModifier = this.getFateModifier(),
          combatBreakTexts = m.languages.components.combatBreak();

      // Itera por danos da parada de combate
      for( let i = 0; i < damages.length; i++ ) {
        // Identificadores
        let damage = damages[ i ];

        // Atualiza pontos totais do dano alvo
        damage.setTotalPoints( defender, attackPoints, i ? 0 : maneuver.penetration, fateModifier );
      }

      // Delega continuidade da atualização para a função pertinente do visualizador atualmente visível
      return this.footer.damagePreview.short.visible ? updateDamageShortPreview.call( this ) : updateDamageLongPreview.call( this );

      // Funções

      /// Atualiza visualizador resumido de dano
      function updateDamageShortPreview() {
        // Identificadores
        var { damagePreview: { short: shortPreview } } = elementFooter;

        // Itera por danos da parada de combate
        for( let i = 0; i < damages.length; i++ ) {
          // Identificadores
          let damage = damages[ i ],
              totalDamage = damage.totalPoints,
              previousSeparator = shortPreview.separators[ i - 1 ],
              [ currentDamage, currentSeparator ] = [ shortPreview.damages[ i ], shortPreview.separators[ i ] ],
              isValidInfliction = !damage.validateInfliction || damage.validateInfliction( defender );

          // Atualiza valor exibido do dano previsto atinente
          currentDamage.text = totalDamage.toString();

          // Tinge texto em função de se infligimento do dano atinente é válido ou não
          currentDamage.tint = isValidInfliction ? m.data.colors.white : m.data.colors.red;

          // Atualiza texto suspenso sobre o dano atinente
          currentDamage.hoverText = combatBreakTexts.getTotalDamageHoverText( damage, !isValidInfliction, defender );

          // Atualiza posicionamento horizontal dos componentes atuais do dano resumido

          /// Dano previsto
          currentDamage.x = previousSeparator ? previousSeparator.x + previousSeparator.width : 0;

          /// Separador, se existente
          if( currentSeparator ) currentSeparator.x = currentDamage.x + currentDamage.width;
        }

        // Centraliza horizontalmente visualizador
        shortPreview.x = elementFooter.width * .5 - shortPreview.width * .5;
      }

      /// Atualiza visualizador extenso de dano
      function updateDamageLongPreview() {
        // Identificadores
        var { damagePreview: { long: longPreview } } = elementFooter,
            damage = longPreview.sourceDamage,
            damageName = damage.name.replace( /^damage-/, '' ),
            totalDamage = damage.totalPoints,
            defenderNaturalResistances = defender.content.stats.effectivity.resistances.natural.current,
            defenderGarments = defender.garments ?? {};

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( damages.includes( damage ) );

        // Atualização de textos

        /// Pontos de ataque
        longPreview.modifiers.attackPoints.text = attackPoints.toString();

        /// Dano bruto
        longPreview.modifiers.rawDamage.text = damage.singlePoints.toString();

        /// Texto suspenso do dano bruto
        longPreview.modifiers.rawDamage.hoverText = combatBreakTexts.getRawDamageHoverText( damage );

        /// Resistência natural
        longPreview.modifiers.naturalResistance.text =
          defenderNaturalResistances[ damageName ] == Infinity ? '∞' : defenderNaturalResistances[ damageName ].toString();

        /// Resistência do traje primário
        longPreview.modifiers.primaryGarmentResistance.text =
          defenderGarments.primary?.resistances?.[ damageName ] == Infinity ? '∞' : defenderGarments.primary?.resistances?.[ damageName ].toString() ?? '0';

        /// Resistência do traje secundário
        longPreview.modifiers.secondaryGarmentResistance.text =
          defenderGarments.secondary?.resistances?.[ damageName ] == Infinity ? '∞' : defenderGarments.secondary?.resistances?.[ damageName ].toString() ?? '0';

        /// Penetração
        longPreview.modifiers.penetration.text = damages[ 0 ] == damage ? maneuver.penetration.toString() : '0';

        /// Modificador de destino
        longPreview.modifiers.fateModifier.text = ( function () {
          // Define modificador para caso manobra relativa ao dano seja 'Arrebatar' e sua origem seja um espírito
          if( damage.maneuver instanceof m.ManeuverRapture && damage.source instanceof m.Spirit ) return fateModifier >= 0 ? '1' : '0';

          // Define modificador para as demais situações
          return !fateModifier ? '1' : fateModifier > 0 ? '2' : '.5';
        } )();

        /// Ajusta modificador de destino

        /// Dano resultante
        longPreview.modifiers.resultingDamage.text = totalDamage.toString();

        // Itera por modificadores do visualizador extenso de dano
        for( let i = 0; i < longPreview.modifiers.length; i++ ) {
          // Identificadores
          let previousSeparator = longPreview.separators[ i - 1 ],
              [ currentModifier, currentSeparator ] = [ longPreview.modifiers[ i ], longPreview.separators[ i ] ];

          // Atualiza posicionamento dos componentes

          /// Modificador atual
          currentModifier.x = previousSeparator ? previousSeparator.x + previousSeparator.width : 0;

          /// Separador, se existente
          if( currentSeparator ) currentSeparator.x = currentModifier.x + currentModifier.width;
        }

        // Centraliza horizontalmente visualizador
        longPreview.x = elementFooter.width * .5 - longPreview.width * .5;
      }
    }

    /// Alterna uso de ponto de destino para a parada
    component.toggleFatePointUsage = function ( fateIcon, isValidPlayer = m.GameMatch.current.isSimulation ) {
      // Identificadores
      var { attacker, defender, footer: { fateIcons } } = this,
          targetParity = [ 'odd', 'even' ].find( parity => fateIcons[ parity ] == fateIcon ),
          targetPlayer = isValidPlayer ? m.GameMatch.current.players[ targetParity ] : m.GamePlayer.current,
          attackerPlayer = attacker.getRelationships().controller,
          targetBeing = targetPlayer == attackerPlayer ? attacker : defender,
          combatBreakTexts = m.languages.components.combatBreak(),
          hoverText = m.app.pixi.stage.children.find( child => child.name == 'combat-break-footer-hover-text' );

      // Não executa função caso rodapé esteja desabilitado
      if( !this.footer.isEnabled ) return false;

      // Não executa função caso ícone clicado seja diferente do da paridade do jogador do usuário
      if( targetParity != targetPlayer?.parity ) return false;

      // Não executa função caso jogador alvo não possa usar pontos de destino
      if( !targetPlayer.fatePoints.current.get() && !targetBeing.content.stats.fatePoints.current ) return false;

      // Alterna uso previsto do ponto de destino
      fateIcon.isToUseFatePoint = !fateIcon.isToUseFatePoint;

      // Alterna uso do filtro para escurecimento do ícone
      this.toggleFateIconFilter( fateIcon );

      // Atualiza visualizador de dano
      this.updateDamagePreview();

      // Atualiza texto suspenso do ícone alvo
      fateIcon.hoverText = combatBreakTexts.getFatePointHoverText(
        targetParity, fateIcon.isToUseFatePoint, Boolean( targetBeing.content.stats.fatePoints.current )
      );

      // Para caso cursor esteja sobre ícone alvo e um texto suspenso já exista no rodapé
      if( fateIcon.oCheckCursorOver() && hoverText ) {
        // Remove texto suspenso existente
        m.app.removeHoverText( hoverText );

        // Exibe texto suspenso atualizado do ícone alvo
        m.app.showHoverText.call( fateIcon );
      }

      // Retorna ícone passado
      return fateIcon;
    }

    /// Alterna uso do filtro para escurecimento do ícone
    component.toggleFateIconFilter = function ( fateIcon ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].some( parity => this.footer.fateIcons[ parity ] == fateIcon ) );

      // Identificadores
      var currentFilter = fateIcon.filters.find( filter => filter.name == 'darken-filter' );

      // Varia operação de alternamento em função de se filtro atualmente existe no ícone
      return currentFilter ? removeFilter() : applyFilter();

      // Aplica filtro de escurecimento ao ícone alvo
      function applyFilter() {
        // Gera filtro para escurecimento
        var darkenFilter = new PIXI.filters.ColorMatrixFilter();

        // Configurações do filtro

        /// Nomeação
        darkenFilter.name = 'darken-filter';

        /// Brilho
        darkenFilter.brightness( .6 );

        // Adiciona filtro ao ícone
        fateIcon.filters.push( darkenFilter );
      }

      // Remove filtro de escurecimento do ícone alvo
      function removeFilter() {
        // Remoção do filtro
        fateIcon.filters.splice( fateIcon.filters.indexOf( currentFilter ), 1 ).pop().destroy( { children: true } );
      }
    }

    /// Retorna modificador de destino previsto para a delonga da parada de combate
    component.getFateModifier = function () {
      // Identificadores
      var { attacker, footer: { fateIcons } } = this,
          attackerRelationships = attacker.getRelationships(),
          attackerIcon = fateIcons[ attackerRelationships.controller.parity ],
          defenderIcon = fateIcons[ attackerRelationships.opponent.parity ],
          fateModifier = 0;

      // Caso um ponto de destino esteja para ser usado em favor do atacante, incrementa modificador de destino
      if( attackerIcon.isToUseFatePoint ) fateModifier++;

      // Caso um ponto de destino esteja para ser usado em favor do defensor, decrementa modificador de destino
      if( defenderIcon.isToUseFatePoint ) fateModifier--;

      // Retorna modificador de destino
      return fateModifier;
    }

    /// Retorna escolhas que jogadores fizeram na delonga da parada de combate
    component.getFateChoices = function () {
      // Identificadores
      var { attacker, footer: { fateIcons } } = this,
          attackerRelationships = attacker.getRelationships(),
          attackerIcon = fateIcons[ attackerRelationships.controller.parity ],
          defenderIcon = fateIcons[ attackerRelationships.opponent.parity ],
          playerChoices = {
            attacker: false, defender: false
          };

      // Caso um ponto de destino esteja para ser usado em favor do atacante, indica isso
      if( attackerIcon.isToUseFatePoint ) playerChoices.attacker = true;

      // Caso um ponto de destino esteja para ser usado em favor do defensor, indica isso
      if( defenderIcon.isToUseFatePoint ) playerChoices.defender = true;

      // Retorna as escolhas feitas pelos jogadores
      return playerChoices;
    }

    /// Remove parada de combate
    component.remove = function () {
      // Identificadores
      var modal = this.parent,
          combatBreakBars = m.app.pixi.stage.children.filter( child => child.name?.startsWith( 'combat-break-' ) && child.name.endsWith( '-static-bar' ) ),
          hoverText = m.app.pixi.stage.children.find( child => child.name?.startsWith( 'combat-break-' ) && child.name.endsWith( '-hover-text' ) );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( modal instanceof m.GameModal );

      // Nulifica execução atual da parada de combate
      this.currentExecution = null;

      // Remove do canvas barras estáticas sobre parada do combate, quando existentes
      for( let bar of combatBreakBars ) bar.remove();

      // Se existente, remove do canvas texto suspenso sobre parada de combate
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Remove do canvas modal da parada do combate
      modal.remove();

      // Sinaliza fim da parada de combate
      m.events.breakEnd.combat.emit( m.GameMatch.current, { combatBreak: this } );
    }

    // Atribuição de propriedades de 'body'
    let rootBody = component.body;

    /// Seções do corpo da parada de combate
    rootBody.sections = {};

    /// Dados de estilo
    rootBody.styleData = {};

    /// Monta um visualizador de pontos de manobras
    rootBody.buildPointsDisplayer = function ( bodySection ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );

      // Identificadores
      var { pointsDisplayerPaddingX: paddingX, pointsDisplayerPaddingY: paddingY, dieSize, dieMargin } = this.styleData,
          { pointsDisplayer } = bodySection.body;

      // Limpa renderização atual
      pointsDisplayer.clear();

      // Confecção da nova renderização

      /// Preparação das bordas
      pointsDisplayer.lineStyle( 1, m.data.colors.semiwhite1, 1, 1 );

      /// Preparação do plano de fundo
      pointsDisplayer.beginFill( m.data.colors.gray );

      /// Renderização
      pointsDisplayer.drawRect( 0, 0, paddingX * 2 + dieSize * 10 + dieMargin * 9, dieSize + paddingY * 2 );

      /// Encerramento
      pointsDisplayer.endFill();

      // Inseri dados no visualizador
      pointsDisplayer.dices.forEach( die => this.addDie( bodySection, die ) );

      // Retorna seção passada
      return bodySection;
    }

    /// Monta um dado para uma das seções do corpo da parada de combate
    rootBody.buildDie = function ( bodySection, isModifier = false ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );

      // Identificadores
      var { dieName, dieHoverText } = bodySection,
          { assignedPoints } = bodySection.body,
          isBonus = Number( assignedPoints.text ) >= 0,
          dieIconName = !isBonus ? m.assets.images.icons.stats.penaltyModifier : isModifier ? m.assets.images.icons.stats.modifier : bodySection.dieIconName,
          dieContainer = new PIXI.Container(),
          die = dieContainer.die = new PIXI.Sprite( PIXI.utils.TextureCache[ dieName ] ),
          dieIcon = dieContainer.icon = new PIXI.Sprite( PIXI.utils.TextureCache[ dieIconName ] ),
          combatBreakTexts = m.languages.components.combatBreak();

      // Redimensionamentos

      /// Do dado
      die.oScaleByGreaterSize( this.styleData.dieSize );

      /// De seu ícone
      dieIcon.oScaleByGreaterSize( this.styleData.dieIconSize );

      // Inserção dos componentes do dado em seu contedor
      dieContainer.addChild( die, dieIcon );

      // Posicionamento do ícone do dado
      dieIcon.position.set( dieContainer.width * .5 - dieIcon.width * .5, dieContainer.height * .5 - dieIcon.height * .5 );

      // Interatividade

      /// Torna dado interativo
      die.interactive = true;

      /// Confere a dado seu texto suspenso
      die.hoverText = !isBonus ? combatBreakTexts.diePenaltyHoverText : isModifier ? combatBreakTexts.dieBonusHoverText : dieHoverText;

      /// Atribui um nome ao texto suspenso do dado
      die.hoverTextName = 'combat-break-body-hover-text';

      /// Adiciona evento para exibir texto suspenso do dado
      die.addListener( 'mouseover', m.app.showHoverText );

      // Captura nomes dos componentes do dado
      [ die.name, dieIcon.name ] = [ dieName, dieIconName ];

      // Retorna dado montado
      return dieContainer;
    }

    /// Adiciona dado a um visualizador de pontos
    rootBody.addDie = function ( bodySection, die ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );
        m.oAssert( !die || bodySection.body.pointsDisplayer.dices.includes( die ) );
      }

      // Identificadores
      var { pointsDisplayerPaddingX: paddingX, dieMargin } = this.styleData,
          { pointsDisplayer } = bodySection.body,
          dieIndex = pointsDisplayer.dices.indexOf( die );

      // Para caso não tenha sido passado um dado
      if( !die ) {
        // Monta novo dado, adiciona-o ao visualizador de pontos, e captura seu índice
        dieIndex = pointsDisplayer.dices.push( pointsDisplayer.addChild( this.buildDie( bodySection ) ) ) - 1;

        // Captura dado recém-adicionado
        die = pointsDisplayer.dices[ dieIndex ];
      }

      // Posiciona dado no visualizador, segundo a posição do dado anterior a ele no arranjo de dados
      die.position.set( paddingX + dieIndex * die.width + dieIndex * dieMargin, pointsDisplayer.height * .5 - die.height * .5 );

      // Retorna seção passada
      return bodySection;
    }

    /// Remove dado de um visualizador de pontos
    rootBody.removeDie = function ( bodySection ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );

      // Identificadores
      var { pointsDisplayer } = bodySection.body,
          { dices } = pointsDisplayer;

      // Não executa função caso não haja nenhum dado para ser removido
      if( !dices.length ) return false;

      // Remove último dado do visualizador de pontos
      pointsDisplayer.removeChild( dices.pop() ).destroy( { children: true } );

      // Retorna seção
      return bodySection;
    }

    /// Adiciona ponto a um visualizador de pontos
    rootBody.addPoint = function ( bodySection, source = 'agility' ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );
        m.oAssert( [ 'agility' ].includes( source ) );
      }

      // Identificadores
      var { availablePoints, pointsDisplayer, assignedPoints } = bodySection.body,
          { dices, pointSources } = pointsDisplayer;

      // Não executa função caso seção esteja desabilitada
      if( !bodySection.isEnabled ) return false;

      // Não executa função caso quantidade máxima de pontos esteja presente
      if( Number( assignedPoints.text ) >= bodySection.maxPoints ) return false;

      // Não executa função caso não haja mais pontos a serem adicionados
      if( !Number( availablePoints.text ) ) return false;

      // Atualiza textos sobre pontos

      /// De pontos disponíveis
      availablePoints.text = Number( availablePoints.text ) - 1;

      /// De pontos atribuídos
      assignedPoints.text = Number( assignedPoints.text ) + 1;

      // Atualiza registro sobre quantidade de pontos da origem passada
      pointSources[ source ] = source in pointSources ? pointSources[ source ] + 1 : 1;

      // Controla dados do visualizador de pontos, em função da quantidade atual de pontos atribuídos
      return Number( assignedPoints.text ) > 0 ? this.addDie( bodySection ) : this.removeDie( bodySection );
    }

    /// Remove ponto de um visualizador de pontos
    rootBody.removePoint = function ( bodySection, source = 'agility' ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.values( this.sections ).some( section => section == bodySection ) );
        m.oAssert( [ 'agility' ].includes( source ) );
      }

      // Identificadores
      var { availablePoints, pointsDisplayer, assignedPoints } = bodySection.body,
          { dices, pointSources } = pointsDisplayer;

      // Não executa função caso seção esteja desabilitada
      if( !bodySection.isEnabled ) return false;

      // Não executa função caso quantidade mínima de pontos esteja presente
      if( Number( assignedPoints.text ) <= bodySection.minPoints ) return false;

      // Atualiza textos sobre pontos

      /// De pontos disponíveis
      availablePoints.text = Number( availablePoints.text ) + 1;

      /// De pontos atribuídos
      assignedPoints.text = Number( assignedPoints.text ) - 1;

      // Atualiza registro sobre quantidade de pontos da origem passada
      pointSources[ source ] = source in pointSources ? pointSources[ source ] - 1 : -1;

      // Controla dados do visualizador de pontos, em função da quantidade atual de pontos atribuídos
      return Number( assignedPoints.text ) < 0 ? this.addDie( bodySection ) : this.removeDie( bodySection );
    }

    // Atribuição de propriedades de 'body.styleData'
    let bodyStyleData = rootBody.styleData;

    /// Registra margem superior do corpo
    bodyStyleData.marginTop = 16;

    /// Registra margem inferior do corpo
    bodyStyleData.marginBottom = 24;

    /// Registra margem vertical da seção de defesa
    bodyStyleData.defenseSectionMarginTop = 36;

    /// Registra margem vertical de uma seção de modificador
    bodyStyleData.modifierSectionMarginTop = 12;

    /// Registra tamanho do ícone de título de uma seção
    bodyStyleData.sectionTitleSize = 25;

    /// Registra margem do título de uma seção
    bodyStyleData.sectionTitleMarginRight = 20;

    /// Registra espaço horizontal de início e fim de um visualizador de dados
    bodyStyleData.pointsDisplayerPaddingX = 6;

    /// Registra espaço vertical de início e fim de um visualizador de dados
    bodyStyleData.pointsDisplayerPaddingY = 5;

    /// Registra tamanho de um dado
    bodyStyleData.dieSize = 25;

    /// Registra tamanho do ícone de um dado
    bodyStyleData.dieIconSize = 12;

    /// Registra margem entre dados
    bodyStyleData.dieMargin = 6;

    /// Registra tamanho das setas de uma seção
    bodyStyleData.arrowSize = 16;
  }
}

/// Propriedades do protótipo
CombatBreakElement.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: CombatBreakElement }
} );
