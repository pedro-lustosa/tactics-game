// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const GameTable = function ( config = {} ) {
  // Iniciação de propriedades
  GameTable.init( this );

  // Identificadores
  var { name, captionText, dataFeed, columns, styleData = {}, selectableRows } = config,
      { heading: tableHeading, body: tableBody } = this.sections,
      [ tableCaption, headingColumns ] = [ tableHeading.caption, tableHeading.columns ],
      [ bodyRows, bodyColumns ] = [ tableBody.rows, tableBody.columns ];

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( typeof captionText == 'string' );
    m.oAssert( Array.isArray( dataFeed ) );
    m.oAssert( columns.constructor instanceof Object );
    m.oAssert( Object.keys( columns ).length >= 2 );
    m.oAssert( Object.values( columns ).every( column => typeof column?.displayName == 'string' && typeof column.content == 'function' ) );
    m.oAssert( !styleData.borderCoverage || [ 'cell', 'table', 'none' ].includes( styleData.borderCoverage ) );
  }

  // Superconstrutor
  PIXI.Graphics.call( this );

  // Atribuição de propriedades pós-superconstrutor

  /// Nome
  if( name ) this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;

  /// Adiciona coluna de ordem às demais, se desejada
  if( config.orderColumn ) columns.order = {
    displayName: m.languages.snippets.order, content: () => '0', order: -10
  };

  /// Ajusta configurações do estilo da tabela, se aplicável
  for( let key in styleData )
    if( key in this.styleData ) this.styleData[ key ] = styleData[ key ];

  /// Inserção de elementos
  this.addChild( tableHeading, tableBody );

  /// Conteúdo da tabela
  tableContent: {
    // Identificadores
    let columnNames = Object.keys( columns ).sort( ( a, b ) => columns[ a ].order ?? 0 - columns[ b ].order ?? 0 ),
        captionTextStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -19 },
        headingTextStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 },
        bodyTextStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -15 };

    // Inserção do título da tabela
    tableCaption.bitmap = tableCaption.addChild( new PIXI.BitmapText( captionText, captionTextStyle ) );

    // Iteração por nomes das colunas
    for( let columnName of columnNames ) {
      // Atribuição da coluna alvo no cabeçalho
      headingColumns[ columnName ] = tableHeading.addChild( new PIXI.Graphics() );

      // Atribuição do texto da coluna alvo no cabeçalho
      headingColumns[ columnName ].bitmap = headingColumns[ columnName ].addChild( new PIXI.BitmapText( columns[ columnName ].displayName, headingTextStyle ) );
    }

    // Geração das fileiras do corpo da tabela
    for( let i = 0; i < dataFeed.length; i++ ) {
      // Geração e inserção de nova fileira
      bodyRows.push( tableBody.addChild( new PIXI.Graphics() ) );

      // Captura objeto de dados a popular fileira alvo
      bodyRows[ i ].source = dataFeed[ i ];

      // Atribuição de colunas à nova fileira
      bodyRows[ i ].columns = {};

      // Caso se deseje uma coluna de ordem, atualiza sua função de conteúdo
      if( config.orderColumn ) columns.order.content = () => ( i + 1 ).toString();

      // Iteração por nomes das colunas
      for( let columnName of columnNames ) {
        // Identificadores
        let rowColumns = bodyRows[ i ].columns;

        // Atribuição da coluna alvo no corpo
        rowColumns[ columnName ] = bodyRows[ i ].addChild( new PIXI.Graphics() );

        // Atribuição do texto da coluna alvo no corpo
        rowColumns[ columnName ].bitmap = rowColumns[ columnName ].addChild( new PIXI.BitmapText(
          columns[ columnName ].content( dataFeed[ i ] ), bodyTextStyle
        ) );

        // Inicia coluna no objeto de colunas do corpo da tabela
        bodyColumns[ columnName ] ??= [];

        // Captura coluna alvo da fileira atual
        bodyColumns[ columnName ].push( rowColumns[ columnName ] );
      }
    }
  }

  /// Formatação da tabela
  tableStyle: {
    // Identificadores
    let headingColumnCells = Object.values( headingColumns );

    // Renderiza células da tabela
    this.drawCells();

    // Posiciona verticalmente células

    /// Do cabeçalho
    for( let cell of headingColumnCells ) cell.y = tableCaption.y + tableCaption.height;

    /// Do corpo

    //// Contedor
    tableBody.y = tableHeading.y + tableHeading.height;

    //// Fileiras
    tableBody.fitRows();
  }

  /// Interatividade da tabela
  tableInteraction: {
    // Não executa bloco caso fileiras do corpo da tabela não sejam selecionáveis
    if( !selectableRows ) break tableInteraction;

    // Concede a cada fileira da tabela configurações para se tornarem selecionáveis
    for( let row of bodyRows ) tableBody.makeRowSelectable( row );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GameTable.init = function ( component ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( component instanceof GameTable );

    // Atribuição de propriedades iniciais

    /// Nome
    component.name = 'game-table';

    /// Seções
    component.sections = {};

    /// Dados de estilo
    component.styleData = {};

    /// Renderiza células da tabela
    component.drawCells = function () {
      // Identificadores
      var { heading: tableHeading, body: tableBody } = this.sections,
          { caption: tableCaption, columns: headingColumns } = tableHeading,
          { rows: bodyRows, columns: bodyColumns } = tableBody,
          [ headingColumnCells, bodyColumnCells ] = [ Object.values( headingColumns ), Object.values( bodyColumns ) ],
          tableColumns = bodyColumnCells.length ?
            bodyColumnCells.map( ( columnCells, index ) => [ headingColumnCells[ index ], ...columnCells ] ) : headingColumnCells.map( cell => [ cell ] ),
          largestWidths = tableColumns.map(
            cells => cells.reduce( ( accumulator, current ) => accumulator.bitmap.width < current.bitmap.width ? current : accumulator )
          ).map( cell => cell.bitmap.width );

      // Renderiza células, de modo que todas tenham a largura da maior célula de sua coluna
      for( let i = 0; i < tableColumns.length; i++ ) tableColumns[ i ].forEach( cell => drawCell.call( this, cell, largestWidths[ i ], i == 0 ) );

      // Posiciona horizontalmente células
      positionCellsX.call( this );

      // Renderiza a célula do título
      drawCaption.call( this );

      // Adequa a largura das células da tabela, para caso a célula do título seja maior que as demais
      fitCellsWidth: {
        // Identificadores
        let headingCellsWidth = headingColumnCells.reduce( ( accumulator, current ) => accumulator += current.width, 0 ),
            extraWidth = ( tableCaption.width - headingCellsWidth ) / tableColumns.length;

        // Não executa bloco caso título não seja mais largo que o conjunto de células do cabeçalho
        if( tableCaption.width <= headingCellsWidth ) break fitCellsWidth;

        // Redimensiona células para que cada fileira seja de largura igual à da célula do título
        for( let i = 0; i < tableColumns.length; i++ )
          tableColumns[ i ].forEach( cell => drawCell.call( this, cell, largestWidths[ i ] + extraWidth, i == 0 ) );

        // Reposiciona horizontalmente células
        positionCellsX.call( this );
      }

      // Retorna tabela
      return this;

      // Renderiza uma célula padrão
      function drawCell( targetCell, contentWidth, isFirst = false ) {
        // Identificadores
        var cellText = targetCell.bitmap,
            { headingBackgroundColor, borderCoverage, borderWidth, borderColor } = this.styleData;

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( headingColumnCells.concat( bodyColumnCells.flat() ).some( cell => cell == targetCell ) );

        // Zera espessura da borda, para caso célula não a renderize
        if( borderCoverage != 'cell' ) borderWidth = 0;

        // Limpa renderização atual
        targetCell.clear();

        // Define novas medidas da célula
        let [ cellWidth, cellHeight ] = [ contentWidth + this.styleData.cellPaddingX, targetCell.height + this.styleData.cellPaddingY ];

        // Atribui medidas das células, e, se aplicável, suas bordas

        /// Cor das bordas
        if( borderCoverage == 'cell' ) targetCell.beginFill( borderColor );

        /// Delimitação esquerda
        if( isFirst ) targetCell.drawRect( 0, 0, borderWidth, cellHeight );

        /// Delimitação direita
        targetCell.drawRect( cellWidth - borderWidth, 0, borderWidth, cellHeight );

        /// Delimitação inferior
        targetCell.drawRect( 0, cellHeight - borderWidth, cellWidth, borderWidth );

        /// Encerramento da renderização das bordas
        if( borderCoverage == 'cell' ) targetCell.endFill();

        // Renderiza plano de fundo da célula, se ela for uma do cabeçalho
        if( headingColumnCells.includes( targetCell ) ) {
          // Identificadores
          let [ xStart, yStart ] = [ isFirst ? borderWidth : 0, 0 ],
              [ bodyWidth, bodyHeight ] = [ cellWidth - borderWidth * ( isFirst ? 2 : 1 ), cellHeight - borderWidth ];

          // Confecção do plano de fundo

          /// Cor de preenchimento
          targetCell.beginFill( headingBackgroundColor );

          /// Renderização
          targetCell.drawRect( xStart, yStart, bodyWidth, bodyHeight );

          /// Encerramento
          targetCell.endFill();

          // Atribui cor contrastiva ao texto da célula
          cellText.tint = m.data.colors.contrastWith( headingBackgroundColor );
        }

        // Posiciona texto da célula
        cellText.position.set( targetCell.width * .5 - cellText.width * .5, targetCell.height * .5 - cellText.height * .5 );

        // Retorna célula
        return targetCell;
      }

      // Renderiza a célula do título
      function drawCaption() {
        // Identificadores
        var captionText = tableCaption.bitmap,
            { captionBackgroundColor, borderCoverage, borderWidth, borderColor, cellPaddingX, cellPaddingY } = this.styleData;

        // Limpa renderização atual
        tableCaption.clear();

        // Renderiza célula

        /// Definição das bordas, se aplicável
        if( borderCoverage == 'cell' ) tableCaption.lineStyle( borderWidth, borderColor, 1, 0 );

        /// Cor de preenchimento
        tableCaption.beginFill( captionBackgroundColor );

        /// Renderização do plano de fundo
        tableCaption.drawRect( 0, 0, Math.max( tableCaption.width + cellPaddingX, this.width ), tableCaption.height + cellPaddingY );

        /// Encerramento
        tableCaption.endFill();

        // Posiciona texto do título
        captionText.position.set( tableCaption.width * .5 - captionText.width * .5, tableCaption.height * .5 - captionText.height * .5 );

        // Atribui cor contrastiva ao texto do título
        captionText.tint = m.data.colors.contrastWith( captionBackgroundColor );

        // Retorna célula do título
        return tableCaption;
      }

      // Posiciona horizontalmente células
      function positionCellsX() {
        // Posiciona células do cabeçalho
        for( let i = 0, previousCell; i < headingColumnCells.length; i++, previousCell = headingColumnCells[ i - 1 ] )
          headingColumnCells[ i ].x = previousCell ? previousCell.x + previousCell.width : 0;

        // Posiciona células do corpo
        for( let row of bodyRows ) {
          // Identificadores
          let rowColumns = Object.values( row.columns );

          // Posicionamento da célula
          for( let i = 0, previousCell; i < rowColumns.length; i++, previousCell = rowColumns[ i - 1 ] )
            rowColumns[ i ].x = previousCell ? previousCell.x + previousCell.width : 0;
        }
      }
    }

    /// Renderiza bordas da tabela
    component.drawBorders = function () {
      // Identificadores
      var { borderCoverage, borderWidth, borderColor } = this.styleData;

      // Limpa renderização atual
      this.clear();

      // Apenas renderiza novas bordas caso elas sejam aplicáveis à tabela
      if( borderCoverage != 'table' ) return false;

      // Renderização das bordas

      /// Definição
      this.lineStyle( borderWidth, borderColor, 1, 1 );

      /// Renderização
      this.drawRect( 0, 0, this.width, this.height );

      // Retorna tabela
      return this;
    }

    // Atribuição de propriedades de 'sections'
    let rootSections = component.sections;

    /// Cabeçalho
    rootSections.heading = new PIXI.Container();

    /// Corpo
    rootSections.body = new PIXI.Container();

    // Atribuição de propriedades de 'sections.heading'
    let heading = rootSections.heading;

    /// Título
    heading.caption = heading.addChild( new PIXI.Graphics() );

    /// Colunas
    heading.columns = {};

    // Atribuição de propriedades de 'sections.body'
    let body = rootSections.body;

    /// Colunas
    body.columns = {};

    /// Fileiras
    body.rows = [];

    /// Fileira selecionada
    body.selectedRow = null;

    /// Renderiza todas as fileiras
    body.drawRows = function () {
      // Renderização das fileiras
      for( let row of this.rows ) this.drawRow( row );

      // Retorna corpo da tabela
      return this;
    }

    /// Renderiza uma fileira
    body.drawRow = function ( targetRow ) {
      // Identificadores
      var targetCells = Object.values( targetRow.columns ?? {} ),
          targetColor = ( function () {
            switch( this.rows.indexOf( targetRow ) % 2 ) {
              case 0:
                return this.selectedRow == targetRow ? component.styleData.bodySelectedOddRowsColor : component.styleData.bodyOddRowsColor;
              default:
                return this.selectedRow == targetRow ? component.styleData.bodySelectedEvenRowsColor : component.styleData.bodyEvenRowsColor;
            }
          } ).call( this );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( this.rows ).some( row => row == targetRow ) );

      // Limpa renderização atual
      targetRow.clear();

      // Renderiza plano de fundo da fileira

      /// Cor de preenchimento
      targetRow.beginFill( targetColor );

      /// Renderização
      targetRow.drawRect( 0, 0, targetRow.width, targetRow.height );

      /// Encerramento
      targetRow.endFill();

      // Atribui cor a textos da fileira

      /// Definição da cor
      let textColor = m.data.colors.contrastWith( targetColor );

      /// Atribuição da cor
      for( let cell of targetCells ) cell.bitmap.tint = textColor;

      // Retorna fileira alvo
      return targetRow;
    }

    /// Adequa o posicionamento das fileiras
    body.fitRows = function () {
      // Itera por fileiras do corpo
      for( let i = 0, previousRow; i < this.rows.length; i++, previousRow = this.rows[ i - 1 ] ) {
        // Atualiza posição vertical da fileira alvo
        this.rows[ i ].y = previousRow ? previousRow.y + previousRow.height : 0;

        // Renderiza plano de fundo da fileira
        this.drawRow( this.rows[ i ] );

        // Para caso haja uma coluna de ordem
        if( this.rows[ i ].columns.order ) {
          // Identificadores
          let orderColumn = this.rows[ i ].columns.order;

          // Atualiza seu texto
          orderColumn.bitmap.text = ( i + 1 ).toString();

          // Atualiza posicionamento horizontal de seu texto
          orderColumn.bitmap.x = orderColumn.width * .5 - orderColumn.bitmap.width * .5;
        }
      }

      // Se aplicável, atualiza bordas da tabela
      if( component.styleData.borderCoverage == 'table' ) component.drawBorders();

      // Retorna corpo da tabela
      return this;
    }

    /// Gera uma fileira
    body.generateRow = function ( rowData, targetIndex ) {
      // Identificadores
      var newRow = new PIXI.Graphics(),
          rowTextStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -15 };

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( rowData.constructor == Object );
        m.oAssert( [ 'source', 'columns' ].every( property => rowData[ property ] ) );
      }

      // Atribuição de propriedades à nova fileira

      /// Origem
      newRow.source = rowData.source;

      /// Colunas
      newRow.columns = {};

      /// Coluna de ordem, se aplicável
      if( this.columns.order ) rowData.columns.order = ( targetIndex + 1 ).toString();

      // Conteúdo da fileira
      rowContent: {
        // Identificadores
        let columnNames = Object.keys( rowData.columns );

        // Caso exista, ajusta posição da coluna de ordem
        if( columnNames.includes( 'order' ) ) columnNames.unshift( columnNames.pop() );

        // Itera por nome das colunas da nova fileira
        for( let columnName of columnNames ) {
          // Identificadores
          let columnValue = rowData.columns[ columnName ];

          // Geração da coluna alvo
          newRow.columns[ columnName ] = newRow.addChild( new PIXI.Graphics() );

          // Atribuição do texto da coluna alvo
          newRow.columns[ columnName ].bitmap = newRow.columns[ columnName ].addChild( new PIXI.BitmapText(
            columnValue, rowTextStyle
          ) );
        }
      }

      // Inserção da fileira
      this.addRow( newRow, targetIndex, true );

      // Formatação da fileira
      rowStyle: {
        // Renderiza novamente células da tabela
        component.drawCells();

        // Realinha verticalmente fileiras da tabela
        this.fitRows();
      }

      // Interatividade da fileira
      rowInteraction: {
        // Não executa bloco caso fileiras já existentes não sejam interativas
        if( !this.rows[0].interactive ) break rowInteraction;

        // Configura nova fileira para que seja selecionável
        this.makeRowSelectable( newRow );
      }

      // Retorna fileira gerada
      return newRow;
    }

    /// Adiciona uma fileira
    body.addRow = function ( targetRow, targetIndex, isGeneratedRow = false ) {
      // Identificadores
      var columnNames = Object.keys( this.columns ),
          targetRowColumns = Object.values( targetRow.columns );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.keys( targetRow.columns ).every( column => columnNames.includes( column ) ) );
        m.oAssert( targetIndex >= 0 && targetIndex <= this.rows.length );
      }

      // Não executa função caso fileira alvo já exista na tabela
      if( this.rows.includes( targetRow ) ) return false;

      // Adiciona fileira ao objeto de colunas
      Object.values( this.columns ).forEach( ( rows, index ) => rows.splice( targetIndex, 0, targetRowColumns[ index ] ) );

      // Adiciona fileira ao arranjo de fileiras
      this.rows.splice( targetIndex, 0, targetRow );

      // Adiciona fileira ao corpo da tabela
      this.addChildAt( targetRow, targetIndex );

      // Readequa fileiras da tabela, caso fileira alvo já esteja formatada
      if( !isGeneratedRow ) this.fitRows();

      // Retorna corpo da tabela
      return this;
    }

    /// Remove uma fileira
    body.removeRow = function ( targetRow, isToKeep = false ) {
      // Identificadores
      var targetRowIndex = this.rows.indexOf( targetRow );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetRowIndex != -1 );

      // Caso fileira alvo seja a selecionada, desfaz essa relação
      if( this.selectedRow == targetRow ) this.selectedRow = null;

      // Remove fileira do objeto de colunas
      Object.values( this.columns ).forEach( rows => rows.splice( targetRowIndex, 1 ) );

      // Remove fileira do arranjo de fileiras
      this.rows.splice( targetRowIndex, 1 );

      // Remove fileira da tabela
      this.removeChild( targetRow );

      // Destrói fileira, caso ela não deva ser mantida
      if( !isToKeep ) targetRow.destroy( { children: true } );

      // Adequa fileiras remanescentes
      return this.fitRows();
    }

    /// Altera fileira selecionada para a passada
    body.changeSelectedRow = function ( targetRow ) {
      // Identificadores
      var currentSelectedRow = this.selectedRow,
          rowsSet = [ targetRow ].concat( currentSelectedRow ?? [] );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.rows.includes( targetRow ) );

      // Não executa função se fileira alvo já for a selecionada
      if( currentSelectedRow == targetRow ) return targetRow;

      // Alteração da fileira selecionada
      this.selectedRow = targetRow;

      // Redefinição das cores das fileiras afetadas
      for( let row of rowsSet ) this.drawRow( row );

      // Retorna fileira selecionada
      return targetRow;
    }

    /// Torna uma fileira selecionável
    body.makeRowSelectable = function ( targetRow ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.rows.includes( targetRow ) );

      // Torna fileira alvo interativa
      targetRow.buttonMode = targetRow.interactive = true;

      // Eventos

      /// Para emitir som ao clicar em fileira alvo
      targetRow.addListener( 'click', GameTable.playRowClickSound.bind( this, targetRow ) );

      /// Para alterar fileira selecionada para a alvo
      targetRow.addListener( 'click', this.changeSelectedRow.bind( this, targetRow ) );
    }

    // Atribuição de propriedades de 'styleData'
    let styleData = component.styleData;

    /// Cobertura das bordas
    styleData.borderCoverage = 'table';

    /// Espessura da borda
    styleData.borderWidth = 1;

    /// Cor da borda
    styleData.borderColor = m.data.colors.black;

    /// Preenchimento horizontal de células
    styleData.cellPaddingX = 12;

    /// Preenchimento vertical de células
    styleData.cellPaddingY = 8;

    /// Cor do plano de fundo de células de título
    styleData.captionBackgroundColor = m.data.colors.black;

    /// Cor do plano de fundo de células do cabeçalho
    styleData.headingBackgroundColor = m.data.colors.semiblack5;

    /// Cor do plano de fundo de fileiras ímpares do corpo
    styleData.bodyOddRowsColor = m.data.colors.semiwhite2;

    /// Cor do plano de fundo de fileiras pares do corpo
    styleData.bodyEvenRowsColor = m.data.colors.semiwhite4;

    /// Cor do plano de fundo de fileiras ímpares selecionadas
    styleData.bodySelectedOddRowsColor = m.data.colors.fadedBeige;

    /// Cor do plano de fundo de fileiras pares selecionadas
    styleData.bodySelectedEvenRowsColor = m.data.colors.fadedBeige;
  }
}

/// Propriedades do protótipo
GameTable.prototype = Object.create( PIXI.Graphics.prototype, {
  // Construtor
  constructor: { value: GameTable }
} );

// Métodos não configuráveis de 'GameTable'
Object.defineProperties( GameTable, {
  // Emite som ao clicar em fileira
  playRowClickSound: {
    value: function ( targetRow ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.rows.includes( targetRow ) );

      // Emite som em função de se fileira alvo já está selecionada ou não
      return m.assets.audios.soundEffects[ this.selectedRow == targetRow ? 'click-deny' : 'click-next' ].play();
    }
  }
} );
