// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const MatchScreen = function ( config = {} ) {
  // Iniciação de propriedades
  MatchScreen.init( this );

  // Identificadores
  var { matchName, matchUsers, matchDecks } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( matchName && typeof matchName == 'string' );
    m.oAssert( [ matchUsers, matchDecks ].every( argument => Array.isArray( argument ) && argument.length == 2 ) );
    m.oAssert( matchUsers.every( user => user instanceof m.GameUser ) );
    m.oAssert( matchDecks.every( deck => deck instanceof m.MainDeck ) );
  }

  // Superconstrutor
  m.GameScreen.call( this, config );

  // Configurações pós-superconstrutor

  /// Criação e captura da partida da tela
  this.match = m.GameMatch.current = new m.GameMatch( {
    name: matchName, users: matchUsers, decks: matchDecks
  } );

  /// Eventos

  //// Para configurar entrada da tela
  m.events.screenChangeEnd.enter.add( this, MatchScreen.setupScreenEntering );

  //// Para configurar saída da tela
  m.events.screenChangeStart.leave.add( this, MatchScreen.setupScreenLeaving );
};

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  MatchScreen.init = function ( matchScreen ) {
    // Chama 'init' ascendente
    m.GameScreen.init( matchScreen );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( matchScreen instanceof MatchScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    matchScreen.name = 'match-screen';

    /// Músicas
    Object.assign( matchScreen.soundTracks, {
      // Música para estágios que não o período do combate
      'goliath': m.assets.audios.soundTracks[ 'goliath' ],
      // Música para o período do combate
      'good-day-to-die': m.assets.audios.soundTracks[ 'good-day-to-die' ]
    } );

    /// Barra de informações
    matchScreen.infoBar = new PIXI.Graphics();

    /// Partida
    matchScreen.match = null;

    /// Cronômetros
    matchScreen.timers = {};

    /// Sai da tela atual
    matchScreen.exit = function ( event ) {
      // Para caso função seja executada via evento de teclado
      if( event?.type == 'keydown' ) {
        // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
        if( event.repeat ) return;

        // Apenas executa função caso teclas pressionadas sejam válidas
        if( event.key != 'Escape' ) return;
      }

      // Identificadores
      var match =  m.GameMatch.current,
          userPlayer = m.GamePlayer.current;

      // Retorna modal confirmando sobre saída da tela
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( userPlayer && !match.isSimulation ? 'concede-match' : 'leave-match' ),
        acceptAction: () => leaveScreen(),
        isFromUser: true
      } ).insert();

      // Executa procedimentos para a saída provocada da tela
      function leaveScreen() {
        // Para caso usuário a sair da partida seja um jogador e partida não seja uma simulação
        if( userPlayer && !match.isSimulation ) {
          // Indica que jogador a desistir da partida deve perder
          userPlayer.isToWin = false;

          // Indica que jogador a permanecer na partida deve ganhar
          match.players[ userPlayer.parity == 'odd' ? 'even' : 'odd' ].isToWin = true;

          // Indica que partida foi concluída
          match.stats.isFinished = true;

          // Propaga decisão do jogador de desistir da partida
          m.sockets.current.emit( 'concede-match', {
            matchName: match.name,
            data: { playerParity: userPlayer.parity }
          } );
        }

        // Altera tela da partida para a de abertura
        return m.GameScreen.change( new m.OpeningScreen() );
      }
    }

    // Atribuição de propriedades de 'frames'
    let frames = matchScreen.frames;

    /// Controla visibilidade de molduras, e o conteúdo a ser exibido nelas
    frames.controlVisibility = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: arrangementStep } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && [ 'begin', 'finish' ].includes( eventType ) && arrangementStep instanceof m.ArrangementStep );

      // Identificadores
      var deploymentStep = m.GameMatch.current.flow.phase.getChild( 'deployment-step' ),
          arrangementName = arrangementStep.name + '-' + arrangementStep.counter,
          deploymentName = deploymentStep.name + '-' + deploymentStep.counter;

      // Executa operações em função de se etapa do arranjo foi iniciada ou encerrada
      switch( eventType ) {
        case 'begin':
          // Oculta molduras das reservas, e as configura para que exibam registros da partida atual
          for( let frame of [ 0, 1 ] ) this[ frame ].hide().content.init( `${ arrangementName }-log` );

          // Exibe molduras do campo, e limpa seu conteúdo
          for( let frame of [ 2, 3 ] ) this[ frame ].show().content.clear();

          // Encerra operação
          break;
        case 'finish':
          // Oculta molduras do campo, e as configura para que exibam registros da partida atual
          for( let frame of [ 2, 3 ] ) this[ frame ].hide().content.init( `${ deploymentName }-log` );

          // Exibe molduras das reservas, e limpa seu conteúdo
          for( let frame of [ 0, 1 ] ) this[ frame ].show().content.clear();
      }
    }

    // Atribuição de propriedades de 'infoBar'
    let infoBar = matchScreen.infoBar;

    /// Seções
    infoBar.sections = {};

    /// Renderiza barra de informações
    infoBar.draw = function () {
      // Identificadores
      var { general: generalSection, odd: oddSection, even: evenSection } = this.sections,
          barHeight = this.height;

      // Limpa renderização atual
      this.clear();

      // Renderização do botão

      /// Preenchimento
      this.beginFill( m.data.colors.semiblack4 );

      /// Confecção
      this.drawRect( 0, 0, m.app.pixi.screen.width, barHeight );

      /// Finalização
      this.endFill();

      // Posicionamento dos contedores

      /// Contedor da seção geral
      generalSection.position.set( this.width * .5 - generalSection.width * .5, barHeight * .5 - generalSection.height * .5 );

      /// Contedor da seção ímpar
      oddSection.position.set( 0 );

      /// Contedor da seção par
      evenSection.position.set( this.width - evenSection.width, 0 );

      // Força altura da barra de informações a continuar igual ao valor original
      this.height = barHeight;
    }

    /// Monta menu de fluxo
    infoBar.buildFlowMenu = function ( eventData ) {
      // Identificadores
      var flowContainer = this.sections.general.flow,
          flowMenu = matchScreen.children.find( child => child.name == 'flow-toggleable-menu' ),
          isOnlyToRemoveMenu = flowMenu?.visible;

      // Para caso já exista um menu de fluxo
      if( flowMenu ) {
        // Retira o evento de ocultamento do menu de fluxo atual
        matchScreen.removeListener( 'click', flowMenu.disposeOnBlur, flowMenu );

        // Remove da tela menu de fluxo atual
        matchScreen.removeChild( flowMenu ).destroy( { children: true } );

        // Caso execução da função seja apenas para remover o menu de fluxo atual, encerra-a
        if( isOnlyToRemoveMenu ) return;
      }

      // Adiciona à tela novo menu de fluxo
      flowMenu = matchScreen.addChild( new m.ToggleableMenu( {
        name: 'flow-toggleable-menu',
        triggers: [ flowContainer ],
        options: setMenuOptions.call( this ),
        styleData: {
          optionsText: { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 }
        },
        isSilent: true
      } ) );

      // Itera por opção do menu
      for( let stageName in flowMenu.options ) {
        // Identificadores
        let menuOption = flowMenu.options[ stageName ];

        // Adiciona evento para forçar aparência do cursor para a do modo informativo caso se esteja nele
        menuOption.addListener( 'mouseover', () => setTimeout( () => m.app.view.style.cursor = m.app.isInInfoMode ? 'help' : 'pointer' ) );

        // Adiciona evento emissão de som de clique ao se clicar na opção
        menuOption.addListener( 'click', () =>
          m.assets.audios.soundEffects[ m.app.isInInfoMode || !flowMenu.visible ? 'click-next' : 'click-deny' ].play()
        );
      }

      // Posiciona menu de fluxo
      flowMenu.position.set( flowContainer.getBounds().x, this.y + this.height );

      // Torna menu de fluxo visível
      flowMenu.visible = true;

      // Monta opções do menu de fluxo
      function setMenuOptions() {
        // Identificadores
        var currentStage = matchScreen.match.flow.round,
            options = {};

        // Itera por estágios atuais da partida
        do {
          // Identificadores
          let flowSection = currentStage.flowType == 'parity' ? 'parities' : currentStage.flowType + 's',
              changeDisplayedStage = this.changeDisplayedStage.bind( this, currentStage );

          // Define opção para o estágio atual
          options[ currentStage.name ] = {
            text: currentStage.displayName,
            action: () => m.app.isInInfoMode ? m.app.openManualSection( flowSection ) : changeDisplayedStage(),
            isToKeepAfterAction: true,
            isSilent: true
          }

          // Atualiza estágio atual
          currentStage = currentStage.children.active;
        }
        while( currentStage && !( currentStage instanceof m.FlowMove ) );

        // Retorna opções do menu de fluxo
        return options;
      }
    }

    // Altera estágio a ser exibido no contedor de fluxo para o passado
    infoBar.changeDisplayedStage = function ( stage ) {
      // Identificadores
      var generalSection = this.sections.general,
          { flow: flowContainer, config: configIcon } = generalSection,
          { bitmap: flowText, arrow: flowArrow } = flowContainer,
          flowMenu = matchScreen.children.find( child => child.name == 'flow-toggleable-menu' ),
          configMenu = matchScreen.children.find( child => child.name == 'config-toggleable-menu' );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( stage instanceof m.FlowUnit );

      // Não executa função caso texto do estágio sendo exibido seja igual ao do estágio passado
      if( flowText.text == stage.displayName ) return this;

      // Oculta menu de seleção de fluxo, quando existente
      if( flowMenu ) flowMenu.visible = false;

      // Atualiza texto sobre fluxo
      flowText.text = stage.displayName;

      // Atualiza nome da unidade de fluxo a que texto é relativo
      flowText.flowType = stage.flowType;

      // Posicionamentos horizontais

      /// Da seta de escolha da exibição do estágio atual
      flowArrow.x = flowText.x + flowText.width + flowArrow.width * .5 + 4;

      /// Do contedor da seção geral
      generalSection.x = this.width * .5 - generalSection.width * .5;

      /// Do menu de configurações, quando existente
      if( configMenu ) configMenu.x = configIcon.getBounds().x - configMenu.width * .5;

      // Retorna menu de informações
      return this;
    }

    /// Atualiza exibidor do estágio atual
    infoBar.updateDisplayedStage = function ( eventData = {} ) {
      // Identificadores
      var matchFlow = matchScreen.match.flow,
          flowText = this.sections.general.flow.bitmap,
          currentStage = matchFlow[ flowText.flowType ] ?? matchFlow.period ?? matchFlow.step ?? matchFlow.round,
          flowMenu = matchScreen.children.find( child => child.name == 'flow-toggleable-menu' ),
          { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.FlowUnit );

      // Oculta menu de seleção de fluxo, quando existente
      if( flowMenu ) flowMenu.visible = false;

      // Altera estágio sendo exibido para o atual
      return this.changeDisplayedStage( currentStage );
    }

    /// Atualiza seção de jogadas
    infoBar.updateMovesData = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: flowRostrum, move } = eventData,
          matchFlow = matchScreen.match.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'move-change' && [ 'add', 'develop', 'remove' ].includes( eventType ) );
        m.oAssert( typeof flowRostrum.setMoves == 'function' );
        m.oAssert( move instanceof m.FlowMove );
      }

      // Apenas executa função para jogadas de fontes controláveis
      if( eventData.move.source.isUncontrollable ) return;

      // Identificadores pós-validação
      var parity = move.source.parity ?? move.source.getRelationships().owner.parity,
          { movesData } = this.sections[ parity ].sections,
          currentMoves = matchFlow.moves[ parity ].filter( move => !move.source.isUncontrollable ),
          undevelopedMoves = currentMoves.filter( move => !move.development ),
          developedMoves = currentMoves.filter( move => move.development > 0 && move.development < 1 ),
          finishedMoves = currentMoves.filter( move => move.development >= 1 ),
          matchScreenTexts = m.languages.components.matchScreen();

      // Atualiza texto sobre jogadas do jogador alvo
      movesData.bitmap.text = `${ finishedMoves.length }/${ currentMoves.length }`;

      // Atualiza texto suspenso sobre jogadas do jogador alvo
      movesData.bitmap.hoverText = ( function () {
        // Identificadores
        var hoverText = '';

        // Para caso haja jogadas não desenvolvidas
        if( undevelopedMoves.length ) hoverText += matchScreenTexts.getUndevelopedMoves( undevelopedMoves ) + '\n';

        // Para caso haja jogadas desenvolvidas, mas não concluídas
        if( developedMoves.length ) hoverText += matchScreenTexts.getDevelopedMoves( developedMoves ) + '\n';

        // Para caso haja jogadas concluídas
        if( finishedMoves.length ) hoverText += matchScreenTexts.getFinishedMoves( finishedMoves );

        // Retorna texto suspenso
        return hoverText;
      } )().trimEnd();
    }

    /// Atualiza seção de dados dos baralhos do jogo
    infoBar.updateDeckData = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventTarget: card, formerActivity, useTarget } = eventData,
          { deckData } = this.sections[ card.deck.owner.parity ].sections,
          targetSections = deckData.sections,
          formerKeyword = eventCategory == 'card-activity' ? formerActivity : card.isInUse ? 'active' : 'inUse',
          newKeyword = card.isInUse ? 'inUse' : card.activity,
          [ formerSection, newSection ] = [ targetSections[ formerKeyword ], targetSections[ newKeyword ] ],
          componentsArray = card.deck.owner.parity == 'odd' ? deckData.children.slice().reverse() : deckData.children,
          matchScreenTexts = m.languages.components.matchScreen();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( card instanceof m.Card );
        m.oAssert( [ 'card-activity', 'card-use' ].includes( eventCategory ) );

        // Para caso evento seja de mudança de atividade
        if( eventCategory == 'card-activity' ) {
          m.oAssert( formerActivity == 'unlinked' || formerSection );
          m.oAssert( card.activity == 'unlinked' || newSection );
        }
      }

      // Para eventos mudança de uso, apenas executa função caso alvo do evento seja igual ao alvo do uso
      if( eventCategory == 'card-use' && card != useTarget ) return;

      // Para caso haja uma antiga seção
      if( formerSection ) {
        // Captura quantidade de cartas da antiga seção
        let cardsArray = card.deck.cards[ formerKeyword ];

        // Ajusta quantidade de cartas da antiga seção, caso ela seja a de cartas ativas
        if( formerKeyword == 'active' ) cardsArray = cardsArray.filter( card => !card.isInUse );

        // Ordena quantidade de cartas segundo tipo primitivo
        cardsArray = card.deck.format.call( { cards: cardsArray } ).cards;

        // Atualiza quantidade de cartas da antiga seção da carta alvo
        formerSection.bitmap.text = cardsArray.length.toString();

        // Atualiza texto suspenso que lista cartas abarcadas pela antiga seção da carta alvo
        formerSection.bitmap.hoverText = matchScreenTexts.getCardsByActivityValue( cardsArray ).trimEnd();
      }

      // Para caso haja uma nova seção
      if( newSection ) {
        // Captura quantidade de cartas da nova seção
        let cardsArray = card.deck.cards[ newKeyword ];

        // Ajusta quantidade de cartas da nova seção, para caso carta alvo esteja ativa mas em desuso
        if( newKeyword == 'active' ) cardsArray = cardsArray.filter( card => !card.isInUse );

        // Ordena quantidade de cartas segundo tipo primitivo
        cardsArray = card.deck.format.call( { cards: cardsArray } ).cards;

        // Atualiza quantidade de cartas da nova seção da carta alvo
        newSection.bitmap.text = cardsArray.length.toString();

        // Atualiza texto suspenso que lista cartas abarcadas pela nova seção da carta alvo
        newSection.bitmap.hoverText = matchScreenTexts.getCardsByActivityValue( cardsArray ).trimEnd();
      }

      // Atualiza posicionamento horizontal dos contedores na seção de atividade
      for( let i = 0, previousComponent; i < componentsArray.length; i++, previousComponent = componentsArray[ i - 1 ] )
        componentsArray[ i ].x = previousComponent ? previousComponent.x + previousComponent.width + 8 : 0;
    }

    /// Define valores da seção de dados de preparo de entes
    infoBar.setReadinessData = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: stage } = eventData,
          { decks: matchDecks, flow: matchFlow } = matchScreen.match;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && stage instanceof m.CombatPeriod );

      // Itera por paridades
      for( let parity of [ 'odd', 'even' ] ) {
        // Identificadores
        let { readinessData } = this.sections[ parity ].sections,
            skipParity = parity == 'odd' ? 'skipEven' : 'skipOdd',
            matchScreenTexts = m.languages.components.matchScreen();

        // Itera por estados de preparo
        for( let readiness of [ 'unprepared', 'prepared', 'occupied', 'exhausted' ] ) {
          // Identificadores
          let targetSection = readinessData.sections[ readiness ],
              targetCards = matchDecks.getAllCards( { [ skipParity ]: true, skipAssets: true, skipSideDeck: true } ).filter(
                card => !card.isUncontrollable && card.readiness.status == readiness && matchFlow.moves[ parity ].some( move => move.source == card )
              );

          // Altera texto da seção do preparo alvo para o padrão
          targetSection.bitmap.text = targetCards.length.toString();

          // Altera texto suspenso do texto da seção do preparo alvo para o padrão
          targetSection.bitmap.hoverText = matchScreenTexts.getCardsByReadinessValue( targetCards );
        }
      }
    }

    /// Atualiza seção de estado de preparo dos entes do jogo
    infoBar.updateReadinessData = function ( eventData = {} ) {
      // Não executa função caso período atual não seja o do combate
      if( !( matchScreen.match?.flow.period instanceof m.CombatPeriod ) ) return;

      // Identificadores pré-validação
      var { eventCategory, eventTarget: being } = eventData,
          { decks: matchDecks, flow: matchFlow } = matchScreen.match;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'card-activity', 'being-readiness' ].includes( eventCategory ) );
        m.oAssert( being instanceof m.Being );
      }

      // Identificadores pós-validação
      var parity = being.getRelationships().owner.parity,
          { readinessData } = this.sections[ parity ].sections,
          skipParity = parity == 'odd' ? 'skipEven' : 'skipOdd',
          matchScreenTexts = m.languages.components.matchScreen();

      // Itera por estados de preparo
      for( let readiness of [ 'unprepared', 'prepared', 'occupied', 'exhausted' ] ) {
        // Identificadores
        let targetSection = readinessData.sections[ readiness ],
            targetCards = matchDecks.getAllCards( { [ skipParity ]: true, skipAssets: true, skipSideDeck: true } ).filter(
              card => !card.isUncontrollable && card.readiness.status == readiness && matchFlow.moves[ parity ].some( move => move.source == card )
            );

        // Atualiza texto da seção do preparo alvo
        targetSection.bitmap.text = targetCards.length.toString();

        // Atualiza texto suspenso do texto da seção do preparo alvo
        targetSection.bitmap.hoverText = matchScreenTexts.getCardsByReadinessValue( targetCards );
      }
    }

    /// Zera valores da seção de dados de preparo de entes
    infoBar.resetReadinessData = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: stage } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && stage instanceof m.CombatPeriod );

      // Itera por paridades
      for( let parity of [ 'odd', 'even' ] ) {
        // Captura seção de preparo da paridade alvo
        let { readinessData } = this.sections[ parity ].sections;

        // Itera por estados de preparo
        for( let readiness of [ 'unprepared', 'prepared', 'occupied', 'exhausted' ] ) {
          // Identificadores
          let targetSection = readinessData.sections[ readiness ];

          // Zera texto da seção do preparo alvo
          targetSection.bitmap.text = '–';

          // Remove texto suspenso do texto da seção do preparo alvo
          targetSection.bitmap.hoverText = '';
        }
      }
    }

    /// Muda seções visíveis, entre as de atividade e as de estado de preparo
    infoBar.changeCardSections = function ( sectionName ) {
      // Identificadores
      var playerSections = [ 'odd', 'even' ].map( parity => this.sections[ parity ].sections ),
          sectionsToShow = playerSections.map( section => section[ sectionName ] ),
          sectionsToHide = playerSections.map( section => section[ sectionName == 'deckData' ? 'readinessData' : 'deckData' ] ),
          hoverText = m.GameScreen.current.children.find( child => /^info-bar-(activity|readiness)-hover-text$/.test( child.name ) );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'deckData', 'readinessData' ].includes( sectionName ) );
        m.oAssert( [ sectionsToShow, sectionsToHide ].every( sections => sections[0].visible == sections[1].visible ) )
      }

      // Não executa função caso seções a serem mostradas já estejam visíveis
      if( sectionsToShow[0].visible ) return;

      // Remove texto suspenso sobre uma das seções de atividade ou preparo, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Altera visibilidade das seções alvo
      for( let i = 0; i < playerSections.length; i++ ) [ sectionsToShow[ i ].visible, sectionsToHide[ i ].visible ] = [ true, false ];
    }

    /// Atualiza seção de pontos de destino
    infoBar.updateFateData = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventTarget: player } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( player instanceof m.GamePlayer );

      // Identificadores pós-validação
      var currentFate = player.fatePoints.current,
          { fateData } = this.sections[ player.parity ].sections;

      // Atualiza texto sobre pontos de destino do jogador alvo
      fateData.bitmap.text = currentFate.intransitive && currentFate.transitive ?
        `${ currentFate.transitive } + ${ currentFate.intransitive }` : `${ currentFate.get() }`;

      // Atualiza texto suspenso do texto sobre pontos de destino do jogador alvo
      fateData.bitmap.hoverText = m.languages.components.matchScreen().getFatePointsValue( currentFate );
    }

    // Atribuição de propriedades de 'infoBar.sections'
    let infoBarSections = infoBar.sections;

    /// Seção geral
    infoBarSections.general = infoBar.addChild( new PIXI.Container() );

    /// Seção ímpar
    infoBarSections.odd = infoBar.addChild( new PIXI.Container() );

    /// Seção par
    infoBarSections.even = infoBar.addChild( new PIXI.Container() );

    // Atribuição de propriedades de 'timers'
    let timers = matchScreen.timers;

    /// Cronômetro do jogador ímpar
    timers.odd = null;

    /// Cronômetro do jogador par
    timers.even = null;

    /// Ativa cronômetro alvo
    timers.activate = function ( timer ) {
      // Não executa função caso cronômetro já esteja ativo
      if( timer.isActive ) return;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].some( parity => timer == timers[ parity ] ) );

      // Preenche cronômetro completamente
      timer.container.bar.draw( timer.styleData.barWidth );

      // Sinaliza que cronômetro está ativo
      timer.isActive = true;

      // Retorna cronômetro
      return timer;
    }

    /// Inativa cronômetro alvo
    timers.inactivate = function ( timer ) {
      // Não executa função caso cronômetro já esteja inativo
      if( !timer.isActive ) return;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even' ].some( parity => timer == timers[ parity ] ) );

      // Esvazia cronômetro completamente
      timer.container.bar.draw( 0 );

      // Sinaliza que cronômetro está inativo
      timer.isActive = false;

      // Retorna cronômetro
      return timer;
    }
  }
}

/// Propriedades do protótipo
MatchScreen.prototype = Object.create( m.GameScreen.prototype, {
  // Construtor
  constructor: { value: MatchScreen }
} );

// Métodos não configuráveis de 'MatchScreen'
Object.defineProperties( MatchScreen, {
  // Configura entrada da tela
  setupScreenEntering: {
    value: function () {
      // Identificadores
      var { match: gameMatch, infoBar, timers } = this,
          { players: matchPlayers, decks: matchDecks, flow: matchFlow } = gameMatch,
          { field, pools } = gameMatch.environments,
          matchStages = m.FlowUnit.getAllStages(),
          matchScreenTexts = m.languages.components.matchScreen();

      // Monta molduras da tela
      configScreenFrames: {
        // Identificadores
        let topGrid = field.grids.odd.y < field.grids.even.y ? field.grids.odd : field.grids.even,
            lowerGrid = topGrid == field.grids.odd ? field.grids.even : field.grids.odd;

        // Montagem das molduras

        /// Do rol ímpar
        pools.odd.addChild( new m.GameFrame( {
          name: 'frame-1', type: 'poolFrame', substitute: pools.odd.reserve, y: pools.odd.reserve.y
        } ) );

        /// Do rol par
        pools.even.addChild( new m.GameFrame( {
          name: 'frame-2', type: 'poolFrame', substitute: pools.even.reserve, y: pools.even.reserve.y
        } ) );

        /// Da grade superior do campo
        field.addChild( new m.GameFrame( {
          name: 'frame-3', type: 'fieldFrame', substitute: topGrid, y: topGrid.y - m.Field.paddingY
        } ) );

        /// Da grade inferior do campo
        field.addChild( new m.GameFrame( {
          name: 'frame-4', type: 'fieldFrame', substitute: lowerGrid, y: lowerGrid.y
        } ) );

        // Eventos

        /// Para controlar visibilidade de molduras segundo se fluxo está ou não na etapa do arranjo
        m.events.flowChangeEnd.transit.add( matchFlow.round.getChild( 'arrangement-step' ), this.frames.controlVisibility, { context: this.frames } );
      }

      // Monta barra de informações
      buildInfoBar: {
        // Identificadores
        let { general: generalSection, odd: oddSection, even: evenSection } = infoBar.sections,
            textStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -19 },
            [ barHeight, outerMarginX ] = [ 40, 50 ];

        // Montagem dos contedores

        /// Geral
        generalSection: {
          // Identificadores
          let configIcon = generalSection.config = generalSection.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.config ] ) ),
              flowContainer = generalSection.flow = generalSection.addChild( new PIXI.Container() ),
              flowText = flowContainer.bitmap = flowContainer.addChild( new PIXI.BitmapText( '', textStyle ) ),
              flowArrow = flowContainer.arrow = flowContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) ),
              [ configMarginX, arrowMarginX ] = [ 8, 4 ];

          // Configurações do ícone de configurações

          /// Redimensionamento
          configIcon.oScaleByGreaterSize( 15 );

          /// Definição do texto suspenso
          configIcon.hoverText = matchScreenTexts.configIcon;

          /// Nomeação do texto suspenso
          configIcon.hoverTextName = 'info-bar-hover-text';

          // Configurações do exibidor do estágio atual

          /// Tipo de fluxo sendo exibido
          flowText.flowType = 'step';

          // Configurações da seta de escolha da exibição do estágio atual

          /// Redimensionamento
          flowArrow.scale.set( .15 );

          /// Âncoras
          flowArrow.anchor.set( .5 );

          /// Rotação
          flowArrow.angle = 270;

          // Posicionamentos

          /// Do ícone de configurações
          configIcon.position.set( 0, generalSection.height * .5 - configIcon.height * .5 - 1.5 );

          /// Do contedor do fluxo
          flowContainer.position.set( configIcon.x + configIcon.width + configMarginX, generalSection.height * .5 - flowContainer.height * .5 );

          /// Da seta de escolha da exibição do estágio atual
          flowArrow.position.set( flowText.x + flowText.width + flowArrow.width * .5 + arrowMarginX, flowContainer.height * .5 );

          // Torna interativos ícone de configurações e contedor de fluxo
          for( let component of [ configIcon, flowContainer ] ) component.buttonMode = component.interactive = true;

          // Eventos

          /// Para exibição de texto suspenso do ícone de configurações
          configIcon.addListener( 'mouseover', m.app.showHoverText );

          /// Para montagem de um menu do fluxo atualizado quando contedor do fluxo for clicado
          flowContainer.addListener( 'click', infoBar.buildFlowMenu, infoBar );

          /// Para emissão de som ao clicar em contedor do fluxo
          flowContainer.addListener( 'click', () =>
            m.assets.audios.soundEffects[ this.children.find(child => child.name == 'flow-toggleable-menu')?.visible ? 'click-next' : 'click-previous' ].play(),
          this );

          /// Para alteração do cursor caso ele esteja no menu de fluxo quando se entrar no modo informativo
          m.events.infoModeEnd.enter.add( this, function () {
            // Identificadores
            var flowMenu = this.children.find( child => child.name == 'flow-toggleable-menu' );

            // Encerra função caso cursor não esteja sobre o eventual menu de fluxo atual
            if( !flowMenu?.oCheckCursorOver() ) return false;

            // Altera cursor do canvar para o do modo informativo
            m.app.view.style.cursor = 'help';
          } );

          /// Itera por estágios da partida
          for( let stage of matchStages ) {
            // Adiciona ao fim do início de paridades evento para atualizar indicador do estágio atual
            if( stage instanceof m.FlowParity ) m.events.flowChangeEnd.begin.add( stage, infoBar.updateDisplayedStage, { context: infoBar } );
          }

          /// Para ao fim do início de períodos do combate alterar estágio sendo exibido para o segmento atual
          m.events.flowChangeEnd.begin.add( matchFlow.round.getChild( 'combat-period' ), () => infoBar.changeDisplayedStage( matchFlow.segment ) );
        }

        /// De paridades
        paritiesSection: {
          // Itera por paridades
          for( let parity of [ 'odd', 'even' ] ) {
            // Identificadores
            let barSection = infoBar.sections[ parity ],
                componentSections = barSection.sections = {},
                accountData = componentSections.accountData = barSection.addChild( new PIXI.Container() ),
                movesData = componentSections.movesData = barSection.addChild( new PIXI.Container() ),
                deckData = componentSections.deckData = barSection.addChild( new PIXI.Container() ),
                readinessData = componentSections.readinessData = barSection.addChild( new PIXI.Container() ),
                fateData = componentSections.fateData = barSection.addChild( new PIXI.Container() ),
                componentSectionsArray = parity == 'odd' ?
                  [ accountData, movesData, deckData, fateData, readinessData ] : [ fateData, deckData, movesData, accountData, readinessData ],
                marginX = 4;

            // Conteúdo da subseção de dados das contas de usuários
            accountDataContent: {
              // Identificadores
              let { displayName: userName, photo: userPhoto } = matchPlayers[ parity ].user.account,
                  playerPhoto = accountData.playerPhoto = accountData.addChild( new PIXI.Sprite.from( userPhoto ) );

              // Configurações da foto

              /// Redimensionamento
              [ playerPhoto.width, playerPhoto.height ] =  [ barHeight, barHeight ];

              /// Interatividade
              playerPhoto.interactive = true;

              /// Texto suspenso
              playerPhoto.hoverText = userName;

              /// Nome do texto suspenso
              playerPhoto.hoverTextName = `info-bar-user-hover-text`;

              /// Adiciona evento para exibição do texto suspenso
              playerPhoto.addListener( 'mouseover', m.app.showHoverText );

              /// Adiciona evento para exibir seção de jogadores do manual caso foto seja clicada
              playerPhoto.addListener( 'click', () => m.app.openManualSection( 'players' ) );
            }

            // Conteúdo da subseção de jogadas
            movesDataContent: {
              // Identificadores
              let movesIcon = movesData.icon = movesData.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.moves ] ) ),
                  movesText = movesData.bitmap = movesData.addChild( new PIXI.BitmapText( '0/0', textStyle ) );

              // Redimensionamento do ícone
              movesIcon.oScaleByGreaterSize( 28 );

              // Posicionamento dos componentes

              /// Ícone
              movesIcon.position.set( 0, barHeight * .5 - movesIcon.height * .5 );

              /// Texto
              movesText.position.set( movesIcon.width + marginX, barHeight * .5 - movesText.height * .5 );

              // Definição do texto suspenso

              /// Do ícone
              movesIcon.hoverText = matchScreenTexts.getRostrumMoves( parity );

              /// Do texto
              movesText.hoverText = '';

              // Configurações em comum entre componentes
              for( let component of [ movesIcon, movesText ] ) {
                // Interatividade
                component.interactive = true;

                // Nomeação do texto suspenso
                component.hoverTextName = `info-bar-move-hover-text`;

                // Adição de evento para exibição de texto suspenso
                component.addListener( 'mouseover', m.app.showHoverText );

                // Adiciona evento para exibir seção de jogadas do manual caso componente seja clicado
                component.addListener( 'click', () => m.app.openManualSection( 'moves' ) );
              }

              // Adiciona a palcos de fluxo evento para, na barra de informações, atualizar jogadas do jogador alvo
              for( let stage of matchStages )
                if( stage.setMoves ) m.events.moveChangeEnd.any.add( stage, infoBar.updateMovesData, { context: infoBar } );
            }

            // Conteúdo da subseção de dados do baralho
            deckDataContent: {
              // Identificadores
              let deckSections = deckData.sections = {};

              // Itera por atividades
              for( let activity of [ 'inUse', 'active', 'suspended', 'withdrawn', 'ended' ] ) {
                // Identificadores
                let activityContainer = deckSections[ activity ] = deckData.addChild( new PIXI.Container() ),
                    activityIcon = activityContainer.icon = activityContainer.addChild( new PIXI.Sprite(
                      PIXI.utils.TextureCache[ m.assets.images.icons.activities[ activity ] ]
                    ) ),
                    activityText = activityContainer.bitmap = activityContainer.addChild( new PIXI.BitmapText(
                      activity != 'active' ? matchDecks[ parity ].cards[ activity ].length.toString() :
                                             matchDecks[ parity ].cards.active.filter( card => !card.isInUse ).length.toString(),
                      textStyle ) ),
                    usageText =
                      activity == 'inUse' ? `${ m.languages.keywords.getMisc( 'inUse' ).toLowerCase() }` :
                      activity == 'active' ? `${ m.languages.keywords.getMisc( 'inDisuse' ).toLowerCase() }` : '';

                // Redimensiona ícone
                activityIcon.oScaleByGreaterSize( 28 );

                // Posiciona componentes

                /// Ícone
                activityIcon.position.set( 0, activityContainer.height * .5 - activityIcon.height * .5 );

                /// Texto
                activityText.position.set( activityIcon.width + marginX, activityContainer.height * .5 - activityText.height * .5 );

                // Definição do texto suspenso

                /// Do ícone
                activityIcon.hoverText = matchScreenTexts.getCardsByActivityDescription( activity, usageText, parity );

                /// Do texto
                activityText.hoverText = '';

                // Configurações em comum entre componentes
                for( let component of [ activityIcon, activityText ] ) {
                  // Interatividade
                  component.interactive = true;

                  // Nomeação do texto suspenso
                  component.hoverTextName = `info-bar-activity-hover-text`;

                  // Adição de evento para exibição de texto suspenso
                  component.addListener( 'mouseover', m.app.showHoverText );

                  // Adiciona evento para exibir seção de atividade do manual caso componente seja clicado
                  component.addListener( 'click', () => m.app.openManualSection( activity == 'inUse' ? 'usage' : 'card-activities' ) );
                }
              }
            }

            // Conteúdo da subseção de estado de preparo dos entes
            readinessDataContent: {
              // Identificadores
              let readinessSections = readinessData.sections = {};

              // Itera por estados de preparo
              for( let readiness of [ 'unprepared', 'prepared', 'occupied', 'exhausted' ] ) {
                // Identificadores
                let readinessContainer = readinessSections[ readiness ] = readinessData.addChild( new PIXI.Container() ),
                    readinessIcon = readinessContainer.icon = readinessContainer.addChild( new PIXI.Sprite(
                      PIXI.utils.TextureCache[ m.assets.images.icons.readiness[ readiness ] ]
                    ) ),
                    readinessText = readinessContainer.bitmap = readinessContainer.addChild( new PIXI.BitmapText( '–', textStyle ) );

                // Redimensiona ícone
                readinessIcon.oScaleByGreaterSize( 28 );

                // Posiciona componentes

                /// Ícone
                readinessIcon.position.set( 0, readinessContainer.height * .5 - readinessIcon.height * .5 );

                /// Texto
                readinessText.position.set( readinessIcon.width + marginX, readinessContainer.height * .5 - readinessText.height * .5 );

                // Definição do texto suspenso

                /// Do ícone
                readinessIcon.hoverText = matchScreenTexts.getCardsByReadinessDescription( readiness, parity );

                /// Do texto
                readinessText.hoverText = '';

                // Configurações em comum entre componentes
                for( let component of [ readinessIcon, readinessText ] ) {
                  // Interatividade
                  component.interactive = true;

                  // Nomeação do texto suspenso
                  component.hoverTextName = `info-bar-readiness-hover-text`;

                  // Adição de evento para exibição de texto suspenso
                  component.addListener( 'mouseover', m.app.showHoverText );

                  // Adiciona evento para exibir seções sobre disponibilidade do manual caso componente seja clicado
                  component.addListener( 'click', () => m.app.openManualSection( readiness != 'occupied' ? 'combat-moves' : 'action-flow-cost' ) );
                }
              }

              // Define como oculta visibilidade inicial da seção de preparo
              readinessData.visible = false;

              // Eventos

              /// Para definir dados iniciais da seção ao início de cada combate
              m.events.flowChangeEnd.begin.add( matchFlow.round.getChild( 'combat-period' ), infoBar.setReadinessData, { context: infoBar } );

              /// Para zerar dados da seção ao fim de cada combate
              m.events.flowChangeEnd.finish.add( matchFlow.round.getChild( 'combat-period' ), infoBar.resetReadinessData, { context: infoBar } );
            }

            // Conteúdo da subseção de pontos de destino
            fateDataContent: {
              // Identificadores
              let currentFate = matchPlayers[ parity ].fatePoints.current,
                  fateIcon = fateData.icon = fateData.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.fatePoints ] ) ),
                  fateText = fateData.bitmap = fateData.addChild( new PIXI.BitmapText( '0', textStyle ) );

              // Redimensionamento do ícone
              fateIcon.oScaleByGreaterSize( 28 );

              // Posicionamento dos componentes

              /// Ícone
              fateIcon.position.set( 0, barHeight * .5 - fateIcon.height * .5 );

              /// Texto
              fateText.position.set( fateIcon.width + marginX, barHeight * .5 - fateText.height * .5 );

              // Definição do texto suspenso

              /// Do ícone
              fateIcon.hoverText = matchScreenTexts.getFatePointsDescription( parity );

              /// Do texto
              fateText.hoverText = matchScreenTexts.getFatePointsValue( currentFate );

              // Configurações em comum entre componentes
              for( let component of [ fateIcon, fateText ] ) {
                // Interatividade
                component.interactive = true;

                // Nomeação do texto suspenso
                component.hoverTextName = `info-bar-fate-hover-text`;

                // Adição de evento para exibição de texto suspenso
                component.addListener( 'mouseover', m.app.showHoverText );

                // Adiciona evento para exibir seção de pontos de destino do manual caso componente seja clicado
                component.addListener( 'click', () => m.app.openManualSection( 'fate-points' ) );
              }

              // Eventos

              /// Para, na barra de informações, atualizar pontos de destino do jogador alvo
              m.events.fatePointsChangeEnd.any.add( matchPlayers[ parity ], infoBar.updateFateData, { context: infoBar } );
            }

            // Iteração por subseções outras que a de preparo
            for( let x = 0; x < componentSectionsArray.length; x++ ) {
              // Identificadores
              let subSection = componentSectionsArray[ x ],
                  previousSection = componentSectionsArray[ x - 1 ],
                  componentsArray = parity == 'odd' ? subSection.children.slice().reverse() : subSection.children;

              // Posicionamento da subseção
              if( previousSection ) subSection.x = previousSection.x + previousSection.width + outerMarginX;

              // Avança para próximo componente caso o atual não seja o sobre dados do baralho ou do preparo
              if( ![ deckData, readinessData ].includes( subSection ) ) continue;

              // Iteração por componentes
              for( let y = 0, innerMarginX = 16; y < componentsArray.length; y++ ) {
                // Identificadores
                let [ currentComponent, previousComponent ] = [ componentsArray[ y ], componentsArray[ y - 1 ] ];

                // Posicionamento do componente alvo

                /// Horizontal
                if( previousComponent ) currentComponent.x = previousComponent.x + previousComponent.width + innerMarginX;

                /// Vertical
                currentComponent.y = barHeight * .5 - currentComponent.height * .5;
              }
            }

            // Ajusta posicionamento horizontal da seção de preparo para que se centralize onde seção de dados do baralho estiver
            readinessData.x = deckData.x + deckData.width * .5 - readinessData.width * .5;
          }
        }

        // Força altura da barra de informações a ser igual ao valor previsto
        infoBar.height = barHeight;

        // Renderiza barra de informações
        infoBar.draw();

        // Inseri barra de informações na tela
        this.addChild( infoBar );

        // Adiciona evento para no período do combate controlar visibilidade das seções de atividade e estado de preparo
        m.events.flowChangeEnd.transit.add( matchFlow.round.getChild( 'combat-period' ), eventData =>
          infoBar.changeCardSections( eventData.eventType == 'begin' ? 'readinessData' : 'deckData' )
        );
      }

      // Inicia partida
      gameMatch.begin();

      // Monta cronômetros
      buildTimers: {
        // Identificadores
        let screenMarginY = this.height - ( field.height + infoBar.height ),
            barWidth = this.height - infoBar.height - m.Field.paddingY * 2 - field.grids.odd.height - field.grids.even.height - screenMarginY - 80,
            barHeight = ( this.width - field.width - pools.odd.width - pools.even.width ) / 2 - 2;

        // Montagem dos cronômetros

        /// Do jogador ímpar
        timers.odd = this.addChild( new m.AutomaticProgressBar( {
          name: 'odd-timer',
          startDegree: 0,
          styleData: {
            barWidth: barWidth,
            barHeight: barHeight,
            barFillColor: m.data.colors.lightGreen
          }
        } ) );

        /// Do jogador par
        timers.even = this.addChild( new m.AutomaticProgressBar( {
          name: 'even-timer',
          startDegree: 0,
          styleData: {
            barWidth: barWidth,
            barHeight: barHeight,
            barFillColor: m.data.colors.lightGreen
          }
        } ) );

        // Aplica aos cronômetros configurações em comum
        for( let timer of [ timers.odd, timers.even ] ) {
          // Rotação
          timer.angle = 90;

          // Posicionamento vertical
          timer.y = field.y + field.height * .5 - timer.width * .5;

          // Interatividade
          timer.interactive = true;

          // Adiciona evento para exibir seção do manual sobre avanço do fluxo caso componente seja clicado
          timer.addListener( 'click', () => m.app.openManualSection( 'flow-advancement' ) );
        }

        // Posiciona horizontalmente cronômetros

        /// Ímpar
        timers.odd.x = pools.odd.x + pools.odd.width + timers.even.height;

        /// Par
        timers.even.x = this.width - pools.even.width - timers.even.height;

        // Ativa cronômetro da paridade atual
        timers.activate( timers[ matchFlow.parity.parity ] );
      }

      // Monta menu de configurações
      buildConfigMenu: {
        // Identificadores
        let configIcon = infoBar.sections.general.config,
            configMenuTexts = m.languages.components.matchConfigMenu(),
            configMenu = configIcon.menu = this.addChild( new m.ToggleableMenu( {
              name: 'config-toggleable-menu',
              triggers: [ configIcon ],
              options: {
                exit: {
                  text: m.GamePlayer.current && !gameMatch.isSimulation ? configMenuTexts.concede : configMenuTexts.exit,
                  action: this.exit
                }
              },
              styleData: {
                optionsText: { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 }
              }
            } ) );

        // Posicionamento do menu
        configMenu.position.set( configIcon.getBounds().x - configMenu.width * .5, infoBar.y + infoBar.height );
      }

      // Eventos

      /// Adiciona evento para o controle do estágio sendo exibido no contedor do fluxo
      window.addEventListener( 'keydown', MatchScreen.controlDisplayedStage );

      /// Adiciona evento para a definição da moldura em foco
      window.addEventListener( 'keydown', MatchScreen.setFocusedFrame );

      /// Adiciona evento para a exibição das molduras da tela
      window.addEventListener( 'keydown', MatchScreen.toggleFrame );

      /// Adiciona evento para a exibição de registros da partida atual
      window.addEventListener( 'keydown', MatchScreen.showLog );

      /// Adiciona evento para a mudança do componente visível do conteúdo em molduras
      window.addEventListener( 'keydown', MatchScreen.changeFrameContentComponent );

      /// Adiciona evento para filtrar exibição de cartas segundo sua atividade
      window.addEventListener( 'keydown', MatchScreen.showCardsByActivity );

      /// Adiciona evento para filtrar exibição de cartas segundo seu preparo
      window.addEventListener( 'keydown', MatchScreen.showCardsByReadiness );

      /// Adiciona evento para filtrar exibição de cartas segundo propriedades da carta alvo
      window.addEventListener( 'keydown', MatchScreen.showCardsByTargetCardProperties );

      /// Adiciona evento para desfazer filtros de exibição de cartas
      window.addEventListener( 'keyup', MatchScreen.showAllCards );

      /// Adiciona evento de alternamento da exibição das designações de cartas
      window.addEventListener( 'keydown', MatchScreen.toggleCardDesignations );

      /// Adiciona evento para na barra de informações alternar visibilidade entre o componente de preparo e o de atividade
      window.addEventListener( 'keydown', MatchScreen.toggleInfoBarCardComponent );

      /// Adiciona evento para mostrar atalhos da tela
      window.addEventListener( 'keydown', this.showScreenShortcuts );

      /// Adiciona evento para alternar entrada no modo informativo
      window.addEventListener( 'keydown', this.toggleInfoMode );

      /// Adiciona evento para sair da tela
      window.addEventListener( 'keydown', this.exit );

      /// Eventos relacionados à execução de músicas da tela
      soundTracksEvents: {
        // Controla execução de música de combates
        m.events.flowChangeEnd.transit.add( matchFlow.round.getChild( 'combat-period' ), eventData =>
          eventData.eventType == 'begin' ? this.playSoundtrack( this.soundTracks[ 'good-day-to-die' ] ) :
          eventData.eventType == 'finish' ? this.stopSoundtrack() : false,
        { context: this } );

        // Controla execução de música de mobilizações
        m.events.flowChangeEnd.begin.add( matchFlow.round.getChild( 'deployment-step' ), eventData =>
          this.playSoundtrack( this.soundTracks[ 'goliath' ] ),
        { context: this } );

        // Controla execução de música de alocações
        m.events.flowChangeEnd.finish.add( matchFlow.round.getChild( 'allocation-step' ), eventData =>
          this.stopSoundtrack(),
        { context: this } );

        // Controla execução de música de remobilizações
        m.events.flowChangeEnd.transit.add( matchFlow.round.getChild( 'redeployment-period' ), eventData =>
          eventData.eventType == 'begin' ? this.playSoundtrack( this.soundTracks[ 'goliath' ] ) :
          eventData.eventType == 'finish' ? this.stopSoundtrack() : false,
        { context: this } );
      }

      /// Itera por estágios da partida
      for( let stage of matchStages ) {
        // Quando visível, força ocultamento do menu de ações ao fim de cada paridade
        if( stage instanceof m.FlowParity )
          m.events.flowChangeEnd.finish.add( stage, () =>
            m.GameScreen.current.children.find( child => child.name?.endsWith( '-actions-menu' ) && !child.isSubMenu )?.hide()
          );
      }

      // Variáveis globais, para depuração
      if( m.app.isInDevelopment ) {
        // Variáveis da partida
        [ window.gameMatch, window.matchPlayers, window.matchDecks, window.matchFlow, window.field, window.pools ] =
        [ gameMatch, matchPlayers, matchDecks, matchFlow, field, pools ];

        // Variáveis da tela
        [ window.infoBar, window.timers ] = [ this.infoBar, this.timers ];
      }
    }
  },
  // Configura saída da tela
  setupScreenLeaving: {
    value: function () {
      // Remove eventos 'keydown'
      for( let handler of [
        MatchScreen.controlDisplayedStage, MatchScreen.setFocusedFrame, MatchScreen.toggleFrame, MatchScreen.showLog, MatchScreen.changeFrameContentComponent,
        MatchScreen.showCardsByActivity, MatchScreen.showCardsByReadiness, MatchScreen.showCardsByTargetCardProperties, MatchScreen.toggleCardDesignations,
        MatchScreen.toggleInfoBarCardComponent, this.showScreenShortcuts, this.toggleInfoMode, this.exit
      ] )
        window.removeEventListener( 'keydown', handler );

      // Remove eventos 'keyup'
      for( let handler of [ MatchScreen.showAllCards ] ) window.removeEventListener( 'keyup', handler );

      // Remove variáveis globais, se em ambiente de desenvolvimento
      if( m.app.isInDevelopment )
        for( let key of [ 'gameMatch', 'matchPlayers', 'matchDecks', 'matchFlow', 'field', 'pools', 'infoBar', 'timers' ] ) delete window[ key ];

      // Para caso exista uma partida atual
      if( this.match ) {
        // Encerra partida
        this.match.finish();

        // Caso partida não seja uma simulação, retira socket do usuário de salas lhe associadas
        if( !this.match.isSimulation ) m.sockets.current.emit( 'leave-match', this.match.name );
      }
    }
  },
  // Controla estágio sendo exibido no contedor do fluxo
  controlDisplayedStage: {
    value: function ( event ) {
      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'ArrowUp', 'ArrowDown' ].includes( event.code ) ) return;

      // Identificadores
      var matchScreen = m.GameScreen.current,
          currentFlowType = matchScreen.infoBar.sections.general.flow.bitmap.flowType,
          currentStage = matchScreen.match.flow[ currentFlowType ],
          targetStage = event.code == 'ArrowUp' ? currentStage?.parent : currentStage?.children.active;

      // Caso um alvo que não uma jogada exista, altera para ele o estágio sendo atualmente exibido no contedor do fluxo
      if( targetStage && !( targetStage instanceof m.FlowMove ) ) matchScreen.infoBar.changeDisplayedStage( targetStage );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Define moldura em foco
  setFocusedFrame: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2', 'Digit3', 'Digit4' ].includes( event.code ) || event.ctrlKey || !event.shiftKey || !event.altKey ) return;

      // Alterna foco da moldura alvo
      m.GameScreen.current.frames[ event.code.slice( 'Digit'.length ) - 1 ].toggleFocus();

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Controla exibição das molduras da tela
  toggleFrame: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2', 'Digit3', 'Digit4' ].includes( event.code ) ) return;

      // Identifica moldura alvo
      var targetFrame = m.GameScreen.current.frames[ event.code.slice( 'Digit'.length ) - 1 ];

      // Se moldura alvo estiver em foco ou uma tecla de controle foi pressionada, sempre mostra moldura; do contrário, alterna sua exibição
      targetFrame.isInFocus || [ event.shiftKey, event.ctrlKey, event.altKey ].some( key => key ) ? targetFrame.show() : targetFrame.toggleDisplay();

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Exibe registros da partida atual na moldura indicada
  showLog: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2', 'Digit3', 'Digit4' ].includes( event.code ) || !event.ctrlKey || event.shiftKey || !event.altKey ) return;

      // Apenas executa função caso haja uma partida em andamento
      if( !m.GameMatch.current ) return;

      // Identifica moldura alvo
      var matchFlow = m.GameMatch.current.flow,
          targetFrame = m.GameScreen.current.frames[ event.code.slice( 'Digit'.length ) - 1 ];

      // Não continua função caso moldura já esteja exibindo registros da partida
      if( targetFrame.content.type == 'match-log' ) return;

      // Exibe registro de eventos do estágio preferencial para o momento atual
      targetFrame.content.init( m.GameMatch.current.log.getLogStage() );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Altera componente visível do conteúdo em molduras
  changeFrameContentComponent: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2', 'Digit3', 'Digit4' ].includes( event.code ) || event.altKey || [ event.shiftKey, event.ctrlKey ].every( key => !key ) ) return;

      // Identificadores
      var targetFrameContent = m.GameScreen.current.frames[ event.code.slice( 'Digit'.length ) - 1 ].content,
          targetComponent =
            event.shiftKey && event.ctrlKey ? targetFrameContent.container?.info?.cardCommand :
            event.shiftKey                  ? targetFrameContent.container?.info?.cardEffects :
            event.ctrlKey                   ? targetFrameContent.container?.info?.cardStats : null;

      // Alteração do componente visível na moldura
      if( targetComponent ) targetFrameContent.toggleVisibleComponent( targetComponent );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Filtra exibição de cartas segundo sua atividade
  showCardsByActivity: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit5' ) return;

      // Identificadores
      var { decks: matchDecks } = m.GameMatch.current,
          targetActivity =
            event.shiftKey && event.ctrlKey ? 'ended' :
            event.shiftKey                  ? 'withdrawn' :
            event.ctrlKey                   ? 'active' : 'suspended';

      // Aplica operação para apenas deixar visíveis cartas da atividade alvo
      matchDecks.getAllCards().forEach( card => card.visible = card.activity == targetActivity );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Filtra exibição de cartas segundo seu preparo
  showCardsByReadiness: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit6' ) return;

      // Identificadores
      var { decks: matchDecks } = m.GameMatch.current,
          targetReadiness =
            event.shiftKey && event.ctrlKey ? 'exhausted' :
            event.shiftKey                  ? 'occupied' :
            event.ctrlKey                   ? 'prepared' : 'unprepared';

      // Aplica operação para apenas deixar visíveis cartas no estado de preparo alvo
      matchDecks.getAllCards().forEach( card => card.visible = card.readiness?.status == targetReadiness );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Filtra exibição de cartas segundo propriedades da carta alvo
  showCardsByTargetCardProperties: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit7' ) return;

      // Identificadores
      var { decks: matchDecks } = m.GameMatch.current,
          matchCards = matchDecks.getAllCards(),
          targetCard = matchCards.find( card => card.oCheckCursorOver() ),
          scope =
            event.shiftKey ? 'inUse' :
            event.ctrlKey  ? 'attachments' : 'owned';

      // Encerra função caso não haja uma carta alvo
      if( !targetCard ) return;

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      event.preventDefault();

      // Executa filtro segundo escopo
      switch( scope ) {
        // Para pertences de 'targetCard'
        case 'owned':
          return matchCards.forEach( function ( card ) {
            // Define visibilidade de cartas outras que 'targetCard', ocultando as que não forem seus pertences
            if( card != targetCard ) card.visible = card.owner == targetCard;
          } );
        // Para vinculados de 'targetCard'
        case 'attachments':
          return matchCards.forEach( function ( card ) {
            // Define visibilidade de cartas outras que 'targetCard'
            if( card != targetCard ) card.visible = ( function () {
              // Mostra 'card' caso seja um vinculado direto de 'targetCard'
              for( let attachment of targetCard.attachments.external )
                if( attachment == card ) return true;

              // Sinaliza que 'card' pode ser omitido
              return false;
            } )();
          } );
        // Para cartas em uso por 'targetCard'
        case 'inUse':
          return matchCards.forEach( function ( card ) {
            // Define visibilidade de cartas outras que 'targetCard'
            if( card != targetCard ) card.visible = ( function () {
              // Sempre oculta cartas em desuso
              if( !card.isInUse ) return false;

              // Para entes, oculta 'card' caso não tenha como convocante 'targetCard'
              if( card instanceof m.Being ) return card.summoner == targetCard;

              // Do contrário, oculta 'card' caso não tenha como dono 'targetCard'
              return card.owner == targetCard;
            } )();
          } );
      }
    }
  },
  // Desfaz filtros de exibição de cartas, de modo a mostrar todas
  showAllCards: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit5', 'Digit6', 'Digit7' ].includes( event.code ) ) return;

      // Identificadores
      var { decks: matchDecks } = m.GameMatch.current;

      // Aplica operação para deixar visíveis todas as cartas
      matchDecks.getAllCards().forEach( card => card.visible = true );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Alterna exibição das designações de cartas
  toggleCardDesignations: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit9' ) return;

      // Identificadores
      var { environments } = m.GameMatch.current,
          scopes =
            event.shiftKey && event.ctrlKey ? [ 'field', 'frame', 'pool' ] :
            event.shiftKey                  ? [ 'frame' ] :
            event.ctrlKey                   ? [ 'field' ] : [ 'pool' ];

      // Define se seções de designação dos escopos devem ser exibidas ou não
      var scopesBoolean = !m.Card.designationVisibility[ scopes[0] ];

      // Itera por escopos alvos
      for( let scope of scopes ) {
        // Atualiza exibição das designações em escopo atual
        m.Card.designationVisibility[ scope ] = scopesBoolean;

        // Atualiza exibição das designações em cartas do escopo atual
        switch( scope ) {
          case 'field':
            // Para cartas nas grades do campo
            for( let parity of [ 'odd', 'even' ] ) environments.field.grids[ parity ].cards.forEach( card => card.body.updateDesignationVisibility( scope ) );

            // Encerra operação
            break;
          case 'pool':
            // Para cartas nos róis
            for( let parity of [ 'odd', 'even' ] ) {
              for( let environmentName of [ 'table', 'reserve' ] )
                environments.pools[ parity ][ environmentName ].grid.cards.forEach( card => card.body.updateDesignationVisibility( scope ) );
            }

            // Encerra operação
            break;
          case 'frame':
            // Para cartas nas molduras
            for( let frame of m.GameScreen.current.frames )
              if( frame.content.type == 'card' ) frame.content.container.body.updateDesignationVisibility( scope );
        }
      }

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Na barra de informações, alterna visibilidade entre o componente de preparo e o de atividade
  toggleInfoBarCardComponent: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit0' || event.ctrlKey || !event.shiftKey || event.altKey ) return;

      // Identificadores
      var infoBar = m.GameScreen.current.infoBar,
          hiddenSectionName = [ 'deckData', 'readinessData' ].find( name => !infoBar.sections.odd.sections[ name ].visible );

      // Exibe componente oculto, no processo ocultando o atualmente visível
      infoBar.changeCardSections( hiddenSectionName );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  }
} );
