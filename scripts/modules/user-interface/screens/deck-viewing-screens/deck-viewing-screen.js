// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DeckViewingScreen = function ( config = {} ) {
  // Iniciação de propriedades
  DeckViewingScreen.init( this );

  // Superconstrutor
  m.GameScreen.call( this, config );

  // Configurações pós-superconstrutor

  /// Eventos

  //// Para configurar entrada da tela
  m.events.screenChangeEnd.enter.add( this, DeckViewingScreen.setupScreenEntering );

  //// Para configurar saída da tela
  m.events.screenChangeStart.leave.add( this, DeckViewingScreen.setupScreenLeaving );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DeckViewingScreen.init = function ( deckViewingScreen ) {
    // Chama 'init' ascendente
    m.GameScreen.init( deckViewingScreen );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deckViewingScreen instanceof DeckViewingScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    deckViewingScreen.name = 'deck-viewing-screen';

    /// Plano de fundo
    deckViewingScreen.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'dummy-background' ] );

    /// Músicas
    Object.assign( deckViewingScreen.soundTracks, {
      // Música padrão
      'night-snow': m.assets.audios.soundTracks[ 'night-snow' ]
    } );

    /// Tabela de baralhos
    deckViewingScreen.decksTable = ( function () {
      // Carrega dados de localização da tabela de baralhos
      var deckTableTexts = m.languages.components.deckViewingScreen().deckTable;

      // Retorna tabela de baralhos
      return new m.GameTable( {
        name: 'decks',
        captionText: deckTableTexts.caption,
        dataFeed: m.GameUser.current.decks,
        columns: {
          deckName: {
            displayName: deckTableTexts.name,
            content: ( deck ) => deck.displayName
          },
          created: {
            displayName: deckTableTexts.created,
            content: ( deck ) => new Date( deck.createdDate ).toLocaleString( m.languages.current ).replace( /:\d+(?=$|( .*$))/, '' )
          },
          modified: {
            displayName: deckTableTexts.modified,
            content: ( deck ) => new Date( deck.modifiedDate ).toLocaleString( m.languages.current ).replace( /:\d+(?=$|( .*$))/, '' )
          },
          matches: {
            displayName: deckTableTexts.matches,
            content: ( deck ) => deck.matches.total > 9999 ? '> 9999' : deck.matches.total.toString()
          },
          wins: {
            displayName: deckTableTexts.wins,
            content: ( deck ) => deck.matches.wins > 9999 ? '> 9999' : deck.matches.wins.toString()
          },
          draws: {
            displayName: deckTableTexts.draws,
            content: ( deck ) => deck.matches.draws > 9999 ? '> 9999' : deck.matches.draws.toString()
          },
          losses: {
            displayName: deckTableTexts.losses,
            content: ( deck ) => deck.matches.losses > 9999 ? '> 9999' : deck.matches.losses.toString()
          },
          unfinished: {
            displayName: deckTableTexts.disconnects,
            content: ( deck ) => deck.matches.unfinished > 9999 ? '> 9999' : deck.matches.unfinished.toString()
          }
        },
        selectableRows: true,
        orderColumn: true,
        styleData: {
          borderCoverage: 'cell'
        }
      } );
    } )();

    /// Contedor de informações
    deckViewingScreen.infoContainer = new PIXI.Container();

    /// Utilitários
    deckViewingScreen.utils = {};

    /// Sai da tela atual
    deckViewingScreen.exit = function ( event ) {
      // Valida execução da função se via evento de teclado
      if( event?.type == 'keydown' && event.key != 'Escape' ) return;

      // Altera a tela para a de abertura
      return m.GameScreen.change( new m.OpeningScreen() );
    }

    // Atribuição de propriedades de 'decksTable.sections.body'
    let tableBody = deckViewingScreen.decksTable.sections.body;

    // Execução atual de 'tableBody.editRowDeck'
    tableBody.editRowDeckExecution = null;

    // Execução atual de 'tableBody.copyRowDeck'
    tableBody.copyRowDeckExecution = null;

    // Execução atual de 'tableBody.renameRowDeck'
    tableBody.renameRowDeckExecution = null;

    // Execução atual de 'tableBody.removeRowDeck'
    tableBody.removeRowDeckExecution = null;

    // Execução atual de 'tableBody.changeRowOrder'
    tableBody.changeRowOrderExecution = null;

    /// Exibe nas molduras conteúdo inicial atinente ao baralho da fileira alvo
    tableBody.showRowDeckContent = function ( targetRow = tableBody.selectedRow ) {
      // Identificadores
      var { cardsFrame, decksFrame } = deckViewingScreen.infoContainer.sections,
          targetDeck = targetRow.source,
          decksSetCards = targetDeck.cards.concat( targetDeck.sideDeck?.cards ?? [] );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.rows.includes( targetRow ) );
        m.oAssert( targetDeck instanceof m.MainDeck );
      }

      // Caso moldura de cartas ainda não mostre uma carta do conjunto de baralhos alvo, mostra sua carta de comandante
      if( !decksSetCards.includes( cardsFrame.content.container?.source ) ) cardsFrame.content.init( targetDeck.cards.commander );

      // Caso moldura de baralhos ainda não mostre o baralho alvo, mostra-o
      if( decksFrame.content.container?.source != targetDeck ) decksFrame.content.init( targetDeck );

      // Adiciona à moldura botões de ação

      /// Botão de editar
      if( !decksFrame.content.container.editButton ) DeckViewingScreen.buildEditButton( decksFrame, this );

      /// Botão de copiar
      if( !decksFrame.content.container.copyButton ) DeckViewingScreen.buildCopyButton( decksFrame, this );

      /// Botão de remover
      if( !decksFrame.content.container.removeButton ) DeckViewingScreen.buildRemoveButton( decksFrame, this );

      /// Botão de renomear
      if( !decksFrame.content.container.renameButton ) DeckViewingScreen.buildRenameButton( decksFrame, this );
    }

    /// Edita baralho da fileira alvo
    tableBody.editRowDeck = function * ( targetRow = tableBody.selectedRow ) {
      // Identificadores
      var targetDeck = targetRow.source,
          deckMatchStats = targetDeck.matches;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.rows.includes( targetRow ) );
        m.oAssert( targetDeck instanceof m.MainDeck );
      }

      // Caso baralho alvo tenha estatísticas de partidas, avisa ao usuário sobre seu zeramento ao fim da edição
      if( Object.values( deckMatchStats ).some( value => value ) )
        yield new m.AlertModal( {
          text: m.languages.notices.getModalText( 'alert-deck-set-editing' ),
          action: () => tableBody.editRowDeckExecution.next()
        } ).insert();

      // Navega para tela de edição de baralhos
      return m.GameScreen.change( new m.DeckBuildingScreen( { mainDeck: targetDeck } ) );
    }

    /// Copia baralho da fileira alvo
    tableBody.copyRowDeck = function * ( targetRow = tableBody.selectedRow ) {
      // Identificadores
      var { decksTable, utils } = deckViewingScreen,
          targetDeck = targetRow.source,
          targetIndex = m.GameUser.current.decks.indexOf( targetDeck ) + 1,
          userDecks = m.GameUser.current.decks,
          copiedMainDeck;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.rows.includes( targetRow ) );
        m.oAssert( targetDeck instanceof m.MainDeck );
      }

      // Não executa função caso usuário alvo já tenha atingido seu limite de baralhos montados
      if( userDecks.length >= m.Deck.maxQuantity ) return new m.AlertModal( {
        text: m.languages.notices.getModalText( 'deck-set-limit-reached' )
      } ).insert();

      // Gera uma réplica do baralho selecionado
      copiedMainDeck = targetDeck.copy( { includeIsStarter: true } );

      // Salva baralho replicado no servidor
      yield m.app.sendHttpRequest( {
        path: `/actions/copy-deck${ location.search }`,
        method: 'PATCH',
        headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
        body: JSON.stringify( { deck: copiedMainDeck.toObject(), index: targetIndex } ),
        isToBlock: true,
        onLoad: () => tableBody.copyRowDeckExecution.next()
      } );

      // Adiciona baralho replicado ao arranjo de baralhos do usuário alvo
      userDecks.splice( targetIndex, 0, copiedMainDeck );

      // Adiciona à tabela de baralhos fileira relativa ao baralho replicado
      let newRow = this.generateRow( {
        source: copiedMainDeck,
        columns: {
          deckName: copiedMainDeck.displayName,
          created: new Date( copiedMainDeck.createdDate ).toLocaleString( m.languages.current ).replace( /:\d+(?=$|( .*$))/, '' ),
          modified: new Date( copiedMainDeck.modifiedDate ).toLocaleString( m.languages.current ).replace( /:\d+(?=$|( .*$))/, '' ),
          matches: copiedMainDeck.matches.total > 9999 ? '> 9999' : copiedMainDeck.matches.total.toString(),
          wins: copiedMainDeck.matches.wins > 9999 ? '> 9999' : copiedMainDeck.matches.wins.toString(),
          draws: copiedMainDeck.matches.draws > 9999 ? '> 9999' : copiedMainDeck.matches.draws.toString(),
          losses: copiedMainDeck.matches.losses > 9999 ? '> 9999' : copiedMainDeck.matches.losses.toString(),
          unfinished: copiedMainDeck.matches.unfinished > 9999 ? '> 9999' : copiedMainDeck.matches.unfinished.toString()
        }
      }, targetIndex );

      // Adiciona à nova fileira evento para exibir nas molduras conteúdo inicial atinente ao seu baralho
      newRow.addListener( 'click', this.showRowDeckContent.bind( this, newRow ) );

      // Ajusta posição vertical de componentes da tela

      /// Tabela de baralhos
      decksTable.y = ( m.app.pixi.screen.height - deckViewingScreen.infoContainer.height ) * .5 - decksTable.height * .5;

      /// Contedores de utilitários
      for( let container of [ utils.backContainer, utils.orderContainer ] ) container.y = decksTable.y + decksTable.height * .5 - container.height * .5;
    }

    /// Renomeia baralho da fileira alvo
    tableBody.renameRowDeck = function * ( targetRow = tableBody.selectedRow ) {
      // Identificadores
      var { decksTable, utils } = deckViewingScreen,
          targetDeck = targetRow.source,
          { name: formerName, displayName: formerDisplayName } = targetDeck,
          sideDeckFormerName = targetDeck.sideDeck?.name ?? '',
          marginX = 16;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.rows.includes( targetRow ) );
        m.oAssert( targetDeck instanceof m.MainDeck );
      }

      // Inseri modal para definição do nome do baralho
      yield new m.PromptModal( {
        name: 'decks-name-modal',
        text: m.languages.notices.getModalText( 'name-deck-set', { isDeckSet: targetDeck.sideDeck?.cards.length } ),
        inputConfig: ( targetModal, targetInput ) => m.Deck.configDeckNamesInput( targetDeck, targetModal, targetInput ),
        action: setDeckNames.bind( this )
      } ).insert();

      // Registra baralho renomeado no servidor
      yield m.app.sendHttpRequest( {
        path: `/actions/rename-deck${ location.search }`,
        method: 'PATCH',
        headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
        body: JSON.stringify( { deck: targetDeck.toObject(), formerName: formerName, index: m.GameUser.current.decks.indexOf( targetDeck ) } ),
        isToBlock: true,
        onLoad: () => tableBody.renameRowDeckExecution.next(),
        onError: () => sideDeckFormerName ?
          [ targetDeck.name, targetDeck.displayName, targetDeck.sideDeck.name ] = [ formerName, formerDisplayName, sideDeckFormerName ] :
          [ targetDeck.name, targetDeck.displayName ] = [ formerName, formerDisplayName ]
      } );

      // Na tabela de baralhos, atualiza coluna de nome do baralho alvo
      targetRow.columns.deckName.bitmap.text = targetDeck.displayName;

      // Atualiza renderização da tabela para comportar novo nome do baralho
      decksTable.drawCells().sections.body.drawRows();

      // Reajusta renderização, para que em alguns casos célula do título não ultrapasse limites reais da tabela
      decksTable.drawCells();

      // Ajusta posição horizontal de componentes da tela

      /// Tabela de baralhos
      decksTable.x = m.app.pixi.screen.width * .5 - decksTable.width * .5;

      /// Contedor de voltar
      utils.backContainer.x = decksTable.x - utils.backContainer.width - marginX;

      /// Contedor de ordem
      utils.orderContainer.x = decksTable.x + decksTable.width + marginX;

      // Funções

      /// Atribui nomes aos baralhos
      function setDeckNames() {
        // Caso atribuição de nome ocorra, torna a executar operação de renomear conjunto de baralhos
        if( m.Deck.setDeckNames( targetDeck ) ) this.renameRowDeckExecution.next();
      }
    }

    /// Remove baralho da fileira alvo
    tableBody.removeRowDeck = function * ( targetRow = tableBody.selectedRow ) {
      // Identificadores
      var { decksTable, utils } = deckViewingScreen,
          { cardsFrame, decksFrame } = deckViewingScreen.infoContainer.sections,
          targetDeck = targetRow.source,
          decksSetCards = targetDeck.cards.concat( targetDeck.sideDeck?.cards ?? [] ),
          targetDeckIndex = m.GameUser.current.decks.indexOf( targetDeck );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.rows.includes( targetRow ) );
        m.oAssert( targetDeck instanceof m.MainDeck );
      }

      // Confirma com usuário se ele realmente deseja remover o baralho alvo
      yield new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-deck-set-removal' ),
        acceptAction: () => tableBody.removeRowDeckExecution.next()
      } ).insert();

      // Remove baralho alvo do servidor
      yield m.app.sendHttpRequest( {
        path: `/actions/remove-deck${ location.search }`,
        method: 'PATCH',
        headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
        body: JSON.stringify( { deckName: targetDeck.name } ),
        isToBlock: true,
        onLoad: () => tableBody.removeRowDeckExecution.next()
      } );

      // Remove baralho alvo do arranjo de baralhos de seu jogador
      if( targetDeckIndex != -1 ) m.GameUser.current.decks.splice( targetDeckIndex, 1 );

      // Remove fileira alvo da tabela
      this.removeRow( targetRow );

      // Ajusta posição vertical de componentes da tela

      /// Tabela de baralhos
      decksTable.y = ( m.app.pixi.screen.height - deckViewingScreen.infoContainer.height ) * .5 - decksTable.height * .5;

      /// Contedores de utilitários
      for( let container of [ utils.backContainer, utils.orderContainer ] ) container.y = decksTable.y + decksTable.height * .5 - container.height * .5;

      // Caso tenham relação com o baralho removido, limpa conteúdos das molduras

      /// Moldura de cartas
      if( decksSetCards.includes( cardsFrame.content.container?.source ) ) cardsFrame.content.clear();

      /// Moldura de baralhos
      if( decksFrame.content.container?.source == targetDeck ) decksFrame.content.clear();
    }

    /// Altera ordem da fileira alvo
    tableBody.changeRowOrder = function * ( number, targetRow = tableBody.selectedRow ) {
      // Identificadores
      var currentIndex = this.rows.indexOf( targetRow ),
          targetIndex = currentIndex + number,
          isSelected = targetRow == this.selectedRow,
          [ userDecks, targetDeck ] = [ m.GameUser.current.decks, targetRow?.source ],
          targetDeckIndex = userDecks.indexOf( targetDeck );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( typeof number == 'number' );
        m.oAssert( !targetRow || this.rows.includes( targetRow ) );
      }

      // Para caso não haja uma fileira alvo
      if( !targetRow ) {
        // Notifica sobre necessidade de escolher uma fileira
        m.noticeBar.show( m.languages.notices.getMiscText( 'deck-viewing-no-selected-row' ), 'lifted' );

        // Encerra função
        return false;
      }

      // Não executa função caso índice alvo seja igual ao atual
      if( currentIndex == targetIndex ) return false;

      // Não executa função caso índice alvo não seja suportado pelo arranjo de fileiras
      if( targetIndex < 0 || targetIndex >= this.rows.length ) return false;

      // Salva no servidor nova posição do baralho
      yield m.app.sendHttpRequest( {
        path: `/actions/update-deck${ location.search }`,
        method: 'PATCH',
        headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
        body: JSON.stringify( { deck: targetDeck.toObject(), index: targetIndex } ),
        isToBlock: true,
        onLoad: () => tableBody.changeRowOrderExecution.next()
      } );

      // Reposiciona fileira alvo
      this.removeRow( targetRow, true ).addRow( targetRow, targetIndex );

      // No arranjo de baralhos do jogador, reposiciona baralho relativo à fileira
      if( targetDeckIndex != -1 ) userDecks.splice( targetIndex, 0, userDecks.splice( targetDeckIndex, 1 ).pop() );

      // Preserva seleção da fileira alvo, se aplicável
      if( isSelected ) this.changeSelectedRow( targetRow );
    }

    // Atribuição de propriedades de 'infoContainer'
    let infoContainer = deckViewingScreen.infoContainer;

    /// Seções
    infoContainer.sections = {};

    // Atribuição de propriedades de 'infoContainer.sections'
    let infoContainerSections = infoContainer.sections;

    /// Moldura de cartas
    infoContainerSections.cardsFrame = null;

    /// Moldura de baralhos
    infoContainerSections.decksFrame = null;

    // Atribuição de propriedades de 'utils'
    let utils = deckViewingScreen.utils;

    /// Contedor da operação de voltar
    utils.backContainer = new PIXI.Graphics();

    /// Contedor das operações de mudança de ordem dos baralhos
    utils.orderContainer = new PIXI.Graphics();

    // Atribuição de propriedades de 'utils.backContainer'
    let backContainer = utils.backContainer;

    /// Seta de voltar
    backContainer.backArrow = backContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) );

    /// Renderiza contedor
    backContainer.draw = function () {
      // Identificadores
      var { backArrow } = this,
          [ paddingX, paddingY ] = [ 12, 16 ];

      // Limpeza da renderização atual
      this.clear();

      // Renderização do contedor

      /// Preenchimento
      this.beginFill( m.data.colors.semiblack1 );

      /// Confecção
      this.drawRoundedRect( 0, 0, this.width + paddingX, this.height + paddingY, m.data.sizes.buttons.roundedCorners );

      /// Finalização
      this.endFill();

      // Posicionamento da seta de voltar
      backArrow.position.set( this.width * .5 - backArrow.width * .5, this.height * .5 - backArrow.height * .5 );
    }

    // Atribuição de propriedades de 'utils.orderContainer'
    let orderContainer = utils.orderContainer;

    /// Seta de subida de posição
    orderContainer.moveUpArrow = orderContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) );

    /// Seta de descida de posição
    orderContainer.moveDownArrow = orderContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ) );

    /// Renderiza contedor
    orderContainer.draw = function () {
      // Identificadores
      var { moveUpArrow, moveDownArrow } = this,
          [ paddingX, paddingY, arrowsMargin ] = [ 12, 8, 12 ];

      // Limpeza da renderização atual
      this.clear();

      // Renderização do contedor

      /// Preenchimento
      this.beginFill( m.data.colors.semiblack1 );

      /// Confecção
      this.drawRoundedRect( 0, 0, this.width + paddingX, this.height * 2 + paddingY + arrowsMargin, m.data.sizes.buttons.roundedCorners );

      /// Finalização
      this.endFill();

      // Posicionamento das setas

      /// De subida
      moveUpArrow.position.set( this.width - moveUpArrow.width - paddingX * .5, this.y + moveUpArrow.height * .5 + paddingY * .5 );

      /// De descida
      moveDownArrow.position.set( moveUpArrow.x, moveUpArrow.y + moveDownArrow.height + arrowsMargin );
    }
  }
}

/// Propriedades do protótipo
DeckViewingScreen.prototype = Object.create( m.GameScreen.prototype, {
  // Construtor
  constructor: { value: DeckViewingScreen }
} );

// Métodos não configuráveis de 'DeckViewingScreen'
Object.defineProperties( DeckViewingScreen, {
  // Configura entrada da tela
  setupScreenEntering: {
    value: function () {
      // Identificadores
      var { decksTable, infoContainer, utils } = this;

      // Configurações do contedor de informações
      configInfoContainer: {
        // Identificadores
        let sections = infoContainer.sections,
            { cardsFrame, decksFrame } = sections;

        // Geração das molduras

        /// De cartas
        cardsFrame = infoContainer.addChild( sections.cardsFrame = new m.GameFrame( {
          name: 'frame-1', type: 'fieldFrame', x: 0
        } ) );

        /// De baralhos
        decksFrame = infoContainer.addChild( sections.decksFrame = new m.GameFrame( {
          name: 'frame-2', type: 'deckFrame', x: cardsFrame.x + cardsFrame.width, borders: 'top-bottom-right'
        } ) );

        // Inseri contedor de informações na tela
        this.addChild( infoContainer );

        // Posiciona contedor de informações
        infoContainer.position.set( m.app.pixi.screen.width * .5 - infoContainer.width * .5, m.app.pixi.screen.height - infoContainer.height );
      }

      // Configurações da tabela de baralhos
      configDecksTable: {
        // Identificadores
        let { body: tableBody } = decksTable.sections,
            { rows: bodyRows } = tableBody;

        // Itera por fileiras do corpo da tabela
        for( let row of bodyRows ) {
          // Evento para exibir nas molduras conteúdo inicial atinente ao baralho da fileira alvo
          row.addListener( 'click', tableBody.showRowDeckContent.bind( tableBody, row ) );
        }

        // Inseri tabela de baralhos na tela
        this.addChild( decksTable );

        // Posiciona tabela de baralhos
        decksTable.position.set(
          m.app.pixi.screen.width * .5 - decksTable.width * .5, ( m.app.pixi.screen.height - this.infoContainer.height ) * .5 - decksTable.height * .5
        );

        // Adiciona evento de delegação de operações com fileiras do corpo da tabela
        window.addEventListener( 'keydown', DeckViewingScreen.controlTableBodyRows );
      }

      // Configurações dos utilitários
      configUtils: {
        // Identificadores
        let { backContainer, orderContainer } = utils,
            [ backArrow, moveUpArrow, moveDownArrow ] = [ backContainer.backArrow, orderContainer.moveUpArrow, orderContainer.moveDownArrow ],
            deckViewingScreenTexts = m.languages.components.deckViewingScreen(),
            marginX = 16;

        // Definição dos textos suspensos

        /// Seta de voltar
        backArrow.hoverText = deckViewingScreenTexts.backArrow;

        /// Seta de subida
        moveUpArrow.hoverText = deckViewingScreenTexts.moveUpArrow;

        /// Seta de descida
        moveDownArrow.hoverText = deckViewingScreenTexts.moveDownArrow;

        // Configurações das setas
        for( let arrow of [ backArrow, moveUpArrow, moveDownArrow ] ) {
          // Tamanho
          arrow.scale.set( .35 );

          // Interatividade
          arrow.buttonMode = arrow.interactive = true;

          // Evento para exibição dos textos suspensos
          arrow.addListener( 'mouseover', m.app.showHoverText );
        }

        // Ajuste da âncora das setas de ordem
        for( let arrow of [ moveUpArrow, moveDownArrow ] ) arrow.anchor.set( .5 );

        // Rotação das setas

        /// De subida
        moveUpArrow.angle = 90;

        /// De descida
        moveDownArrow.angle = 270;

        // Renderização dos contedores das setas
        for( let container of [ backContainer, orderContainer ] ) container.draw();

        // Inseri utilitários na tela
        this.addChild( backContainer, orderContainer );

        // Posicionamentos

        /// Do contedor de voltar
        backContainer.position.set( decksTable.x - backContainer.width - marginX, decksTable.y + decksTable.height * .5 - backContainer.height * .5 );

        /// Do contedor de ordem
        orderContainer.position.set( decksTable.x + decksTable.width + marginX, decksTable.y + decksTable.height * .5 - orderContainer.height * .5 );

        // Eventos

        /// Da seta de voltar

        //// Emissão do som de voltar
        backArrow.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

        //// Retorno à tela de abertura
        backArrow.addListener( 'click', this.exit );

        /// Da seta de descida

        //// Emissão do som de clique, em função da existência de uma fileira selecionada
        moveUpArrow.addListener(
          'click', () => m.assets.audios.soundEffects[ decksTable.sections.body.selectedRow ? 'click-next' : 'click-deny' ].play()
        );

        //// Desce posição da fileira
        moveUpArrow.addListener( 'click', () => ( decksTable.sections.body.changeRowOrderExecution = decksTable.sections.body.changeRowOrder( -1 ) ).next() );

        /// Da seta de subida

        //// Emissão do som de clique, em função da existência de uma fileira selecionada
        moveDownArrow.addListener(
          'click', () => m.assets.audios.soundEffects[ decksTable.sections.body.selectedRow ? 'click-previous' : 'click-deny' ].play()
        );

        //// Sobe posição da fileira
        moveDownArrow.addListener( 'click', () => ( decksTable.sections.body.changeRowOrderExecution = decksTable.sections.body.changeRowOrder( 1 ) ).next() );
      }

      // Eventos

      /// Adiciona evento de mudança do componente visível do conteúdo em molduras
      window.addEventListener( 'keydown', DeckViewingScreen.changeFrameContentComponent );

      /// Adiciona evento de alternamento da exibição das designações de cartas
      window.addEventListener( 'keydown', DeckViewingScreen.toggleCardDesignations );

      /// Adiciona evento para mostrar atalhos da tela
      window.addEventListener( 'keydown', this.showScreenShortcuts );

      /// Adiciona evento para alternar entrada no modo informativo
      window.addEventListener( 'keydown', this.toggleInfoMode );

      /// Adiciona evento para sair da tela
      window.addEventListener( 'keydown', this.exit );

      // Variáveis globais, para depuração
      if( m.app.isInDevelopment ) [ window.decksTable, window.infoContainer, window.utils ] = [ decksTable, infoContainer, utils ];
    }
  },
  // Configura saída da tela
  setupScreenLeaving: {
    value: function () {
      // Remove eventos 'keydown'
      for( let eventHandler of [
        DeckViewingScreen.controlTableBodyRows, DeckViewingScreen.changeFrameContentComponent, DeckViewingScreen.toggleCardDesignations,
        this.showScreenShortcuts, this.toggleInfoMode, this.exit
      ] )
        window.removeEventListener( 'keydown', eventHandler );

      // Remove variáveis globais, se em ambiente de desenvolvimento
      if( m.app.isInDevelopment )
        for( let key of [ 'decksTable', 'infoContainer', 'utils' ] ) delete window[ key ];
    }
  },
  // Monta botão de edição do baralho selecionado
  buildEditButton: {
    value: function ( targetFrame, targetTableBody ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          editButton = frameContainer.editButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFrame.content.type == 'deck' );

      // Geração do texto
      editButton.bitmap = editButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'edit' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Renderização do botão

      /// Preenchimento
      editButton.beginFill( m.data.colors.darkGreen );

      /// Confecção
      editButton.drawRoundedRect( 0, 0, editButton.width + buttonsData.paddingX, editButton.height + buttonsData.paddingY, buttonsData.roundedCorners );

      /// Finalização
      editButton.endFill();

      // Posicionamento do texto
      editButton.bitmap.position.set( editButton.width * .5 - editButton.bitmap.width * .5, editButton.height * .5 - editButton.bitmap.height * .5 );

      // Posicionamento do botão
      editButton.position.set( targetFrame.width * .015, targetFrame.height - editButton.height - 9 );

      // Interatividade
      editButton.buttonMode = editButton.interactive = true;

      // Eventos

      /// Emissão do som de edição
      editButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

      /// Edição do baralho alvo
      editButton.addListener( 'click', () => ( targetTableBody.editRowDeckExecution = targetTableBody.editRowDeck() ).next() );

      // Retorna botão
      return editButton;
    }
  },
  // Monta botão de replicação do baralho selecionado
  buildCopyButton: {
    value: function ( targetFrame, targetTableBody ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          { editButton } = frameContainer,
          copyButton = frameContainer.copyButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFrame.content.type == 'deck' );

      // Geração do texto
      copyButton.bitmap = copyButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'copy' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Renderização do botão

      /// Preenchimento
      copyButton.beginFill( m.data.colors.darkBlue );

      /// Confecção
      copyButton.drawRoundedRect( 0, 0, copyButton.width + buttonsData.paddingX, copyButton.height + buttonsData.paddingY, buttonsData.roundedCorners );

      /// Finalização
      copyButton.endFill();

      // Posicionamento do texto
      copyButton.bitmap.position.set( copyButton.width * .5 - copyButton.bitmap.width * .5, copyButton.height * .5 - copyButton.bitmap.height * .5 );

      // Posicionamento do botão
      copyButton.position.set( editButton.x + editButton.width + 16, targetFrame.height - copyButton.height - 9 );

      // Interatividade
      copyButton.buttonMode = copyButton.interactive = true;

      // Eventos

      /// Emissão do som de replicação
      copyButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

      /// Replicação do baralho alvo
      copyButton.addListener( 'click', () => ( targetTableBody.copyRowDeckExecution = targetTableBody.copyRowDeck() ).next() );

      // Retorna botão
      return copyButton;
    }
  },
  // Monta botão de remoção do baralho selecionado
  buildRemoveButton: {
    value: function ( targetFrame, targetTableBody ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          removeButton = frameContainer.removeButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFrame.content.type == 'deck' );

      // Geração do texto
      removeButton.bitmap = removeButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'remove' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Renderização do botão

      /// Preenchimento
      removeButton.beginFill( m.data.colors.darkRed );

      /// Confecção
      removeButton.drawRoundedRect( 0, 0, removeButton.width + buttonsData.paddingX, removeButton.height + buttonsData.paddingY, buttonsData.roundedCorners );

      /// Finalização
      removeButton.endFill();

      // Posicionamento do texto
      removeButton.bitmap.position.set( removeButton.width * .5 - removeButton.bitmap.width * .5, removeButton.height * .5 - removeButton.bitmap.height * .5 );

      // Posicionamento do botão
      removeButton.position.set( targetFrame.width - removeButton.width - targetFrame.width * .015, targetFrame.height - removeButton.height - 9 );

      // Interatividade
      removeButton.buttonMode = removeButton.interactive = true;

      // Eventos

      /// Emissão do som de remoção
      removeButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

      /// Remoção do baralho alvo
      removeButton.addListener( 'click', () => ( targetTableBody.removeRowDeckExecution = targetTableBody.removeRowDeck() ).next() );

      // Retorna botão
      return removeButton;
    }
  },
  // Monta botão de renomeação do baralho selecionado
  buildRenameButton: {
    value: function ( targetFrame, targetTableBody ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          { removeButton } = frameContainer,
          renameButton = frameContainer.renameButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetFrame.content.type == 'deck' );

      // Geração do texto
      renameButton.bitmap = renameButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'rename' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Renderização do botão

      /// Preenchimento
      renameButton.beginFill( m.data.colors.darkYellow );

      /// Confecção
      renameButton.drawRoundedRect( 0, 0, renameButton.width + buttonsData.paddingX, renameButton.height + buttonsData.paddingY, buttonsData.roundedCorners );

      /// Finalização
      renameButton.endFill();

      // Posicionamento do texto
      renameButton.bitmap.position.set( renameButton.width * .5 - renameButton.bitmap.width * .5, renameButton.height * .5 - renameButton.bitmap.height * .5 );

      // Posicionamento do botão
      renameButton.position.set( removeButton.x - renameButton.width - 16, targetFrame.height - renameButton.height - 9 );

      // Interatividade
      renameButton.buttonMode = renameButton.interactive = true;

      // Eventos

      /// Emissão do som de renomeação
      renameButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

      /// Renomeação do baralho alvo
      renameButton.addListener( 'click', () => ( targetTableBody.renameRowDeckExecution = targetTableBody.renameRowDeck() ).next() );

      // Retorna botão
      return renameButton;
    }
  },
  // Delega operações com fileiras do corpo da tabela
  controlTableBodyRows: {
    value: function ( event ) {
      // Identificadores
      var { body: tableBody } = m.GameScreen.current.decksTable.sections,
          { selectedRow } = tableBody,
          selectedRowIndex = tableBody.rows.indexOf( selectedRow );

      // Apenas executa operações caso haja fileiras na tabela
      if( !tableBody.rows.length ) return;

      // Para teclas outras que 'Home' e 'End', apenas executa operações caso haja uma fileira selecionada
      if( !selectedRow && ![ 'Home', 'End' ].includes( event.code ) ) return;

      // Delega operação em função de tecla pressionada
      switch( event.code ) {
        case 'Home':
          // Caso tecla 'ctrl' esteja pressionada e haja uma fileira selecionada, move-a para a primeira posição
          if( event.ctrlKey && selectedRow )
            return ( tableBody.changeRowOrderExecution = tableBody.changeRowOrder( -selectedRowIndex ) ).next();

          // Altera fileira selecionada para a primeira da tabela
          tableBody.changeSelectedRow( tableBody.rows[ 0 ] );

          // Mostra conteúdo do baralho de fileira selecionada
          return tableBody.showRowDeckContent();
        case 'End':
          // Caso tecla 'ctrl' esteja pressionada e haja uma fileira selecionada, move-a para a última posição
          if( event.ctrlKey && selectedRow )
            return ( tableBody.changeRowOrderExecution = tableBody.changeRowOrder( tableBody.rows.length - 1 - selectedRowIndex ) ).next();

          // Altera fileira selecionada para a última da tabela
          tableBody.changeSelectedRow( tableBody.rows[ tableBody.rows.length - 1 ] );

          // Mostra conteúdo do baralho de fileira selecionada
          return tableBody.showRowDeckContent();
        case 'ArrowUp':
          // Caso fileira selecionada seja a primeira, não executa operação
          if( selectedRowIndex == 0 ) return;

          // Caso tecla 'ctrl' esteja pressionada, move para cima fileira selecionada
          if( event.ctrlKey ) return ( tableBody.changeRowOrderExecution = tableBody.changeRowOrder( -1 ) ).next();

          // Altera fileira selecionada para a anterior à atual
          tableBody.changeSelectedRow( tableBody.rows[ selectedRowIndex - 1 ] );

          // Mostra conteúdo do baralho de fileira selecionada
          return tableBody.showRowDeckContent();
        case 'ArrowDown':
          // Caso fileira selecionada seja a última, não executa operação
          if( selectedRowIndex == tableBody.rows.length - 1 ) return;

          // Caso tecla 'ctrl' esteja pressionada, move para baixo fileira selecionada
          if( event.ctrlKey ) return ( tableBody.changeRowOrderExecution = tableBody.changeRowOrder( 1 ) ).next();

          // Altera fileira selecionada para a posterior à atual
          tableBody.changeSelectedRow( tableBody.rows[ selectedRowIndex + 1 ] );

          // Mostra conteúdo do baralho de fileira selecionada
          return tableBody.showRowDeckContent();
        case 'Enter':
          // Em função da tecla 'ctrl', copia ou edita o baralho de fileira selecionada
          return event.ctrlKey ?
            ( tableBody.copyRowDeckExecution = tableBody.copyRowDeck() ).next() : ( tableBody.editRowDeckExecution = tableBody.editRowDeck() ).next();
        case 'Delete':
          // Em função da tecla 'ctrl', renomeia ou remove o baralho de fileira selecionada
          return event.ctrlKey ?
            ( tableBody.renameRowDeckExecution = tableBody.renameRowDeck() ).next() : ( tableBody.removeRowDeckExecution = tableBody.removeRowDeck() ).next()
      }
    }
  },
  // Altera componente visível do conteúdo em molduras
  changeFrameContentComponent: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2' ].includes( event.code ) || event.altKey || [ event.shiftKey, event.ctrlKey ].every( key => !key ) ) return;

      // Captura conteúdo da moldura alvo
      var targetFrameContent = ( function () {
        switch( event.code ) {
          case 'Digit1':
            return m.GameScreen.current.infoContainer.sections.cardsFrame.content;
          case 'Digit2':
            return m.GameScreen.current.infoContainer.sections.decksFrame.content;
        }
      } )();

      // Identifica conteúdo alvo
      var targetComponent = ( function () {
        switch( targetFrameContent?.type ) {
          case 'card':
            return event.shiftKey && event.ctrlKey ? targetFrameContent.container.info.cardCommand :
                   event.shiftKey                  ? targetFrameContent.container.info.cardEffects :
                   event.ctrlKey                   ? targetFrameContent.container.info.cardStats : null;
          case 'deck':
            return event.shiftKey ? targetFrameContent.container.components.validation :
                   event.ctrlKey  ? targetFrameContent.container.components.sideDeck : null;
          default:
            return null;
        }
      } )();

      // Alteração do componente visível na moldura
      if( targetComponent ) targetFrameContent.toggleVisibleComponent( targetComponent );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Alterna exibição das designações de cartas
  toggleCardDesignations: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit9' || !event.shiftKey ) return;

      // Atualiza exibição das designações de cartas no escopo de molduras
      m.Card.designationVisibility.frame = !m.Card.designationVisibility.frame;

      // Reflete atualização para carta na moldura de cartas
      m.GameScreen.current.infoContainer.sections.cardsFrame.content.container?.body.updateDesignationVisibility( 'frame' );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  }
} );
