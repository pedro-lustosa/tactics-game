// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const OpeningScreen = function ( config = {} ) {
  // Iniciação de propriedades
  OpeningScreen.init( this );

  // Superconstrutor
  m.GameScreen.call( this, config );

  // Configurações pós-superconstrutor

  /// Eventos

  //// Para configurar entrada da tela
  m.events.screenChangeEnd.enter.add( this, OpeningScreen.setupScreenEntering );
};

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OpeningScreen.init = function ( openingScreen ) {
    // Chama 'init' ascendente
    m.GameScreen.init( openingScreen );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( openingScreen instanceof OpeningScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    openingScreen.name = 'opening-screen';

    /// Plano de fundo
    openingScreen.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'opening-screen' ] );

    /// Músicas
    Object.assign( openingScreen.soundTracks, {
      // Música de abertura
      'immortality': m.assets.audios.soundTracks[ 'immortality' ]
    } );

    /// Menu
    openingScreen.menu = {};

    /// Indicador da versão atual do jogo
    openingScreen.version = new PIXI.Container();

    // Atribuição de propriedades de 'menu'
    let menu = openingScreen.menu;

    /// Seções
    menu.sections = {};

    /// Estilo dos textos do menu
    menu.textStyle = new PIXI.TextStyle( {
      fontFamily: m.assets.fonts.sourceSansPro.default.name, fontSize: 42, padding: m.assets.fonts.padding, fill: 'white',
      dropShadow: true, dropShadowBlur: 3, dropShadowDistance: 4
    } );

    // Atribuição de propriedades de 'menu.sections'
    let menuSections = menu.sections,
        openingMenuTexts = m.languages.components.openingMenu();

    /// Aprender
    menuSections.learn = {
      // Título
      caption: openingMenuTexts.learn,
      // Submenus
      sections: {
        // Manual
        manual: {
          // Título
          caption: openingMenuTexts.manual,
          // Ação ao se clicar
          action: () => window.open( `/manual/${ location.search }`, 'tactics-manual' )
        },
        // Vídeos
        videos: {
          // Título
          caption: openingMenuTexts.videos,
          // Ação ao se clicar
          action: () => window.open( `https://youtu.be/W3qtLGfutc4`, 'youtube-video' )
        },
        // Voltar
        previous: {
          // Título
          caption: openingMenuTexts.back
        }
      }
    };

    /// Jogar
    menuSections.play = {
      // Título
      caption: openingMenuTexts.play,
      // Submenus
      sections: {
        // Simulação
        simulate: {
          // Título
          caption: openingMenuTexts.simulate,
          // Ação ao se clicar
          action: () => OpeningScreen.enterSimulatedMatch()
        },
        // Multijogador
        multiplayer: {
          // Título
          caption: openingMenuTexts.multiplayer,
          // Ação ao se clicar
          action: () => OpeningScreen.enterMatchmaking()
        },
        // Voltar
        previous: {
          // Título
          caption: openingMenuTexts.back
        }
      }
    };

    /// Baralhos
    menuSections.decks = {
      // Título
      caption: openingMenuTexts.decks,
      // Submenus
      sections: {
        // Visualizar
        view: {
          // Título
          caption: openingMenuTexts.view,
          // Ação ao se clicar
          action: () => OpeningScreen.enterDeckViewing()
        },
        // Montar
        build: {
          // Título
          caption: openingMenuTexts.build,
          // Ação ao se clicar
          action: () => OpeningScreen.enterDeckBuilding()
        },
        // Voltar
        previous: {
          // Título
          caption: openingMenuTexts.back
        }
      }
    };

    /// Configurações
    menuSections.config = {
      // Título
      caption: openingMenuTexts.config,
      // Submenus
      sections: {
        // Áudio
        audio: {
          // Título
          caption: openingMenuTexts.audio,
          // Ação ao se clicar
          action: () => OpeningScreen.showAudioModal()
        },
        // Atalho de telas
        keyboardShortcuts: {
          // Título
          caption: openingMenuTexts.shortcuts,
          // Ação ao se clicar
          action: () => OpeningScreen.showScreensShortcuts()
        },
        // Idioma do jogo
        gameLanguage: {
          // Título
          caption: openingMenuTexts.languages,
          sections: {
            // Inglês
            english: {
              // Título
              caption: 'English',
              // Ação ao se clicar
              action: () => m.languages.change( 'en' )
            },
            // Português
            portuguese: {
              // Título
              caption: 'Português',
              // Ação ao se clicar
              action: () => m.languages.change( 'pt' )
            },
            // Voltar
            previous: {
              // Título
              caption: openingMenuTexts.back
            }
          }
        },
        // Voltar
        previous: {
          // Título
          caption: openingMenuTexts.back
        }
      }
    };
  }
}

/// Propriedades do protótipo
OpeningScreen.prototype = Object.create( m.GameScreen.prototype, {
  // Construtor
  constructor: { value: OpeningScreen }
} );

// Métodos não configuráveis de 'OpeningScreen'
Object.defineProperties( OpeningScreen, {
  // Configura entrada da tela
  setupScreenEntering: {
    value: function () {
      // Configurações do menu
      configMenu: {
        // Identificadores
        let { menu } = this;

        // Monta seções do menu
        OpeningScreen.setMenuSections.call( this, menu );

        // Torna contedor do menu principal visível
        menu.container.visible = true;
      }

      // Configurações do contedor da versão atual do jogo
      configVersion: {
        // Identificadores
        let { version: versionContainer } = this,
            versionText = versionContainer.bitmap = versionContainer.addChild( new PIXI.BitmapText(
              `${ m.languages.snippets.version } ${ m.app.version }`, { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -14 }
            ) );

        // Posiciona contedor da versão
        versionContainer.position.set( this.width - versionContainer.width - 20, this.height - versionContainer.height - 20 );

        // Inseri contedor da versão na tela
        this.addChild( versionContainer );
      }
    }
  },
  // Monta seção do menu
  setMenuSections: {
    value: function ( sectionsMenu, superMenu ) {
      // Identificadores
      var { menu: { textStyle } } = this,
          { sections } = sectionsMenu,
          menuContainer = sectionsMenu.container = new PIXI.Container(),
          offsetY = 0;

      // Configurações do contedor do menu

      /// Posicionamento
      menuContainer.position.set( 40, 80 );

      /// Visibilidade
      menuContainer.visible = false;

      /// Inserção na tela
      this.addChild( menuContainer );

      // Itera por seções do menu
      for( let sectionName in sections ) {
        // Identificadores
        let section = sections[ sectionName ];

        // Se aplicável, captura supermenu da seção
        if( superMenu ) section.superMenu = superMenu;

        // Criação do objeto de texto
        section.caption = new PIXI.Text( section.caption, textStyle );

        // Configurações do texto

        /// Inserção no contedor do menu
        menuContainer.addChild( section.caption );

        /// Posicionamento vertical
        section.caption.y = offsetY;

        /// Interatividade
        section.caption.buttonMode = section.caption.interactive = true;

        // Incrementa distância para colocação do próximo título
        offsetY += section.caption.height + 12;

        /// Eventos

        //// Para caso seção tenha uma ação predefinida
        if( section.action ) {
          // Evento para emissão do som de avançar
          section.caption.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

          // Evento para executar sua ação
          section.caption.addListener( 'click', section.action );
        }

        //// Para caso seção tenha subseções
        else if( section.sections ) {
          // Evento para emissão do som de avançar
          section.caption.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

          // Evento para navegar para seu submenu
          section.caption.addListener( 'click', OpeningScreen.toNextMenu, section );
        }

        //// Para caso nenhuma das outras opções seja válida
        else {
          // Evento para emissão do som de retroceder
          section.caption.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

          // Evento para navegar para seu supermenu
          section.caption.addListener( 'click', OpeningScreen.toPreviousMenu, section );
        }

        // Caso seção tenha subseções, monta-as
        if( section.sections ) OpeningScreen.setMenuSections.call( this, section, sectionsMenu );
      }
    }
  },
  // Avança para um submenu
  toNextMenu: {
    value: function () {
      // Oculta menu atual
      this.caption.parent.visible = false;

      // Exibe menu da seção clicada
      this.container.visible = true;
    }
  },
  // Avança para um supermenu
  toPreviousMenu: {
    value: function () {
      // Oculta menu atual
      this.caption.parent.visible = false;

      // Exibe supermenu
      this.superMenu.container.visible = true;
    }
  },
  // Entra em uma partida simulada
  enterSimulatedMatch: {
    value: function () {
      // Identificadores
      var gameUser = m.GameUser.current,
          decksToUse = gameUser.decks.filter( deck => deck.validate( { shortCircuit: true } ).isValid ).slice( 0, 2 );

      // Caso usuário esteja buscando uma partida, indica que não é possível acessar tela desejada e encerra função
      if( m.GameMatch.isInMatchmaking ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'no-access-while-searching-for-match' ) } ).insert();

      // Caso usuário não tenha ao menos 2 baralhos válidos, notifica isso a ele e encerra função
      if( decksToUse.length != 2 ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'not-enough-valid-decks-for-simulate' ) } ).insert();

      // Avança para a tela de partidas
      return m.GameScreen.change( new m.MatchScreen( {
        matchName: 'sample-match',
        matchUsers: [ gameUser, gameUser ],
        matchDecks: decksToUse
      } ) );
    }
  },
  // Entra na busca de uma partida multijogador
  enterMatchmaking: {
    value: function () {
      // Identificadores
      var gameUser = m.GameUser.current,
          isWithValidDeck = gameUser.decks.some( deck => deck.validate( { shortCircuit: true } ).isValid );

      // Não executa função caso usuário a procurar uma partida seja o global
      if( gameUser.name == m.GameUser.globalUser ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'only-users-allowed' ) } ).insert();

      // Não executa função caso usuário não tenha um baralho válido
      if( !isWithValidDeck ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'not-enough-valid-decks-for-multiplayer' ) } ).insert();

      // Inicia procura por uma partida
      return m.sockets.enterMatchmaking();
    }
  },
  // Entra na tela de visualização de baralhos
  enterDeckViewing: {
    value: function () {
      // Caso usuário esteja buscando uma partida, indica que não é possível acessar tela desejada e encerra função
      if( m.GameMatch.isInMatchmaking ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'no-access-while-searching-for-match' ) } ).insert();

      // Avança para a tela de visualização de baralhos
      return m.GameScreen.change( new m.DeckViewingScreen() );
    }
  },
  // Entra na tela de montagem de baralhos
  enterDeckBuilding: {
    value: function () {
      // Identificadores
      var userDecks = m.GameUser.current.decks;

      // Caso usuário esteja buscando uma partida, indica que não é possível acessar tela desejada e encerra função
      if( m.GameMatch.isInMatchmaking ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'no-access-while-searching-for-match' ) } ).insert();

      // Caso usuário tenha alcançado o limite de baralhos montados, notifica isso a ele e encerra função
      if( userDecks.length >= m.Deck.maxQuantity ) return new m.AlertModal( { text: m.languages.notices.getModalText( 'deck-set-limit-reached' ) } ).insert();

      // Avança para a tela de montagem de baralhos
      return m.GameScreen.change( new m.DeckBuildingScreen() );
    }
  },
  // Mostra modal para controle do áudio
  showAudioModal: {
    value: function () {
      // Identificadores
      var modalTexts = m.languages.components.audioModal(),
          audioContainer = new PIXI.Container(),
          soundTrackBar = audioContainer.addChild( new m.ManualProgressBar( {
            name: 'sound-tracks',
            caption: modalTexts.soundTracks,
            startDegree: Object.values( m.assets.audios.soundTracks )[ 0 ].volume,
            action: ( currentDegree ) => Object.values( m.assets.audios.soundTracks ).forEach( audio => audio.volume = currentDegree ),
            isToShowDegree: true,
            styleData: { barBorderRadius: 10 }
          } ) ),
          soundEffectBar = audioContainer.addChild( new m.ManualProgressBar( {
            name: 'sound-effects',
            caption: modalTexts.soundEffects,
            startDegree: Object.values( m.assets.audios.soundEffects )[ 0 ].volume,
            action: ( currentDegree ) => Object.values( m.assets.audios.soundEffects ).forEach( audio => audio.volume = currentDegree ),
            isToShowDegree: true,
            styleData: { barBorderRadius: 10 }
          } ) );

      // Ajusta posicionamento do contedor de efeitos sonoros
      soundEffectBar.x = soundTrackBar.x + soundTrackBar.width + 90;

      // Inserção da modal com barras de progresso
      return new m.AlertModal( {
        name: 'audio-modal',
        component: audioContainer,
        action: updateAudioPreferences
      } ).insert();

      // Funções

      /// Salva no servidor ajustes ao volume dos áudios
      function updateAudioPreferences() {
        // Identificadores
        var audioVolume = {
          soundTracks: Number( soundTrackBar.container.bar.degree.toFixed( 2 ) ),
          soundEffects: Number( soundEffectBar.container.bar.degree.toFixed( 2 ) )
        },
        updateTarget = m.GameUser.current.name == m.GameUser.globalUser ? 'session' : 'user';

        // Envia requisição para atualização das preferências de usuário
        m.app.sendHttpRequest( {
          path: `/actions/update-${ updateTarget }${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
          body: JSON.stringify( { dataObject: { 'preferences.audioVolume': audioVolume } } )
        } );
      }
    }
  },
  // Mostra atalhos de telas que os têm
  showScreensShortcuts: {
    value: function () {
      // Identificadores
      var tablesSet = [ 'match-screen', 'deck-viewing-screen', 'deck-building-screen' ].map( screenName => m.GameScreen.getScreenShortcuts( screenName ) ),
          largestTable = tablesSet.reduce( ( accumulator, current ) => accumulator = accumulator.width >= current.width ? accumulator : current ),
          tallestTable = tablesSet.reduce( ( accumulator, current ) => accumulator = accumulator.height >= current.height ? accumulator : current ),
          alertModal = new m.AlertModal( {
            contentWidth: largestTable.width,
            component: tallestTable,
            action: removeTablesSet,
            isToKeepAfterAction: true
          } ),
          { inputsSet: modalInputsSet } = alertModal.content,
          navigationArrows = {
            previous: new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ),
            next: new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] )
          },
          arrowsMarginX = 12;

      // Configurações das setas de navegação
      configArrows: {
        // Itera por setas de navegação
        for( let type in navigationArrows ) {
          // Identificadores
          let arrow = navigationArrows[ type ];

          // Tamanho
          arrow.scale.set( .2 );

          // Âncoras
          arrow.anchor.set( .5 );

          // Interatividade
          arrow.buttonMode = arrow.interactive = true;

          // Inserção na célula de título da tabela de atalhos alvo
          modalInputsSet.addChild( arrow );

          // Posicionamento vertical
          arrow.y = modalInputsSet.modalButton.y + modalInputsSet.modalButton.height * .5;
        }

        // Rotaciona seta de navegação posterior
          navigationArrows.next.angle = 180;

        // Posiciona horizontalmente setas de navegação

        /// Anterior
        navigationArrows.previous.x = modalInputsSet.modalButton.x - navigationArrows.previous.width * .5 - arrowsMarginX;

        /// Posterior
        navigationArrows.next.x = modalInputsSet.modalButton.x + modalInputsSet.modalButton.width + navigationArrows.next.width * .5 + arrowsMarginX;

        // Eventos

        /// Para emissão do som de retroceder
        navigationArrows.previous.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

        /// Para emissão do som de avançar
        navigationArrows.next.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

        /// Para navegação para tabela anterior a atualmente exibida
        navigationArrows.previous.addListener( 'click', () => changeDisplayedTable( -1 ) );

        /// Para navegação para tabela posterior a atualmente exibida
        navigationArrows.next.addListener( 'click', () => changeDisplayedTable( 1 ) );
      }

      // Inseri na tela modal com tabela inicial
      alertModal.insert();

      // Configurações das tabelas de atalho
      configShortcutTables: {
        // Identificadores
        let modalTableBounds = tallestTable.getBounds();

        // Itera por tabelas de atalho
        for( let table of tablesSet ) {
          // Filtra tabela inserida na modal
          if( table == tallestTable ) continue;

          // Oculta tabela alvo
          table.visible = false;

          // Adiciona ao canvas tabela alvo
          m.app.pixi.stage.addChild( table );

          // Posiciona tabela alvo
          table.position.set(
            modalTableBounds.x + modalTableBounds.width * .5 - table.width * .5,
            modalTableBounds.y + modalTableBounds.height * .5 - table.height * .5
          );
        }
      }

      // Funções

      /// Altera tabela exibida na modal
      function changeDisplayedTable( number ) {
        // Identificadores
        var displayedTable = tablesSet.find( table => table.visible ),
            targetTable = tablesSet.oCycle( tablesSet.indexOf( displayedTable ) + number );

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( displayedTable );

        // Oculta tabela atualmente visível, e exibe a tabela alvo
        [ displayedTable.visible, targetTable.visible ] = [ false, true ];
      }

      /// Remove arranjo de tabelas de atalho
      function removeTablesSet() {
        // Identificadores
        var tablesOutsideModal = tablesSet.filter( table => m.app.pixi.stage.children.includes( table ) );

        // Remoção das tabelas fora da modal
        for( let table of tablesOutsideModal ) m.app.pixi.stage.removeChild( table ).destroy( { children: true } );

        // Remoção da modal
        alertModal.remove();

        // Limpeza do arranjo de tabelas
        tablesSet = [];
      }
    }
  }
} );
