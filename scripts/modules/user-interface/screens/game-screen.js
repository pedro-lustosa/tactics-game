// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameScreen = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this.name );

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Plano de fundo
  if( this.background ) {
    // Medidas
    for( let size of [ 'width', 'height' ] ) this.background[ size ] = m.app.pixi.screen[ size ];

    // Inserção do elemento
    this.addChildAt( this.background, 0 );
  }

  /// Interatividade
  this.interactive = true;

  /// Filtros
  this.filters = [];
}

/// Propriedades do construtor
defineProperties: {
  // Tela atual
  GameScreen.current = null;

  // Iniciação de propriedades
  GameScreen.init = function ( gameScreen ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( gameScreen instanceof GameScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    gameScreen.name = '';

    /// Plano de fundo
    gameScreen.background = null;

    /// Músicas
    gameScreen.soundTracks = {};

    /// Molduras
    gameScreen.frames = [];

    /// Transita para dentro da tela
    gameScreen.enter = function () {
      // Não executa função se já se estiver em alguma tela
      if( GameScreen.current ) return false;

      // Identificadores
      var soundTracks = Object.values( this.soundTracks );

      // Sinaliza início da entrada da tela
      m.events.screenChangeStart.enter.emit( this );

      // Exibe tela
      m.app.pixi.stage.addChild( this );

      // Define tela como a atual
      GameScreen.current = this;

      // Define músicas da tela como por padrão iteráveis
      soundTracks.forEach( audio => audio.loop = true );

      // Executa primeira música da tela, quando existente
      this.playSoundtrack( soundTracks[0] );

      // Sinaliza fim da entrada da tela
      m.events.screenChangeEnd.enter.emit( this );
    }

    /// Transita para fora da tela
    gameScreen.leave = function ( isContinuousSoundtrack = false ) {
      // Não executa função se tela não for a atual
      if( GameScreen.current != this ) return false;

      // Identificadores
      var soundTracks = Object.values( this.soundTracks );

      // Sinaliza início da saída da tela
      m.events.screenChangeStart.leave.emit( this );

      // Oculta barra de notificações, se visível
      m.noticeBar.hide();

      // Remove eventuais modais na fila de modais a serem inseridas
      while( m.GameModal.queue.length ) m.GameModal.queue.pop().destroy( { children: true } );

      // Remove modal atualmente inserida, se existente
      m.GameModal.current?.remove();

      // Caso o jogo esteja no modo informativo, sai desse modo
      if( m.app.isInInfoMode ) m.app.leaveInfoMode();

      // Define músicas da tela como não mais iteráveis
      soundTracks.forEach( audio => audio.loop = false );

      // Encerra execução da música sendo atualmente tocada na tela, quando aplicável
      if( !isContinuousSoundtrack ) this.stopSoundtrack();

      // Remove tela
      m.app.pixi.stage.removeChild( this );

      // Remove e destrói outros eventuais elementos ainda diretamente no estágio
      while( m.app.pixi.stage.children.length ) m.app.pixi.stage.removeChild( m.app.pixi.stage.children[ 0 ] ).destroy( { children: true } );

      // Remove condição de tela atual
      GameScreen.current = null;

      // Sinaliza fim da saída da tela
      m.events.screenChangeEnd.leave.emit( this );

      // Destrói tela
      this.destroy( { children: true } );
    }

    /// Toca uma música da tela
    gameScreen.playSoundtrack = function ( soundTrack ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !soundTrack || soundTrack instanceof HTMLAudioElement );

      // Não executa função se não houver uma música para ser tocada
      if( !soundTrack ) return false;

      // Não executa função se tela alvo não for a atual
      if( GameScreen.current != this ) return false;

      // Não executa função se música não estiver na lista de músicas da tela
      if( !Object.values( this.soundTracks ).includes( soundTrack ) ) return false;

      // Não executa função se música já estiver sendo tocada
      if( !soundTrack.paused ) return false;

      // Caso navegador estime que música alvo não possa ser tocada sem interrupções, torna a executar esta função em um momento posterior
      if( soundTrack.readyState < 4 ) return setTimeout( this.playSoundtrack.bind( this, soundTrack ), 1000 );

      // Encerra execução da música sendo atualmente tocada na tela, quando existente
      this.stopSoundtrack();

      // Toca música passada
      soundTrack.play();

      // Retorna música passada
      return soundTrack;
    }

    /// Encerra execução da música sendo atualmente tocada na tela
    gameScreen.stopSoundtrack = function () {
      // Identificadores
      var soundTrack = Object.values( this.soundTracks ).find( audio => !audio.paused );

      // Não executa função se não houver uma música para ser encerrada
      if( !soundTrack ) return false;

      // Pausa música alvo
      soundTrack.pause();

      // Retorna ponteiro da música alvo a seu início
      soundTrack.currentTime = 0;

      // Retorna música alvo
      return soundTrack;
    }

    /// Mostra atalhos da tela
    gameScreen.showScreenShortcuts = function ( event ) {
      // Valida execução da função, se via evento de teclado
      if( event?.type == 'keydown' ) {
        // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
        if( event.repeat ) return;

        // Apenas executa função caso teclas pressionadas sejam válidas
        if( event.code != 'Digit0' || [ event.ctrlKey, event.shiftKey, event.altKey ].some( key => key ) ) return;

        // Impede que teclas do evento acionem funcionalidades próprias do navegador
        event.preventDefault();
      }

      // Inseri modal mostrando atalhos da tela atual
      return new m.AlertModal( {
        component: m.GameScreen.getScreenShortcuts( gameScreen.name ),
        isFromUser: true
      } ).insert();
    }

    // Alterna entrada no modo informativo
    gameScreen.toggleInfoMode = function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit0' || !event.ctrlKey || !event.shiftKey || event.altKey ) return;

      // Alterna exibição do modo informativo
      m.app[ m.app.isInInfoMode ? 'leaveInfoMode' : 'enterInfoMode' ]();

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  }

  // Transita para a tela inicial
  GameScreen.begin = function () {
    // Define e entra em tela inicial
    return new m.LoadingScreen().enter();
  }

  // Transita para uma nova tela
  GameScreen.change = function ( newScreen ) {
    // Identificadores
    var currentSoundTrack = Object.values( this.current.soundTracks ).find( audio => !audio.paused );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( newScreen instanceof GameScreen );

    // Não executa função se nova tela já for a atual
    if( this.current == newScreen ) return false;

    // Sai da tela atual
    this.current.leave( currentSoundTrack == Object.values( newScreen.soundTracks )[0] );

    // Entra na nova tela
    newScreen.enter();

    // Caso ambiente seja o de desenvolvimento, salva tela atual em um identificador global
    if( m.app.isInDevelopment ) window.gameScreen = this.current;

    // Retorna nova tela
    return newScreen;
  }

  // Retorna tabela com atalhos da tela alvo
  GameScreen.getScreenShortcuts = function ( screenName ) {
    // Identificadores
    var tableTexts = m.languages.components.shortcutsTable( screenName );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( screenName && typeof screenName == 'string' );

    // Geração e retorno da tabela de atalhos
    return new m.GameTable( {
      name: `screen-shortcuts`,
      captionText: tableTexts.caption,
      dataFeed: tableTexts.body,
      columns: {
        operation: {
          displayName: tableTexts.operation,
          content: ( shortcut ) => shortcut.description
        },
        trigger: {
          displayName: tableTexts.trigger,
          content: ( shortcut ) => shortcut.trigger
        },
        combinations: {
          displayName: tableTexts.combinations,
          content: function ( shortcut ) {
            // Identificadores
            var combinationsText = '',
                controlKeys = [ 'ctrl', 'shift', 'alt' ];

            // Itera por combinações possíveis
            for( let combination of shortcut.combinations ) {
              // Descreve efeito da combinação
              combinationsText += combination.description + ': ';

              // Caso tecla de controle seja usada na combinação alvo, acrescenta essa informação ao texto de combinações
              for( let key of controlKeys )
                if( combination[ key + 'Key' ] ) combinationsText += `${ key[ 0 ].toUpperCase() + key.slice( 1 ) } + `;

              // Caso nenhuma tecla de controle seja usável, sinaliza isso
              if( combinationsText.endsWith( ': ' ) ) combinationsText += 'Ø'

              // Caso uma ou mais teclas de controle sejam usáveis, remove operador de adição final
              else combinationsText = combinationsText.replace( / \+ $/, '' );

              // Adiciona separador de combinações
              combinationsText += ' | ';
            }

            // Remove separador de combinações final, se aplicável
            combinationsText = combinationsText.replace( / \| $/, '' );

            // Retorna texto sobre combinações
            return combinationsText;
          }
        }
      },
      styleData: {
        borderColor: m.data.colors.white,
        headingBackgroundColor: m.data.colors.semiblack2,
        bodyOddRowsColor: m.data.colors.semiwhite3,
        bodyEvenRowsColor: m.data.colors.gray
      }
    } );
  }
}

/// Propriedades do protótipo
GameScreen.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: GameScreen }
} );
