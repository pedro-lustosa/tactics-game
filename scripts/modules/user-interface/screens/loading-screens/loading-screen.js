// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const LoadingScreen = function ( config = {} ) {
  // Iniciação de propriedades
  LoadingScreen.init( this );

  // Superconstrutor
  m.GameScreen.call( this, config );

  // Configurações pós-superconstrutor

  /// Eventos

  //// Para configurar entrada da tela
  m.events.screenChangeEnd.enter.add( this, LoadingScreen.setupScreenEntering );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  LoadingScreen.init = function ( loadingScreen ) {
    // Chama 'init' ascendente
    m.GameScreen.init( loadingScreen );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( loadingScreen instanceof LoadingScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    loadingScreen.name = 'loading-screen';

    /// Carregador de ativos
    loadingScreen.loader = new PIXI.Loader();

    /// Contedor dos textos de carregamento
    loadingScreen.textsContainer = new PIXI.Container();

    // Atribuição de propriedades de 'textsContainer'
    let textsContainer = loadingScreen.textsContainer;

    /// Texto sobre carregamento concluído
    textsContainer.startText = null;

    /// Texto sobre carregamento em progressso
    textsContainer.inProgressText = null;

    /// Texto sobre progresso do carregamento
    textsContainer.progressText = null;
  }
}

/// Propriedades do protótipo
LoadingScreen.prototype = Object.create( m.GameScreen.prototype, {
  // Construtor
  constructor: { value: LoadingScreen }
} );

// Métodos não configuráveis de 'LoadingScreen'
Object.defineProperties( LoadingScreen, {
  // Configura entrada da tela
  setupScreenEntering: {
    value: function () {
      // Identificadores
      var { loader, textsContainer } = this,
          screenTexts = m.languages.components.loadingScreen(),
          defaultTextStyle = new PIXI.TextStyle( {
            fontFamily: m.assets.fonts.sourceSansPro.default.name,
            fontSize: 22,
            padding: m.assets.fonts.padding,
            fill: 'white'
          } ),
          startTextStyle = new PIXI.TextStyle( {
            fontFamily: m.assets.fonts.sourceSansPro.default.name,
            fontSize: 26,
            padding: m.assets.fonts.padding,
            fill: 'white',
            fontVariant: 'small-caps'
          } ),
          startText = textsContainer.startText = textsContainer.addChild( new PIXI.Text( screenTexts.start, startTextStyle ) ),
          inProgressText = textsContainer.inProgressText = textsContainer.addChild( new PIXI.Text( screenTexts.inProgress, defaultTextStyle ) ),
          progressText = textsContainer.progressText = textsContainer.addChild( new PIXI.Text( '0%', defaultTextStyle ) );

      // Definição de ativos a serem carregados
      setAssets: {
        // Define pré-carregamento dos áudios
        for( let audioType of [ 'soundTracks', 'soundEffects' ] )
          for( let audio in m.assets.audios[ audioType ] ) m.assets.audios[ audioType ][ audio ].preload = 'auto';

        // Define texturas
        loader.add( [ ...m.assets.textureAtlases ] );

        // Define fontes
        for( let size in m.assets.fonts.sourceSansPro ) loader.add( m.assets.fonts.sourceSansPro[ size ].file );
      }

      // Configurações dos componentes
      configComponents: {
        // Posicionamentos

        /// Texto sobre progresso do carregamento
        progressText.x = inProgressText.width + 15;

        /// Texto sobre carregamento concluído
        startText.position.set( textsContainer.width * .5 - startText.width * .5, inProgressText.height + 15 );

        /// Contedor dos textos
        textsContainer.position.set( m.app.pixi.screen.width * .5 - textsContainer.width * .5, m.app.pixi.screen.height * .5 - textsContainer.height * .5 );

        // Torna parcialmente transparente texto de conclusão do carregamento
        startText.alpha = .25;

        // Eventos

        /// Para atualizar texto sobre progresso do carregamento
        loader.onProgress.add( loader => progressText.text = Math.round( loader.progress ) + '%' );

        /// Para emissão do som de clique ao se clicar em texto de carregamento
        startText.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

        /// Para mudança de tela ao se clicar no texto de carregamento concluído
        startText.addListener( 'click', () => m.GameScreen.change( new m.OpeningScreen() ) );
      }

      // Inseri na tela textos sobre carregamento
      this.addChild( textsContainer );

      // Realiza carregamento de ativos, e de dados do usuário
      Promise.all( [ loadAssets(), m.GameUser.loadData() ] ).then(
        // Executa operações finais após carregamento dos ativos
        finishLoading,

        // Informa a usuário eventual erro ao se tentar carregar ativos
        () => m.noticeBar.show( m.languages.notices.getNetworkText( 'failed-request' ), 'lifted' )
      );

      // Funções

      /// Carrega ativos do Pixi
      function loadAssets() {
        // Retorna promessa a ser cumprida após carregamento dos ativos
        return new Promise( ( resolve, reject ) => loader.load( () => resolve( true ) ) );
      }

      /// Ações a serem executadas após conclusão de carregamento dos ativos
      function finishLoading( data ) {
        // Identificadores
        var userData = data[ 1 ],
            gameUser = m.GameUser.current = new m.GameUser( { userData: userData } );

        // Define volume inicial de áudios
        m.assets.audios.setStartingVolume( userData.preferences.audioVolume );

        // Atualiza transparência do texto de iniciar
        startText.alpha = 1;

        // Permite que texto de iniciar execute evento de mudança de tela
        startText.buttonMode = startText.interactive = true;

        // Caso ambiente seja o de desenvolvimento, salva usuário do jogo em um identificador global
        if( m.app.isInDevelopment ) window.gameUser = gameUser;
      }
    }
  }
} );
