// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DeckBuildingScreen = function ( config = {} ) {
  // Iniciação de propriedades
  DeckBuildingScreen.init( this );

  // Identificadores
  var { mainDeck } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !mainDeck || mainDeck instanceof m.MainDeck );

  // Superconstrutor
  m.GameScreen.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição de baralhos

  //// Principal
  this.decks.main = mainDeck?.copy( { includeName: true, includeDate: true } ) ?? new m.MainDeck( { name: this.name, owner: m.GameUser.current } );

  //// Coadjuvante
  this.decks.side = this.decks.main.sideDeck ?? new m.SideDeck( { name: this.name, mainDeck: this.decks.main } );

  //// Ativo
  this.decks.active = this.decks.main;

  //// Original
  if( mainDeck instanceof m.MainDeck ) this.decks.base = mainDeck;

  /// Definição de se tela é uma para edição de um baralho já existente
  this.isToEdit = m.GameUser.current.decks.includes( mainDeck );

  /// Eventos

  //// Para configurar entrada da tela
  m.events.screenChangeEnd.enter.add( this, DeckBuildingScreen.setupScreenEntering );

  //// Para configurar saída da tela
  m.events.screenChangeStart.leave.add( this, DeckBuildingScreen.setupScreenLeaving );
}

/// Propriedades do construtor
defineProperties: {
  // Modo de layout padrão da tela
  DeckBuildingScreen.layoutMode = 1;

  // Iniciação de propriedades
  DeckBuildingScreen.init = function ( deckBuildingScreen ) {
    // Chama 'init' ascendente
    m.GameScreen.init( deckBuildingScreen );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deckBuildingScreen instanceof DeckBuildingScreen );

    // Atribuição de propriedades iniciais

    /// Nome
    deckBuildingScreen.name = 'deck-building-screen';

    /// Músicas
    Object.assign( deckBuildingScreen.soundTracks, {
      // Música padrão
      'night-snow': m.assets.audios.soundTracks[ 'night-snow' ]
    } );

    /// Indica se tela é uma para edição de um baralho já existente
    deckBuildingScreen.isToEdit = false;

    /// Baralhos da tela
    deckBuildingScreen.decks = {};

    /// Filtros de cartas
    deckBuildingScreen.cardFilters = {};

    /// Listas de cartas
    deckBuildingScreen.cardLists = {};

    /// Contedor de informações
    deckBuildingScreen.infoContainer = new PIXI.Container();

    /// Altera seção visível da tela
    deckBuildingScreen.changeSection = function( newSection ) {
      // Identificadores
      var { cardFilters, cardLists } = this,
          currentBar = Object.values( cardFilters.bars ).find( bar => bar.visible ),
          currentList = Object.values( cardLists.sections ).find( cardList => cardList.visible ),
          [ newBar, targetMenuOption, newList ] = [ cardFilters.bars[ newSection ], cardFilters.menu.options[ newSection ], cardLists.sections[ newSection ] ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( currentBar && currentList && newBar && newList );

      // Encerra função caso seção atual seja igual à nova ou seção nova não tenha entradas na lista
      if( currentList == newList || !newList.entries.length ) return false;

      // Oculta menu de seleção de seções
      cardFilters.menu.visible = false;

      // Caso esta exista, limpa destaque da opção do menu atinente à nova seção da tela
      if( targetMenuOption ) this.cardFilters.menu.drawOption( targetMenuOption );

      // Revela barra de filtros da nova seção, e oculta a da antiga
      [ newBar.visible, currentBar.visible ] = [ true, false ];

      // Revela lista da nova seção, e oculta a da antiga
      [ newList.visible, currentList.visible ] = [ true, false ];
    }

    /// Redefine layout das molduras de cartas para o inicial
    deckBuildingScreen.resetCardFramesLayout = function () {
      // Identificadores
      var { leftFrame } = this.infoContainer.sections;

      // Altera modo de layout para o 1
      DeckBuildingScreen.setLayout( 1 );

      // Limpa conteúdo da moldura esquerda
      leftFrame.content.clear();
    }

    /// Altera o modo de layout da tela de modo a fixar em uma moldura o componente de comando do comandante do baralho
    deckBuildingScreen.fixCommandComponent = function () {
      // Identificadores
      var infoSections = this.infoContainer.sections,
          targetCommander = this.decks.main.cards.commander;

      // Não executa função se baralho não estiver com um comandante
      if( !targetCommander ) return false;

      // Não executa função se modo de layout atual for o 2
      if( DeckBuildingScreen.layoutMode == 2 ) return false;

      // Muda layout da tela
      DeckBuildingScreen.setLayout( 2 );

      // Foca moldura esquerda
      infoSections.leftFrame.focus();

      // Limpa conteúdo da moldura esquerda
      infoSections.leftFrame.content.clear();

      // Define conteúdo de moldura direita como o do comandante
      infoSections.rightFrame.content.init( targetCommander );

      // Muda componente do conteúdo de moldura direita para o de comando
      infoSections.rightFrame.content.changeVisibleComponent( infoSections.rightFrame.content.container.info.cardCommand );
    }

    /// Sai da tela atual
    deckBuildingScreen.exit = function ( event ) {
      // Valida execução da função se via evento de teclado
      if( event?.type == 'keydown' && event.key != 'Escape' ) return;

      // Altera a tela em função de se a atual é uma de edição ou não
      return m.GameScreen.change( deckBuildingScreen.isToEdit ? new m.DeckViewingScreen() : new m.OpeningScreen() );
    }

    // Atribuição de propriedades de 'decks'
    let decks = deckBuildingScreen.decks;

    /// Baralho principal
    decks.main = null;

    /// Baralho coadjuvante
    decks.side = null;

    /// Baralho ativo
    decks.active = null;

    /// Baralho original
    decks.base = null;

    /// Execução atual de 'decks.saveDecksSet'
    decks.saveDecksSetExecution = null;

    /// Adiciona carta ao baralho ativo
    decks.addToActive = function ( targetCard ) {
      // Identificadores
      var middleFrame = deckBuildingScreen.infoContainer.sections.middleFrame,
          validation;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Object.values( deckBuildingScreen.cardLists.sections ).some( list => list?.cards.includes( targetCard ) ) );

      // Tenta adicionar ao baralho cópia da carta alvo, e captura validação da operação
      validation = this.active?.add( [ targetCard.copy() ] );

      // Para caso carta não tenha sido adicionada ao baralho
      if( !validation?.isValid ) {
        // Emite som de ação negada
        m.assets.audios.soundEffects[ 'click-deny' ].play();

        // Notifica usuário sobre necessidade de definir o baralho ativo
        if( !validation )
          m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-no-active-deck' ), 'lifted' );

        // Notifica usuário sobre quantidade máxima de cartas alcançada
        else if( validation.maxDeckCards === false )
          m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-over-max-cards' ), 'lifted' );

        // Notifica usuário sobre quantidade insuficiente de moedas
        else if( validation.maxCoinsSpent === false )
          m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-over-max-coins' ), 'lifted' );

        // Notifica usuário sobre quantidade máxima de cartas iguais alcançada
        else if( validation.maxSameCards === false )
          m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-over-availability' ), 'lifted' );

        // Notifica usuário sobre quantidade máxima de cartas comandadas alcançada
        else if( validation.maxCommandCards === false )
          m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-command-invalid' ), 'lifted' );

        // Encerra função
        return false;
      }

      // Emite som de carta adicionada
      m.assets.audios.soundEffects[ 'click-next' ].play();

      // Caso baralho principal seja o ativo, atualiza componente de comando do seu comandante
      if( this.active.type == 'main-deck' ) DeckBuildingScreen.updateCommandComponent.call( deckBuildingScreen );

      // Para caso tipo de conteúdo da moldura central seja sobre baralhos
      if( middleFrame.content.type == 'deck' ) {
        // Configura baralho ativo na moldura
        DeckBuildingScreen.configDecksContent.call( deckBuildingScreen, this.active );

        // Configura botão de salvar da moldura
        middleFrame.content.container.saveButton.config();
      }
    }

    /// Remove carta do baralho ativo
    decks.removeFromActive = function ( targetCard ) {
      // Identificadores
      var middleFrame = deckBuildingScreen.infoContainer.sections.middleFrame;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.active );
        m.oAssert( this.active.cards.includes( targetCard ) );
      }

      // Para caso carta alvo seja a de comandante
      if( targetCard.content.stats.command ) {
        // Define mensagem sobre ação negada
        let denyMessage = m.languages.notices.getDeckText(
          deckBuildingScreen.isToEdit ? 'deck-editing-no-commander-removal' : 'deck-building-no-commander-removal'
        );

        // Emite som de ação negada
        m.assets.audios.soundEffects[ 'click-deny' ].play();

        // Notifica usuário sobre ação negada
        m.noticeBar.show( denyMessage, 'lifted' );

        // Encerra função
        return false;
      }

      // Remove do baralho carta passada
      this.active.remove( [ targetCard ] );

      // Emite som de carta removida
      m.assets.audios.soundEffects[ 'click-previous' ].play();

      // Caso baralho principal seja o ativo, atualiza componente de comando do seu comandante
      if( this.active.type == 'main-deck' ) DeckBuildingScreen.updateCommandComponent.call( deckBuildingScreen );

      // Para caso tipo de conteúdo da moldura central seja sobre baralhos
      if( middleFrame.content.type == 'deck' ) {
        // Configura baralho ativo na moldura
        DeckBuildingScreen.configDecksContent.call( deckBuildingScreen, this.active );

        // Configura botão de salvar da moldura
        middleFrame.content.container.saveButton.config();
      }
    }

    /// Atualiza baralho ativo
    decks.updateActiveDeck = function ( eventData ) {
      // Identificadores
      var { currentComponent } = eventData;

      // Atualização e retorno do baralho ativo
      return this.active = ( function () {
        switch( currentComponent.name ) {
          case 'mainDeck':
            return this.main;
          case 'sideDeck':
            return this.side;
          default:
            return null;
        }
      } ).call( this );
    }

    /// Salva conjunto de baralhos
    decks.saveDecksSet = function * () {
      // Identificadores
      var decksSet = [ this.main ].concat( this.main.sideDeck ?? [] );

      // Apenas executa função caso conjunto de baralhos esteja válido
      if( !deckBuildingScreen.infoContainer.sections.middleFrame.content.container?.components?.validation.sections.body.isValidDeck ) return false;

      // Caso conjunto de baralhos ainda possa receber cartas, retorna uma modal de confirmação da operação de salvar
      if( decksSet.some( deck => deck.cards.length < deck.constructor.maxCards && deck.coins.max - deck.coins.spent >= 20 ) )
        yield new m.ConfirmationModal( {
          text: m.languages.notices.getModalText( 'confirm-deck-set-saving', { decksSet: decksSet, isNewDeck: !deckBuildingScreen.isToEdit } ),
          acceptAction: () => decks.saveDecksSetExecution.next(),
          declineAction: () => decks.saveDecksSetExecution = null
        } ).replace();

      // Para caso conjunto de baralhos seja um novo
      if( !deckBuildingScreen.isToEdit ) {
        // Inseri modal de submissão do nome do conjunto de baralhos
        yield new m.PromptModal( {
          name: 'decks-name-modal',
          text: m.languages.notices.getModalText( 'name-deck-set', { isDeckSet: this.main.sideDeck?.cards.length } ),
          inputConfig: ( targetModal, targetInput ) => m.Deck.configDeckNamesInput( decks.main, targetModal, targetInput ),
          action: setDeckNames.bind( this )
        } ).replace();

        // Salva conjunto de baralhos no servidor
        yield m.app.sendHttpRequest( {
          path: `/actions/save-deck${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
          body: JSON.stringify( { deck: this.main.toObject() } ),
          isToBlock: true,
          onLoad: () => decks.saveDecksSetExecution.next(),
          onError: () => decks.saveDecksSetExecution = null
        } );

        // Salva conjunto de baralhos no arranjo de baralhos do usuário atual
        m.GameUser.current.decks.unshift( this.main );

        // Gera datas de criação e modificação do baralho
        this.main.modifiedDate = this.main.createdDate = new Date().toISOString();
      }

      // Do contrário
      else {
        // Atualiza conjunto de baralhos no servidor
        yield m.app.sendHttpRequest( {
          path: `/actions/update-deck${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ], [ 'requires-auth', '1' ] ],
          body: JSON.stringify( { deck: this.main.toObject(), index: m.GameUser.current.decks.indexOf( this.base ) } ),
          isToBlock: true,
          onLoad: () => decks.saveDecksSetExecution.next(),
          onError: () => decks.saveDecksSetExecution = null
        } );

        // Remove conjunto original, e adiciona a versão editada em seu lugar
        m.GameUser.current.decks.splice( m.GameUser.current.decks.indexOf( this.base ), 1, this.main );

        // Atualiza data de modificação do baralho
        this.main.modifiedDate = new Date().toISOString();
      }

      // Emite aviso informando que conjunto de baralhos foi salvo
      return new m.AlertModal( {
        text: m.languages.notices.getModalText( 'deck-set-saved', { isDeckSet: this.main.sideDeck?.cards.length, isNewDeck: !deckBuildingScreen.isToEdit } ),
        action: () => m.GameScreen.change( new m.OpeningScreen() )
      } ).replace();

      // Funções

      /// Atribui nomes aos baralhos
      function setDeckNames() {
        // Caso atribuição de nome ocorra, torna a executar operação de salvar conjunto de baralhos
        if( m.Deck.setDeckNames( this.main ) ) this.saveDecksSetExecution.next();
      }
    }

    /// Redefine baralhos
    decks.reset = function () {
      // Muda seção da tela para a do comando
      deckBuildingScreen.changeSection( 'command' );

      // Redefine baralho ativo
      this.active = this.main;

      // Limpa baralhos
      for( let deckType of [ 'main', 'side' ] ) this[ deckType ].clear().forEach( card => card.destroy( { children: true } ) );

      // Itera por listas outras que a de comando
      for( let section of [ 'creatures', 'items', 'spells' ] ) {
        // Identificadores
        let targetList = deckBuildingScreen.cardLists.sections[ section ];

        // Remove lista alvo da tela, e a destrói
        deckBuildingScreen.removeChild( targetList ).destroy( { children: true } );

        // Desvincula lista alvo de seção de listas
        deckBuildingScreen.cardLists.sections[ section ] = null;
      }

      // Redefine conteúdo da moldura central para o sobre comandantes
      DeckBuildingScreen.buildCommanderContent.call( deckBuildingScreen );

      // Redefine layout das molduras de cartas para o inicial
      return deckBuildingScreen.resetCardFramesLayout();
    }

    // Atribuição de propriedades de 'cardFilters'
    let cardFilters = deckBuildingScreen.cardFilters;

    /// Barras dos filtros de cartas
    cardFilters.bars = {}

    /// Menu de seleção da barra ativa dos filtros de cartas
    cardFilters.menu = null;

    /// Alterna aplicação de um filtro de cartas a sua lista
    cardFilters.toggle = function ( targetSection, targetFilter, eventData ) {
      // Não executa função caso ela tenha sido acionada por um evento de clique e a tecla 'ctrl' esteja pressionada
      if( eventData?.type == 'click' && m.app.keysPressed.ctrl ) return false;

      // Alternação da aplicação do filtro
      return targetFilter.isActive ? cardFilters.apply( targetSection, targetFilter ) : cardFilters.remove( targetSection, targetFilter );
    }

    /// Aplica um filtro de cartas a sua lista
    cardFilters.apply = function ( targetSection, targetFilter ) {
      // Não executa função caso filtro esteja bloqueado
      if( targetFilter.isBlocked ) return false;

      // Identificadores
      var targetList = deckBuildingScreen.cardLists.sections[ targetSection ],
          targetEntries = targetFilter.targetCards.map( card => card.parent );

      // Remove da lista alvo entradas afetadas
      for( let entry of targetEntries ) targetList.removeChild( entry );

      // Reposiciona entradas
      return targetList.entries.arrange();
    }

    /// Remove um filtro de cartas de sua lista
    cardFilters.remove = function ( targetSection, targetFilter ) {
      // Não executa função caso filtro esteja bloqueado
      if( targetFilter.isBlocked ) return false;

      // Identificadores
      var targetList = deckBuildingScreen.cardLists.sections[ targetSection ],
          targetEntries = targetFilter.targetCards.map( card => card.parent ),
          activeFilters = deckBuildingScreen.cardFilters.bars[ targetSection ].sections.body.activeFilters;

      // Itera por filtros ainda ativos
      for( let filter of activeFilters ) {
        // Caso a seção alvo não seja a de comando, filtra filtros do mesmo conjunto de filtros
        if( targetSection != 'command' && filter.parent == targetFilter.parent ) continue;

        // Filtra cartas afetadas pela inativação do filtro alvo mas ainda filtradas por filtros ativos
        targetEntries = targetEntries.filter( entry => !filter.targetCards.includes( entry.card ) );
      }

      // Adiciona à lista alvo entradas afetadas
      for( let entry of targetEntries ) targetList.addChild( entry );

      // Reposiciona entradas
      return targetList.entries.arrange();
    }

    /// Alterna aplicação de um filtro de cartas a sua lista
    cardFilters.applyUntargetedFilters = function ( targetSection, targetFilter, eventData ) {
      // Não executa função caso filtro alvo esteja bloqueado
      if( targetFilter.isBlocked ) return false;

      // Não executa função caso ela tenha sido acionada por um evento de clique e a tecla 'ctrl' não esteja pressionada
      if( eventData?.type == 'click' && !m.app.keysPressed.ctrl ) return false;

      // Identificadores
      var siblingFilters = targetFilter.parent.filtersArray.filter( filter => filter != targetFilter );

      // Remove filtro alvo
      this.remove( targetSection, targetFilter );

      // Aplica demais filtros
      for( let filter of siblingFilters ) this.apply( targetSection, filter );
    }

    /// Notifica usuário sobre falta de cartas para o filtro alvo, se aplicável
    cardFilters.notifyCardsLackage = function ( targetFilter ) {
      // Não executa função se filtro não estiver bloqueado ou tiver cartas filtráveis
      if( !targetFilter.isBlocked || targetFilter.targetCards.length ) return false;

      // Notificação para usuário
      m.noticeBar.show( m.languages.notices.getDeckText( 'deck-building-no-card-to-filter' ), 'lifted' );
    }

    // Atribuição de propriedades de 'cardFilters.bars'
    let cardFilterBars = cardFilters.bars;

    /// Comando
    cardFilterBars.command = new m.CardsFilterElement( { type: 'command' } );

    /// Criaturas
    cardFilterBars.creatures = new m.CardsFilterElement( { type: 'creature' } );

    /// Equipamentos
    cardFilterBars.items = new m.CardsFilterElement( { type: 'item' } );

    /// Magias
    cardFilterBars.spells = new m.CardsFilterElement( { type: 'spell' } );

    // Atribuição de propriedades de 'cardLists'
    let cardLists = deckBuildingScreen.cardLists;

    /// Seções
    cardLists.sections = {};

    /// Navegação
    cardLists.navigation = {};

    // Atribuição de propriedades de 'cardLists.sections'
    let cardListsSections = cardLists.sections;

    /// Comando
    cardListsSections.command = new m.CardsList( { type: 'command' } );

    /// Criaturas
    cardListsSections.creatures = null;

    /// Equipamentos
    cardListsSections.items = null;

    /// Magias
    cardListsSections.spells = null;

    // Atribuição de propriedades de 'cardLists.navigation'
    let listsNavigation = cardLists.navigation;

    /// Seta para navegação anterior
    listsNavigation.previousArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] );

    /// Seta para navegação posterior
    listsNavigation.nextArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] );

    /// Navega para entradas anteriores da lista visível
    listsNavigation.toPrevious = function ( event ) {
      // Valida execução da função se via evento de teclado
      if( event?.type == 'keydown' && event.key != 'ArrowLeft' ) return;

      // Identificadores
      var entriesQuantity = m.app.keysPressed.ctrl ? 6 : 1,
          targetList = Object.values( cardLists.sections ).find( list => list?.visible ),
          currentEntryIndex = targetList.entries.findIndex( entry => !entry.x && entry.parent == targetList ),
          targetEntryIndex = currentEntryIndex,
          hoverText = deckBuildingScreen.children.find( child => child.name == `${ targetList.name }-hover-text` );

      // Encerra função caso não tenha sido encontrada uma entrada inserida em sua lista
      if( currentEntryIndex == -1 ) return false;

      // Remove texto suspenso da lista, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Realiza atualização de navegação uma quantidade de vezes igual a 'entriesQuantity'
      for( let i = 0; i < entriesQuantity; i++ ) {
        // Identifica primeira entrada antes da atual e que está na lista
        do {
          // Atualiza índice alvo, e o ajusta caso esteja fora do escopo do arranjo
          if( --targetEntryIndex == -1 ) targetEntryIndex = targetList.entries.length - 1;
        }
        while( targetList.entries[ targetEntryIndex ].parent != targetList );
      }

      // Reposiciona entradas da lista alvo
      return targetList.entries.arrange( targetEntryIndex );
    }

    /// Navega para entradas posteriores da lista visível
    listsNavigation.toNext = function ( event ) {
      // Valida execução da função se via evento de teclado
      if( event?.type == 'keydown' && event.key != 'ArrowRight' ) return;

      // Identificadores
      var entriesQuantity = m.app.keysPressed.ctrl ? 6 : 1,
          targetList = Object.values( cardLists.sections ).find( list => list?.visible ),
          currentEntryIndex = targetList.entries.findIndex( entry => !entry.x && entry.parent == targetList ),
          targetEntryIndex = currentEntryIndex,
          hoverText = deckBuildingScreen.children.find( child => child.name == `${ targetList.name }-hover-text` );

      // Encerra função caso não tenha sido encontrada uma entrada inserida em sua lista
      if( currentEntryIndex == -1 ) return false;

      // Remove texto suspenso da lista, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Realiza atualização de navegação uma quantidade de vezes igual a 'entriesQuantity'
      for( let i = 0; i < entriesQuantity; i++ ) {
        // Identifica primeira entrada depois da atual e que está na lista
        do {
          // Atualiza índice alvo, e o ajusta caso esteja fora do escopo do arranjo
          if( ++targetEntryIndex == targetList.entries.length ) targetEntryIndex = 0;
        }
        while( targetList.entries[ targetEntryIndex ].parent != targetList );
      }

      // Reposiciona entradas da lista alvo
      return targetList.entries.arrange( targetEntryIndex );
    }

    // Atribuição de propriedades de 'infoContainer'
    let infoContainer = deckBuildingScreen.infoContainer;

    /// Seções
    infoContainer.sections = {};

    /// Indica qual será a próxima moldura a exibir uma carta
    infoContainer.nextDisplayFrame = null;

    // Atribuição de propriedades de 'infoContainer.sections'
    let infoContainerSections = infoContainer.sections;

    /// Moldura esquerda
    infoContainerSections.leftFrame = null;

    /// Moldura central
    infoContainerSections.middleFrame = null;

    /// Moldura direita
    infoContainerSections.rightFrame = null;
  }

  // Atribui layout a ser usado pela tela
  DeckBuildingScreen.setLayout = function ( layoutMode = 1 ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( m.GameScreen.current instanceof DeckBuildingScreen );
      m.oAssert( [ 1, 2 ].includes( layoutMode ) );
    }

    // Não executa função caso novo modo de layout já seja o atual
    if( DeckBuildingScreen.layoutMode == layoutMode ) return false;

    // Identificadores
    var infoContainer = m.GameScreen.current.infoContainer,
        { leftFrame, middleFrame, rightFrame } = infoContainer.sections,
        middleFrameComponent = Object.values( middleFrame.content.container.componentsMenu?.options ?? {} ).find( option => option.source.visible )?.source,
        rebuildMiddleFrameContent = ( function () {
          switch( middleFrame.content.type ) {
            case 'misc':
              return DeckBuildingScreen.buildCommanderContent.bind( m.GameScreen.current );
            case 'deck':
              return DeckBuildingScreen.buildDecksContent.bind( m.GameScreen.current );
          }
        } )();

    // Identifica novo modo de layout
    switch( layoutMode ) {
      case 1: {
        // Quando existente, desfaz foco de molduras
        m.GameScreen.current.frames.find( frame => frame.isInFocus )?.blur();

        // Define próxima moldura a exibir conteúdo de cartas como a esquerda
        infoContainer.nextDisplayFrame = leftFrame;

        // Remove moldura direita da tela, e a destrói
        infoContainer.removeChild( rightFrame ).destroy( { children: true } );

        // Desvincula moldura direita do contedor de informações
        infoContainer.sections.rightFrame = null;

        // Atualiza tipo da moldura central
        middleFrame.type = 'deckFrame';

        // Atualiza bordas da moldura central
        middleFrame.borders = 'top-bottom-right';

        // Encerra operação
        break;
      }
      case 2: {
        // Cria moldura direita
        infoContainer.addChild( infoContainer.sections.rightFrame = new m.GameFrame( {
          name: 'frame-3', type: 'fieldFrame', x: middleFrame.x + middleFrame.width
        } ) );

        // Caso moldura esquerda já esteja com conteúdo, define próxima moldura a exibir conteúdo de cartas como a direita
        if( leftFrame.content.container ) infoContainer.nextDisplayFrame = infoContainer.sections.rightFrame;

        // Atualiza tipo da moldura central
        middleFrame.type = 'fieldFrame';

        // Atualiza bordas da moldura central
        middleFrame.borders = 'top-bottom';
      }
    }

    // Limpa conteúdo da moldura central
    middleFrame.content.clear();

    // Ajusta molduras da tela
    DeckBuildingScreen.fitScreenFrames.call( m.GameScreen.current );

    // Remonta conteúdo atual da moldura central
    rebuildMiddleFrameContent();

    // Altera componente visível na moldura central para o originalmente visível, se aplicável
    if( middleFrameComponent )
      middleFrame.content.changeVisibleComponent( Object.values( middleFrame.content.container.componentsMenu.options ).find(
        option => option.source.name == middleFrameComponent.name
      ).source );

    // Salva novo modo de layout
    return DeckBuildingScreen.layoutMode = layoutMode;
  }
}

/// Propriedades do protótipo
DeckBuildingScreen.prototype = Object.create( m.GameScreen.prototype, {
  // Construtor
  constructor: { value: DeckBuildingScreen }
} );

// Métodos não configuráveis de 'DeckBuildingScreen'
Object.defineProperties( DeckBuildingScreen, {
  // Configura entrada da tela
  setupScreenEntering: {
    value: function () {
      // Identificadores
      var { cardFilters, cardLists, infoContainer } = this,
          menuBarHeadings = Object.keys( cardFilters.bars ).filter( key => key != 'command' ).map( key => cardFilters.bars[ key ].sections.heading ),
          largestBarHeadingWidth = menuBarHeadings.reduce( ( accumulator, current ) => accumulator.width < current.width ? current : accumulator ).width;

      // Configurações das barras de filtros
      configFilterBars: {
        // Itera por barras de filtros
        for( let barName in cardFilters.bars ) {
          // Captura barra atual
          let filterBar = cardFilters.bars[ barName ];

          // Por padrão, oculta barra
          filterBar.visible = false;

          // Caso barra alvo seja uma vinculada ao menu, ajusta a renderização de seu cabeçalho se menor que o maior dentre os demais
          if( menuBarHeadings.includes( filterBar.sections.heading ) && filterBar.sections.heading.width != largestBarHeadingWidth )
            filterBar.draw( largestBarHeadingWidth );

          // Inseri barra na tela
          this.addChild( filterBar );

          // Posiciona barra na tela
          filterBar.position.set( 0, 0 );

          // Itera por seções de filtros da barra alvo
          for( let section in filterBar.sections.body.sections ) {
            // Identificadores
            let filtersSection = filterBar.sections.body.sections[ section ];

            // Itera por filtros da barra alvo
            for( let filter of filtersSection.filtersArray ) {
              // Adiciona evento para alternação da aplicação do filtro a sua lista
              filter.addListener( 'click', cardFilters.toggle.bind( cardFilters, barName, filter ) );

              // Adiciona evento para remover filtro alvo, e aplicar todos os outros de sua seção
              filter.addListener( 'click', cardFilters.applyUntargetedFilters.bind( cardFilters, barName, filter ) );

              // Adiciona evento para notificar usuário sobre falta de cartas para o filtro alvo, quando aplicável
              filter.addListener( 'click', cardFilters.notifyCardsLackage.bind( cardFilters, filter ) );
            }
          }
        }

        // Configura barra de filtros inicial
        DeckBuildingScreen.configCardFilters.call( this, 'command' );

        // Revela barra de filtros inicial
        cardFilters.bars.command.visible = true;
      }

      // Configurações do menu das barras de filtros
      configFilterMenu: {
        // Gera menu
        let sectionsMenu = cardFilters.menu = this.addChild( new m.ToggleableMenu( {
              triggers: menuBarHeadings,
              options: {
                creatures: {
                  text: m.languages.keywords.getPrimitiveType( 'creature', { plural: true } ),
                  action: this.changeSection.bind( this, 'creatures' ),
                  isToKeepAfterAction: true
                },
                items: {
                  text: m.languages.keywords.getPrimitiveType( 'item', { plural: true } ),
                  action: this.changeSection.bind( this, 'items' ),
                  isToKeepAfterAction: true
                },
                spells: {
                  text: m.languages.keywords.getPrimitiveType( 'spell', { plural: true } ),
                  action: this.changeSection.bind( this, 'spells' ),
                  isToKeepAfterAction: true
                }
              },
              styleData: {
                menuContentWidth: largestBarHeadingWidth,
                optionPaddingX: 0
              }
            } ) );

        // Posiciona menu
        sectionsMenu.position.set( 0, cardFilters.bars.command.y + cardFilters.bars.command.height );
      }

      // Configurações do contedor de informações
      configInfoContainer: {
        // Identificadores
        let sections = infoContainer.sections,
            { leftFrame, middleFrame, rightFrame } = sections,
            middleFrameType = DeckBuildingScreen.layoutMode == 1 ? 'deckFrame' : 'fieldFrame',
            middleFrameBorders = 'top-bottom' + ( DeckBuildingScreen.layoutMode == 1 ? '-right' : '' );

        // Geração das molduras

        /// Esquerda
        leftFrame = infoContainer.addChild( infoContainer.nextDisplayFrame = sections.leftFrame = new m.GameFrame( {
          name: 'frame-1', type: 'fieldFrame', x: 0
        } ) );

        /// Central
        middleFrame = infoContainer.addChild( sections.middleFrame = new m.GameFrame( {
          name: 'frame-2', type: middleFrameType, x: leftFrame.x + leftFrame.width, borders: middleFrameBorders
        } ) );

        /// Direita
        if( DeckBuildingScreen.layoutMode == 2 ) rightFrame = infoContainer.addChild( sections.rightFrame = new m.GameFrame( {
          name: 'frame-3', type: 'fieldFrame', x: middleFrame.x + middleFrame.width
        } ) );

        // Na moldura central, monta conteúdo sobre seção de comandante
        DeckBuildingScreen.buildCommanderContent.call( this );

        // Eventos

        /// Para atualização do baralho ativo de acordo com o componente visível da moldura central
        m.events.frameChangeEnd.component.add( middleFrame, this.decks.updateActiveDeck, { context: this.decks } );

        // Inseri contedor de informações na tela
        this.addChild( infoContainer );

        // Posiciona contedor de informações
        infoContainer.position.set( m.app.pixi.screen.width * .5 - infoContainer.width * .5, m.app.pixi.screen.height - infoContainer.height );
      }

      // Configurações da lista de cartas
      configCardLists: {
        // Identificadores
        let listSections = cardLists.sections,
            { previousArrow, nextArrow } = cardLists.navigation;

        // Configura setas
        for( let arrow of [ previousArrow, nextArrow ] ) {
          // Tamanho
          arrow.scale.set( .4 );

          // Âncoras
          arrow.anchor.set( .5 );

          // Interatividade
          arrow.buttonMode = arrow.interactive = true;
        }

        // Rotaciona seta de navegação posterior
        nextArrow.angle = 180;

        // Adiciona componentes à tela
        this.addChild( listSections.command, previousArrow, nextArrow );

        // Posicionamentos

        /// Da lista de cartas de comandantes
        DeckBuildingScreen.setListPosition.call( this, 'command' );

        /// Horizontal da seta para navegação anterior
        previousArrow.x = listSections.command.x * .5;

        /// Horizontal da seta para navegação posterior
        nextArrow.x = m.app.pixi.screen.width - previousArrow.x;

        /// Vertical das setas de navegação
        for( let arrow of [ previousArrow, nextArrow ] ) arrow.y = listSections.command.y + listSections.command.height * .5;

        // Eventos

        /// Para emissão do som de retroceder
        previousArrow.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

        /// Para emissão do som de avançar
        nextArrow.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

        /// Para navegação para carta anterior da lista visível

        //// Por clique
        previousArrow.addListener( 'click', cardLists.navigation.toPrevious, cardLists.navigation );

        //// Por tecla
        window.addEventListener( 'keydown', cardLists.navigation.toPrevious );

        /// Para navegação para carta posterior da lista visível

        //// Por clique
        nextArrow.addListener( 'click', cardLists.navigation.toNext, cardLists.navigation );

        //// Por tecla
        window.addEventListener( 'keydown', cardLists.navigation.toNext );

        /// Itera por cartas de comandantes
        for( let card of listSections.command.cards ) {
          // Cria lista de criaturas, equipamentos e magias selecionáveis pelo comandante do baralho
          card.addListener( 'dblclick', DeckBuildingScreen.buildCardLists.bind( this, card ) );

          // Alterna visibilidade entre lista de comandantes e outra
          card.addListener( 'dblclick', this.changeSection.bind(
            this, [ 'creature', 'item', 'spell' ].find( type => card.content.stats.command.scopes[ type ].total.max ) + 's'
          ) );

          // Adiciona carta alvo ao baralho ativo
          card.addListener( 'dblclick', this.decks.addToActive.bind( this.decks, card ) );

          // Na moldura central, monta conteúdo sobre seção dos baralhos
          card.addListener( 'dblclick', DeckBuildingScreen.buildDecksContent, this );

          // Altera o modo de layout da tela de modo a fixar em uma moldura comando do comandante do baralho
          card.addListener( 'dblclick', this.fixCommandComponent, this );
        }
      }

      // Para caso baralho principal já esteja com cartas
      if( this.decks.main.cards.length ) {
        // Identificadores
        let deckCards = this.decks.main.cards;

        // Cria lista de criaturas, equipamentos e magias selecionáveis pelo comandante do baralho
        DeckBuildingScreen.buildCardLists.call( this, deckCards.commander );

        // Alterna visibilidade entre lista de comandantes e outra
        this.changeSection( [ 'creature', 'item', 'spell' ].find( type => deckCards.commander.content.stats.command.scopes[ type ].total.max ) + 's' );

        // Na moldura central, monta conteúdo sobre seção dos baralhos
        DeckBuildingScreen.buildDecksContent.call( this );

        // Altera o modo de layout da tela de modo a fixar em uma moldura comando do comandante do baralho
        this.fixCommandComponent();
      }

      // Eventos

      /// Adiciona evento de definição da moldura em foco
      window.addEventListener( 'keydown', DeckBuildingScreen.setFocusedFrame );

      /// Adiciona evento de mudança do componente visível do conteúdo em molduras
      window.addEventListener( 'keydown', DeckBuildingScreen.changeFrameContentComponent );

      /// Adiciona evento de inativação de todos os filtros de dadas seções
      window.addEventListener( 'keydown', DeckBuildingScreen.inactivateSectionFilters );

      /// Adiciona evento de alternamento da exibição das designações de cartas
      window.addEventListener( 'keydown', DeckBuildingScreen.toggleCardDesignations );

      /// Adiciona evento de alternamento do modo de layout
      window.addEventListener( 'keydown', DeckBuildingScreen.changeLayoutMode );

      /// Adiciona evento para mostrar atalhos da tela
      window.addEventListener( 'keydown', this.showScreenShortcuts );

      /// Adiciona evento para alternar entrada no modo informativo
      window.addEventListener( 'keydown', this.toggleInfoMode );

      /// Adiciona evento para sair da tela
      window.addEventListener( 'keydown', this.exit );

      // Variáveis globais, para depuração
      if( m.app.isInDevelopment ) [ window.cardFilters, window.cardLists, window.infoContainer ] = [ cardFilters, cardLists, infoContainer ];
    }
  },
  // Configura saída da tela
  setupScreenLeaving: {
    value: function () {
      // Identificadores
      var { navigation } = this.cardLists;

      // Remove eventos 'keydown'
      for( let eventHandler of [
        DeckBuildingScreen.setFocusedFrame, DeckBuildingScreen.changeFrameContentComponent, DeckBuildingScreen.inactivateSectionFilters,
        DeckBuildingScreen.toggleCardDesignations, DeckBuildingScreen.changeLayoutMode, navigation.toPrevious, navigation.toNext, this.showScreenShortcuts,
        this.toggleInfoMode, this.exit
      ] )
        window.removeEventListener( 'keydown', eventHandler );

      // Remove variáveis globais, se em ambiente de desenvolvimento
      if( m.app.isInDevelopment )
        for( let key of [ 'cardFilters', 'cardLists', 'infoContainer' ] ) delete window[ key ];

      // Redefine modo de layout da tela para o 1
      DeckBuildingScreen.layoutMode = 1;
    }
  },
  // Ajusta molduras da tela
  fitScreenFrames: {
    value: function () {
      // Identificadores
      var infoContainer = this.infoContainer,
          { middleFrame, rightFrame } = infoContainer.sections;

      // Redefine largura da moldura central com base no tamanho da tela
      middleFrame.draw( m.app.pixi.screen.width - ( infoContainer.width - middleFrame.width ) );

      // Reposiciona moldura direita, se aplicável
      if( rightFrame ) rightFrame.x = middleFrame.x + middleFrame.width;
    }
  },
  // Monta listas de cartas
  buildCardLists: {
    value: function ( targetCommander ) {
      // Identificadores
      var listSections = this.cardLists.sections;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( targetCommander?.content?.stats?.command );

      // Geração de listas

      /// De criaturas
      listSections.creatures = new m.CardsList( { type: 'creature', commander: targetCommander } );

      /// De equipamentos
      listSections.items = new m.CardsList( { type: 'item', commander: targetCommander } );

      /// De magias
      listSections.spells = new m.CardsList( { type: 'spell', commander: targetCommander } );

      // Configura listas geradas
      for( let listType of [ 'creatures', 'items', 'spells' ] ) {
        // Visibilidade
        listSections[ listType ].visible = false;

        // Inserção na tela
        this.addChildAt( listSections[ listType ], 0 );

        // Posicionamento
        DeckBuildingScreen.setListPosition.call( this, listType );

        // Configura filtros de cartas
        DeckBuildingScreen.configCardFilters.call( this, listType );

        // Itera por cartas da lista alvo
        for( let card of listSections[ listType ].cards ) {
          // Evento para adicionar carta alvo ao baralho ativo
          card.addListener( 'dblclick', this.decks.addToActive.bind( this.decks, card ) );
        }
      }
    }
  },
  // Configura filtros de cartas
  configCardFilters: {
    value: function ( sectionType ) {
      // Identificadores
      var [ filtersBody, listCards ] = [ this.cardFilters.bars[ sectionType ].sections.body, this.cardLists.sections[ sectionType ].cards ],
          propertyPathStart = sectionType == 'command' ? [ 'content', 'stats', 'command', 'scopes', 'creature' ] : [ 'content', 'typeset' ];

      // Itera por seções no corpo da barra de filtros alvo
      for( let section in filtersBody.sections ) {
        // Identificadores
        let filtersSection = filtersBody.sections[ section ],
            propertyPath, getTargetCards;

        // Para caso tipo de seção seja a de comando
        if( sectionType == 'command' ) {
          // Define caminho para a propriedade relativa à seção de filtros alvo
          propertyPath = propertyPathStart.concat( Array.of( section ) );

          // Define função a identificar cartas afetadas por filtros da seção alvo
          getTargetCards = ( path, name ) => listCards.filter( card =>
            card.content.stats.command.scopes.creature.total.max &&
            path.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], card )[ name ].max
          );
        }
        else {
          // Define caminho para a propriedade relativa à seção do filtro alvo
          propertyPath = propertyPathStart.concat( section.search( /[A-Z]/ ) == -1 ?
            Array.of( section ) : section.replace( /[A-Z].*/g, '.' ).concat( section.slice( section.search( /[A-Z]/ ) ).toLowerCase() ).split( '.' )
          );

          // Define função a identificar cartas afetadas por filtros da seção alvo
          getTargetCards = ( path, name ) => listCards.filter(
            card => path.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], card ) == name
          );
        }

        // Itera por filtros da seção de filtros
        for( let filter of filtersSection.filtersArray ) {
          // Trata nome do filtro
          let filterName = filter.name == 'null' ? null : filter.name;

          // Inativa filtro
          filtersBody.inactivateFilter( filter );

          // Salva cartas afetadas pelo filtro
          filter.targetCards = getTargetCards( propertyPath, filterName );

          // Controla bloqueio do filtro
          filter.targetCards.length ? filtersBody.unblockFilter( filter ) : filtersBody.blockFilter( filter );
        }
      }
    }
  },
  // Posiciona lista
  setListPosition: {
    value: function ( listType ) {
      // Identificadores
      var [ filterBar, cardsList, infoContainer ] = [ this.cardFilters.bars[ listType ], this.cardLists.sections[ listType ], this.infoContainer ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( filterBar && cardsList );

      // Posiciona lista
      cardsList.position.set(
        ( m.app.pixi.screen.width - cardsList.displayOffset ) / 2,
        filterBar.height + ( m.app.pixi.screen.height - filterBar.height - infoContainer.height ) * .5 - cardsList.height * .5
      );
    }
  },
  // Na moldura central, monta conteúdo sobre seção de comandante
  buildCommanderContent: {
    value: function () {
      // Identificadores
      var targetFrame = this.infoContainer.sections.middleFrame,
          frameContainer = new PIXI.Container();

      // Inicia conteúdo na moldura alvo
      targetFrame.content.init( frameContainer );

      // Montagens

      /// Do título sobre escolha do comandante
      DeckBuildingScreen.buildCommanderCaption( targetFrame );

      /// Do botão de voltar para a tela de abertura
      DeckBuildingScreen.buildBackButton( targetFrame, this.exit );
    }
  },
  // Na moldura central, monta conteúdo sobre seção dos baralhos
  buildDecksContent: {
    value: function () {
      // Identificadores
      var targetFrame = this.infoContainer.sections.middleFrame,
          backAction = this.isToEdit ? this.exit : this.decks.reset.bind( this.decks );

      // Inicia montagem do conteúdo dos baralhos
      targetFrame.content.init( this.decks.main );

      // Configura conteúdo de baralhos da moldura montada
      for( let deckType of [ 'main', 'side' ] ) DeckBuildingScreen.configDecksContent.call( this, this.decks[ deckType ] );

      // Adiciona à moldura botões de ação

      /// Botão de salvar
      DeckBuildingScreen.buildSaveButton.call( this, targetFrame );

      /// Botão de voltar
      DeckBuildingScreen.buildBackButton( targetFrame, backAction, 'edge' );
    }
  },
  // Na moldura central, configura conteúdo sobre seção dos baralhos
  configDecksContent: {
    value: function ( targetDeck ) {
      // Identificadores
      var targetFrame = this.infoContainer.sections.middleFrame,
          deckComponent = targetDeck.type == 'main-deck' ? targetFrame.content.container.components.mainDeck : targetFrame.content.container.components.sideDeck;

      // Não executa função caso o componente de baralho alvo não exista
      if( !deckComponent ) return false;

      // Atualiza moldura, tomando como referência baralho passado
      targetFrame.content.updateDeck( targetDeck );

      // Itera por entradas da lista de cartas do baralho passado
      for( let entry of deckComponent.sections.body.children ) {
        // Encerra iteração caso entrada alvo não tenha uma carta lhe vinculada
        if( !entry.title ) break;

        // Adiciona evento para remoção da carta vinculada à entrada alvo
        entry.title.addListener( 'dblclick', this.decks.removeFromActive.bind( this.decks, entry.title.source ) );
      }
    }
  },
  // Atualiza componentes de comando de comandantes
  updateCommandComponent: {
    value: function () {
      // Identificadores
      var infoSections = this.infoContainer.sections,
          targetFrames = Object.values( infoSections ).filter( frame => frame?.content.container?.source?.content?.stats?.command );

      // Atualiza componentes de comandantes
      for( let frame of targetFrames ) frame.content.updateCard( { targetComponents: [ 'cardCommand' ] } );
    }
  },
  // Monta título sobre escolha de comandante
  buildCommanderCaption: {
    value: function ( targetFrame ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          caption = frameContainer.caption = frameContainer.addChild( new PIXI.Container() ),
          frameTexts = m.languages.components.commanderFrame();

      // Geração do título
      caption.heading = caption.addChild( new PIXI.BitmapText(
        frameTexts.title, { fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22 }
      ) );

      // Geração do subtítulo
      caption.subheading = caption.addChild( new PIXI.BitmapText(
        frameTexts.subtitle, { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 }
      ) );

      // Posicionamento horizontal dos componentes do título
      for( let headingType of [ 'heading', 'subheading' ] ) caption[ headingType ].x = caption.width * .5 - caption[ headingType ].width * .5;

      /// Posicionamento vertical do subtítulo
      caption.subheading.y = caption.heading.y + caption.heading.height;

      // Posicionamento do título
      caption.position.set( targetFrame.width * .5 - caption.width * .5, 6 );

      // Retorna título
      return caption;
    }
  },
  // Monta botão de voltar
  buildBackButton: {
    value: function ( targetFrame, action, positionX = 'middle' ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          backButton = frameContainer.backButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'middle', 'edge' ].includes( positionX ) );

      // Geração do texto
      backButton.bitmap = backButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'back' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Renderização do botão

      /// Preenchimento
      backButton.beginFill( m.data.colors.darkRed );

      /// Confecção
      backButton.drawRoundedRect( 0, 0, backButton.width + buttonsData.paddingX, backButton.height + buttonsData.paddingY, buttonsData.roundedCorners );

      /// Finalização
      backButton.endFill();

      // Posicionamento do texto
      backButton.bitmap.position.set( backButton.width * .5 - backButton.bitmap.width * .5, backButton.height * .5 - backButton.bitmap.height * .5 );

      // Posicionamento do botão
      backButton.position.set(
        positionX == 'middle' ? targetFrame.width * .5 - backButton.width * .5 : targetFrame.width - backButton.width - targetFrame.width * .015,
        targetFrame.height - backButton.height - 9
      );

      // Interatividade
      backButton.buttonMode = backButton.interactive = true;

      // Eventos

      /// Emissão do som de retroceder
      backButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

      /// Execução da ação passada
      backButton.addListener( 'click', action );

      // Retorna botão
      return backButton;
    }
  },
  // Monta botão de salvar
  buildSaveButton: {
    value: function ( targetFrame ) {
      // Identificadores
      var frameContainer = targetFrame.content.container,
          validationComponent = frameContainer.components.validation,
          saveButton = frameContainer.saveButton = frameContainer.addChild( new PIXI.Graphics() ),
          buttonsData = m.data.sizes.buttons;

      // Método para configuração do botão
      saveButton.config = function () {
        // Renderização

        /// Limpa renderização atual
        this.clear();

        /// Cor de preenchimento
        this.beginFill( validationComponent.sections.body.isValidDeck ? m.data.colors.darkGreen : m.data.colors.darkFadedGreen );

        /// Confecção
        this.drawRoundedRect( 0, 0, this.width + buttonsData.paddingX, this.height + buttonsData.paddingY, buttonsData.roundedCorners );

        /// Finalização
        this.endFill();

        // Texto suspenso

        /// Definição
        this.hoverText = validationComponent.sections.body.isValidDeck ? '' : m.languages.components.deckFrame().invalidDeck;

        /// Nomeação
        this.hoverTextName = `${ targetFrame.name }-fixedContent-hover-text`;
      }

      // Geração do texto
      saveButton.bitmap = saveButton.addChild( new PIXI.BitmapText(
        m.languages.snippets.getUserInterfaceAction( 'save' ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: buttonsData.fontSize }
      ) );

      // Configuração do botão
      saveButton.config();

      // Posicionamento do texto do botão
      saveButton.bitmap.position.set( saveButton.width * .5 - saveButton.bitmap.width * .5, saveButton.height * .5 - saveButton.bitmap.height * .5 );

      // Posicionamento do botão
      saveButton.position.set( targetFrame.width * .015, targetFrame.height - saveButton.height - 9 );

      // Interatividade
      saveButton.buttonMode = saveButton.interactive = true;

      // Eventos

      /// Para exibição de texto suspenso do ícone
      saveButton.addListener( 'mouseover', m.app.showHoverText );

      /// Emite som de interação, a depender de se baralho está válido ou não
      saveButton.addListener( 'click', () =>
        m.assets.audios.soundEffects[ validationComponent.sections.body.isValidDeck ? 'click-next' : 'click-deny' ].play()
      );

      /// Para adição do conjunto de baralhos da tela aos do usuário atual
      saveButton.addListener( 'click', () => ( this.decks.saveDecksSetExecution = this.decks.saveDecksSet() ).next(), this );

      // Retorna botão
      return saveButton;
    }
  },
  // Define moldura em foco
  setFocusedFrame: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit3' ].includes( event.code ) || event.ctrlKey || !event.shiftKey || !event.altKey ) return;

      // Apenas executa função caso a moldura direita exista
      if( !m.GameScreen.current.infoContainer.sections.rightFrame ) return;

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      event.preventDefault();

      // Alternação da moldura em foco
      switch( event.code ) {
        case 'Digit1':
          return m.GameScreen.current.infoContainer.sections.leftFrame.toggleFocus();
        case 'Digit3':
          return m.GameScreen.current.infoContainer.sections.rightFrame.toggleFocus();
      }
    }
  },
  // Altera componente visível do conteúdo em molduras
  changeFrameContentComponent: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Digit1', 'Digit2', 'Digit3' ].includes( event.code ) || event.altKey || [ event.shiftKey, event.ctrlKey ].every( key => !key ) ) return;

      // Captura conteúdo da moldura alvo
      var targetFrameContent = ( function () {
        switch( event.code ) {
          case 'Digit1':
            return m.GameScreen.current.infoContainer.sections.leftFrame.content;
          case 'Digit2':
            return m.GameScreen.current.infoContainer.sections.middleFrame.content;
          case 'Digit3':
            return m.GameScreen.current.infoContainer.sections.rightFrame?.content;
        }
      } )();

      // Identifica conteúdo alvo
      var targetComponent = ( function () {
        switch( targetFrameContent?.type ) {
          case 'card':
            return event.shiftKey && event.ctrlKey ? targetFrameContent.container.info.cardCommand :
                   event.shiftKey                  ? targetFrameContent.container.info.cardEffects :
                   event.ctrlKey                   ? targetFrameContent.container.info.cardStats : null;
          case 'deck':
            return event.shiftKey ? targetFrameContent.container.components.validation :
                   event.ctrlKey  ? targetFrameContent.container.components.sideDeck : null;
          default:
            return null;
        }
      } )();

      // Alteração do componente visível na moldura
      if( targetComponent ) targetFrameContent.toggleVisibleComponent( targetComponent );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Inativa todos os filtros de dadas seções
  inactivateSectionFilters: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit4' ) return;

      // Identificadores
      var cardFilters = m.GameScreen.current.cardFilters,
          targetBar = Object.values( cardFilters.bars ).find( bar => bar.visible ),
          targetSection = targetBar.type == 'command' ? 'command' : targetBar.type + 's',
          targetBarBody = targetBar.sections.body,
          sectionsByKey = { ctrlKey: 0, shiftKey: 1, altKey: 2 },
          filterSections = [];

      // Identifica seções afetadas em função de teclas de controle pressionadas
      for( let key of Object.keys( sectionsByKey ) )
        if( event[ key ] ) filterSections.push( targetBarBody.children[ sectionsByKey[ key ] ] );

      // Caso nenhuma tecla de controle tenha sido pressionada, mira todas as seções da barra alvo
      if( !filterSections.length ) filterSections = targetBarBody.children;

      // Itera por seções afetadas
      for( let section of filterSections ) {
        // Itera por filtros das seções afetadas
        for( let filter of section.filtersArray ) {
          // Inativa filtro
          targetBarBody.inactivateFilter( filter );

          // Remove filtro
          cardFilters.remove( targetSection, filter );
        }
      }

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Alterna exibição das designações de cartas
  toggleCardDesignations: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit9' || [ event.ctrlKey, event.shiftKey ].every( key => !key ) ) return;

      // Captura escopos alvos
      var scopes =
            event.shiftKey && event.ctrlKey ? [ 'list', 'frame' ] :
            event.shiftKey                  ? [ 'frame' ] :
            event.ctrlKey                   ? [ 'list' ] : null;

      // Define se seções dos escopos devem ser exibidas ou não
      var scopesBoolean = !m.Card.designationVisibility[ scopes[0] ];

      // Itera por escopos alvos
      for( let scope of scopes ) {
        // Atualiza exibição das designações em escopo atual
        m.Card.designationVisibility[ scope ] = scopesBoolean;

        // Atualiza exibição das designações em cartas do escopo atual
        switch( scope ) {
          case 'list':
            // Para cartas nas listas
            for( let listName in m.GameScreen.current.cardLists.sections ) {
              // Identificadores
              let listCards = m.GameScreen.current.cardLists.sections[ listName ]?.cards;

              // Filtra listas inexistentes
              if( !listCards ) continue;

              // Atualiza visibilidade de seções de cartas da lista alvo
              for( let card of listCards ) card.body.updateDesignationVisibility( scope );
            }

            // Encerra operação
            break;
          case 'frame':
            // Para cartas nas molduras
            for( let frame of m.GameScreen.current.frames )
              if( frame.content.type == 'card' ) frame.content.container.body.updateDesignationVisibility( scope );
        }
      }

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  },
  // Alterna modo de layout
  changeLayoutMode: {
    value: function ( event ) {
      // Desconsidera eventos decorrentes de teclas a continuarem a ser pressionadas
      if( event.repeat ) return;

      // Apenas executa função caso teclas pressionadas sejam válidas
      if( event.code != 'Digit0' || !event.ctrlKey || event.shiftKey || event.altKey ) return;

      // Alternação do modo de layout
      DeckBuildingScreen.setLayout( DeckBuildingScreen.layoutMode == 1 ? 2 : 1 );

      // Impede que teclas do evento acionem funcionalidades próprias do navegador
      return event.preventDefault();
    }
  }
} );
