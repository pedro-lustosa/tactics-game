// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const InteractionModal = function ( config = {} ) {
  // Iniciação de propriedades
  InteractionModal.init( this );

  // Identificadores
  var { action, component: modalComponent, validationChecker = {}, isToKeepAfterAction = false, isSilent = false, isValid = false } = config,
      { submitButton } = this.content.inputsSet;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( typeof action == 'function' );
    m.oAssert( modalComponent );
    m.oAssert( Object.keys( validationChecker ).every( key =>
      [ 'click', 'dblclick', 'rightclick', 'mouseover', 'mouseout', 'added', 'removed' ].includes( key )
    ) );
    m.oAssert( Object.values( validationChecker ).every( value =>
      Array.isArray( value ) && value.every( component => component instanceof PIXI.DisplayObject )
    ) );
  }

  // Superconstrutor
  m.GameModal.call( this, config );

  // Configurações pós-superconstrutor

  /// Indica se configurações atuais do componente da modal estão válidas para submissão, e atualiza cor de seu botão
  if( modalComponent.isValid = Boolean( isValid ) ) submitButton.draw( m.data.colors.darkBlue );

  /// Botão da modal
  configSubmitButton: {
    // Identificadores
    let validationEvents = Object.keys( validationChecker );

    // Posicionamento horizontal
    submitButton.x = this.width * .5 - submitButton.width * .5;

    // Eventos

    /// Atualiza cor de validação do botão após emissão de eventos passados nos componentes passados
    for( let eventName of validationEvents )
      for( let component of validationEvents[ eventName ] )
        component.addListener( eventName, () => submitButton.draw( modalComponent.isValid ? m.data.colors.darkBlue : m.data.colors.darkFadedBlue ) );

    /// Emissão do som do botão, segundo validade do componente da modal e se não explicitado o contrário
    if( !isSilent ) submitButton.addListener( 'click', () => m.assets.audios.soundEffects[ modalComponent.isValid ? 'click-next' : 'click-deny' ].play() );

    /// Ação do botão
    submitButton.addListener( 'click', action );

    /// Para caso modal deva ser removida após a ação passada
    if( !isToKeepAfterAction ) submitButton.addListener( 'click', () => modalComponent.isValid ? this.remove() : null, this );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  InteractionModal.init = function ( interactionModal ) {
    // Chama 'init' ascendente
    m.GameModal.init( interactionModal );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( interactionModal instanceof InteractionModal );

    // Atribuição de propriedades iniciais

    /// Nome
    interactionModal.name = 'interaction-modal';

    // Atribuição de propriedades de 'content.inputsSet'
    let inputsSet = interactionModal.content.inputsSet;

    /// Botão de submissão
    inputsSet.submitButton = inputsSet.buildButton( m.languages.snippets.getUserInterfaceAction( 'submit' ), m.data.colors.darkFadedBlue );
  }
}

/// Propriedades do protótipo
InteractionModal.prototype = Object.create( m.GameModal.prototype, {
  // Construtor
  constructor: { value: InteractionModal }
} );
