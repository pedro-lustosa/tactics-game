// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const AlertModal = function ( config = {} ) {
  // Iniciação de propriedades
  AlertModal.init( this );

  // Identificadores
  var { action, isToKeepAfterAction = false, isSilent = false } = config,
      { modalButton } = this.content.inputsSet;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !action || typeof action == 'function' );

  // Superconstrutor
  m.GameModal.call( this, config );

  // Configurações pós-superconstrutor

  /// Botão da modal
  configModalButton: {
    // Posicionamento horizontal
    modalButton.x = this.width * .5 - modalButton.width * .5;

    // Eventos

    /// Emissão do som do botão, se não explicitado o contrário
    if( !isSilent ) modalButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

    /// Ação do botão
    modalButton.addListener( 'click', action ?? this.remove.bind( this ) );

    /// Para caso modal deva ser removida após a ação passada
    if( action && !isToKeepAfterAction ) modalButton.addListener( 'click', this.remove.bind( this ) );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  AlertModal.init = function ( alertModal ) {
    // Chama 'init' ascendente
    m.GameModal.init( alertModal );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( alertModal instanceof AlertModal );

    // Atribuição de propriedades iniciais

    /// Nome
    alertModal.name = 'alert-modal';

    // Atribuição de propriedades de 'content.inputsSet'
    let inputsSet = alertModal.content.inputsSet;

    /// Botão da modal
    inputsSet.modalButton = inputsSet.buildButton( m.languages.snippets.getUserInterfaceAction( 'acknowledge' ), m.data.colors.darkBlue );
  }
}

/// Propriedades do protótipo
AlertModal.prototype = Object.create( m.GameModal.prototype, {
  // Construtor
  constructor: { value: AlertModal }
} );
