// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameModal = function ( config = {} ) {
  // Identificadores
  var { name, isFromUser = false, contentWidth = 516, padding: modalPadding = 12, text: modalText, component: modalComponent } = config,
      bodyMarginBottom = 12;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( ( name || this.name ) && [ name, this.name ].every( value => !value || typeof value == 'string' ) );
    m.oAssert( typeof isFromUser == 'boolean' );
    m.oAssert( [ contentWidth, modalPadding ].every( value => value > 0 ) );
    m.oAssert( ( modalText || modalComponent ) && !( modalText && modalComponent ) );
    m.oAssert( !modalText || typeof modalText == 'string' );
    m.oAssert( !modalComponent || modalComponent instanceof PIXI.DisplayObject );
  }

  // Superconstrutor
  PIXI.Graphics.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome da modal
  if( name ) this.name = name.endsWith( '-modal' ) ? name : name + '-modal';

  /// Atribuição da indição de se modal é uma gerada pelo usuário
  this.isFromUser = isFromUser;

  /// Para caso tenha sido passado um conteúdo textual
  if( modalText ) {
    // Atribuição do texto de bitmap
    this.content.body = new PIXI.BitmapText(
      modalText, { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16, align: 'center', maxWidth: contentWidth }
    );
  }

  /// Para caso tenha sido passado um componente
  else if( modalComponent ) {
    // Atribuição do componente
    this.content.body = modalComponent;

    // Caso largura do componente seja maior que largura passada para o conteúdo da modal, ajusta esta para que equivala àquela
    contentWidth = Math.max( contentWidth, modalComponent.width );
  }

  /// Inserção dos elementos da modal
  this.addChild( this.content.body, this.content.inputsSet );

  /// Renderização da modal
  this.draw( contentWidth + modalPadding * 2, this.content.body.height + this.content.inputsSet.height + bodyMarginBottom + modalPadding * 2 );

  /// Posicionamentos

  //// Do corpo do conteúdo da modal
  this.content.body.position.set( this.width * .5 - this.content.body.width * .5, modalPadding );

  //// Do conjunto de operadores
  this.content.inputsSet.y = this.height - this.content.inputsSet.height - modalPadding;

  //// Da modal
  this.position.set( m.app.pixi.screen.width * .5 - this.width * .5, m.app.pixi.screen.height * .5 - this.height * .5 );
}

/// Propriedades do construtor
defineProperties: {
  // Modal atualmente inserida
  GameModal.current = null;

  // Modais a serem inseridas
  GameModal.queue = [];

  // Iniciação de propriedades
  GameModal.init = function ( gameModal ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( gameModal instanceof GameModal );

    // Atribuição de propriedades iniciais

    /// Nome
    gameModal.name = '';

    /// Conteúdo
    gameModal.content = {};

    /// Indica se modal foi gerada pelo usuário
    gameModal.isFromUser = false;

    /// Renderiza modal
    gameModal.draw = function ( width = this.width, height = this.height ) {
      // Renderização

      /// Limpa renderização atual
      this.clear();

      /// Cor de preenchimento
      this.beginFill( m.data.colors.semiblack3 );

      /// Confecção
      this.drawRoundedRect( 0, 0, width, height, 10 );

      /// Finalização
      this.endFill();
    }

    /// Inseri modal no canvas
    gameModal.insert = function () {
      // Apenas executa função se modal não foi destruída
      if( this._destroyed ) return false;

      // Não executa função caso modal já esteja inserida
      if( GameModal.current == this ) return this;

      // Não executa função caso já haja uma modal inserida e nova modal a ser inserida tenha sido gerada pelo usuário
      if( GameModal.current && this.isFromUser ) return false;

      // Caso haja uma outra modal inserida, substitui-a pela nova caso a atual seja uma do usuário; do contrário, adiciona nova modal na fila de inserção
      if( GameModal.current )
        return GameModal.current.isFromUser ? this.replace() : queueModal.call( this );

      // Sinaliza início da inserção da modal
      m.events.modalChangeStart.insert.emit( this );

      // Caso modal esteja na fila de modais a serem inseridas, retira-a de lá
      if( GameModal.queue.includes( this ) ) GameModal.queue.splice( GameModal.queue.indexOf( this ), 1 );

      // Oculta barra de notificações, quando estiver visível
      m.noticeBar.hide();

      // Impede eventos 'keydown'
      m.app.blockInteractionCount.keydown++;

      // Tratamento da tela
      adjustScreen: {
        // Não executa bloco caso a tela já esteja tratada
        if( m.app.pixi.stage.children.some( child => child.name == 'insert-modal-sprite' ) ) break adjustScreen;

        // Identificadores
        let blurFilter = new PIXI.filters.BlurFilter(),
            extractor = m.app.pixi.renderer.plugins.extract,
            screenSprite = null;

        // Adiciona filtro de desfoque à tela atual
        m.GameScreen.current.filters.push( blurFilter );

        // Gera sprite a partir da imagem atual da tela
        screenSprite = PIXI.Sprite.from( extractor.image( m.GameScreen.current, 'image/jpeg' ).src );

        // Nomeia sprite da tela
        screenSprite.name = 'insert-modal-sprite';

        // Adiciona ao canvas sprite
        m.app.pixi.stage.addChild( screenSprite );

        // Oculta tela
        m.GameScreen.current.visible = false;

        // Remove filtro de desfoque da tela
        m.GameScreen.current.filters.splice( m.GameScreen.current.filters.indexOf( blurFilter ), 1 ).pop().destroy( { children: true } );
      }

      // Define modal como a atualmente inserida na tela
      GameModal.current = this;

      // Adiciona modal ao canvas
      m.app.pixi.stage.addChild( this );

      // Sinaliza fim da inserção da modal
      m.events.modalChangeEnd.insert.emit( this );

      // Retorna modal
      return this;

      // Adiciona modal à lista de modais a serem inseridas
      function queueModal() {
        // Não executa função caso modal já esteja na lista de modais a serem inseridas
        if( GameModal.queue.includes( this ) ) return this;

        // Identificadores
        var previousModal = GameModal.queue.slice( -1 ).pop() ?? GameModal.current;

        // Adiciona modal à fila de modais a serem inseridas
        GameModal.queue.push( this );

        // Adiciona evento para inserir modal após a última modal inserida prevista ser removida
        m.events.modalChangeEnd.remove.add( previousModal, this.insert, { context: this, once: true } );

        // Retorna modal
        return this;
      }
    }

    /// Substitui modal no canvas por esta
    gameModal.replace = function () {
      // Apenas executa função se modal não foi destruída
      if( this._destroyed ) return false;

      // Caso não haja uma modal inserida na tela, inseri modal atual
      if( !GameModal.current ) return this.insert();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !GameModal.queue.includes( this ) );
        m.oAssert( !this.isFromUser );
      }

      // Identificadores
      var currentModal = GameModal.current,
          nextModal = GameModal.queue[ 0 ];

      // Inseri modal na lista de modais a serem inseridas
      GameModal.queue.unshift( this );

      // Adiciona evento para inserir modal após remoção da modal atual
      m.events.modalChangeEnd.remove.add( currentModal, this.insert, { context: this, once: true } );

      // Para caso tenha havido uma modal a ser inserida após a atual
      if( nextModal ) {
        // Remove evento para inserir póxima modal após a remoção da modal atual
        m.events.modalChangeEnd.remove.remove( currentModal, nextModal.insert, { context: nextModal } );

        // Adiciona evento para inserir póxima modal após a remoção desta modal
        m.events.modalChangeEnd.remove.add( this, nextModal.insert, { context: nextModal, once: true } );
      }

      // Remove modal atualmente inserida
      currentModal.remove();

      // Retorna modal
      return this;
    }

    /// Remove modal do canvas, ou da fila de modais a serem inseridas
    gameModal.remove = function () {
      // Apenas executa função se modal não foi destruída
      if( this._destroyed ) return false;

      // Delega operação em função de se modal está no canvas ou na fila de modais
      return GameModal.current == this ? removeFromCanvas.call( this ) : GameModal.queue.includes( this ) ? removeFromQueue.call( this ) : false;

      // Funções

      /// Remove modal do canvas
      function removeFromCanvas() {
        // Sinaliza início da remoção da modal
        m.events.modalChangeStart.remove.emit( this );

        // Remoção da modal
        m.app.pixi.stage.removeChild( this );

        // Nulificação da modal atualmente inserida
        GameModal.current = null;

        // Reverte impedimento de eventos 'keydown'
        m.app.blockInteractionCount.keydown--;

        // Sinaliza fim da remoção da modal
        m.events.modalChangeEnd.remove.emit( this );

        // Destruição da modal
        this.destroy( { children: true } );

        // Tratamento da tela
        adjustScreen: {
          // Não executa bloco caso no momento haja uma outra modal inserida
          if( GameModal.current ) break adjustScreen;

          // Identificadores
          let screenSprite = m.app.pixi.stage.children.find( child => child.name == 'insert-modal-sprite' );

          // Revelação da tela
          m.GameScreen.current.visible = true;

          // Remoção do sprite
          m.app.pixi.stage.removeChild( screenSprite ).destroy( { children: true } );
        }
      }

      /// Remove modal da fila de modais a serem inseridas
      function removeFromQueue() {
        // Identificadores
        var modalIndex = GameModal.queue.indexOf( this ),
            [ previousModal, nextModal ] = [ GameModal.queue[ modalIndex - 1 ] ?? GameModal.current, GameModal.queue[ modalIndex + 1 ] ];

        // Remove evento para inserir modal alvo após a remoção da modal anterior a ela
        m.events.modalChangeEnd.remove.remove( previousModal, this.insert, { context: this } );

        // Para caso haja uma modal a ser revelada após a alvo
        if( nextModal ) {
          // Remove evento para inserir modal posterior após a remoção da modal alvo
          m.events.modalChangeEnd.remove.remove( this, nextModal.insert, { context: nextModal } );

          // Adiciona evento para inserir modal posterior a alvo após a remoção da modal anterior a alvo
          m.events.modalChangeEnd.remove.add( previousModal, nextModal.insert, { context: nextModal } );
        }

        // Remove modal da fila de modais, e a destrói
        GameModal.queue.splice( modalIndex, 1 ).pop().destroy( { children: true } );
      }
    }

    // Atribuição de propriedades de 'content'
    let content = gameModal.content;

    /// Corpo do conteúdo
    content.body = null;

    /// Conjunto de operadores
    content.inputsSet = new PIXI.Container();

    // Atribuição de propriedades de 'content.inputsSet'
    let inputsSet = content.inputsSet;

    /// Monta botão para conjunto de operadores
    inputsSet.buildButton = function ( buttonText, buttonColor, buttonWidth = 0 ) {
      // Identificadores
      var button = new PIXI.Graphics(),
          textStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -19, maxWidth: buttonWidth || 516
          },
          buttonBitmap = button.bitmap = button.addChild( new PIXI.BitmapText( buttonText, textStyle ) );

      // Configurações do botão

      /// Medidas
      [ button.paddingX, button.paddingY, button.roundedCorners ] = [ 30, 4, 10 ];

      /// Método para renderização
      button.draw = function ( color = buttonColor, width = buttonWidth ) {
        // Renderiza o botão

        /// Limpa renderização atual
        button.clear();

        /// Preenchimento
        button.beginFill( color );

        /// Confecção
        button.drawRoundedRect( 0, 0, ( width || button.width ) + button.paddingX, button.height + button.paddingY, button.roundedCorners );

        /// Finalização
        button.endFill();
      }

      // Renderização do botão
      button.draw();

      // Posicionamento do texto
      buttonBitmap.position.set( button.width * .5 - buttonBitmap.width * .5, button.height * .5 - buttonBitmap.height * .5 );

      // Interatividade
      button.buttonMode = button.interactive = true;

      // Inseri botão no conjunto de operadores de sua modal
      this.addChild( button );

      // Retorna botão
      return button;
    }
  }
}

/// Propriedades do protótipo
GameModal.prototype = Object.create( PIXI.Graphics.prototype, {
  // Construtor
  constructor: { value: GameModal }
} );
