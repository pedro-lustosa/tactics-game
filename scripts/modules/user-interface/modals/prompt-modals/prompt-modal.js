// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const PromptModal = function ( config = {} ) {
  // Iniciação de propriedades
  PromptModal.init( this );

  // Identificadores
  var { action, inputConfig, padding: modalPadding = 12, isToKeepAfterAction = false, isSilent = false } = config,
      { textInput, submitButton } = this.content.inputsSet;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( typeof action == 'function' );
    m.oAssert( !inputConfig || typeof inputConfig == 'function' );
  }

  // Superconstrutor
  m.GameModal.call( this, config );

  // Configurações pós-superconstrutor

  /// Campo de texto
  configTextInput: {
    // Posicionamento
    textInput.position.set( modalPadding, this.content.inputsSet.height - textInput.height );

    // Nomeação
    textInput.name = `${ this.name }-input`;

    // Interatividade
    textInput.interactive = true;

    // Quando campo de texto estiver fora de foco, impede que contedor do pixi seja exibido em seu lugar
    textInput.substituteText = false;

    // Indica se campo de texto está válido ou não
    textInput.isValid = false;

    // Executa configurações passadas para o campo de texto, se aplicável
    if( inputConfig ) inputConfig( this, textInput );

    // Caso campo de texto esteja válido, altera cor do botão de submissão
    if( textInput.isValid ) submitButton.draw( m.data.colors.darkBlue );

    // Evento para alternar cor do botão de submissão segundo validade do campo de texto
    textInput.addListener( 'input', () => submitButton.draw( textInput.isValid ? m.data.colors.darkBlue : m.data.colors.darkFadedBlue ) );
  }

  /// Botão de submissão
  configSubmitButton: {
    // Posicionamento horizontal
    submitButton.x = this.width - submitButton.width - modalPadding;

    // Eventos

    /// Emissão do som do botão, segundo validade do campo de texto e se não explicitado o contrário
    if( !isSilent ) submitButton.addListener( 'click', () => m.assets.audios.soundEffects[ textInput.isValid ? 'click-next' : 'click-deny' ].play() );

    /// Ação do botão
    submitButton.addListener( 'click', action );

    /// Para caso modal deva ser removida após a ação passada
    if( !isToKeepAfterAction ) submitButton.addListener( 'click', () => this.content.inputsSet.textInput.isValid ? this.remove() : null, this );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  PromptModal.init = function ( promptModal ) {
    // Chama 'init' ascendente
    m.GameModal.init( promptModal );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( promptModal instanceof PromptModal );

    // Atribuição de propriedades iniciais

    /// Nome
    promptModal.name = 'prompt-modal';

    // Atribuição de propriedades de 'content.inputsSet'
    let inputsSet = promptModal.content.inputsSet;

    /// Campo de texto
    inputsSet.textInput = inputsSet.addChild( new PIXI.TextInput( {
      input: {
        font: '15px Source Sans Pro, sans-serif', padding: '4px', width: '240px', color: PIXI.utils.hex2string( m.data.colors.white ),
        borderBottom: 'solid 1px ' + PIXI.utils.hex2string( m.data.colors.darkBlue )
      }
    } ) );

    /// Botão de submissão
    inputsSet.submitButton = inputsSet.buildButton( m.languages.snippets.getUserInterfaceAction( 'submit' ), m.data.colors.darkFadedBlue );
  }
}

/// Propriedades do protótipo
PromptModal.prototype = Object.create( m.GameModal.prototype, {
  // Construtor
  constructor: { value: PromptModal }
} );
