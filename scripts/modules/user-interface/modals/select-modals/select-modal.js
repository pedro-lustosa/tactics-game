// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const SelectModal = function ( config = {} ) {
  // Iniciação de propriedades
  SelectModal.init( this );

  // Identificadores
  var { inputsArray, isToKeepAfterAction = false, isSilent = false } = config,
      { inputsSet } = this.content;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( Array.isArray( inputsArray ) );
    m.oAssert( inputsArray.length >= 2 );
    m.oAssert( inputsArray.every( input => typeof input.text == 'string' && typeof input.action == 'function' ) );
  }

  // Configurações pré-superconstrutor

  /// Montagem dos botões da modal
  buildButtons: {
    // Itera por botões passados
    for( let input of inputsArray ) {
      // Monta botão alvo, e o captura
      let inputButton = inputsSet.buildButton( input.text, m.data.colors.darkBlue );

      // Emissão do som de clique no botão, se não explicitado o contrário
      if( !isSilent ) inputButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

      // Adiciona ação relativa ao botão alvo
      inputButton.addListener( 'click', input.action );

      // Para caso modal deva ser removida após execução da ação de qualquer botão
      if( !isToKeepAfterAction ) inputButton.addListener( 'click', this.remove.bind( this ) );
    }
  }

  /// Formatação dos botões da modal
  styleButtons: {
    // Identificadores
    let marginTop = 12;

    // Itera por botões
    for( let i = 0; i < inputsSet.children.length; i++ ) {
      // Identificadores
      let [ currentButton, previousButton ] = [ inputsSet.children[ i ], inputsSet.children[ i - 1 ] ];

      // Posiciona botão alvo verticalmente
      currentButton.y = previousButton ? previousButton.y + previousButton.height + marginTop : 0;
    }
  }

  // Superconstrutor
  m.GameModal.call( this, config );

  // Configurações pós-superconstrutor

  /// Ajustes de formatação
  styleButtons: {
    // Identificadores
    let buttons = inputsSet.children,
        largestTextWidth = buttons.reduce( ( accumulator, current ) => accumulator.bitmap.width >= current.bitmap.width ? accumulator : current ).bitmap.width;

    // Itera por botões da modal
    for( let button of buttons ) {
      // Filtra botões cuja largura do texto já seja a maior
      if( button.bitmap.width == largestTextWidth ) continue;

      // Renderiza novamente botão alvo, com sua largura uniformizada
      button.draw( m.data.colors.darkBlue, largestTextWidth );

      // Centraliza texto do botão
      button.bitmap.x = button.width * .5 - button.bitmap.width * .5;
    }

    // Posiciona horizontalmente o contedor de botões
    inputsSet.x = this.width * .5 - inputsSet.width * .5;
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SelectModal.init = function ( selectModal ) {
    // Chama 'init' ascendente
    m.GameModal.init( selectModal );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( selectModal instanceof SelectModal );

    // Atribuição de propriedades iniciais

    /// Nome
    selectModal.name = 'select-modal';
  }
}

/// Propriedades do protótipo
SelectModal.prototype = Object.create( m.GameModal.prototype, {
  // Construtor
  constructor: { value: SelectModal }
} );
