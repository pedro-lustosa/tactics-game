// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConfirmationModal = function ( config = {} ) {
  // Iniciação de propriedades
  ConfirmationModal.init( this );

  // Identificadores
  var { acceptAction, declineAction, padding: modalPadding = 12, isToKeepAfterAction = false, isSilent = false } = config,
      { acceptButton, declineButton } = this.content.inputsSet;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ acceptAction, declineAction ].some( action => action ) );
    m.oAssert( [ acceptAction, declineAction ].every( action => !action || typeof action == 'function' ) );
  }

  // Superconstrutor
  m.GameModal.call( this, config );

  // Configurações pós-superconstrutor

  /// Botão de aceitação
  configAcceptButton: {
    // Posicionamento horizontal
    acceptButton.x = modalPadding;

    // Eventos

    /// Emissão do som de confirmação, se não explicitado o contrário
    if( !isSilent ) acceptButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-next' ].play() );

    /// Ação do botão
    acceptButton.addListener( 'click', acceptAction ?? this.remove.bind( this ) );

    /// Para caso modal deva ser removida após a ação passada
    if( acceptAction && !isToKeepAfterAction ) acceptButton.addListener( 'click', this.remove.bind( this ) );
  }

  /// Botão de recusa
  configDeclineButton: {
    // Posicionamento horizontal
    declineButton.x = this.width - declineButton.width - modalPadding;

    // Eventos

    /// Emissão do som de recusa, se não explicitado o contrário
    if( !isSilent ) declineButton.addListener( 'click', () => m.assets.audios.soundEffects[ 'click-previous' ].play() );

    /// Ação do botão
    declineButton.addListener( 'click', declineAction ?? this.remove.bind( this ) );

    /// Para caso modal deva ser removida após a ação passada
    if( declineAction && !isToKeepAfterAction ) declineButton.addListener( 'click', this.remove.bind( this ) );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConfirmationModal.init = function ( confirmationModal ) {
    // Chama 'init' ascendente
    m.GameModal.init( confirmationModal );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( confirmationModal instanceof ConfirmationModal );

    // Atribuição de propriedades iniciais

    /// Nome
    confirmationModal.name = 'confirmation-modal';

    // Atribuição de propriedades de 'content.inputsSet'
    let inputsSet = confirmationModal.content.inputsSet;

    /// Botão de aceitação
    inputsSet.acceptButton = inputsSet.buildButton( m.languages.snippets.getUserInterfaceAction( 'agree' ), m.data.colors.darkGreen );

    /// Botão de recusa
    inputsSet.declineButton = inputsSet.buildButton( m.languages.snippets.getUserInterfaceAction( 'disagree' ), m.data.colors.darkRed );
  }
}

/// Propriedades do protótipo
ConfirmationModal.prototype = Object.create( m.GameModal.prototype, {
  // Construtor
  constructor: { value: ConfirmationModal }
} );
