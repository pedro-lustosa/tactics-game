// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameFrame = function ( config = {} ) {
  // Iniciação de propriedades
  GameFrame.init( this );

  // Identificadores
  var { name, type, borders, substitute, x = 0, y = 0 } = config,
      { width = GameFrame.sizes[ type ]?.width, height = GameFrame.sizes[ type ]?.height } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( name && typeof name == 'string' );
    m.oAssert( Object.keys( GameFrame.sizes ).includes( type ) );
    m.oAssert(
      !borders || [ 'all', 'none' ].includes( borders ) || borders.split( '-' ).every( value => [ 'left', 'right', 'top', 'bottom' ].includes( value ) )
    );
    m.oAssert( !substitute || substitute instanceof PIXI.DisplayObject );
  }

  // Superconstrutor
  PIXI.Graphics.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição de propriedades
  for( let property of [ 'name', 'type', 'borders', 'substitute' ] ) this[ property ] = config[ property ] ?? this[ property ];

  /// Posicionamento da moldura
  this.position.set( x, y );

  /// Geração da moldura
  this.draw( width, height );

  /// Ocultamento da moldura, caso seu substituto esteja visível
  if( this.substitute?.visible ) this.visible = false;

  /// Inserção da moldura no arranjo de molduras de sua tela
  m.GameScreen.current.frames.push( this );
}

/// Propriedades do construtor
defineProperties: {
  // Tamanhos possíveis de molduras
  GameFrame.sizes = {
    poolFrame: {
      width: 630, height: 243.75
    },
    fieldFrame: {
      width: 630, height: 359
    },
    deckFrame: {
      width: 1290, height: 359
    }
  };

  // Iniciação de propriedades
  GameFrame.init = function ( frame ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( frame instanceof GameFrame );

    // Atribuição de propriedades iniciais

    /// Nome
    frame.name = '';

    /// Tipo
    frame.type = '';

    /// Indica existência de bordas
    frame.borders = 'all';

    /// Elemento a aparacer no lugar da moldura, caso ela esteja oculta
    frame.substitute = null;

    /// Indica se moldura tem prioridade na exibição de conteúdo
    frame.isInFocus = false;

    /// Conteúdo atualmente exibido pela moldura
    frame.content = {};

    /// Renderiza moldura
    frame.draw = function ( targetWidth, targetHeight ) {
      // Identificadores
      var [ frameWidth, frameHeight ] = [ targetWidth ?? this.width, targetHeight ?? this.height ],
          borderWidth = 1,
          borderColor = this.isInFocus ? m.data.colors.green : m.data.colors.semiwhite1,
          bordersX = [ 'left', 'right' ].filter( value => this.borders.includes( value ) ),
          bordersY = [ 'top', 'bottom' ].filter( value => this.borders.includes( value ) );

      // Limpa renderização atual
      this.clear();

      // Renderiza ou define aparência de suas bordas, se existentes
      borderConfig: {
        // Para todas
        if( this.borders == 'all' ) {
          // Define bordas, a serem renderizadas junto com o corpo da moldura
          this.lineStyle( borderWidth, borderColor, 1, 0 );
          break borderConfig;
        }

        // Define cor das bordas
        this.beginFill( borderColor );

        // Para esquerda
        if( bordersX.includes( 'left' ) ) this.drawRect( 0, 0, borderWidth, frameHeight );

        // Para direita
        if( bordersX.includes( 'right' ) ) this.drawRect( frameWidth - borderWidth, 0, borderWidth, frameHeight );

        // Para superior
        if( bordersY.includes( 'top' ) ) this.drawRect( 0, 0, frameWidth, borderWidth );

        // Para inferior
        if( bordersY.includes( 'bottom' ) ) this.drawRect( 0, frameHeight - borderWidth, frameWidth, borderWidth );

        // Encerra renderização
        this.endFill();
      }

      // Renderiza corpo da moldura
      drawBody: {
        // Identificadores
        let [ xStart, yStart ] = [ bordersX.includes( 'left' ) ? borderWidth : 0, bordersY.includes( 'top' ) ? borderWidth : 0 ],
            [ bodyWidth, bodyHeight ] = [ frameWidth - borderWidth * bordersX.length, frameHeight - borderWidth * bordersY.length ];

        // Cor de preenchimento
        this.beginFill( m.data.colors.black );

        // Renderização
        this.drawRect( xStart, yStart, bodyWidth, bodyHeight );

        // Encerramento
        this.endFill();
      }

      // Retorna moldura
      return this;
    }

    /// Exibe moldura
    frame.show = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current.frames.includes( this ) );

      // Não executa função caso moldura já esteja visível
      if( this.visible ) return this;

      // Identificadores
      var hoverText = m.GameScreen.current.children.find( child => child.name?.endsWith( '-hover-text' ) ),
          actionsMenu = m.GameScreen.current.children.find( child => child.name?.endsWith( 'actions-menu' ) && !child.isSubMenu );

      // Caso um texto suspenso esteja sendo exibido e cursor esteja na área em que aparecerá moldura, remove o texto suspenso
      if( hoverText && this.oCheckCursorOver() ) m.app.removeHoverText( hoverText );

      // Caso um menu de ações esteja sendo exibido e esteja na área em que aparecerá moldura, oculta menu
      if( actionsMenu && this.oCheckElementOver( actionsMenu ) ) actionsMenu.hide();

      // Oculta elemento substituto da moldura, se aplicável
      if( this.substitute ) this.substitute.visible = false;

      // Exibe moldura
      this.visible = true;

      // Retorna moldura
      return this;
    }

    /// Oculta moldura
    frame.hide = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current.frames.includes( this ) );

      // Não executa função caso moldura já esteja oculta
      if( !this.visible ) return this;

      // Identificadores
      var hoverText = m.GameScreen.current.children.find( child => child.name?.startsWith( this.name + '-' ) && child.name.endsWith( '-hover-text' ) );

      // Remove texto suspenso de moldura, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Quando existente, remove foco da moldura
      this.blur();

      // Oculta moldura
      this.visible = false;

      // Exibe elemento substituto da moldura, se aplicável
      if( this.substitute ) this.substitute.visible = true;

      // Retorna moldura
      return this;
    }

    /// Alterna exibição da moldura
    frame.toggleDisplay = function () {
      // Alternação da exibição de moldura
      return this.visible ? this.hide() : this.show();
    }

    /// Foca moldura
    frame.focus = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current.frames.includes( this ) );

      // Não executa função caso moldura já esteja em foco
      if( this.isInFocus ) return this;

      // Caso exista uma moldura em foco, desfoca-a
      m.GameScreen.current.frames.find( screenFrame => screenFrame.isInFocus )?.blur();

      // Indica que moldura está em foco
      this.isInFocus = true;

      // Atualiza renderização da moldura
      this.draw();

      // Retorna moldura
      return this;
    }

    /// Desfoca moldura
    frame.blur = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current.frames.includes( this ) );

      // Não executa função caso moldura já não esteja em foco
      if( !this.isInFocus ) return this;

      // Indica que moldura não está mais em foco
      this.isInFocus = false;

      // Atualiza renderização da moldura
      this.draw();

      // Retorna moldura
      return this;
    }

    /// Alterna foco da moldura
    frame.toggleFocus = function () {
      // Alternação do foco da moldura
      return this.isInFocus ? this.blur() : this.focus();
    }

    // Atribuição de propriedades de 'content'
    let content = frame.content;

    /// Contedor do conteúdo exibido pela moldura
    content.container = null;

    /// Identifica tipo do conteúdo na moldura
    content.type = '';

    /// Prepara moldura para exibir um conteúdo
    content.init = function ( targetContent ) {
      // Para caso já exista um conteúdo sendo exibido pela moldura
      if( this.container ) {
        // Encerra função caso conteúdo alvo já seja o exibido na moldura
        if( this.container.source == targetContent ) return false;

        // Remove conteúdo atualmente exibido
        this.clear();
      }

      // Sinaliza início da entrada de contedor
      m.events.frameChangeStart.containerIn.emit( frame, { targetContent: targetContent } );

      // Configurações do conteúdo a ser exibido

      /// Para caso seja uma carta
      if( targetContent instanceof m.Card ) this.buildCard( targetContent )

      /// Para caso seja um baralho
      else if( targetContent instanceof m.Deck ) this.buildDeck( targetContent )

      /// Para caso seja uma casa
      else if( targetContent instanceof m.CardSlot ) this.buildCardSlot( targetContent )

      /// Para caso seja um registro de logs
      else if( typeof targetContent == 'string' && targetContent.endsWith( '-log' ) ) this.buildLog( targetContent )

      /// Para caso seja um conteúdo genérico
      else {
        // Inseri conteúdo na moldura, e o captura
        this.container = frame.addChild( targetContent );

        // Define tipo do conteúdo
        this.type = 'misc';
      }

      // Sinaliza fim da entrada de contedor
      m.events.frameChangeEnd.containerIn.emit( frame, { targetContent: targetContent } );
    }

    /// Monta conteúdo da carta a ser exibida
    content.buildCard = function ( sourceCard, isUpdate = false, componentsToUpdate = [] ) {
      // Identificadores
      var displayCard = this.container ??= new PIXI.Container(),
          isFieldFrame = frame.type == 'fieldFrame',
          componentsFontSize = isFieldFrame ? -15 : -16,
          isToHaveCaptionAlone = isFieldFrame ? true : false,
          frameTexts = m.languages.components.componentFrame( sourceCard );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( sourceCard instanceof m.Card );
        m.oAssert( !isUpdate || sourceCard == displayCard.source );
        m.oAssert( !isUpdate || componentsToUpdate.length );
        m.oAssert( componentsToUpdate.every( component => [ 'cardHeading', 'cardData', 'cardStats', 'cardCommand', 'cardEffects' ].includes( component ) ) );
      }

      // Configurações iniciais
      startConfig: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break startConfig;

        // Definição do tipo de conteúdo
        this.type = 'card';

        // Atribuição de propriedades
        [ displayCard.source, displayCard.body, displayCard.info ] = [ sourceCard, buildBody(), buildInfo() ];

        // Inserção de elementos

        /// Componentes da carta a ser exibida em seu contedor
        displayCard.addChild( displayCard.body.front, displayCard.info );

        /// Contedor da carta a ser exibida na moldura
        frame.addChild( displayCard );

        // Posicionamento horizontal do conteúdo da carta e do contedor de informações
        [ displayCard.x, displayCard.info.x ] = [ frame.width * .015, displayCard.body.front.width + 10 ];
      }

      // Montagem do conteúdo do contedor de informações
      buildInfoContent: {
        // Título
        if( !isUpdate || componentsToUpdate.includes( 'cardHeading' ) )
          displayCard.info.cardHeading.sections.title = displayCard.info.cardHeading.addChild( buildTitle() );

        // Seções de dados e estatísticas

        /// Para entes
        if( sourceCard instanceof m.Being ) {
          // Para humanoides
          if( sourceCard instanceof m.Humanoid ) {
            // Dados gerais da carta
            if( !isUpdate || componentsToUpdate.includes( 'cardData' ) ) buildHumanoidData();

            // Estatísticas da carta
            if( !isUpdate || componentsToUpdate.includes( 'cardStats' ) ) buildHumanoidStats();
          }

          // Para demais entes
          else {
            // Dados gerais da carta
            if( !isUpdate || componentsToUpdate.includes( 'cardData' ) ) buildNonHumanoidData();
          }
        }

        /// Para equipamentos
        else if( sourceCard instanceof m.Item ) {
          // Dados gerais da carta
          if( !isUpdate || componentsToUpdate.includes( 'cardData' ) ) buildItemData();

          // Estatísticas da carta, para armas
          if( sourceCard instanceof m.Weapon && ( !isUpdate || componentsToUpdate.includes( 'cardStats' ) ) ) buildWeaponStats();
        }

        /// Para magias
        else if( sourceCard instanceof m.Spell ) {
          // Dados gerais da carta
          if( !isUpdate || componentsToUpdate.includes( 'cardData' ) ) buildSpellData();
        }

        // Comando
        if( sourceCard.content.stats.command && ( !isUpdate || componentsToUpdate.includes( 'cardCommand' ) ) ) buildCommandSections( sourceCard );

        // Efeitos
        if( sourceCard.content.effects.description && ( !isUpdate || componentsToUpdate.includes( 'cardEffects' ) ) )
          displayCard.info.cardEffects.sections.description = displayCard.info.cardEffects.addChild( buildTextBlock( sourceCard.content.effects.description ) );

        // Caso moldura deva ser apenas atualizada, encerra bloco
        if( isUpdate ) break buildInfoContent;

        // Epígrafe
        displayCard.info.cardFlavor.sections.epigraph = displayCard.info.cardFlavor.addChild( buildTextBlock( sourceCard.content.epigraph ) );

        // Crédito pela ilustração da carta
        if( isFieldFrame ) displayCard.info.cardFlavor.sections.artistInfo = displayCard.info.cardFlavor.addChild( buildArtistInfo() );
      }

      // Elaboração do menu de componentes da moldura, para caso seja sua primeira montagem
      if( !isUpdate )
        displayCard.componentsMenu = displayCard.addChild( this.buildComponentsMenu(
          displayCard.info.bodyComponents.filter( component => Object.keys( displayCard.info[ component.name ].sections ).length )
        ) );

      // Posicionamentos
      positionComponents: {
        // Identificadores
        let bodyComponents = isUpdate ?
          componentsToUpdate.filter( name => name != 'cardHeading' ).map( name => displayCard.info[ name ] ) : displayCard.info.bodyComponents;

        // Posicionamento dos componentes no corpo do componente de informações
        positionBodyComponents( bodyComponents );

        // Posicionamento vertical do conteúdo da moldura
        displayCard.y = frame.height * .5 - displayCard.height * .5;

        // Posicionamento da seção de informações do artista, se existente
        if( displayCard.info.cardFlavor.sections.artistInfo )
          displayCard.info.cardFlavor.sections.artistInfo.y =
            frame.height - displayCard.y * 2 - displayCard.info.cardFlavor.y - displayCard.info.cardFlavor.sections.artistInfo.height;

        // Caso moldura deva ser apenas atualizada, encerra bloco
        if( isUpdate ) break positionComponents;

        // Posicionamento horizontal do menu de componentes
        displayCard.componentsMenu.x = frame.width - displayCard.x * 2 - displayCard.componentsMenu.width;
      }

      // Atualização do texto suspenso
      updateHoverText: {
        // Apenas executa bloco caso moldura tenha sido atualizada
        if( !isUpdate ) break updateHoverText;

        // Captura eventual texto suspenso
        let hoverText = m.GameScreen.current.children.find( child => child.name?.startsWith( frame.name + '-' ) && child.name?.endsWith( '-hover-text' ) );

        // Apenas executa bloco caso haja um texto suspenso
        if( !hoverText ) break updateHoverText;

        // Captura componentes da moldura
        let componentsMenuOptions = displayCard.componentsMenu.options,
            visibleComponent = Object.values( componentsMenuOptions ).find( option => option.source.visible )?.source;

        // Apenas executa bloco caso texto suspenso não provenha de uma das opções do menu de componentes
        if( Object.values( componentsMenuOptions ).some( option => option.oCheckCursorOver() ) ) break updateHoverText;

        // Identifica se texto suspenso provém ou não do cabeçalho
        let isHeadingHoverText = hoverText.name.includes( '-fixedContent-' );

        // Não executa bloco caso texto suspenso provenha do cabeçalho e ele não tenha sido atualizado
        if( isHeadingHoverText && !componentsToUpdate.includes( 'cardHeading' ) ) break updateHoverText;

        // Não executa bloco caso texto suspenso não provenha do cabeçalho e seu componente não tenha sido atualizado
        if( !isHeadingHoverText && !componentsToUpdate.includes( visibleComponent.name ) ) break updateHoverText;

        // Remove texto suspenso da moldura
        m.app.removeHoverText( hoverText );
      }

      // Retorna conteúdo da moldura
      return this;

      // Funções

      /// Monta imagem da carta a ser exibida
      function buildBody() {
        // Identificadores
        var cardBody = sourceCard.body.build.call( {}, frame.type, true );

        // Transmite métodos do corpo da carta original para o da carta da moldura
        for( let method of [ 'applyGrayScale', 'dropGrayScale', 'updateDesignationVisibility' ] ) cardBody[ method ] = sourceCard.body[ method ];

        // Caso carta original esteja com filtro de tons de cinza, aplica-o à carta da moldura
        if( sourceCard.body.front.background.filters.some( filter => filter.name == 'gray-scale-filter' ) ) cardBody.applyGrayScale();

        // Torna imagem da carta interativa
        cardBody.front.interactive = true;

        // Eventos

        /// Para exibição de seção temática da carta, quando cursor pairar em sua imagem
        cardBody.front.addListener( 'mouseover', toggleFlavorComponent );

        /// Para abrir seção do manual sobre cartas ao se clicar na imagem da carta
        cardBody.front.addListener( 'click', () => m.app.openManualSection( 'cards' ) );

        /// Para aplicar filtro de tons de cinza à carta na moldura quando seu efeito tiver sido inativado, ou quando ela tiver entrada em uso
        for( let eventPath of [ m.events.cardEffectEnd.disable, m.events.cardUseStart.useIn ] )
          eventPath.add( sourceCard, cardBody.applyGrayScale, { context: cardBody } );

        /// Para remover filtro de tons de cinza da carta na moldura quando seu efeito tiver sido ativado, ou quando ela tiver entrada em desuso
        for( let eventPath of [ m.events.cardEffectEnd.enable, m.events.cardUseEnd.useOut ] )
          eventPath.add( sourceCard, cardBody.dropGrayScale, { context: cardBody } );

        // Retorna corpo da carta
        return cardBody;
      }

      /// Monta contedor de informações da carta
      function buildInfo() {
        // Identificadores
        var cardInfo = new PIXI.Container();

        // Define arranjo a conter componentes do corpo do contedor de informações
        cardInfo.bodyComponents = [];

        // Itera por nome dos componentes do contedor de informações
        for( let componentName of [ 'cardHeading', 'cardData', 'cardStats', 'cardCommand', 'cardEffects', 'cardFlavor' ] ) {
          // Gera e inseri componente
          let targetComponent = cardInfo[ componentName ] = cardInfo.addChild( new PIXI.Container() );

          // Nomeia componente
          targetComponent.name = componentName;

          // Cria objeto de seções do componente
          targetComponent.sections = {};

          // Filtra 'cardHeading'
          if( componentName == 'cardHeading' ) continue;

          // Inseri componente na lista de componentes do corpo do contedor de informações
          cardInfo.bodyComponents.push( targetComponent );

          // Define visibilidade padrão do componente
          targetComponent.visible = false;
        }

        // Torna visível primeiro componente do contedor de informações
        cardInfo.bodyComponents[ 0 ].visible = true;

        // Adiciona eventos de atualização de dados da carta
        for( let scopeName of [ 'cardContentEnd', 'attachabilityChangeEnd', 'cardUseEnd', 'cardEffectEnd', 'conditionChangeEnd' ] )
          m.events[ scopeName ].any.add( sourceCard, frame.content.updateCard, { context: frame.content } );

        // Retorna contedor de informações
        return cardInfo;
      }

      /// Monta título da carta a ser exibida
      function buildTitle() {
        // Identificadores
        var titleContainer = new PIXI.Container(),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22
            },
            titleText = new PIXI.BitmapText( sourceCard.content.designation.title, textStyle ),
            titleIcon = ( function () {
              // Identificadores
              var iconPath =
                sourceCard instanceof m.Being ? { super: 'races', sub: sourceCard.content.typeset.race } :
                sourceCard instanceof m.Item ? { super: 'equipages', sub: sourceCard.content.typeset.equipage } :
                sourceCard instanceof m.Spell ? { super: 'paths', sub: sourceCard.content.typeset.path } : null;

              // Retorna ícone do título
              return new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons[ iconPath.super ][ iconPath.sub ] ] );
            } )();

        // Inserção dos elementos
        titleContainer.addChild( titleText, titleIcon );

        // Redimensionamento do ícone
        titleIcon.oScaleByGreaterSize( 34 );

        // Posicionamento do texto
        titleText.position.set( titleIcon.width + 10, titleContainer.height * .5 - titleText.height * .5 );

        // Configurações em comum entre componentes
        for( let component of [ titleText, titleIcon ] ) {
          // Torna componente interativo
          component.interactive = true;

          // Abre seção do manual sobre tipos primitivos ao se clicar no componente
          component.addListener( 'click', () => m.app.openManualSection( 'primitive-types-of-cards' ) );
        }

        // Elaboração do texto suspenso do ícone do título
        iconHoverText: {
          // Identificadores
          let hoverText = ( sourceCard instanceof m.Being ? buildBeingHoverText() : buildAssetsHoverText() ).trim();

          // Não executa bloco caso um texto suspenso não tenha sido definido
          if( !hoverText ) break iconHoverText;

          // Configurações do ícone

          /// Definição do texto suspenso
          titleIcon.hoverText = hoverText;

          /// Nome do texto suspenso
          titleIcon.hoverTextName = `${ frame.name }-fixedContent-hover-text`;

          /// Evento para exibição do texto suspenso
          titleIcon.addListener( 'mouseover', m.app.showHoverText );
        }

        // Retorna contedor do título
        return titleContainer;

        // Funções

        /// Elabora texto suspenso para ícones de entes
        function buildBeingHoverText() {
          // Identificadores
          var hoverText = '';

          // Adiciona condições
          addConditions: {
            // Não executa bloco caso ente alvo não tenha condições
            if( !sourceCard.conditions.length ) break addConditions;

            // Identificadores
            let binaryConditions = sourceCard.conditions.filter( condition => condition instanceof m.BinaryCondition ),
                gradedConditions = sourceCard.conditions.filter( condition => condition instanceof m.GradedCondition );

            // Adiciona cabeçalho do texto suspenso
            hoverText += frameTexts.conditions + ':\n';

            // Itera por grupo de condições
            for( let conditionsSet of [ binaryConditions, gradedConditions ] ) {
              // Alfabeta grupo de condições
              conditionsSet.sort( ( a, b ) =>
                m.languages.keywords.getCondition( a.name ).localeCompare( m.languages.keywords.getCondition( b.name ), m.languages.current )
              );

              // Itera por condições do grupo
              for( let condition of conditionsSet ) {
                // Adiciona ao texto suspenso nome da condição
                hoverText += `  ${ m.languages.keywords.getCondition( condition.name ) }`;

                // Caso exista, adiciona ao texto suspenso quantidade de marcadores da condição
                if( condition.markers ) hoverText += ` (${ condition.markers })`;

                // Adiciona ao texto suspenso quebra de linha
                hoverText += '\n';
              }
            }
          }

          // Adiciona indicação de se alvo está adiantável
          addForwardable: {
            // Não executa bloco caso ente alvo não esteja adiantável
            if( !sourceCard.readiness.isForwardable ) break addForwardable;

            // Adiciona ao texto suspenso informação de que alvo está adiantável
            hoverText += '\n' + m.languages.snippets.isForwardable;
          }

          // Retorna texto suspenso
          return hoverText;
        }

        /// Elabora texto suspenso para ícones de ativos
        function buildAssetsHoverText() {
          // Não executa função caso carta alvo não esteja em uso, ou seu efeito esteja ativo
          if( !sourceCard.isInUse || sourceCard.content.effects.isEnabled ) return '';

          // Indica que efeito da carta alvo está inativo
          return frameTexts.disabledEffect;
        }
      }

      /// Monta seções de dados para humanoides
      function buildHumanoidData() {
        // Identificadores
        var [ cardTypeset, cardStats ] = [ sourceCard.content.typeset, sourceCard.content.stats ],
            cardDataSections = displayCard.info.cardData.sections,
            cardDataRows = displayCard.info.cardData.rows = [];

        // Geração das primeiras fileiras
        for( let i = 1; i <= 4; i++ ) cardDataRows.push( displayCard.info.cardData.addChild( new PIXI.Container() ) );

        // Primeira fileira
        cardDataRows[ 0 ].addChild(
          // Raça
          cardDataSections.race = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'race' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getRace( cardTypeset.race ),
              hoverText: ( function () {
                // Identificadores
                var hoverText = '',
                    cardTraits = Object.values( sourceCard.content.traits );

                // Montagem do texto suspenso

                /// Cabeçalho
                hoverText += frameTexts.traitCaption + '\n\n';

                /// Corpo
                for( let trait of cardTraits ) hoverText += `${ trait.title }\n  ${ trait.description }\n\n`;

                // Retorna texto suspenso
                return hoverText;
              } )().trimEnd(),
              hoverTextFontSize: -12
            },
            manualSection: 'being-races'
          } ),
          // Ambiente
          cardDataSections.environment = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'environment' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getEnvironment( cardTypeset.environment ),
              hoverText: frameTexts.environment
            },
            manualSection: 'being-races'
          } )
        );

        // Segunda fileira
        cardDataRows[ 1 ].addChild(
          // Tamanho
          cardDataSections.size = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'size' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getSize( cardTypeset.size ) },
            manualSection: 'size'
          } ),
          // Contagem de peso
          cardDataSections.weightCount = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getMisc( 'weightCount' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: sourceCard.weightCount.current.toString(),
              hoverText: sourceCard.getAllCardAttachments().filter( attachment => attachment instanceof m.Item ).reduce( function ( accumulator, current ) {
                // Identificadores
                var weightScore = m.Item.getWeightScore( current );

                // Retorna pontuação de peso do equipamento
                return accumulator += weightScore ? m.languages.snippets.getSomethingFrom( current.content.designation.title, weightScore ) + '\n' : '';
              }, '' ).trimEnd()
            },
            manualSection: 'weight-and-weight-count'
          } )
        );

        // Ajuste de cor do texto da contagem de peso
        if( cardDataSections.weightCount.text.text >= sourceCard.weightCount.max ) cardDataSections.weightCount.text.tint = m.data.colors.red;

        // Terceira fileira
        cardDataRows[ 2 ].addChild(
          // Atuação
          cardDataSections.role = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'role' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getRoleName( cardTypeset.role.name ) },
            icon: {
              super: 'roleTypes',
              sub: cardTypeset.role.type,
              hoverText: m.languages.keywords.getRoleType( cardTypeset.role.type, { person: true } )
            },
            manualSection: 'humanoid-roles'
          } ),
          // Mãos
          cardDataSections.hands = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getMisc( 'hands' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: sourceCard.hands.available.toString(),
              hoverText: ( function () {
                // Identificadores
                var hoverText = '',
                    beingManeuvers = sourceCard.content.stats.effectivity.maneuvers,
                    twoHandedItemRank = [ 'primary', 'secondary' ].find( rank => beingManeuvers[ rank ].source?.content.typeset.wielding == 'two-handed' );

                // Caso exista um equipamento bimanual, retorna texto o indicando
                if( twoHandedItemRank ) return frameTexts.hands.all + beingManeuvers[ twoHandedItemRank ].source.content.designation.title;

                // Caso exista, acrescenta ao texto suspenso equipamento na mão hábil
                if( beingManeuvers.primary.source )
                  hoverText += frameTexts.hands.primary + beingManeuvers.primary.source.content.designation.title;

                // Caso exista, acrescenta ao texto suspenso equipamento na mão inábil
                if( beingManeuvers.secondary.source )
                  hoverText += '\n' + frameTexts.hands.secondary + beingManeuvers.secondary.source.content.designation.title;

                // Retorna texto suspenso
                return hoverText.trimStart();
              } )()
            },
            manualSection: 'hands-and-wielding'
          } )
        );

        // Quarta fileira
        cardDataRows[ 3 ].addChild(
          // Experiência
          cardDataSections.experience = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'experience' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getExperienceGrade( cardTypeset.experience.grade ) },
            icon: {
              super: 'experienceLevels',
              sub: cardTypeset.experience.level.toString(),
              hoverText: m.languages.keywords.getExperienceLevel( cardTypeset.experience.level.toString() )
            },
            manualSection: 'humanoids-experience'
          } ),
          // Varia seção em função da tela
          m.GameScreen.current instanceof m.MatchScreen ?
            // Pontos de destino embutidos
            cardDataSections.fatePoints = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getMisc( 'fatePoints', { plural: true } ) + ':',
              fontSize: componentsFontSize,
              text: {
                value: cardStats.fatePoints.current == Infinity ? '∞' : cardStats.fatePoints.current.toString(),
                hoverText: cardStats.fatePoints.sources.reduce( function ( accumulator, current ) {
                  // Identificadores
                  var { baseValue, currentValue } = current,
                      sourceName = m.languages.snippets.getEmbeddedFatePointsSource( current.name ),
                      snippet = m.languages.snippets.getSomethingFrom( sourceName, currentValue == Infinity ? '∞' : currentValue.toString() );

                  // Caso valor atual seja diferente do original, adiciona valor original
                  if( currentValue != baseValue ) snippet += ` (${ m.languages.snippets.base }: ${ baseValue == Infinity ? '∞' : baseValue })`;

                  // Retorna origem do ponto de destino
                  return accumulator += snippet + '\n';
                }, '' ).trimEnd()
              },
              manualSection: 'fate-points'
            } ) :
            // Disponibilidade
            cardDataSections.availability = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getTypeset( 'availability' ) + ':',
              fontSize: componentsFontSize,
              text: { value: cardTypeset.availability.toString() },
              manualSection: 'card-availability'
            } )
        );

        // Caso seção de pontos de destino exista e seu valor atual esteja abaixo do original, ajusta cor de seu texto
        if( cardDataSections.fatePoints )
          if( cardStats.fatePoints.current < cardStats.fatePoints.sources.reduce( ( accumulator, current ) => accumulator += current.baseValue, 0 ) )
            cardDataSections.fatePoints.text.tint = m.data.colors.red;

        // Posicionamento das seções posteriores das fileiras com duas seções
        setLatterSectionsPosition( cardDataRows.filter( row => row.children.length == 2 ) );

        // Monta seção de vinculados
        if( Object.values( sourceCard.maxAttachments ).some( number => number ) )
          cardDataRows.push( cardDataSections.attachments = displayCard.info.cardData.addChild( buildAttachmentsSection() ) );
      }

      /// Monta seções de dados para entes que não sejam humanoides
      function buildNonHumanoidData() {
        // Identificadores
        var [ cardTypeset, cardStats ] = [ sourceCard.content.typeset, sourceCard.content.stats ],
            [ cardAttributes, cardEffectivity ] = [ cardStats.attributes, cardStats.effectivity ],
            cardDataSections = displayCard.info.cardData.sections,
            cardDataRows = displayCard.info.cardData.rows = [];

        // Geração das primeiras fileiras
        for( let i = 1; i <= 2; i++ ) cardDataRows.push( displayCard.info.cardData.addChild( new PIXI.Container() ) );

        // Primeira fileira
        cardDataRows[ 0 ].addChild(
          // Raça
          cardDataSections.race = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'race' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getRace( cardTypeset.race ) },
            manualSection: 'being-races'
          } ),
          // Ambiente
          cardDataSections.environment = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'environment' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getEnvironment( cardTypeset.environment ),
              hoverText: frameTexts.environment
            },
            manualSection: 'being-races'
          } )
        );

        // Posicionamento da seção posterior da primeira fileira
        setLatterSectionsPosition( [ cardDataRows[ 0 ] ] );

        // Segunda fileira

        /// Montagem da seção de atributos
        cardDataRows[ 1 ].addChild(
          cardDataSections.attributes = content.buildMultiComponentSection( {
            caption: m.languages.keywords.getStat( 'attributes', { plural: true } ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: cardAttributes.current,
            iconsKey: 'attributes',
            textsCallback: ( name ) => cardAttributes.current[ name ] != Infinity ? cardAttributes.current[ name ].toString() : '∞',
            manualSection: 'being-attributes'
          } )
        );

        /// Configurações pós-montagem da seção de atributos
        for( let i = 0; i < cardDataSections.attributes.names.length; i++ ) {
          let name = cardDataSections.attributes.names[i],
              icon = cardDataSections.attributes.icons[i],
              text = cardDataSections.attributes.texts[i],
              [ currentValue, baseValue ] = [ cardAttributes.current[ name ], cardAttributes.base[ name ] ];

          // Ajuste de cor do texto
          if( currentValue > baseValue ) text.tint = m.data.colors.green

          else if( currentValue < baseValue ) text.tint = m.data.colors.red;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getAttribute( name );

          /// Do texto alvo
          text.hoverText = ( function () {
            // Para caso valor atual e base sejam iguais
            if( currentValue == baseValue ) return '';

            // Para caso valor base seja infinito
            if( baseValue == Infinity ) return `${ m.languages.snippets.base }: ∞`;

            // Para os demais casos
            return `${ m.languages.snippets.base }: ${ baseValue }`;
          } )();
        }

        // Efetividade
        if( Object.keys( cardStats.combativeness.fighting.weapons ).length )
          cardDataRows.push( cardDataSections.effectivity = displayCard.info.cardData.addChild( buildEffectivitySection() ) );

        // Resistências

        /// Montagem da seção renderizável
        cardDataRows.push( cardDataSections.resistances = buildResistancesSection() );

        /// Adição da seção em seu componente
        if( cardDataSections.resistances.children.length ) displayCard.info.cardData.addChild( cardDataSections.resistances );

        // Para caso ente possa canalizar
        if( cardStats.combativeness.channeling ) {
          // Identificadores
          let lastSection;

          // Monta componentes de canalização
          setChanneling: {
            // Identificadores
            let cardChanneling = cardStats.combativeness.channeling;

            // Montagem da seção renderizável
            lastSection = content.buildMultiComponentSection( {
              caption: frameTexts.channelingAndAttachments + ':',
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: cardChanneling.paths,
              componentsFilter: ( name ) => cardChanneling.paths[ name ].skill,
              iconsKey: 'paths',
              textsCallback: ( name ) => cardChanneling.paths[ name ].skill.toString(),
              manualSection: 'channeling-stats'
            } );

            // Configurações pós-montagem
            for( let i = 0; i < lastSection.names.length; i++ ) {
              let name = lastSection.names[ i ],
                  icon = lastSection.icons[ i ],
                  text = lastSection.texts[ i ],
                  pathPolarity = cardChanneling.paths[ name ].polarity;

              // Textos suspensos

              /// Do ícone alvo
              icon.hoverText = m.languages.keywords.getPath( name );

              /// Do texto alvo
              text.hoverText =
                //// Mana da senda atual
                `${ frameTexts.manaQuantity.path }: ${ cardChanneling.mana.current[ name ] }` +

                //// Mana da senda base
                ( cardChanneling.mana.current[ name ] == cardChanneling.mana.base[ name ] ?
                  '' : ` (${ m.languages.snippets.base }: ${ cardChanneling.mana.base[ name ] })`
                ) +

                //// Mana total atual
                `\n${ frameTexts.manaQuantity.total }: ${ cardChanneling.mana.current.total }` +

                //// Mana total base
                ( cardChanneling.mana.current.total == cardChanneling.mana.base.total ?
                  '' : ` (${ m.languages.snippets.base }: ${ cardChanneling.mana.base.total })`
                );

              // Adendo para sendas polares
              if( [ 'positive', 'negative' ].includes( pathPolarity ) ) {
                // Ajusta cor do texto segundo polaridade da senda
                text.tint = pathPolarity == 'positive' ? m.data.colors.green : m.data.colors.red;

                // Acrescenta polaridade em texto suspenso do ícone
                icon.hoverText += ` (${ m.languages.keywords.getPolarity( pathPolarity ) })`;
              }
            }
          }

          // Adiciona a componentes de canalização um componente com magias vinculadas
          setAttachments: {
            // Identificadores
            let directSpellAttachments = Object.values( sourceCard.attachments ).flat().filter( attachment => attachment instanceof m.Spell ),
                spellAttachments = { spell: directSpellAttachments.length };

            // Montagem da seção renderizável
            content.buildMultiComponentSection( {
              insertInto: { section: lastSection, position: lastSection.containers.length },
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: spellAttachments,
              iconsKey: 'primitiveTypes',
              textsCallback: ( name ) => spellAttachments[ name ].toString() + '/' + sourceCard.maxAttachments[ name ].toString(),
              manualSection: 'attachness'
            } );

            // Captura do índice das magias
            let spellIndex = lastSection.names.indexOf( 'spell' );

            // Textos suspensos

            /// Do ícone
            lastSection.icons[ spellIndex ].hoverText = m.languages.keywords.getPrimitiveType( 'spell', { plural: true } );

            /// Do texto
            lastSection.texts[ spellIndex ].hoverText = directSpellAttachments.reduce(
              ( accumulator, current ) => accumulator += frameTexts.getCardAttachment( current ), ''
            ).trimEnd();
          }

          // Inseri e registra última seção do componente
          cardDataRows.push( cardDataSections.channelingAndAttachments = displayCard.info.cardData.addChild( lastSection ) );
        }

        // Do contrário
        else {
          // Vinculados
          if( Object.values( sourceCard.maxAttachments ).some( number => number ) )
            cardDataRows.push( cardDataSections.attachments = displayCard.info.cardData.addChild( buildAttachmentsSection() ) );
        }
      }

      /// Monta seções de dados para equipamentos
      function buildItemData() {
        // Identificadores
        var [ cardTypeset, cardStats ] = [ sourceCard.content.typeset, sourceCard.content.stats ],
            cardDataSections = displayCard.info.cardData.sections,
            cardDataRows = displayCard.info.cardData.rows = [];

        // Geração das primeiras fileiras
        for( let i = 1; i <= 4; i++ ) cardDataRows.push( displayCard.info.cardData.addChild( new PIXI.Container() ) );

        // Primeira fileira
        cardDataRows[ 0 ].addChild(
          // Equipagem
          cardDataSections.equipage = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'equipage' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getEquipage( cardTypeset.equipage ) },
            manualSection: 'item-equipages'
          } ),
          // Alvo
          cardDataSections.target = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getMisc( 'target' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getAttachability( cardTypeset.attachability.name ),
              hoverText: frameTexts.target
            },
            manualSection: 'agency'
          } )
        );

        // Segunda fileira
        cardDataRows[ 1 ].addChild(
          // Tamanho
          cardDataSections.size = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'size' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getSize( cardTypeset.size ) },
            manualSection: 'size'
          } ),
          // Peso
          cardDataSections.weight = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'weight' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getWeight( cardTypeset.weight ) },
            manualSection: 'weight-and-weight-count'
          } )
        );

        // Terceira fileira
        thirdRow: {
          // Definição das seções

          /// Primeira
          let firstSection = sourceCard instanceof m.Weapon ?
            // Atuação
            cardDataSections.role = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getTypeset( 'role' ) + ':',
              fontSize: componentsFontSize,
              text: { value: m.languages.keywords.getWeapon( cardTypeset.role.name ) },
              icon: {
                super: 'roleTypes',
                sub: cardTypeset.role.type,
                hoverText: m.languages.keywords.getRoleType( cardTypeset.role.type )
              },
              manualSection: 'fighting-stats'
            } ) : sourceCard instanceof m.Garment ?
            // Cobertura
            cardDataSections.coverage = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getStat( 'coverage' ) + ':',
              fontSize: componentsFontSize,
              text: { value: cardStats.effectivity.current.coverage.toString() },
              manualSection: 'garment-coverage'
            } ) :
            // Uso
            cardDataSections.usage = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getTypeset( 'usage' ) + ':',
              fontSize: componentsFontSize,
              text: { value: m.languages.keywords.getUsage( cardTypeset.usage ) },
              manualSection: 'usage'
            } );

          /// Segunda
          let secondSection = sourceCard instanceof m.Garment ?
            // Condutividade
            cardDataSections.conductivity = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getStat( 'conductivity' ) + ':',
              fontSize: componentsFontSize,
              text: { value: cardStats.effectivity.current.conductivity.toString() },
              manualSection: 'conductivity'
            } ) :
            // Empunhadura
            cardDataSections.wielding = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getTypeset( 'wielding' ) + ':',
              fontSize: componentsFontSize,
              text: { value: m.languages.keywords.getWielding( cardTypeset.wielding ) },
              manualSection: 'hands-and-wielding'
            } );

          // Inserção dos componentes da fileira
          cardDataRows[ 2 ].addChild( firstSection, secondSection );
        }

        // Quarta fileira
        cardDataRows[ 3 ].addChild(
          // Categoria
          cardDataSections.grade = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'grade' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getItemGrade( cardTypeset.grade ) },
            manualSection: 'item-grades'
          } ),
          // Disponibilidade
          cardDataSections.availability = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'availability' ) + ':',
            fontSize: componentsFontSize,
            text: { value: cardTypeset.availability.toString() },
            manualSection: 'card-availability'
          } )
        );

        // Posicionamento das seções posteriores das fileiras com duas seções
        setLatterSectionsPosition( cardDataRows.filter( row => row.children.length == 2 ) );

        // Monta fileira final

        /// Para armas e apetrechos com mana, monta seção de vinculados
        if( sourceCard instanceof m.Weapon || sourceCard instanceof m.Implement && sourceCard.maxAttachments.mana )
          cardDataRows.push( cardDataSections.attachments = displayCard.info.cardData.addChild( buildAttachmentsSection() ) )

        /// Para trajes, monta seção de resistências
        else if( sourceCard instanceof m.Garment ) {
          // Identificadores
          let cardResistances = cardStats.effectivity.current.resistances;

          // Montagem da seção renderizável
          cardDataSections.resistances = Object.values( cardResistances ).some( value => value ) ?
            content.buildMultiComponentSection( {
              caption: m.languages.keywords.getStat( 'resistances', { plural: true } ) + ':',
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: cardResistances,
              componentsFilter: ( name ) => cardResistances[ name ],
              iconsKey: 'damages',
              textsCallback: ( name ) => cardResistances[ name ] != Infinity ? cardResistances[ name ].toString() : '∞',
              manualSection: 'resistance-types'
            } ) : new PIXI.Container();

          // Para caso seção tenha conteúdo
          if( cardDataSections.resistances.children.length ) {
            // Adição da seção em seu componente
            cardDataRows.push( displayCard.info.cardData.addChild( cardDataSections.resistances ) );

            // Configurações pós-montagem
            for( let i = 0; i < cardDataSections.resistances.names.length; i++ ) {
              let name = cardDataSections.resistances.names[i],
                  icon = cardDataSections.resistances.icons[i],
                  text = cardDataSections.resistances.texts[i],
                  [ currentValue, baseValue ] = [ cardResistances[ name ], cardStats.effectivity.base.resistances[ name ] ];

              // Ajuste de cor do texto
              if( currentValue > baseValue ) text.tint = m.data.colors.green

              else if( currentValue < baseValue ) text.tint = m.data.colors.red;

              // Textos suspensos

              /// Do ícone alvo
              icon.hoverText = m.languages.snippets.getResistanceType( name );

              /// Do texto alvo
              text.hoverText = ( function () {
                // Para caso valor atual e base sejam iguais
                if( currentValue == baseValue ) return '';

                // Para caso valor base seja infinito
                if( baseValue == Infinity ) return `${ m.languages.snippets.base }: ∞`;

                // Para os demais casos
                return `${ m.languages.snippets.base }: ${ baseValue }`;
              } )();
            }
          }
        }
      }

      /// Monta seções de dados para magias
      function buildSpellData() {
        // Identificadores
        var [ cardTypeset, cardStats ] = [ sourceCard.content.typeset, sourceCard.content.stats ],
            cardDataSections = displayCard.info.cardData.sections,
            cardDataRows = displayCard.info.cardData.rows = [];

        // Geração das fileiras
        for( let i = 1; i <= 4; i++ ) cardDataRows.push( displayCard.info.cardData.addChild( new PIXI.Container() ) );

        // Primeira fileira
        cardDataRows[ 0 ].addChild(
          // Senda
          cardDataSections.path = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'path' ) + ':',
            fontSize: componentsFontSize,
            text: { value: m.languages.keywords.getPath( cardTypeset.path ) },
            manualSection: 'spell-paths'
          } ),
          // Alvo
          cardDataSections.target = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getMisc( 'target' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getAttachability( cardTypeset.attachability?.name ),
              hoverText: frameTexts.target
            },
            manualSection: 'agency'
          } )
        );

        // Segunda fileira
        cardDataRows[ 1 ].addChild(
          // Polaridade
          cardDataSections.polarity = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'polarity' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getPolarity( cardTypeset.polarity, { feminine: true } ),
              hoverText: frameTexts.oppositeSpell
            },
            manualSection: 'spell-polarity'
          } ),
          // Alcance
          cardDataSections.range = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getStat( 'range' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: cardStats.current.range,
              hoverText: cardStats.current.range != cardStats.base.range ? `${ m.languages.snippets.base }: ${ cardStats.base.range }` : ''
            },
            manualSection: 'attack-and-spell-range'
          } )
        );

        // Se aplicável, ajusta cor da seção de alcance a partir da diferença entre seu valor atual e o base
        if( cardStats.current.range != cardStats.base.range ) {
          // Identificadores
          let [ currentRange, baseRange ] = [ cardStats.current.range, cardStats.base.range ],
              [ currentScope, baseScope ] = [ currentRange.replace( /\d/g, '' ), baseRange.replace( /\d/g, '' ) ],
              [ currentDigit, baseDigit ] = [ currentRange.replace( /\D/g, '' ), baseRange.replace( /\D/g, '' ) ];

          // Ajuste de cor
          cardDataSections.range.text.tint = currentScope == 'G' && baseScope == 'R' || currentDigit > baseDigit ? m.data.colors.green : m.data.colors.red;
        }

        // Terceira fileira
        cardDataRows[ 2 ].addChild(
          // Custo de fluxo
          cardDataSections.flowCost = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getStat( 'flowCost' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: ( function () {
                // Identificadores
                var [ textsArray, currentFlowCost ] = [ [], cardStats.current.flowCost ];

                // Exibição do custo de canalização
                textsArray.push( { channelCost: currentFlowCost.toChannel.toString() } );

                // Se não houver custo de sustentação, retorna arranjo de textos
                if( !currentFlowCost.toSustain ) return textsArray;

                // Exibição do conteúdo antes do custo de sustentação
                textsArray.push( { beforeSustainCost: ' + ' } );

                // Exibição do custo de sustentação
                textsArray.push( { sustainCost: currentFlowCost.toSustain.toString() } );

                // Retorna arranjo de textos
                return textsArray;
              } )(),
              hoverText: ( function () {
                // Identificadores
                var [ text, currentFlowCost, baseFlowCost ] = [ '', cardStats.current.flowCost, cardStats.base.flowCost ];

                // Texto a ser incondicionalmente exibido
                text += `${ frameTexts.flowCosts.channeling }: ${ currentFlowCost.toChannel }`;

                // Para caso valor atual de custo para canalização seja diferente do original
                if( currentFlowCost.toChannel != baseFlowCost.toChannel ) text += ` (${ m.languages.snippets.base }: ${ baseFlowCost.toChannel })`;

                // Para caso haja custo para sustentação
                if( baseFlowCost.toSustain ) {
                  // Acrescenta ao texto custo atual de sustentação
                  text += `\n${ frameTexts.flowCosts.sustaining }: ${ currentFlowCost.toSustain }`;

                  // Para caso valor atual de custo para sustentação seja diferente do original
                  if( currentFlowCost.toSustain != baseFlowCost.toSustain ) text += ` (${ m.languages.snippets.base }: ${ baseFlowCost.toSustain })`;
                }

                // Retorna texto
                return text;
              } )()
            },
            manualSection: 'action-flow-cost'
          } ),
          // Custo de mana
          cardDataSections.manaCost = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getStat( 'manaCost' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: ( function () {
                // Identificadores
                var [ textsArray, currentManaCost ] = [ [], cardStats.current.manaCost ];

                // Exibição do custo mínimo de canalização
                textsArray.push( { channelMinCost: currentManaCost.toChannel.min.toString() } );

                // Para caso custo máximo seja distinto do mínimo
                if( currentManaCost.toChannel.min != currentManaCost.toChannel.max ) {
                  // Exibição do separador de custo
                  textsArray.push( { channelCostSeparator: '–' } );

                  // Exibição do custo máximo de canalização
                  textsArray.push( { channelMaxCost: currentManaCost.toChannel.max.toString() } );
                }

                // Se não houver custo de sustentação, retorna arranjo de textos
                if( !currentManaCost.toSustain ) return textsArray;

                // Exibição do conteúdo antes do custo de sustentação
                textsArray.push( { beforeSustainCost: ' + ' } );

                // Exibição do custo de sustentação
                textsArray.push( { sustainCost: currentManaCost.toSustain.toString() } );

                // Se custo máximo de uso for vinte, retorna arranjo de textos
                if( currentManaCost.maxUsage == 20 ) return textsArray;

                // Exibição do conteúdo antes do custo máximo de uso
                textsArray.push( { beforeMaxUsage: ' (' } );

                // Exibição do custo máximo de uso
                textsArray.push( { maxUsage: currentManaCost.maxUsage.toString() } );

                // Exibição do conteúdo depois do custo máximo de uso
                textsArray.push( { afterMaxUsage: ')' } );

                // Retorna arranjo de textos
                return textsArray;
              } )(),
              hoverText: ( function () {
                // Identificadores
                var [ text, currentManaCost, baseManaCost ] = [ '', cardStats.current.manaCost, cardStats.base.manaCost ];

                // Exibição do custo de mana mínimo para canalização
                text += `${ frameTexts.manaCosts.minChanneling }: ${ currentManaCost.toChannel.min }`;

                // Acrescenta custo de mana mínimo original, se diferente
                if( currentManaCost.toChannel.min != baseManaCost.toChannel.min )
                  text += ` (${ m.languages.snippets.base }: ${ baseManaCost.toChannel.min })`;

                // Exibição do custo de mana máximo para canalização
                text += `\n${ frameTexts.manaCosts.maxChanneling }: ${ currentManaCost.toChannel.max }`;

                // Acrescenta custo de mana máximo original, se diferente
                if( currentManaCost.toChannel.max != baseManaCost.toChannel.max )
                  text += ` (${ m.languages.snippets.base }: ${ baseManaCost.toChannel.max })`;

                // Se não houver custo de sustentação, retorna texto
                if( !baseManaCost.toSustain ) return text;

                // Exibição do custo de sustentação
                text += `\n${ frameTexts.manaCosts.sustainingCost }: ${ currentManaCost.toSustain }`;

                // Acrescenta custo de sustentação original, se diferente
                if( currentManaCost.toSustain != baseManaCost.toSustain )
                  text += ` (${ m.languages.snippets.base }: ${ baseManaCost.toSustain })`;

                // Exibição do custo máximo de uso
                text += `\n${ frameTexts.manaCosts.maxUsageCost }: ${ currentManaCost.maxUsage }`;

                // Acrescenta custo máximo de uso original, se diferente
                if( currentManaCost.maxUsage != baseManaCost.maxUsage )
                  text += ` (${ m.languages.snippets.base }: ${ baseManaCost.maxUsage })`;

                // Retorna texto
                return text;
              } )()
            },
            manualSection: 'channeling-stats'
          } )
        );

        // Ajuste de cor do texto das seções
        changeTextTint: {
          // Identificadores
          let [ flowCostSections, manaCostSections ] = [ cardDataSections.flowCost.text.sections, cardDataSections.manaCost.text.sections ],
              flowObject = {
                'channelCost': [ 'toChannel' ], 'sustainCost': [ 'toSustain' ]
              },
              manaObject = {
                'channelMinCost': [ 'toChannel', 'min' ], 'channelMaxCost': [ 'toChannel', 'max' ], 'sustainCost': [ 'toSustain' ], 'maxUsage': [ 'maxUsage' ]
              };

          // Atualização de cor para seções do custo de fluxo
          for( let sectionName in flowObject ) {
            // Filtra seções que não existem
            if( !flowCostSections[ sectionName ] ) continue;

            // Captura valor atual e original da estatística alvo
            let currentStat = flowObject[ sectionName ].reduce( ( accumulator, current ) => accumulator[ current ], cardStats.current.flowCost ),
                baseStat = flowObject[ sectionName ].reduce( ( accumulator, current ) => accumulator[ current ], cardStats.base.flowCost );

            // Filtra estatísticas iguais
            if( currentStat == baseStat ) continue;

            // Aplica mudança de cor
            flowCostSections[ sectionName ].tint = currentStat < baseStat ? m.data.colors.green : m.data.colors.red;
          }

          // Atualização de cor para seções do custo de mana
          for( let sectionName in manaObject ) {
            // Filtra seções que não existem
            if( !manaCostSections[ sectionName ] ) continue;

            // Captura valor atual e original da estatística alvo
            let currentStat = manaObject[ sectionName ].reduce( ( accumulator, current ) => accumulator[ current ], cardStats.current.manaCost ),
                baseStat = manaObject[ sectionName ].reduce( ( accumulator, current ) => accumulator[ current ], cardStats.base.manaCost );

            // Filtra estatísticas iguais
            if( currentStat == baseStat ) continue;

            // Aplica mudança de cor segundo seção
            manaCostSections[ sectionName ].tint =
              [ 'channelMinCost', 'sustainCost' ].includes( sectionName ) ?
                ( currentStat < baseStat ? m.data.colors.green : m.data.colors.red ) :
                ( currentStat < baseStat ? m.data.colors.red : m.data.colors.green );
          }
        }

        // Quarta fileira
        cardDataRows[ 3 ].addChild(
          // Duração
          cardDataSections.duration = content.buildSingleComponentSection( {
            caption: m.languages.keywords.getTypeset( 'duration' ) + ':',
            fontSize: componentsFontSize,
            text: {
              value: m.languages.keywords.getDuration( cardTypeset.duration ),
              hoverText: cardTypeset.duration == 'sustained' ? m.languages.keywords.getDuration( 'sustained-' + cardTypeset.durationSubtype ) : ''
            },
            manualSection: 'spell-duration'
          } ),
          // Varia seção em função da tela
          m.GameScreen.current instanceof m.MatchScreen ?
            // Potência
            cardDataSections.potency = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getStat( 'potency' ) + ':',
              fontSize: componentsFontSize,
              text: {
                value: cardStats.current.potency.toString(),
                hoverText: ( function () {
                  // Identificadores
                  var hoverText = '',
                      [ currentPotency, basePotency ] = [ cardStats.current.potency, cardStats.base.potency ];

                  // Caso magia seja persistente
                  if( cardTypeset.duration == 'persistent' ) {
                    // Caso magia tenha um segmento de uso, exibe-o
                    if( sourceCard.usageSegment )
                      hoverText +=
                        `${ m.languages.keywords.getMisc( 'usageSegment' ) }: ` + m.languages.snippets.getSomethingOf(
                          sourceCard.usageSegment.parent.displayName, sourceCard.usageSegment.displayName
                        ) + '\n';

                    // Exibição do custo de persistência
                    hoverText += `${ m.languages.keywords.getStat( 'persistenceCost' ) }: ` + (
                      sourceCard.isDynamicPersistence && !cardStats.current.manaCost.toPersist ?
                        m.languages.snippets.dynamic : cardStats.current.manaCost.toPersist.toString()
                    );
                  }

                  // Encerra função caso potência atual seja igual à base
                  if( currentPotency == basePotency ) return hoverText;

                  // Adiciona a texto suspenso potência base
                  hoverText = `${ m.languages.snippets.base }: ${ basePotency }\n` + hoverText;

                  // Retorna texto suspenso
                  return hoverText;
                } )().trimEnd()
              },
              manualSection: 'spell-potency'
            } ) :
            // Disponibilidade
            cardDataSections.availability = content.buildSingleComponentSection( {
              caption: m.languages.keywords.getTypeset( 'availability' ) + ':',
              fontSize: componentsFontSize,
              text: { value: cardTypeset.availability.toString() },
              manualSection: 'card-availability'
            } )
        );

        // Se aplicável, ajusta cor da seção de potência a partir da diferença entre seu valor atual e o base
        if( cardDataSections.potency && cardStats.current.potency != cardStats.base.potency )
          cardDataSections.potency.text.tint = cardStats.current.potency > cardStats.base.potency ? m.data.colors.green : m.data.colors.red;

        // Posicionamento das seções posteriores das fileiras com duas seções
        setLatterSectionsPosition( cardDataRows.filter( row => row.children.length == 2 ) );

        // Monta seção de vinculados
        cardDataRows.push( cardDataSections.attachments = displayCard.info.cardData.addChild( buildAttachmentsSection() ) );
      }

      /// Monta seções de estatísticas para humanoides
      function buildHumanoidStats() {
        // Identificadores
        var cardStats = sourceCard.content.stats,
            [ cardAttributes, cardFighting, cardChanneling ] = [ cardStats.attributes, cardStats.combativeness.fighting, cardStats.combativeness.channeling ],
            cardEffectivity = cardStats.effectivity,
            cardStatsSections = displayCard.info.cardStats.sections;

        // Atributos

        /// Montagem da seção renderizável
        cardStatsSections.attributes = displayCard.info.cardStats.addChild( content.buildMultiComponentSection( {
          caption: m.languages.keywords.getStat( 'attributes', { plural: true } ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: cardAttributes.current,
          iconsKey: 'attributes',
          textsCallback: ( name ) => cardAttributes.current[ name ] != Infinity ? cardAttributes.current[ name ].toString() : '∞',
          manualSection: 'being-attributes'
        } ) );

        /// Configurações pós-montagem
        for( let i = 0; i < cardStatsSections.attributes.names.length; i++ ) {
          let name = cardStatsSections.attributes.names[i],
              icon = cardStatsSections.attributes.icons[i],
              text = cardStatsSections.attributes.texts[i],
              [ currentValue, baseValue ] = [ cardAttributes.current[ name ], cardAttributes.base[ name ] ];

          // Ajuste de cor do texto
          if( currentValue > baseValue ) text.tint = m.data.colors.green

          else if( currentValue < baseValue ) text.tint = m.data.colors.red;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getAttribute( name );

          /// Do texto alvo
          text.hoverText = ( function () {
            // Para caso valor atual e base sejam iguais
            if( currentValue == baseValue ) return '';

            // Para caso valor base seja infinito
            if( baseValue == Infinity ) return `${ m.languages.snippets.base }: ∞`;

            // Para os demais casos
            return `${ m.languages.snippets.base }: ${ baseValue }`;
          } )();
        }

        // Luta
        if( cardFighting ) {
          // Montagem da seção renderizável
          cardStatsSections.fighting = displayCard.info.cardStats.addChild( content.buildMultiComponentSection( {
            caption: m.languages.keywords.getStat( 'fighting' ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: cardFighting.weapons,
            iconsKey: 'weapons',
            textsCallback: ( name ) => cardFighting.weapons[ name ].skill.toString(),
            manualSection: 'fighting-stats'
          } ) );

          // Configurações pós-montagem
          for( let i = 0; i < cardStatsSections.fighting.names.length; i++ ) {
            let name = cardStatsSections.fighting.names[i],
                icon = cardStatsSections.fighting.icons[i],
                text = cardStatsSections.fighting.texts[i],
                baseStat = cardFighting.weapons[ name ].raceSkill;

            // Ajuste de cor do texto
            if( text.text > baseStat ) text.tint = m.data.colors.green;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = m.languages.keywords.getWeapon( name, { plural: true } );

            /// Do texto alvo
            text.hoverText = text.text != baseStat ? `${ m.languages.snippets.base }: ${ baseStat }` : '';
          }
        }

        // Canalização
        if( cardChanneling ) {
          // Montagem da seção renderizável
          cardStatsSections.channeling = displayCard.info.cardStats.addChild( content.buildMultiComponentSection( {
            caption: m.languages.keywords.getStat( 'channeling' ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: cardChanneling.paths,
            componentsFilter: ( name ) => cardChanneling.paths[ name ].skill,
            iconsKey: 'paths',
            textsCallback: ( name ) => cardChanneling.paths[ name ].skill.toString(),
            manualSection: 'channeling-stats'
          } ) );

          // Configurações pós-montagem
          for( let i = 0; i < cardStatsSections.channeling.names.length; i++ ) {
            let name = cardStatsSections.channeling.names[i],
                icon = cardStatsSections.channeling.icons[i],
                text = cardStatsSections.channeling.texts[i],
                pathPolarity = cardChanneling.paths[ name ].polarity;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = m.languages.keywords.getPath( name );

            /// Do texto alvo
            text.hoverText =
              //// Mana da senda atual
              `${ frameTexts.manaQuantity.path }: ${ cardChanneling.mana.current[ name ] }` +

              //// Mana da senda base
              ( cardChanneling.mana.current[ name ] == cardChanneling.mana.base[ name ] ?
                '' : ` (${ m.languages.snippets.base }: ${ cardChanneling.mana.base[ name ] })`
              ) +

              //// Mana total atual
              `\n${ frameTexts.manaQuantity.total }: ${ cardChanneling.mana.current.total }` +

              //// Mana total base
              ( cardChanneling.mana.current.total == cardChanneling.mana.base.total ?
                '' : ` (${ m.languages.snippets.base }: ${ cardChanneling.mana.base.total })`
              );

            // Adendo para sendas polares
            if( [ 'positive', 'negative' ].includes( pathPolarity ) ) {
              // Ajusta cor do texto segundo polaridade da senda
              text.tint = pathPolarity == 'positive' ? m.data.colors.green : m.data.colors.red;

              // Acrescenta polaridade em texto suspenso do ícone
              icon.hoverText += ` (${ m.languages.keywords.getPolarity( pathPolarity ) })`;
            }
          }
        }

        // Efetividade

        /// Montagem da seção renderizável
        cardStatsSections.effectivity = displayCard.info.cardStats.addChild( buildEffectivitySection() );

        // Resistências

        /// Montagem da seção renderizável
        cardStatsSections.resistances = buildResistancesSection();

        /// Adição da seção em seu componente, se com conteúdo
        if( cardStatsSections.resistances.children.length ) displayCard.info.cardStats.addChild( cardStatsSections.resistances );
      }

      /// Monta seções de estatísticas para armas
      function buildWeaponStats() {
        // Identificadores
        var cardEffectivity = sourceCard.content.stats.effectivity,
            [ cardAttacks, cardDefenses ] = [ cardEffectivity.current.attacks, cardEffectivity.current.defenses ],
            attacksArray = displayCard.info.cardStats.sections.attacks = [],
            defensesArray = displayCard.info.cardStats.sections.defenses = [];

        // Ataques
        cardAttacks.forEach( function ( attack, index ) {
          // Montagem de estatísticas gerais da seção renderizável
          attacksArray.push( displayCard.info.cardStats.addChild( content.buildMultiComponentSection( {
            caption: m.languages.keywords.getManeuver( attack.name ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: attack,
            componentsFilter: ( name ) => [ 'modifier', 'penetration', 'recovery', 'range' ].includes( name ),
            iconsKey: 'stats',
            textsCallback: ( name ) => attack[ name ].toString()
          } ) ) );

          // Captura seção de ataque atual
          let attackSection = attacksArray[ index ];

          // Define seções do manual vinculadas a cada seção do componente
          setManualSections: {
            // Identificadores
            let sectionObject = {
                  caption: attack.name,
                  modifier: 'attacks-and-defenses',
                  penetration: 'attack-penetration',
                  recovery: 'maneuver-recovery',
                  range: 'attack-and-spell-range'
                },
                componentObject = {
                  caption: [ attackSection.caption ]
                };

            // Itera por nome de componentes já adicionados
            for( let componentName of [ 'modifier', 'penetration', 'recovery', 'range' ] ) {
              // Identificadores
              let nameIndex = attackSection.names.indexOf( componentName );

              // Adiciona ao objeto de componentes componentes vinculados ao nome alvo
              componentObject[ componentName ] = [ attackSection.texts[ nameIndex ], attackSection.icons[ nameIndex ] ];
            }

            // Itera por nome de componentes
            for( let componentName in componentObject ) {
              // Adiciona evento para abrir seção pertinente do manual quando se clicar em um componente
              for( let component of componentObject[ componentName ] )
                component.addListener( 'click', m.app.openManualSection.bind( m.app, sectionObject[ componentName ] ) );
            }
          }

          // Montagem das estatísticas de dano da seção renderizável
          content.buildMultiComponentSection( {
            insertInto: { section: attackSection, position: 1 },
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: attack.damage,
            iconsKey: 'damages',
            textsCallback: ( name ) => attack.damage[ name ].toString(),
            manualSection: 'damage-types'
          } );

          // Configurações pós-montagem
          for( let i = 0; i < attackSection.names.length; i++ ) {
            let name = attackSection.names[i],
                icon = attackSection.icons[i],
                text = attackSection.texts[i],
                baseStat = name in attack.damage ? cardEffectivity.base.attacks[ index ].damage[ name ] : cardEffectivity.base.attacks[ index ][ name ],
                iconHoverText = name in attack.damage ? m.languages.snippets.getDamageType( name ) : m.languages.keywords.getStat( name );

            // Ajuste de cor do texto
            if( text.text > baseStat ) text.tint = m.data.colors.green

            else if( text.text < baseStat ) text.tint = m.data.colors.red;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = iconHoverText;

            /// Do texto alvo
            text.hoverText = text.text != baseStat ? `${ m.languages.snippets.base }: ${ baseStat }` : '';
          }
        } );

        // Defesas
        cardDefenses.forEach( function ( defense, index ) {
          // Montagem da seção renderizável
          defensesArray.push( displayCard.info.cardStats.addChild( content.buildMultiComponentSection( {
            caption: m.languages.keywords.getManeuver( defense.name ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: defense,
            componentsFilter: ( name ) => [ 'modifier', 'recovery' ].includes( name ),
            iconsKey: 'stats',
            textsCallback: ( name ) => defense[ name ].toString()
          } ) ) );

          // Captura seção de defesa atual
          let defenseSection = defensesArray[ index ];

          // Define seções do manual vinculadas a cada seção do componente
          setManualSections: {
            // Identificadores
            let sectionObject = {
                  caption: defense.name,
                  modifier: 'attacks-and-defenses',
                  recovery: 'maneuver-recovery'
                },
                componentObject = {
                  caption: [ defenseSection.caption ]
                };

            // Itera por nome de componentes já adicionados
            for( let componentName of [ 'modifier', 'recovery' ] ) {
              // Identificadores
              let nameIndex = defenseSection.names.indexOf( componentName );

              // Adiciona ao objeto de componentes componentes vinculados ao nome alvo
              componentObject[ componentName ] = [ defenseSection.texts[ nameIndex ], defenseSection.icons[ nameIndex ] ];
            }

            // Itera por nome de componentes
            for( let componentName in componentObject ) {
              // Adiciona evento para abrir seção pertinente do manual quando se clicar em um componente
              for( let component of componentObject[ componentName ] )
                component.addListener( 'click', m.app.openManualSection.bind( m.app, sectionObject[ componentName ] ) );
            }
          }

          // Configurações pós-montagem
          for( let i = 0; i < defenseSection.names.length; i++ ) {
            let name = defenseSection.names[i],
                icon = defenseSection.icons[i],
                text = defenseSection.texts[i],
                baseStat = cardEffectivity.base.defenses[ index ][ name ];

            // Ajuste de cor do texto
            if( text.text > baseStat ) text.tint = m.data.colors.green

            else if( text.text < baseStat ) text.tint = m.data.colors.red;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = m.languages.keywords.getStat( name );

            /// Do texto alvo
            text.hoverText = text.text != baseStat ? `${ m.languages.snippets.base }: ${ baseStat }` : '';
          }
        } );
      }

      /// Monta seções do comando
      function buildCommandSections( commander ) {
        // Identificadores
        var scopes = commander.content.stats.command.scopes,
            deckCards = commander.deck?.cards,
            creatures = deckCards?.creatures.map( creature => creature.sourceBeing ?? creature ).filter( creature => !creature.content.stats.command ) ?? [],
            items = deckCards?.items ?? [],
            commandSections = displayCard.info.cardCommand.sections;

        // Seção dos tipos primitivos

        /// Montagem
        commandSections.primitiveType = displayCard.info.cardCommand.addChild( content.buildMultiComponentSection( {
          caption: m.languages.keywords.getMisc( 'primitiveType' ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: { creature: null, item: null, spell: null },
          componentsFilter: ( name ) => scopes[ name ].total.max > 0,
          iconsKey: 'primitiveTypes',
          textsCallback: commander.deck ? ( name ) => commander.deck.cards[ name + 's' ].length.toString() : () => '0',
          manualSection: 'commander-command'
        } ) );

        /// Configurações pós-montagem
        for( let i = 0; i < commandSections.primitiveType.names.length; i++ ) {
          let name = commandSections.primitiveType.names[i],
              icon = commandSections.primitiveType.icons[i],
              text = commandSections.primitiveType.texts[i];

          // Ajuste de cor do texto
          if( text.text < scopes[ name ].total.min || text.text > scopes[ name ].total.max ) text.tint = m.data.colors.red

          else if( text.text == scopes[ name ].total.max ) text.tint = m.data.colors.green;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getPrimitiveType( name, { plural: true } );

          /// Do texto alvo
          text.hoverText = getTextsHoverText( scopes[ name ].total );
        }

        // Seção da raça

        /// Montagem
        commandSections.race = displayCard.info.cardCommand.addChild( content.buildMultiComponentSection( {
          caption: m.languages.keywords.getTypeset( 'race' ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: scopes.creature.race,
          componentsFilter: ( name ) => scopes.creature.race[ name ].max > 0,
          iconsKey: 'races',
          textsCallback: ( name ) => creatures.filter( creature => creature.content.typeset.race == name ).length.toString(),
          manualSection: 'commander-command'
        } ) );

        /// Configurações pós-montagem
        for( let i = 0; i < commandSections.race.names.length; i++ ) {
          let name = commandSections.race.names[i],
              icon = commandSections.race.icons[i],
              text = commandSections.race.texts[i];

          // Ajuste de cor do texto
          if( text.text < scopes.creature.race[ name ].min || text.text > scopes.creature.race[ name ].max ) text.tint = m.data.colors.red

          else if( text.text == scopes.creature.race[ name ].max ) text.tint = m.data.colors.green;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getRace( name, { plural: true } );

          /// Do texto alvo
          text.hoverText = getTextsHoverText( scopes.creature.race[ name ] );
        }

        // Seção do tipo de atuação

        /// Montagem
        commandSections.roleType = displayCard.info.cardCommand.addChild( content.buildMultiComponentSection( {
          caption: m.languages.keywords.getTypeset( 'roleType' ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: scopes.creature.roleType,
          componentsFilter: ( name ) => scopes.creature.roleType[ name ].max > 0,
          iconsKey: 'roleTypes',
          textsCallback: ( name ) => creatures.filter( creature => creature.content.typeset.role.type == name ).length.toString(),
          manualSection: 'commander-command'
        } ) );

        /// Configurações pós-montagem
        for( let i = 0; i < commandSections.roleType.names.length; i++ ) {
          let name = commandSections.roleType.names[i],
              icon = commandSections.roleType.icons[i],
              text = commandSections.roleType.texts[i];

          // Ajuste de cor do texto
          if( text.text < scopes.creature.roleType[ name ].min || text.text > scopes.creature.roleType[ name ].max ) text.tint = m.data.colors.red

          else if( text.text == scopes.creature.roleType[ name ].max ) text.tint = m.data.colors.green;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getRoleType( name, { person: true, plural: true } );

          /// Do texto alvo
          text.hoverText = getTextsHoverText( scopes.creature.roleType[ name ] );
        }

        // Seção do nível de experiência

        /// Montagem
        commandSections.experienceLevel = displayCard.info.cardCommand.addChild( content.buildMultiComponentSection( {
          caption: m.languages.keywords.getTypeset( 'experienceLevel' ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: scopes.creature.experienceLevel,
          componentsFilter: ( name ) => scopes.creature.experienceLevel[ name ].max > 0,
          iconsKey: 'experienceLevels',
          textsCallback: ( name ) => creatures.filter( creature => creature.content.typeset.experience.level == name ).length.toString(),
          manualSection: 'commander-command'
        } ) );

        /// Definição dos textos suspensos
        for( let i = 0; i < commandSections.experienceLevel.names.length; i++ ) {
          let name = commandSections.experienceLevel.names[i],
              icon = commandSections.experienceLevel.icons[i],
              text = commandSections.experienceLevel.texts[i];

          // Ajuste de cor do texto
          if( text.text < scopes.creature.experienceLevel[ name ].min || text.text > scopes.creature.experienceLevel[ name ].max ) text.tint = m.data.colors.red

          else if( text.text == scopes.creature.experienceLevel[ name ].max ) text.tint = m.data.colors.green;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getExperienceLevel( name );

          /// Do texto alvo
          text.hoverText = getTextsHoverText( scopes.creature.experienceLevel[ name ] );
        }

        // Seção da categoria de equipamentos
        if( Object.values( scopes.item.grade ).some( number => number.max > 0 ) ) {
          // Montagem
          commandSections.grade = displayCard.info.cardCommand.addChild( content.buildMultiComponentSection( {
            caption: m.languages.keywords.getTypeset( 'grade' ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: scopes.item.grade,
            componentsFilter: ( name ) => scopes.item.grade[ name ].max > 0,
            iconsKey: 'itemsGrade',
            textsCallback: ( name ) => items.filter( item => item.content.typeset.grade == name ).length.toString(),
            manualSection: 'commander-command'
          } ) );

          // Definição dos textos suspensos
          for( let i = 0; i < commandSections.grade.names.length; i++ ) {
            let name = commandSections.grade.names[i],
                icon = commandSections.grade.icons[i],
                text = commandSections.grade.texts[i];

            // Ajuste de cor do texto
            if( text.text < scopes.item.grade[ name ].min || text.text > scopes.item.grade[ name ].max ) text.tint = m.data.colors.red

            else if( text.text == scopes.item.grade[ name ].max ) text.tint = m.data.colors.green;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = m.languages.keywords.getItemGrade( name );

            /// Do texto alvo
            text.hoverText = getTextsHoverText( scopes.item.grade[ name ] );
          }
        }

        // Retorna texto suspenso de texto do escopo passado
        function getTextsHoverText( scope ) {
          return `${ m.languages.snippets.min }: ${ scope.min.toString() }\n` +
                 `${ m.languages.snippets.max }: ${ scope.max == Infinity ? '∞' : scope.max.toString() }` +
                 ( scope.constraint.description ? '\n' + scope.constraint.description : '' );
        }
      }

      /// Monta seção sobre ilustrador da carta a ser exibida
      function buildArtistInfo() {
        // Identificadores
        var artistContainer = new PIXI.Container(),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: componentsFontSize
            },
            artistText = new PIXI.BitmapText( frameTexts.artist, textStyle );

        // Inserção do texto sobre artista da imagem
        artistContainer.addChild( artistText );

        // Retorna contedor do artista
        return artistContainer;
      }

      /// Monta seção de efetividade da carta
      function buildEffectivitySection() {
        // Identificadores
        var effectivitySection = content.buildMultiComponentSection( {
              caption: m.languages.keywords.getStat( 'effectivity' ) + ':',
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: {},
              iconsKey: '',
              textsCallback: () => '',
              manualSection: 'attacks-and-defenses'
            } ),
            [ cardFighting, cardEffectivity ] = [ sourceCard.content.stats.combativeness.fighting, sourceCard.content.stats.effectivity ],
            configIterator = configEffectivitySection();

        // Iniciação do iterador
        configIterator.next();

        // Para manobras
        maneuverComponents: {
          // Itera por fontes de manobras da carta
          for( let rank of [ 'primary', 'secondary' ] ) {
            // Identificadores
            let { attacks, source, currentPoints, spentPoints } = cardEffectivity.maneuvers[ rank ];

            // Filtra fontes de manobras sem ataques
            if( !attacks.length ) continue;

            // Para fontes de manobras naturais e de apetrechos
            if( !source || source instanceof m.Implement ) {
              // Identificadores
              let attack = attacks[ 0 ],
                  attackName = attack.name.replace( /^maneuver-/, '' ).oKebadToCamel(),
                  component = { [ attackName ]: currentPoints.toString() };

              // Monta componente
              content.buildMultiComponentSection( {
                insertInto: { section: effectivitySection, position: effectivitySection.containers.length },
                fontSize: componentsFontSize,
                isToHaveCaptionAlone: isToHaveCaptionAlone,
                components: component,
                iconsKey: 'maneuvers',
                textsCallback: ( name ) => component[ name ],
                manualSection: 'attacks-and-defenses'
              } );

              // Configura seção recém-criada
              configIterator.next( attack );
            }

            // Para fontes de manobras de armas
            else {
              // Identificadores
              let sourceRole = source.content.typeset.role.name,
                  component = {
                    [ sourceRole ]: currentPoints.toString()
                  };

              // Monta componente
              content.buildMultiComponentSection( {
                insertInto: { section: effectivitySection, position: effectivitySection.containers.length },
                fontSize: componentsFontSize,
                isToHaveCaptionAlone: isToHaveCaptionAlone,
                components: component,
                iconsKey: 'weapons',
                textsCallback: ( name ) => component[ name ],
                manualSection: 'attacks-and-defenses'
              } );

              // Configura seção recém-criada
              configIterator.next( source );
            }

            // Para caso haja pontos gastos com a fonte de manobras
            if( spentPoints ) {
              // Identificadores
              let maneuverText = effectivitySection.texts.oCycle( -1 );

              // Ajusta cor do texto da fonte de manobras
              maneuverText.tint = m.data.colors.red;

              // Mostra pontos já gastos
              maneuverText.hoverText = frameTexts.spentPoints + ': ' + spentPoints.toString();
            }
          }
        }

        // Para trajes
        garmentComponents: {
          for( let rank of [ 'primary', 'secondary' ] ) {
            let source = sourceCard.garments?.[ rank ].source;

            // Encerra iteração caso não exista traje na posição alvo
            if( !source ) break;

            // Define componente para mostrar traje alvo
            let component = { [ source.content.typeset.equipage ]: '' };

            // Monta componente
            content.buildMultiComponentSection( {
              insertInto: { section: effectivitySection, position: effectivitySection.containers.length },
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: component,
              iconsKey: 'equipages',
              textsCallback: ( name ) => component[ name ],
              manualSection: 'attacks-and-defenses'
            } );

            // Configura seção recém-criada
            configIterator.next( source );
          }
        }

        // Para apetrechos
        implementComponents: {
          // Captura apetrechos em uso
          let inUseImplements = Object.values( sourceCard.attachments ).flat().filter( attachment => attachment instanceof m.Implement && attachment.isInUse );

          // Itera por apetrechos em uso
          for( let implement of inUseImplements ) {
            let component = { implement: '' };

            // Monta componente
            content.buildMultiComponentSection( {
              insertInto: { section: effectivitySection, position: effectivitySection.containers.length },
              fontSize: componentsFontSize,
              isToHaveCaptionAlone: isToHaveCaptionAlone,
              components: component,
              iconsKey: 'equipages',
              textsCallback: ( name ) => component[ name ],
              manualSection: 'attacks-and-defenses'
            } );

            // Configura seção recém-criada
            configIterator.next( implement );
          }
        }

        // Adição da condutividade
        conductivityComponent: {
          // Identificadores
          let cardConductivity = cardEffectivity.conductivity;

          // Encerra bloco caso carta tenha condutividade nula
          if( !cardConductivity.current && !cardConductivity.natural.base ) break conductivityComponent;

          // Monta componente
          content.buildMultiComponentSection( {
            insertInto: { section: effectivitySection, position: effectivitySection.containers.length },
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: { conductivity: cardConductivity.current },
            iconsKey: 'stats',
            textsCallback: ( name ) => cardEffectivity[ name ].current.toString(),
            manualSection: 'conductivity'
          } );

          // Configura componente

          /// Identificadores
          let conductivityIndex = effectivitySection.names.indexOf( 'conductivity' ),
              conductivityIcon = effectivitySection.icons[ conductivityIndex ],
              conductivityText = effectivitySection.texts[ conductivityIndex ];

          /// Ajuste de cor do texto
          if( cardConductivity.current > cardConductivity.natural.base ) conductivityText.tint = m.data.colors.green

          else if( cardConductivity.current < cardConductivity.natural.base ) conductivityText.tint = m.data.colors.red;

          /// Textos suspensos

          //// Do ícone
          conductivityIcon.hoverText = m.languages.keywords.getStat( 'conductivity' );

          //// Do texto
          conductivityText.hoverText = cardConductivity.current == cardConductivity.natural.base ? '' : (
            `${ m.languages.snippets.source }: ${ cardConductivity.source.content.designation.title }\n` +
            `${ m.languages.snippets.base }: ${ cardConductivity.natural.base }`
          );
        }

        // Retorna seção de efetividade
        return effectivitySection;

        // Configurações pós-montagem
        function * configEffectivitySection() {
          for( let i = 0, target = yield; true; i++, target = yield ) {
            let icon = effectivitySection.icons[ i ],
                isItem = target instanceof m.Item;

            // Textos suspensos

            /// Do ícone alvo
            icon.hoverText = isItem ? target.content.designation.title : buildManeuverHoverText( target );

            // Eventos

            /// Exibição do equipamento apontado pelo ícone, se aplicável
            if( isItem ) icon.addListener( 'click', showLinkedItem.bind( this, target ) );
          }

          // Funções

          /// Monta texto suspenso com informações sobre manobra alvo
          function buildManeuverHoverText( target ) {
            // Identificadores
            var hoverText = '';

            // Adiciona nome da manobra ao texto suspenso
            hoverText += m.languages.keywords.getManeuver( target.name ) + ':';

            // Adiciona modificador ao texto suspenso, se existente
            if( target.modifier ) hoverText += `\n  ${ m.languages.keywords.getStat( 'modifier' ) }: ${ target.modifier }`;

            // Adiciona danos ao texto suspenso
            for( let damageType in target.damage ) hoverText += `\n  ${ m.languages.snippets.getDamageType( damageType ) }: ${ target.damage[ damageType ] }`;

            // Adiciona recuperação ao texto suspenso, se existente
            if( target.recovery ) hoverText += `\n  ${ m.languages.keywords.getStat( 'recovery' ) }: ${ target.recovery }`;

            // Adiciona alcance ao texto suspenso
            hoverText += `\n  ${ m.languages.keywords.getStat( 'range' ) }: ${ target.range }`;

            // Retorna texto suspenso
            return hoverText;
          }

          /// Mostra equipamento apontado pelo ícone
          function showLinkedItem( item ) {
            // Validação
            if( m.app.isInDevelopment ) m.oAssert( item instanceof m.Item );

            // Caso se esteja no modo informativo, não executa função
            if( m.app.isInInfoMode ) return false;

            // Exibe conteúdo do equipamento passado na moldura alvo
            return frame.content.init( item );
          }
        }
      }

      /// Monta seção de resistências da carta
      function buildResistancesSection() {
        // Identificadores
        var resistancesSection,
            cardResistances = sourceCard.content.stats.effectivity.resistances,
            resistancesArray = cardResistances.sources.concat( [ { source: null, value: cardResistances.natural.base } ] );

        // Encerra função caso o valor de todas as resistências para renderização seja 0
        if( [ cardResistances.current, cardResistances.natural.base ].every( origin => Object.values( origin ).every( resistance => !resistance ) ) )
          return new PIXI.Container();

        // Montagem da seção renderizável
        resistancesSection = content.buildMultiComponentSection( {
          caption: m.languages.keywords.getStat( 'resistances', { plural: true } ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: cardResistances.current,
          componentsFilter: ( name ) => cardResistances.current[ name ] || cardResistances.natural.base[ name ],
          iconsKey: 'damages',
          textsCallback: ( name ) => cardResistances.current[ name ] != Infinity ? cardResistances.current[ name ].toString() : '∞',
          manualSection: 'resistance-types'
        } );

        // Configurações pós-montagem
        for( let i = 0; i < resistancesSection.names.length; i++ ) {
          let name = resistancesSection.names[i],
              icon = resistancesSection.icons[i],
              text = resistancesSection.texts[i],
              [ currentValue, baseValue ] = [ cardResistances.current[ name ], cardResistances.natural.base[ name ] ];

          // Ajuste de cor do texto
          if( currentValue > baseValue ) text.tint = m.data.colors.green

          else if( currentValue < baseValue ) text.tint = m.data.colors.red;

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.snippets.getResistanceType( name );

          /// Do texto alvo
          text.hoverText = ( function () {
            // Não exibe texto suspenso caso valor atual e base sejam iguais
            if( currentValue == baseValue ) return '';

            // Cria texto suspenso a partir das fontes de resistência
            return resistancesArray.reduce( function ( accumulator, current ) {
              // Filtra valores nulos
              if( !current.value[ name ] ) return accumulator += '';

              // Trata valores infinitos
              if( current.value[ name ] == Infinity ) current.value[ name ] = '∞';

              // Monta texto inicial para valor alvo
              accumulator +=
                `\n${ m.languages.snippets.getSomethingFrom( current.source?.content.designation.title ?? m.languages.snippets.base, current.value[ name ] ) }`;

              // Caso fonte da resistência seja um traje, inclui valor de sua cobertura
              if( current.source instanceof m.Garment )
                accumulator += ` (${ m.languages.keywords.getStat( 'coverage' ) }: ${ current.source.content.stats.effectivity.current.coverage })`;

              // Retorna texto atual
              return accumulator;
            }, '' ).trimStart();
          } )();
        }

        // Retorna seção de resistências
        return resistancesSection;
      }

      /// Monta seção de vinculados da carta
      function buildAttachmentsSection() {
        // Identificadores
        var attachmentsSection,
            itemAttachments = { weapon: 0, garment: 0, implement: 0 },
            spellAttachments = { spell: 0 },
            manaAttachments = { mana: 0 },
            directAttachments = Object.values( sourceCard.attachments ).flat().filter(
              attachment => attachment instanceof m.Card || typeof attachment == 'number'
            );

        // Itera por componentes vinculados à carta
        for( let attachment of directAttachments ) {
          // Incrementa contagem de equipamentos vinculados
          if( attachment instanceof m.Item ) itemAttachments[ attachment.content.typeset.equipage ]++

          // Incrementa contagem de magias vinculadas
          else if( attachment instanceof m.Spell ) spellAttachments.spell++

          // Determina mana vinculado
          else if( typeof attachment == 'number' ) manaAttachments.mana = attachment;
        }

        // Montagem da seção renderizável
        attachmentsSection = content.buildMultiComponentSection( {
          caption: m.languages.keywords.getMisc( 'attachments', { plural: true } ) + ':',
          fontSize: componentsFontSize,
          isToHaveCaptionAlone: isToHaveCaptionAlone,
          components: itemAttachments,
          componentsFilter: ( name ) => sourceCard.maxAttachments[ name ],
          iconsKey: 'equipages',
          textsCallback: ( name ) => itemAttachments[ name ].toString() + '/' + sourceCard.maxAttachments[ name ].toString(),
          manualSection: 'attachness'
        } );

        // Configurações pós-montagem
        for( let i = 0; i < attachmentsSection.names.length; i++ ) {
          let name = attachmentsSection.names[i],
              icon = attachmentsSection.icons[i],
              text = attachmentsSection.texts[i];

          // Textos suspensos

          /// Do ícone alvo
          icon.hoverText = m.languages.keywords.getEquipage( name, { plural: true } );

          /// Do texto alvo
          text.hoverText = directAttachments.filter( attachment => attachment.content?.typeset.equipage == name ).reduce(
            ( accumulator, current ) => accumulator += frameTexts.getCardAttachment( current ), ''
          ).trimEnd();
        }

        // Adição das magias, se aplicável
        if( sourceCard.maxAttachments.spell ) {
          // Montagem da seção renderizável
          content.buildMultiComponentSection( {
            insertInto: { section: attachmentsSection, position: attachmentsSection.containers.length },
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: spellAttachments,
            iconsKey: 'primitiveTypes',
            textsCallback: ( name ) => spellAttachments[ name ].toString() + '/' + sourceCard.maxAttachments[ name ].toString(),
            manualSection: 'attachness'
          } );

          // Captura do índice das magias
          let spellIndex = attachmentsSection.names.indexOf( 'spell' );

          // Textos suspensos

          /// Do ícone
          attachmentsSection.icons[ spellIndex ].hoverText = m.languages.keywords.getPrimitiveType( 'spell', { plural: true } );

          /// Do texto
          attachmentsSection.texts[ spellIndex ].hoverText = directAttachments.filter( attachment => attachment instanceof m.Spell ).reduce(
            ( accumulator, current ) => accumulator += frameTexts.getCardAttachment( current ), ''
          ).trimEnd();
        }

        // Adição do mana, se aplicável
        if( sourceCard.maxAttachments.mana ) {
          // Montagem da seção renderizável
          content.buildMultiComponentSection( {
            insertInto: { section: attachmentsSection, position: attachmentsSection.containers.length },
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: manaAttachments,
            iconsKey: 'stats',
            textsCallback: ( name ) => manaAttachments[ name ].toString() + '/' + sourceCard.maxAttachments[ name ].toString(),
            manualSection: 'attachness'
          } );

          // Captura do índice do mana
          let manaIndex = attachmentsSection.names.indexOf( 'mana' );

          // Texto suspenso do ícone
          attachmentsSection.icons[ manaIndex ].hoverText = m.languages.keywords.getStat( 'mana' );
        }

        // Retorna seção de vinculados
        return attachmentsSection;
      }

      /// Monta seção de um bloco de texto
      function buildTextBlock( textSource ) {
        // Identificadores
        var textContainer = new PIXI.Container(),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.default.name,
              fontSize: componentsFontSize,
              maxWidth: frame.width - ( displayCard.body.front.getBounds().width + 30 )
            },
            bitmapText = new PIXI.BitmapText( textSource, textStyle );

        // Inserção do texto em seu contedor
        textContainer.addChild( bitmapText );

        // Retorna contedor do bloco de texto
        return textContainer;
      }

      /// Define posicionamento das seções posteriores em componentes com mais de uma seção por fileira
      function setLatterSectionsPosition( rows ) {
        // Define posicionamento das seções posteriores
        rows.map( row => row.children[1] ).forEach( ( section ) => section.x = frame.width - displayCard.info.x - displayCard.x - section.width - 20 );
      }

      /// Posiciona componentes do corpo do contedor de informações
      function positionBodyComponents( bodyComponents = displayCard.info.bodyComponents ) {
        // Validação
        if( m.app.isInDevelopment ) m.oAssert( bodyComponents.every( component => displayCard.info.bodyComponents.includes( component ) ) );

        // Iteração por componentes
        for( let component of bodyComponents ) {
          // Identificadores
          let marginY = 14,
              rows = Object.values( component.rows ?? component.sections ).flat().filter( row => row.children.length );

          // Posicionamento
          component.y = displayCard.info.cardHeading.y + displayCard.info.cardHeading.height + marginY;

          // Itera por seções do componente alvo
          for( let i = 0; i < rows.length; i++ ) {
            // Captura última seção
            let previousRow = rows[ i - 1 ];

            // Posiciona seção atual
            rows[i].position.y = previousRow ? previousRow.position.y + previousRow.height + marginY : 0;
          }
        }
      }

      /// Altera conteúdo visível no contedor de informações para o da temática da carta durante tempo em que cursor pairar em sua imagem
      function toggleFlavorComponent( event = {} ) {
        // Validação
        if( m.app.isInDevelopment ) m.oAssert( event.type == 'mouseover' );

        // Identificadores
        var visibleComponent = displayCard.info.bodyComponents.find( component => component.visible ),
            { cardFlavor } = displayCard.info;

        // Não executa função caso temática da carta já esteja visível
        if( cardFlavor == visibleComponent ) return;

        // Adiciona evento para restaurar visibilidade de componente atualmente visível no contedor de informações
        displayCard.body.front.once( 'mouseout', resetVisibleComponent );

        // Troca visibilidade entre a temática da carta e o componente atualmente visível
        return frame.content.changeVisibleComponent( cardFlavor );

        // Restaura visibilidade do componente originalmente visível no corpo do contedor de informações
        function resetVisibleComponent() {
          // Não executa função caso componente visível não seja mais o da temática da carta
          if( !cardFlavor.visible ) return;

          // Restauração da visibilidade
          return frame.content.changeVisibleComponent( visibleComponent );
        }
      }
    }

    /// Atualiza conteúdo de carta na moldura
    content.updateCard = function ( eventData = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.type == 'card' );
        m.oAssert(
          !eventData.targetComponents ||
          eventData.targetComponents.every( component => [ 'cardHeading', 'cardData', 'cardStats', 'cardCommand', 'cardEffects' ].includes( component ) )
        );
      }

      // Identificadores
      var [ displayCard, sourceCard ] = [ this.container, this.container.source ],
          cardInfo = displayCard.info,
          { targetComponents = setTargetComponents() } = eventData;

      // Itera por componentes atualizáveis da carta
      for( let componentName of targetComponents ) {
        // Remove elementos do componente
        cardInfo[ componentName ].removeChildren().forEach( child => child.destroy( { children: true } ) );

        // Limpa registro das seções do componente
        cardInfo[ componentName ].sections = {};

        // Limpa registro de fileiras dos componentes que o têm
        cardInfo[ componentName ].rows &&= [];
      }

      // Inicia atualização do conteúdo da carta
      return this.buildCard( sourceCard, true, targetComponents );

      // Funções

      /// Define seções padrão a serem atualizadas
      function setTargetComponents() {
        // Identificadores
        var { eventCategory, eventType } = eventData;

        // Para eventos de efeito e condições, atualiza apenas cabeçalho
        if( [ 'card-effect', 'condition-change' ].includes( eventCategory ) ) return [ 'cardHeading' ];

        // Para eventos de vinculação, atualiza apenas seção de dados
        if( eventCategory == 'attachability-change' ) return [ 'cardData' ];

        // Para eventos de uso, atualiza cabeçalho, seção de dados e de estatísticas
        if( eventCategory == 'card-use' ) return [ 'cardHeading', 'cardData', 'cardStats' ];

        // Define seções padrão a partir do tipo de evento
        switch( eventType ) {
          case 'size':
            return [ 'cardData', 'cardEffects' ];
          case 'mana-cost':
          case 'flow-cost':
          case 'potency':
          case 'item-in':
          case 'item-out':
            return [ 'cardData' ];
          default:
            return [ 'cardData', 'cardStats' ];
        }
      }
    }

    /// Monta conteúdo do baralho a ser exibido
    content.buildDeck = function ( sourceDeck, isUpdate = false ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( sourceDeck instanceof m.MainDeck || ( isUpdate && [ this.container.source, this.container.source.sideDeck ].includes( sourceDeck ) ) );
      }

      // Identificadores
      var displayDeck = this.container ??= frame.addChild( new PIXI.Container() ),
          deckComponents = displayDeck.components ??= {},
          currentScreen = m.GameScreen.current,
          isFieldFrame = frame.type == 'fieldFrame';

      // Configurações iniciais
      startConfig: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break startConfig;

        // Identificadores
        let componentsArray = [ 'mainDeck' ];

        // Definição do tipo de conteúdo
        this.type = 'deck';

        // Captura do baralho de referência
        displayDeck.source = sourceDeck;

        // Se baralho principal tiver um coadjuvante com moedas e – salvo a tela de montagem – com cartas, adiciona seu componente ao arranjo de componentes
        if( sourceDeck.sideDeck?.coins.max && ( currentScreen instanceof m.DeckBuildingScreen || sourceDeck.sideDeck.cards.length ) )
          componentsArray.push( 'sideDeck' );

        // Se na tela de montagem de baralhos, adiciona componente de validação ao arranjo de componentes
        if( currentScreen instanceof m.DeckBuildingScreen ) componentsArray.push( 'validation' );

        // Configuração dos componentes da moldura
        for( let componentName of componentsArray ) {
          // Cria e inseri componente alvo
          let targetComponent = displayDeck.components[ componentName ] = displayDeck.addChild( new PIXI.Container() );

          // Nomeia componente alvo
          targetComponent.name = componentName;

          // Define visibilidade padrão do componente alvo
          targetComponent.visible = false;

          // Cria objeto de seções do componente
          targetComponent.sections = {};
        }

        // Define como visível componente sobre baralho principal
        deckComponents.mainDeck.visible = true;
      }

      // Elaboração do conteúdo da moldura

      /// Do baralho principal
      buildMainDeck: {
        // Identificadores
        let { mainDeck } = deckComponents,
            targetDeck = sourceDeck.type == 'main-deck' ? sourceDeck : sourceDeck.mainDeck;

        // Montagem do cabeçalho
        mainDeck.sections.heading ??= mainDeck.addChild( buildDecksHeading( targetDeck ) );

        // Montagem do corpo
        mainDeck.sections.body ??= mainDeck.addChild( buildDecksCardList( targetDeck ) );
      }

      /// Do baralho coadjuvante
      buildSideDeck: {
        // Não executa bloco caso moldura não tenha componente atinente
        if( !deckComponents.sideDeck ) break buildSideDeck;

        // Identificadores
        let { sideDeck } = deckComponents,
            targetDeck = sourceDeck.type == 'side-deck' ? sourceDeck : sourceDeck.sideDeck;

        // Montagem do cabeçalho
        sideDeck.sections.heading ??= sideDeck.addChild( buildDecksHeading( targetDeck ) );

        // Montagem do corpo
        sideDeck.sections.body ??= sideDeck.addChild( buildDecksCardList( targetDeck ) );
      }

      /// Da validação
      buildValidation: {
        // Não executa bloco caso moldura não tenha componente atinente
        if( !deckComponents.validation ) break buildValidation;

        // Identificadores
        let { validation } = deckComponents;

        // Montagem do cabeçalho
        validation.sections.heading ??= validation.addChild( buildValidationHeading() );

        // Montagem do corpo
        validation.sections.body ??= validation.addChild( buildValidationBody() );
      }

      /// Do menu de componentes
      buildComponentsMenu: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break buildComponentsMenu;

        // Montagem
        displayDeck.componentsMenu = displayDeck.addChild( this.buildComponentsMenu( Object.values( deckComponents ) ) );

        // Posicionamento
        displayDeck.componentsMenu.position.set( frame.width - displayDeck.componentsMenu.width - frame.width * .015, 11.5 );
      }

      // Retorna conteúdo da moldura
      return this;

      // Funções

      /// Monta cabeçalho de baralhos
      function buildDecksHeading( targetDeck ) {
        // Identificadores
        var headingContainer = new PIXI.Container(),
            marginY = 6,
            [ deckMinCards, deckMaxCards ] = [ targetDeck.constructor.minCards, targetDeck.constructor.maxCards ],
            deckTexts = m.languages.components.deckFrame();

        // Montagem do título
        buildCaption: {
          // Geração da seção do título
          let sectionCaption = headingContainer.caption = headingContainer.addChild( new PIXI.Container() );

          // Geração do título
          let mainTitle = sectionCaption.main = sectionCaption.addChild( new PIXI.BitmapText(
                m.languages.keywords.getDeck( targetDeck.type ), { fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22 }
              ) );

          // Para caso se esteja na tela de montagem de baralhos
          if( currentScreen instanceof m.DeckBuildingScreen ) {
            // Geração do subtítulo
            let subTitle = sectionCaption.sub = sectionCaption.addChild( new PIXI.BitmapText(
                  deckTexts.getSubtitle( deckMinCards, deckMaxCards ), { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16 }
                ) );

            // Posicionamento horizontal dos títulos
            for( let type of [ 'main', 'sub' ] ) sectionCaption[ type ].x = sectionCaption.width * .5 - sectionCaption[ type ].width * .5;

            // Posicionamento vertical do subtítulo
            subTitle.y = mainTitle.y + mainTitle.height;

            // Configurações do subtítulo

            /// Torna subtítulo interativo
            subTitle.interactive = true;

            /// Abre seção do manual sobre limite de cartas ao se clicar no subtítulo
            subTitle.addListener( 'click', () => m.app.openManualSection( 'card-limit' ) );
          }

          // Posicionamento do contedor título
          sectionCaption.position.set( frame.width * .5 - sectionCaption.width * .5, marginY );

          // Configurações do título

          /// Torna título interativo
          mainTitle.interactive = true;

          /// Abre seção do manual sobre baralhos ao se clicar no título
          mainTitle.addListener( 'click', () => m.app.openManualSection( 'decks' ) );
        }

        // Montagem do indicador de moedas
        buildCoinsDisplayer: {
          // Geração e inserção
          let { coins: deckCoins, credits: deckCredits } = targetDeck,
              coinsContainer = headingContainer.coins = headingContainer.addChild( new PIXI.Container() ),
              coinsIcon = coinsContainer.icon = coinsContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.coin ] ) ),
              textStyle = {
                fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -19
              },
              textContainer = coinsContainer.textContainer = coinsContainer.addChild( new PIXI.Container() ),
              coinsText = textContainer.coins = textContainer.addChild( new PIXI.BitmapText( String( deckCoins.max - deckCoins.spent ), textStyle ) ),
              interactiveComponents = [ coinsIcon, coinsText ];

          // Destaca cor do texto sobre moedas disponíveis caso seu valor seja negativo
          if( coinsText.text < 0 ) coinsText.tint = m.data.colors.red;

          // Redimensionamento do ícone
          coinsIcon.oScaleByGreaterSize( 21 );

          // Textos suspensos

          /// Do ícone
          coinsIcon.hoverText = deckTexts.availableCoins;

          /// Do texto sobre moedas
          coinsText.hoverText =
            `${ deckTexts.totalCoins }: ${ deckCoins.max }\n` + `${ deckTexts.spentCoins }: ${ deckCoins.spent }` +
            ( deckCredits.spent ? `\n${ deckTexts.spentCredits }: ${ deckCredits.spent }` : '' );

          // Para caso baralho tenha créditos
          if( deckCredits.total ) {
            // Identificadores
            let separator = textContainer.separator = textContainer.addChild( new PIXI.BitmapText( ' + ', textStyle ) ),
                creditsText = textContainer.credits = textContainer.addChild( new PIXI.BitmapText( deckCredits.total.toString(), textStyle ) );

            // Define texto suspenso sobre créditos
            creditsText.hoverText = deckTexts.getCreditsSource( deckCredits );

            // Adiciona texto sobre créditos ao arranjo de componentes interativos
            interactiveComponents.push( creditsText );

            // Posiciona horizontalmente textos adicionados
            for( let i = 1, components = [ coinsText, separator, creditsText ]; i < components.length; i++ )
              components[ i ].x = components[ i - 1 ].x + components[ i - 1 ].width;
          }

          // Posicionamentos

          /// Do ícone
          coinsIcon.position.set( textContainer.x + textContainer.width + 2, textContainer.height * .5 - coinsIcon.height * .5 );

          /// Do contedor
          coinsContainer.position.set( frame.width * .015, marginY );

          // Itera por componentes interativos do indicador de moedas
          for( let component of interactiveComponents ) {
            // Torna componente interativo
            component.interactive = true;

            // Nomeia texto suspenso do componente
            component.hoverTextName = `${ frame.name }-toggleableContent-hover-text`;

            // Adiciona evento para exibição do texto suspenso
            component.addListener( 'mouseover', m.app.showHoverText );

            // Adiciona evento para abrir seção do manual sobre preço de cartas ao se clicar no componente
            component.addListener( 'click', () => m.app.openManualSection( 'card-price' ) );
          }
        }

        // Retorna componente do cabeçalho
        return headingContainer;
      }

      /// Monta lista de cartas de baralhos
      function buildDecksCardList( targetDeck ) {
        // Identificadores
        var cardsList = new PIXI.Container(),
            isMainDeck = targetDeck.type == 'main-deck',
            targetComponent = isMainDeck ? deckComponents.mainDeck : deckComponents.sideDeck,
            componentHeading = targetComponent.sections.heading,
            deckMaxCards = targetDeck.constructor.maxCards,
            fontSize = isFieldFrame ? -15 : -16,
            columnsQuantity = !isFieldFrame ? 5 : isMainDeck ? 3 : 2,
            [ entryWidth, entryHeight ] = [ isFieldFrame ? 205.5 : 219, 22 ],
            [ entryMarginX, entryMarginY ] = [ isFieldFrame && isMainDeck ? 9 : 18, isFieldFrame && isMainDeck ? 4 : 12 ],
            cardsListWidth = ( entryWidth + entryMarginX ) * columnsQuantity - entryMarginX;

        // Monta lista de cartas
        for( let i = 1, row = 0; i <= deckMaxCards; i++ ) {
          // Identificadores
          let listEntry = cardsList.addChild( new PIXI.Container() ),
              targetCard = targetDeck.cards[ i - 1 ],
              cardText = targetCard?.content.designation.full.replace( / \(\w\)$/, '+' ) ?? '',
              textStyle = { fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: fontSize },
              entryIndex = listEntry.index = listEntry.addChild( new PIXI.BitmapText( i + '. ', textStyle ) ),
              entryTitle = listEntry.title = targetCard ? listEntry.addChild( new PIXI.BitmapText( cardText, textStyle ) ) : null;

          // Posicionamentos

          /// Do nome da carta, se aplicável
          if( entryTitle ) entryTitle.x = entryIndex.x + entryIndex.width;

          /// Da entrada
          listEntry.position.set(
            ( ( i - 1 ) % columnsQuantity ) * ( entryWidth + entryMarginX ), row * ( entryHeight + entryMarginY )
          );

          // Atualiza fileira da lista
          if( i % columnsQuantity == 0 ) row++;

          // Caso não haja uma carta na entrada alvo, avança para próxima iteração
          if( !entryTitle ) continue;

          // Configurações do nome da carta

          /// Captura da referência
          entryTitle.source = targetCard;

          /// Interatividade
          entryTitle.buttonMode = entryTitle.interactive = true;

          /// Mudança de cor, ao pairar cursor sobre si
          entryTitle.addListener( 'mouseover', () => entryTitle.tint = m.data.colors.fadedRed );

          /// Retorno a cor original, à saída do cursor de si
          entryTitle.addListener( 'mouseout', () => entryTitle.tint = m.data.colors.white );

          /// Mostra em uma moldura carta vinculada ao nome
          entryTitle.addListener( 'click', GameFrame.showCard, targetCard );
        }

        // Posiciona lista de cartas
        cardsList.position.set(
          frame.width * .5 - cardsListWidth * .5,
          isMainDeck && isFieldFrame ? componentHeading.y + componentHeading.height + 9 : frame.height * .5 - cardsList.height * .5
        );

        // Retorna lista de cartas
        return cardsList;
      }

      /// Monta cabeçalho da validação
      function buildValidationHeading() {
        // Identificadores
        var headingContainer = new PIXI.Container(),
            sectionCaption = headingContainer.caption = headingContainer.addChild( new PIXI.BitmapText(
              m.languages.keywords.getFrameComponent( 'validation' ), { fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22 }
            ) );

        // Posicionamento do contedor do título
        sectionCaption.position.set( frame.width * .5 - sectionCaption.width * .5, 6 );

        // Retorna cabeçalho
        return headingContainer;
      }

      /// Monta corpo da validação
      function buildValidationBody() {
        // Identificadores
        var componentHeading = deckComponents.validation.sections.heading,
            validation = sourceDeck.validate(),
            { invalidData } = validation,
            sideDeck = sourceDeck.type == 'side-deck' ? sourceDeck : sourceDeck.sideDeck,
            bodyContainer = new PIXI.Container(),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -16, maxWidth: frame.width - frame.width * .015 * 2,
              tint: validation.isValid ? m.data.colors.lightGreen : m.data.colors.lightRed
            },
            invalidationTexts = m.languages.components.deckFrame().validation;

        // Geração dos textos de validação

        /// Para conjunto de baralhos válido
        if( validation.isValid )
          bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getIsValidText( sideDeck?.cards.length ), textStyle ) )

        /// Para conjunto de baralhos inválido
        else {
          // Para invalidação na quantidade mínima de cartas
          if( !validation.minDeckCards )
            bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getMinDeckCards( Object.entries( invalidData.minDeckCards ) ), textStyle ) );

          // Para invalidação na quantidade máxima de cartas
          if( !validation.maxDeckCards )
            bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getMaxDeckCards( Object.entries( invalidData.maxDeckCards ) ), textStyle ) );

          // Para invalidação na quantidade máxima de moedas gastas
          if( !validation.maxCoinsSpent )
            bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getMaxCoinsSpent( Object.keys( invalidData.maxCoinsSpent ) ), textStyle ) );

          // Para invalidação na quantidade máxima de cartas iguais
          if( !validation.maxSameCards )
            bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getMaxSameCards(), textStyle ) );

          // Para invalidação na quantidade [mínima/máxima] de cartas comandadas
          for( let validationType of [ 'minCommandCards', 'maxCommandCards' ] ) {
            // Filtra tipos de validação válidos
            if( validation[ validationType ] ) continue;

            // Geração do texto de validação
            bodyContainer.addChild( new PIXI.BitmapText( invalidationTexts.getCommandCards( validationType, invalidData ), textStyle ) );
          }
        }

        // Captura do resultado da validação
        bodyContainer.isValidDeck = validation.isValid;

        // Posicionamentos

        /// Dos textos de validação
        for( let i = 0, validationTexts = bodyContainer.children; i < validationTexts.length; i++ ) {
          // Identificadores
          let [ previousText, currentText ] = [ validationTexts[ i - 1 ], validationTexts[ i ] ];

          // Posicionamento vertical do texto alvo
          currentText.y = previousText ? previousText.y + previousText.height + 6 : 0;
        }

        /// Do contedor do corpo
        bodyContainer.position.set( frame.width * .015, componentHeading.y + componentHeading.height + 12 );


        // Retorna corpo da validação
        return bodyContainer;
      }
    }

    /// Atualiza conteúdo do baralho na moldura
    content.updateDeck = function ( targetDeck ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.type == 'deck' );
        m.oAssert( [ this.container.source, this.container.source.sideDeck ].includes( targetDeck ) );
      }

      // Identificadores
      var displayDeck = this.container,
          deckComponents = displayDeck.components,
          targetComponents = [];

      // Adiciona componente do baralho atual ao arranjo de componentes
      targetComponents.push( targetDeck.type == 'main-deck' ? 'mainDeck' : 'sideDeck' );

      // Caso componente de validação exista, adiciona-o ao arranjo de componentes
      if( deckComponents.validation ) targetComponents.push( 'validation' );

      // Itera por componentes atualizáveis do baralho
      for( let componentName of targetComponents ) {
        // Remove elementos do componente
        deckComponents[ componentName ].removeChildren().forEach( child => child.destroy( { children: true } ) );

        // Limpa registro das seções do componente
        deckComponents[ componentName ].sections = {};
      }

      // Inicia atualização dos componentes da moldura
      return this.buildDeck( targetDeck, true );
    }

    /// Monta conteúdo da casa a ser exibida
    content.buildCardSlot = function ( sourceSlot, isUpdate = false ) {
      // Identificadores
      var displaySlot = this.container ??= frame.addChild( new PIXI.Container() ),
          isFieldFrame = frame.type == 'fieldFrame',
          componentsFontSize = -16,
          isToHaveCaptionAlone = false,
          frameTexts = m.languages.components.componentFrame( sourceSlot );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( sourceSlot instanceof m.FieldSlot );
        m.oAssert( !isUpdate || sourceSlot == displaySlot.source );
      }

      // Configurações iniciais
      startConfig: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break startConfig;

        // Definição do tipo de conteúdo
        this.type = 'card-slot';

        // Captura casa de referência
        displaySlot.source = sourceSlot;

        // Posicionamento horizontal do contedor do conteúdo
        displaySlot.x = frame.width * .015;

        // Adiciona evento de atualização de dados da casa
        m.events.attachabilityChangeEnd.attachment.add( displaySlot.source, this.updateCardSlot, { context: this } );
      }

      // Configurações da imagem da casa
      configBody: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break configBody;

        // Identificadores
        let slotBody = displaySlot.body = displaySlot.addChild( new PIXI.Graphics() ),
            textStyle = new PIXI.TextStyle( {
              fontFamily: m.assets.fonts.sourceSansPro.default.name, fontSize: isFieldFrame ? 72 : 45, fill: 'white'
            } ),
            bodyText = slotBody.bitmap = slotBody.addChild( new PIXI.Text( sourceSlot.name.replace( /^.+-/, '' ), textStyle ) );

        // Renderização da casa

        /// Definição das bordas
        slotBody.lineStyle( m.CardSlot.borderSize, m.data.colors.semiwhite1, 1, 0 );

        /// Adição de preenchimento
        slotBody.beginFill( m.data.colors.black, 1 );

        /// Renderização
        slotBody.drawRect( 0, 0, m.Card.sizes[ frame.type ].width, m.Card.sizes[ frame.type ].height );

        /// Término do preenchimento
        slotBody.endFill();

        // Posicionamento do texto da casa
        bodyText.position.set( slotBody.width * .5 - bodyText.width * .5, slotBody.height * .5 - bodyText.height * .5 );

        // Configurações do corpo da casa

        /// Torna corpo interativo
        slotBody.interactive = true;

        /// Adiciona evento para abrir seção do manual sobre estrutura do campo quando o corpo da casa for clicado
        slotBody.addListener( 'click', () => m.app.openManualSection( 'the-field' ) );
      }

      // Configurações das informações sobre casa
      configInfo: {
        // Identificadores
        let slotInfo = displaySlot.info ??= displaySlot.addChild( new PIXI.Container() ),
            infoSections = slotInfo.sections ??= {};

        // Montagem do título
        buildTitle: {
          // Não executa bloco caso moldura deva ser apenas atualizada
          if( isUpdate ) break buildTitle;

          // Identificadores
          let titleContainer = infoSections.title = slotInfo.addChild( new PIXI.Container() ),
              textStyle = {
                fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -22
              },
              titleText = titleContainer.bitmap = titleContainer.addChild( new PIXI.BitmapText( frameTexts.title, textStyle ) );

          // Configurações do título

          /// Torna título interativo
          titleText.interactive = true;

          /// Adiciona evento para abrir seção do manual sobre nomeação de casas quando o título for clicado
          titleText.addListener( 'click', () => m.app.openManualSection( 'card-slot-names' ) );
        }

        // Montagem da seção de vinculados
        infoSections.attachments = slotInfo.addChild( buildAttachmentsSection() );

        // Posicionamento vertical da seção de vinculados
        infoSections.attachments.y = displaySlot.body.height - infoSections.attachments.height - 10;

        // Posicionamento horizontal do contedor de informações
        if( !isUpdate ) displaySlot.info.x = displaySlot.body.width + 10;
      }

      // Posicionamento vertical do contedor do conteúdo
      displaySlot.y = frame.height * .5 - displaySlot.height * .5;

      // Retorna conteúdo da moldura
      return this;

      // Funções

      /// Monta seção de vinculados da casa
      function buildAttachmentsSection() {
        // Identificadores
        var attachmentsSection,
            spellAttachments = { spell: 0 },
            manaAttachments = { mana: 0 },
            directAttachments = Object.values( sourceSlot.attachments ).flat().filter(
              attachment => attachment instanceof m.Card || typeof attachment == 'number'
            );

        // Itera por componentes vinculados à casa
        for( let attachment of directAttachments ) {
          // Incrementa contagem de magias vinculadas
          if( attachment instanceof m.Spell ) spellAttachments.spell++

          // Determina mana vinculado
          else if( typeof attachment == 'number' ) manaAttachments.mana = attachment;
        }

        // Montagem das magias vinculadas
        buildAttachedSpells: {
          // Montagem da seção renderizável
          attachmentsSection = content.buildMultiComponentSection( {
            caption: m.languages.keywords.getMisc( 'attachments', { plural: true } ) + ':',
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: spellAttachments,
            iconsKey: 'primitiveTypes',
            textsCallback: ( name ) => spellAttachments[ name ].toString() + '/' + sourceSlot.maxAttachments[ name ].toString(),
            manualSection: 'attachness'
          } );

          // Captura do índice das magias
          let spellIndex = attachmentsSection.names.indexOf( 'spell' );

          // Textos suspensos

          /// Do ícone
          attachmentsSection.icons[ spellIndex ].hoverText = m.languages.keywords.getPrimitiveType( 'spell', { plural: true } );

          /// Do texto
          attachmentsSection.texts[ spellIndex ].hoverText = directAttachments.filter( attachment => attachment instanceof m.Spell ).reduce(
            ( accumulator, current ) => accumulator += frameTexts.getCardAttachment( current ), ''
          ).trimEnd();
        }

        // Montagem do mana vinculado
        buildAttachedMana: {
          // Montagem da seção renderizável
          content.buildMultiComponentSection( {
            insertInto: { section: attachmentsSection, position: attachmentsSection.containers.length },
            fontSize: componentsFontSize,
            isToHaveCaptionAlone: isToHaveCaptionAlone,
            components: manaAttachments,
            iconsKey: 'stats',
            textsCallback: ( name ) => manaAttachments[ name ].toString() + '/' + sourceSlot.maxAttachments[ name ].toString(),
            manualSection: 'attachness'
          } );

          // Captura do índice do mana
          let manaIndex = attachmentsSection.names.indexOf( 'mana' );

          // Texto suspenso do ícone
          attachmentsSection.icons[ manaIndex ].hoverText = m.languages.keywords.getStat( 'mana' );
        }

        // Retorna seção de vinculados
        return attachmentsSection;
      }
    }

    /// Atualiza conteúdo da casa na moldura
    content.updateCardSlot = function ( eventData ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.type == 'card-slot' );

      // Identificadores
      var [ displaySlot, sourceSlot ]  = [ this.container, this.container.source ];

      // Remove do contedor de informações antiga seção de vinculados
      displaySlot.info.removeChild( displaySlot.info.sections.attachments ).destroy( { children: true } );

      // Inicia atualização do conteúdo da casa
      return this.buildCardSlot( sourceSlot, true );
    }

    /// Monta registro de eventos ocorridos na partida em que se estiver
    content.buildLog = function ( sourceStage, isUpdate = false, page = 1 ) {
      // Identificadores
      var { flow: matchFlow, log: matchLog } = m.GameMatch.current,
          stageName = sourceStage.replace( /-log$/, '' ),
          stageSnippet = new RegExp( `(^|\.)${ stageName }(\.|$)` ),
          displayLog = this.container ??= frame.addChild( new PIXI.Container() ),
          logMarginX = frame.width * .015,
          frameTexts = m.languages.components.logFrame();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( typeof sourceStage == 'string' && sourceStage.endsWith( '-log' ) );
        m.oAssert( !isUpdate || sourceStage == displayLog.source );
        m.oAssert( Number.isInteger( page ) && page >= 0 );
      }

      // Caso página a ser exibida seja a '0', exibe registros de um estágio de referência posterior ao atual
      if( !page ) return changeLog( 'advance' );

      // Configurações iniciais
      startConfig: {
        // Não executa bloco caso moldura deva ser apenas atualizada
        if( isUpdate ) break startConfig;

        // Definição do tipo de conteúdo
        this.type = 'match-log';

        // Captura indicador sobre estágio de referência para exibição dos registros de eventos
        displayLog.source = sourceStage;

        // Posicionamento horizontal do contedor do conteúdo
        displayLog.x = logMarginX;

        // Adiciona evento para atualização dos registros da partida
        m.events.logChangeEnd.any.add( m.GameMatch.current, this.updateLog, { context: this } );
      }

      // Elaboração do conteúdo da moldura
      buildContent: {
        // Quando ainda não existente, montagem e captura do cabeçalho
        displayLog.heading ??= displayLog.addChild( buildHeading() );

        // Caso moldura deva ser atualizada, remove corpo atual
        if( isUpdate ) displayLog.removeChild( displayLog.body ).destroy( { children: true } );

        // Montagem e captura do corpo
        displayLog.body = displayLog.addChild( buildBody() );

        // Caso página desejada não tenha sido alcançada, exibe registros de um estágio de referência anterior ao atual
        if( displayLog.body.page != page ) return changeLog( 'regress' );
      }

      // Retorna conteúdo da moldura
      return this;

      // Funções

      /// Monta cabeçalho
      function buildHeading() {
        // Identificadores
        var headingContainer = new PIXI.Container(),
            stageLastName = stageName.slice( stageName.lastIndexOf( '.' ) + 1 ),
            stageNumber = stageName.replace( /\D/g, '' ),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -20
            },
            headingTitle = headingContainer.title = headingContainer.addChild(
              new PIXI.BitmapText( frameTexts.getTitle( stageLastName, stageNumber ), textStyle )
            ),
            headingArrows = headingContainer.arrows = [],
            beginArrow = headingContainer.arrows.beginArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrowDouble ] ),
            previousArrow = headingContainer.arrows.previousArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ),
            nextArrow = headingContainer.arrows.nextArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrow ] ),
            endArrow = headingContainer.arrows.endArrow = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.arrowDouble ] ),
            [ arrowMarginX, arrowMarginY ] = [ 20, 11.5 ];

        // Itera por setas
        for( let arrowName of [ 'beginArrow', 'previousArrow', 'nextArrow', 'endArrow' ] ) {
          // Identificadores
          let arrow = headingContainer.arrows[ arrowName ];

          // Adiciona seta ao arranjo de setas
          headingArrows.push( arrow );

          // Redimensiona seta alvo
          arrow.oScaleByGreaterSize( 15 );

          // Ajusta ponto âncora da seta alvo
          arrow.anchor.set( .5 );

          // Adiciona seta alvo ao contedor do cabeçalho
          headingContainer.addChild( arrow );

          // Torna seta alvo interativa
          arrow.buttonMode = arrow.interactive = true;

          // Define texto suspenso da seta alvo
          arrow.hoverText = frameTexts[ arrowName ];

          // Nomeia texto suspenso da seta alvo
          arrow.hoverTextName = `${ frame.name }-fixedContent-hover-text`;

          // Adiciona evento para exibição de texto suspenso
          arrow.addListener( 'mouseover', m.app.showHoverText );
        }

        // Rotaciona setas para exibição de entradas de registro posteriores
        for( let arrow of [ nextArrow, endArrow ] ) arrow.angle = 180;

        // Posicionamentos

        /// Do título
        headingTitle.position.set( ( frame.width - logMarginX * 2 ) * .5 - headingTitle.width * .5, 0 );

        /// Das setas
        headingContainer.arrows.forEach( ( arrow, index ) => arrow.position.set( arrow.width * .5 + index * arrowMarginX, arrowMarginY ) );

        /// Do cabeçalho
        headingContainer.y = 6;

        // Eventos

        /// Para exibição dos eventos mais anteriores
        beginArrow.addListener( 'click', () =>
          m.assets.audios.soundEffects[ changeLog( 'toFirst' ) ? 'click-previous' : 'click-deny' ].play()
        );

        /// Para exibição de eventos anteriores
        previousArrow.addListener( 'click', () =>
          m.assets.audios.soundEffects[ frame.content.buildLog( sourceStage, true, displayLog.body.page + 1 ) ? 'click-previous' : 'click-deny' ].play()
        );

        /// Para exibição de eventos posteriores
        nextArrow.addListener( 'click', () =>
          m.assets.audios.soundEffects[ frame.content.buildLog( sourceStage, true, displayLog.body.page - 1 ) ? 'click-next' : 'click-deny' ].play()
        );

        /// Para exibição dos eventos mais posteriores
        endArrow.addListener( 'click', () =>
          m.assets.audios.soundEffects[ changeLog( 'toLast' ) ? 'click-next' : 'click-deny' ].play()
        );

        // Retorna cabeçalho
        return headingContainer;
      }

      /// Monta corpo dos registros
      function buildBody() {
        // Identificadores
        var bodyContainer = new PIXI.Container();

        // Define cadeias de fluxo a terem eventos exibidos
        setFlowChains: {
          // Identificadores
          var flowChains = [];

          // Itera por cadeias de fluxo já decorridas
          for( let flowChain in matchLog.body ) {
            // Filtra rodadas que não sejam a atual
            if( !flowChain.startsWith( matchFlow.round?.name ?? '' ) ) continue;

            // Filtra cadeias de fluxo sem o nome do estágio alvo
            if( !stageSnippet.test( flowChain ) ) continue;

            // Adiciona cadeia de fluxo alvo ao arranjo de cadeias de fluxo a ter o registro exibido
            flowChains.push( flowChain );
          }

          // Ordena cadeias de fluxo pertinentes da mais recente a mais antiga
          flowChains.reverse();
        }

        // Exibe registros de cadeias de fluxo alvo
        buildLogs: {
          // Identificadores
          let textStyle = {
                fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -15, maxWidth: frame.width - logMarginX * 2
              },
              [ outerMarginY, innerMarginY ] = [ 6, 4 ],
              [ currentHeight, currentPage, pageEntries ] = [ 0, 1, [] ],
              maxHeight = frame.height - displayLog.heading.y - displayLog.heading.height;

          // Itera por cadeias de fluxo alvo
          flowChainLoop: for( let flowChain of flowChains ) {
            // Identificadores
            let eventsToLog = matchLog.body[ flowChain ].events.slice().reverse(),
                flowChainText = m.FlowUnit.displayFlowChain( flowChain.slice( flowChain.search( stageName ) + stageName.length + 1 ) );

            // Itera por eventos a serem registrados
            eventsLoop: for( let eventData of eventsToLog ) {
              // Filtra eventos sem uma descrição
              if( !eventData.description ) continue;

              // Identificadores
              let entryContainer = new PIXI.Container(),
                  eventDescription = eventData.description,
                  textToLog = flowChainText + ': ' + eventDescription,
                  logEntry = entryContainer.addChild( new PIXI.BitmapText( textToLog, textStyle ) ),
                  previousEntry = pageEntries.slice( -1 ).pop(),
                  marginY = previousEntry ? innerMarginY : outerMarginY;

              // Para caso adição do registro atual ultrapasse altura máxima permitida
              if( currentHeight + marginY + entryContainer.height > maxHeight ) {
                // Caso página atual seja a desejada, finaliza inserção de entradas
                if( page == currentPage ) break flowChainLoop;

                // Atualiza página atual
                currentPage++;

                // Zera altura atual
                currentHeight = 0;

                // Limpa arranjo de entradas
                while( pageEntries.length ) pageEntries.shift().destroy( { children: true } );

                // Ajusta valor da margem vertical
                marginY = outerMarginY;
              }

              // Adiciona entrada ao arranjo de entradas da página atual
              pageEntries.push( entryContainer );

              // Atualiza valor da altura atual
              currentHeight += marginY + entryContainer.height;
            }
          }

          // Captura página cujos registros serão exibidos
          bodyContainer.page = currentPage;

          // Se existirem, adiciona ao corpo de registros entradas da página alvo
          if( pageEntries.length ) bodyContainer.addChild( ...pageEntries );

          // Itera por entradas adicionadas
          for( let i = 0; i < bodyContainer.children.length; i++ ) {
            // Identificadores
            let [ currentEntry, previousEntry ] = [ bodyContainer.children[ i ], bodyContainer.children[ i - 1 ] ];

            // Posiciona entrada alvo
            currentEntry.y = previousEntry ? previousEntry.y + previousEntry.height + innerMarginY : outerMarginY;
          }
        }

        // Posiciona contedor de registros abaixo do cabeçalho
        bodyContainer.y = displayLog.heading.y + displayLog.heading.height;

        // Retorna contedor de registros
        return bodyContainer;
      }

      /// Altera registros sendo exibidos
      function changeLog( changeType ) {
        // Validação
        if( m.app.isInDevelopment ) m.oAssert( [ 'toFirst', 'regress', 'advance', 'toLast' ].includes( changeType ) );

        // Identificadores
        var currentRoundName = matchFlow.round?.name ?? '',
            matchLogs = Object.keys( matchLog.body ),
            newStage = ( function () {
              // Identifica estágio de referência alvo segundo argumento recebido
              switch( changeType ) {
                case 'toFirst':
                  return toFirst();
                case 'regress':
                  return regress();
                case 'advance':
                  return advance();
                case 'toLast':
                  return toLast();
              }
            } )();

        // Caso não tenha sido encontrado um novo estágio, indica isso
        if( !newStage ) return false;

        // Monta registro de eventos de novo estágio de referência
        frame.content.init( newStage );

        // Indica que um novo estágio de referência está sendo exibido
        return true;

        // Funções

        /// Retorna aos registros mais anteriores
        function toFirst() {
          // Identificadores
          var stageNameIndex = matchLogs.findIndex( flowChain =>
                flowChain.startsWith( currentRoundName ) && matchLog.body[ flowChain ].events.some( eventData => eventData.description )
              ),
              newStage = matchLog.getLogStage( matchLogs[ stageNameIndex ] ?? '' );

          // Para caso estágio sendo atualmente exibido seja o mais anterior
          if( displayLog.source == newStage ) {
            // Caso página de registros do estágio já seja a primeira, indica que não é possível realizar a operação
            if( displayLog.body.page == 1 ) return false;

            // Mostra a primeira página de registros do estágio alvo
            frame.content.buildLog( newStage, true );
          }

          // Retorna estágio de referência mais anterior da rodada em que se estiver
          return newStage;
        }

        /// Exibe registros anteriores aos atuais
        function regress() {
          // Identificadores
          var stageNameIndex = matchLogs.findIndex( flowChain => flowChain.startsWith( currentRoundName ) && stageSnippet.test( flowChain ) ),
              newFlowChain = matchLogs.slice( 0, stageNameIndex ).reverse().find(
                flowChain => flowChain.startsWith( currentRoundName ) && matchLog.body[ flowChain ].events.some( eventData => eventData.description )
              ) ?? '';

          // Retorna primeiro estágio de referência anterior ao atual que tenha eventos
          return matchLog.getLogStage( newFlowChain );
        }

        /// Exibe registros posteriores aos atuais
        function advance() {
          // Identificadores
          var stageNameIndex = matchLogs.findLastIndex( flowChain => flowChain.startsWith( currentRoundName ) && stageSnippet.test( flowChain ) ),
              newFlowChain = matchLogs.slice( stageNameIndex + 1 ).find(
                flowChain => flowChain.startsWith( currentRoundName ) && matchLog.body[ flowChain ].events.some( eventData => eventData.description )
              ) ?? '';

          // Retorna primeiro estágio de referência posterior ao atual que tenha eventos
          return matchLog.getLogStage( newFlowChain );
        }

        /// Retorna aos registros mais posteriores
        function toLast() {
          // Identificadores
          var stageNameIndex = matchLogs.findLastIndex( flowChain =>
                flowChain.startsWith( currentRoundName ) && matchLog.body[ flowChain ].events.some( eventData => eventData.description )
              ),
              newStage = matchLog.getLogStage( matchLogs[ stageNameIndex ] ?? '' );

          // Para caso estágio sendo atualmente exibido seja o mais posterior
          if( displayLog.source == newStage ) {
            // Caso página de registros do estágio já seja a primeira, indica que não é possível realizar a operação
            if( displayLog.body.page == 1 ) return false;

            // Mostra a primeira página de registros do estágio alvo
            frame.content.buildLog( newStage, true );
          }

          // Retorna estágio de referência mais posterior da rodada em que se estiver
          return newStage;
        }
      }
    }

    /// Atualiza conteúdo dos registros de eventos do estágio de referência atual
    content.updateLog = function ( eventData ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.type == 'match-log' );

      // Identificadores
      var { data: logData, flowChain } = eventData,
          sourceStage = this.container.source,
          stageSnippet = new RegExp( `(^|\.)${ sourceStage.replace( /-log$/, '' ) }(\.|$)` );

      // Não executa função caso uma entrada sem descrição tenha sido adicionada
      if( logData && !logData.description ) return;

      // Caso evento tenha ocorrido na cadeia de fluxo sendo exibida, apenas atualiza o registro de eventos
      if( stageSnippet.test( flowChain ) ) return this.buildLog( sourceStage, true );

      // Do contrário, altera registro de eventos para que estágio de referência seja o novo prioritário da cadeia de fluxo atual
      return this.init( m.GameMatch.current.log.getLogStage( flowChain ) );
    }

    /// Monta menu de componentes da moldura
    content.buildComponentsMenu = function ( menuComponents ) {
      // Identificadores
      var componentsMenu = new PIXI.Container(),
          menuOptions = componentsMenu.options = {},
          previousOption = null,
          marginX = 8,
          visibleComponent = menuComponents.find( component => component.visible );

      // Itera por componentes do menu
      for( let component of menuComponents ) {
        // Define opção do menu, enquanto um ícone do componente alvo
        let option = menuOptions[ component.name ] = componentsMenu.addChild(
          new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.buttons[ component == visibleComponent ? 'roundRed' : 'roundGray' ] ] )
        );

        // Redimensiona opção alvo
        option.oScaleByGreaterSize( 12 );

        // Posiciona opção alvo
        option.x = previousOption ? previousOption.x + previousOption.width + marginX : 0;

        // Torna opção alvo interativa
        option.buttonMode = option.interactive = true;

        // Define texto suspenso da opção alvo
        option.hoverText = m.languages.keywords.getFrameComponent( component.name );

        // Define nome para o texto suspenso
        option.hoverTextName = `${ frame.name }-fixedContent-hover-text`;

        // Vincula componente a sua opção
        option.source = component;

        // Adição de eventos

        /// Para exibição do componente da opção
        option.addListener( 'click', frame.content.changeVisibleComponent.bind( frame.content, component ) );

        /// Para exibição de texto suspenso do ícone
        option.addListener( 'mouseover', m.app.showHoverText );

        // Atualiza última opção do menu
        previousOption = option;
      }

      // Retorna menu de componentes
      return componentsMenu;
    }

    /// Monta uma seção da moldura com uma legenda e um componente
    content.buildSingleComponentSection = function ( config = {} ) {
      // Identificadores pré-validação
      var { caption, fontSize = 0, text: textData, icon: iconData, manualSection = '' } = config;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( caption && typeof caption == 'string' );
        m.oAssert( Number.isInteger( fontSize ) );
        m.oAssert( textData?.constructor == Object );
        m.oAssert( textData.value && (
          typeof textData.value == 'string' || Array.isArray( textData.value ) && textData.value.every( data => data.constructor == Object )
        ) );
        m.oAssert( !iconData || iconData.constructor == Object && [ 'super', 'sub' ].every( key => iconData[ key ] && typeof iconData[ key ] == 'string' ) );
        m.oAssert( typeof manualSection == 'string' );
      }

      // Identificadores pós-validação
      var sectionContainer = new PIXI.Container(),
          textStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: fontSize || -16
          },
          textsArray = Array.isArray( textData.value ) ? textData.value : null,
          sectionCaption = new PIXI.BitmapText( caption, textStyle ),
          sectionText = textsArray ? new PIXI.Container() : new PIXI.BitmapText( textData.value, textStyle ),
          sectionIcon = iconData ? new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons[ iconData.super ][ iconData.sub ] ] ) : null,
          marginX = 5;

      // Inserção dos elementos

      /// Legenda e texto
      sectionContainer.addChild( sectionCaption, sectionText );

      /// Ícone
      if( sectionIcon ) sectionContainer.addChild( sectionIcon );

      /// Subtextos da seção de texto, caso ela seja um contedor
      if( textsArray )
        textsArray.forEach( function ( text, index ) {
          // Identificadores
          var textEntry = Object.entries( text )[ 0 ],
              bitmapText = new PIXI.BitmapText( textEntry[ 1 ], textStyle ),
              lastText = index ? sectionText.getChildAt( index - 1 ) : null;

          // Criação da seção de textos
          sectionText.sections ??= {};

          // Captura do trecho de texto
          sectionText.sections[ textEntry[ 0 ] ] = bitmapText;

          // Inserção do texto em seu contedor
          sectionText.addChild( bitmapText );

          // Posiciona texto em seu contedor
          bitmapText.x = lastText ? lastText.x + lastText.width : 0;
        } );

      // Redimensionamento do ícone
      sectionIcon?.oScaleByGreaterSize( 18 );

      // Posicionamento dos elementos

      /// Texto
      sectionText.position.set( sectionCaption.x + sectionCaption.width + marginX, sectionContainer.height * .5 - sectionText.height * .5 );

      /// Ícone
      sectionIcon?.position.set( sectionText.x + sectionText.width + marginX, sectionContainer.height * .5 - sectionIcon.height * .5 );

      // Captura de elementos pelo contedor
      [ sectionContainer.caption, sectionContainer.text, sectionContainer.icon ] = [ sectionCaption, sectionText, sectionIcon ];

      // Configurações em comum entre componentes
      for( let elementName of [ 'caption', 'text', 'icon' ] ) {
        // Identificadores
        let component = sectionContainer[ elementName ];

        // Filtra componentes não existentes
        if( !component ) continue;

        // Torna componente interativo
        component.interactive = true;

        // Caso uma seção do manual tenha sido passada, adiciona evento para abrir essa seção após clique no componente
        if( manualSection ) component.addListener( 'click', () => m.app.openManualSection( manualSection ) );

        // Filtra títulos, e componentes sem texto suspenso
        if( elementName == 'caption' || !( 'hoverText' in config[ elementName ] ) ) continue;

        // Define texto suspenso
        component.hoverText = config[ elementName ].hoverText;

        // Define nome para o texto suspenso
        component.hoverTextName = `${ frame.name }-toggleableContent-hover-text`;

        // Define tamanho da fonte do texto suspenso
        component.hoverTextFontSize = config[ elementName ].hoverTextFontSize || -11;

        // Adiciona evento para exibição do texto suspenso
        component.addListener( 'mouseover', m.app.showHoverText );
      }

      // Retorna contedor da seção
      return sectionContainer;
    }

    /// Monta uma seção da moldura com uma legenda e múltiplos componentes
    content.buildMultiComponentSection = function ( config = {} ) {
      // Identificadores pré-validação
      var { caption, insertInto, fontSize = 0, components: componentData, componentsFilter = () => true, iconsKey, textsCallback, manualSection = '' } = config;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( caption && typeof caption == 'string' || !caption && insertInto?.constructor == Object );
        m.oAssert( !insertInto || insertInto.section instanceof PIXI.DisplayObject && insertInto.position >= 0 && Number.isInteger( insertInto.position ) );
        m.oAssert( Number.isInteger( fontSize ) );
        m.oAssert( componentData && typeof componentData == 'object' );
        m.oAssert( [ componentsFilter, textsCallback ].every( value => typeof value == 'function' ) );
        m.oAssert( [ iconsKey, manualSection ].every( value => typeof value == 'string' ) );
      }

      // Identificadores pós-validação
      var sectionContainer = new PIXI.Container(),
          textStyle = {
            fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: fontSize || -16
          },
          sectionCaption = !insertInto ? new PIXI.BitmapText( caption, textStyle ) : insertInto.section.caption,
          componentNames = Object.keys( componentData ).filter( componentsFilter ),
          componentContainers = componentNames.map( () => new PIXI.Container() ),
          componentIcons = componentNames.map( name => new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons[ iconsKey ][ name ] ] ) ),
          componentTexts = componentNames.map( name => new PIXI.BitmapText( textsCallback( name ), textStyle ) ),
          marginX = 10;

      // Para caso conteúdo esteja sendo gerado pela primeira vez
      if( !insertInto ) {
        // Inseri legenda
        sectionContainer.addChild( sectionCaption );

        // Torna legenda interativa
        sectionCaption.interactive = true;

        // Caso uma seção do manual tenha sido passada, adiciona evento para abrir essa seção após clique na legenda
        if( manualSection ) sectionCaption.addListener( 'click', () => m.app.openManualSection( manualSection ) );
      }

      // Iteração por contedores de componentes
      for( let i = 0; i < componentContainers.length; i++ ) {
        // Captura último contedor
        let previousContainer = componentContainers[ i - 1 ];

        // Inserção dos elementos

        /// Componente em seu contedor
        componentContainers[ i ].addChild( componentTexts[ i ], componentIcons[ i ] );

        /// Contedor do componente no contedor da seção
        sectionContainer.addChild( componentContainers[ i ] );

        // Redimensionamento do ícone do componente
        componentIcons[ i ].oScaleByGreaterSize( 18 );

        // Posicionamento do componente

        /// Ícone
        componentIcons[ i ].position.set( 0, componentContainers[ i ].height * .5 - componentIcons[ i ].height * .5 );

        /// Texto
        componentTexts[ i ].position.set( componentIcons[ i ].width + 4, componentContainers[ i ].height * .5 - componentTexts[ i ].height * .5 );

        /// Contedor
        config.isToHaveCaptionAlone ?
          componentContainers[ i ].position.set(
            previousContainer ? previousContainer.position.x + previousContainer.width + marginX : 0,
            sectionCaption.y + sectionCaption.height + 2
          ) :
          componentContainers[ i ].position.x =
            previousContainer ? previousContainer.position.x + previousContainer.width + marginX :
            !insertInto ? sectionCaption.x + sectionCaption.width + marginX : 0;

        // Configurações adicionais do ícone e do texto
        for( let component of [ componentIcons[ i ], componentTexts[ i ] ] ) {
          // Nomeação
          component.name = componentNames[ i ];

          // Interatividade
          component.interactive = true;

          // Definição do nome de textos suspensos
          component.hoverTextName = `${ frame.name }-toggleableContent-hover-text`;

          // Definição do tamanho da fonte de textos suspensos
          component.hoverTextFontSize = config.hoverTextFontSize || -11;

          // Adição de evento para exibição de texto suspenso
          component.addListener( 'mouseover', m.app.showHoverText );

          // Caso uma seção do manual tenha sido passada, adiciona evento para abrir essa seção após clique no componente
          if( manualSection ) component.addListener( 'click', () => m.app.openManualSection( manualSection ) );
        }
      }

      // Armazena na seção informações de seus componentes
      [ sectionContainer.caption, sectionContainer.names ] = [ sectionCaption, componentNames ],
      [ sectionContainer.containers, sectionContainer.icons, sectionContainer.texts ] = [ componentContainers, componentIcons, componentTexts ];

      // Se aplicável, ajusta posicionamento de componente inserido após montagem da seção
      if( insertInto ) {
        // Identificadores
        let { section, position } = insertInto,
            previousContainer = section.containers[ position - 1 ],
            nextContainer = section.containers[ position ];

        // Ajusta identidade do componente anterior caso ele também tenha sido adicionado após formação da seção
        if( previousContainer && previousContainer.parent != section ) previousContainer = previousContainer.parent;

        // Inserção do novo componente na seção
        section.addChildAt( sectionContainer, position + 1 );

        // Inserção das propriedades do novo componente nas propriedades da seção
        for( let property of [ 'names', 'containers', 'icons', 'texts' ] ) section[ property ].splice( position, 0, ...sectionContainer[ property ] );

        // Ajustes de posicionamento dos componentes da seção

        /// Posicionamento do novo componente
        sectionContainer.position.x =
          previousContainer ? previousContainer.x + previousContainer.width + marginX :
          !config.isToHaveCaptionAlone ? section.caption.x + section.caption.width + marginX : 0;

        /// Para caso haja um componente após o novo
        if( nextContainer ) {
          // Posiciona componentes após o novo
          for( let i = section.containers.indexOf( nextContainer ), previousContainer = sectionContainer; i < section.containers.length; i++ ) {
            // Posiciona componente alvo
            section.containers[ i ].x = previousContainer.x + previousContainer.width + marginX;

            // Atualiza componente anterior ao próximo a ser o alvo
            previousContainer = section.containers[ i ];
          }
        }
      }

      // Retorna contedor da seção
      return sectionContainer;
    }

    /// Limpa conteúdo da moldura
    content.clear = function () {
      // Não executa função caso moldura não esteja exibindo um conteúdo
      if( !this.container ) return false;

      // Identificadores
      var targetContent = this.container,
          hoverText = m.GameScreen.current.children.find( child => child.name?.startsWith( frame.name + '-' ) && child.name?.endsWith( '-hover-text' ) );

      // Sinaliza início da saída de contedor
      m.events.frameChangeStart.containerOut.emit( frame, { targetContent: targetContent } );

      // Remove texto suspenso de moldura, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Operações em função do tipo da moldura
      switch( this.type ) {
        case 'card': {
          // Identificadores
          let displayCard = this.container,
              [ sourceCard, cardBody ] = [ displayCard.source, displayCard.body ];

          // Remove evento de aplicação de filtro de tons de cinza
          for( let eventPath of [ m.events.cardEffectEnd.disable, m.events.cardUseStart.useIn ] )
            eventPath.remove( sourceCard, cardBody.applyGrayScale, { context: cardBody } );

          // Remove eventos de remoção de filtro de tons de cinza
          for( let eventPath of [ m.events.cardEffectEnd.enable, m.events.cardUseEnd.useOut ] )
            eventPath.remove( sourceCard, cardBody.dropGrayScale, { context: cardBody } );

          // Remove eventos de atualização de dados da carta
          for( let scopeName of [ 'cardContentEnd', 'attachabilityChangeEnd', 'cardUseEnd', 'cardEffectEnd', 'conditionChangeEnd' ] )
            m.events[ scopeName ].any.remove( sourceCard, this.updateCard, { context: this } );

          // Encerra operação
          break;
        }
        case 'card-slot':
          // Remove evento de atualização de dados da casa
          m.events.attachabilityChangeEnd.attachment.remove( this.container.source, this.updateCardSlot, { context: this } );

          // Encerra operação
          break;
        case 'match-log':
          // Remove evento para atualização dos registros da partida
          m.events.logChangeEnd.any.remove( m.GameMatch.current, this.updateLog, { context: this } );
      }

      // Remove conteúdo da moldura, e o destrói
      frame.removeChild( this.container ).destroy( { children: true } );

      // Encerra relação entre conteúdo removido e moldura
      this.container = null;

      // Remove informação sobre tipo de conteúdo da moldura
      this.type = '';

      // Sinaliza fim da saída de contedor
      m.events.frameChangeEnd.containerOut.emit( frame, { targetContent: targetContent } );
    }

    /// Muda componente visível na moldura para o passado no argumento
    content.changeVisibleComponent = function ( newComponent ) {
      // Apenas executa função se em moldura existir um menu de componentes
      if( !this.container?.componentsMenu ) return false;

      // Identificadores
      var componentsMenuOptions = this.container.componentsMenu.options,
          formerComponent = Object.values( componentsMenuOptions ).find( option => option.source.visible )?.source;

      // Encerra função caso componente alvo seja igual ao já visível
      if( formerComponent == newComponent ) return false;

      // Encerra função caso componente alvo não exista no menu de componentes
      if( !Object.values( componentsMenuOptions ).some( option => option.source == newComponent ) ) return false;

      // Sinaliza início da mudança de componente
      m.events.frameChangeStart.component.emit( frame, { currentComponent: formerComponent, newComponent: newComponent } );

      // Oculta componente atualmente visível na moldura
      this.hideVisibleComponent();

      // No menu de componentes, troca ícone de botão do componente alvo
      componentsMenuOptions[ newComponent.name ].texture = PIXI.utils.TextureCache[ m.assets.images.icons.buttons.roundRed ];

      // Revela componente
      newComponent.visible = true;

      // Sinaliza fim da mudança de componente
      m.events.frameChangeEnd.component.emit( frame, { currentComponent: newComponent, formerComponent: formerComponent } );

      // Retorna componente visível
      return newComponent;
    }

    /// Oculta componente atualmente visível na moldura
    content.hideVisibleComponent = function () {
      // Apenas executa função se em moldura existir um menu de componentes
      if( !this.container?.componentsMenu ) return false;

      // Identificadores
      var componentsMenuOptions = this.container.componentsMenu.options,
          visibleComponent = Object.values( componentsMenuOptions ).find( option => option.source.visible )?.source,
          hoverText = m.GameScreen.current.children.find( child => child.name == `${ frame.name }-toggleableContent-hover-text` );

      // Encerra função caso não haja um componente visível
      if( !visibleComponent ) return false;

      // Remove texto suspenso de moldura, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );

      // Troca ícone de botão do componente no cabeçalho do contedor de informações
      componentsMenuOptions[ visibleComponent.name ].texture = PIXI.utils.TextureCache[ m.assets.images.icons.buttons.roundGray ];

      // Omite componente
      visibleComponent.visible = false;

      // Retorna componente omitido
      return visibleComponent;
    }

    /// Alterna conteúdo visível na moldura entre componente passado e seu primeiro
    content.toggleVisibleComponent = function ( component ) {
      // Apenas executa função se em moldura existir um menu de componentes
      if( !this.container?.componentsMenu ) return false;

      // Identificadores
      var componentsMenuOptions = this.container.componentsMenu.options,
          visibleComponent = Object.values( componentsMenuOptions ).find( option => option.source.visible )?.source,
          firstComponent = Object.values( componentsMenuOptions )[0].source;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( component != firstComponent );

      // Encerra função caso componente alvo não exista no menu de componentes
      if( !Object.values( componentsMenuOptions ).some( option => option.source == component ) ) return false;

      // Determina qual componente será o novo a estar visível, e o revela
      return this.changeVisibleComponent( component == visibleComponent ? firstComponent : component );
    }
  }

  // Exibe o conteúdo da carta alvo em dada moldura
  GameFrame.showCard = function ( eventData = {} ) {
    // Não executa função caso tecla 'Ctrl' esteja sendo pressionada
    if( m.app.keysPressed.ctrl ) return;

    // Identificadores
    var card = this,
        screenFrames = m.GameScreen.current.frames,
        focusedFrame = screenFrames.find( frame => frame.isInFocus ),
        targetDisplayFrame = focusedFrame ?? ( function () {
          // Identifica moldura a exibir conteúdo da carta em função da tela em que jogo está
          switch( m.GameScreen.current.name ) {
            // Tela de partidas
            case 'match-screen': {
              // Identificadores
              let owner = card.getRelationships().owner;

              // Atribui moldura em função da paridade do jogador da carta
              switch( owner.parity ) {
                case 'odd':
                  return m.GameMatch.current?.flow.step instanceof m.ArrangementStep ?
                    screenFrames[ m.GameMatch.current.isSimulation || m.GamePlayer.current?.parity == 'odd' ? 3 : 2 ] : screenFrames[ 0 ];
                case 'even':
                  return m.GameMatch.current?.flow.step instanceof m.ArrangementStep ?
                    screenFrames[ m.GameMatch.current.isSimulation || m.GamePlayer.current?.parity == 'odd' ? 2 : 3 ] : screenFrames[ 1 ];
              }
            }
            // Tela de montagem de baralhos
            case 'deck-building-screen':
              return m.GameScreen.current.infoContainer.nextDisplayFrame;
            // Tela de visualização de baralhos
            case 'deck-viewing-screen':
              return m.GameScreen.current.infoContainer.sections.cardsFrame;
            default:
              return;
          }
        } )(),
        formerComponent = targetDisplayFrame?.content.container?.info?.bodyComponents?.find( component => component.visible );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

    // Não continua função caso não tenha sido encontrada uma moldura para exibir conteúdo da carta
    if( !targetDisplayFrame ) return;

    // Para caso tela atual seja a de montagem de baralhos
    if( m.GameScreen.current.name == 'deck-building-screen' ) {
      // Identificadores
      let infoContainer = m.GameScreen.current.infoContainer,
          { sections } = infoContainer;

      // Define próxima moldura a exibir o conteúdo de uma carta
      infoContainer.nextDisplayFrame = !sections.rightFrame || targetDisplayFrame == sections.rightFrame ? sections.leftFrame : sections.rightFrame;
    }

    // Delega exibição da carta para moldura alvo
    targetDisplayFrame.content.init( card );

    // Caso tenha existido um elemento sendo exibido antes do atual, altera componente visível da moldura para o que era anteriormente visível, quando aplicável
    if( formerComponent )
      targetDisplayFrame.content.changeVisibleComponent( targetDisplayFrame.content.container.info.bodyComponents.find(
        component => component.name == formerComponent.name
      ) );
  }

  // Exibe o conteúdo da casa alvo em dada moldura
  GameFrame.showCardSlot = function ( eventData = {} ) {
    // Não executa função caso tecla 'Ctrl' não esteja sendo pressionada
    if( !m.app.keysPressed.ctrl ) return;

    // Identificadores
    var cardSlot = this,
        screenFrames = m.GameScreen.current.frames,
        focusedFrame = screenFrames.find( frame => frame.isInFocus ),
        targetDisplayFrame = focusedFrame ?? ( function () {
          // Atribui moldura em função da paridade do jogador da casa
          switch( cardSlot.owner?.parity ?? m.GamePlayer.current?.parity ) {
            case 'odd':
              return m.GameMatch.current?.flow.step instanceof m.ArrangementStep ?
                screenFrames[ m.GamePlayer.current?.parity == 'odd' ? 3 : 2 ] : screenFrames[ 0 ];
            case 'even':
            default:
              return m.GameMatch.current?.flow.step instanceof m.ArrangementStep ?
                screenFrames[ m.GamePlayer.current?.parity == 'odd' ? 2 : 3 ] : screenFrames[ 1 ];
          }
        } )();

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( cardSlot instanceof m.FieldSlot );
      m.oAssert( m.GameScreen.current instanceof m.MatchScreen );
    }

    // Não continua função caso não tenha sido encontrada uma moldura para exibir conteúdo da carta
    if( !targetDisplayFrame ) return;

    // Delega exibição da casa para moldura alvo
    targetDisplayFrame.content.init( cardSlot );
  }
}

/// Propriedades do protótipo
GameFrame.prototype = Object.create( PIXI.Graphics.prototype, {
  // Construtor
  constructor: { value: GameFrame }
} );
