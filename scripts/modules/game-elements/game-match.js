// Módulos
import * as m from '../../modules.js';

// Identificadores

/// Base do módulo
export const GameMatch = function ( config = {} ) {
  // Iniciação de propriedades
  GameMatch.init( this );

  // Identificadores pré-validação
  var { name, users, decks } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( name && typeof name == 'string' );
    m.oAssert( [ users, decks ].every( argument => Array.isArray( argument ) && argument.length == 2 ) );
    m.oAssert( users.every( user => user instanceof m.GameUser ) );
    m.oAssert( decks.every( ( deck, index ) => deck instanceof m.MainDeck && deck.owner == users[ index ] ) );
  }

  // Identificadores pós-validação
  var matchNumber = Number( name.replace( /\D/g, '' ) ),
      oddUser = users[ 0 ],
      evenUser = users[ 1 ];

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  this.name = name;

  /// Identifica se partida é ou não uma simulação
  this.isSimulation = users[ 0 ].name == users[ 1 ].name;

  /// Configuração dos jogadores
  configPlayers: {
    // Gera jogadores da partida
    Object.assign( this.players, {
      odd: new m.GamePlayer( { parity: 'odd', user: oddUser } ),
      even: new m.GamePlayer( { parity: 'even', user: evenUser } )
    } );

    // Caso partida não seja uma simulação, atualiza quantidade total de partidas dos jogadores
    if( !this.isSimulation )
      for( let user of users ) user.matches.total++, user.matches.unfinished++;
  }

  /// Configuração dos baralhos
  configDecks: {
    // Gera baralhos da partida
    Object.assign( this.decks, {
      odd: decks.find( deck => deck.owner == this.players.odd.user ).copy( {
        owner: this.players.odd, includeName: true, includeIsStarter: true, includeDate: true, includeMatches: true
      } ),
      even: decks.findLast( deck => deck.owner == this.players.even.user ).copy( {
        owner: this.players.even, includeName: true, includeIsStarter: true, includeDate: true, includeMatches: true
      } )
    } );

    // Caso partida não seja uma simulação, atualiza quantidade total de partidas dos baralhos
    if( !this.isSimulation )
      for( let deck of decks ) deck.matches.total++, deck.matches.unfinished++;
  }

  /// Configuração dos ambientes
  configEnvironments: {
    // Gera campo
    this.environments.field = new m.Field( {
      owners: { odd: this.players.odd, even: this.players.even },
      upperGrid: oddUser == m.GameUser.current ? 'even' : 'odd'
    } );

    // Gera róis
    for( let parity of [ 'odd', 'even' ] ) this.environments.pools[ parity ] = new m.Pool( { owner: this.players[ parity ] } );
  }

  // Para facilitar depuração através de relatórios dos testadores
  if( !this.isSimulation ) console.log( `The name of this match is "${ this.name }".` );
}

/// Propriedades do construtor
defineProperties: {
  // Partida atual
  GameMatch.current = null;

  // Execução atual de 'GameMatch.executeFateBreak'
  GameMatch.fateBreakExecution = null;

  /// Indica se uma partida está sendo procurada
  GameMatch.isInMatchmaking = false;

  // Iniciação de propriedades
  GameMatch.init = function ( match ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( match instanceof GameMatch );

    // Atribuição de propriedades iniciais

    /// Nome
    match.name = '';

    /// Indica se partida é apenas uma simulação
    match.isSimulation = false;

    /// Estatísticas da partida
    match.stats = {};

    /// Jogadores
    match.players = {};

    /// Baralhos
    match.decks = {};

    /// Ambientes
    match.environments = {};

    /// Fluxo
    match.flow = {};

    /// Objeto para registro de eventos na partida
    match.log = {};

    /// Indica se existe alguma operação programada para abortar esta partida
    match.abortTimeout = null;

    /// Inicia uma partida
    match.begin = function () {
      // Não executa função caso rodada da partida já esteja ativa
      if( this.flow.round.isActive ) return false;

      // Caso exista uma partida atual que seja diferente desta, encerra-a
      if( GameMatch.current && GameMatch.current != this ) GameMatch.current.finish();

      // Define esta partida como a atual
      GameMatch.current = this;

      // Define jogador atual como o igual ao usuário atual
      m.GamePlayer.current = this.players.get( 'all' ).find( player => player.user == m.GameUser.current ) ?? null;

      // Dispõe ambientes
      this.environments.arrange();

      // Configura baralhos
      this.decks.config();

      // Dispõe baralhos
      this.decks.arrange();

      // Configura ambientes
      this.environments.config();

      // Inicia rodada
      this.flow.round.begin();

      // Para caso usuário seja um dos jogadores da partida e ela não seja uma simulação
      if( m.GamePlayer.current && !this.isSimulation ) {
        // Identificadores
        let dataPath = `matches.${ this.name }.playerParity`;

        // Caso usuário seja o ímpar, atualiza servidor sobre estado da partida e de jogadores
        if( m.GamePlayer.current.parity == 'odd' ) this.updateServer( true );

        // Envia evento para atualização da paridade do jogador no servidor
        m.sockets.current.emit( 'set-data', { [ dataPath ]: m.GamePlayer.current.parity } );

        // Indica paridade do jogador vinculado ao usuário
        m.noticeBar.show( m.languages.notices.getMiscText( 'player-parity', { parity: m.GamePlayer.current.parity } ), 'green' );
      }

      // Adiciona evento para controlar avanço do fluxo
      window.addEventListener( 'keydown', this.flow.controlProgress );

      // Retorna partida
      return this;
    }

    /// Conclui uma partida
    match.finish = function () {
      // Não executa função caso esta partida não seja a atual
      if( GameMatch.current != this ) return false;

      // Encerra rodada
      this.flow.round.finish();

      // Limpa baralhos
      this.decks.clear();

      // Desconfigura baralhos
      this.decks.unconfig();

      // Desconfigura ambientes
      this.environments.unconfig();

      // Limpa conteúdo de molduras na tela
      for( let frame of m.GameScreen.current.frames ) frame.content.clear();

      // Remove evento para controlar avanço do fluxo
      window.removeEventListener( 'keydown', this.flow.controlProgress );

      // Nulifica partida, e seu jogador atual
      GameMatch.current = m.GamePlayer.current = null;

      // Retorna partida
      return this;
    }

    /// Atualiza servidor sobre estatísticas da partida e de seus jogadores
    match.updateServer = async function ( isFirstUpdate = false ) {
      // Não executa função caso partida seja uma simulação
      if( this.isSimulation ) return false;

      // Não executa função caso usuário não seja um dos jogadores da partida
      if( !m.GamePlayer.current ) return false;

      // Identificadores
      var matchPlayers = this.players.get( 'all' ),
          matchData = {
            'stats': this.stats,
            'log': this.log.formatToServer()
          };

      // Caso seja primeira atualização para o servidor, inclui informação sobre baralhos a serem usados na partida
      if( isFirstUpdate )
        for( let parity of [ 'odd', 'even' ] ) matchData[ `players.${ parity }.gameDeck` ] = this.decks[ parity ].toObject();

      // Salva no servidor estatísticas da partida
      m.app.sendHttpRequest( {
        path: `/actions/update-match${ location.search }`,
        method: 'PATCH',
        headers: [ [ 'content-type', 'application/json' ] ],
        body: JSON.stringify( {
          match: this.name,
          dataObject: matchData
        } ),
        isSilentError: true
      } );

      // Itera por jogadores da partida
      for( let player of matchPlayers ) {
        // Identificadores
        let userDeck = player.user.decks.find( deck => deck.name == player.gameDeck.name );

        // Salva no servidor situação atual de partidas do jogador alvo
        await m.app.sendHttpRequest( {
          path: `/actions/update-user${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ] ],
          body: JSON.stringify( {
            user: player.user.name,
            dataObject: { 'matches': player.user.matches }
          } ),
          isSilentError: true
        } );

        // Salva no servidor situação atual de partidas do baralho do jogador alvo
        await m.app.sendHttpRequest( {
          path: `/actions/update-deck${ location.search }`,
          method: 'PATCH',
          headers: [ [ 'content-type', 'application/json' ] ],
          body: JSON.stringify( {
            user: player.user.name,
            deck: userDeck.toObject(),
            index: player.user.decks.indexOf( userDeck )
          } ),
          isSilentError: true
        } );
      }
    }

    // Configura um ente gerado no decorrer de uma partida
    match.configBeing = function ( eventData = {} ) {
      // Identificadores
      var { flow: matchFlow } = this,
          { eventCategory, eventType, summon: being } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( !eventCategory || eventCategory == 'summon-change' );
        m.oAssert( !eventType || eventType == 'summon-in' );
      }

      // Gera símbolo do ente
      being.body.buildSymbol();

      // Para caso ente seja uma ficha controlável
      if( being.isToken && !being.isUncontrollable ) {
        // Identificadores
        let { infoBar } = m.GameScreen.current;

        // Caso ainda não exista, adiciona evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da atividade
        if( !being.listeners( 'card-activity-end-any' ).some( listener => listener == infoBar.updateReadinessData ) )
          m.events.cardActivityEnd.any.add( being, infoBar.updateReadinessData, { context: infoBar } );

        // Caso ainda não exista, adiciona evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da disponibilidade
        if( !being.listeners( 'being-readiness-end-any' ).some( listener => listener == infoBar.updateReadinessData ) )
          m.events.beingReadinessEnd.any.add( being, infoBar.updateReadinessData, { context: infoBar } );
      }

      // Caso período atual não seja o do combate ou o da remobilização, encerra função
      if( ![ m.CombatPeriod, m.RedeploymentPeriod ].some( constructor => matchFlow.period instanceof constructor ) ) return;

      // Define ações do ente
      matchFlow.period.setActions( { targetCards: [ being ] } );

      // Caso período atual não seja o do combate, encerra função
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return;

      // Para configurar jogada do ente
      setupMove: {
        // Não executa bloco caso ente não esteja ativo
        if( being.activity != 'active' ) break setupMove;

        // Identificadores
        let ownerParity = being.getRelationships().owner.parity,
            beingSegmentNumber = being.content.stats.attributes.current.initiative,
            moveSegment = matchFlow.period.getChild( `segment-${ beingSegmentNumber }` ),
            moveParity = moveSegment.getChild( `${ ownerParity }-parity` );

        // Gera jogada para o ente, e a adiciona ao palco do fluxo atual
        let move = new m.FlowMove( { parent: moveParity, source: being } ).add();

        // Para caso segmento de preparo do ente já tenha se passado, ou seja o atual e paridade do ente já tenha se passado
        if( !moveParity.checkIfIsAfter( matchFlow.parity, matchFlow.period ) ) {
          // Abre jogada
          move.isOpen = true;

          // Atualiza disponibilidade do ente
          being.readiness.update();
        }
      }
    }

    // Atribuição de propriedades de 'match.stats'
    let matchStats = match.stats;

    /// Indica se partida foi concluída
    matchStats.isFinished = false;

    /// Indica vencedor da partida
    matchStats.winner = '';

    // Atribuição de propriedades de 'players'
    let players = match.players;

    /// Ímpar
    players.odd = null;

    /// Par
    players.even = null;

    /// Retorna um ou mais jogadores, conforme argumento passado
    players.get = function ( playerType ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'odd', 'even', 'flow-controller', 'opponent', 'all' ].includes( playerType ) );

      // Retorna jogador(es) em função de 'playerType'
      switch( playerType ) {
        // Para o ímpar ou o par
        case 'odd':
        case 'even':
          return this[ playerType ];
        // Para o controlador do fluxo
        case 'flow-controller':
          return this[ match.flow.parity.parity ];
        // Para o oponente do controlador do fluxo
        case 'opponent':
          return this[ match.flow.parity.parity == 'odd' ? 'even' : 'odd' ];
        // Para todos os jogadores
        case 'all':
          return [ this.odd, this.even ];
      }
    }

    /// Alterna jogador atual do usuário da partida, de modo que ele seja sempre igual ao controlador do fluxo
    players.toggleCurrent = function ( eventData = {} ) {
      // Apenas executa função caso partida seja uma simulada
      if( !match.isSimulation ) return false;

      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.FlowParity );

      // Define jogador atual como o cuja paridade corresponder com a do estágio atual
      m.GamePlayer.current = this[ eventTarget.parity ];
    }

    /// Adiciona pontos de destino intransitivos pré-definidos
    players.addDefaultFatePoints = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget.name.startsWith( 'pre-combat' ) );

      // Concede a jogadores pontos de destino intransitivos
      for( let parity of [ 'odd', 'even' ] ) this[ parity ].fatePoints.increase( 2 );
    }

    /// Limpa pontos de destino
    players.clearFatePoints = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: stage } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' );
        m.oAssert( [ m.FlowRound, m.BattleTurn ].some( constructor => stage instanceof constructor ) );
      }

      // Zera pontos de destino de jogadores
      for( let parity of [ 'odd', 'even' ] )
        this[ parity ].fatePoints.decrease( stage instanceof m.FlowRound ? this[ parity ].fatePoints.max : this[ parity ].fatePoints.current.intransitive );
    }

    // Atribuição de propriedades de 'decks'
    let decks = match.decks;

    /// Ímpar
    decks.odd = null;

    /// Par
    decks.even = null;

    /// Retorna todas as cartas em baralhos, e suas fichas
    decks.getAllCards = function ( config = {} ) {
      // Identificadores
      var cardsArray = [];

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( Object.keys( config ).every( key => [ 'skipOdd', 'skipEven', 'skipAssets', 'skipTokens', 'skipSideDeck' ].includes( key ) ) );

      // Adiciona cartas da paridade alvo ao arranjo de cartas, se desejado
      for( let parity of [ 'odd', 'even' ] )
        if( !config[ 'skip' + parity[ 0 ].toUpperCase() + parity.slice( 1 ) ] )
          cardsArray = cardsArray.concat( this[ parity ].cards, config.skipSideDeck || !this[ parity ].sideDeck ? [] : this[ parity ].sideDeck.cards );

      // Adiciona fichas ao arranjo de cartas, se desejado
      if( !config.skipTokens ) cardsArray = cardsArray.concat( cardsArray.flatMap( card => card.summons ?? [] ) );

      // Filtra ativos do arranjo de cartas, se desejado
      if( config.skipAssets ) cardsArray = cardsArray.filter( card => card instanceof m.Being );

      // Retorna arranjo de cartas
      return cardsArray;
    }

    /// Configura cartas de baralhos
    decks.config = function ( cards = this.getAllCards( { skipTokens: true } ) ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current instanceof m.MatchScreen );

      // Identificadores
      var { infoBar } = m.GameScreen.current;

      // Itera por cartas alvo
      for( let card of cards ) {
        // Identificadores
        let mainDeck = card.getRelationships().owner.gameDeck;

        // Adiciona evento para atualizar arranjos de atividade do baralho do jogo em função de mudança de atividade da carta
        m.events.cardActivityEnd.any.add( card, mainDeck.updateActivityLists, { context: mainDeck } );

        // Adiciona evento para atualizar arranjo de uso do baralho do jogo em função de mudança de uso da carta
        m.events.cardUseEnd.useInOut.add( card, mainDeck.updateUseList, { context: mainDeck } );

        // Adiciona evento para, na barra de informações, atualizar seção de dados do baralho segundo mudanças de atividade
        m.events.cardActivityEnd.any.add( card, infoBar.updateDeckData, { context: infoBar } );

        // Adiciona evento para, na barra de informações, atualizar seção de dados do baralho segundo mudanças de uso
        m.events.cardUseEnd.useInOut.add( card, infoBar.updateDeckData, { context: infoBar } );

        // Adiciona evento para atualizar estilo de casa de uma carta conforme atividade desta
        m.events.cardActivityEnd.any.add( card, m.CardSlot.updateSlotStyle, { context: m.CardSlot } );

        // Para caso carta seja um ente
        if( card instanceof m.Being ) {
          // Adiciona evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da atividade
          m.events.cardActivityEnd.any.add( card, infoBar.updateReadinessData, { context: infoBar } );

          // Adiciona evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da disponibilidade
          m.events.beingReadinessEnd.any.add( card, infoBar.updateReadinessData, { context: infoBar } );
        }

        // Do contrário
        else {
          // Adiciona evento para atualizar estilo de casa de uma carta conforme uso desta
          m.events.cardUseEnd.useInOut.add( card, m.CardSlot.updateSlotStyle, { context: m.CardSlot } );
        }

        // Adiciona evento para configurar fichas convocadas
        m.events.summonChangeEnd.summonIn.add( card, match.configBeing, { context: match } );

        // Quando aplicável, adiciona à partida configurações próprias da carta
        card.configMatch?.();
      }
    }

    /// Desconfigura cartas de baralhos
    decks.unconfig = function ( cards = this.getAllCards( { skipTokens: true } ) ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameScreen.current instanceof m.MatchScreen );

      // Identificadores
      var { infoBar } = m.GameScreen.current;

      // Itera por cartas alvo
      for( let card of cards ) {
        // Identificadores
        let mainDeck = card.getRelationships().owner.gameDeck;

        // Remove evento para atualizar arranjos de atividade do baralho do jogo em função de mudança de atividade da carta
        m.events.cardActivityEnd.any.remove( card, mainDeck.updateActivityLists, { context: mainDeck } );

        // Remove evento para atualizar arranjo de uso do baralho do jogo em função de mudança de uso da carta
        m.events.cardUseEnd.useInOut.remove( card, mainDeck.updateUseList, { context: mainDeck } );

        // Remove evento para, na barra de informações, atualizar seção de dados do baralho segundo mudanças de atividade
        m.events.cardActivityEnd.any.remove( card, infoBar.updateDeckData, { context: infoBar } );

        // Remove evento para, na barra de informações, atualizar seção de dados do baralho segundo mudanças de uso
        m.events.cardUseEnd.useInOut.remove( card, infoBar.updateDeckData, { context: infoBar } );

        // Remove evento para atualizar estilo de casa de uma carta conforme atividade desta
        m.events.cardActivityEnd.any.remove( card, m.CardSlot.updateSlotStyle, { context: m.CardSlot } );

        // Para caso carta seja um ente
        if( card instanceof m.Being ) {
          // Remove evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da atividade
          m.events.cardActivityEnd.any.remove( card, infoBar.updateReadinessData, { context: infoBar } );

          // Remove evento para, na barra de informações, atualizar seção de dados da disponibilidade da carta segunda mudança da disponibilidade
          m.events.beingReadinessEnd.any.remove( card, infoBar.updateReadinessData, { context: infoBar } );
        }

        // Do contrário
        else {
          // Remove evento para atualizar estilo de casa de uma carta conforme uso desta
          m.events.cardUseEnd.useInOut.remove( card, m.CardSlot.updateSlotStyle, { context: m.CardSlot } );
        }

        // Remove evento para configurar fichas convocadas
        m.events.summonChangeEnd.summonIn.remove( card, match.configBeing, { context: match } );

        // Quando aplicável, desfaz configurações próprias da carta
        card.unconfigMatch?.();
      }
    }

    /// Dispõe baralhos em seus róis
    decks.arrange = function () {
      // Identificadores
      var cards = this.getAllCards( { skipTokens: true } );

      // Itera por cartas
      for( let card of cards ) {
        // Coloca carta em seu respectivo ambiente do rol
        match.environments.pools[ card.deck.owner.parity ].put( card );

        // Suspende cartas no baralho principal
        if( card.deck instanceof m.MainDeck ) card.inactivate( 'suspended' );
      }
    }

    /// Remove dos ambientes da partida todas as cartas de baralhos
    decks.clear = function () {
      // Remoção das cartas do campo
      match.environments.field.clear();

      // Remoção das cartas dos róis
      for( let parity of [ 'odd', 'even' ] ) match.environments.pools[ parity ].clear();
    }

    // Atribuição de propriedades de 'environments'
    let environments = match.environments;

    /// Campo
    environments.field = null;

    /// Róis
    environments.pools = {};

    /// Configura ambientes da partida
    environments.config = function () {
      // Identificadores
      var fieldSlots = this.field.getSlots(),
          engagementZones = this.field.divider.slots;

      // Itera por casas do campo
      for( let slot of fieldSlots ) {
        // Adiciona evento de verificação de disponibilidade de ação 'Expelir'
        m.events.attachabilityChangeEnd.attachment.add( slot, m.ActionExpel.checkAvailability, { context: m.ActionExpel } );
      }

      // Itera por zonas de engajamento
      for( let engagementZone of engagementZones ) {
        // Identificadores
        let adjacentGridSlots = engagementZone.getAdjacentGridSlots();

        // Adiciona eventos para verificações de disponibilidade de ações em função de mudanças de ocupantes na zona de engajamento alvo
        for( let action of [ m.ActionEngage, m.ActionDisengage, m.ActionChannel, m.ActionRetreat ] )
          m.events.fieldSlotCardChangeEnd.any.add( engagementZone, action.checkAvailability, { context: action } );

        // Para casas da grade do campo adjacentes à zona de engajamento alvo, adiciona verificação para 'Desengajar'
        adjacentGridSlots.forEach(
          slot => m.events.fieldSlotCardChangeEnd.any.add( slot, m.ActionDisengage.checkAvailability, { context: m.ActionDisengage } )
        );
      }
    }

    /// Dispõe na tela atual ambientes da partida
    environments.arrange = function () {
      // Identificadores
      var { infoBar } = m.GameScreen.current,
          { field, pools } = environments;

      // Inseri ambientes na tela
      m.GameScreen.current.addChild( field, pools.odd, pools.even );

      // Posiciona ambientes na tela

      /// Campo
      field.position.set(
        m.app.pixi.screen.width * .5 - field.width * .5, infoBar.height + ( m.app.pixi.screen.height - infoBar.height ) * .5 - field.height * .5
      );

      /// Rol ímpar
      pools.odd.position.set(
        0, infoBar.height + ( m.app.pixi.screen.height - infoBar.height ) * .5 - pools.odd.height * .5
      );

      /// Rol par
      pools.even.position.set(
        m.app.pixi.screen.width - pools.even.width, infoBar.height + ( m.app.pixi.screen.height - infoBar.height ) * .5 - pools.even.height * .5
      );
    }

    /// Desconfigura ambientes da partida
    environments.unconfig = function () {
      // Identificadores
      var fieldSlots = this.field.getSlots(),
          engagementZones = this.field.divider.slots;

      // Itera por casas do campo
      for( let slot of fieldSlots ) {
        // Remove evento de verificação de disponibilidade de ação 'Expelir'
        m.events.attachabilityChangeEnd.attachment.remove( slot, m.ActionExpel.checkAvailability, { context: m.ActionExpel } );
      }

      // Itera por zonas de engajamento
      for( let engagementZone of engagementZones ) {
        // Identificadores
        let adjacentGridSlots = engagementZone.getAdjacentGridSlots();

        // Remove eventos para verificações de disponibilidade de ações em função de mudanças de ocupantes na zona de engajamento alvo
        for( let action of [ m.ActionEngage, m.ActionDisengage, m.ActionChannel, m.ActionRetreat ] )
          m.events.fieldSlotCardChangeEnd.any.remove( engagementZone, action.checkAvailability, { context: action } );

        // Para casas da grade do campo adjacentes à zona de engajamento alvo, remove verificação para 'Desengajar'
        adjacentGridSlots.forEach(
          slot => m.events.fieldSlotCardChangeEnd.any.remove( slot, m.ActionDisengage.checkAvailability, { context: m.ActionDisengage } )
        );
      }
    }

    // Atribuição de propriedades de 'environments.pools'
    let pools = environments.pools;

    /// Ímpar
    pools.odd = null;

    /// Par
    pools.even = null;

    // Atribuição de propriedades de 'flow'
    let flow = match.flow;

    /// Rodada da partida
    flow.round = new m.FlowRound();

    /// Lista de jogadas no palco do fluxo atual
    flow.moves = [];

    /// Indica se existe alguma operação programada para avançar o fluxo
    flow.advanceTimeout = null;

    /// Cronômetro para o avanço padrão do fluxo
    flow.moveTimer = 0;

    /// Indica se fluxo está suspenso
    flow.isSuspended = false;

    /// Indica se fluxo pode ser suspenso por usuário
    flow.isSuspendable = false;

    /// Abre jogadas, e prepara seus entes
    flow.openMoves = function ( eventData = {} ) {
      // Identificadores
      var { decks: matchDecks, environments: matchEnvironments } = match,
          { eventCategory, eventType, eventTarget: parityStage } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && parityStage instanceof m.FlowParity );

      // Identifica se jogadas do estágio alvo devem ser abertas segundo a etapa
      switch( this.step.name ) {
        case 'arrangement-step': {
          // Identificadores
          let isWithValidSideDeck = Boolean( matchDecks[ parityStage.parity ].sideDeck?.cards.some( card => !card.content.stats?.command ) );

          // Não abre jogadas da etapa do arranjo caso seu controlador não tenha um baralho coadjuvante válido
          if( !isWithValidSideDeck ) return false;

          // Encerra operação
          break;
        }
        case 'deployment-step': {
          // Identificadores
          let isWithFullFieldGrid = matchEnvironments.field.grids[ parityStage.parity ].slots.every( slot => slot.card ),
              isWithoutFullCommitters = matchDecks[ parityStage.parity ].cards.beings.every(
                being => !being.actions.some( action => action.commitment.subtype == 'full' )
              );

          // Não abre jogadas da etapa da mobilização caso a grade do campo do controlador esteja completamente ocupada ou ele não possa acionar ações plenas
          if( isWithFullFieldGrid || isWithoutFullCommitters ) return false;

          // Encerra operação
          break;
        }
        case 'allocation-step': {
          // Identificadores
          let { beings: deckBeings, items: deckItems, spells: deckSpells } = matchDecks[ parityStage.parity ].cards,
              isEquipDoable =
                deckBeings.some( card => card.actions.some( action => action instanceof m.ActionEquip ) ) &&
                deckItems.some( card => card.activity == 'suspended' ),
              isChannelDoable =
                deckBeings.some( card => card.actions.some( action => action instanceof m.ActionChannel ) ) && deckSpells.some( card =>
                  [ 'suspended', 'active' ].includes( card.activity ) && !card.isInUse && card.content.typeset.duration == 'persistent'
                );

          // Não abre jogadas da etapa da alocação caso não seja possível ao controlador acionar nem a ação 'equipar', nem a ação 'canalizar'
          if( !isEquipDoable && !isChannelDoable ) return false;

          // Encerra operação
          break;
        }
      }

      // Itera por jogadas na paridade alvo
      for( let move of parityStage.children ) {
        // Filtra jogadas que já estejam abertas, ou que já tenham sido concluídas
        if( move.isOpen || move.development >= 1 ) continue;

        // Abre jogada alvo
        move.isOpen = true;

        // Caso se esteja no período do combate, atualiza disponibilidade de seu ente
        if( this.period instanceof m.CombatPeriod ) move.source.readiness.update();
      }
    }

    /// Atualiza sincronização do fluxo
    flow.updateSynchronization = function ( eventData = {} ) {
      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != this ) return false;

      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.FlowParity );

      // Caso partida seja uma simulação, avança o fluxo automaticamente
      if( match.isSimulation ) return setTimeout( this.advance.bind( this ) );

      // Caso partida esteja nos períodos intersticiais, avança o fluxo automaticamente
      if( this.period instanceof m.BreakPeriod ) return setTimeout( this.advance.bind( this ) );

      // Monta barra sobre notificação de aguardo de sincronização do fluxo da partida
      new m.StaticBar( {
        name: 'synchronizing-flow',
        text: m.languages.notices.getFlowText( 'synchronizing-flow' )
      } ).insert();

      // Indica que socket está preparado para avançar no fluxo da partida
      m.sockets.current.emit( 'progress-flow', { matchName: match.name } );
    }

    /// Avança fluxo, em alguns casos após certo intervalo
    flow.advance = function () {
      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != this ) return;

      // Não executa função caso fluxo já esteja programado para ser avançado
      if( this.advanceTimeout ) return;

      // Identificadores
      var currentParity = this.parity.parity,
          flowChain = this.toChain();

      // Caso haja ações a serem progredidas, realiza essa progressão
      if( m.GameAction.progressQueue.length ) return ( m.GameAction.progressExecution = m.GameAction.progress() ).next();

      // Caso partida esteja nos períodos intersticiais, programa avanço imediato do fluxo
      if( this.period instanceof m.BreakPeriod ) return this.advanceTimeout = setTimeout( progressFlow.bind( this ) );

      // Para caso usuário não seja o controlador do fluxo
      if( m.GamePlayer.current != GameMatch.current.players.get( 'flow-controller' ) ) {
        // Caso fluxo não esteja suspenso e barra de intermissão não exista, monta-a
        if( !this.isSuspended && !m.GameScreen.current.children.find( child => child.name == 'intermission-static-bar' ) )
          new m.StaticBar( {
            name: 'intermission',
            text: m.languages.notices.getFlowText( 'flow-intermission', { parity: currentParity } )
          } ).insert();

        // Encerra execução da função
        return;
      }

      // Em relação ao controlador do fluxo, identifica se existem jogadas abertas ou ações em andamento
      let isWithOpenMoves = this.moves.some( checkOpenness ),
          isWithActionsInProgress = m.GameAction.currents.some( action =>
            action.committer instanceof m.Being && action.committer.getRelationships().controller.parity == currentParity
          );

      // Caso não haja nenhuma jogada aberta para o jogador da paridade alvo e ele não tenha ações em andamento, programa avanço imediato do fluxo
      if( !isWithOpenMoves && !isWithActionsInProgress ) return this.advanceTimeout = setTimeout( progressFlow.bind( this ) );

      // Identifica se existem jogadas abertas na paridade atual
      let currentParityMoves = this.parity.parent.children.flatMap( parityStage => parityStage.children ).filter( checkOpenness );

      // Para caso não haja nenhuma jogada aberta para o jogador da paridade alvo na paridade atual
      if( !currentParityMoves.length ) {
        // Indica que fluxo pode ser suspenso por usuário
        this.isSuspendable = true;

        // Avança o fluxo após pequeno intervalo
        return this.advanceTimeout = setTimeout( progressFlow.bind( this ), 500 );
      }

      // Suspende fluxo
      this.suspend();

      // Caso partida seja uma real, propaga interação com o fluxo para os outros sockets na partida
      if( !match.isSimulation )
        m.sockets.current.emit( 'control-flow-progress', {
          matchName: match.name,
          data: { flowChain: flowChain, method: 'suspend' }
        } );

      // Funções

      /// Verifica se jogada alvo pode ser acionada na paridade atual
      function checkOpenness( move ) {
        return move.isOpen && ( move.source.parity == currentParity || move.source.getRelationships?.().controller.parity == currentParity );
      }

      /// Progride fluxo da partida após intervalo
      function progressFlow() {
        // Identificadores
        var isInBreakPeriod = this.period instanceof m.BreakPeriod;

        // Limpa programação para avanço do fluxo
        this.advanceTimeout = null;

        // Indica que fluxo não pode ser suspenso por usuário
        this.isSuspendable = false;

        // Tenta progredir fluxo, e caso progressão não tenha sido bem-sucedida, encerra execução da função
        if( !this.progress() ) return;

        // Caso partida seja real e usuário não [esteja/tenha saído] de um período intersticial, propaga interação com o fluxo para os outros sockets na partida
        if( !match.isSimulation && !isInBreakPeriod )
          m.sockets.current.emit( 'control-flow-progress', {
            matchName: match.name,
            data: { flowChain: flowChain, method: 'progress' }
          } );
      }
    }

    /// Avança fluxo, imediatamente
    flow.progress = function () {
      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != this ) return false;

      // Não executa função caso partida não esteja ativa
      if( !this.parity ) return false;

      // Não executa função caso ainda haja ações no arranjo de ações a serem executadas ou progredidas
      for( let key of [ 'executionQueue', 'progressQueue' ] )
        if( m.GameAction[ key ].length ) return false;

      // Caso haja paradas em andamento, executa esta função novamente após término de alguma
      if( GameMatch.getActiveBreaks().length )
        return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( flow.progress.bind( this ), m.app.timeoutDelay.average ), { once: true } );

      // Identificadores
      var parityTimer = m.GameScreen.current.timers[ this.parity.parity ],
          requiredMove = this.moves[ this.parity.parity ].find( move => move.isRequired && move.isOpen );

      // Limpa programação para avanço do fluxo, caso exista
      this.advanceTimeout &&= clearTimeout( this.advanceTimeout ) ?? null;

      // Não avança o fluxo caso exista 1 ou mais jogadas obrigatórias para o controlador atual
      if( requiredMove )
        return m.noticeBar.show( requiredMove.requiredMessage || m.languages.notices.getFlowText( 'finish-required-move' ) );

      // Para caso uma modal esteja ativa
      if( m.GameModal.current ) {
        // Remove eventuais modais na fila de modais a serem inseridas
        while( m.GameModal.queue.length ) m.GameModal.queue.pop().destroy( { children: true } );

        // Remove modal atual da tela
        m.GameModal.current.remove();

        // Indica que fluxo avançou
        m.noticeBar.show( m.languages.notices.getFlowText( 'flow-advanced' ), 'green', 'fast' );
      }

      // Caso fluxo esteja suspenso, emite som de fluxo liberado
      if( this.isSuspended ) m.assets.audios.soundEffects[ 'ping' ].play();

      // Indica que fluxo não está suspenso
      this.isSuspended = false;

      // Caso cronômetros já existam, inativa o da paridade atual, quando ativo
      if( parityTimer ) m.GameScreen.current.timers.inactivate( parityTimer );

      // Avança fluxo para próxima paridade
      return this.parity.parent.progress();
    }

    /// Suspende fluxo
    flow.suspend = function () {
      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != this ) return false;

      // Não executa função caso partida não esteja ativa
      if( !this.parity ) return false;

      // Não executa função caso fluxo já esteja suspenso
      if( this.isSuspended ) return false;

      // Identificadores
      var parityTimer = m.GameScreen.current.timers[ this.parity.parity ];

      // Limpa programação para avanço do fluxo, caso exista
      this.advanceTimeout &&= clearTimeout( this.advanceTimeout ) ?? null;

      // Indica que fluxo está suspenso
      this.isSuspended = true;

      // Indica que fluxo não pode ser suspenso por usuário
      this.isSuspendable = false;

      // Caso partida não tenha acabado de começar, emite som de fluxo suspenso
      if( this.step.name != 'arrangement-step' || this.turn.counter != 1 || this.parity.parity != 'odd' ) m.assets.audios.soundEffects[ 'ping' ].play();

      // Caso cronômetros já existam, ativa o da paridade atual, quando inativo
      if( parityTimer ) m.GameScreen.current.timers.activate( parityTimer );

      // Caso cronômetro para avanço do fluxo esteja habilitado, avança-o após chegar ao fim
      if( this.moveTimer ) return this.advanceTimeout = setTimeout( this.progress.bind( this ), this.moveTimer );

      // Retorna fluxo
      return this;
    }

    /// Controla avanço do fluxo
    flow.controlProgress = function ( event ) {
      // Apenas executa função caso teclas pressionadas sejam válidas
      if( ![ 'Enter', 'Space' ].includes( event.code ) ) return;

      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != flow ) return;

      // Não executa função caso usuário não seja o controlador do fluxo
      if( m.GamePlayer.current != GameMatch.current.players.get( 'flow-controller' ) ) return;

      // Não executa função caso haja uma parada em andamento
      if( GameMatch.getActiveBreaks().length ) return;

      // Não executa função caso haja uma barra estática sendo exibida
      if( m.GameScreen.current.children.some( child => child.name?.endsWith( '-static-bar' ) && child.visible ) ) return;

      // Identifica se fluxo deve ser liberado ou suspenso
      let operationMethod = event.code == 'Enter' ? 'progress' : 'suspend';

      // Não executa função caso fluxo já esteja no estado desejado
      if( operationMethod == 'progress' && !flow.isSuspended || operationMethod == 'suspend' && flow.isSuspended ) return;

      // Não executa suspensão do fluxo caso ele não possa ser suspenso por usuário
      if( operationMethod == 'suspend' && !flow.isSuspendable ) return;

      // Captura cadeia de fluxo atual
      let flowChain = flow.toChain();

      // Tenta liberar ou suspender o fluxo, e captura resultado da tentativa
      let operationResult = flow[ operationMethod ]();

      // Para caso operação com fluxo tenha sido realizada com sucesso
      if( operationResult ) {
        // Caso partida seja uma real, propaga interação com o fluxo para os outros sockets na partida
        if( !match.isSimulation )
          m.sockets.current.emit( 'control-flow-progress', {
            matchName: match.name,
            data: { flowChain: flowChain, method: operationMethod }
          } );

        // Notifica usuário que fluxo foi liberado ou suspenso
        m.noticeBar.show( m.languages.notices.getFlowText( operationMethod == 'progress' ? 'flow-released' : 'flow-suspended' ), 'green', 'fast' );
      }

      // Para caso operação com fluxo não tenha sido realizada com sucesso
      else {
        // Emite som de ação negada
        m.assets.audios.soundEffects[ 'click-deny' ].play();

        // Caso uma mensagem sobre impedimento da operação desejada ainda não tenha sido enviada ao usuário, lança mensagem genérica
        if( operationResult === false )
          m.noticeBar.show( m.languages.notices.getFlowText( operationMethod == 'progress' ? 'flow-release-failure' : 'flow-suspension-failure' ) );
      }
    }

    /// Condensa fluxo atual da partida em um texto a representar uma cadeia das unidades de fluxo atuais
    flow.toChain = function () {
      // Não executa função caso partida lhe relativa não exista mais
      if( GameMatch.current?.flow != this ) return false;

      // Não executa função caso não haja uma fase ativa
      if( !this.phase ) return false;

      // Identificadores
      var flowState = [];

      // Adição da rodada, fase, etapa e turno
      flowState.push(
        this.round.name + '-' + this.round.counter, this.phase.name, this.step.name + '-' + this.step.counter, this.turn.name + '-' + this.turn.counter
      );

      // Adição de quase todas as demais unidades de fluxo, quando existentes
      flowState = flowState.concat( [ 'period', 'block', 'segment', 'parity' ].map( name => flow[ name ]?.name ).filter( flowUnit => flowUnit ) );

      // Retorno da cadeia de unidades de fluxo, sob a forma de uma string
      return flowState.join( '.' );
    }

    // Atribuição de propriedades de 'flow.moves'
    let moves = flow.moves;

    /// Jogadas do jogador ímpar
    moves.odd = [];

    /// Jogadas do jogador par
    moves.even = [];

    // Atribuição de propriedades de 'log'
    var matchLog = match.log;

    /// Objeto para inserção de registros de eventos na partida
    matchLog.body = {};

    /// Adiciona cadeia de fluxo para o registro de eventos
    matchLog.addFlowChain = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData,
          flowChain = flow.toChain();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.FlowParity );
        m.oAssert( !( flowChain in this.body ) );
      }

      // Adiciona cadeia de fluxo atual, para controle de eventos
      this.body[ flowChain ] = { events: [] };
    }

    /// Adiciona entrada de registro de evento
    matchLog.addEntry = function ( eventData = {} ) {
      // Não executa função caso não haja uma fase ativa
      if( !flow.phase ) return false;

      // Identificadores pré-validação
      var { eventType, eventTarget: event, flowChain = flow.toChain() } = eventData;

      // Caso não haja uma paridade ativa no momento do registro, ajusta cadeia de fluxo para que ela seja a última existente
      if( flowChain == flow.toChain() && !flow.parity ) flowChain = Object.keys( this.body ).pop();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( event );
        m.oAssert(
          [ m.CombatBreakElement, m.GameAction, m.GameCondition, m.GameDamage ].some( constructor => event instanceof constructor ) ||
          event == GameMatch.fateBreakExecution || typeof event == 'number'
        );
        m.oAssert( this.body[ flowChain ]?.events );
      }

      // Identificadores pós-validação
      var currentEvents = this.body[ flowChain ].events,
          logData = event instanceof m.CombatBreakElement ? configCombatBreak() :
                    event == GameMatch.fateBreakExecution ? configFateBreak() :
                    event instanceof m.GameAction ? configAction() :
                    event instanceof m.GameCondition ? configCondition() :
                    event instanceof m.GameDamage ? configDamage() :
                    eventType == 'heal' ? configHeal() : false;

      // Caso não haja dados a serem registrados, não continua função
      if( !logData ) return false;

      // Indica início do registro do evento
      m.events.logChangeStart.insert.emit( match, { event: event, data: logData, flowChain: flowChain } );

      // Atualiza arranjo de eventos do estágio alvo
      currentEvents.push( logData );

      // Indica fim do registro do evento
      m.events.logChangeEnd.insert.emit( match, { event: event, data: logData, flowChain: flowChain } );

      // Funções

      /// Configura o objeto de evento para paradas de combate
      function configCombatBreak() {
        // Retorna objeto relativo a resultados de paradas de combate
        return {
          name: event.name,
          type: 'combat-break',
          description: m.languages.notices.getOutcome( 'combat-break', { combatBreak: event, isFull: true } )
        };
      }

      /// Configura o objeto de evento para paradas de destino
      function configFateBreak() {
        // Identificadores
        var { name: fateBreakName, result: fateBreakResult, decidingPlayer, isContestable, requiredPoints } = GameMatch.fateBreakExecution;

        // Retorna objeto relativo a resultados de paradas de destino
        return {
          name: fateBreakName,
          type: 'fate-break',
          description: m.languages.notices.getOutcome( 'fate-break', {
            fateBreakName: fateBreakName, fateBreakResult: fateBreakResult, decidingPlayer: decidingPlayer, isContestable: isContestable,
            requiredPoints: requiredPoints
          } )
        };
      }

      /// Configura o objeto de evento para ações
      function configAction() {
        // Não executa função caso ação tenha sido concluída e [ainda não acionada/seja atacar], visto que o registro seria uma cópia de um já existente
        if( eventType == 'finish' && ( !event.isCommitted || event instanceof m.ActionAttack ) ) return false;

        // Identificadores
        var description = eventType == 'prevent' ?
              m.languages.notices.getOutcome( 'action-prevented', { action: event, isFull: true } ) :
              m.languages.notices.getActionLog( event.name, { action: event, isFull: true, eventType: eventType } );

        // Indica que ação foi registrada
        event.isLogged = true;

        // Retorna objeto relativo a eventos que sejam ações
        return {
          name: event.name,
          type: 'action',
          object: event.toObject(),
          description: description
        };
      }

      /// Configura o objeto de evento para condições
      function configCondition() {
        // Identificadores
        var { conditionTarget = event.target, markers = 0 } = eventData,
            outcomeName = [ 'apply', 'increase' ].includes( eventType ) ? 'condition-gained' : 'condition-removed';

        // Retorna objeto relativo a condições alteradas
        return {
          name: event.name,
          type: 'condition',
          description: m.languages.notices.getOutcome( outcomeName, {
            condition: event, being: conditionTarget, markers: markers
          } )
        };
      }

      /// Configura o objeto de evento para danos
      function configDamage() {
        // Identificadores
        var { mainDamage, validDamages, damageTarget, isToBeRemoved } = eventData,
            mainValidDamage = validDamages[ 0 ];

        // Caso dano não seja o principal a ser infligido, não executa função
        if( event != mainValidDamage ) return false;

        // Retorna objeto relativo a danos infligidos
        return {
          name: mainDamage.name,
          type: 'damage',
          description: m.languages.notices.getOutcome( 'inflicted-damage', {
            damages: validDamages, damageTarget: damageTarget, isToBeRemoved: isToBeRemoved, isFull: true
          } ),
          damageTarget: damageTarget,
          inflictedDamages: Object.fromEntries( validDamages.map( damage => [ damage.name.replace( /^damage-/, '' ), damage.totalPoints ] ) )
        };
      }

      /// Configura o objeto de evento para curas
      function configHeal() {
        // Identificadores
        var { healTarget, source } = eventData;

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( event ) && event > 0 );

        // Retorna objeto relativo a dano curado
        return {
          name: source.name,
          type: 'heal',
          description: m.languages.notices.getOutcome( 'healed-damage', {
            healedDamage: event, healTarget: healTarget, source: source, isFull: true
          } ),
          healTarget: healTarget,
          healedDamage: event
        };
      }
    }

    /// Remove entrada de registro de evento
    matchLog.removeEntry = function ( logData, flowChain = flow.toChain() ) {
      // Não executa função caso não haja uma fase ativa
      if( !flow.phase ) return false;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( logData.constructor == Object );
        m.oAssert( this.body[ flowChain ]?.events );
      }

      // Identificadores
      var { events: logEvents } = this.body[ flowChain ],
          dataKeys = Object.keys( logData ),
          targetEvent = logEvents.find( event => dataKeys.every( key => event[ key ] == logData[ key ] ) );

      // Não executa função caso um evento para remoção não tenha sido encontrado
      if( !targetEvent ) return false;

      // Indica início da remoção do evento
      m.events.logChangeStart.delete.emit( match, { event: targetEvent, flowChain: flowChain } );

      // Remove evento alvo
      logEvents.splice( logEvents.indexOf( targetEvent ), 1 );

      // Indica fim da remoção do evento
      m.events.logChangeEnd.delete.emit( match, { event: targetEvent, flowChain: flowChain } );
    }

    /// Na cadeia de fluxo passada, indica qual trecho deve ser usado como referência para a montagem do registro de eventos
    matchLog.getLogStage = function ( flowChain = flow.toChain() ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( typeof flowChain == 'string' );

      // Identificadores
      var targetStages = [
        /(?<=\.)(arrangement|deployment|allocation)-step(-\d+)?(?=\.)/,
        /(?<=\.)battle-turn-\d+\.(pre-combat-break|combat|post-combat-break|redeployment)-period(?=\.)/,
        /^round-\d+/
      ];

      // Itera por expressões de estágios possíveis
      for( let stage of targetStages ) {
        // Identificadores
        let stageName = stage.exec( flowChain )?.shift();

        // Caso estágio preferencial tenha sido encontrado, retorna-o
        if( stageName ) return stageName + '-log';
      }

      // Indica que não foi encontrado um estágio preferencial
      return false;
    }

    /// Formata registros, para serem enviados ao servidor
    matchLog.formatToServer = function () {
      // Identificadores
      var matchLogs = this.body,
          formattedLogs = {};

      // Itera por cadeias de fluxo
      for( let flowChain in matchLogs ) {
        // Captura entrada de registros da cadeia de fluxo alvo
        let logEntry = matchLogs[ flowChain ];

        // Filtra entradas sem eventos
        if( !logEntry.events.length ) continue;

        // Filtra eventos da entrada alvo, transferindo apenas eventos conversíveis em Json
        formattedLogs[ m.FlowUnit.displayFlowChain( flowChain ) ] = filterEventData( logEntry.events );
      }

      // Retorna registros da partida formatados
      return formattedLogs;

      // Funções

      /// Filtra dados de eventos, de modo a remover dados que não possam ser convertidos em Json
      function filterEventData( eventData ) {
        // Validação
        if( m.app.isInDevelopment ) m.oAssert( eventData?.constructor == Object || Array.isArray( eventData ) );

        // Identificadores
        var filteredData = Array.isArray( eventData ) ? [] : {};

        // Itera por chaves do evento atual
        for( let key in eventData ) {
          // Identificadores
          let currentData = eventData[ key ];

          // Caso dado atual seja um tipo primitivo escalar, transfere-o para os dados filtrados
          if( !currentData || [ 'string', 'number', 'boolean' ].includes( typeof currentData ) ) filteredData[ key ] = currentData

          // Caso dado atual seja um objeto literal ou um arranjo, realiza recursão sobre suas chaves
          else if( currentData.constructor == Object || Array.isArray( currentData ) ) filteredData[ key ] = filterEventData( currentData );
        }

        // Retorna dados filtrados
        return filteredData;
      }
    }
  }

  // Retorna paradas ativas na partida atual
  GameMatch.getActiveBreaks = function () {
    // Identificadores
    var activeBreaks = [];

    // Encerra função caso não haja uma partida em andamento
    if( !this.current ) return activeBreaks;

    // Se uma parada de destino estiver ativa, registra-a
    if( this.fateBreakExecution ) activeBreaks.push( 'fate' );

    // Se uma parada de ataque livre estiver ativa, registra-a
    if( m.ActionAttack.freeAttackExecution ) activeBreaks.push( 'free-attack' );

    // Se uma parada de combate estiver ativa, registra-a
    if( m.app.pixi.stage.children.find( child => child.name == 'combat-break-modal' ) ) activeBreaks.push( 'combat' );

    // Retorna paradas atualmente ativas
    return activeBreaks;
  }

  // Programa iniciação de uma parada de destino
  GameMatch.programFateBreak = function ( player, config = {} ) {
    // Caso uma parada de destino já esteja em andamento, torna a executar esta função após seu término
    if( this.fateBreakExecution ) return m.events.breakEnd.fate.add( this.current, this.programFateBreak.bind( this, player, config ), { once: true } );

    // Inicia parada de destino
    this.fateBreakExecution = this.executeFateBreak( player, config );

    // Executa parada de destino
    return setTimeout( () => GameMatch.fateBreakExecution.next() );
  }

  // Executa uma parada de destino
  GameMatch.executeFateBreak = function * ( player, config = {} ) {
    // Identificadores pré-validação
    var { name, playerBeing, opponentBeing, isContestable = false, requiredPoints = 1, middleAction, successAction, failureAction, modalArguments } = config,
        { fateBreakExecution } = this,
        fateBreakComponent;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( this.current && fateBreakExecution );
      m.oAssert( player instanceof m.GamePlayer );
      m.oAssert( Object.keys( config ).every( key => [
        'name', 'playerBeing', 'opponentBeing', 'isContestable', 'requiredPoints', 'middleAction', 'successAction', 'failureAction', 'modalArguments'
      ].includes( key ) ) );
      m.oAssert( name && typeof name == 'string' );
      m.oAssert( [ playerBeing, opponentBeing ].every( being => !being || being instanceof m.Being ) );
      m.oAssert( isContestable === null || typeof isContestable == 'boolean' );
      m.oAssert( Number.isInteger( requiredPoints ) && requiredPoints >= 1 );
      m.oAssert( !middleAction && isContestable !== null || typeof middleAction == 'function' );
      m.oAssert( [ successAction, failureAction ].some( action => typeof action == 'function' ) );
      m.oAssert( Array.isArray( modalArguments ) );
      m.oAssert( modalArguments[ 0 ] && typeof modalArguments[ 0 ] == 'string' );
      m.oAssert( !modalArguments[ 1 ] || modalArguments[ 1 ].constructor == Object );
    }

    // Identificadores pós-validação
    var { parity: playerParity, fatePoints: playerFatePoints } = player,
        isDecidingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == player,
        opponentParity = playerParity == 'odd' ? 'even' : 'odd',
        opponent = this.current.players[ opponentParity ],
        isOpponent = m.GameMatch.current.isSimulation || m.GamePlayer.current == opponent,
        opponentFatePoints = opponent.fatePoints,
        fateBreakText = '',
        isToUseFatePoint = {
          player: false, opponent: false
        };

    // Registra informações sobre parada de destino

    /// De argumentos de configuração passados
    for( let key of [ 'name', 'playerBeing', 'opponentBeing' ] ) fateBreakExecution[ key ] = config[ key ];

    /// Demais propriedades
    [ fateBreakExecution.decidingPlayer, fateBreakExecution.result, fateBreakExecution.isContestable, fateBreakExecution.requiredPoints ] =
    [ player, isToUseFatePoint, isContestable, requiredPoints ];

    // Sinaliza início da parada de destino
    m.events.breakStart.fate.emit( this.current, { player: player, opponent: opponent, isContestable: isContestable } );

    // Caso parada de destino deva ser desconsiderada, encerra função
    if( fateBreakExecution.isToSkip ) return finishExecution( true );

    // Captura pontos de destino usáveis do jogador a decidir parada
    let playerTotalFatePoints = playerFatePoints.current.get() + ( playerBeing?.content.stats.fatePoints.current ?? 0 );

    // Caso jogador alvo não tenha os pontos de destino requeridos, encerra parada de destino
    if( playerTotalFatePoints < requiredPoints ) return finishExecution( true );

    // Define texto padrão introdutório do componente da parada de destino
    fateBreakText = m.languages.notices.getFateBreakText( 'fate-break-heading-' + ( isDecidingPlayer ? 'player' : 'other' ), {
      player: player,
      requiredPoints: requiredPoints,
      embeddedPoints: playerBeing?.content.stats.fatePoints.current ?? 0,
      isContestable: isContestable
    } );

    // Para caso usuário seja o a decidir parada de destino
    if( isDecidingPlayer ) {
      // Gera componente para decisão sobre uso do ponto de destino
      fateBreakComponent = new m.ConfirmationModal( {
        text: fateBreakText + m.languages.notices.getFateBreakText( ...modalArguments ) + '?',
        acceptAction: () => progressExecution( true ),
        declineAction: () => progressExecution( false )
      } ).insert();
    }

    // Do contrário
    else {
      // Gera componente avisando sobre decisão do jogador da parada de destino
      fateBreakComponent = new m.AlertModal( {
        text: fateBreakText + m.languages.notices.getFateBreakText( ...modalArguments ) + '.',
        action: () => fateBreakComponent = new m.StaticBar( {
          text: m.languages.notices.getChoiceText( 'awaiting-player-choice', { playerParity: player.parity } )
        } ).insert()
      } ).insert();
    }

    // Captura decisão sobre uso do ponto de destino
    isToUseFatePoint.player = yield;

    // Caso usuário não seja o jogador responsável pela decisão, informa decisão a usuário
    if( !isDecidingPlayer )
      m.noticeBar.show(
        m.languages.notices.getChoiceText( 'fate-break-choice', { playerParity: player.parity, isToUse: isToUseFatePoint.player } ),
        isOpponent && !isToUseFatePoint.player ? 'green' : 'red'
      );

    // Caso jogador tenha optado por não usar pontos de destino, encerra função
    if( !isToUseFatePoint.player ) return finishExecution();

    // Remove da tela modal atual
    fateBreakComponent.remove();

    // Para caso haja uma função a ser executada antes do gasto do ponto de destino
    if( middleAction ) {
      // Executa função
      yield middleAction();

      // Atualiza dados da parada de destino que possam ter sido alterados
      [ playerBeing, opponentBeing, isContestable ] = [ fateBreakExecution.playerBeing, fateBreakExecution.opponentBeing, fateBreakExecution.isContestable ];

      // Para caso parada de destino deva ser desconsiderada
      if( fateBreakExecution.isToSkip ) {
        // Reverte decisão do jogador de usar pontos de destino
        isToUseFatePoint.player = false;

        // Encerra função
        return finishExecution();
      }
    }

    // Gasta pontos de destino relativos à parada
    playerFatePoints.decrease( playerBeing?.content.stats.fatePoints.spend( requiredPoints ) ?? requiredPoints );

    // Caso parada de destino não seja contestável, encerra função
    if( !isContestable ) return finishExecution();

    // Captura pontos de destino usáveis do oponente
    let opponentTotalFatePoints = opponentFatePoints.current.get() + ( opponentBeing?.content.stats.fatePoints.current ?? 0 );

    // Caso o oponente não tenha os pontos de destino requeridos, encerra parada de destino
    if( opponentTotalFatePoints < requiredPoints ) return finishExecution();

    // Define texto padrão do componente de contestação da parada de destino
    fateBreakText = m.languages.notices.getFateBreakText( 'fate-break-contestation-' + ( isOpponent ? 'player' : 'other' ), {
      player: opponent,
      requiredPoints: requiredPoints,
      embeddedPoints: opponentBeing?.content.stats.fatePoints.current ?? 0,
      effect: m.languages.notices.getFateBreakText( ...modalArguments )
    } );

    // Para caso usuário seja o a contestar parada de destino
    if( isOpponent ) {
      // Gera componente para decisão sobre contestação do ponto de destino
      fateBreakComponent = new m.ConfirmationModal( {
        text: fateBreakText,
        acceptAction: () => progressExecution( true ),
        declineAction: () => progressExecution( false )
      } ).replace();
    }

    // Do contrário
    else {
      // Gera componente avisando sobre possível contestação do ponto de destino
      fateBreakComponent = new m.StaticBar( {
        text: fateBreakText
      } ).insert();
    }

    // Captura decisão do oponente sobre uso do ponto de destino
    isToUseFatePoint.opponent = yield;

    // Caso usuário não seja o jogador contestante, informa-o sobre sua decisão
    if( !isOpponent )
      m.noticeBar.show(
        m.languages.notices.getChoiceText( 'fate-break-contestation-choice', { playerParity: opponent.parity, isToUse: isToUseFatePoint.opponent } ),
        isDecidingPlayer && !isToUseFatePoint.opponent ? 'green' : 'red'
      );

    // Caso oponente tenha optado por não usar pontos de destino, encerra função
    if( !isToUseFatePoint.opponent ) return finishExecution();

    // Gasta pontos de destino relativos à contestação
    opponentFatePoints.decrease( opponentBeing?.content.stats.fatePoints.spend( requiredPoints ) ?? requiredPoints );

    // Caso a magia 'O Festim' não seja aplicável para o jogador da parada, encerra função
    if( !m.SpellTheFeast.checkCounterContestation( player, playerBeing ) ) return finishExecution();

    // Remove da tela modal atual
    fateBreakComponent.remove();

    // Para caso usuário seja o a decidir parada de destino
    if( isDecidingPlayer ) {
      // Gera componente para decisão sobre uso do ponto de destino via 'O Festim'
      fateBreakComponent = new m.ConfirmationModal( {
        text: m.languages.notices.getFateBreakText( 'the-feast-contestation-player', {
          requiredPoints: requiredPoints,
          embeddedPoints: playerBeing?.content.stats.fatePoints.current ?? 0,
        } ),
        acceptAction: () => progressExecution( false ),
        declineAction: () => progressExecution( true )
      } ).insert();
    }

    // Do contrário
    else {
      // Gera componente avisando sobre decisão do jogador da parada de destino
      fateBreakComponent = new m.StaticBar( {
        text: m.languages.notices.getChoiceText( 'choosing-deny-fate-break-contestation', { playerParity: player.parity } )
      } ).insert();
    }

    // Captura decisão sobre negação da contestação
    isToUseFatePoint.opponent = yield;

    // Caso usuário não seja o jogador responsável pela decisão, informa decisão a usuário
    if( !isDecidingPlayer )
      m.noticeBar.show(
        m.languages.notices.getChoiceText( 'deny-fate-break-contestation-choice', { playerParity: player.parity, isToDeny: !isToUseFatePoint.opponent } ),
        isOpponent && isToUseFatePoint.opponent ? 'green' : 'red'
      );

    // Caso jogador tenha optado por não negar a contestação, encerra função
    if( isToUseFatePoint.opponent ) return finishExecution();

    // Gasta pontos de destino relativos à negação da contestação
    playerFatePoints.decrease( playerBeing?.content.stats.fatePoints.spend( requiredPoints ) ?? requiredPoints );

    // Caso a magia 'O Festim' não seja aplicável para o oponente, encerra função
    if( !m.SpellTheFeast.checkCounterContestation( opponent, opponentBeing ) ) return finishExecution();

    // Remove da tela modal atual
    fateBreakComponent.remove();

    // Para caso usuário seja o oponente
    if( isOpponent ) {
      // Gera componente para decisão sobre contestação da negação da parada de destino
      fateBreakComponent = new m.ConfirmationModal( {
        text: m.languages.notices.getFateBreakText( 'the-feast-contestation-opponent', {
          requiredPoints: requiredPoints,
          embeddedPoints: opponentBeing?.content.stats.fatePoints.current ?? 0,
        } ),
        acceptAction: () => progressExecution( true ),
        declineAction: () => progressExecution( false )
      } ).insert();
    }

    // Do contrário
    else {
      // Gera componente avisando sobre decisão do oponente
      fateBreakComponent = new m.StaticBar( {
        text: m.languages.notices.getChoiceText( 'choosing-contest-fate-break-denying', { playerParity: opponent.parity } )
      } ).insert();
    }

    // Captura decisão sobre contestação da negação
    isToUseFatePoint.opponent = yield;

    // Caso usuário não seja o oponente, informa-o sobre sua decisão
    if( !isOpponent )
      m.noticeBar.show(
        m.languages.notices.getChoiceText( 'contest-fate-break-denying-choice', { playerParity: opponent.parity, isToContest: isToUseFatePoint.opponent } ),
        isDecidingPlayer && !isToUseFatePoint.opponent ? 'green' : 'red'
      );

    // Caso oponente tenha optado por não contestar a negação da contestação, encerra função
    if( !isToUseFatePoint.opponent ) return finishExecution();

    // Gasta pontos de destino relativos à contestação da negação
    opponentFatePoints.decrease( opponentBeing?.content.stats.fatePoints.spend( requiredPoints ) ?? requiredPoints );

    // Encerra função com o contestamento da parada de destino
    return finishExecution();

    // Funções

    /// Progride parada de destino
    function progressExecution( value ) {
      // Caso partida não seja uma simulação, propaga decisão sobre uso de pontos de destino
      if( !m.GameMatch.current.isSimulation )
        m.sockets.current.emit( 'progress-fate-break', {
          matchName: GameMatch.current.name,
          data: {
            fateBreakName: GameMatch.fateBreakExecution.name,
            value: value
          }
        } );

      // Prossegue execução da função principal
      GameMatch.fateBreakExecution.next( value );
    }

    /// Encerra execução da parada de destino
    function finishExecution( isToSkipLog = false ) {
      // Identificadores
      var wasSuccess = isToUseFatePoint.player && !isToUseFatePoint.opponent;

      // Caso registro sobre parada de destino não deva ser omitido, executa-o
      if( !isToSkipLog ) GameMatch.current.log.addEntry( { eventTarget: GameMatch.fateBreakExecution } );

      // Nulifica iterador da parada de destino
      GameMatch.fateBreakExecution = null;

      // Quando existente, remove da tela componente sobre a parada do destino
      fateBreakComponent?.remove();

      // Quando existirem, executa funções de sucesso ou fracasso da parada
      wasSuccess ? successAction?.() : failureAction?.();

      // Sinaliza fim da parada de destino
      m.events.breakEnd.fate.emit( GameMatch.current, {
        player: player, opponent: opponent, isContestable: isContestable, wasSkipped: isToSkipLog, wasSuccess: wasSuccess
      } );
    }
  }
}

/// Propriedades do protótipo
GameMatch.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: GameMatch }
} );
