// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const MainDeck = function ( config = {} ) {
  // Iniciação de propriedades
  MainDeck.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.name || config.displayName );
    m.oAssert( !config.displayName || typeof config.displayName == 'string' );
    m.oAssert( [ m.GameUser, m.GamePlayer ].some( constructor => config.owner instanceof constructor ) );
  }

  // Atribuição de propriedades pré-superconstrutor

  /// Atribuição do nome de exibição, se existente
  if( config.displayName ) this.displayName = config.displayName.trim();

  /// Atribuição do dono
  this.owner = config.owner;

  /// Caso dono seja um jogador, define este baralho como o seu do jogo
  if( this.owner instanceof m.GamePlayer ) this.owner.gameDeck = this;

  /// Atribuição das datas de criação e modificação
  this.modifiedDate = this.createdDate = new Date().toISOString();

  // Superconstrutor
  m.Deck.call( this, config );

  // Eventos

  /// Para ajustar quantidade máxima de humanos aceitos pelo comandante deste baralho, quando existente
  m.events.deckChangeEnd.any.add( this, ( eventData ) => this.cards.commander ? m.Human.triggerRacialMix( this.cards.commander, eventData ) : null );

  /// Para redefinir quantidade máxima de humanos aceitos pelo comandante deste baralho quando ele estiver para ser removido do baralho
  m.events.deckChangeStart.remove.add( this, (eventData) => this.cards.commander ? m.Human.triggerRacialMix( this.cards.commander, eventData ) : null );
}

/// Propriedades do construtor
defineProperties: {
  // Quantidade mínima de cartas permitidas
  MainDeck.minCards = 15;

  // Quantidade máxima de cartas permitidas
  MainDeck.maxCards = 30;

  // Iniciação de propriedades
  MainDeck.init = function ( deck ) {
    // Chama 'init' ascendente
    m.Deck.init( deck );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deck instanceof MainDeck );

    // Atribuição de propriedades iniciais

    /// Tipo
    deck.type = 'main-deck';

    /// Nome de exibição
    deck.displayName = '';

    /// Indica se baralho é um pré-definido pelo jogo
    deck.isStarter = false;

    /// Data de criação
    deck.createdDate = '';

    /// Data de modificação
    deck.modifiedDate = '';

    /// Registro de partidas
    deck.matches = {};

    /// Baralho coadjuvante vinculado a este baralho
    deck.sideDeck = null;

    /// Retorna cópia do baralho principal, com seu eventual baralho coadjuvante
    deck.copy = function ( config = {} ) {
      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( Object.keys( config ).every( key => [ 'owner', 'includeName', 'includeIsStarter', 'includeDate', 'includeMatches' ].includes( key ) ) );

      // Identificadores
      var { owner = null, includeName = false, includeIsStarter = false, includeDate = false, includeMatches = false } = config,
          propertiesToAssign = [
            ( includeIsStarter ? 'isStarter' : '' ), ( includeDate ? 'createdDate' : '' ), ( includeMatches ? 'matches' : '' )
          ].filter( key => key ),
          copiedDeck = new MainDeck( {
            name: includeName ? this.name : '',
            displayName: this.displayName,
            owner: owner ?? this.owner
          } );

      // Atribui propriedades a serem copiadas ao novo baralho principal
      if( propertiesToAssign.length ) Object.oAssignByCopy( copiedDeck, this, { only: propertiesToAssign } );

      // Adiciona ao baralho copiado uma cópia das cartas do baralho original
      copiedDeck.add( this.cards.map( card => card.copy() ), true );

      // Caso baralho principal não tenha um coadjuvante, encerra função
      if( !this.sideDeck ) return copiedDeck;

      // Gera baralho coadjuvante do principal
      let copiedSideDeck = new m.SideDeck( {
            name: includeName ? this.sideDeck.name : '',
            mainDeck: copiedDeck
          } );

      // Adiciona ao baralho coadjuvante copiado uma cópia das cartas do baralho original
      copiedSideDeck.add( this.sideDeck.cards.map( card => card.copy() ), true );

      // Retorna baralho copiado
      return copiedDeck;
    }

    /// Converte baralho principal em um objeto literal
    deck.toObject = function () {
      // Identificadores
      var dataObject = {};

      // Registra propriedades escalares do baralho principal
      for( let key of [ 'name', 'displayName', 'isStarter', 'createdDate', 'modifiedDate', 'matches' ] ) dataObject[ key ] = this[ key ];

      // Registra cartas do baralho principal
      dataObject.cards = this.cards.map( card => card.toObject() );

      // Se baralho principal tiver um coadjuvante, também o registra
      if( this.sideDeck )
        dataObject.sideDeck = {
          name: this.sideDeck.name, cards: this.sideDeck.cards.map( card => card.toObject() )
        };

      // Retorna objeto literal
      return dataObject;
    }

    /// Atualiza colocação de carta alvo em arranjos de atividade do baralho
    deck.updateActivityLists = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventTarget: card, formerActivity } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-activity' );
        m.oAssert( card instanceof m.Card );
      }

      // Identificadores pós-validação
      var activityIndex = this.cards[ formerActivity ]?.indexOf( card ) ?? -1;

      // Caso carta exista no arranjo de sua atividade antiga, retira-a de lá
      if( activityIndex != -1 ) this.cards[ formerActivity ].splice( activityIndex, 1 );

      // Quando existente, adiciona carta ao arranjo de cartas de sua nova atividade
      this.cards[ card.activity ]?.push( card );
    }

    /// Atualiza colocação de carta alvo em arranjo de cartas em uso do baralho
    deck.updateUseList = function ( eventData = {} ) {
      // Apenas executa função caso alvo do uso seja igual ao alvo do evento
      if( eventData.eventTarget != eventData.useTarget ) return;

      // Identificadores
      var { eventCategory, eventTarget: card, useTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' );
        m.oAssert( card instanceof m.Card );
      }

      // Para caso carta atualmente esteja em uso
      if( card.isInUse ) {
        // Adiciona carta ao arranjo de cartas em uso, caso ela ainda não esteja lá
        if( !this.cards.inUse.includes( card ) ) this.cards.inUse.push( card );
      }

      // Do contrário, remove carta do arranjo de cartas em uso, caso ela esteja lá
      else if( this.cards.inUse.includes( card ) ) this.cards.inUse.splice( this.cards.inUse.indexOf( card ), 1 );
    }

    // Atribuição de propriedades de 'cards'
    let cards = deck.cards;

    /// Cartas em uso
    cards.inUse = [];

    /// Cartas ativas
    cards.active = [];

    /// Cartas suspensas
    cards.suspended = [];

    /// Cartas afastadas
    cards.withdrawn = [];

    /// Cartas findadas
    cards.ended = [];

    // Atribuição de propriedades de 'matches'
    let matches = deck.matches;

    /// Total
    matches.total = 0;

    /// Vitórias
    matches.wins = 0;

    /// Empates
    matches.draws = 0;

    /// Derrotas
    matches.losses = 0;

    /// Incompletudes
    matches.unfinished = 0;
  }
}

/// Propriedades do protótipo
MainDeck.prototype = Object.create( m.Deck.prototype, {
  // Construtor
  constructor: { value: MainDeck }
} );
