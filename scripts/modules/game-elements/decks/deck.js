// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const Deck = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !config.name || typeof config.name == 'string' );

  // Atribuição de propriedades pré-superconstrutor

  /// Nome de identificação
  this.name = config.name || this.syncDeckNames();

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Eventos

  /// Para ajustar moedas do baralho e de suas cartas
  m.events.deckChangeEnd.any.add( this, this.adjustCoins );
}

/// Propriedades do construtor
defineProperties: {
  // Quantidade máxima de conjunto de baralhos permitidos por usuário
  Deck.maxQuantity = 16;

  // Iniciação de propriedades
  Deck.init = function ( deck ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deck instanceof Deck );

    // Atribuição de propriedades iniciais

    /// Nome
    deck.name = '';

    /// Tipo
    deck.type = '';

    /// Moedas
    deck.coins = {};

    /// Créditos
    deck.credits = {};

    /// Dono
    deck.owner = null;

    /// Cartas
    deck.cards = [];

    /// Sincroniza nome de identificação do baralho com o de exibição do baralho principal
    deck.syncDeckNames = function () {
      // Identificadores
      var mainDeck = this.type == 'main-deck' ? this : this.mainDeck,
          { displayName } = mainDeck,
          mainDeckName = displayName.trim().toLowerCase().replace( /\s/g, '-' ) + `-main-deck`,
          userDecks = mainDeck.owner.decks ?? [];

      // Limpa nome de identificação atual do baralho principal
      mainDeck.name = '';

      // Caso novo nome de identificação seja igual ao de outro baralho do mesmo dono, ajusta-o
      for( let i = 2; userDecks.some( deck => deck.name == mainDeckName ); i++ ) mainDeckName = mainDeckName.replace( /-\d+$/, '' ) + '-' + i;

      // Atribui nome de identificação do baralho principal
      mainDeck.name = mainDeckName;

      // Atribui nome de identificação do baralho coadjuvante, se aplicável
      if( mainDeck.sideDeck ) mainDeck.sideDeck.name = mainDeckName.replace( /-main-deck(?=$|(-\d+$))/, '-side-deck' );

      // Retorna nome de identificação atual do baralho
      return this.name;
    }

    /// Adiciona cartas ao baralho
    deck.add = function ( cards, skipValidation = false ) {
      // Identificadores
      var validationConfig = { shortCircuit: true, skipMinDeckCards: true, skipMinCommandCards: true },
          mainDeck = this.type == 'main-deck' ? this : this.mainDeck;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( mainDeck );
        m.oAssert( cards.every( card => card instanceof m.Card ) );
        m.oAssert( cards.every( card => card.activity == 'unlinked' ) );
        m.oAssert( cards.every( card => !card.deck ) );
        m.oAssert( cards.every( card => !card.isToken ) );
        m.oAssert( cards.every( card => !card.isEmbedded ) );
        m.oAssert( cards.every( card => !card.attachments.external.length ) );
        m.oAssert( cards.every( card => !card.content.stats.command || this == mainDeck && !this.cards.commander ) );
      }

      // Não executa função caso não haja cartas a serem adicionadas
      if( !cards.length ) return cards;

      // Sinaliza ao baralho início da adição de cartas
      m.events.deckChangeStart.add.emit( this, { cards: cards, deck: this } );

      // Sinaliza às cartas passadas início da inclusão no baralho
      cards.forEach( card => m.events.deckChangeStart.add.emit( card, { cards: cards, deck: this } ) );

      // Itera por cartas passadas
      for( let card of cards ) {
        // Adiciona carta ao baralho
        this.cards.push( card );

        // Vincula baralho à carta
        card.deck = this;
      }

      // Formata baralho
      this.format();

      // Atualiza data de modificação
      mainDeck.modifiedDate = new Date().toISOString();

      // Sinaliza às cartas passadas fim da inclusão no baralho
      cards.forEach( card => m.events.deckChangeEnd.add.emit( card, { cards: cards, deck: this } ) );

      // Sinaliza ao baralho fim da adição de cartas
      m.events.deckChangeEnd.add.emit( this, { cards: cards, deck: this } );

      // Caso não se tenha requerido validação, retorna cartas adicionadas
      if( skipValidation ) return cards;

      // Valida cartas
      let validation = this.validate( validationConfig );

      // Caso baralho esteja inválido, remove cartas adicionadas
      if( !validation.isValid ) this.remove( cards );

      // Retorna objeto de validação
      return validation;
    }

    /// Remove cartas do baralho
    deck.remove = function ( cards ) {
      // Identificadores
      var mainDeck = this.type == 'main-deck' ? this : this.mainDeck,
          cardsToRemove = cards.filter( card => card.deck == this ),
          removedCards = cardsToRemove.slice();

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( mainDeck );
        m.oAssert( cards.every( card => card instanceof m.Card ) );
        m.oAssert( cards.every( card => !card.attachments.external.length ) );
      }

      // Não executa função caso não haja cartas a serem removidas
      if( !cardsToRemove.length ) return cards;

      // Sinaliza ao baralho início da remoção de cartas
      m.events.deckChangeStart.remove.emit( this, { cards: cardsToRemove, deck: this } );

      // Sinaliza às cartas passadas início de sua remoção do baralho
      cardsToRemove.forEach( card => m.events.deckChangeStart.remove.emit( card, { cards: cardsToRemove, deck: this } ) );

      // Remoção de cartas do baralho
      while( cardsToRemove.length ) {
        // Identificadores
        let targetCard = cardsToRemove.shift();

        // Remove carta do baralho
        this.cards.splice( this.cards.indexOf( targetCard ), 1 );

        // Remove baralho de carta
        targetCard.deck = null;
      }

      // Formata baralho
      this.format();

      // Atualiza data de modificação
      mainDeck.modifiedDate = new Date().toISOString();

      // Sinaliza às cartas passadas fim de sua remoção do baralho
      removedCards.forEach( card => m.events.deckChangeEnd.remove.emit( card, { cards: removedCards, deck: this } ) );

      // Sinaliza ao baralho fim da remoção de cartas
      m.events.deckChangeEnd.remove.emit( this, { cards: removedCards, deck: this } );

      // Retorna cartas passadas
      return cards;
    }

    /// Substitui cartas em baralho por outras
    deck.replace = function ( cardsSet, skipValidation = false ) {
      // Identificadores
      var { old: oldCards, new: newCards } = cardsSet;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ oldCards, newCards ].flat().every( card => card instanceof m.Card ) );
        m.oAssert( oldCards.every( card => card.deck == this ) );
        m.oAssert( newCards.every( card => !card.deck ) );
      }

      // Remove cartas atuais
      this.remove( oldCards );

      // Adiciona novas cartas
      this.add( newCards, true );

      // Caso não se tenha requerido validação, retorna conjunto de cartas
      if( skipValidation ) return cardsSet;

      // Valida baralho, e captura seu objeto de validação
      let validation = this.validate( { shortCircuit: true, skipMinDeckCards: true } );

      // Caso baralho esteja inválido, desfaz a substituição
      if( !validation.isValid ) this.replace( { old: newCards, new: oldCards }, true );

      // Retorna objeto de validação
      return validation;
    }

    /// Troca baralho de cartas
    deck.swap = function ( cardsSet, skipValidation = false ) {
      // Identificadores
      var { old: oldCards, new: newCards } = cardsSet,
          newCardsDeck = newCards[0].deck,
          validationArray = [];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ oldCards, newCards ].flat().every( card => card instanceof m.Card ) );
        m.oAssert( oldCards.every( card => card.deck == this ) );
        m.oAssert( newCards.every( card => card.deck == newCardsDeck ) );
        m.oAssert( this != newCardsDeck );
      }

      // Remove cartas de seus respectivos baralhos

      /// Baralho atual
      this.remove( oldCards );

      /// Baralho de referência
      newCardsDeck.remove( newCards );

      // Adiciona cartas em seus novos baralhos

      /// Baralho atual
      this.add( newCards, true );

      /// Baralho de referência
      newCardsDeck.add( oldCards, true );

      // Caso não se tenha requerido validação, retorna conjunto de cartas
      if( skipValidation ) return cardsSet;

      // Valida baralhos, e captura seu objeto de validação
      for( let deck of [ this, newCardsDeck ] ) validationArray.push( deck.validate( { shortCircuit: true, skipMinDeckCards: true } ) );

      // Caso ao menos um dos baralhos esteja inválido, desfaz a troca
      if( validationArray.some( validation => !validation.isValid ) ) this.swap( { old: newCards, new: oldCards }, true );

      // Retorna arranjo de validação
      return validationArray;
    }

    /// Limpa baralho
    deck.clear = function () {
      // Remove todas as cartas do baralho
      return this.remove( this.cards );
    }

    /// Formata baralho
    deck.format = function () {
      // Identificadores
      var { cards } = this;

      // Ordena posição de cartas no baralho
      sortCards: {
        cards.sort( function ( a, b ) {
          // Identificadores
          var cardValues = [];

          // Atribuição de pontuação comparativa
          for( let card of [ a, b ] ) {
            // Identificadores
            let cardValue = '';

            // Por tipo primitivo
            cardValue +=
              card.content.stats?.command ? '1' :
              card instanceof m.Being     ? '2' :
              card instanceof m.Item      ? '3' :
              card instanceof m.Spell     ? '4' : '0';

            // Por tipo derivado
            switch( cardValue ) {
              // Para criaturas
              case '2':
                cardValue +=
                  ( card.sourceBeing ?? card ) instanceof m.Human    ? '1' :
                  ( card.sourceBeing ?? card ) instanceof m.Orc      ? '2' :
                  ( card.sourceBeing ?? card ) instanceof m.Elf      ? '3' :
                  ( card.sourceBeing ?? card ) instanceof m.Dwarf    ? '4' :
                  ( card.sourceBeing ?? card ) instanceof m.Halfling ? '5' :
                  ( card.sourceBeing ?? card ) instanceof m.Gnome    ? '6' :
                  ( card.sourceBeing ?? card ) instanceof m.Goblin   ? '7' :
                  ( card.sourceBeing ?? card ) instanceof m.Ogre     ? '8' : '0';
                break;
              // Para equipamentos
              case '3':
                cardValue +=
                  card instanceof m.Weapon    ? '1' :
                  card instanceof m.Garment   ? '2' :
                  card instanceof m.Implement ? '3' : '0';
                break;
              // Para magias
              case '4':
                cardValue +=
                  card instanceof m.Metoth   ? '1' :
                  card instanceof m.Enoth    ? '2' :
                  card instanceof m.Powth    ? '3' :
                  card instanceof m.Voth     ? '4' :
                  card instanceof m.Faoth    ? '5' : '0';
                break;
              // Para comandantes e casos imprevistos
              default:
                cardValue += '0';
            }

            // Por ordem em grades
            cardValue += 100 + card.gridOrder;

            // Captura da pontuação da carta
            cardValues.push( Number( cardValue ) );
          }

          // Compara valor da primeira carta com a segunda, e as ordena crescentemente
          return cardValues[ 0 ] - cardValues[ 1 ];
        } );
      }

      // Define arranjos de cartas do baralho segundo seu tipo primitivo
      segmentCards: {
        // Definição do comandante
        cards.commander = cards.find( card => card.content.stats.command );

        // Definição dos entes
        cards.beings = cards.filter( card => card instanceof m.Being && !card.astralForm );

        // Definição das criaturas
        cards.creatures = cards.beings.filter( card => !card.content.stats.command );

        // Definição dos ativos
        cards.assets = cards.filter( card => [ m.Item, m.Spell ].some( constructor => card instanceof constructor ) );

        // Definição dos equipamentos
        cards.items = cards.assets.filter( card => card instanceof m.Item );

        // Definição das magias
        cards.spells = cards.assets.filter( card => card instanceof m.Spell );
      }

      // Retorna baralho
      return this;
    }

    /// Valida conjunto de baralhos
    deck.validate = function ( config = {} ) {
      // Identificadores
      var mainDeck = this.type == 'main-deck' ? this : this.mainDeck,
          { cards, coins, sideDeck } = mainDeck,
          { commander, creatures, items, spells } = cards,
          { shortCircuit = false } = config,
          validation = {
            isValid: false, invalidData: {}, minDeckCards: undefined, maxDeckCards: undefined, maxCoinsSpent: undefined, maxSameCards: undefined,
            minCommandCards: undefined, maxCommandCards: undefined
          };

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Object.keys( config ).every( key => [
          'shortCircuit', 'skipMinDeckCards', 'skipMaxDeckCards', 'skipMaxCoinsSpent', 'skipMaxSameCards', 'skipMinCommandCards', 'skipMaxCommandCards'
        ].includes( key ) ) );
        m.oAssert( !sideDeck || ( sideDeck instanceof m.SideDeck && mainDeck.owner == sideDeck.owner ) );
        m.oAssert( commander && !sideDeck?.cards.commander );
        m.oAssert( !creatures.some( creature => creature.content.stats.command ) );
      }

      // Validação de quantidade mínima de cartas no baralho, se desejada
      if( !config.skipMinDeckCards ) {
        // Valida quantidade mínima de cartas no baralho
        validation.minDeckCards = validateMinDeckCards.call( mainDeck );

        // Para uma validação de curto-circuito, retorna objeto de validação se baralho estiver inválido
        if( shortCircuit && !validation.minDeckCards ) return validation;
      }

      // Validação de quantidade máxima de cartas no baralho, se desejada
      if( !config.skipMaxDeckCards ) {
        // Valida quantidade máxima de cartas no baralho
        validation.maxDeckCards = validateMaxDeckCards.call( mainDeck );

        // Para uma validação de curto-circuito, retorna objeto de validação se baralho estiver inválido
        if( shortCircuit && !validation.maxDeckCards ) return validation;
      }

      // Validação de quantidade máxima de moedas gastas, se desejada
      if( !config.skipMaxCoinsSpent ) {
        // Valida quantidade máxima de moedas gastas
        validation.maxCoinsSpent = validateMaxCoinsSpent.call( mainDeck );

        // Para uma validação de curto-circuito, retorna objeto de validação se baralho estiver inválido
        if( shortCircuit && !validation.maxCoinsSpent ) return validation;
      }

      // Validação de quantidade máxima de cartas iguais, se desejada
      if( !config.skipMaxSameCards ) {
        // Valida quantidade máxima de cartas iguais
        validation.maxSameCards = validateMaxSameCards.call( mainDeck );

        // Para uma validação de curto-circuito, retorna objeto de validação se baralho estiver inválido
        if( shortCircuit && !validation.maxSameCards ) return validation;
      }

      // Validação de quantidade mínima e máxima de cartas comandadas, se desejada
      if( !config.skipMinCommandCards || !config.skipMaxCommandCards ) {
        // Valida comando de comandantes
        let commandValidation = validateCommand.call( mainDeck );

        // Para validação de quantidade mínima de cartas comandadas, se desejada
        if( !config.skipMinCommandCards ) {
          // Define como válida ou não quantidade mínima de cartas comandadas
          validation.minCommandCards = Object.keys( commandValidation.minCommandCards ).length ? false: true;

          // Se inválida, captura invalidações
          if( !validation.minCommandCards ) Object.assign( validation.invalidData, { minCommandCards: commandValidation.minCommandCards } );
        }

        // Para validação de quantidade máxima de cartas comandadas, se desejada
        if( !config.skipMaxCommandCards ) {
          // Define como válida ou não quantidade máxima de cartas comandadas
          validation.maxCommandCards = Object.keys( commandValidation.maxCommandCards ).length ? false: true;

          // Se inválida, captura invalidações
          if( !validation.maxCommandCards ) Object.assign( validation.invalidData, { maxCommandCards: commandValidation.maxCommandCards } );
        }

        // Para uma validação de curto-circuito, retorna objeto de validação se baralho estiver inválido
        if( shortCircuit && ( !config.skipMinCommandCards && !validation.minCommandCards || !config.skipMaxCommandCards && !validation.maxCommandCards ) )
          return validation;
      }

      // Caso nenhuma invalidação seja encontrada, indica que baralho está validado
      if( !Object.keys( validation ).some( key => validation[ key ] === false && key != 'isValid' ) ) validation.isValid = true;

      // Retorna objeto de validação
      return validation;

      // Funções

      /// Valida quantidade mínima de cartas
      function validateMinDeckCards() {
        // Identificadores
        var invalidData = {};

        // Valida quantidade de cartas no baralho principal, e, se inválida, salva diferença entre ela e quantidade mínima
        if( cards.length < m.MainDeck.minCards ) invalidData.mainDeck = m.MainDeck.minCards - cards.length;

        // Valida quantidade de cartas no baralho coadjuvante, e, se inválida, salva diferença entre ela e quantidade mínima
        if( sideDeck && sideDeck.cards.length < m.SideDeck.minCards ) invalidData.sideDeck = m.SideDeck.minCards - sideDeck.cards.length;

        // Caso haja invalidações, transfere-as ao objeto de validação
        if( Object.keys( invalidData ).length ) Object.assign( validation.invalidData, { minDeckCards: invalidData } );

        // Indica se validação foi bem-sucedida ou não
        return !validation.invalidData.minDeckCards;
      }

      /// Valida quantidade máxima de cartas
      function validateMaxDeckCards() {
        // Identificadores
        var invalidData = {};

        // Valida quantidade de cartas no baralho principal, e, se inválida, salva diferença entre ela e quantidade máxima
        if( cards.length > m.MainDeck.maxCards ) invalidData.mainDeck = cards.length - m.MainDeck.maxCards;

        // Valida quantidade de cartas no baralho coadjuvante, e, se inválida, salva diferença entre ela e quantidade máxima
        if( sideDeck && sideDeck.cards.length > m.SideDeck.maxCards ) invalidData.sideDeck = sideDeck.cards.length - m.SideDeck.maxCards;

        // Caso haja invalidações, transfere-as ao objeto de validação
        if( Object.keys( invalidData ).length ) Object.assign( validation.invalidData, { maxDeckCards: invalidData } );

        // Indica se validação foi bem-sucedida ou não
        return !validation.invalidData.maxDeckCards;
      }

      /// Valida quantidade máxima de moedas gastas
      function validateMaxCoinsSpent() {
        // Identificadores
        var invalidData = {};

        // Valida quantidade de moedas gastas no baralho principal, e, se inválida, salva diferença entre ela e quantidade máxima
        if( coins.spent > coins.max ) invalidData.mainDeck = coins.spent - coins.max;

        // Valida quantidade de moedas gastas no baralho coadjuvante, e, se inválida, salva diferença entre ela e quantidade máxima
        if( sideDeck && sideDeck.coins.spent > sideDeck.coins.max ) invalidData.sideDeck = sideDeck.coins.spent - sideDeck.coins.max;

        // Caso haja invalidações, transfere-as ao objeto de validação
        if( Object.keys( invalidData ).length ) Object.assign( validation.invalidData, { maxCoinsSpent: invalidData } );

        // Indica se validação foi bem-sucedida ou não
        return !validation.invalidData.maxCoinsSpent;
      }

      /// Valida quantidade máxima de cartas iguais
      function validateMaxSameCards() {
        // Identificadores
        var [ invalidData, cardsNumber ] = [ {}, {} ],
            cardsToValidate = cards.concat( sideDeck?.cards ?? [] );

        // Itera pelas cartas a serem validadas
        for( let card of cardsToValidate ) {
          // Identificadores
          let targetName = card.name;

          // Caso carta seja um equipamento com tamanho, trata nome alvo para que ele não o discrimine
          if( card instanceof m.Item && card.content.typeset.size )
            targetName = targetName.replace( new RegExp( '(?<=^|-)' + card.content.typeset.size + '-' ), '' );

          // Define dados de carta atual, caso isso ainda não tenha sido feito
          cardsNumber[ targetName ] ??= {
            number: 0, max: card.content.typeset.availability
          };

          // Incrementa quantidade da carta alvo no baralho, e, caso ela tenha ultrapassado seu máximo permitido, salva quantidade excedente
          if( ++cardsNumber[ targetName ].number > cardsNumber[ targetName ].max )
            invalidData[ targetName ] = cardsNumber[ targetName ].number - cardsNumber[ targetName ].max;
        }

        // Caso haja invalidações, transfere-as ao objeto de validação
        if( Object.keys( invalidData ).length ) Object.assign( validation.invalidData, { maxSameCards: invalidData } );

        // Indica se validação foi bem-sucedida ou não
        return !validation.invalidData.maxSameCards;
      }

      /// Valida comando de comandantes
      function validateCommand() {
        // Identificadores
        var invalidData = { minCommandCards: {}, maxCommandCards: {} },
            commandScopes = commander.content.stats.command.scopes;

        // Itera por superescopos
        for( let superScope in commandScopes ) {
          // Identificadores
          let primitiveType = cards[ superScope + 's' ];

          // Validação de quantidade de tipos primitivos

          /// Mínima
          if( primitiveType.length < commandScopes[ superScope ].total.min )
            invalidData.minCommandCards[ superScope ] = commandScopes[ superScope ].total.min - primitiveType.length;

          /// Máxima
          if( primitiveType.length > commandScopes[ superScope ].total.max )
            invalidData.maxCommandCards[ superScope ] = primitiveType.length - commandScopes[ superScope ].total.max;

          // Validação de restrições especiais

          /// Filtra superescopos sem restrições especiais
          if( !commandScopes[ superScope ].total.constraint.description ) continue;

          /// Verifica se existem cartas no conjunto de baralhos que não atendem às restrições especiais
          if( !commandScopes[ superScope ].total.constraint.check( ...( cards[ superScope + 's' ].concat( sideDeck?.cards[ superScope + 's' ] ?? [] ) ) ) ) {
            // Inicia objeto de invalidação alvo, se aplicável
            invalidData.maxCommandCards.constraints ??= [];

            // Salva invalidação de uma restrição especial de superescopo alvo
            invalidData.maxCommandCards.constraints.push( superScope );
          }
        }

        // Validação dos escopos de criaturas
        for( let scope of [ [ 'race' ], [ 'role', 'type' ], [ 'experience', 'level' ] ] ) validateScopes( scope, 'creature' );

        // Validação dos escopos de equipamentos
        validateScopes( [ 'grade' ], 'item' );

        // Retorna objeto de validação
        return invalidData;

        // Valida escopos
        function validateScopes( keys, superScope ) {
          // Identificadores
          var linkedKeys = keys.reduce( ( accumulator, current ) => accumulator += current[ 0 ].toUpperCase() + current.slice( 1 ) ),
              targetScope = commandScopes[ superScope ][ linkedKeys ],
              scopeCards = cards[ superScope + 's' ],
              sideDeckScopeCards = sideDeck?.cards[ superScope + 's' ],
              cardValues = scopeCards.map(
                card => keys.reduce( ( accumulator, current ) => accumulator[ current ], card.content.typeset )
              ),
              sideDeckCardValues = sideDeckScopeCards?.map(
                card => keys.reduce( ( accumulator, current ) => accumulator[ current ], card.content.typeset )
              );

          // Itera por chaves do escopo alvo
          for( let value in targetScope ) {
            // Identificadores
            let scopeStats = targetScope[ value ],
                cardsFromValue = scopeCards.filter( ( card, index ) => cardValues[ index ] == value ),
                sideDeckCardsFromValue = sideDeckScopeCards?.filter( ( card, index ) => sideDeckCardValues[ index ] == value );

            // Validação da quantidade mínima de cartas com o valor alvo no baralho
            if( cardsFromValue.length < scopeStats.min ) {
              // Inicia objeto de invalidação alvo, se aplicável
              invalidData.minCommandCards[ linkedKeys ] ??= {};

              // Salva no objeto de invalidação diferença entre a quantidade mínima de cartas com o valor alvo e a no baralho
              invalidData.minCommandCards[ linkedKeys ][ value ] = scopeStats.min - cardsFromValue.length;
            }

            // Validação da quantidade máxima de cartas com o valor alvo no baralho
            if( cardsFromValue.length > scopeStats.max ) {
              // Inicia objeto de invalidação alvo, se aplicável
              invalidData.maxCommandCards[ linkedKeys ] ??= {};

              // Salva no objeto de invalidação diferença entre a quantidade máxima de cartas com o valor alvo e a no baralho
              invalidData.maxCommandCards[ linkedKeys ][ value ] = cardsFromValue.length - scopeStats.max;
            }

            // Se aplicável, validação de valores permitidos no baralho coadjuvante
            if( scopeStats.max == 0 && sideDeckCardsFromValue?.length ) {
              // Inicia objeto de invalidação alvo, se aplicável
              invalidData.maxCommandCards[ linkedKeys ] ??= {};

              // Salva no objeto de invalidação diferença entre a quantidade máxima de cartas com o valor alvo e a no conjunto de baralhos
              invalidData.maxCommandCards[ linkedKeys ][ value ] =
                ( invalidData.maxCommandCards[ linkedKeys ][ value ] ?? 0 ) + sideDeckCardsFromValue.length
            }
          }
        }
      }
    }

    /// Define quantidade máxima de moedas
    deck.setMaxCoins = function () {
      // Identificadores
      var mainDeck = this.type == 'main-deck' ? this : this.mainDeck;

      // Definição da quantidade máxima de moedas

      /// Do baralho principal
      mainDeck.coins.max = mainDeck.cards.commander?.content.stats.command.availableCoins.mainDeck ?? 0;

      /// Do baralho coadjuvante
      if( mainDeck.sideDeck ) mainDeck.sideDeck.coins.max = mainDeck.cards.commander?.content.stats.command.availableCoins.sideDeck ?? 0;
    }

    /// Ajusta moedas gastas do baralho e valor atual de moedas de suas cartas
    deck.adjustCoins = function ( eventData = {} ) {
      // Identificadores
      var { eventType, cards } = eventData,
          deckHasDwarf = this.cards.beings.some( card => card instanceof m.Dwarf ),
          deckHasElfChanneler = this.cards.beings.some( card => card instanceof m.Elf && card.content.stats.combativeness.channeling );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'add', 'remove' ].includes( eventType ) );
        m.oAssert( cards?.length );
        m.oAssert( cards.every( card => card instanceof m.Card ) );
      }

      // Zera valor de créditos totais e gastos do baralho
      for( let key of [ 'total', 'spent' ] ) this.credits[ key ] = 0;

      // Ativa característica 'ingeniousCraftsmanship', caso haja anões entre as cartas alvo, ou baralho tenha anões
      if( cards.some( card => card instanceof m.Dwarf ) || deckHasDwarf )
        m.Dwarf.triggerIngeniousCraftsmanship( this );

      // Ativa característica 'arcaneMastery', caso haja elfos canalizadores entre as cartas alvo, ou baralho tenha elfos canalizadores
      if( cards.some( card => card instanceof m.Elf && card.content.stats.combativeness.channeling ) || deckHasElfChanneler )
        m.Elf.triggerArcaneMastery( this );

      // Atualiza valor de créditos totais do baralho
      this.credits.total = this.credits.items + Object.values( this.credits.spells ).reduce( ( accumulator, current ) => accumulator += current, 0 );

      // Ajusta valor de moedas gastas no baralho
      this.coins.spent = this.cards.reduce( ( accumulator, current ) => accumulator += current.coins.current, 0 );

      // Caso evento seja de remoção, restaura valor das cartas removidas para o seu original
      if( eventType == 'remove' )
        for( let card of cards ) card.coins.current = card.coins.base;

      // Retorna moedas gastas no baralho
      return this.coins.spent;
    }

    // Atribuição de propriedades de 'coins'
    let coins = deck.coins;

    /// Quantidade máxima de moedas
    coins.max = 0;

    /// Quantidade gasta de moedas
    coins.spent = 0;

    // Atribuição de propriedades de 'credits'
    let credits = deck.credits;

    /// Créditos de equipamentos
    credits.items = 0;

    /// Créditos de magias
    credits.spells = {};

    /// Créditos totais
    credits.total = 0;

    /// Créditos gastos
    credits.spent = 0;

    // Atribuição de propriedades de 'credits.spells'
    let spellCredits = credits.spells;

    /// Atribui créditos por senda
    for( let path of [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ] ) spellCredits[ path ] = 0;

    // Atribuição de propriedades de 'cards'
    let cards = deck.cards;

    /// Comandante
    cards.commander = null;

    /// Entes
    cards.beings = [];

    /// Criaturas
    cards.creatures = [];

    /// Ativos
    cards.assets = [];

    /// Equipamentos
    cards.items = [];

    /// Magias
    cards.spells = [];
  }

  // Configura campo de inserção de modal para nomeação de um baralho
  Deck.configDeckNamesInput = function ( mainDeck, targetModal, targetInput ) {
    // Identificadores
    var { htmlInput } = targetInput;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( mainDeck instanceof m.MainDeck );
      m.oAssert( targetModal instanceof m.PromptModal );
      m.oAssert( targetInput instanceof PIXI.TextInput );
      m.oAssert( targetInput.name == 'decks-name-modal-input' );
    }

    // Configura propriedades do campo de inserção

    /// Valor inicial
    htmlInput.value = mainDeck.displayName;

    /// Texto de aviso
    targetInput.placeholder = m.languages.notices.getModalText( 'name-deck-set-placeholder' );

    /// Quantidade máxima de caracteres
    targetInput.maxLength = 30;

    /// Validade inicial
    if( htmlInput.value.length > 0 && htmlInput.value.length <= targetInput.maxLength ) targetInput.isValid = true;

    // Evento para limitar caracteres usáveis para espaços, dígitos e letras do alfabeto latino básico, suplementar e estendido-A
    targetInput.addListener( 'input', () =>
      htmlInput.value = htmlInput.value.trimStart().replace( /[^ 0-9A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u017E]/g, '' ).replace( /\s+/g, ' ' )
    );

    // Evento para definir validade do campo de inserção
    targetInput.addListener( 'input', () => targetInput.isValid = htmlInput.value.length > 0 && htmlInput.value.length <= targetInput.maxLength );
  }

  // Atribui nomes a um conjunto de baralhos, enquanto definidos por uma modal
  Deck.setDeckNames = function ( mainDeck ) {
    // Identificadores
    var promptModal = m.app.pixi.stage.children.find( child => child.name == 'decks-name-modal' ),
        deckNamesInput = promptModal?.content.inputsSet.textInput,
        { htmlInput } = deckNamesInput ?? {};

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( mainDeck instanceof m.MainDeck );
      m.oAssert( htmlInput );
    }

    // Encerra função caso campo de inserção esteja inválido
    if( !deckNamesInput.isValid ) return false;

    // Define nome de exibição do baralho principal
    mainDeck.displayName = htmlInput.value.trim();

    // Define nomes de identificação do conjunto de baralhos
    mainDeck.syncDeckNames();

    // Indica que nomes foram atribuídos com sucesso
    return true;
  }

  // Reproduz um baralho principal, através de seus dados de registro
  Deck.load = function ( owner, deckData = {} ) {
    // Identificadores pré-validação
    var { name, displayName, isStarter, cards, createdDate, modifiedDate, matches, sideDeck } = deckData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( owner instanceof m.GameUser );
      m.oAssert( deckData.constructor == Object );
      m.oAssert( Object.keys( deckData ).every( key => [
        'name', 'displayName', 'isStarter', 'cards', 'createdDate', 'modifiedDate', 'matches', 'sideDeck'
      ].includes( key ) ) );
      m.oAssert( [ name, displayName, createdDate, modifiedDate ].every( value => value && typeof value == 'string' ) );
      m.oAssert( typeof isStarter == 'boolean' );
      m.oAssert( Array.isArray( cards ) );
      m.oAssert( matches.constructor == Object );
      m.oAssert( !sideDeck || sideDeck.constructor == Object );
    }

    // Identificadores pós-validação
    var mainDeck = new m.MainDeck( { name: name, displayName: displayName, owner: owner } );

    // Adiciona ao baralho principal suas cartas registradas
    mainDeck.add( cards.map( card => m.Card.load( card ) ), true );

    // Atribui ao baralho principal dados sobre partidas
    Object.assign( mainDeck.matches, matches );

    // Para caso haja um baralho coadjuvante
    if( sideDeck ) {
      // Gera baralho coadjuvante
      new m.SideDeck( { name: sideDeck.name, mainDeck: mainDeck } );

      // Adiciona ao baralho coadjuvante suas cartas registradas
      mainDeck.sideDeck.add( sideDeck.cards.map( card => m.Card.load( card ) ), true );
    }

    // Atribui ao baralho principal datas de criação, de modificação e indicação de se ele é um baralho predefinido
    for( let key of [ 'isStarter', 'createdDate', 'modifiedDate' ] ) mainDeck[ key ] = deckData[ key ];

    // Retorna baralho criado
    return mainDeck;
  }
}

/// Propriedades do protótipo
Deck.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: Deck }
} );
