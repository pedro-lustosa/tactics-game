// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const SideDeck = function ( config = {} ) {
  // Iniciação de propriedades
  SideDeck.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.mainDeck instanceof m.MainDeck );
    m.oAssert( config.name || config.mainDeck.displayName );
    m.oAssert( !config.mainDeck.sideDeck );
  }

  // Atribuição de propriedades pré-superconstrutor

  /// Atribuição do dono
  this.owner = config.mainDeck.owner;

  /// Atribuição do baralho principal
  this.mainDeck = config.mainDeck;

  /// Atribuição do baralho coadjuvante de seu baralho principal
  this.mainDeck.sideDeck = this;

  /// Atribuição de suas moedas máximas, para caso baralho principal já tenha um comandante
  if( this.mainDeck.cards.commander ) this.setMaxCoins();

  // Superconstrutor
  m.Deck.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Quantidade mínima de cartas permitidas
  SideDeck.minCards = 0;

  // Quantidade máxima de cartas permitidas
  SideDeck.maxCards = 10;

  // Iniciação de propriedades
  SideDeck.init = function ( deck ) {
    // Chama 'init' ascendente
    m.Deck.init( deck );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deck instanceof SideDeck );

    // Atribuição de propriedades iniciais

    /// Tipo
    deck.type = 'side-deck';

    /// Baralho principal vinculado a este baralho
    deck.mainDeck = null;
  }
}

/// Propriedades do protótipo
SideDeck.prototype = Object.create( m.Deck.prototype, {
  // Construtor
  constructor: { value: SideDeck }
} );
