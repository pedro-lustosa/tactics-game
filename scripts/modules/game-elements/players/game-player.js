// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GamePlayer = function ( config = {} ) {
  // Iniciação de propriedades
  GamePlayer.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'odd', 'even' ].includes( config.parity ) );
    m.oAssert( config.user instanceof m.GameUser );
  }

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  this.name = config.parity + '-player';

  /// Atribuição da paridade e do usuário
  for( let property of [ 'parity', 'user' ] ) this[ property ] = config[ property ];
}

/// Propriedades do construtor
defineProperties: {
  // Jogador atual
  GamePlayer.current = null;

  // Iniciação de propriedades
  GamePlayer.init = function ( player ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( player instanceof GamePlayer );

    // Atribuição de propriedades iniciais

    /// Nome
    player.name = '';

    /// Paridade
    player.parity = '';

    /// Indica se jogador deve vencer a rodada em que estiver
    player.isToWin = false;

    /// Pontos de destino
    player.fatePoints = {};

    /// Usuário
    player.user = null;

    /// Baralho do jogo
    player.gameDeck = null;

    /// Grade do campo
    player.fieldGrid = null;

    /// Rol
    player.pool = null;

    // Atribuição de propriedades de 'fatePoints'
    let fatePoints = player.fatePoints;

    /// Pontos de destino atuais
    fatePoints.current = {};

    /// Pontos de destino máximos
    fatePoints.max = 9;

    /// Concede ao jogador pontos de destino
    fatePoints.increase = function ( number, type = 'intransitive' ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( [ 'intransitive', 'transitive' ].includes( type ) );
      }

      // Identificadores
      var currentFatePoints = this.current.get(),
          changeNumber = Math.min( this.max - currentFatePoints, number );

      // Não executa função caso não haja pontos de destino adicionáveis
      if( changeNumber <= 0 ) return this.current;

      // Não adiciona pontos de destino caso magia 'O Inverno' invalide operação
      if( !m.SpellTheWinter.validateFatePointGain( player ) ) return this.current;

      // Caso pontos de destino a serem adicionados sejam intransitivos, se aplicável converte-os para transitivos
      if( type == 'intransitive' && m.SpellTheHoard.validateFatePointConversion( player ) ) type = 'transitive';

      // Sinaliza início do aumento de pontos de destino
      m.events.fatePointsChangeStart.increase.emit( player, { changeNumber: changeNumber, type: type } );

      // Adiciona pontos de destino
      this.current[ type ] += changeNumber;

      // Sinaliza fim do aumento de pontos de destino
      m.events.fatePointsChangeEnd.increase.emit( player, { changeNumber: changeNumber, type: type } );

      // Retorna pontos de destino atuais
      return this.current;
    }

    /// Reduz do jogador pontos de destino
    fatePoints.decrease = function ( number ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( number ) && number >= 0 );

      // Identificadores
      var currentFatePoints = this.current.get(),
          changeNumber = Math.min( currentFatePoints, number );

      // Não executa função caso não haja pontos de destino reduzíveis
      if( changeNumber <= 0 ) return this.current;

      // Sinaliza início da redução de pontos de destino
      m.events.fatePointsChangeStart.decrease.emit( player, { changeNumber: -changeNumber } );

      // Itera por tipos de pontos de destino alvos da redução
      for( let type of [ 'intransitive', 'transitive' ] ) {
        // Redução dos pontos de destino
        while( number && this.current[ type ] ) {
          // Decrementa pontos de destino atuais
          this.current[ type ]--;

          // Decrementa pontos de destino a serem reduzidos
          number--;
        }
      }

      // Sinaliza fim da redução de pontos de destino
      m.events.fatePointsChangeEnd.decrease.emit( player, { changeNumber: -changeNumber } );

      // Retorna pontos de destino atuais
      return this.current;
    }

    // Atribuição de propriedades de 'fatePoints.current'
    let currentFatePoints = fatePoints.current;

    /// Pontos de destino intransitivos
    currentFatePoints.intransitive = 0;

    /// Pontos de destino transitivos
    currentFatePoints.transitive = 0;

    /// Pontos de destino totais
    currentFatePoints.get = function () {
      return this.intransitive + this.transitive;
    }
  }
}

/// Propriedades do protótipo
GamePlayer.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: GamePlayer }
} );
