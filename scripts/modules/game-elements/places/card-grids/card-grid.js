// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const CardGrid = function ( config = {} ) {
  // Iniciação de propriedades
  CardGrid.init( this );

  // Identificadores
  var { name, environment, owner, rows, columns, namePrefix = '', isReverse = false } = config,
      [ gridRows, gridColumns ] = [ {}, {} ];


  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ name, namePrefix ].every( value => !value || typeof value == 'string' ) );
    m.oAssert( [ m.Field, m.Table, m.Reserve ].some( constructor => environment instanceof constructor ) );
    m.oAssert( !owner || owner instanceof m.GamePlayer );
    m.oAssert( [ rows, columns ].every( number => Number.isInteger( number ) && number >= 1 && number <= 6 ) );
    m.oAssert( typeof isReverse == 'boolean' );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Ajuste do nome
  if( name ) this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;

  /// Atribuição do ambiente e do dono
  for( let property of [ 'environment', 'owner' ] ) this[ property ] = config[ property ] || null;

  /// Definição das fileiras
  setRows: {
    // Caso haja apenas uma fileira, atribui a ela letra 'C'
    if( rows == 1 )
      gridRows[ 'C' ] = []

    // Do contrário, itera por letras maiúsculas a partir de 'A', e atribui às fileiras o resultado da iteração
    else
      for( let row = 65; row <= 64 + rows; row++ ) gridRows[ String.fromCharCode( row ) ] = [];

    // Atribuição das fileiras à grade
    this.rows = gridRows;
  }

  /// Definição das colunas
  setColumns: {
    // Itera por números a partir de '1', e atribui às colunas o resultado da iteração
    for( let column = 1; column <= columns; column++ ) gridColumns[ column ] = [];

    // Atribuição das colunas à grade
    this.columns = gridColumns;
  }

  /// Definição das casas
  this.slots = ( function () {
    // Identificadores
    var slotsArray = [],
        slotConstructor = !( environment instanceof m.Field ) ? m.PoolSlot : this.owner ? m.FieldGridSlot : m.EngagementZone,
        slotPrefix = namePrefix ? namePrefix + '-' : '',
        fromCharCode = Object.keys( this.rows )[0].charCodeAt(),
        toNextCharCode = ( char ) => ++char;

    // Ajusta configurações para o caso de grades que precisem ser alfabetadas em ordem inversa
    if( isReverse ) [ fromCharCode, toNextCharCode ] = [ 64 + rows, ( char ) => --char ];

    // Itera por fileiras da grade
    for( let row = 1, charCode = fromCharCode; row <= rows; row++, charCode = toNextCharCode( charCode ) ) {
      // Itera por colunas da grade
      for( let column = 1; column <= columns; column++ ) {
        // Gera casa
        let rowChar = String.fromCharCode( charCode ),
            slot = this.addChild( new slotConstructor( {
              name: slotPrefix + rowChar + column,
              owner: this.owner,
              environment: environment
            } ) );

        // Posiciona casa
        slot.position.set(
          slot.width * ( column - 1 ) + environment.constructor.slotMarginX * ( column - 1 ),
          slot.height * ( row - 1 ) + environment.constructor.slotMarginY * ( row - 1 )
        );

        // Captura casa

        /// Nas fileiras de sua grade
        this.rows[ rowChar ].push( slot );

        /// Nas colunas de sua grade
        this.columns[ column ].push( slot );

        /// No arranjo de casas de sua grade
        slotsArray.push( slot );
      }
    }

    // Retorna arranjo de casas da grade
    return slotsArray;
  } ).call( this );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CardGrid.init = function ( cardGrid ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( cardGrid instanceof CardGrid );

    // Atribuição de propriedades iniciais

    /// Nome
    cardGrid.name = 'grid';

    /// Ambiente
    cardGrid.environment = undefined;

    /// Dono
    cardGrid.owner = undefined;

    /// Fileiras
    cardGrid.rows = {};

    /// Colunas
    cardGrid.columns = {};

    /// Casas
    cardGrid.slots = [];

    /// Cartas
    cardGrid.cards = [];
  }
}

/// Propriedades do protótipo
CardGrid.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: CardGrid }
} );
