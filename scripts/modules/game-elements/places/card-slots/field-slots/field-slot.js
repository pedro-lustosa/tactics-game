// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const FieldSlot = function ( config = {} ) {
  // Identificadores
  var { environment } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( environment instanceof m.Field );

  // Superconstrutor
  m.CardSlot.call( this, config );

  // Configurações pós-superconstrutor

  /// Medidas
  [ this.width, this.height ] = [ environment.constructor.slotWidth, environment.constructor.slotHeight ];

  /// Interatividade
  this.interactive = true;

  // Eventos

  /// Para exibir conteúdo da casa na moldura ativa
  this.addListener( 'click', m.GameFrame.showCardSlot );

  /// Para selecionar casa como alvo de uma ação
  this.addListener( 'dblclick', m.GameAction.selectTarget );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FieldSlot.init = function ( slot ) {
    // Chama 'init' ascendente
    m.CardSlot.init( slot );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( slot instanceof FieldSlot );

    // Atribuição de propriedades iniciais

    /// Vinculatividade
    Object.oAssignByCopy( slot, m.mixinAttachable.call( slot ) );

    /// Retorna casa situada em uma certa fileira e coluna em relação a esta
    slot.getSlotAt = function ( row, column ) {
      // Identificadores
      var field = this.environment,
          fieldSlots = field.getSlots(),
          fieldRows = fieldSlots.oChunk( value => value.name.endsWith( '1' ) ),
          slotRowIndex = fieldRows.findIndex( row => row.includes( this ) ),
          slotColumnIndex = fieldRows[ slotRowIndex ].findIndex( slot => slot == this ),
          targetSlot;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ row, column ].every( value => Number.isInteger( value ) ) );

      // Define casa alvo como a que corresponder à fileira e à coluna passada
      targetSlot = fieldRows[ slotRowIndex + row ]?.[ slotColumnIndex + column ] ?? null;

      // Retorna casa alvo
      return targetSlot;
    }

    /// Retorna casas adjacentes a esta
    slot.getAdjacentSlots = function () {
      // Identificadores
      var adjacentSlots = [],
          slotCoordinates = [ [ 1, 0 ], [ 0, 1 ], [ -1, 0 ], [ 0, -1 ] ];

      // Captura casas adjacentes à alvo
      for( let coordinate of slotCoordinates ) adjacentSlots.push( this.getSlotAt( ...coordinate ) );

      // Remove do arranjo de casas adjacentes valores nulos
      adjacentSlots = adjacentSlots.filter( slot => slot );

      // Retorna arranjo de casas adjacentes
      return adjacentSlots;
    }

    /// Retorna um identificador único para a casa em relação a uma partida
    slot.getMatchId = function () {
      // Retorna nome da casa
      return this.name;
    }

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = slot.maxAttachments;

    /// Magias
    maxAttachments.spell = 1;

    /// Mana
    maxAttachments.mana = 20;
  }

  // Retorna uma casa a partir de seu identificador na partida atual
  FieldSlot.getFromMatchId = function ( matchId ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( m.GameMatch.current );
      m.oAssert( matchId && typeof matchId == 'string' );
      m.oAssert( matchId.length == 2 || matchId.length == 5 );
    }

    // Identificadores
    var field = m.GameMatch.current.environments.field,
        fieldSlots = field.getSlots();

    // Retorna casa com o nome do identificador passado
    return fieldSlots.find( slot => slot.name == matchId ) ?? false;
  }

  // Retorna distância entre duas casas do campo
  FieldSlot.getDistanceFrom = function ( firstSlot, secondSlot ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( [ firstSlot, secondSlot ].every( slot => slot instanceof this ) );

    // Identificadores
    var fieldSlots = firstSlot.environment.getSlots(),
        fieldRows = fieldSlots.oChunk( value => value.name.endsWith( '1' ) ),
        firstRowIndex = fieldRows.findIndex( row => row.includes( firstSlot ) ),
        secondRowIndex = fieldRows.findIndex( row => row.includes( secondSlot ) ),
        firstColumnIndex = fieldRows[ firstRowIndex ].findIndex( slot => slot == firstSlot ),
        secondColumnIndex = fieldRows[ secondRowIndex ].findIndex( slot => slot == secondSlot ),
        distance = 0;

    // Acrescenta à distância a quantidade de casas que separam verticalmente as casas passadas
    distance += Math.abs( firstRowIndex - secondRowIndex );

    // Acrescenta à distância a quantidade de casas que separam horizontalmente as casas passadas
    distance += Math.abs( firstColumnIndex - secondColumnIndex );

    // Retorna distância
    return distance;
  }
}

/// Propriedades do protótipo
FieldSlot.prototype = Object.create( m.CardSlot.prototype, {
  // Construtor
  constructor: { value: FieldSlot }
} );
