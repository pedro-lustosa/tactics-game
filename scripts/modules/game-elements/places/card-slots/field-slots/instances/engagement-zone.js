// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const EngagementZone = function ( config = {} ) {
  // Iniciação de propriedades
  EngagementZone.init( this );

  // Identificadores
  var { owner = null } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( owner === null );

  // Superconstrutor
  m.FieldSlot.call( this, config );

  // Configurações pós-superconstrutor

  /// Medidas
  [ this.width, this.height ] = [ EngagementZone.width, EngagementZone.height ];

  /// Dimensionamento do plano de fundo
  for( let size of [ 'width', 'height' ] ) this.background[ size ] = this[ size ];

  /// Inserção do plano de fundo
  this.addChild( this.background );
}

/// Propriedades do construtor
defineProperties: {
  // Limite de cartas em uma casa, por jogador
  EngagementZone.maxCards = 6;

  // Largura de casas
  EngagementZone.width = m.Card.sizes.fieldSlot.width;

  // Altura de casas
  EngagementZone.height = 160;

  // Margem entre as áreas dos jogadores
  EngagementZone.innerMargin = 10;

  // Iniciação de propriedades
  EngagementZone.init = function ( slot ) {
    // Chama 'init' ascendente
    m.FieldSlot.init( slot );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( slot instanceof EngagementZone );

    // Atribuição de propriedades iniciais

    /// Plano de fundo
    slot.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'engagement-zone' ] );

    /// Cartas em casa
    slot.cards = [];

    /// Retorna casa adjacente à zona de engajamento que esteja na grade do jogador cuja paridade é a passada
    slot.getAdjacentGridSlots = function ( ...parities ) {
      // Identificadores
      var field = this.environment,
          parities = parities.length ? parities : [ 'odd', 'even' ],
          cardSlots = [];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( parities.every( parity => [ 'odd', 'even' ].includes( parity ) ) );

      // Itera por paridades
      for( let parity of parities ) {
        // Itera por casas da grade da paridade alvo
        for( let slot of field.grids[ parity ].slots ) {
          // Captura nome abreviado da casa alvo
          let slotName = slot.name.slice( -2 );

          // Filtra casas da retaguarda
          if( slotName[ 0 ] != 'B' ) continue;

          // Filtra casas de colunas diferentes da em que zona de engajamento está
          if( slotName[ 1 ] != this.name[ 1 ] ) continue;

          // Adiciona casa ao arranjo de casas
          cardSlots.push( slot );

          // Encerra iteração por casas da grade alvo
          break;
        }
      }

      // Retorna casa do jogador da paridade explicitada
      return cardSlots.length < 2 ? cardSlots.pop() : cardSlots;
    }

    /// Desloca entes físicos na vanguarda para a zona de engajamento, se ainda não ocupada por nenhum ente de seu jogador
    slot.pullBeings = function () {
      // Itera por paridades
      for( let parity of [ 'odd', 'even' ] ) {
        // Filtra jogadores com 1 ou mais entes na zona de engajamento
        if( this.cards[ parity ].length ) continue;

        // Captura ente na casa da grade da paridade alvo adjacente à zona de engajamento
        let targetBeing = this.getAdjacentGridSlots( parity ).card;

        // Para caso ente alvo seja um físico
        if( targetBeing instanceof m.PhysicalBeing ) {
          // Desloca ente alvo para zona de engajamento
          this.environment.put( targetBeing, this.name );

          // Caso ente alvo esteja com uma ação 'engajar' em andamento, conclui-a
          m.GameAction.currents.find( action => action.committer == targetBeing && action instanceof m.ActionEngage )?.complete( 'finish' );
        }
      }
    }

    /// Atualiza posição e tamanho de cartas em zona de engajamento
    slot.updateCards = function ( ...parities ) {
      // Identificadores
      var field = this.environment,
          parities = parities.length ? parities : [ 'odd', 'even' ],
          areaWidth = EngagementZone.width,
          areaHeight = ( EngagementZone.height - EngagementZone.innerMargin ) * .5,
          topParity = field.grids.odd.y < field.grids.even.y ? 'odd' : 'even',
          startY = {
            odd: topParity == 'odd' ? 0 : EngagementZone.height * .5 + EngagementZone.innerMargin * .5,
            even: topParity == 'even' ? 0 : EngagementZone.height * .5 + EngagementZone.innerMargin * .5
          },
          putAt = {
            startX: ( symbol ) => 0,
            centerX: ( symbol ) => areaWidth * .5 - symbol.width * .5,
            endX: ( symbol ) => areaWidth - symbol.width,
            closeY: ( symbol, parity ) => parity == topParity ? areaHeight - symbol.height + startY[ parity ] : startY[ parity ],
            centerY: ( symbol, parity ) => areaHeight * .5 - symbol.height * .5 + startY[ parity ],
            farY: ( symbol, parity ) => parity == topParity ? startY[ parity ] : areaHeight - symbol.height + startY[ parity ]
          },
          adjustsByLength = {
            1: {
              scaleValue: 1,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                putAt.centerX( symbol ),
                putAt.centerY( symbol, parity )
              )
            },
            2: {
              scaleValue: .8,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                index == 0 ? putAt.startX( symbol ) : putAt.endX( symbol ),
                putAt.centerY( symbol, parity )
              )
            },
            3: {
              scaleValue: .7,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                index == 0 ? putAt.startX( symbol ) : index == 1 ? putAt.endX( symbol ) : putAt.centerX( symbol ),
                index == 2 ? putAt.closeY( symbol, parity ) : putAt.farY( symbol, parity )
              )
            },
            4: {
              scaleValue: .6,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                !( index % 2 ) ? putAt.startX( symbol ) : putAt.endX( symbol ),
                index < 2 ? putAt.farY( symbol, parity ) : putAt.closeY( symbol, parity )
              )
            },
            5: {
              scaleValue: .6,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                index == 4 ? putAt.centerX( symbol ) : !( index % 2 ) ? putAt.startX( symbol ) : putAt.endX( symbol ),
                index == 4 ? putAt.centerY( symbol, parity ) : index < 2 ? putAt.farY( symbol, parity ) : putAt.closeY( symbol, parity )
              )
            },
            6: {
              scaleValue: .58,
              setPosition: ( symbol, parity, index ) => symbol.position.set(
                [ 0, 3 ].includes( index ) ? putAt.startX( symbol ) : [ 1, 4 ].includes( index ) ? putAt.centerX( symbol ) : putAt.endX( symbol ),
                index < 3 ? putAt.farY( symbol, parity ) : putAt.closeY( symbol, parity )
              )
            }
          };

      // Validação
      if( m.app.isInDevelopment ) parities.every( parity => [ 'odd', 'even' ].includes( parity ) );

      // Itera por paridades
      for( let parity of parities ) {
        // Captura símbolos do jogador alvo na zona de engajamento alvo
        let targetSymbols = this.cards[ parity ].map( card => card.body.symbol );

        // Itera por símbolos
        for( let i = 0; i < targetSymbols.length; i++ ) {
          // Identificadores
          let symbol = targetSymbols[ i ];

          // Ajusta dimensões do símbolo alvo
          symbol.scale.set( adjustsByLength[ targetSymbols.length ].scaleValue );

          // Ajusta posicionamento do símbolo alvo
          adjustsByLength[ targetSymbols.length ].setPosition( symbol, parity, i );
        }
      }

      // Retorna zona de engajamento
      return this;
    }

    /// Retorna jogador que atualmente controla zona de engajamento
    slot.getController = function () {
      // Identificadores
      var { players: matchPlayers } = m.GameMatch.current,
          oddCards = this.cards.odd.length,
          evenCards = this.cards.even.length;

      // Retorna controlador atual da zona de engajamento
      return oddCards > evenCards ? matchPlayers.odd : evenCards > oddCards ? matchPlayers.even : null;
    }

    // Atribuição de propriedades de 'cards'
    let cards = slot.cards;

    /// Cartas ímpares
    cards.odd = [];

    /// Cartas pares
    cards.even = [];
  }
}

/// Propriedades do protótipo
EngagementZone.prototype = Object.create( m.FieldSlot.prototype, {
  // Construtor
  constructor: { value: EngagementZone }
} );
