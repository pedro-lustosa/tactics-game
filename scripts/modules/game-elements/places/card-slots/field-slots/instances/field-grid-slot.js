// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const FieldGridSlot = function ( config = {} ) {
  // Iniciação de propriedades
  FieldGridSlot.init( this );

  // Identificadores
  var { owner } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( owner instanceof m.GamePlayer );

  // Superconstrutor
  m.FieldSlot.call( this, config );

  // Configurações pós-superconstrutor

  /// Medidas
  [ this.width, this.height ] = [ FieldGridSlot.width, FieldGridSlot.height ];

  /// Dimensionamento do plano de fundo
  for( let size of [ 'width', 'height' ] ) this.background[ size ] = this[ size ];

  /// Inserção do plano de fundo
  this.addChild( this.background );

  /// Definição das bordas
  this.toStyle( 'empty' );
}

/// Propriedades do construtor
defineProperties: {
  // Largura de casas
  FieldGridSlot.width = m.Card.sizes.fieldSlot.width;

  // Altura de casas
  FieldGridSlot.height = m.Card.sizes.fieldSlot.height;

  // Iniciação de propriedades
  FieldGridSlot.init = function ( slot ) {
    // Chama 'init' ascendente
    m.FieldSlot.init( slot );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( slot instanceof FieldGridSlot );

    // Atribuição de propriedades iniciais

    /// Plano de fundo
    slot.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'card-slots-body' ] );

    /// Carta em casa
    slot.card = null;

    /// Retorna zona de engajamento da coluna da casa
    slot.getColumnEngagementZone = function () {
      // Identificadores
      var field = this.environment,
          slotNumber = this.name.slice( -1 );

      // Retorna zona de engajamento cujo dígito final corresponder com o da casa alvo
      for( let engagementZone of field.divider.slots )
        if( engagementZone.name.slice( -1 ) == slotNumber ) return engagementZone;
    }

    /// Retorna jogador que controla casa
    slot.getController = function () {
      // Retorna dono da casa
      return this.owner;
    }
  }
}

/// Propriedades do protótipo
FieldGridSlot.prototype = Object.create( m.FieldSlot.prototype, {
  // Construtor
  constructor: { value: FieldGridSlot }
} );
