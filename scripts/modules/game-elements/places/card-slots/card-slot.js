// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const CardSlot = function ( config = {} ) {
  // Identificadores
  var { name, environment, owner } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( name && typeof name == 'string' );

  // Superconstrutor
  PIXI.Graphics.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome, ambiente e dono
  for( let property of [ 'name', 'environment', 'owner' ] ) this[ property ] = config[ property ] || this[ property ];
}

/// Propriedades do construtor
defineProperties: {
  // Espessura de bordas
  CardSlot.borderSize = 2;

  // Iniciação de propriedades
  CardSlot.init = function ( slot ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( slot instanceof CardSlot );

    // Atribuição de propriedades iniciais

    /// Nome
    slot.name = '';

    /// Ambiente
    slot.environment = null;

    /// Dono
    slot.owner = null;

    /// Muda aparência da casa para que marque a atividade de sua carta
    slot.toStyle = function ( style ) {
      // Identificadores
      var { borderSize } = CardSlot;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !( this instanceof m.EngagementZone ) );
        m.oAssert( [ 'inUse', 'active', 'suspended', 'withdrawn', 'ended', 'empty' ].includes( style ) );
      }

      // Remoção de elementos

      /// Verso da carta de casa
      this.card?.removeChild( this.card.body.back );

      /// Bordas da casa
      this.clear();

      // Redefinição das bordas da casa e da face visível de sua carta
      switch( style ) {
        case 'inUse':
          this.lineStyle( borderSize, m.data.colors.green, 1, 1 );
          break;
        case 'active':
          this.lineStyle( borderSize, m.data.colors.blue, 1, 1 );
          break;
        case 'suspended':
          this.lineStyle( borderSize, m.data.colors.yellow, 1, 1 );
          break;
        case 'withdrawn':
          this.lineStyle( borderSize, m.data.colors.red, 1, 1 );
          break;
        case 'ended':
          this.card?.addChild( this.card.body.back ) ?? this.lineStyle( borderSize, m.data.colors.semiwhite1, 1, 1 );
          break;
        case 'empty':
          this.lineStyle( 1, m.data.colors.semiwhite1, 1, 1 );
      }

      // Renderização das bordas
      this.drawRect( 0, 0, this.width, this.height );

      // Retorna casa
      return this;
    }
  }

  // Atualiza aparência de casa da carta
  CardSlot.updateSlotStyle = function ( eventData ) {
    // Identificadores
    var { eventTarget, eventCategory, eventType } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventTarget instanceof m.Card );
      m.oAssert( eventTarget.activity == 'unlinked' || eventTarget.slot instanceof m.CardSlot );
      m.oAssert( [ 'card-activity', 'card-use' ].some( category => eventCategory == category ) );
    }

    // Filtra cartas alheias
    if( eventTarget.activity == 'unlinked' ) return;

    // Filtra entes ativos, visto que a aparência de suas casas já é definida na função de ocupação de casas do campo
    if( eventTarget instanceof m.Being && eventTarget.activity == 'active' ) return;

    // Delega mudança de estilo para casa em que carta está e segundo evento gerado
    return eventTarget.slot.toStyle( eventType == 'use-in' ? 'inUse' : eventTarget.activity );
  }
}

/// Propriedades do protótipo
CardSlot.prototype = Object.create( PIXI.Graphics.prototype, {
  // Construtor
  constructor: { value: CardSlot },
  // Largura
  width: {
    value: 0, writable: true, enumerable: true
  },
  // Altura
  height: {
    value: 0, writable: true, enumerable: true
  }
} );
