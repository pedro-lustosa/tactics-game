// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const PoolSlot = function ( config = {} ) {
  // Iniciação de propriedades
  PoolSlot.init( this );

  // Identificadores
  var { environment, owner } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ m.Table, m.Reserve ].some( constructor => environment instanceof constructor ) );
    m.oAssert( owner instanceof m.GamePlayer );
  }

  // Superconstrutor
  m.CardSlot.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição de medidas
  [ this.width, this.height ] = [ PoolSlot.width, PoolSlot.height ];

  /// Dimensionamento do plano de fundo
  for( let size of [ 'width', 'height' ] ) this.background[ size ] = this[ size ];

  /// Inserção do plano de fundo
  this.addChild( this.background );

  /// Definição das bordas
  this.toStyle( 'empty' );
}

/// Propriedades do construtor
defineProperties: {
  // Largura
  PoolSlot.width = m.Card.sizes.poolSlot.width;

  // Altura
  PoolSlot.height = m.Card.sizes.poolSlot.height;

  // Iniciação de propriedades
  PoolSlot.init = function ( slot ) {
    // Chama 'init' ascendente
    m.CardSlot.init( slot );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( slot instanceof PoolSlot );

    // Atribuição de propriedades iniciais

    /// Plano de fundo
    slot.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'card-slots-body' ] );

    /// Carta em casa
    slot.card = null;
  }
}

/// Propriedades do protótipo
PoolSlot.prototype = Object.create( m.CardSlot.prototype, {
  // Construtor
  constructor: { value: PoolSlot }
} );
