// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const Environment = function ( config = {} ) {
  // Identificadores
  var { name } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( this.name && typeof this.name == 'string' );
    m.oAssert( !name || typeof name == 'string' );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do nome
  if( name ) this.name = name.endsWith( '-' + this.name ) ? name : name + '-' + this.name;
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Environment.init = function ( environment ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( environment instanceof Environment );

    // Atribuição de propriedades iniciais

    /// Nome
    environment.name = '';
  }
}

/// Propriedades do protótipo
Environment.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: Environment }
} );
