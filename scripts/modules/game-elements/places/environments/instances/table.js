// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Table = function ( config = {} ) {
  // Iniciação de propriedades
  Table.init( this );

  // Identificadores
  var { pool } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( pool instanceof m.Pool );

  // Configurações pré-superconstrutor

  /// Ajuste do nome
  this.name = `${ pool.parity }-${ this.name }`;

  // Superconstrutor
  m.Environment.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição do dono e da paridade
  for( let keyName of [ 'owner', 'parity' ] ) this[ keyName ] = pool[ keyName ];

  /// Geração e inserção da grade
  this.grid = this.addChild( new m.CardGrid( {
    name: 'table-grid',
    owner: this.owner,
    environment: this,
    rows: Table.gridRowsQuantity,
    columns: Table.gridColumnsQuantity,
    namePrefix: Table.charId + ( this.parity == 'odd' ? 1 : 2 )
  } ) );
}

/// Propriedades do construtor
defineProperties: {
  // Letra identificadora
  Table.charId = 'T';

  // Quantidade de fileiras de grades
  Table.gridRowsQuantity = 5;

  // Quantidade de colunas de grades
  Table.gridColumnsQuantity = 6;

  // Espaçamento horizontal entre casas
  Table.slotMarginX = 25.5;

  // Espaçamento vertical entre casas
  Table.slotMarginY = 12.75;

  // Iniciação de propriedades
  Table.init = function ( table ) {
    // Chama 'init' ascendente
    m.Environment.init( table );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( table instanceof Table );

    // Atribuição de propriedades iniciais

    /// Nome
    table.name = 'table';

    /// Dono
    table.owner = null;

    /// Paridade
    table.parity = '';

    /// Grade
    table.grid = null;
  }
}

/// Propriedades do protótipo
Table.prototype = Object.create( m.Environment.prototype, {
  // Construtor
  constructor: { value: Table }
} );
