// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Field = function ( config = {} ) {
  // Iniciação de propriedades
  Field.init( this );

  // Identificadores pré-validação
  var { owners, upperGrid = 'odd' } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( Object.values( owners ).every( owner => owner instanceof m.GamePlayer ) );
    m.oAssert( [ 'odd', 'even' ].includes( upperGrid ) );
  }

  // Identificadores pós-validação
  var lowerGrid = upperGrid == 'odd' ? 'even' : 'odd';

  // Superconstrutor
  m.Environment.call( this, config );

  // Configurações pós-superconstrutor

  /// Inserção do plano de fundo
  this.addChild( this.background );

  /// Configurações das grades
  gridsConfig: {
    // Geração
    let grids = this.grids = {
      odd: new m.CardGrid( {
        name: 'field-odd-grid',
        owner: owners.odd,
        environment: this,
        rows: Field.gridRowsQuantity,
        columns: Field.gridColumnsQuantity,
        namePrefix: Field.charId + 1,
        isReverse: upperGrid == 'even'
      } ),
      even: new m.CardGrid( {
        name: 'field-even-grid',
        owner: owners.even,
        environment: this,
        rows: Field.gridRowsQuantity,
        columns: Field.gridColumnsQuantity,
        namePrefix: Field.charId + 2,
        isReverse: upperGrid == 'odd'
      } )
    };

    // Posicionamento

    /// Grade superior
    grids[ upperGrid ].position.set( this.width * .5 - grids[ upperGrid ].width * .5, Field.paddingY );

    /// Grade inferior
    grids[ lowerGrid ].position.set( this.width * .5 - grids[ lowerGrid ].width * .5, this.height - ( grids[ lowerGrid ].height + Field.paddingY ) );

    // Vincula grades a seu respectivo dono, e as inseri no campo
    for( let parity of [ 'odd', 'even' ] ) owners[ parity ].fieldGrid = this.addChild( grids[ parity ] );
  }

  /// Configurações da divisória
  dividerConfig: {
    // Geração e inserção
    let divider = this.divider = this.addChild( new m.CardGrid( {
      name: 'divider-grid',
      environment: this,
      rows: 1,
      columns: Field.gridColumnsQuantity
    } ) );

    // Posicionamento
    divider.position.set( this.grids.odd.x, this.height * .5 - m.EngagementZone.height * .5 );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Letra identificadora
  Field.charId = 'F';

  // Preenchimento vertical
  Field.paddingY = 30;

  // Quantidade de fileiras de grades
  Field.gridRowsQuantity = 2;

  // Quantidade de colunas de grades
  Field.gridColumnsQuantity = 4;

  // Espaçamento horizontal entre casas
  Field.slotMarginX = 38;

  // Espaçamento vertical entre casas
  Field.slotMarginY = 19;

  // Iniciação de propriedades
  Field.init = function ( field ) {
    // Chama 'init' ascendente
    m.Environment.init( field );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( field instanceof Field );

    // Atribuição de propriedades iniciais

    /// Nome
    field.name = 'field';

    /// Plano de fundo
    field.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'the-field' ] );

    /// Grades
    field.grids = {};

    /// Divisória
    field.divider = null;

    /// Coloca carta no ambiente
    field.put = function ( card, slotName ) {
      // Identificadores
      var { owner } = card.getRelationships(),
          fieldSlots = this.grids[ owner.parity ].slots.concat( this.divider.slots ),
          slot = fieldSlots.find( slot => slot.name.endsWith( slotName ) );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( card instanceof m.Being );
        m.oAssert( slotName && typeof slotName == 'string' );
        m.oAssert( slot instanceof m.FieldSlot );
      }

      // Para casas nas grades do campo, não coloca carta caso outra já exista na casa alvo
      if( slot.card ) return false;

      // Para zonas de engajamento, não coloca carta caso zona de engajamento já tenha alcançado o limite de cartas do jogador alvo
      if( slot.cards?.[ owner.parity ].length >= m.EngagementZone.maxCards ) return false;

      // Sinaliza início da entrada de ocupante
      for( let eventTarget of [ card, slot ] ) m.events.fieldSlotCardChangeStart.enter.emit( eventTarget, { slot: slot, card: card } );

      // Retira carta do ambiente em que estiver, se aplicável
      if( card.slot ) card.slot.environment instanceof Field ? this.remove( card ) : owner.pool.remove( card );

      // Define casa alvo como a em que carta está
      card.slot = slot;

      // Para casas da grade
      if( slot instanceof m.FieldGridSlot ) {
        // Altera estrutura visual da carta a ser exibida
        [ card.body.front.visible, card.body.symbol.visible ] = [ true, false ];

        // Adapta o tamanho da carta para o da casa
        card.toSize( 'fieldSlot' );

        // Altera estilo de casa para indicar que está ocupada
        slot.toStyle( 'inUse' );

        // Adiciona carta ao arranjo de cartas em sua grade
        this.grids[ owner.parity ].cards.push( card );

        // Define carta como a que ocupa casa alvo
        slot.card = card;
      }

      // Para zonas de engajamento
      else if( slot instanceof m.EngagementZone ) {
        // Altera estrutura visual da carta a ser exibida
        [ card.body.front.visible, card.body.symbol.visible ] = [ false, true ];

        // Adapta o tamanho da carta para o da zona de engajamento
        card.toSize( 'engagementZone' );

        // Adiciona carta aos arranjos pertinentes da divisória
        for( let cardsArray of [ this.divider.cards, slot.cards, slot.cards[ owner.parity ] ] ) cardsArray.push( card );

        // Atualiza posição e tamanho de cartas do jogador alvo na zona de engajamento alvo
        slot.updateCards( owner.parity );
      }

      // Inseri carta na casa
      slot.addChild( card );

      // Sinaliza fim da entrada de ocupante
      for( let eventTarget of [ card, slot ] ) m.events.fieldSlotCardChangeEnd.enter.emit( eventTarget, { slot: slot, card: card } );

      // Retorna carta
      return card;
    }

    /// Remove carta do campo
    field.remove = function ( card ) {
      // Identificadores
      var { slot } = card,
          { owner } = card.getRelationships();

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Being );

      // Não executa função caso carta não esteja no campo
      if( !( slot instanceof m.FieldSlot ) ) return false;

      // Sinaliza início da saída de ocupante
      for( let eventTarget of [ card, slot ] ) m.events.fieldSlotCardChangeStart.leave.emit( eventTarget, { slot: slot, card: card } );

      // Remove eventual texto suspenso da carta
      m.Card.removeHoverText( card );

      // Retira carta da casa
      slot.removeChild( card );

      // Para casas de uma grade
      if( slot instanceof m.FieldGridSlot ) {
        // Altera estilo de casa para indicar que está vazia
        slot.toStyle( 'empty' );

        // Retira carta do arranjo de cartas em sua grade
        this.grids[ owner.parity ].cards.splice( this.grids[ owner.parity ].cards.indexOf( card ), 1 );

        // Remove relação de casa com sua carta
        slot.card =  null;
      }

      // Para zonas de engajamento
      else if( slot instanceof m.EngagementZone ) {
        // Retira carta dos arranjos pertinentes da divisória
        for( let cardsArray of [ this.divider.cards, slot.cards, slot.cards[ owner.parity ] ] ) cardsArray.splice( cardsArray.indexOf( card ), 1 );

        // Atualiza posição e tamanho de cartas do jogador alvo na zona de engajamento alvo
        slot.updateCards( owner.parity );

        // Restaura dimensões originais do símbolo da carta
        card.body.symbol.scale.set( 1 );
      }

      // Remove relação de carta com sua casa
      card.slot = null;

      // Sinaliza fim da saída de ocupante
      for( let eventTarget of [ card, slot ] ) m.events.fieldSlotCardChangeEnd.leave.emit( eventTarget, { slot: slot, card: card } );

      // Retorna carta retirada
      return card;
    }

    /// Remove componentes no ambiente
    field.clear = function ( config = {} ) {
      // Identificadores pré-validação
      var { skipOdd = false, skipEven = false, skipFieldGrid = false, skipEngagementZones = false, skipMana = false } = config;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( Object.keys( config ).every( key => [ 'skipOdd', 'skipEven', 'skipFieldGrid', 'skipEngagementZones', 'skipMana' ].includes( key ) ) );

      // Identificadores pós-validação
      var targetParities = [],
          removedCards = [];

      // Se aplicável, adiciona o ímpar ao arranjo de paridades a serem alvo da remoção
      if( !skipOdd ) targetParities.push( 'odd' );

      // Se aplicável, adiciona o par ao arranjo de paridades a serem alvo da remoção
      if( !skipEven ) targetParities.push( 'even' );

      // Itera por paridades a serem alvo da remoção
      for( let parity of targetParities ) {
        // Se aplicável, remove cartas da paridade alvo na grade do campo
        if( !skipFieldGrid )
          this.grids[ parity ].cards.slice().forEach( card => removedCards.push( card.inactivate( 'suspended' ) ) );

        // Se aplicável, remove cartas da paridade alvo nas zonas de engajamento
        if( !skipEngagementZones )
          this.divider.slots.flatMap( slot => slot.cards[ parity ] ).forEach( card => removedCards.push( card.inactivate( 'suspended' ) ) );
      }

      // Se aplicável, remove o mana de casas
      if( !skipMana ) this.getSlots().forEach( slot => slot.detach( slot.attachments.mana, 'mana' ) );

      // Retorna cartas removidas
      return removedCards;
    }

    /// Retorna casas do campo, na ordem em que estão dispostas
    field.getSlots = function () {
      // Identificadores
      var fieldGrids = [ this.grids.odd, this.divider, this.grids.even ].sort( ( a, b ) => a.y - b.y ),
          fieldSlots = fieldGrids.flatMap( grid => grid.slots );

      // Retorno das casas do campo
      return fieldSlots;
    }

    /// Aplica posicionamentos de ajuste
    field.applyAdjustmentPlacements = function () {
      // Itera por paridades
      for( let parity of [ 'odd', 'even' ] ) {
        // Identificadores
        let fieldGrid = this.grids[ parity ],
            gridColumns = Object.values( fieldGrid.columns ),
            [ rearguardIndex, vanguardIndex ] = fieldGrid.y < this.divider.y ? [ 0, 1 ] : [ 1, 0 ];

        // Itera por colunas da grade alvo
        for( let column of gridColumns ) {
          // Identificadores
          let columnCards = column.map( cardSlot => cardSlot.card ),
              [ rearguardCard, vanguardCard ] = [ columnCards[ rearguardIndex ], columnCards[ vanguardIndex ] ];

          // Caso não haja cartas na coluna alvo, avança para próxima coluna
          if( columnCards.every( card => !card ) ) continue;

          // Caso haja um ente físico na retaguarda e nenhum na vanguarda, desloca esse ente para a vanguarda
          if( rearguardCard instanceof m.PhysicalBeing && !vanguardCard )
            this.put( rearguardCard, column[ vanguardIndex ].name )

          // Caso haja um ente astral na vanguarda e nenhum na retaguarda, desloca esse ente para a retaguarda
          else if( vanguardCard instanceof m.AstralBeing && !rearguardCard )
            this.put( vanguardCard, column[ rearguardIndex ].name )

          // Para caso haja um ente físico na retaguarda e um ente astral na vanguarda
          else if( rearguardCard instanceof m.PhysicalBeing && vanguardCard instanceof m.AstralBeing ) {
            // Remove cartas de seus ambientes
            for( let card of columnCards ) this.remove( card );

            // Posiciona ente físico na vanguarda
            this.put( rearguardCard, column[ vanguardIndex ].name );

            // Posiciona ente astral na retaguarda
            this.put( vanguardCard, column[ rearguardIndex ].name );
          }
        }

        // Reitera por colunas da grade alvo
        for( let i = 0; i < gridColumns.length - 1; i++ ) {
          // Identificadores
          let targetColumns = [ gridColumns[ i ], gridColumns[ i + 1 ] ],
              columnCards = targetColumns.map( column => column.map( cardSlot => cardSlot.card ) ),
              rearguardCards = columnCards.map( cards => cards[ rearguardIndex ] ),
              vanguardCards = columnCards.map( cards => cards[ vanguardIndex ] );

          // Caso nenhuma das colunas alvo tenha duas cartas do mesmo ambiente, avança para próximo conjunto de colunas
          if( !columnCards.some( cards => [ m.PhysicalBeing, m.AstralBeing ].some( constructor => cards.every( card => card instanceof constructor ) ) ) )
            continue;

          // Itera por índices de colunas alvo
          for( let columnIndexes of [ [ 0, 1 ], [ 1, 0 ] ] ) {
            // Caso haja um ente físico na retaguarda de uma das colunas alvo e nenhum na vanguarda da outra coluna alvo, desloca esse ente para essa vanguarda
            if( rearguardCards[ columnIndexes[0] ] instanceof m.PhysicalBeing && !vanguardCards[ columnIndexes[1] ] )
              this.put( rearguardCards[ columnIndexes[0] ], targetColumns[ columnIndexes[1] ][ vanguardIndex ].name );

            // Caso haja um ente astral na vanguarda de uma das colunas alvo e nenhum na retaguarda da outra coluna alvo, desloca esse ente para essa retaguarda
            else if( vanguardCards[ columnIndexes[0] ] instanceof m.AstralBeing && !rearguardCards[ columnIndexes[1] ] )
              this.put( vanguardCards[ columnIndexes[0] ], targetColumns[ columnIndexes[1] ][ rearguardIndex ].name );

            // Para caso haja um ente físico na retaguarda de uma das colunas e um ente astral na vanguarda da outra coluna
            else if( rearguardCards[ columnIndexes[0] ] instanceof m.PhysicalBeing && vanguardCards[ columnIndexes[1] ] instanceof m.AstralBeing ) {
              // Remove cartas de seus ambientes
              for( let card of [ rearguardCards[ columnIndexes[0] ], vanguardCards[ columnIndexes[1] ] ] ) this.remove( card );

              // Posiciona ente físico na vanguarda
              this.put( rearguardCards[ columnIndexes[0] ], targetColumns[ columnIndexes[1] ][ vanguardIndex ].name );

              // Posiciona ente astral na retaguarda
              this.put( vanguardCards[ columnIndexes[1] ], targetColumns[ columnIndexes[0] ][ rearguardIndex ].name );
            }
          }
        }
      }

      // Itera por zonas de engajamento
      for( let engagementZone of this.divider.slots ) {
        // Filtra zonas de engajamento vazias
        if( !engagementZone.cards.length ) continue;

        // Quando aplicável, desloca entes para zona de engajamento alvo
        engagementZone.pullBeings();
      }
    }
  }
}

/// Propriedades do protótipo
Field.prototype = Object.create( m.Environment.prototype, {
  // Construtor
  constructor: { value: Field }
} );
