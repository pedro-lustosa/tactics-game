// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Reserve = function ( config = {} ) {
  // Iniciação de propriedades
  Reserve.init( this );

  // Identificadores
  var { pool } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( pool instanceof m.Pool );

  // Configurações pré-superconstrutor

  /// Ajuste do nome
  this.name = `${ pool.parity }-${ this.name }`;

  // Superconstrutor
  m.Environment.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição do dono e da paridade
  for( let keyName of [ 'owner', 'parity' ] ) this[ keyName ] = pool[ keyName ];

  /// Geração e inserção da grade
  this.grid = this.addChild( new m.CardGrid( {
    name: 'reserve-grid',
    owner: this.owner,
    environment: this,
    rows: Reserve.gridRowsQuantity,
    columns: Reserve.gridColumnsQuantity,
    namePrefix: Reserve.charId + ( this.parity == 'odd' ? 1 : 2 )
  } ) );
}

/// Propriedades do construtor
defineProperties: {
  // Letra identificadora
  Reserve.charId = 'R';

  // Quantidade de fileiras de grades
  Reserve.gridRowsQuantity = 2;

  // Quantidade de colunas de grades
  Reserve.gridColumnsQuantity = 5;

  // Espaçamento horizontal entre casas
  Reserve.slotMarginX = 42.5;

  // Espaçamento vertical entre casas
  Reserve.slotMarginY = 12.75;

  // Iniciação de propriedades
  Reserve.init = function ( reserve ) {
    // Chama 'init' ascendente
    m.Environment.init( reserve );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( reserve instanceof Reserve );

    // Atribuição de propriedades iniciais

    /// Nome
    reserve.name = 'reserve';

    /// Dono
    reserve.owner = null;

    /// Paridade
    reserve.parity = '';

    /// Grade
    reserve.grid = null;
  }
}

/// Propriedades do protótipo
Reserve.prototype = Object.create( m.Environment.prototype, {
  // Construtor
  constructor: { value: Reserve }
} );
