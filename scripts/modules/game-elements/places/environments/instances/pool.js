// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Pool = function ( config = {} ) {
  // Iniciação de propriedades
  Pool.init( this );

  // Identificadores
  var { owner } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( owner instanceof m.GamePlayer );

  // Configurações pré-superconstrutor

  /// Ajuste do nome
  this.name = `${ owner.parity }-${ this.name }`;

  // Superconstrutor
  m.Environment.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição do dono
  this.owner = owner;

  /// Atribuição da paridade
  this.parity = owner.parity;

  /// Vinculação de ambiente com dono
  owner.pool = this;

  /// Inserção do plano de fundo
  this.addChild( this.background );

  /// Configurações da banca
  tableConfig: {
    // Geração e inserção
    let table = this.table = this.addChild( new m.Table( { pool: this } ) );

    // Posicionamento
    table.position.set( this.width * .5 - table.width * .5, Pool.paddingY );
  }

  /// Configurações da reserva
  reserveConfig: {
    // Geração e inserção
    let reserve = this.reserve = this.addChild( new m.Reserve( { pool: this } ) );

    // Posicionamento
    reserve.position.set( this.width * .5 - reserve.width * .5, this.height - ( reserve.height + Pool.paddingY ) );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Preenchimento vertical
  Pool.paddingY = 20;

  // Iniciação de propriedades
  Pool.init = function ( pool ) {
    // Chama 'init' ascendente
    m.Environment.init( pool );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( pool instanceof Pool );

    // Atribuição de propriedades iniciais

    /// Nome
    pool.name = 'pool';

    /// Plano de fundo
    pool.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'the-pool' ] );

    /// Dono
    pool.owner = null;

    /// Paridade
    pool.parity = '';

    /// Banca
    pool.table = null;

    /// Reserva
    pool.reserve = null;

    /// Coloca carta em seu ambiente
    pool.put = function ( card ) {
      // Identificadores
      var environment = card.deck instanceof m.MainDeck ? this.table : this.reserve,
          targetSlot = environment.grid.slots.find( slot => slot.card == card ) ?? environment.grid.slots.find( slot => !slot.card );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( card instanceof m.Card );
        m.oAssert( card.deck );
        m.oAssert( this.owner == card.deck.owner );
      }

      // Não executa função caso não haja nenhuma casa disponível
      if( !targetSlot ) return false;

      // Não executa função caso carta já esteja na casa alvo
      if( card.slot == targetSlot ) return false;

      // Retira carta do ambiente em que estiver, se aplicável
      if( card.slot ) card.slot.environment instanceof m.Field ? card.slot.environment.remove( card ) : this.remove( card, true );

      // Para entes, altera estrutura visual a ser exibida
      if( card instanceof m.Being ) [ card.body.front.visible, card.body.symbol.visible ] = [ true, false ];

      // Adapta o tamanho da carta para o da casa
      card.toSize( 'poolSlot' );

      // Captura relação entre carta e sua nova casa
      [ card.slot, targetSlot.card ] = [ targetSlot, card ];

      // Adiciona carta à lista de cartas em sua grade
      environment.grid.cards.push( card );

      // Se ambiente for a reserva, altera estilo de casa para indicar que está ocupada
      if( environment instanceof m.Reserve ) targetSlot.toStyle( 'withdrawn' );

      // Inseri carta na casa
      targetSlot.addChild( card );

      // Retorna carta
      return card;
    }

    /// Remove carta de seu ambiente
    pool.remove = function ( card, isToResetSlot = false ) {
      // Identificadores
      var cardEnvironment = card.slot?.environment,
          poolGridCards = [ m.Table, m.Reserve ].some( constructor => cardEnvironment instanceof constructor ) ? cardEnvironment.grid.cards : null;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( card instanceof m.Card );
        m.oAssert( card.deck );
        m.oAssert( this.owner == card.deck.owner );
      }

      // Não executa função caso carta não esteja em sua grade
      if( !poolGridCards ) return false;

      // Remove eventual texto suspenso da carta
      m.Card.removeHoverText( card );

      // Retira carta da casa
      card.slot.removeChild( card );

      // Retira carta da lista de cartas em sua grade
      poolGridCards.splice( poolGridCards.indexOf( card ), 1 );

      // Altera estilo de casa para indicar que está vazia
      card.slot.toStyle( 'empty' );

      // Remove relação de casa com carta, se aplicável
      if( isToResetSlot ) card.slot.card = null;

      // Remove relação de carta com casa
      card.slot = null;

      // Retorna carta retirada
      return card;
    }

    /// Remove e alheia todas as cartas em ambientes do rol
    pool.clear = function ( ...environmentNames ) {
      // Identificadores
      var environmentNames = environmentNames.length ? environmentNames : [ 'table', 'reserve' ],
          removedCards = [];

      // Validação
      if( m.app.isInDevelopment ) environmentNames.every( name => [ 'table', 'reserve' ].includes( name ) );

      // Itera por nome dos ambientes mirados
      for( let name of environmentNames ) {
        // Identificadores
        let environment = this[ name ],
            targetCards = environment.grid.slots.filter( slot => slot.card?.slot == slot ).map( slot => slot.card );

        // Suspende cartas ainda não suspensas ou alheadas
        for( let card of targetCards )
          if( ![ 'unlinked', 'suspended' ].includes( card.activity ) ) card.inactivate( 'suspended' );

        // Alheia cartas, e as adiciona ao arranjo de cartas removidas
        for( let card of targetCards )
          removedCards.push( card.activity == 'unlinked' ? this.remove( card, true ) : card.unlink() );
      }

      // Retorna cartas removidas
      return removedCards.filter( card => card );
    }
  }
}

/// Propriedades do protótipo
Pool.prototype = Object.create( m.Environment.prototype, {
  // Construtor
  constructor: { value: Pool }
} );
