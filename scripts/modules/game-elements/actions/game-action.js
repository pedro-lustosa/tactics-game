// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameAction = function ( config = {} ) {
  // Identificadores
  var { commitment } = this,
      gameMatch = m.GameMatch.current,
      matchFlow = gameMatch.flow;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( typeof this.name == 'string' && this.name.startsWith( 'action-' ) );
    m.oAssert( config.committer instanceof m.Card );
    m.oAssert( this.targetStages.length );
    m.oAssert( this.targetStages.every( stage => [ m.FlowStep, m.FlowPeriod ].some( constructor => stage.prototype instanceof constructor ) ) );
    m.oAssert( this.animationTargets.every( animationTarget => animationTarget && typeof animationTarget == 'string' ) );
    m.oAssert(
      commitment.type == 'dedicated' && [ 'full', 'partial' ].includes( commitment.subtype ) || commitment.type == 'free' && !commitment.subtype
    );
    m.oAssert( !this.iterativity || [ 'single', 'iterative' ].includes( this.iterativity ) );
    m.oAssert( typeof this.execute == 'function' );
  }

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Atribuição de propriedades pós-superconstrutor

  /// Acionante e alvo
  for( let key of [ 'committer', 'target' ] ) this[ key ] = config[ key ] ?? this[ key ];

  /// Indicador de se ação é uma propagada
  this.isPropagated = Boolean( config.isPropagated );

  /// Grau de acionamento
  commitment.degree = ( function () {
    switch( commitment.subtype ) {
      case 'full':
        return 1;
      case 'partial':
        return .5;
      default:
        return 0;
    }
  } )();

  /// Iteratividade
  this.iterativity ||= commitment.type == 'dedicated' || matchFlow.period instanceof m.CombatPeriod ? 'iterative' : 'single';

  // Eventos

  /// Para criar entrada de registro da ação na partida, quando acionada, concluída ou impedida
  for( let eventName of [ 'commit', 'finish', 'prevent' ] )
    m.events.actionChangeStart[ eventName ].add( this, gameMatch.log.addEntry, { context: gameMatch.log } );

  /// Para partidas reais e ações não propagadas, propaga para outros sockets na partida ação, quando a ser acionada
  if( !gameMatch.isSimulation && !this.isPropagated )
    m.events.actionChangeStart.commit.add( this, m.sockets.propagateAction, { context: m.sockets } );

  /// Se aplicável, para executar operação de conclusão da ação
  if( this.finishExecution )
    m.events.actionChangeEnd.finish.add( this, this.finishExecution );

  /// Se aplicável, para limpar dados da ação após seu encerramento
  if( this.clearData )
    for( let eventName of [ 'complete', 'cancel' ] ) m.events.actionChangeEnd[ eventName ].add( this, this.clearData );

  /// Caso ação seja dedicada e acionante um humano, adiciona eventos relativos à característica 'adaptiveTactics'
  if( this.commitment.type == 'dedicated' && this.committer instanceof m.Human )
    for( let key of [ 'actionChangeStart', 'actionChangeEnd' ] ) m.events[ key ].commit.add( this, m.Human.triggerAdaptiveTactics, { context: m.Human } );
}

/// Propriedades do construtor
defineProperties: {
  // Ações em andamento
  GameAction.currents = [];

  // Ações a serem geradas e executadas
  GameAction.executionQueue = [];

  // Ações a serem progredidas
  GameAction.progressQueue = [];

  // Instruções para formatação de animações de ações
  GameAction.animationTargetsConfig = {
    'committer': {
      color: 'lightBlue'
    },
    'target': {
      color: 'lightYellow'
    },
    'miscCard': {
      color: 'lightOrange'
    },
    'manaToAssign': {
      color: 'darkRed'
    },
    'miscNumber': {
      color: 'darkGreen'
    }
  };

  // Execução atual de 'GameAction.progress'
  GameAction.progressExecution = null;

  // Iniciação de propriedades
  GameAction.init = function ( action ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof GameAction );

    // Atribuição de propriedades iniciais

    /// Nome
    action.name = '';

    /// Acionante
    action.committer = null;

    /// Alvo
    action.target = null;

    /// Indica se ação foi acionada
    action.isCommitted = false;

    /// Indica se ação foi registrada
    action.isLogged = false;

    /// Indica se ação é uma propagada
    action.isPropagated = false;

    /// Estágios em que ação pode ser acionada
    action.targetStages = [];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'target' ];

    /// Indica se ação deve ser animada no momento de seu acionamento
    action.isToAnimateAtCommit = true;

    /// Indica se ação deve ser animada no momento de sua progressão
    action.isToAnimateAtProgress = true;

    /// Execução atual de 'action.execute'
    action.currentExecution = null;

    /// Acionamento
    action.commitment = {};

    /// Iteratividade
    action.iterativity = '';

    /// Custo de fluxo
    action.flowCost = 0;

    /// Identifica nome da superação
    action.superActionName = '';

    /// Define um alvo para a ação
    action.selectTarget = function ( choiceText ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.currentExecution && this.committer );
        m.oAssert( choiceText && typeof choiceText == 'string' );
      }

      // Retorna barra sobre notificação de seleção de alvo
      return new m.StaticBar( {
        name: this.name + '-target-static-bar',
        text: m.languages.notices.getChoiceText( choiceText ),
        isRemovable: true,
        removeHoverText: m.languages.snippets.getUserInterfaceAction( 'cancel' ),
        removeAction: this.cancel.bind( this, this.isPropagated )
      } ).insert();
    }

    /// Aciona ação
    action.commit = async function ( completeType = 'finish' ) {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          flowParity = matchFlow.parity,
          isCombatPeriod = matchFlow.period instanceof m.CombatPeriod,
          { committer, target } = this;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( completeType && typeof completeType == 'string' );

      // Sinaliza início do acionamento da ação
      m.events.actionChangeStart.commit.emit( this, { actionCommitter: committer, actionTarget: target } );

      // Caso ação não tenha sido interrompida e deva ser animada no acionamento, executa animação
      if( completeType == 'finish' && this.isToAnimateAtCommit ) await this.animate();

      // Para caso ação seja singular
      if( this.iterativity == 'single' ) {
        // Captura índice da ação alvo no arranjo de ações do acionante
        let actionIndex = committer.actions.indexOf( this );

        // Caso ação ainda exista no arranjo de ações do acionante, remove-a de lá
        if( actionIndex != -1 ) committer.actions.splice( actionIndex, 1 );

        // Inclui construtor da ação no arranjo de ações singulares acionadas de seu acionante
        committer.actions.singleCommitments.push( this.constructor );
      }

      // Para caso ação seja dedicada
      if( this.commitment.type == 'dedicated' ) {
        // Captura jogada aberta do controlador do fluxo
        let targetMove = matchFlow.period instanceof m.CombatPeriod ?
              matchFlow.moves.find( move => move.source == committer && move.isOpen ) :
              matchFlow.moves.find( move => move.source == committer.getRelationships().owner && move.isOpen );

        // Desenvolve jogada alvo
        targetMove.develop( this.commitment.degree );
      }

      // Para caso ação tenha custo de fluxo e período atual seja o do combate
      if( this.flowCost && isCombatPeriod ) {
        // Altera disponibilidade do acionante para ocupado
        committer.readiness.change( 'occupied' );

        // Programa progressão da ação na próxima paridade do jogador alvo, quando aplicável
        this.programProgress();
      }

      // Caso ação não atenda condição acima, completa-a imediatamente
      else this.complete( completeType );

      // Indica que ação foi acionada
      this.isCommitted = true;

      // Sinaliza fim do acionamento da ação
      m.events.actionChangeEnd.commit.emit( this, { actionCommitter: committer, actionTarget: target } );

      // Caso ação não tenha custo de fluxo ou não seja o período do combate, redefine acionamento da ação
      if( !this.flowCost || !isCombatPeriod ) this.isCommitted = false;

      // Programa avanço do fluxo caso não haja mais jogadas abertas do controlador do fluxo, ou caso seja o período do combate e ele não tenha entes preparados
      if( matchFlow.moves[ flowParity.parity ].every( move => !move.isOpen || isCombatPeriod && move.source.readiness.status != 'prepared' ) )
        setTimeout( () => matchFlow.parity == flowParity ? matchFlow.progress() : null );

      // Retorna ação
      return this;
    }

    /// Programa progressão da ação em períodos do combate
    action.programProgress = function () {
      // Identificadores pré-validação
      var matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( matchFlow.period instanceof m.CombatPeriod );

      // Identificadores pós-validação
      var nextParity = matchFlow.period.getStageFrom( matchFlow.parity, 1 );

      // Caso haja uma próxima paridade válida, adiciona evento para progredir ação
      if( nextParity ) m.events.flowChangeEnd.begin.add( nextParity, this.progress, { context: this, once: true } );
    }

    /// Progride ação
    action.progress = function ( eventData = {} ) {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Identificadores
      var { committer } = this,
          { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( GameAction.currents.includes( this ) );
        m.oAssert( this.flowCost > 0 );
        m.oAssert( committer.readiness.status == 'occupied' );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
        m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.FlowParity );
      }

      // Incrementa em 1 custo de fluxo já gasto do acionante com ação alvo
      committer.actions.spentFlowCost++;

      // Caso custo de fluxo já gasto do acionante com ação alvo seja menor que o da ação, programa próxima progressão da ação e encerra função
      if( committer.actions.spentFlowCost < this.flowCost ) return this.programProgress();

      // Coloca ação na fila de ações a serem executadas
      GameAction.progressQueue.push( this );
    }

    /// Executa pós-acionamento da ação, para ações com custo de fluxo
    action.postExecute = async function () {
      // Identificadores
      var { committer } = this;

      // Sinaliza início da execução da ação pós-acionamento
      m.events.actionChangeStart.postExecution.emit( this );

      // Encerra execução do pós-acionamento da ação caso ela não esteja sendo executada
      if( !this.currentExecution ) return m.events.actionChangeEnd.postExecution.emit( this );

      // Se revalidada, para caso ação não seja mais válida
      if( this.revalidate && !this.revalidate() ) {
        // Notifica usuário sobre ação a ser impedida
        m.noticeBar.show(
          m.languages.notices.getOutcome( 'action-prevented', { action: this, isInvalid: true } ),
          !m.GameMatch.current.isSimulation && m.GamePlayer.current == m.GameMatch.current.players.get( 'opponent' ) ? 'green' : 'red'
        );

        // Impede ação
        this.complete( 'prevent' );

        // Encerra execução do pós-acionamento da ação
        return m.events.actionChangeEnd.postExecution.emit( this );
      }

      // Caso ação deva ser animada em sua progressão, executa animação
      if( this.isToAnimateAtProgress ) await this.animate();

      // Realiza execução pós-acionamento da ação, e conclui ação caso não sejam sinalizadas novas re-execuções
      if( ( await this.currentExecution.next() ).done ) this.complete( 'finish' )

      // Para caso execução da ação não tenha sido concluída e ela não seja 'Atacar'
      else if( !( this instanceof m.ActionAttack ) ) {
        // Registra execução atual da ação
        m.GameMatch.current.log.addEntry( { eventTarget: this } );

        // Zera custo de fluxo gasto do acionante com ação alvo
        committer.actions.spentFlowCost = 0;

        // Atualiza texto suspenso sobre acionante, quando aplicável
        m.Card.updateHoverText( { currentTarget: committer } );

        // Programa próxima progressão da ação
        this.programProgress();
      }

      // Encerra execução do pós-acionamento da ação
      return m.events.actionChangeEnd.postExecution.emit( this );
    }

    /// Completa ação
    action.complete = function ( eventType ) {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Identificadores
      var { committer: actionCommitter, target: actionTarget } = this,
          matchFlow = m.GameMatch.current.flow,
          isCombatPeriod = matchFlow.period instanceof m.CombatPeriod;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'finish', 'abort', 'prevent' ].includes( eventType ) );

      // Sinaliza início do término da ação
      m.events.actionChangeStart[ eventType ].emit( this, { actionCommitter: actionCommitter, actionTarget: actionTarget } );

      // Zera ou atualiza o custo de fluxo gasto de seu acionante, em função de se período atual é o do combate
      actionCommitter.actions.spentFlowCost = isCombatPeriod ? 0 : actionCommitter.actions.spentFlowCost + this.flowCost;

      // Encerra execução da ação
      this.dispose();

      // Sinaliza fim do término da ação
      m.events.actionChangeEnd[ eventType ].emit( this, { actionCommitter: actionCommitter, actionTarget: actionTarget } );

      // Caso período atual seja o do combate, atualiza disponibilidade do acionante; do contrário, atualiza seu eventual texto suspenso
      isCombatPeriod ? actionCommitter.readiness.update() : m.Card.updateHoverText( { currentTarget: actionCommitter } );

      // Retorna ação
      return this;
    }

    /// Cancela execução da ação
    action.cancel = function ( isToPropagate = false ) {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Identificadores
      var { committer: actionCommitter, target: actionTarget } = this,
          match = m.GameMatch.current;

      // Sinaliza início do cancelamento da ação
      m.events.actionChangeStart.cancel.emit( this, { actionCommitter: actionCommitter, actionTarget: actionTarget } );

      // Caso partida não seja uma simulação e cancelamento da ação deva ser propagado, realiza operação
      if( !match.isSimulation && isToPropagate )
        m.sockets.current.emit( 'cancel-action', {
          matchName: match.name,
          data: {
            playerParity: m.GamePlayer.current.parity,
            actionName: this.name,
            actionCommitterId: this.committer.getMatchId(),
            actionIndex: GameAction.currents.indexOf( this )
          }
        } );

      // Para caso ação já tenha sido registrada
      if( this.isLogged ) {
        // Identificadores
        let matchLogs = Object.keys( match.log.body );

        // Retira registro sobre a ação
        match.log.removeEntry( {
          description: m.languages.notices.getActionLog( this.name, { action: this, isFull: true } )
        }, matchLogs[ matchLogs.indexOf( match.flow.toChain() ) - this.flowCost ] );
      }

      // Encerra execução da ação
      this.dispose();

      // Sinaliza fim do cancelamento da ação
      m.events.actionChangeEnd.cancel.emit( this, { actionCommitter: actionCommitter, actionTarget: actionTarget } );

      // Retorna ação
      return this;
    }

    /// Encerra execução da ação
    action.dispose = function ( eventData = {} ) {
      // Identificadores
      var actionIndex = GameAction.currents.indexOf( this ),
          actionBar = m.GameScreen.current.children.find( child => child.name?.startsWith( this.name + '-' ) && child.name.endsWith( '-static-bar' ) );

      // Remove qualquer barra sobre a ação, quando existir
      actionBar?.remove();

      // Remove ação do arranjo de ações atuais, se existente
      if( actionIndex != -1 ) GameAction.currents.splice( actionIndex, 1 );

      // Indica que execução atual da ação foi encerrada, e nulifica seu alvo
      this.currentExecution = this.target = null;

      // Redefine indicadores de informações sobre a ação
      this.isLogged = this.isCommitted = false;

      // Retorna ação
      return this;
    }

    /// Executa animação da ação
    action.animate = async function () {
      // Identificadores
      var promisesArray = [],
          isCombatPeriod = m.GameMatch.current.flow.period instanceof m.CombatPeriod,
          animationTargets = setAnimationTargets.call( this );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.currentExecution && this.committer );

      // Não executa função caso não haja alvos para a animação
      if( !animationTargets.length ) return this;

      // Mostra barra sobre animação
      let animationBar = new m.StaticBar( {
            name: this.name + '-animation-static-bar',
            text: m.languages.notices.getActionLog( this.name, { action: this } )
          } ).insert();

      // Filtra cartas alvo da animação para que sejam obrigatoriamente distintas
      animationTargets = animationTargets.filter( ( setToAnimate, index, array ) =>
        array.indexOf( setToAnimate ) == array.findIndex( innerSetToAnimate => innerSetToAnimate[ 0 ] == setToAnimate[ 0 ] )
      );

      // Itera por conjuntos de alvos da animação
      for( let setToAnimate of animationTargets ) {
        // Identificadores
        let card = setToAnimate[ 0 ],
            cardConfig = GameAction.animationTargetsConfig[ setToAnimate[ 1 ] ] ?? GameAction.animationTargetsConfig.miscCard,
            floatingText = setToAnimate.find( value => typeof value == 'number' ),
            animationParameters = {
              color: cardConfig.color
            };

        // Para equipamentos embutidos
        if( card.isEmbedded ) {
          // Ajusta alvo de modo que seja igual a seu dono
          card = card.owner;

          // Filtra cartas alvo ajustadas que já existam no arranjo de alvos da animação
          if( animationTargets.some( innerSetToAnimate => innerSetToAnimate.includes( card ) ) ) continue;
        }

        // Para caso tenha sido encontrado um texto a ser animado
        if( floatingText !== undefined ) {
          // Adiciona aos parâmetros da animação texto a ser animado
          animationParameters.floatingText = floatingText.toString();

          // Adiciona aos parâmetros da animação cor sob a que exibir o texto
          animationParameters.floatingTextColor = (
            GameAction.animationTargetsConfig[ setToAnimate[ setToAnimate.indexOf( floatingText ) + 1 ] ] ?? GameAction.animationTargetsConfig.miscNumber
          ).color;
        }

        // Destaca alvo da animação, e o adiciona ao arranjo de promessas
        promisesArray.push( m.animations.highlightCard( card, animationParameters ) );
      }

      // Aguarda fim das animações de destaque
      await Promise.allSettled( promisesArray );

      // Remove barra sobre animação
      animationBar.remove();

      // Retorna ação
      return this;

      // Funções

      /// Define alvos a serem animados
      function setAnimationTargets() {
        // Identificadores
        var animationTargets = [];

        // Itera por nomes de alvos da ação a serem animados
        for( let propertiesPath of this.animationTargets ) {
          // Identificadores
          let targetValues = [ propertiesPath.split( '.' ).reduce( ( accumulator, current ) => accumulator?.[ current ], this ) ].flat();

          // Adiciona alvos da animação ao arranjo pertinente, junto com sua propriedade de referência
          for( let targetValue of targetValues ) animationTargets.push( targetValue, propertiesPath );
        }

        // Segmenta alvos da animação em arranjos encabeçados por cada carta a sofrer animação
        animationTargets = animationTargets.oChunk( target => target instanceof m.Card || [ undefined, null ].includes( target ), true );

        // Remove do arranjo de alvos da animação segmentos que não forem encabeçados por um alvo
        animationTargets = animationTargets.filter( set => set[ 0 ] instanceof m.Card );

        // Retorna alvos a serem animados
        return animationTargets;
      }
    }

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target?.getMatchId() ?? null
      };
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = '';

    /// Subtipo de acionamento
    commitment.subtype = '';

    /// Grau de acionamento
    commitment.degree = 0;
  }

  // Progride ações no arranjo de ações a serem progredidas
  GameAction.progress = function * () {
    // Identificadores
    var delayExecution = () => setTimeout( () => GameAction.progressExecution?.next(), m.app.timeoutDelay.short );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( this.progressExecution );
      m.oAssert( this.progressQueue.every( action => action instanceof this ) );
    }

    // Itera por ações no arranjo
    while( this.progressQueue.length ) {
      // Identificadores
      let action = this.progressQueue[ 0 ];

      // Adiciona evento para continuar progressão de ações após o fim da execução pós-acionamento da ação atual
      m.events.actionChangeEnd.postExecution.add( action, delayExecution, { context: this, once: true } );

      // Aguarda fim da progressão da ação atual
      yield setTimeout( action.postExecute.bind( action ) );

      // Aguarda resolução de todas as paradas provocadas por esta ação antes de prosseguir função
      while( m.GameMatch.getActiveBreaks().length ) yield m.events.breakEnd.any.add( m.GameMatch.current, delayExecution, { once: true } );

      // Retira ação progredida do arranjo
      this.progressQueue.shift();
    }

    // Nulifica execução atual de progressão das ações
    this.progressExecution = null;

    // Avança fluxo
    m.GameMatch.current.flow.advance();
  }

  // Seleciona alvo para a ação em andamento mais anterior
  GameAction.selectTarget = function ( eventData ) {
    // Identificadores
    var { currentTarget } = eventData,
        actionBar = m.GameScreen.current.children.find( child => child.name?.startsWith( 'action-' ) && child.name.endsWith( '-target-static-bar' ) );

    // Não executa função caso não haja uma barra sendo exibida sobre seleção do alvo de uma ação
    if( !actionBar ) return;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( GameAction.currents.length );

    // Se tecla 'Ctrl' não estiver pressionada, filtra casas
    if( !m.app.keysPressed.ctrl && currentTarget instanceof m.CardSlot ) return;

    // Se tecla 'Ctrl' estiver pressionada, filtra cartas
    if( m.app.keysPressed.ctrl && currentTarget instanceof m.Card ) return;

    // Identificadores para captura do resultado da validação alvo da ação em andamento mais anterior
    let action = GameAction.currents[ 0 ],
        validationResult = action.validateTarget( currentTarget );

    // Emite som de desfecho da validação
    m.assets.audios.soundEffects[ validationResult ? 'click-next' : 'click-deny' ].play();

    // Para caso validação tenha sido bem-sucedida
    if( validationResult ) {
      // Remove barra de seleção do alvo da ação
      actionBar.remove();

      // Prossegue com execução da ação
      return action.currentExecution.next( currentTarget );
    }
  }

  // Redefine ações do ente passado
  GameAction.refreshActions = function ( being ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

    // Identificadores
    var matchFlow = m.GameMatch.current.flow,
        isCombatPeriod = matchFlow.period instanceof m.CombatPeriod;

    // Re-executa controle de ações do ente passado, segundo estágio atual
    return isCombatPeriod ? matchFlow.period.controlActions( { eventTarget: being } ) :
           matchFlow.period ? matchFlow.period.unsetActions?.( { targetCards: [ being ] } ).setActions( { targetCards: [ being ] } ) :
           matchFlow.step ? matchFlow.step.unsetActions?.( [ being ] ).setActions( [ being ] ) : false;
  }

  // Impede todas as ações passadas
  GameAction.preventAllActions = function ( actions = [] ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( actions.every( action => action instanceof this ) );

    // Executa prevenção das ações segundo se cada uma tem ou não um custo de fluxo
    actions.forEach( action => action.flowCost ? action.complete( 'prevent' ) : action.commit( 'prevent' ) );
  }

  // Limpa informações relativas a ações de cartas
  GameAction.clearCardsData = function ( eventData = {} ) {
    // Identificadores
    var targetCards = m.GameMatch.current.decks.getAllCards(),
        { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment )
      m.oAssert(
        eventCategory == 'flow-change' && eventType == 'finish' && [ m.FlowStep, m.FlowPeriod ].some( constructor => eventTarget instanceof constructor )
      );

    // Itera por cartas alvo
    for( let card of targetCards ) {
      // Remove registro de ações singulares da carta alvo
      while( card.actions.singleCommitments.length ) card.actions.singleCommitments.shift();

      // Para entes, limpa custo de fluxo gasto com ações
      if( card instanceof m.Being ) card.actions.spentFlowCost = 0;
    }

    // Impede ações em andamento
    this.preventAllActions( this.currents.slice() );
  }

  // Valida se casas passadas estão dentro do alcance passado
  GameAction.validateRange = function ( sourceSlot, targetSlot, maxRange ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ sourceSlot, targetSlot ].every(
        slot => slot instanceof m.CardSlot && [ m.Field, m.Table ].some( constructor => slot.environment instanceof constructor )
      ) );
      m.oAssert( [ /^M[0-8]$/, /^R[0-5]$/, /^G[0-2]$/ ].some( regex => regex.test( maxRange ) ) );
    }

    // Para origens que não sejam uma casa do campo, invalida alcance
    if( !( sourceSlot instanceof m.FieldSlot ) ) return false;

    // Para um alcance do escopo próximo, valida-o em função de se casa de origem e de destino são as mesmas
    if( maxRange.startsWith( 'M' ) ) return sourceSlot == targetSlot;

    // Para um alcance do escopo global
    if( maxRange.startsWith( 'G' ) ) {
      // Para 'G0', sempre retorna que alcance é inválido
      if( maxRange.endsWith( '0' ) ) return false;

      // Para 'G1', retorna que alcance é válido se a casa de destino estiver no campo
      if( maxRange.endsWith( '1' ) ) return targetSlot instanceof m.FieldSlot;

      // Para 'G2', sempre retorna que alcance é válido
      if( maxRange.endsWith( '2' ) ) return true;
    }

    // Para alcances de escopo distante
    rangedScope: {
      // Para destinos que não sejam uma casa do campo, invalida alcance
      if( !( targetSlot instanceof m.FieldSlot ) ) return false;

      // Identificadores
      let maxExtension = Number( maxRange.replace( /\D/g, '' ) ),
          effectiveExtension = m.FieldSlot.getDistanceFrom( sourceSlot, targetSlot );

      // Valida alcance em função de se extensão efetiva é menor ou igual à máxima
      return effectiveExtension <= maxExtension;
    }
  }

  // Valida se custo de fluxo alvo pode ser pago
  GameAction.validateFlowCost = function ( committer, flowCost ) {
    // Identificadores
    var matchFlow = m.GameMatch.current.flow;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( committer instanceof m.Being );
      m.oAssert( flowCost >= 0 && Number.isInteger( flowCost ) );
    }

    // Validação do custo de fluxo para a etapa da alocação
    if( matchFlow.step instanceof m.AllocationStep && committer.actions.spentFlowCost + flowCost > 5 )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'allocation-step-invalid-flow-cost' ) );

    // Validação do custo de fluxo para o período do combate
    if( matchFlow.period instanceof m.CombatPeriod && !matchFlow.period.getStageFrom( matchFlow.segment, flowCost ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'combat-period-invalid-flow-cost' ) );

    // Indica que custo de fluxo é válido
    return true;
  }
}

/// Propriedades do protótipo
GameAction.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: GameAction }
} );
