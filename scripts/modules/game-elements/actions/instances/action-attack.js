// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionAttack = function ( config = {} ) {
  // Iniciação de propriedades
  ActionAttack.init( this );

  // Identificadores
  var { committer, customValidations = [], isFree = false } = config,
      committerManeuvers = committer.content.stats.effectivity.maneuvers;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( Array.isArray( customValidations ) && customValidations.every( object =>
      object.constructor == Object && typeof object.validate == 'function' && object.message && typeof object.message == 'string'
    ) );
    m.oAssert( !isFree || committer.readiness.status != 'occupied' );
    m.oAssert( [ 'primary', 'secondary' ].some( rank => committerManeuvers[ rank ].attacks.length && committerManeuvers[ rank ].currentPoints ) );
  }

  // Configurações pré-superconstrutor

  /// Caso ataque deva ser livre, ajusta seu acionamento para livre
  if( isFree ) [ this.commitment.type, this.commitment.subtype ] = [ 'free', '' ]

  /// Do contrário, caso o acionante esteja com ao menos 3 marcadores de encorajado, ajusta seu subtipo de acionamento para parcial
  else if( committer.conditions.find( condition => condition instanceof m.ConditionEmboldened )?.markers >= 3 ) this.commitment.subtype = 'partial';

  /// Quando existente, atribui validações de escolha de alvo próprias para esta ação
  this.customValidations = customValidations;

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Execução atual de 'ActionAttack.executeFreeAttack'
  ActionAttack.freeAttackExecution = null;

  // Iniciação de propriedades
  ActionAttack.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionAttack );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-attack';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Ataque a ser acionado
    action.attack = null;

    /// Arranjo com validações próprias para o alvo do ataque
    action.customValidations = [];

    /// Indica pontos de destino requeridos pela condição 'admirável' para escolha do alvo
    action.aweRequiredFatePoints = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        attack: {
          maneuverName: this.attack.maneuver.name,
          maneuverRank: [ 'primary', 'secondary' ].find(
            rank => this.committer.content.stats.effectivity.maneuvers[ rank ].attacks.includes( this.attack.maneuver )
          )
        },
        aweRequiredFatePoints: this.aweRequiredFatePoints
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { attack: attackData, aweRequiredFatePoints } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( attackData?.constructor == Object );
        m.oAssert( attackData.maneuverName && typeof attackData.maneuverName == 'string' );
        m.oAssert( [ 'primary', 'secondary' ].includes( attackData.maneuverRank ) );
        m.oAssert( Number.isInteger( aweRequiredFatePoints ) && aweRequiredFatePoints >= 0 );
      }

      // Identificadores pós-validação
      var { maneuverName, maneuverRank } = attackData,
          committerManeuvers = this.committer.content.stats.effectivity.maneuvers;

      // Atribui à ação objeto de ataque relativo aos dados passados no objeto da ação
      this.attack = {
        maneuver: committerManeuvers[ maneuverRank ].attacks.find( attack => attack.name == maneuverName ),
        maneuverSource: committerManeuvers[ maneuverRank ]
      };

      // Define pontos de destino requeridos pela condição 'admirável' para o acionamento da ação
      this.aweRequiredFatePoints = aweRequiredFatePoints;
    }

    /// Inicia acionamento da ação
    action.startCommit = async function () {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Sinaliza início do acionamento da ação
      m.events.actionChangeStart.commit.emit( this, { actionCommitter: this.committer, actionTarget: this.target } );

      // Executa animação da ação
      await this.animate();
    }

    /// Delega acionamento da ação para métodos próprios desta ação
    action.commit = function ( completeType = 'finish' ) {
      // Delegação do acionamento para método de fim do acionamento
      return this.endCommit( completeType );
    }

    /// Finaliza acionamento da ação
    action.endCommit = function ( completeType = 'finish' ) {
      // Não executa função caso ação não esteja sendo executada
      if( !this.currentExecution ) return false;

      // Caso ação já tenha sido acionada, finaliza-a e encerra execução da função
      if( this.isCommitted ) return this.complete( completeType );

      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          flowParity = matchFlow.parity;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( completeType && typeof completeType == 'string' );

      // Caso ação seja dedicada, desenvolve jogada aberta do controlador do fluxo
      if( this.commitment.type == 'dedicated' )
        matchFlow.moves.find( move => move.source == this.committer && move.isOpen ).develop( this.commitment.degree );

      // Para caso ação tenha custo de fluxo
      if( this.flowCost ) {
        // Altera disponibilidade do acionante para ocupado
        this.committer.readiness.change( 'occupied' );

        // Programa progressão da ação na próxima paridade do jogador alvo, quando aplicável
        this.programProgress();
      }

      // Caso ação não atenda condição acima, completa-a imediatamente
      else this.complete( completeType );

      // Indica que ação foi acionada
      this.isCommitted = true;

      // Sinaliza fim do acionamento da ação
      m.events.actionChangeEnd.commit.emit( this, { actionCommitter: this.committer, actionTarget: this.target } );

      // Caso ação não tenha custo de fluxo, redefine indicação de seu acionamento
      if( !this.flowCost ) this.isCommitted = false;

      // Programa avanço do fluxo caso não haja mais jogadas abertas do controlador do fluxo, ou caso ele não tenha entes preparados
      if( matchFlow.moves[ flowParity.parity ].every( move => !move.isOpen || move.source.readiness.status != 'prepared' ) )
        setTimeout( () => matchFlow.parity == flowParity ? matchFlow.progress() : null );

      // Retorna ação
      return this;
    }

    /// Executa operação da ação
    action.execute = async function * () {
      // Identificadores
      var { committer, target: defender, attack } = this,
          { controller: attackingPlayer, opponent } = committer.getRelationships(),
          isAttackingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == attackingPlayer,
          isOpponent = m.GameMatch.current.isSimulation || m.GamePlayer.current == opponent,
          attackScope = attack.maneuver.range.replace( /\d/g, '' ),
          attackExtension = attack.maneuver.range.replace( /\D/g, '' ),
          isCancelableAttack = true,
          validDefenses = [],
          defense = null;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( attack.constructor instanceof Object );
        m.oAssert( attack.maneuver instanceof m.OffensiveManeuver );
        m.oAssert( [ 'primary', 'secondary' ].some( rank => committer.content.stats.effectivity.maneuvers[ rank ] == attack.maneuverSource ) );
        m.oAssert( !defender || defender instanceof attack.maneuver.targetType );
      }

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Ajusta custo de fluxo da ação
      adjustFlowCost: {
        // Apenas executa bloco para ataques 'Disparar' de bestas
        if( !( attack.maneuver.source instanceof m.WeaponCrossbow ) || !( attack.maneuver instanceof m.ManeuverShoot ) ) break adjustFlowCost;

        // Ajusta para 1 custo de fluxo da ação
        this.flowCost = 1;

        // Aplica validação padrão do custo de fluxo de ações
        if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

        // Indica que ataque não pode ser cancelado
        isCancelableAttack = false;
      }

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso ação já tenha um alvo
        if( defender ) break setTarget;

        // Para caso o usuário seja o jogador do acionante
        if( isAttackingPlayer ) {
          // Interrompe execução da função para seleção do alvo
          defender = this.target = yield this.selectTarget( 'choose-target-being' );

          // Caso pontos de destino devam ser gastos devido à condição 'admirável', aguarda confirmação do jogador sobre o alvo
          if( this.aweRequiredFatePoints )
            yield new m.ConfirmationModal( {
              text: m.languages.notices.getModalText( 'confirm-fate-point-spent-for-awed-target', {
                target: defender,
                requiredPoints: this.aweRequiredFatePoints
              } ),
              acceptAction: () => action.currentExecution.next(),
              declineAction: () => action.cancel()
            } ).insert();

          // Caso partida não seja uma simulação e ação seja oriunda de um ataque livre, propaga escolha do alvo
          if( !m.GameMatch.current.isSimulation && this.isPropagated )
            m.sockets.current.emit( 'progress-action', {
              matchName: m.GameMatch.current.name,
              data: {
                actionName: this.name,
                actionCommitterId: this.committer.getMatchId(),
                actionIndex: m.GameAction.currents.indexOf( this ),
                value: defender.getMatchId(),
                isMatchId: true
              }
            } );
        }

        // Do contrário
        else {
          // Gera e inseri na tela barra sobre seleção do alvo
          let bar = new m.StaticBar( {
            name: this.name + '-notice-static-bar',
            text: m.languages.notices.getChoiceText( 'choosing-attack-target', {
              playerParity: attackingPlayer.parity,
              sourceCard: this.committer
            } )
          } ).insert();

          // Aguarda seleção do alvo antes de prosseguir execução da função
          defender = this.target = yield;

          // Remove barra sobre seleção do alvo
          bar.remove();

          // Quando aplicável, define pontos de destino que devem ser gastos devido à condição 'admirável'
          m.ConditionAwed.validateAction( this, defender );
        }
      }

      // Uso de pontos de destino para a escolha do alvo
      spentFatePointsForTarget: {
        // Apenas executa bloco caso haja pontos de destino a serem gastos via a condição 'admirável'
        if( !this.aweRequiredFatePoints ) break spentFatePointsForTarget;

        // Gasta pontos de destino requeridos para a escolha do alvo
        attackingPlayer.fatePoints.decrease( committer.content.stats.fatePoints.spend( this.aweRequiredFatePoints ) );

        // Indica que ataque não pode ser cancelado
        isCancelableAttack = false;
      }

      // Inicia acionamento da ação
      await this.startCommit();

      // Caso ação tenha custo de fluxo, encerra seu acionamento, e aguarda seu término
      if( this.flowCost ) yield this.endCommit();

      // Impedimento de ações atuais do defensor
      preventActions: {
        // Não executa bloco caso escopo do ataque atual não seja o próximo
        if( attackScope != 'M' ) break preventActions;

        // Captura eventual ação em andamento do defensor
        let defenderAction = m.GameAction.currents.find( action => action.committer == defender && action.flowCost );

        // Não executa bloco caso defensor não esteja acionando uma ação com custo de fluxo
        if( !defenderAction ) break preventActions;

        // Impedi ação do defensor
        defenderAction.complete( 'prevent' );

        // Indica que ataque não pode ser cancelado
        isCancelableAttack = false;
      }

      // Verificação de possibilidade de ataque livre
      checkFreeAttack: {
        // Não executa bloco caso um ataque livre já esteja em andamento
        if( ActionAttack.freeAttackExecution ) break checkFreeAttack;

        // Define função para capturar ataques válidos
        let freeAttack = ( function () {
          // Identificadores
          var freeAttack = {};

          // Para caso escopo atual seja o próximo
          if( attackScope == 'M' ) {
            // Define nome do ataque livre
            freeAttack.name = `melee-range-free-attack-${ defender.getMatchId() }`;

            // Atribui validação de manobras para o ataque livre
            freeAttack.getValidAttacks =
              ( attackExtension, attack ) => attack.range.replace( /\d/g, '' ) == 'M' && attack.range.replace( /\D/g, '' ) > attackExtension;

            // Inseri na validação argumentos não passados pela função em que será passada
            freeAttack.getValidAttacks = freeAttack.getValidAttacks.bind( null, attackExtension );
          }

          // Para caso defensor seja mirado por 'Arrebatar'
          else if( attack.maneuver instanceof m.ManeuverRapture ) {
            // Define nome do ataque livre
            freeAttack.name = `rapture-free-attack-${ defender.getMatchId() }`;

            // Atribui validação de manobras para o ataque livre
            freeAttack.getValidAttacks = ( attack ) => attack instanceof m.ManeuverManaBolt;
          }

          // Retorna objeto de ataque livre
          return freeAttack;
        } )();

        // Encerra bloco caso não haja possibilidade de defensor desferir ataques livres
        if( !freeAttack.getValidAttacks ) break checkFreeAttack;

        // Define argumentos da modal de confirmação do ataque livre
        let modalArguments = [ 'confirm-free-attack-by-defender', {
              attacker: committer, defender: defender, attack: attack.maneuver, maneuverSource: attack.maneuverSource
            } ];

        // Caso ataque livre a ser iniciado resulte na parada de destino para impedimento do ataque original, cancela-a caso ele já seria impedido
        m.events.breakStart.fate.add( m.GameMatch.current, validateFateBreakStart );

        // Programa execução do ataque livre, e aguarda seu término
        yield ActionAttack.programFreeAttack( defender, {
          name: freeAttack.name,
          defender: committer,
          getValidAttacks: freeAttack.getValidAttacks,
          finishAction: () => setTimeout( resumeActionProgress ),
          modalArguments: modalArguments
        } );

        // Caso prosseguimento da ação não tenha sido revalidado, encerra sua execução
        if( !revalidateActionProgress() ) return;
      }

      // Define defesas válidas
      setValidDefenses: {
        // Não executa bloco caso defensor esteja indefeso
        if( defender.conditions.some( condition => condition instanceof m.ConditionIncapacitated ) ) break setValidDefenses;

        // Identificadores
        let defenderManeuvers = defender.content.stats.effectivity.maneuvers;

        // Itera por fontes de manobras
        for( let rank of [ 'primary', 'secondary' ] ) {
          // Filtra fontes de manobras para as que defensor não tenha mais pontos usáveis
          if( !defenderManeuvers[ rank ].currentPoints ) continue;

          // Itera por defesas da fonte de manobras alvo
          for( let defense of defenderManeuvers[ rank ].defenses ) {
            // Filtra defesa "Equivar"
            if( defense instanceof m.ManeuverDodge ) continue;

            // Filtra defesas inválidas para o ataque de acionante
            if( !attack.maneuver.defenses.includes( defense.constructor ) ) continue;

            // Caso ataque seja 'Bater' e de um mangual, filtra defesa 'Bloquear'
            if( attack.maneuver.source instanceof m.WeaponFlail && attack.maneuver instanceof m.ManeuverBash && defense instanceof m.ManeuverBlock ) continue;

            // Adiciona defesa alvo ao arranjo de defesas válidas
            validDefenses.push( {
              maneuver: defense, maneuverSource: defenderManeuvers[ rank ]
            } );
          }
        }

        // Caso 'Esquivar' não seja uma defesa válida, encerra bloco
        if( !attack.maneuver.defenses.includes( m.ManeuverDodge ) ) break setValidDefenses;

        // Caso defensor não tenha defesa 'Esquivar', encerra bloco
        if( !defenderManeuvers.natural.current.defenses.find( defense => defense instanceof m.ManeuverDodge ) ) break setValidDefenses;

        // Caso defensor não tenha nenhum ponto de agilidade restante, encerra bloco
        if( !defender.content.stats.attributes.current.agility ) break setValidDefenses;

        // Caso defensor esteja vinculado à magia 'Aperto da Terra', encerra bloco
        if( defender.getAllCardAttachments().some( card => card instanceof m.SpellEarthGrip && card.content.effects.isEnabled ) ) break setValidDefenses;

        // Adiciona 'Esquivar' ao arranjo de defesas válidas
        validDefenses.push( {
          maneuver: defenderManeuvers.natural.current.defenses.find( defense => defense instanceof m.ManeuverDodge ),
          maneuverSource: defenderManeuvers.natural.current
        } );
      }

      // Seleciona defesa para ataque
      selectDefense: {
        // Caso não haja defesas válidas, encerra bloco
        if( !validDefenses.length ) break selectDefense;

        // Para caso haja apenas 1 defesa válida
        if( validDefenses.length == 1 ) {
          // Define defesa válida como a escolhida
          defense = validDefenses[ 0 ];

          // Encerra bloco
          break selectDefense;
        }

        // Para caso usuário seja o oponente do atacante
        if( isOpponent ) {
          // Gera e inseri modal de seleção da defesa do ataque
          new m.SelectModal( {
            text: m.languages.notices.getModalText(
              'select-defense-for-attack', { attack: attack.maneuver, maneuverSource: attack.maneuverSource, attacker: committer }
            ),
            inputsArray: validDefenses.map( function ( defense, index ) {
              return {
                text: m.languages.keywords.getManeuver( defense.maneuver.name ) + ' (' + (
                  defense.maneuverSource.source ? `${ defense.maneuverSource.source.content.designation.title } – ` : ''
                ) + ( defense.maneuverSource.currentPoints ?? defender.content.stats.attributes.current.agility ) + 'P)',
                action: function () {
                  // Caso partida não seja uma simulação, propaga escolha da defesa
                  if( !m.GameMatch.current.isSimulation )
                    m.sockets.current.emit( 'progress-action', {
                      matchName: m.GameMatch.current.name,
                      data: {
                        actionName: action.name,
                        actionCommitterId: action.committer.getMatchId(),
                        actionIndex: m.GameAction.currents.indexOf( action ),
                        value: index
                      }
                    } );

                  // Progride execução do ataque
                  action.currentExecution.next( index );
                }
              };
            } )
          } ).replace();

          // Captura defesa escolhida para o ataque
          defense = validDefenses[ yield ];
        }

        // Do contrário
        else {
          // Gera e inseri na tela barra sobre seleção da defesa
          let bar = new m.StaticBar( {
            name: this.name + '-notice-static-bar',
            text: m.languages.notices.getChoiceText( 'choosing-attack-defense', {
              playerParity: opponent.parity,
              action: this
            } )
          } ).insert();

          // Captura defesa escolhida para o ataque
          defense = validDefenses[ yield ];

          // Remove barra sobre seleção da defesa
          bar.remove();
        }
      }

      // Lança parada de combate
      setCombatBreak: {
        // Identificadores
        var combatBreak = new m.CombatBreakElement( {
              name: `${ committer.getMatchId() }-${ attack.maneuver.name }-${ defender.getMatchId() }-${defense?.maneuver.name ?? ''}-${this.commitment.type}`,
              attacker: committer,
              defender: defender,
              attack: attack,
              defense: defense
            } );

        // Inseri na tela parada de combate, vinculando sua progressão à função 'progressCombatBreakExecution'
        combatBreak.insert( progressCombatBreakExecution, isAttackingPlayer );

        // Captura modal em que parada de combate está, e seu botão de submissão
        var combatBreakModal = combatBreak.parent,
            { submitButton } = combatBreakModal.content.inputsSet;

        // Inicia execução da parada de combate, e aguarda sua resolução
        yield ( combatBreak.currentExecution = progressCombatBreak() ).next();

        // Registra resultado da parada de combate
        m.GameMatch.current.log.addEntry( { eventTarget: combatBreak } );

        // Mostra resultado final da parada de combate
        displayFinalResult: {
          // Identificadores
          let bar = new m.StaticBar( {
                name: 'combat-break-result',
                text: m.languages.notices.getOutcome( 'combat-break-result' ),
                isStageChild: true
              } ),
              buttonBitmap = submitButton.bitmap;

          // Remove evento de progressão da parada de combate
          submitButton.removeListener( 'click', progressCombatBreakExecution );

          // Inseri barra indicando que estado atual é o resultado da parada de combate
          bar.insert();

          // Altera texto do botão de submissão
          buttonBitmap.text = m.languages.snippets.getUserInterfaceAction( 'acknowledge' );

          // Torna parada de combate válida
          combatBreak.isValid = true;

          // Altera cor do botão para indicar validade da parada de combate
          submitButton.draw( m.data.colors.darkBlue );

          // Centraliza novo texto do botão de submissão
          buttonBitmap.position.set( submitButton.width * .5 - buttonBitmap.width * .5, submitButton.height * .5 - buttonBitmap.height * .5 );

          // Recentraliza horizontalmente botão de submissão
          submitButton.x = combatBreakModal.width * .5 - submitButton.width * .5;

          // Adiciona ao botão de submissão evento para continuidade do ataque
          submitButton.addListener( 'click', () => action.currentExecution?.next() );

          // Aguarda certo tempo antes de prosseguir execução do ataque
          yield setTimeout( () => action.currentExecution?.next(), 5000 );

          // Remove barra sobre notificação do resultado da parada de combate
          bar.remove();
        }

        // Remove parada de combate
        combatBreak.remove();
      }

      // Finaliza acionamento da ação
      return this.endCommit();

      // Funções

      /// Para progredir parada de combate
      function * progressCombatBreak() {
        // Identificadores
        var players = Object.fromEntries( [ 'odd', 'even' ].map( parity => [ parity, m.GameMatch.current.players[ parity ] ] ) ),
            { attack, defense, attacker, defender, damages } = combatBreak,
            [ attackerManeuvers, defenderManeuvers ] = [ attacker.content.stats.effectivity.maneuvers, defender.content.stats.effectivity.maneuvers ],
            [ attackerAttributes, defenderAttributes ] = [ attacker.content.stats.attributes, defender.content.stats.attributes ];

        // Quando aplicável, realiza configurações a serem executadas após início da parada de combate
        for( let maneuver of [ attack.maneuver, defense?.maneuver ] ) maneuver?.source?.content.effects.configOnCombatBreak?.( combatBreak );

        // Condução da parada de combate
        combatBreakFlow: {
          // Identificadores
          let bodySections = combatBreak.body.sections,
              attackSections = [ bodySections.attack, bodySections.attackModifiers ],
              defenseSections = [ bodySections.defense, bodySections.defenseModifiers ],
              sectionSet = [ attackSections, defenseSections ],
              barTextNames = [ 'choose-attack-points', 'choose-defense-points' ];

          // Itera por conjunto de seções do corpo da parada de combate
          for( let i = 0; i <= sectionSet.length; i++ ) {
            // Identificadores
            let [ currentSections, previousSections ] = [ sectionSet[ i ] ?? [], sectionSet[ i - 1 ] ?? [] ],
                isToEnable = m.GameMatch.current.isSimulation ? true : isAttackingPlayer ? !( i % 2 ) : isOpponent ? i % 2 : false;

            // Quando existentes, desabilita e inativa seções anteriores
            for( let section of previousSections ) section.isActive = section.isEnabled = false;

            // Itera por seções atuais
            for( let section of currentSections ) {
              // Identificadores
              let { availablePoints } = section.body ?? {}

              // Filtra seções que não estejam inseridas
              if( !section.parent ) continue;

              // Filtra seções sem pontos disponíveis
              if( !Number( availablePoints.text ) ) continue;

              // Indica que seção atual é no momento interagível
              section.isActive = true;

              // Determina habilitação da seção alvo
              section.isEnabled = isToEnable;
            }

            // Avança iteração caso não haja nenhuma seção interagível
            if( !currentSections.some( section => section.isActive ) ) continue;

            // Determina validade atual da parada de combate
            combatBreak.isValid = isToEnable;

            // Atualiza cor do botão de submissão segundo validade da parada de combate
            submitButton.draw( combatBreak.isValid ? m.data.colors.darkBlue : m.data.colors.darkFadedBlue );

            // Captura e exibe barra alvo
            let bar = new m.StaticBar( {
              name: 'combat-break-assignment',
              text: m.languages.notices.getChoiceText( barTextNames[ i ] + ( isToEnable ? '-player' : '-other' ), {
                player: i % 2 ? opponent : attackingPlayer,
              } ),
              isStageChild: true,
              isRemovable: !isToEnable || i || !isCancelableAttack ? false : true,
              removeHoverText: !isToEnable || i || !isCancelableAttack ? null : m.languages.snippets.getUserInterfaceAction( 'cancel' ),
              removeAction: !isToEnable || i || !isCancelableAttack ? null : cancelCombatBreak.bind( action )
            } ).insert();

            // Aguarda fim da interação com seções atuais
            yield;

            // Remove barra alvo
            bar.remove();
          }
        }

        // Redução de pontos atribuídos
        assignedPointsReduction: {
          // Identificadores
          let assignedPoints = {};

          // Não executa bloco caso ataque da parada seja 'Reprimir'
          if( attack.maneuver instanceof m.ManeuverRepress ) break assignedPointsReduction;

          // Captura pontos atribuídos à parada de combate
          Object.assign( assignedPoints, combatBreak.getAssignedPoints() );

          // Para caso ataque da parada seja 'Arrebatar'
          if( attack.maneuver instanceof m.ManeuverRapture ) {
            // Programa aplicação do dano colateral do ataque
            attack.maneuver.applyCollateralDamage( assignedPoints.attack );

            // Encerra bloco
            break assignedPointsReduction;
          }

          // Reduz pontos de ataque atribuídos dos pontos da fonte de manobras do atacante
          attackerManeuvers.spendPoints( attack.maneuver, assignedPoints.attack );

          // Caso exista uma defesa, reduz pontos de defesa atribuídos dos pontos da fonte de manobras do defensor, ou de sua agilidade
          if( defense )
            defense.maneuver instanceof m.ManeuverDodge ? defenderAttributes.reduce( assignedPoints.defense, 'agility', { triggeringCard: defender } ) :
                                                          defenderManeuvers.spendPoints( defense.maneuver, assignedPoints.defense );

          // Reduz pontos de agilidade usados nos modificadores de ataque, se aplicável
          if( 'agility' in attackerAttributes.current )
            attackerAttributes.reduce( assignedPoints.attackModifiers.agility ?? 0, 'agility', { triggeringCard: attacker } );

          // Reduz pontos de agilidade usados nos modificadores de defesa, se aplicável
          if( 'agility' in defenderAttributes.current )
            defenderAttributes.reduce( assignedPoints.defenseModifiers.agility ?? 0, 'agility', { triggeringCard: defender } );

          // Caso ataque da parada tenha sido 'Raio de Mana', reduz pontos de mana de Powth de seu canalizador
          if( attack.maneuver instanceof m.ManeuverManaBolt ) attack.maneuver.reduceManaPoints( assignedPoints.attack );
        }

        // Operações variáveis por arma
        weaponConfig: {
          // Identificadores
          let [ attackSource, defenseSource ] = [ attack.maneuver.source, defense?.maneuver.source ];

          // Para caso meio de ataque tenha sido via uma arma
          if( attackSource instanceof m.Weapon ) {
            // Identificadores
            let weaponAttachments = attackSource.getAllCardAttachments();

            // Caso arma usada no ataque da parada seja passível de ser removida, quando aplicável verifica possibilidade de seu afastamento
            if( [ m.WeaponWarHammer, m.WeaponDefaultSpear ].some( constructor => attackSource instanceof constructor ) )
              attackSource.content.effects.checkWeaponRemoval( combatBreak )

            // Caso arma usada no ataque da parada seja um pique, verifica possibilidade de se acionar um ataque livre
            else if( attackSource instanceof m.WeaponPike ) attackSource.content.effects.checkFreeAttack( combatBreak );

            // Caso arma usada no ataque tenha um veneno em uso, prepara aplicação de seu efeito
            weaponAttachments.find( card => card instanceof m.ImplementPoison && card.isInUse )?.content.effects.checkDiseasedGain( combatBreak );
          }

          // Para caso meio de defesa tenha sido via uma arma
          if( defenseSource instanceof m.Weapon ) {
            // Caso arma usada na defesa da parada tenha sido um escutum, quando aplicável registra pontos de defesa usados
            if( defenseSource instanceof m.WeaponScutum ) defenseSource.content.effects.withstandDamage( combatBreak )

            // Caso arma usada na defesa da parada tenha sido um ranseur, quando aplicável verifica possibilidade de afastamento da arma atacante
            else if( defenseSource instanceof m.WeaponRanseur ) defenseSource.content.effects.checkWeaponRemoval( combatBreak );
          }
        }

        // Caso não haja pontos de ataque a serem infligidos, encerra parada de combate
        if( !combatBreak.gaugeInflictedAttack() ) return action.currentExecution.next();

        // Caso não haja pontos de dano a serem infligidos, encerra parada de combate
        if( !damages.filter( damage => damage.totalPoints ).length ) return action.currentExecution.next();

        // Fase da delonga
        overtime: {
          // Identificadores
          let { fateIcons } = combatBreak.footer,
              attackerParity = attackingPlayer.parity,
              opponentParity = opponent.parity,
              isWithEmbeddedFatePoints = Boolean( attacker.content.stats.fatePoints.current || defender.content.stats.fatePoints.current );

          // Encerra bloco caso nenhum dos jogadores possa usar pontos de destino
          if( Object.values( players ).every( player => !player.fatePoints.current.get() ) && !isWithEmbeddedFatePoints ) break overtime;

          // Para caso ataque da parada seja 'Arrebatar'
          if( attack.maneuver instanceof m.ManeuverRapture ) {
            // Identificadores
            let etherDamage = damages.find( damage => damage instanceof m.DamageEther ),
                { totalPoints } = etherDamage,
                isSpirit = attacker instanceof m.Spirit,
                isWithFatePoints = Boolean( players[ attackerParity ].fatePoints.current.get() );

            // Encerra bloco caso dano não possa ser infligido
            if( !etherDamage.validateInfliction(defender) && ( isSpirit || !etherDamage.validateInfliction( defender, totalPoints * 2 ) || !isWithFatePoints ) )
              break overtime;
          }

          // Atribuição dos pontos de destino
          assignFatePoints: {
            // Identificadores
            let currentPlayer = m.GamePlayer.current,
                playerBeing = !currentPlayer ? null : currentPlayer == attackingPlayer ? attacker : defender,
                isWithFatePoints = Boolean( currentPlayer?.fatePoints.current.get() || playerBeing?.content.stats.fatePoints.current ),
                bar;

            // Caso partida não seja uma simulação e usuário alvo não possa usar pontos de destino, indica ao servidor que ele já concluiu a delonga
            if( !m.GameMatch.current.isSimulation && !isWithFatePoints )
              m.sockets.current.emit( 'progress-combat-break-overtime', {
                matchName: m.GameMatch.current.name,
                data: { combatBreakName: combatBreak.name }
              } )

            // Do contrário
            else {
              // Valida parada de combate
              combatBreak.isValid = true;

              // Atualiza cor do botão de submissão para indicar validade da parada de combate
              submitButton.draw( m.data.colors.darkBlue );
            }

            // Indica que rodapé pode ser modificado no momento
            combatBreak.footer.isEnabled = true;

            // Inseri e captura barra sobre atribuição de pontos de destino
            yield bar = new m.StaticBar( {
                name: 'combat-break-overtime',
                text: m.languages.notices.getChoiceText(
                  !currentPlayer ? 'players-choosing-assign-fate-point' : !isWithFatePoints ? 'opponent-choosing-assign-fate-point' : 'choose-assign-fate-point'
                ),
                isStageChild: true
            } ).insert();

            // Indica que rodapé não pode mais ser modificado no momento
            combatBreak.footer.isEnabled = false;

            // Remove barra sobre atribuição de pontos de destino
            bar.remove();
          }

          // Para o uso do ponto de destino pelo atacante
          attakerFatePoint: {
            // Não executa bloco caso jogador do atacante não tenha atribuído um ponto de destino
            if( !fateIcons[ attackerParity ].isToUseFatePoint ) break attakerFatePoint;

            // Remove 1 ponto de destino do jogador do atacante, ou do atacante
            players[ attackerParity ].fatePoints.decrease( attacker.content.stats.fatePoints.spend( 1 ) );
          }

          // Para o uso do ponto de destino pelo oponente
          opponentFatePoint: {
            // Não executa bloco caso oponente não tenha atribuído um ponto de destino
            if( !fateIcons[ opponentParity ].isToUseFatePoint ) break opponentFatePoint;

            // Remove 1 ponto de destino do oponente do atacante, ou do defensor
            players[ opponentParity ].fatePoints.decrease( defender.content.stats.fatePoints.spend( 1 ) );
          }
        }

        // Adiciona evento para aplicar dano do ataque após conclusão de sua ação
        m.events.actionChangeEnd.finish.add( action, applyDamage, { once: true } );

        // Prossegue com execução da ação
        return action.currentExecution.next();

        // Aplica dano da parada de combate
        function applyDamage() {
          // Identificadores
          var mainDamage = damages[ 0 ];

          // Registra no dano principal eventuais danos colaterais
          mainDamage.collateralDamages.push( ...damages.slice( 1 ) );

          // Inflige dano principal, infligindo também seus danos colaterais, quando existentes
          mainDamage.inflictPoints( defender );
        }
      }

      /// Para avançar parada de combate
      function progressCombatBreakExecution() {
        // Apenas executa função caso parada de combate esteja válida
        if( !combatBreak.isValid ) return false;

        // Identificadores
        var activeSections = Object.values( combatBreak.body.sections ).filter( section => section.isActive );

        // Invalida parada de combate
        combatBreak.isValid = false;

        // Atualiza cor do botão de submissão para indicar que parada de combate não é mais válida
        submitButton.draw( m.data.colors.darkFadedBlue );

        // Caso partida seja uma simulação, prossegue execução da parada de combate
        if( m.GameMatch.current.isSimulation ) return combatBreak.currentExecution.next();

        // Delega continuidade da operação em função de se parada de combate está na etapa principal ou na delonga
        return activeSections.length ? progressBody() : progressFooter();

        // Funções

        /// Progride seções no corpo da parada de combate
        function progressBody() {
          // Identificadores
          var assignedPoints = activeSections.map( section => Number( section.body.assignedPoints.text ) );

          // Propaga progressão da parada de combate
          m.sockets.current.emit( 'progress-combat-break-body', {
            matchName: m.GameMatch.current.name,
            data: {
              combatBreakName: combatBreak.name,
              assignedPoints: assignedPoints
            }
          } );

          // Prossegue execução da parada de combate
          combatBreak.currentExecution.next();
        }

        /// Progride seção do rodapé da parada de combate
        function progressFooter() {
          // Identificadores
          var playerParity = m.GamePlayer.current.parity,
              targetIcon = combatBreak.footer.fateIcons[ playerParity ];

          // Propaga progressão da delonga da parada de combate
          m.sockets.current.emit( 'progress-combat-break-overtime', {
            matchName: m.GameMatch.current.name,
            data: {
              combatBreakName: combatBreak.name,
              [ playerParity ]: targetIcon.isToUseFatePoint
            }
          } );

          // Indica que rodapé não pode mais ser modificado no momento
          combatBreak.footer.isEnabled = false;

          // Gera uma barra avisando sobre espera da decisão de outro jogador
          new m.StaticBar( {
            name: 'combat-break-overtime-awaiting',
            text: m.languages.notices.getChoiceText( 'opponent-choosing-assign-fate-point' ),
            isStageChild: true
          } ).insert();
        }
      }

      /// Para cancelar parada de combate
      function cancelCombatBreak() {
        // Caso partida não seja uma simulação, propaga cancelamento do ataque para outros sockets
        if( !m.GameMatch.current.isSimulation )
          m.sockets.current.emit( 'cancel-combat-break', {
            matchName: m.GameMatch.current.name,
            data: {
              combatBreakName: combatBreak.name,
              playerParity: m.GamePlayer.current.parity,
              actionIndex: m.GameAction.currents.indexOf( this )
            }
          } );

        // Remove parada de combate
        combatBreak.remove();

        // Cancela ação
        return this.cancel();
      }

      /// Valida início de eventual parada de destino sobre impedimento de ações, quando iniciada por força de um ataque livre
      function validateFateBreakStart() {
        // Identificadores
        var fateBreak = m.GameMatch.fateBreakExecution;

        // Apenas executa função caso parada de destino a ser iniciada seja a sobre impedimento de ações
        if( !fateBreak?.name.startsWith( 'prevent-actions-fate-break-' ) ) return;

        // Apenas executa função caso ente alvo da parada de destino seja o acionante desta ação
        if( fateBreak.playerBeing != committer ) return;

        // Caso ação não seja mais válida, indica que parada de destino deve ser desconsiderada
        if( !revalidateActionProgress() ) fateBreak.isToSkip = true;
      }

      /// Revalida execução da ação, para caso um ataque livre tenha sido desferido
      function revalidateActionProgress() {
        // Identificadores
        var invalidationMessage = '';

        // Caso eventual equipamento a ser usado no ataque não esteja mais ativo, define mensagem de invalidação apropriada
        if( attack.maneuver.source instanceof m.Item && attack.maneuver.source.activity != 'active' )
          invalidationMessage ||= 'attack-prevented-by-removed-item';

        // Caso não haja mais pontos disponíveis na fonte de manobras do ataque, define mensagem de invalidação apropriada
        if( !attack.maneuverSource.currentPoints )
          invalidationMessage ||= 'attack-prevented-by-lack-of-points';

        // Caso não tenha sido gerada uma mensagem de invalidação, indica que ação pode prosseguir
        if( !invalidationMessage ) return true;

        // Notifica usuário sobre impedimento do ataque
        m.noticeBar.show(
          m.languages.notices.getInvalidationText( invalidationMessage, { attacker: committer } ),
          !m.GameMatch.current.isSimulation && isOpponent ? 'green' : 'red'
        );

        // Finaliza acionamento da ação, enquanto impedindo prosseguimento do ataque
        action.endCommit( 'prevent' );
      }

      /// Retoma execução da ação, após eventual desferimento de um ataque livre
      function resumeActionProgress() {
        // Antes de prosseguir com ataque original, aguarda resolução de todas as eventuais paradas geradas por ataque livre
        while( m.GameMatch.getActiveBreaks().length )
          return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( resumeActionProgress ), { once: true } );

        // Remove evento para cancelamento circunstancial de parada de destino
        m.events.breakStart.fate.remove( m.GameMatch.current, validateFateBreakStart );

        // Quando ainda aplicável, continua execução da ação
        action.currentExecution?.next();
      }
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var { committer, attack: { maneuver, maneuver: { targetType } }, customValidations } = this;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do tipo do alvo
      if( !( target instanceof targetType ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'attack-invalid-target-type', { targetType: m.languages.keywords.getAttachability( targetType.name, { plural: true } ).toLowerCase() }
        ) );

      // Validação da distinção de alvo
      if( committer == target )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-committer' ) );

      // Validação do alcance
      if( !m.GameAction.validateRange( committer.slot, target.slot, maneuver.range ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'attack-invalid-range' ) );

      // Validação da manobra, se aplicável
      if( maneuver.validateTarget && !maneuver.validateTarget( target ) ) return false;

      // Validação do tipo de ente
      if( [ m.Swarm, m.Angel ].some( constructor => target instanceof constructor ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'being-cannot-be-direct-target-of-attacks' ) );

      // Validação do amparo
      if( committer instanceof m.AstralBeing && target.conditions.some( condition => condition instanceof m.ConditionGuarded ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-guarded' ) );

      // Validações personalizadas
      for( let validationObject of customValidations )
        if( !validationObject.validate( target ) ) return m.noticeBar.show( validationObject.message );

      // Validação da condição 'admirável'
      if( !m.ConditionAwed.validateAction( action, target ) ) return false;

      // Indica que alvo é válido
      return true;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer, attack: { maneuver } } = this,
          formerIsQuiet = m.app.isQuiet;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Se escopo do ataque não for próximo, invalida ação caso acionante não esteja em uma casa controlada por seu jogador
      if( !maneuver.range.startsWith( 'M' ) && committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura resultado da validação do alvo da ação
      let validationResult = this.validateTarget();

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna resultado da validação
      return Boolean( validationResult );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa registro de ataque da ação
      this.attack = null;

      // Zera custo de fluxo da ação
      this.flowCost = 0;

      // Redefine indicação de pontos de destino requeridos pela condição 'admirável' para escolha do alvo
      this.aweRequiredFatePoints = 0;
    }

    /// Determina ataques habilitados para o acionante
    action.getEnabledAttacks = function () {
      // Identificadores
      var enabledAttacks = {},
          { committer } = this,
          committerManeuvers = committer.content.stats.effectivity.maneuvers,
          committerOwner = committer.getRelationships().owner,
          { field } = m.GameMatch.current.environments,
          fieldCards = [ ...field.grids.odd.cards, ...field.divider.cards, ...field.grids.even.cards ];

      // Itera por fontes de manobras
      for( let rank of [ 'primary', 'secondary' ] ) {
        // Filtra fontes de manobras sem pontos
        if( !committerManeuvers[ rank ].currentPoints ) continue;

        // Atribui ao arranjo de ataques habilitados os da fonte de manobras alvo
        enabledAttacks[ rank ] = committerManeuvers[ rank ].attacks;

        // Caso apenas o acionante esteja no campo, filtra manobras cujo alcance não seja 'G2'
        if( fieldCards.length < 2 )
          enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => attack.range == 'G2' )

        // Caso o acionante não esteja em uma casa com outras cartas, filtra manobras próximas e as de alcance 'R0'
        else if( !committer.slot.cards || committer.slot.cards.length < 2 )
          enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => !attack.range.startsWith( 'M' ) && attack.range != 'R0' )

        // Caso o acionante esteja em uma casa não controlada por seu jogador, filtra manobras não próximas
        else if( committer.slot.getController() != committerOwner )
          enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => attack.range.startsWith( 'M' ) );

        // Filtra manobras que estejam sem nenhum dano
        enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => Object.values( attack.damage ).some( value => value ) );

        // Para ataques livres, filtra manobras 'Disparar' de bestas
        if( this.commitment.type == 'free' && committerManeuvers[ rank ].source instanceof m.WeaponCrossbow )
          enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => !( attack instanceof m.ManeuverShoot ) );

        // Caso o acionante esteja vinculado a uma magia 'Casulo Eólico', filtra manobras 'Disparar' e 'Arremessar'
        if( committer.getAllCardAttachments().some( attachment => attachment instanceof m.SpellAirCocoon && attachment.content.effects.isEnabled ) )
          enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack =>
            [ m.ManeuverShoot, m.ManeuverThrow ].every( constructor => !( attack instanceof constructor ) )
          );

        // Para caso o acionante esteja vinculado a uma magia 'Aperto da Terra'
        if( committer.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) ) {
          // Caso não haja em execução um ataque livre próximo provocado pelo alcance de uma arma, filtra manobras próximas
          if( !m.ActionAttack.freeAttackExecution?.name.startsWith( 'melee-range-free-attack-' ) )
            enabledAttacks[ rank ] = enabledAttacks[ rank ].filter( attack => !attack.range.startsWith( 'M' ) );
        }

        // Registra origem da fonte de manobras alvo
        enabledAttacks[ rank ].source = committerManeuvers[ rank ].source;
      }

      // Retorna ataques habilitados
      return enabledAttacks;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionAttack.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-attack' ),
        cardManeuvers = card.content.stats.effectivity.maneuvers,
        isWithUsableAttacks = [ 'primary', 'secondary' ].some( rank => cardManeuvers[ rank ].attacks.length && cardManeuvers[ rank ].currentPoints );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-content' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa e tenha 1 ou mais ataques habilitados em uma fonte de manobras com pontos
    if( card.activity == 'active' && isWithUsableAttacks ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionAttack( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }

  // Programa iniciação de uma parada de ataque livre
  ActionAttack.programFreeAttack = function ( committer, config = {} ) {
    // Para caso já exista um ataque livre em andamento
    if( this.freeAttackExecution ) {
      // Caso função de finalização do ataque livre exista, executa-a após intervalo
      if( config.finishAction ) setTimeout( config.finishAction, 0, false );

      // Encerra função
      return;
    }

    // Inicia parada de ataque livre
    this.freeAttackExecution = this.executeFreeAttack( committer, config );

    // Executa parada de ataque livre
    return setTimeout( () => ActionAttack.freeAttackExecution.next() );
  }

  // Executa uma parada de ataque livre
  ActionAttack.executeFreeAttack = function * ( committer, config = {} ) {
    // Identificadores pré-validação
    var { name, defender = null, getValidAttacks = () => true, customValidations = [], finishAction, modalArguments } = config,
        freeAttackComponent;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( this.freeAttackExecution );
      m.oAssert( committer instanceof m.Being );
      m.oAssert( Object.keys( config ).every( key => [
        'name', 'defender', 'getValidAttacks', 'customValidations', 'finishAction', 'modalArguments'
      ].includes( key ) ) );
      m.oAssert( name && typeof name == 'string' );
      m.oAssert( !defender || defender instanceof m.Being );
      m.oAssert( typeof getValidAttacks == 'function' );
      m.oAssert( Array.isArray( customValidations ) && customValidations.every( object =>
        object.constructor == Object && typeof object.validate == 'function' && object.message && typeof object.message == 'string'
      ) );
      m.oAssert( !finishAction || typeof finishAction == 'function' );
      m.oAssert( Array.isArray( modalArguments ) );
      m.oAssert( modalArguments[ 0 ] && typeof modalArguments[ 0 ] == 'string' );
      m.oAssert( !modalArguments[ 1 ] || modalArguments[ 1 ].constructor == Object );
    }

    // Identificadores pós-validação
    var { controller: attackingPlayer, opponent } = committer.getRelationships(),
        isAttackingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == attackingPlayer,
        isOpponent = m.GameMatch.current.isSimulation || m.GamePlayer.current == opponent;

    // Registra nome do ataque livre
    this.freeAttackExecution.name = name;

    // Sinaliza início da parada de ataque livre
    m.events.breakStart.freeAttack.emit( m.GameMatch.current, { player: attackingPlayer, opponent: opponent, committer: committer } );

    // Captura valor original de 'isQuiet'
    let formerIsQuiet = m.app.isQuiet;

    // Força para ativada configuração 'isQuiet'
    m.app.isQuiet = true;

    // Caso parada de ataque livre deva ser desconsiderada, encerra função
    if( this.freeAttackExecution.isToSkip ) return finishAttack();

    // Não aciona ataque livre caso período não seja o do combate
    if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return finishAttack();

    // Não aciona ataque livre caso acionante não esteja ativo
    if( committer.activity != 'active' ) return finishAttack();

    // Não aciona ataque livre caso acionante esteja ocupado
    if( committer.readiness.status == 'occupied' ) return finishAttack();

    // Não aciona ataque livre caso acionante já esteja cometendo alguma ação
    if( m.GameAction.currents.some( action => action.committer == committer ) ) return finishAttack();

    // Não aciona ataque livre caso acionante e defensor sejam a mesma carta
    if( committer == defender ) return finishAttack();

    // Não aciona ataque livre caso defensor seja um enxame ou um anjo
    if( [ m.Swarm, m.Angel ].some( constructor => defender instanceof constructor ) ) return finishAttack();

    // Não aciona ataque livre caso acionante seja um ente astral e defensor esteja amparado
    if( committer instanceof m.AstralBeing && defender?.conditions.some( condition => condition instanceof m.ConditionGuarded ) ) return finishAttack();

    // Identificadores para validação de manobras
    let committerManeuvers = committer.content.stats.effectivity.maneuvers,
        validRanks = [ 'primary', 'secondary' ].filter( rank => committerManeuvers[ rank ].attacks.length && committerManeuvers[ rank ].currentPoints ),
        validAttacks = validRanks.flatMap( rank => committerManeuvers[ rank ].attacks.filter( getValidAttacks ).map( function ( attack ) {
          return { maneuver: attack, maneuverSource: committerManeuvers[ rank ] }
        } ) );

    // Não aciona ataque livre caso acionante não tenha ataques válidos
    if( !validAttacks.length ) return finishAttack();

    // Para caso um defensor já tenha sido definido
    if( defender ) {
      // Submete ataques a suas respectivas validações padrão ante o defensor
      validAttacks = validAttacks.filter( attackObject =>
        defender instanceof attackObject.maneuver.targetType && ( !attackObject.maneuver.validateTarget || attackObject.maneuver.validateTarget( defender ) )
      );

      // Caso parada de ataque livre não seja sobre um ataque em resposta a arrebatar, submete ataques à validação do alcance
      if( !this.freeAttackExecution.name.startsWith( 'rapture-free-attack' ) )
        validAttacks = validAttacks.filter( attackObject => m.GameAction.validateRange( committer.slot, defender.slot, attackObject.maneuver.range ) );

      // Não aciona ataque livre caso acionante não tenha mais ataques válidos
      if( !validAttacks.length ) return finishAttack();
    }

    // Gera ataque livre, e captura ataques lhe habilitados
    let committerAttack = new ActionAttack( {
          committer: committer, target: defender, customValidations: customValidations, isFree: true, isPropagated: true
        } ),
        enabledAttacks = Object.values( committerAttack.getEnabledAttacks() ).flat();

    // Submete ataques válidos ao filtro padrão de ataques habilitados
    validAttacks = validAttacks.filter( attackObject => enabledAttacks.some( attack => attack == attackObject.maneuver ) );

    // Não aciona ataque livre caso acionante não tenha mais ataques válidos
    if( !validAttacks.length ) return finishAttack();

    // Não aciona ataque livre caso um defensor já tenha sido definido e ação tenha sido invalidada por 'admirável' ante ele
    if( defender && !m.ConditionAwed.validateAction( committerAttack, defender ) ) return finishAttack();

    // Restaura configuração 'isQuiet' para seu valor original
    m.app.isQuiet = formerIsQuiet;

    // Para caso usuário seja o a desferir ataque livre
    if( isAttackingPlayer ) {
      // Identificadores
      let freeAttackText = m.languages.notices.getModalText( ...modalArguments );

      // Caso o uso de pontos de destino seja requerido devido à condição 'admirável', acrescenta essa informação ao texto sobre o ataque livre
      if( committerAttack.aweRequiredFatePoints )
        freeAttackText += m.languages.notices.getModalText( 'awed-free-attack-addendum', {
          target: defender,
          requiredPoints: committerAttack.aweRequiredFatePoints
        } );

      // Gera componente para decisão sobre desferimento ou não do ataque livre
      freeAttackComponent = new m.ConfirmationModal( {
        text: freeAttackText,
        acceptAction: () => progressExecution( true ),
        declineAction: () => progressExecution( false )
      } ).insert();
    }

    // Do contrário
    else {
      // Gera componente avisando decisão sobre o ataque livre
      freeAttackComponent = new m.StaticBar( {
        text: m.languages.notices.getChoiceText( 'choosing-free-attack', {
          playerParity: attackingPlayer.parity,
          committer: committer,
          defender: defender
        } )
      } ).insert();
    }

    // Captura decisão sobre desferimento do ataque livre
    let isToMakeFreeAttack = yield;

    // Caso usuário não seja o jogador responsável pela decisão, informa-o sobre essa decisão
    if( !isAttackingPlayer )
      m.noticeBar.show(
        m.languages.notices.getChoiceText( 'free-attack-choice', { playerParity: attackingPlayer.parity, isToAttack: isToMakeFreeAttack } ),
        isOpponent && !isToMakeFreeAttack ? 'green' : 'red'
      );

    // Caso acionante tenha optado por não desferir ataque livre, encerra função
    if( !isToMakeFreeAttack ) return finishAttack();

    // Remove da tela componente sobre o ataque livre
    freeAttackComponent.remove();

    // Caso haja apenas 1 manobra válida para desferir o ataque livre, registra-a em si
    if( validAttacks.length == 1 ) committerAttack.attack = validAttacks[ 0 ]

    // Do contrário
    else {
      // Caso usuário seja o a desferir o ataque livre, gera e inseri componente de escolha da manobra do ataque
      if( isAttackingPlayer )
        freeAttackComponent = new m.SelectModal( {
          text: m.languages.notices.getModalText( 'select-maneuver-for-free-attack' ),
          inputsArray: validAttacks.map( function ( attack, index ) {
            return {
              text: m.languages.keywords.getManeuver( attack.maneuver.name ) + ' (' + (
                attack.maneuverSource.source ? `${ attack.maneuverSource.source.content.designation.title } – ` : ''
              ) + attack.maneuverSource.currentPoints + 'P)',
              action: () => progressExecution( index )
            };
          } )
        } ).replace();

      // Do contrário, notifica a usuário sobre escolha de uma manobra para o ataque livre
      else
        freeAttackComponent = new m.StaticBar( {
          text: m.languages.notices.getChoiceText( 'choosing-free-attack-maneuver', {
            playerParity: attackingPlayer.parity,
            committer: committer
          } ),
        } ).insert();

      // Captura do ataque escolhido
      committerAttack.attack = validAttacks[ yield ];
    }

    // Remove da tela componente sobre o ataque livre
    freeAttackComponent.remove();

    // Adiciona eventos para executar função de conclusão do ataque após seu fim
    for( let eventName of [ 'complete', 'cancel' ] )
      m.events.actionChangeEnd[ eventName ].add( committerAttack, eventData => finishAttack( eventData.eventType == 'finish' ) );

    // Inicia execução do ataque livre
    return ( committerAttack.currentExecution = committerAttack.execute() ).next();

    // Funções

    /// Progride parada de ataque livre
    function progressExecution( value ) {
      // Caso partida não seja uma simulação, propaga progressão da parada do ataque livre
      if( !m.GameMatch.current.isSimulation )
        m.sockets.current.emit( 'progress-free-attack-break', {
          matchName: m.GameMatch.current.name,
          data: {
            freeAttackName: ActionAttack.freeAttackExecution.name,
            value: value
          }
        } );

      // Prossegue execução da função principal
      ActionAttack.freeAttackExecution.next( value );
    }

    /// Encerra execução da parada de ataque livre atual
    function finishAttack( wasCommited = false ) {
      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Nulifica iterador do ataque livre
      ActionAttack.freeAttackExecution = null;

      // Quando existente, remove da tela componente sobre o ataque livre
      freeAttackComponent?.remove();

      // Quando existente, executa função de conclusão do ataque livre
      finishAction?.( wasCommited );

      // Sinaliza fim da parada de ataque livre
      m.events.breakEnd.freeAttack.emit( m.GameMatch.current, { player: attackingPlayer, opponent: opponent, committer: committer, wasCommited: wasCommited } );
    }
  }
}

/// Propriedades do protótipo
ActionAttack.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionAttack }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionAttack );
