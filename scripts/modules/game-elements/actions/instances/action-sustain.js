// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionSustain = function ( config = {} ) {
  // Iniciação de propriedades
  ActionSustain.init( this );

  // Identificadores
  var { committer, target } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( committer.content.stats.combativeness.channeling );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( target instanceof m.Spell );
    m.oAssert( target.content.typeset.duration == 'sustained' );
    m.oAssert( target.content.stats.current.flowCost.toSustain > 0 );
  }

  // Superconstrutor
  m.GameAction.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição do custo de fluxo da ação
  this.flowCost = target.content.stats.current.flowCost.toSustain;

  /// Atribuição do custo de mana da ação
  this.manaToAssign = target.content.stats.current.manaCost.toSustain;

  /// Remoção do evento para registrar acionamento da ação
  m.events.actionChangeStart.commit.remove( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionSustain.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionSustain );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-sustain';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'manaToAssign', 'target', 'target.target' ];

    /// Indica se ação deve ser animada no momento de seu acionamento
    action.isToAnimateAtCommit = false;

    /// Mana a ser atribuído para a sustentação da magia
    action.manaToAssign = 0;

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Aciona ação
      yield this.commit();

      // Enquanto magia puder ser sustentada, realiza sua sustentação
      while( this.target.sustain( this ) ) yield;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer, target: targetSpell } = this,
          { target: spellTarget } = targetSpell;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Invalida ação caso magia alvo não esteja ativa
      if( targetSpell.activity != 'active' ) return false;

      // Invalida ação caso magia alvo não esteja em uso
      if( !targetSpell.isInUse ) return false;

      // Invalida ação caso efeito da magia alvo não esteja ativo
      if( !targetSpell.content.effects.isEnabled ) return false;

      // Invalida ação caso acionante não esteja mais em uma casa controlada por seu jogador
      if( committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Invalida ação caso acionante esteja com ao menos 3 marcadores de enfurecido
      if( committer.conditions.find( condition => condition instanceof m.ConditionEnraged )?.markers >= 3 ) return false;

      // Identificadores para o prosseguimento da revalidação
      let validationResult = true,
          formerIsQuiet = m.app.isQuiet;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Atualiza resultado da validação com validação do dono ante a magia
      validationResult &&= targetSpell.content.effects.validateOwner?.( committer, this ) ?? true;

      // Para magias com mana a ser gasto, atualiza resultado da validação com validação desse mana
      if( this.manaToAssign ) validationResult &&= m.Spell.validateManaCost( this );

      // Para magias com alvo, atualiza resultado da validação com validação desse alvo
      if( spellTarget ) validationResult &&= m.Spell.validateTarget( this );

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Invalida ação caso quando isso tiver sido explicitado
      if( !validationResult ) return false;

      // Indica que ação foi revalidada com sucesso
      return true;
    }

    /// Limpa dados relativos à ação
    action.clearData = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: spell } = eventData;

      // Zera estatísticas da ação sobre custo de fluxo e mana a ser atribuído
      for( let key of [ 'flowCost', 'manaToAssign' ] ) this[ key ] = 0;

      // Desusa magia
      spell.disuse();

      // Quando aplicável, remove da ação configurações adicionadas por sua magia
      spell.unconfigSustainAction?.( this );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }
}

/// Propriedades do protótipo
ActionSustain.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionSustain }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionSustain );
