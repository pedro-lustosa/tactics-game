// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionSwap = function ( config = {} ) {
  // Iniciação de propriedades
  ActionSwap.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Card );
    m.oAssert( committer.deck instanceof m.MainDeck );
    m.oAssert( !committer.content.stats.command );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionSwap.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionSwap );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-swap';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.ArrangementStep ];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-card' );

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerDeck, targetDeck ] = [ this.committer.deck, target.deck ?? target.summoner?.deck ],
          [ committerOwner, targetOwner ] = [ committerDeck.owner, targetDeck?.owner ],
          formerActivity = this.committer.activity;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-player' ) );

      // Validação do baralho
      if( committerDeck.sideDeck != targetDeck )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-side-deck' ) );

      // Validação de comandante
      if( target.content.stats?.command )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );

      // Simula alheiamento do acionante
      this.committer.activity = 'unlinked';

      // Valida baralhos, e captura objeto de validação
      let validation = committerDeck.swap( { old: [ this.committer ], new: [ target ] } ).shift(),
          { invalidData } = validation;

      // Para caso conjunto de baralhos esteja inválido
      if( !validation.isValid ) {
        // Restaura atividade do acionante para a original
        this.committer.activity = formerActivity;

        // Para caso preço de um baralho esteja inválido
        if( validation.maxCoinsSpent === false )
          return m.noticeBar.show( m.languages.notices.getInvalidationText(
            'swap-over-max-coins', { invalidDeckTypes: invalidData.maxCoinsSpent }
          ) );

        // Itera por validações de comando
        for( let validationType of [ 'minCommandCards', 'maxCommandCards' ] ) {
          // Filtra tipos de validação não inválidos
          if( validation[ validationType ] !== false ) continue;

          // Notifica usuário sobre invalidação do comando, quando aplicável
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'swap-invalid-command' ) );
        }

        // Notificação de invalidação genérica, quando aplicável
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'swap-invalid' ) );
      }

      // Reverte troca de cartas entre baralhos
      this.committer.deck.swap( { old: [ this.committer ], new: [ target ] }, true );

      // Restaura atividade do acionante para a original
      this.committer.activity = formerActivity;

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Alheia acionante
      committer.unlink();

      // Realiza troca entre cartas
      committer.deck.swap( { old: [ committer ], new: [ target ] }, true );

      // Redispõe cartas em seus ambientes
      for( let method of [ 'clear', 'arrange' ] ) m.GameMatch.current.decks[ method ]( [ committer.deck.owner.parity ] );

      // Captura eventual carta sobre que cursor esteja
      let cardInFocus = m.GameMatch.current.decks.getAllCards().find( card => card.oCheckCursorOver() );

      // Caso cursor esteja sobre uma carta, atualiza seu texto suspenso
      if( cardInFocus ) m.Card.updateHoverText( { currentTarget: cardInFocus } );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionSwap.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-swap' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Card );
      m.oAssert( !card.isToken );
      m.oAssert( !card.content.stats.command );
      m.oAssert( !eventCategory || eventCategory == 'deck-change' );
    }

    // Para caso carta esteja no baralho principal
    if( card.deck instanceof m.MainDeck ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionSwap( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionSwap.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionSwap }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionSwap );
