// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionGuard = function ( config = {} ) {
  // Iniciação de propriedades
  ActionGuard.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Angel );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionGuard.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionGuard );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-guard';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-being' );

      // Aciona ação
      return this.commit();
    }

     /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerController, targetOwner ] = [ this.committer.getRelationships().controller, target.getRelationships?.().owner ];

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerController != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-player' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Being ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-being' ) );

      // Validação do tipo de ente
      if( [ m.Swarm, m.Angel ].some( constructor => target instanceof constructor ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'being-cannot-be-direct-target-of-actions' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação do amparo
      if( target.conditions.some( condition => condition instanceof m.ConditionGuarded ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'being-is-already-guarded' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: target } = eventData;

      // Torna ente amparado
      new m.ConditionGuarded().apply( target );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionGuard.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-guard' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Angel );
      m.oAssert( !eventCategory || [ 'card-activity' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa
    if( card.activity == 'active' ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionGuard( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionGuard.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionGuard }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionGuard );
