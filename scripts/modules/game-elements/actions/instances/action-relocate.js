// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionRelocate = function ( config = {} ) {
  // Iniciação de propriedades
  ActionRelocate.init( this );

  // Identificadores
  var { committer, formerSlot } = config,
      matchFlow = m.GameMatch.current?.flow;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( !( matchFlow?.step instanceof m.DeploymentStep ) || committer instanceof m.Halfling );
    m.oAssert( committer.activity == 'active' );
  }

  // Configurações pré-superconstrutor

  /// Se aplicável, registra casa de referência em que acionante estava
  this.formerSlot = formerSlot ?? this.formerSlot;

  /// Em períodos do combate, ajusta subtipo de acionamento para parcial
  if( matchFlow?.period instanceof m.CombatPeriod ) this.commitment.subtype = 'partial'

  /// Em etapas da mobilização, ajusta tipo de iteratividade para singular
  else if( matchFlow?.step instanceof m.DeploymentStep ) this.iterativity = 'single';

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionRelocate.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionRelocate );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-relocate';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.DeploymentStep, m.CombatPeriod, m.RedeploymentPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [];

    /// Identifica nome da superação
    action.superActionName = 'action-move';

    /// Casa de referência em que acionante estava, para ações 'posicionar' mútuas que levaram o acionante a ser retirado do campo
    action.formerSlot = null;

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-card-slot' );

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var committerOwner = this.committer.getRelationships().owner;

      // Validação do componente
      if( target instanceof m.Card )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card-slot' ) );

      // Validação da distinção de alvo
      if( this.committer.slot == target )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-committer-card-slot' ) );

      // Validação para casas em grades do campo
      if( target instanceof m.FieldGridSlot ) {
        // Validação do jogador
        if( committerOwner != target.owner )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-opponent-component' ) );

        // Validação para casas ocupadas
        if( target.card ) {
          // Identificadores
          let matchFlow = m.GameMatch.current.flow,
              openMovesCount = matchFlow.moves[ committerOwner.parity ].filter( move => move.isOpen ).length;

          // Validação da ação
          if( !target.card.actions.some( action => action instanceof ActionRelocate ) )
            return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-occupant-able-to-commit-relocate' ) );

          // Validação da jogada
          if( !( matchFlow.period instanceof m.CombatPeriod ) && openMovesCount < 2 ) {
            // Antes de invalidar alvo, considera característica 'adaptiveTactics', de humanos
            if( openMovesCount + m.Human.adaptiveTacticsOnMutualMoves( this.committer, target.card ) < 2 )
              return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-occupant-able-to-do-distinct-move' ) );
          }
        }
      }

      // Validação para zonas de engajamento
      else if( target instanceof m.EngagementZone ) {
        // Validação do período
        if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-field-grid-slot' ) );

        // Validação da ocupação
        if( !target.cards.length )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'engagement-zone-target-must-be-occupied' ) );

        // Validação do limite de cartas
        if( target.cards[ committerOwner.parity ].length >= m.EngagementZone.maxCards )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-player-occupants' ) );
      }

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Para caso casa alvo seja de uma grade do campo e tenha um ocupante
      if( target instanceof m.FieldGridSlot && target.card ) {
        // Identificadores
        let currentOccupant = target.card,
            relocateAction = new ActionRelocate( { committer: currentOccupant, target: committer.slot, formerSlot: target, isPropagated: true } );

        // Remove do campo acionante e ocupante atual de alvo
        for( let card of [ committer, currentOccupant ] ) target.environment.remove( card );

        // Adiciona evento para que ação 'posicionar' do ocupante original de alvo seja executada após o fim do acionamento desta
        m.events.actionChangeEnd.commit.add( this, () => ( relocateAction.currentExecution = relocateAction.execute() ).next(), { once: true } );
      }

      // Posiciona acionante em casa alvo
      target.environment.put( committer, target.name );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Nulifica casa de referência em que acionante estava
      this.formerSlot = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionRelocate.controlAvailability = function ( eventData = {} ) {
    // Não executa função caso esse construtor esteja no arranjo de ações acionadas da carta alvo
    if( eventData.eventTarget.actions.singleCommitments.includes( ActionRelocate ) ) return;

    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-relocate' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( !( m.GameMatch.current?.flow.step instanceof m.DeploymentStep ) || card instanceof m.Halfling );
      m.oAssert( !eventCategory || eventCategory == 'card-activity' );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso não se esteja no período do combate e carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) && card.slot instanceof m.EngagementZone ) break controlValidation;

      // Caso carta esteja vinculada a 'Aperto da Terra', invalida habilitação da ação
      if( card.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) )
        break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionRelocate( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionRelocate.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionRelocate }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionRelocate );
