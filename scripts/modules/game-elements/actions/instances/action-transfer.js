// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionTransfer = function ( config = {} ) {
  // Iniciação de propriedades
  ActionTransfer.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.getAllCardAttachments( 'external' ).some( attachment => attachment instanceof m.Item ) );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionTransfer.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionTransfer );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-transfer';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.RedeploymentPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'target', 'itemOwner' ];

    /// Identifica nome da superação
    action.superActionName = 'action-move';

    /// Arranjo de vinculantes válidos
    action.validAttachers = [];

    /// Ente a ser o novo dono do equipamento mirado pela ação
    action.itemOwner = null;

    /// Componente a ser o vinculante do equipamento mirado pela ação
    action.itemAttacher = null;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        itemOwner: this.itemOwner.getMatchId(),
        itemAttacher: this.itemAttacher.getMatchId()
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'itemOwner', 'itemAttacher' ].every( key => actionObject[ key ] && typeof actionObject[ key ] == 'string' ) );

      // Atribui à ação seu novo dono e vinculante
      for( let key of [ 'itemOwner', 'itemAttacher' ] ) this[ key ] = m.Card.getFromMatchId( actionObject[ key ] );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define propriedades-chave da ação
      setProperties: {
        // Não executa bloco caso ação já tenha um alvo
        if( this.target ) break setProperties;

        // Interrompe execução da função para seleção de seu alvo
        this.target = yield this.selectTarget( 'choose-target-item' );

        // Interrompe execução da função para seleção do novo dono do equipamento alvo, e da montagem do arranjo de vinculantes válidos
        this.itemOwner = yield this.selectTarget( 'choose-new-owner' );

        // Para caso equipamento alvo só tenha um vinculante válido
        if( this.validAttachers.length == 1 ) {
          // Define vinculante do equipamento mirado pela ação como o único válido
          this.itemAttacher = this.validAttachers[ 0 ];

          // Encerra bloco
          break setProperties;
        }

        // Gera modal para seleção do vinculante do equipamento mirado pela ação, e aguarda essa seleção
        yield new m.SelectModal( {
          text: m.languages.notices.getModalText( 'select-item-attacher' ),
          inputsArray: this.validAttachers.map( function ( attacher ) {
            return {
              text: attacher.content.designation.title + ' (' +
                ( attacher.isEmbedded ?
                    m.languages.snippets.embedded :
                    m.languages.snippets.getSomethingIn( attacher.slot.name.slice( attacher.slot.name.indexOf( '-' ) + 1 ) )
                ) +
              ')',
              action: function () {
                // Define vinculante do equipamento mirado pela ação como o escolhido pelo jogador
                action.itemAttacher = attacher;

                // Prossegue com execução da ação
                return action.currentExecution.next();
              }
            };
          } )
        } ).insert();
      }

      // Aciona ação
      return this.commit();
    }

    /// Valida alvos da ação
    action.validateTarget = function ( target ) {
      // Delega validação em função do alvo a ser definido
      return !this.target ? this.validateItem( target ) : this.validateOwner( target );
    }

    /// Valida equipamento da ação
    action.validateItem = function ( target = this.target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ];

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Item ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-item' ) );

      // Validação do tipo de vinculação
      if( target.isEmbedded )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-external-item' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação do dono
      if( !this.committer.getAllCardAttachments( 'external' ).includes( target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-owned-by-committer' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Valida dono da ação, e seus vinculantes, se aplicável
    action.validateOwner = function ( target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ],
          formerIsQuiet = m.app.isQuiet;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Humanoid ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-humanoid' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação da localização
      if( !( target.slot instanceof m.FieldGridSlot ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-in-player-grid' ) );

      // Para caso alvo do equipamento seja um humanoide
      if( this.target.content.typeset.attachability == m.Humanoid ) {
        // Validação da distinção de alvo
        if( this.committer == target )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-committer' ) );

        // Caso equipamento não possa ser vinculado ao novo dono, invalida-o
        if( !m.Item.validateAttacher( this.target, target ) ) return false;

        // Adiciona o novo dono ao arranjo de vinculantes válidos
        this.validAttachers.push( target );

        // Indica que alvo é válido
        return true;
      }

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura vinculados que podem ser o vinculante do alvo passado
      this.validAttachers = target.getAllCardAttachments().filter(
        attachment => attachment instanceof m.Weapon && m.Item.validateAttacher( this.target, attachment )
      );

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Validação de vinculantes válidos para o alvo
      if( !this.validAttachers.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'no-valid-attachers' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData,
          { itemAttacher } = this;

      // Vincula equipamento ao vinculante escolhido
      target.activate( itemAttacher, { triggeringCard: committer } );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa arranjo de vinculantes válidos
      while( this.validAttachers.length ) this.validAttachers.shift();

      // Nulifica vinculante e dono do equipamento mirado pela ação
      for( let key of [ 'itemOwner', 'itemAttacher' ] ) this[ key ] = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionTransfer.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-transfer' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || eventCategory == 'card-content' );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( card.slot instanceof m.EngagementZone ) break controlValidation;

      // Caso carta não tenha nenhum equipamento externo, invalida habilitação da ação
      if( !card.getAllCardAttachments( 'external' ).some( attachment => attachment instanceof m.Item ) ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionTransfer( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionTransfer.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionTransfer }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionTransfer );
