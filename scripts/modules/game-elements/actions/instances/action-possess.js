// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionPossess = function ( config = {} ) {
  // Iniciação de propriedades
  ActionPossess.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ m.Spirit, m.Demon ].some( constructor => committer instanceof constructor ) );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionPossess.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionPossess );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-possess';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Custo de fluxo
    action.flowCost = 2;

    /// Executa operação da ação
    action.execute = async function * () {
      // Identificadores
      var { committer } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Se ainda inexistente, define alvo da ação
      let target = this.target ??= yield this.selectTarget( 'choose-target-being' );

      // Aciona ação
      await this.commit();

      // Inicia parada de destino sobre interrompimento da ação
      yield m.GameMatch.programFateBreak( committer.getRelationships().opponent, {
        name: `${ this.name }-fate-break-${ committer.getMatchId() }`,
        playerBeing: this.target,
        opponentBeing: committer,
        isContestable: committer instanceof m.Demon,
        successAction: () => action.complete( 'prevent' ),
        failureAction: () => action.currentExecution.next(),
        modalArguments: [ `fate-break-${ this.name }`, { action: this } ]
      } );

      // Impede ações em andamento do alvo
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == target ) );

      // Adiciona ação ao arranjo de efeitos que tornam o alvo ocupado
      target.readiness.relatedEffects.occupied.unshift( this );

      // Atualiza disponibilidade do alvo
      target.readiness.update();

      // Retorna ação
      return this;
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var { committer } = this,
          committerAttributes = committer.content.stats.attributes,
          targetAttributes = target.content?.stats?.attributes;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação da raça
      if( !( target instanceof m.BioticBeing ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-biotic-being' ) );

      // Validação do tipo de ente
      if( target instanceof m.Swarm )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'being-cannot-be-direct-target-of-actions' ) );

      // Validação de comandante
      if( target.content.stats.command )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação da disponibilidade
      if( !this.isCommitted && m.GameAction.currents.some( action => action instanceof ActionPossess && action.target == target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'possess-must-target-unpossessed-target' ) );

      // Validação da existência da condição
      if( target.conditions.some( condition => condition instanceof m.ConditionPossessed ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'must-not-be-with-possess-condition' ) );

      // Validação da vontade
      if( targetAttributes.current.will >= committerAttributes.current.will )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-will-than-committer' ) );

      // Validação do vigor
      if( targetAttributes.current.stamina * 2 >= committerAttributes.current.energy )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-stamina-than-committer-energy' ) );

      // Validação da experiência
      if( ( target.content.typeset.experience?.level ?? 0 ) >= committerAttributes.current.power )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-experience-than-committer-power' ) );

      // Validação da aplicação da condição 'possuído' ante 'Santuário' e 'Purificação Corporal'
      if( [ m.SpellSanctuary, m.SpellBodyCleansing ].some( constructor => !constructor.validateConditionApply( target, 'condition-possessed', true ) ) )
        return false;

      // Validação do amparo
      if( target.conditions.some( condition => condition instanceof m.ConditionGuarded ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-guarded' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer } = this,
          formerIsQuiet = m.app.isQuiet;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura resultado da validação do alvo da ação
      let validationResult = this.validateTarget();

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna resultado da validação
      return Boolean( validationResult );
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData,
          matchFlow = m.GameMatch.current.flow,
          expireStage = matchFlow.period.getStageFrom( matchFlow.parity, committer.content.stats.attributes.current.power - 1 ),
          possessedCondition = new m.ConditionPossessed( { controller: committer.getRelationships().controller } );

      // Reduz energia de acionante
      committer.content.stats.attributes.reduce( target.content.stats.attributes.current.stamina * 2, 'energy', { triggeringCard: committer } );

      // Torna alvo possuído
      possessedCondition.apply( target, expireStage );

      // Adiciona evento para que condição de possuído seja removida de alvo caso acionante seja removido
      m.events.cardActivityEnd.removed.add( committer, possessedCondition.drop, { context: possessedCondition, once: true } );

      // Adiciona evento para remover evento de remoção da condição segundo atividade de acionante após fim do período do combate atual
      m.events.flowChangeEnd.finish.add( matchFlow.period, () =>
        m.events.cardActivityEnd.removed.remove( committer, possessedCondition.drop, { context: possessedCondition } ),
      { once: true } );
    }

    /// Limpa dados relativos à ação
    action.clearData = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: target } = eventData,
          actionOccupiedIndex = target?.readiness.relatedEffects.occupied.indexOf( this ) ?? -1;

      // Para caso ação esteja no arranjo de efeitos que tornam alvo ocupado
      if( actionOccupiedIndex != -1 ) {
        // Remove ação do arranjo de efeitos que tornam alvo ocupado
        target.readiness.relatedEffects.occupied.splice( actionOccupiedIndex, 1 );

        // Caso alvo ainda esteja ocupado, atualiza sua disponibilidade
        if( target.readiness.status == 'occupied' ) target.readiness.update();
      }
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionPossess.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-possess' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.Spirit, m.Demon ].some( constructor => card instanceof constructor ) );
      m.oAssert( !eventCategory || [ 'card-activity' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa
    if( card.activity == 'active' ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionPossess( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionPossess.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionPossess }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionPossess );
