// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionPrepare = function ( config = {} ) {
  // Iniciação de propriedades
  ActionPrepare.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.getAllCardAttachments().some( attachment => attachment instanceof m.ImplementHorn && attachment.isInUse ) );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionPrepare.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionPrepare );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-prepare';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'beingsToPrepare' ];

    /// Arranjo com jogadas a serem abertas por esta ação
    action.movesToOpen = [];

    /// Arranjo com entes a serem preparados por esta ação
    action.beingsToPrepare = [];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define entes a serem afetados pela ação
      this.setBeingsToPrepare();

      // Aciona ação
      return this.commit();
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { movesToOpen, beingsToPrepare } = this;

      // Abre jogadas capturadas pela ação
      movesToOpen.forEach( move => move.isOpen = true );

      // Atualiza disponibilidade de entes capturados pela ação
      beingsToPrepare.forEach( being => being.readiness.update() );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa arranjos com entes e jogadas afetáveis por esta ação
      for( let key of [ 'beingsToPrepare', 'movesToOpen' ] )
        while( this[ key ].length ) this[ key ].shift();
    }

    /// Retorna entes a serem afetados pela ação
    action.setBeingsToPrepare = function () {
      // Identificadores
      var { committer } = this,
          committerSize = committer.content.typeset.size,
          segmentsRange = committerSize == 'large' ? 3 : committerSize == 'medium' ? 2 : 1,
          matchFlow = m.GameMatch.current.flow,
          committerController = committer.getRelationships().controller;

      // Limpa arranjos com entes e jogadas afetáveis por esta ação
      for( let key of [ 'beingsToPrepare', 'movesToOpen' ] )
        while( this[ key ].length ) this[ key ].shift();

      // Itera por segmentos dentro da extensão do efeito desta ação
      for( let i = 0; i <= segmentsRange; i++ ) {
        // Identificadores
        let currentSegment = matchFlow.period.getStageFrom( matchFlow.segment, i );

        // Caso segmento alvo não exista, encerra iteração
        if( !currentSegment ) break;

        // Captura jogadas no segmento alvo
        let currentMoves = currentSegment.children.flatMap( parityStage => parityStage.children );

        // Itera por jogadas alvo
        for( let move of currentMoves ) {
          // Filtra jogadas que já estejam abertas
          if( move.isOpen ) continue;

          // Filtra jogadas que já tenham sido concluídas
          if( move.development >= 1 ) continue;

          // Filtra jogadas que não sejam de entes ativos
          if( move.source.activity != 'active' ) continue;

          // Filtra jogadas que não sejam de humanoides
          if( !( move.source instanceof m.Humanoid ) ) continue;

          // Filtra jogadas cujo controlador de seu ente seja diferente do controlador do acionante desta ação
          if( move.source.getRelationships().controller != committerController ) continue;

          // Adiciona jogada ao arranjo de jogadas a serem abertas
          this.movesToOpen.push( move );

          // Adiciona ente relativo à jogada ao arranjo de entes a serem preparados
          this.beingsToPrepare.push( move.source );
        }
      }

      // Retorna ação
      return this;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'partial';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionPrepare.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-prepare' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || [ 'card-use' ].includes( eventCategory ) );
    }

    // Para caso carta esteja com um berrante em uso
    if( card.getAllCardAttachments().some( attachment => attachment instanceof m.ImplementHorn && attachment.isInUse ) ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionPrepare( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionPrepare.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionPrepare }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionPrepare );
