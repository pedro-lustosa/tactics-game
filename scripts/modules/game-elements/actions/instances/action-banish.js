// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionBanish = function ( config = {} ) {
  // Iniciação de propriedades
  ActionBanish.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Angel );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionBanish.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionBanish );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-banish';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-card' );

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do tipo de carta
      if( ![ m.Demon, m.Spell ].some( constructor => target instanceof constructor ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-demon-or-spell' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação do uso
      if( !target.isInUse )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-in-use' ) );

      // Para magias, validação da polaridade
      if( target instanceof m.Spell && target.content.typeset.polarity != 'negative' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'if-spell-target-must-be-negative' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Finda alvo
      target.inactivate( 'ended', { triggeringCard: committer } );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionBanish.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-banish' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Angel );
      m.oAssert( !eventCategory || [ 'card-activity' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa
    if( card.activity == 'active' ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionBanish( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionBanish.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionBanish }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionBanish );
