// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionExpel = function ( config = {} ) {
  // Iniciação de propriedades
  ActionExpel.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( committer.content.stats.combativeness.channeling?.mana.current.metoth );
    m.oAssert( committer.slot.attachments.mana < committer.slot.maxAttachments.mana );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionExpel.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionExpel );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-expel';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Custo de fluxo
    action.flowCost = 1;

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer, target } = this,
          committerMana = committer.content.stats.combativeness.channeling.mana;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Se ainda não definido, define alvo da ação como casa em que acionante está
      target ??= this.target = committer.slot;

      // Aciona ação
      yield this.commit();

      // Enquanto ação puder ser sustentada, realiza sua sustentação
      while( true ) {
        // Remove 1 ponto de mana de metoth de acionante
        committerMana.reduce( 1, 'metoth' );

        // Vincula à casa alvo 1 ponto de mana
        target.attach( 1, 'mana' );

        // Caso acionante não tenha mais mana de Metoth ou caso seu alvo tenha atingido seu limite de mana vinculável, encerra operação
        if( !committerMana.current.metoth || target.attachments.mana >= target.maxAttachments.mana ) return;

        // Pausa operação
        yield;
      }
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer, target } = this,
          committerMana = committer.content.stats.combativeness.channeling.mana;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Invalida ação caso acionante não esteja mais em alvo
      if( committer.slot != target ) return false;

      // Invalida ação caso acionante não tenha mais mana de Metoth disponível
      if( !committerMana.current.metoth ) return false;

      // Invalida ação caso casa alvo tenha atingido seu limite de mana vinculável
      if( target.attachments.mana >= target.maxAttachments.mana ) return false;

      // Indica que ação foi revalidada com sucesso
      return true;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'partial';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionExpel.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-expel' ),
        cardMana = card.content.stats.combativeness.channeling?.mana,
        isInFieldGrid = card.slot instanceof m.FieldGridSlot;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( card.content.stats.combativeness.channeling?.paths.metoth.skill );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-content', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa, esteja em uma grade do campo, tenha mana de Metoth disponível e sua casa ainda possa ser vinculada a pontos de mana
    if( card.activity == 'active' && isInFieldGrid && cardMana.current.metoth && card.slot.attachments.mana < card.slot.maxAttachments.mana ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionExpel( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionExpel.checkAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: slot, eventCategory } = eventData,
        targetCards = ( slot.cards ?? [ slot.card ] ).filter( card =>
          card?.listeners( 'card-content-end-mana' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'attachability-change' );
      m.oAssert( slot instanceof m.FieldSlot );
    }

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionExpel.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionExpel }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionExpel );
