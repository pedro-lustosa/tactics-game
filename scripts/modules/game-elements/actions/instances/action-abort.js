// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionAbort = function ( config = {} ) {
  // Iniciação de propriedades
  ActionAbort.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.readiness.status == 'occupied' );
    m.oAssert( m.GameAction.currents.some( action => action.committer == committer ) );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionAbort.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionAbort );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-abort';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Ação a ser abortada
    action.actionToAbort = null;

    /// Executa operação da ação
    action.execute = function * () {
      // Captura ação a ser abortada
      this.actionToAbort = m.GameAction.currents.find( action => action.committer == this.committer );

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Aciona ação
      return this.commit();
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function () {
      // Aborta ação capturada
      this.actionToAbort.complete( 'abort' );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Nulifica ação a ser abortada
      this.actionToAbort = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionAbort.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-abort' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( card.activity == 'active' );
      m.oAssert( card.readiness.status == 'occupied' );
      m.oAssert( m.GameAction.currents.some( action => action.committer == card ) );
    }

    // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
    if( actionIndex == -1 ) card.actions.push( new ActionAbort( { committer: card } ) );
  }
}

/// Propriedades do protótipo
ActionAbort.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionAbort }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionAbort );
