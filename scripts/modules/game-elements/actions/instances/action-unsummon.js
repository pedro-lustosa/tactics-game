// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionUnsummon = function ( config = {} ) {
  // Iniciação de propriedades
  ActionUnsummon.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( committer.isToken );
    m.oAssert( !committer.isUncontrollable );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionUnsummon.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionUnsummon );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-unsummon';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.RedeploymentPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Aciona ação
      return this.commit();
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer } = eventData;

      // Alheia acionante
      committer.unlink();
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionUnsummon.controlAvailability = function ( eventData = {} ) {
    // Não executa função caso esse construtor esteja no arranjo de ações acionadas da carta alvo
    if( eventData.eventTarget.actions.singleCommitments.includes( ActionUnsummon ) ) return;

    // Identificadores
    var { eventTarget: card } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-unsummon' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( card.isToken );
      m.oAssert( !card.isUncontrollable );
      m.oAssert( card.activity == 'active' );
    }

    // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
    if( actionIndex == -1 ) card.actions.push( new ActionUnsummon( { committer: card } ) );
  }
}

/// Propriedades do protótipo
ActionUnsummon.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionUnsummon }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionUnsummon );
