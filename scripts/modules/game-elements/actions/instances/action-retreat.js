// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionRetreat = function ( config = {} ) {
  // Iniciação de propriedades
  ActionRetreat.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( !committer.isToken );
    m.oAssert( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) || committer instanceof m.Goblin );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );

  // Configurações pós-superconstrutor

  /// Para caso se esteja no período do combate
  if( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) {
    // Concede custo de fluxo à ação
    this.flowCost = 2;

    // Indica que ação deve ser animada no momento de seu acionamento
    this.isToAnimateAtCommit = true;
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionRetreat.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionRetreat );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-retreat';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod, m.RedeploymentPeriod ];

    /// Indica se ação deve ser animada no momento de seu acionamento
    action.isToAnimateAtCommit = false;

    /// Indica se ação deve ser animada no momento de sua progressão
    action.isToAnimateAtProgress = false;

    /// Identifica nome da superação
    action.superActionName = 'action-suspend';

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Aciona ação
      return this.commit();
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer } = this;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Invalida ação caso casa de acionante não seja mais controlada por seu jogador
      if( committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Indica que ação foi revalidada com sucesso
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer } = eventData;

      // Suspende acionante
      committer.inactivate( 'suspended', { triggeringCard: committer } );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionRetreat.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-retreat' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( !card.isToken );
      m.oAssert( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) || card instanceof m.Goblin );
      m.oAssert( !eventCategory || [ 'card-activity', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso não se esteja no período do combate e carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) && card.slot instanceof m.EngagementZone ) break controlValidation;

      // Captura dono da carta
      let cardOwner = card.getRelationships().owner;

      // Caso controlador da casa em que carta está seja diferente de seu dono, invalida habilitação da ação
      if( card.slot.getController() != cardOwner ) break controlValidation;

      // Caso carta seja um comandante e não haja uma magia 'O Regente' ativa para seu jogador, invalida habilitação da ação
      if( card.content.stats.command && !m.SpellTheRuler.playersWithRuler.includes( cardOwner ) ) break controlValidation;

      // Captura vinculados da carta
      let cardAttachments = card.getAllCardAttachments();

      // Caso carta esteja vinculada a 'Aperto da Terra', invalida habilitação da ação
      if( cardAttachments.some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) ) break controlValidation;

      // Para caso carta esteja vinculada a 'O Regente'
      if( cardAttachments.some( attachment => attachment instanceof m.SpellTheRuler && attachment.content.effects.isEnabled ) ) {
        // Captura comandante do dono da carta
        let playerCommander = cardOwner.gameDeck.cards.commander;

        // Caso comandante do dono da carta não esteja ativo, invalida habilitação da ação
        if( playerCommander.activity != 'active' ) break controlValidation;
      }

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionRetreat( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionRetreat.checkAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: slot, eventCategory } = eventData,
        targetCards = slot.cards.filter(
          card => card.listeners( 'field-slot-card-change-end-enter' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( slot instanceof m.EngagementZone );
    }

    // Apenas executa função caso período atual seja o do combate
    if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionRetreat.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionRetreat }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionRetreat );
