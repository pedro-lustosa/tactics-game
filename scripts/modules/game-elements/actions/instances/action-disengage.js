// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionDisengage = function ( config = {} ) {
  // Iniciação de propriedades
  ActionDisengage.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.PhysicalBeing );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.slot instanceof m.EngagementZone );
  }

  // Superconstrutor
  m.GameAction.call( this, config );

  // Configurações pós-superconstrutor

  /// Caso acionante seja um ogro, ajusta custo de fluxo da ação, segundo característica 'slowPace'
  if( this.committer instanceof m.Ogre ) this.flowCost = 4;
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionDisengage.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionDisengage );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-disengage';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Custo de fluxo
    action.flowCost = 2;

    /// Indica se ação deve ser animada no momento de sua progressão
    action.isToAnimateAtProgress = false;

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Quando ainda não definido, define alvo da ação como casa do jogador do acionante adjacente à zona de engajamento
      this.target ??= committer.slot.getAdjacentGridSlots( committer.getRelationships().owner.parity );

      // Aciona ação
      return this.commit();
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer, target } = this;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
        m.oAssert( committer.slot instanceof m.EngagementZone );
      }

      // Invalida ação caso seu alvo já esteja ocupado
      if( target.card ) return false;

      // Invalida ação caso zona de engajamento de acionante não seja mais controlada por seu jogador
      if( committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Indica que ação foi revalidada com sucesso
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Posiciona acionante em casa alvo
      target.environment.put( committer, target.name );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'partial';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionDisengage.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-disengage' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.PhysicalBeing );
      m.oAssert( !eventCategory || [ 'card-activity', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta não esteja em uma zona de engajamento, invalida habilitação da ação
      if( !( card.slot instanceof m.EngagementZone ) ) break controlValidation;

      // Captura dono da carta
      let cardOwner = card.getRelationships().owner;

      // Caso casa em que carta esteja não seja controlada por seu jogador, invalida habilitação da ação
      if( card.slot.getController() != cardOwner ) break controlValidation;

      // Caso casa adjacente à zona de engajamento da carta já esteja ocupada, invalida habilitação da ação
      if( card.slot.getAdjacentGridSlots( cardOwner.parity ).card ) break controlValidation;

      // Caso carta esteja vinculada a 'Aperto da Terra', invalida habilitação da ação
      if( card.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) )
        break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionDisengage( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionDisengage.checkAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: slot, eventCategory } = eventData,
        targetCards = ( slot instanceof m.EngagementZone ? slot.cards : slot.getColumnEngagementZone().cards ).filter( card =>
          card.listeners( 'field-slot-card-change-end-enter' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( slot instanceof m.FieldSlot );
    }

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionDisengage.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionDisengage }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionDisengage );
