// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionEquip = function ( config = {} ) {
  // Iniciação de propriedades
  ActionEquip.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionEquip.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionEquip );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-equip';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.AllocationStep, m.RedeploymentPeriod ];

    /// Identifica nome da superação
    action.superActionName = 'action-activate';

    /// Arranjo de vinculantes válidos
    action.validAttachers = [];

    /// Componente a ser o vinculante do equipamento mirado pela ação
    action.itemAttacher = null;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        itemAttacher: this.itemAttacher.getMatchId()
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { itemAttacher } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( itemAttacher && typeof itemAttacher == 'string' );

      // Atribui à ação vinculante de seu equipamento alvo
      this.itemAttacher = m.Card.getFromMatchId( itemAttacher );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso ação já tenha um alvo
        if( this.target ) break setTarget;

        // Interrompe execução da função para seleção de seu alvo
        this.target = yield this.selectTarget( 'choose-target-item' );

        // Para caso equipamento alvo só tenha um vinculante válido
        if( this.validAttachers.length == 1 ) {
          // Define vinculante do equipamento mirado pela ação como o único válido
          this.itemAttacher = this.validAttachers[ 0 ];

          // Encerra bloco
          break setTarget;
        }

        // Gera modal para seleção do vinculante do equipamento mirado pela ação, e aguarda essa seleção
        yield new m.SelectModal( {
          text: m.languages.notices.getModalText( 'select-item-attacher' ),
          inputsArray: this.validAttachers.map( function ( attacher ) {
            return {
              text: attacher.content.designation.title + ' (' +
                ( attacher.isEmbedded ?
                    m.languages.snippets.embedded :
                    m.languages.snippets.getSomethingIn( attacher.slot.name.slice( attacher.slot.name.indexOf( '-' ) + 1 ) )
                ) +
              ')',
              action: function () {
                // Define vinculante do equipamento mirado pela ação como o escolhido pelo jogador
                action.itemAttacher = attacher;

                // Prossegue com execução da ação
                return action.currentExecution.next();
              }
            };
          } )
        } ).insert();
      }

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ],
          formerIsQuiet = m.app.isQuiet;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Item ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-item' ) );

      // Validação do tipo de vinculação
      if( target.isEmbedded )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-external-item' ) );

      // Validação da atividade
      if( target.activity != 'suspended' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-suspended' ) );

      // Para caso alvo do equipamento seja um humanoide
      if( target.content.typeset.attachability == m.Humanoid ) {
        // Caso equipamento não possa ser vinculado ao acionante, invalida-o
        if( !m.Item.validateAttacher( target, this.committer ) ) return false;

        // Caso construtor do equipamento tenha um arranjo com humanoides afetados e caso acionante esteja nele, invalida-o
        if( Array.isArray( target.constructor.affectedBeings ) && target.constructor.affectedBeings.includes( this.committer ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'consumable-already-used-by-being' ) );

        // Adiciona o acionante ao arranjo de vinculantes válidos
        this.validAttachers.push( this.committer );

        // Indica que alvo é válido
        return true;
      }

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura vinculados que podem ser o vinculante do alvo passado
      this.validAttachers = this.validAttachers.concat( this.committer.getAllCardAttachments().filter(
        attachment => attachment instanceof m.Weapon && m.Item.validateAttacher( target, attachment )
      ) );

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Validação de vinculantes válidos para o alvo
      if( !this.validAttachers.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'no-valid-attachers' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData,
          { itemAttacher } = this;

      // Vincula equipamento ao vinculante escolhido
      target.activate( itemAttacher, { triggeringCard: committer } );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa arranjo de vinculantes válidos
      while( this.validAttachers.length ) this.validAttachers.shift();

      // Nulifica vinculante do equipamento mirado pela ação
      this.itemAttacher = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionEquip.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-equip' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || eventCategory == 'card-activity' );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( card.slot instanceof m.EngagementZone ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionEquip( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionEquip.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionEquip }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionEquip );
