// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionHandle = function ( config = {} ) {
  // Iniciação de propriedades
  ActionHandle.init( this );

  // Identificadores
  var { committer } = config,
      matchFlow = m.GameMatch.current?.flow;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.getAllCardAttachments().some( attachment => attachment instanceof m.Item && attachment.content.typeset.usage == 'dynamic' ) );
  }

  // Configurações pré-superconstrutor

  /// Em períodos do combate, ajusta tipo e subtipo de acionamento
  if( matchFlow?.period instanceof m.CombatPeriod ) [ this.commitment.type, this.commitment.subtype ] = [ 'dedicated', 'partial' ];

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionHandle.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionHandle );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-handle';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.AllocationStep, m.CombatPeriod, m.RedeploymentPeriod ];

    /// Arranjo de equipamentos válidos
    action.validItems = [];

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso ação já tenha um alvo
        if( this.target ) break setTarget;

        // Define arranjo de alvos válidos para a ação
        yield this.selectTarget( 'choose-target-item-or-committer' );

        // Para caso apenas um alvo tenha sido encontrado
        if( this.validItems.length == 1 ) {
          // Define equipamento encontrado como o alvo da ação
          this.target = this.validItems[ 0 ];

          // Encerra bloco
          break setTarget;
        }

        // Inseri modal de seleção do equipamento alvo da ação, e aguarda sua seleção
        this.target = yield new m.SelectModal( {
          text: m.languages.notices.getModalText( 'select-embedded-item' ),
          inputsArray: this.validItems.map( function ( item ) {
            return {
              text: item.content.designation.title,
              action: () => action.currentExecution.next( item )
            };
          } )
        } ).insert();
      }

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ];

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Item ) && this.committer != target )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-item-or-committer' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Para caso o alvo seja um equipamento
      if( target instanceof m.Item ) {
        // Validação do dono
        if( !this.committer.getAllCardAttachments( 'external' ).includes( target ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-owned-by-committer' ) );

        // Validação do tipo de uso
        if( target.content.typeset.usage != 'dynamic' )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-usage-must-be-dynamic' ) );

        // Validação do uso do vinculante
        if( !target.isInUse && !target.attacher.isInUse )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-attacher-must-be-in-use' ) );

        // Adiciona o equipamento ao arranjo de equipamentos válidos
        this.validItems.push( target );

        // Indica que alvo é válido
        return true;
      }

      // Captura equipamentos embutidos dinâmicos do acionante
      this.validItems = this.committer.getAllCardAttachments( 'embedded' ).filter( attachment =>
        attachment instanceof m.Item && attachment.content.typeset.usage == 'dynamic' && ( attachment.isInUse || attachment.attacher.isInUse )
      );

      // Validação de equipamentos válidos para a ação
      if( !this.validItems.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'no-valid-embedded-item' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: target } = eventData;

      // Alterna uso do alvo
      target.isInUse ? target.disuse() : target.use();
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa arranjo de equipamentos válidos
      while( this.validItems.length ) this.validItems.shift();
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionHandle.controlAvailability = function ( eventData = {} ) {
    // Não executa função caso esse construtor esteja no arranjo de ações acionadas da carta alvo
    if( eventData.eventTarget.actions.singleCommitments.includes( ActionHandle ) ) return;

    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-handle' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-content' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso não se esteja no período do combate e carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) && card.slot instanceof m.EngagementZone ) break controlValidation;

      // Caso carta não tenha nenhum equipamento dinâmico, invalida habilitação da ação
      if( !card.getAllCardAttachments().some( card => card instanceof m.Item && card.content.typeset.usage == 'dynamic' ) ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionHandle( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionHandle.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionHandle }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionHandle );
