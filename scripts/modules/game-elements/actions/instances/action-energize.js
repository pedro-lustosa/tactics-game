// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionEnergize = function ( config = {} ) {
  // Iniciação de propriedades
  ActionEnergize.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
    m.oAssert(
      committer.getAllCardAttachments().some( attachment => attachment instanceof m.ImplementEctoplasmUrn && attachment.isInUse && attachment.attachments.mana )
    );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionEnergize.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionEnergize );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-energize';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Fonte a transferir o mana
    action.manaGiver = null;

    /// Pontos de mana a serem transferidos para o alvo da ação
    action.manaToTransfer = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        manaGiver: this.manaGiver.getMatchId(),
        manaToTransfer: this.manaToTransfer
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { manaToTransfer, manaGiver: manaGiverId } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( manaToTransfer ) && manaToTransfer > 0 );
        m.oAssert( manaGiverId && typeof manaGiverId == 'string' );
      }

      // Atribui à ação pontos de mana a serem transferidos
      this.manaToTransfer = manaToTransfer;

      // Atribui à ação fonte a transferir o mana
      this.manaGiver = m.Card.getFromMatchId( manaGiverId );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-being' );

      // Se ainda inexistente, define fonte a transferir o mana
      this.manaGiver ??= this.committer.getAllCardAttachments().find(
        attachment => attachment instanceof m.ImplementEctoplasmUrn && attachment.isInUse && attachment.attachments.mana
      );

      // Define quantidade de mana a ser transferida
      setManaToTransfer: {
        // Não executa bloco caso quantidade de mana a ser transferida já tenha sido definida
        if( this.manaToTransfer ) break setManaToTransfer;

        // Gera modal para definição da quantidade de mana a ser transferida, e aguarda definição dessa quantidade
        yield new m.PromptModal( {
          name: 'mana-transfer-modal',
          text: m.languages.notices.getModalText( 'assign-mana-to-transfer' ),
          inputConfig: configManaTransferModal.bind( this ),
          action: assignMana.bind( this )
        } ).insert();
      }

      // Aciona ação
      return this.commit();

      // Funções

      /// Atribui à ação pontos de mana a serem transferidos
      function assignMana () {
        // Identificadores
        var promptModal = m.app.pixi.stage.children.find( child => child.name == 'mana-transfer-modal' ),
            manaTransferInput = promptModal?.content.inputsSet.textInput,
            { htmlInput } = manaTransferInput ?? {},
            manaToTransfer = Number( htmlInput.value );

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( 'value' in htmlInput );

        // Encerra função caso campo de inserção esteja inválido
        if( !manaTransferInput.isValid ) return false;

        // Captura pontos de mana a serem transferidos
        this.manaToTransfer = manaToTransfer;

        // Prossegue com execução da ação
        return this.currentExecution.next();
      }

      /// Configura mana a ser absorvido
      function configManaTransferModal( targetModal, targetInput ) {
        // Identificadores
        var { target: astralBeing, manaGiver } = this,
            { htmlInput } = targetInput,
            beingAttributes = astralBeing.content.stats.attributes,
            maxMana = Math.min( manaGiver.attachments.mana, beingAttributes.base.energy - beingAttributes.current.energy );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( targetModal instanceof m.PromptModal );
          m.oAssert( targetInput instanceof PIXI.TextInput );
          m.oAssert( targetInput.name == 'mana-transfer-modal-input' );
        }

        // Configura propriedades do campo de inserção

        /// Texto de aviso
        targetInput.placeholder = m.languages.notices.getModalText( 'assign-mana-placeholder', { manaPoints: maxMana } );

        /// Quantidade máxima de caracteres
        targetInput.maxLength = 2;

        // Evento para limitar caracteres usáveis para dígitos
        targetInput.addListener( 'input', () => htmlInput.value = htmlInput.value.replace( /\D/g, '' ) );

        // Evento para definir validade do campo de inserção
        targetInput.addListener( 'input', () => targetInput.isValid = htmlInput.value > 0 && htmlInput.value <= maxMana );
      }
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerController, targetOwner ] = [ this.committer.getRelationships().controller, target.getRelationships?.().owner ];

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerController != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-player' ) );

      // Validação do tipo de carta
      if( ![ m.Spirit, m.Demon ].some( constructor => target instanceof constructor ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-spirit-or-demon' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação da energia
      if( target.content.stats.attributes.base.energy <= target.content.stats.attributes.current.energy )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-energy-must-be-lower-than-base' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: astralBeing } = eventData,
          { manaGiver, manaToTransfer } = this;

      // Desvincula o mana da fonte de mana alvo
      manaGiver.detach( manaToTransfer, 'mana' );

      // Restaura energia do espírito alvo
      astralBeing.content.stats.attributes.restore( manaToTransfer, 'energy' );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Nulifica fonte a transferir o mana
      this.manaGiver = null;

      // Zera pontos de mana a serem transferidos para o alvo da ação
      this.manaToTransfer = 0;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'partial';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionEnergize.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-energize' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || [ 'card-use' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Captura eventual urna de ectoplasma vinculada à carta
      let ectoplasmUrn = card.getAllCardAttachments().find( attachment => attachment instanceof m.ImplementEctoplasmUrn );

      // Caso carta não tenha uma urna de ectoplasma em uso, invalida habilitação da ação
      if( !ectoplasmUrn?.isInUse ) break controlValidation;

      // Caso urna de ectoplasma não tenha mana lhe vinculado, invalida habilitação da ação
      if( !ectoplasmUrn.attachments.mana ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionEnergize( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionEnergize.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionEnergize }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionEnergize );
