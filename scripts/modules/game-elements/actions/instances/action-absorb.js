// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionAbsorb = function ( config = {} ) {
  // Iniciação de propriedades
  ActionAbsorb.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionAbsorb.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionAbsorb );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-absorb';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.RedeploymentPeriod ];

    /// Pontos de mana a serem absorvidos
    action.manaToAbsorb = 0;

    /// Destinatário do mana a ser absorvido
    action.manaReceiver = null;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        manaToAbsorb: this.manaToAbsorb,
        manaReceiver: this.manaReceiver.getMatchId()
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { manaToAbsorb, manaReceiver: manaReceiverId } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( manaToAbsorb ) && manaToAbsorb > 0 );
        m.oAssert( manaReceiverId && typeof manaReceiverId == 'string' );
      }

      // Atribui à ação pontos de mana a serem absorvidos
      this.manaToAbsorb = manaToAbsorb;

      // Atribui à ação destinatário do mana a ser absorvido
      this.manaReceiver = m.Card.getFromMatchId( manaReceiverId );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação como casa em que acionante está
      this.target ??= this.committer.slot;

      // Se ainda inexistente, define destinatário do mana a ser absorvido como equipamento pertinente em uso, ou, na ausência de um, o acionante
      this.manaReceiver ??= this.committer.getAllCardAttachments().find( attachment =>
        attachment.isInUse && [ m.ImplementRodOfAbsorption, m.ImplementEctoplasmUrn ].some(
          constructor => attachment instanceof constructor && attachment.maxAttachments.mana > attachment.attachments.mana
        )
      ) ?? this.committer;

      // Define quantidade de mana a ser absorvida
      setManaToAbsorb: {
        // Não executa bloco caso quantidade de mana a ser absorvida já tenha sido definida
        if( this.manaToAbsorb ) break setManaToAbsorb;

        // Gera modal para definição da quantidade de mana a ser absorvida, e aguarda definição dessa quantidade
        yield new m.PromptModal( {
          name: 'mana-absorption-modal',
          text: m.languages.notices.getModalText( 'assign-mana-to-absorb' ),
          inputConfig: configManaAbsorptionModal.bind( this ),
          action: assignMana.bind( this )
        } ).insert();
      }

      // Aciona ação
      return this.commit();

      // Funções

      /// Atribui à ação pontos de mana a serem absorvidos
      function assignMana () {
        // Identificadores
        var promptModal = m.app.pixi.stage.children.find( child => child.name == 'mana-absorption-modal' ),
            manaAbsorptionInput = promptModal?.content.inputsSet.textInput,
            { htmlInput } = manaAbsorptionInput ?? {},
            manaToAbsorb = Number( htmlInput.value );

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( 'value' in htmlInput );

        // Encerra função caso campo de inserção esteja inválido
        if( !manaAbsorptionInput.isValid ) return false;

        // Captura pontos de mana a serem absorvidos
        this.manaToAbsorb = manaToAbsorb;

        // Prossegue com execução da ação
        return this.currentExecution.next();
      }

      /// Configura mana a ser absorvido
      function configManaAbsorptionModal( targetModal, targetInput ) {
        // Identificadores
        var { committer, target, manaReceiver } = this,
            { htmlInput } = targetInput,
            committerMana = committer.content.stats.combativeness?.channeling?.mana,
            maxMana = Math.min( target.attachments.mana, manaReceiver == committer ?
              committerMana.base.metoth - committerMana.current.metoth : manaReceiver.maxAttachments.mana - manaReceiver.attachments.mana
            );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( targetModal instanceof m.PromptModal );
          m.oAssert( targetInput instanceof PIXI.TextInput );
          m.oAssert( targetInput.name == 'mana-absorption-modal-input' );
        }

        // Configura propriedades do campo de inserção

        /// Texto de aviso
        targetInput.placeholder = m.languages.notices.getModalText( 'assign-mana-placeholder', { manaPoints: maxMana } );

        /// Quantidade máxima de caracteres
        targetInput.maxLength = 2;

        // Evento para limitar caracteres usáveis para dígitos
        targetInput.addListener( 'input', () => htmlInput.value = htmlInput.value.replace( /\D/g, '' ) );

        // Evento para definir validade do campo de inserção
        targetInput.addListener( 'input', () => targetInput.isValid = htmlInput.value > 0 && htmlInput.value <= maxMana );
      }
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData,
          { manaToAbsorb, manaReceiver } = this;

      // Desvincula de casa alvo pontos de mana a serem absorvidos
      target.detach( manaToAbsorb, 'mana' );

      // Adiciona quantidade de mana desvinculada ao destinatário do mana a ser absorvido
      manaReceiver == committer ?
        committer.content.stats.combativeness.channeling.mana.restore( manaToAbsorb, 'metoth' ) : manaReceiver.attach( manaToAbsorb, 'mana' );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Zera pontos de mana a serem absorvidos
      this.manaToAbsorb = 0;

      // Nulifica destinatário do mana a ser absorvido
      this.manaReceiver = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionAbsorb.controlAvailability = function ( eventData = {} ) {
    // Não executa função caso esse construtor esteja no arranjo de ações acionadas da carta alvo
    if( eventData.eventTarget.actions.singleCommitments.includes( ActionAbsorb ) ) return;

    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-absorb' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-use', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( card.slot instanceof m.EngagementZone ) break controlValidation;

      // Caso casa em que carta esteja não tenha mana, invalida habilitação da ação
      if( !card.slot.attachments.mana ) break controlValidation;

      // Identificadores para prosseguir validação
      let cardMana = card.content.stats.combativeness?.channeling?.mana,
          inUseAbsorbItem = card.getAllCardAttachments().find( attachment =>
            attachment.isInUse && [ m.ImplementRodOfAbsorption, m.ImplementEctoplasmUrn ].some(
              constructor => attachment instanceof constructor && attachment.maxAttachments.mana > attachment.attachments.mana
            )
          );

      // Caso carta não possa restaurar mana de Metoth e não tenha um equipamento que absorva mana, invalida habilitação da ação
      if( ( !cardMana || cardMana.base.metoth <= cardMana.current.metoth ) && !inUseAbsorbItem ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionAbsorb( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionAbsorb.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionAbsorb }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionAbsorb );
