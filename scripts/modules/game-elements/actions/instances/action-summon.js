// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionSummon = function ( config = {} ) {
  // Iniciação de propriedades
  ActionSummon.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionSummon.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionSummon );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-summon';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.DeploymentStep, m.RedeploymentPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'target.card' ];

    /// Iteratividade
    action.iterativity = 'single';

    /// Ficha a ser convocada
    action.token = null;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        token: {
          constructorName: this.token.constructor.name,
          sourceBeing: this.token.sourceBeing ?? null
        }
      };
    }

    /// Determina alvos habilitados para a convocação
    action.getEnabledSummons = function () {
      // Identificadores
      var { committer } = this,
          committerOwner = committer.getRelationships().owner,
          enabledSummons = {},
          formerIsQuiet = m.app.isQuiet;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Validação de convocações
      validateSummons: {
        // Captura informações relativas à validação de 'Bigorna da Animação'
        let fieldGrid = m.GameMatch.current.environments.field.grids[ committerOwner.parity ],
            isUsingAnvil = committer.getAllCardAttachments().some( attachment => attachment.isInUse && attachment instanceof m.ImplementAnvilOfSouls );

        // Habilita golens caso relíquia 'Bigorna da Animação' esteja em uso e haja ao menos 1 tropa na grade de acionante
        if( isUsingAnvil && fieldGrid.cards.some( card => card instanceof m.Humanoid && card != committer ) )
          enabledSummons.golem = m.Golem;

        // Encerra validação caso todas as cartas da grade alvo do campo estejam ocupadas
        if( fieldGrid.slots.every( slot => slot.card ) ) break validateSummons;

        // Captura sendas de acionante
        let committerPaths = committer.content.stats.combativeness?.channeling?.paths;

        // Encerra validação caso acionante não seja um vothe
        if( !committerPaths?.voth.skill ) break validateSummons;

        // Habilita espíritos caso acionante tenha menos de 3
        if( !committer.summons.spirit || committer.summons.spirit.length < 3 )
          enabledSummons.spirit = m.Spirit;

        // Habilita tulpas caso acionante tenha menos de 3 e ao menos 2 pontos de mana em Metoth
        if( ( !committer.summons.tulpa || committer.summons.tulpa.length < 3 ) && committer.content.stats.combativeness.channeling.mana.current.metoth >= 2 )
          enabledSummons.tulpa = m.Tulpa;

        // Captura magias cujos efeitos estejam ativos e que tenham sido usadas pelo jogador do acionante
        let playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells ).filter(
              spell => spell.isInUse && spell.content.effects.isEnabled && spell.triggeringPlayer == committerOwner
            );

        // Habilita anjos caso magia 'Apelo Celestial' esteja presente e acionante atenda seus requisitos
        if( playerSpells.some( card => card instanceof m.SpellCelestialAppeal && card.content.effects.validateSummoner( committer ) ) )
          enabledSummons.angel = m.Angel;

        // Habilita demônios caso magia 'Evocação Abissal' esteja presente e acionante atenda seus requisitos
        if( playerSpells.some( card => card instanceof m.SpellAbyssalEvocation && card.content.effects.validateSummoner( committer ) ) )
          enabledSummons.demon = m.Demon;

        // Habilita salamandras douradas caso magia 'Animação Pirótica' esteja presente e acionante atenda seus requisitos
        if( playerSpells.some( card => card instanceof m.SpellPyroticAnimation && card.content.effects.validateSummoner( committer, 'golden' ) ) )
          enabledSummons.goldenSalamander = m.GoldenSalamander;

        // Habilita salamandras aniladas caso magia 'Animação Pirótica' esteja presente e acionante atenda seus requisitos
        if( playerSpells.some( card => card instanceof m.SpellPyroticAnimation && card.content.effects.validateSummoner( committer, 'indigo' ) ) )
          enabledSummons.indigoSalamander = m.IndigoSalamander;
      }

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna convocações válidas
      return enabledSummons;
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { token: tokenData } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( tokenData?.constructor == Object );
        m.oAssert( tokenData.constructorName && typeof tokenData.constructorName == 'string' );
      }

      // Identificadores pós-validação
      var { constructorName, sourceBeing } = tokenData,
          tokenConstructor = m.data.cards.creatures.find( constructor => constructor.name == constructorName );

      // Atribui à ação ficha relativa aos dados passados no objeto da ação
      this.token = new tokenConstructor( {
        summoner: this.committer,
        sourceBeing: sourceBeing
      } );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { token } = this;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( token instanceof m.Being );
        m.oAssert( token.isToken );
      }

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso ação já tenha um alvo
        if( this.target ) break setTarget;

        // Interrompe execução da função para seleção de seu alvo
        this.target = yield this.selectTarget( token instanceof m.Golem ? 'choose-target-for-golem' : 'choose-card-slot-for-token' );

        // Caso alvo escolhido seja uma carta, reajusta-o para a sua casa
        if( this.target instanceof m.Card ) this.target = this.target.slot;
      }

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var { token } = this,
          [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ];

      // Validação para caso ficha a ser convocada seja um golem
      if( token instanceof m.Golem ) {
        // Validação do componente
        if( target instanceof m.CardSlot )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

        // Validação do jogador
        if( committerOwner != targetOwner )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

        // Validação da raça
        if( !( target instanceof m.Humanoid ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-humanoid' ) );

        // Validação da atividade
        if( target.activity != 'active' )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

        // Validação da localização
        if( !( target.slot instanceof m.FieldGridSlot ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-in-player-grid' ) );

        // Validação da distinção de alvo
        if( this.committer == target )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-committer' ) );

        // Validação de comandante
        if( target.content.stats?.command )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );
      }
      // Validação para outras fichas a serem convocadas
      else {
        // Validação do componente
        if( target instanceof m.Card )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card-slot' ) );

        // Validação do tipo de casa
        if( !( target instanceof m.FieldGridSlot ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-field-grid-slot' ) );

        // Validação da ocupação
        if( target.card )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-unoccupied-card-slot' ) );

        // Validação do jogador
        if( committerOwner != target.owner )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-field-grid' ) );
      }

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData,
          { token } = this;

      // Caso ficha a ser convocada seja uma de golem, finda o ente na casa alvo
      if( token instanceof m.Golem ) target.card.inactivate( 'ended', { triggeringCard: committer } );

      // Ativa convocação em casa alvo
      committer.summon( token, target.name );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Nulifica ficha relativa à ação
      this.token = null;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionSummon.controlAvailability = function ( eventData = {} ) {
    // Não executa função caso esse construtor esteja no arranjo de ações acionadas da carta alvo
    if( eventData.eventTarget.actions.singleCommitments.includes( ActionSummon ) ) return;

    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-summon' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-use', 'condition-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( card.slot instanceof m.EngagementZone ) break controlValidation;

      // Identificadores para prosseguir validação
      let isVothe = Boolean( card.content.stats.combativeness.channeling?.paths.voth.skill ),
          isUsingAnvil = card.getAllCardAttachments().some( attachment => attachment.isInUse && attachment instanceof m.ImplementAnvilOfSouls );

      // Caso carta não seja um vothe e não esteja com 'Bigorna da Animação' em uso, invalida habilitação da ação
      if( !isVothe && !isUsingAnvil ) break controlValidation;

      // Captura marcadores de enfurecido da carta
      let enragedMarkers = card.conditions.find( condition => condition instanceof m.ConditionEnraged )?.markers ?? 0;

      // Caso carta esteja com 3 ou mais marcadores de enfurecido, invalida habilitação da ação
      if( enragedMarkers >= 3 ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionSummon( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionSummon.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionSummon }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionSummon );
