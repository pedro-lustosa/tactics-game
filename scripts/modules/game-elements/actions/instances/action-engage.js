// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionEngage = function ( config = {} ) {
  // Iniciação de propriedades
  ActionEngage.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.PhysicalBeing );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.slot instanceof m.EngagementZone || committer.slot.name.replace( /^.+-/, '' )[ 0 ] == 'B' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );

  // Configurações pós-superconstrutor

  /// Caso acionante seja um ogro, ajusta custo de fluxo da ação, segundo característica 'slowPace'
  if( this.committer instanceof m.Ogre ) this.flowCost = 4;

  // Eventos

  /// Para reverificar disponibilidade de ações 'interceptar' após término desta ação
  m.events.actionChangeEnd.complete.add( this, eventData => m.ActionIntercept.checkAvailability( eventData.actionCommitter ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionEngage.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionEngage );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-engage';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Custo de fluxo
    action.flowCost = 2;

    /// Indica se ação deve ser animada no momento de sua progressão
    action.isToAnimateAtProgress = false;

    /// Casa em que acionante está ao acionar a ação
    action.formerSlot = null;

    /// Indica pontos de destino requeridos pela condição 'temível' para escolha do alvo
    action.fearedRequiredFatePoints = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        fearedRequiredFatePoints: this.fearedRequiredFatePoints
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { fearedRequiredFatePoints } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( fearedRequiredFatePoints ) && fearedRequiredFatePoints >= 0 );

      // Define pontos de destino requeridos pela condição 'temível' para o acionamento da ação
      this.fearedRequiredFatePoints = fearedRequiredFatePoints;
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer, target: engagementZone } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Captura casa atual de acionante
      this.formerSlot = committer.slot;

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso o alvo já tenha sido definido
        if( engagementZone ) break setTarget;

        // Captura zonas de engajamento adjacentes a acionante
        let adjacentEngagementZones = committer.slot.getAdjacentSlots().filter( slot => slot instanceof m.EngagementZone );

        // Para caso exista apenas 1 zona de engajamento adjacente ao acionante
        if( adjacentEngagementZones.length == 1 ) {
          // Define o alvo como a única zona de engajamento possível
          engagementZone = this.target = adjacentEngagementZones[ 0 ];

          // Aplica à zona de engajamento alvo validação de 'temível', e cancela ação caso essa casa tenha sido invalidada
          if( !m.ConditionFeared.validateAction( this, engagementZone ) ) return this.cancel();
        }

        // Do contrário, aguarda seleção da zona de engajamento alvo
        else engagementZone = this.target = yield this.selectTarget( 'choose-target-card-slot' );
      }

      // Para caso pontos de destino devam ser gastos devido à condição 'temível'
      if( this.fearedRequiredFatePoints ) {
        // Identificadores
        let { controller: committerPlayer, opponent: committerOpponent } = committer.getRelationships(),
            isDecidingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == committerPlayer;

        // Caso jogador seja o que escolheu ação, aguarda confirmação sobre o pagamento de pontos de destino
        if( isDecidingPlayer )
          yield new m.ConfirmationModal( {
            text: m.languages.notices.getModalText( 'confirm-fate-point-spent-by-feared', {
              slot: engagementZone.cards[ committerOpponent.parity ].length ? engagementZone : engagementZone.getAdjacentGridSlots( committerOpponent.parity ),
              requiredPoints: this.fearedRequiredFatePoints
            } ),
            acceptAction: () => action.currentExecution.next(),
            declineAction: () => action.cancel()
          } ).insert();

        // Gasta pontos de destino requeridos para a escolha do alvo
        committerPlayer.fatePoints.decrease( committer.content.stats.fatePoints.spend( this.fearedRequiredFatePoints ) );
      }

      // Verifica disponibilidade de ações 'interceptar', em resposta a esta ação
      m.ActionIntercept.checkAvailability( committer );

      // Aciona ação
      yield this.commit();

      // Posiciona acionante em casa alvo
      engagementZone.environment.put( committer, engagementZone.name );

      // Força conclusão da ação
      this.complete( 'finish' );

      // Verifica se é possível realizar um ataque livre
      checkFreeAttack: {
        // Identificadores
        let oppositeParity = committer.getRelationships().owner.parity == 'odd' ? 'even' : 'odd',
            opponentBeing = engagementZone.getAdjacentGridSlots( oppositeParity ).card;

        // Encerra bloco caso ente adversário adjacente à nova zona de engajamento do acionante não exista ou não seja um ente físico
        if( !( opponentBeing instanceof m.PhysicalBeing ) ) break checkFreeAttack;

        // Define função a filtrar manobras inválidas para o ataque de oportunidade
        let getValidAttacks = ( attack ) => attack.range.replace( /\d/g, '' ) == 'R' && attack.range.replace( /\D/g, '' ) >= 1;

        // Define argumentos da modal de confirmação do ataque livre
        let modalArguments = [ 'confirm-free-attack-by-engage-action', {
              engager: committer, attacker: opponentBeing, slot: engagementZone
            } ];

        // Programa execução do ataque livre
        return m.ActionAttack.programFreeAttack( opponentBeing, {
          name: `engage-action-free-attack-${ opponentBeing.getMatchId() }`,
          defender: committer,
          getValidAttacks: getValidAttacks,
          finishAction: () => committer.slot?.pullBeings?.(),
          modalArguments: modalArguments
        } );
      }

      // Quando aplicável, desloca para casa alvo ente físico do oponente adjacente à zona de engajamento
      engagementZone.pullBeings();

      // Retorna ação
      return this;
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var committerOwner = this.committer.getRelationships().owner;

      // Validação do componente
      if( target instanceof m.Card )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card-slot' ) );

      // Validação do tipo de casa
      if( !( target instanceof m.EngagementZone ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-engagement-zone' ) );

      // Validação da localização
      if( !this.committer.slot.getAdjacentSlots().includes( target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-adjacent-engagement-zone' ) );

      // Validação do limite de cartas
      if( target.cards[ committerOwner.parity ].length >= m.EngagementZone.maxCards )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-player-occupants' ) );

      // Validação da condição 'temível'
      if( !m.ConditionFeared.validateAction( this, target ) ) return false;

      // Indica que alvo é válido
      return true;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer } = this,
          formerIsQuiet = m.app.isQuiet;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Invalida ação caso casa de acionante não seja mais controlada por seu jogador
      if( committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura resultado da validação do alvo da ação
      let validationResult = this.validateTarget();

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna resultado da validação
      return Boolean( validationResult );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Nulifica registro da casa em que acionante estava ao acionar ação
      this.formerSlot = null;

      // Redefine indicação de pontos de destino requeridos pela condição 'temível' para escolha do alvo
      this.fearedRequiredFatePoints = 0;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'partial';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionEngage.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-engage' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.PhysicalBeing );
      m.oAssert( !eventCategory || [ 'card-activity', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Identificadores para continuidade das validações
      let cardOwner = card.getRelationships().owner,
          isInEngagementZone = card.slot instanceof m.EngagementZone,
          isInVanguard = card.slot instanceof m.FieldGridSlot && card.slot.name.replace( /^.+-/, '' )[ 0 ] == 'B';

      // Caso carta não esteja na vanguarda e tampouco em uma zona de engajamento controlado por seu jogador, invalida habilitação da ação
      if( !isInVanguard && ( !isInEngagementZone || card.slot.getController() != cardOwner ) ) break controlValidation;

      // Captura alvos válidos para o acionamento da ação
      let validSlots = card.slot.getAdjacentSlots?.().filter( slot =>
            slot instanceof m.EngagementZone && slot.cards[ cardOwner.parity ].length < m.EngagementZone.maxCards
          );

      // Caso não haja alvos válidos para ação engajar, invalida habilitação da ação
      if( !validSlots?.length ) break controlValidation;

      // Caso carta esteja vinculada a 'Aperto da Terra', invalida habilitação da ação
      if( card.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) )
        break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionEngage( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionEngage.checkAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: slot, eventCategory } = eventData,
        targetCards = slot.cards.concat( slot.getAdjacentSlots().flatMap( slot => slot.card || slot.cards ) ).filter( card =>
          card?.listeners( 'field-slot-card-change-end-enter' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( slot instanceof m.EngagementZone );
    }

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionEngage.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionEngage }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionEngage );
