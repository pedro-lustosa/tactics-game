// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionIntercept = function ( config = {} ) {
  // Iniciação de propriedades
  ActionIntercept.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.PhysicalBeing );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( [ 'prepared', 'exhausted' ].includes( committer.readiness.status ) );
    m.oAssert( committer.slot.name.replace( /^.+-/, '' )[ 0 ] == 'B' );
    m.oAssert( m.GameAction.currents.some( action => action instanceof m.ActionEngage ) );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionIntercept.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionIntercept );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-intercept';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [];

    /// Arranjo de casas válidas para a ação
    action.validSlots = [];

    /// Ente alvo da interceptação
    action.interceptedBeing = null;

    /// Indica pontos de destino requeridos pela condição 'temível' para escolha do alvo
    action.fearedRequiredFatePoints = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        fearedRequiredFatePoints: this.fearedRequiredFatePoints
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { fearedRequiredFatePoints } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( fearedRequiredFatePoints ) && fearedRequiredFatePoints >= 0 );

      // Define pontos de destino requeridos pela condição 'temível' para o acionamento da ação
      this.fearedRequiredFatePoints = fearedRequiredFatePoints;
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Define alvo da ação
      setTarget: {
        // Não executa bloco caso o alvo já tenha sido definido
        if( this.target ) break setTarget;

        // Para caso exista apenas 1 zona de engajamento válida
        if( this.validSlots.length == 1 ) {
          // Define o alvo como a única zona de engajamento possível
          this.target = this.validSlots[ 0 ];

          // Aplica à zona de engajamento alvo validação de 'temível', e cancela ação caso essa casa tenha sido invalidada
          if( !m.ConditionFeared.validateAction( this, this.target ) ) return this.cancel();
        }

        // Do contrário, aguarda seleção da zona de engajamento alvo
        else this.target = yield this.selectTarget( 'choose-target-card-slot' );
      }

      // Para caso pontos de destino devam ser gastos devido à condição 'temível'
      if( this.fearedRequiredFatePoints ) {
        // Identificadores
        let { controller: committerPlayer, opponent: committerOpponent } = committer.getRelationships(),
            isDecidingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == committerPlayer;

        // Caso jogador seja o que escolheu ação, aguarda confirmação sobre o pagamento de pontos de destino
        if( isDecidingPlayer )
          yield new m.ConfirmationModal( {
            text: m.languages.notices.getModalText( 'confirm-fate-point-spent-by-feared', {
              slot: this.target.getAdjacentGridSlots( committerOpponent.parity ),
              requiredPoints: this.fearedRequiredFatePoints
            } ),
            acceptAction: () => action.currentExecution.next(),
            declineAction: () => action.cancel()
          } ).insert();

        // Gasta pontos de destino requeridos para a escolha do alvo
        committerPlayer.fatePoints.decrease( committer.content.stats.fatePoints.spend( this.fearedRequiredFatePoints ) );
      }

      // Define o ente alvo da interceptação como o do oponente engajando em alvo
      this.interceptedBeing = m.GameAction.currents.find( action =>
        action instanceof m.ActionEngage && action.target == this.target && action.committer.getRelationships().owner != committer.getRelationships().owner
      ).committer;

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var committerOwner = this.committer.getRelationships().owner;

      // Validação do componente
      if( target instanceof m.Card )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card-slot' ) );

      // Validação do tipo de casa
      if( !( target instanceof m.EngagementZone ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-engagement-zone' ) );

      // Validação da ocupação
      if( target.cards.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-empty-engagement-zone' ) );

      // Validação da inclusão em arranjo de alvos válidos
      if( !this.validSlots.includes( target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'invalid-target' ) );

      // Validação da condição 'temível'
      if( !m.ConditionFeared.validateAction( this, target ) ) return false;

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Posiciona acionante em casa alvo
      target.environment.put( committer, target.name );

      // Progride para conclusão da ação engajar do ente interceptado
      m.GameAction.currents.find( action => action instanceof m.ActionEngage && action.committer == this.interceptedBeing ).currentExecution.next();
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Limpa arranjo de casas válidas
      while( this.validSlots.length ) this.validSlots.shift();

      // Nulifica ente interceptado
      this.interceptedBeing = null;

      // Redefine indicação de pontos de destino requeridos pela condição 'temível' para escolha do alvo
      this.fearedRequiredFatePoints = 0;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'free';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionIntercept.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-intercept' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.PhysicalBeing );
      m.oAssert( [ 'prepared', 'exhausted' ].includes( card.readiness.status ) );
      m.oAssert( !eventCategory || [ 'card-activity', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Identifica se carta está na vanguarda
      let isInVanguard = card.slot instanceof m.FieldGridSlot && card.slot.name.replace( /^.+-/, '' )[ 0 ] == 'B';

      // Caso carta não esteja na vanguarda, invalida habilitação da ação
      if( !isInVanguard ) break controlValidation;

      // Identificadores relativos a ações 'engajar' em andamento
      let cardOwner = card.getRelationships().owner,
          engageActions = m.GameAction.currents.filter( action => action instanceof m.ActionEngage ),
          enemyEngagers = engageActions.map( action => action.committer ).filter( being => being.getRelationships().owner != cardOwner ),
          emptySlots = engageActions.filter( action => enemyEngagers.includes(action.committer) && !action.target.cards.length ).map( action => action.target );

      // Caso não haja zonas de engajamento vazias miradas pela ação 'engajar' de um ente adversário, invalida habilitação da ação
      if( !emptySlots.length ) break controlValidation;

      // Identificadores relativos às casas válidas para habilitação da ação
      let field = card.slot.environment,
          topParity = field.grids.odd.y < field.grids.even.y ? 'odd' : 'even',
          rowIndex = cardOwner.parity == topParity ? 1 : -1,
          adjacentSlots = [ card.slot.getSlotAt( rowIndex, -1 ), card.slot.getSlotAt( rowIndex, 1 ) ].filter( slot => slot ),
          adjacentTargetSlots = adjacentSlots.filter( slot => emptySlots.includes( slot ) );

      // Caso não haja casas válidas para ação 'interceptar', invalida habilitação da ação
      if( !adjacentTargetSlots.length ) break controlValidation;

      // Caso carta esteja vinculada a 'Aperto da Terra', invalida habilitação da ação
      if( card.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) )
        break controlValidation;

      // Gera e captura ação 'interceptar' de carta, ou apenas a captura caso já exista
      let interceptAction = actionIndex == -1 ? new ActionIntercept( { committer: card } ) : card.actions[ actionIndex ];

      // Registra casas válidas para a ação
      for( let slot of adjacentTargetSlots )
        if( !interceptAction.validSlots.includes( slot ) ) interceptAction.validSlots.push( slot );

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( interceptAction );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionIntercept.checkAvailability = function ( engager ) {
    // Identificadores
    var engagerParity = engager.getRelationships().owner.parity,
        targetSkip = engagerParity == 'odd' ? 'skipOdd' : 'skipEven',
        targetCards = m.GameMatch.current.decks.getAllCards( { [ targetSkip ]: true, skipAssets: true, skipSideDeck: true } ).filter(
          being => being.listeners( 'field-slot-card-change-end-enter' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( engager instanceof m.PhysicalBeing );

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionIntercept.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionIntercept }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionIntercept );
