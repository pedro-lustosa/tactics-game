// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionUnequip = function ( config = {} ) {
  // Iniciação de propriedades
  ActionUnequip.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Humanoid );
    m.oAssert( !committer.isToken );
    m.oAssert( committer.activity == 'active' );
    m.oAssert( committer.getAllCardAttachments( 'external' ).some( attachment => attachment instanceof m.Item ) );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionUnequip.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionUnequip );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-unequip';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.RedeploymentPeriod ];

    /// Identifica nome da superação
    action.superActionName = 'action-suspend';

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-item' );

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ];

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Item ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-item' ) );

      // Validação do tipo de vinculação
      if( target.isEmbedded )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-external-item' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação do dono
      if( !this.committer.getAllCardAttachments( 'external' ).includes( target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-owned-by-committer' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Suspende equipamento alvo
      target.inactivate( 'suspended', { triggeringCard: committer } );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionUnequip.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-unequip' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Humanoid );
      m.oAssert( !card.isToken );
      m.oAssert( !eventCategory || eventCategory == 'card-content' );
    }

    // Bloco para melhor separar em etapas validação da habilitação da ação
    controlValidation: {
      // Caso carta não esteja ativa, invalida habilitação da ação
      if( card.activity != 'active' ) break controlValidation;

      // Caso carta esteja em uma zona de engajamento, invalida habilitação da ação
      if( card.slot instanceof m.EngagementZone ) break controlValidation;

      // Caso carta não tenha nenhum equipamento externo, invalida habilitação da ação
      if( !card.getAllCardAttachments( 'external' ).some( attachment => attachment instanceof m.Item ) ) break controlValidation;

      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionUnequip( { committer: card } ) );

      // Encerra operação
      return;
    }

    // Remove ação, caso ela exista no arranjo de ações da carta
    if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
  }
}

/// Propriedades do protótipo
ActionUnequip.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionUnequip }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionUnequip );
