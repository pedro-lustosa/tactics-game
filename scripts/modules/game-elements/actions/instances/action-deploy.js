// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionDeploy = function ( config = {} ) {
  // Iniciação de propriedades
  ActionDeploy.init( this );

  // Identificadores
  var { committer } = config,
      matchFlow = m.GameMatch.current?.flow;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( !committer.isToken );
    m.oAssert( !( matchFlow?.period instanceof m.CombatPeriod ) || committer instanceof m.Goblin );
    m.oAssert( committer.activity == 'suspended' );
  }

  // Configurações pré-superconstrutor

  /// Em períodos do combate, ajusta subtipo de acionamento para parcial
  if( matchFlow?.period instanceof m.CombatPeriod ) this.commitment.subtype = 'partial';

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionDeploy.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionDeploy );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-deploy';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.DeploymentStep, m.CombatPeriod, m.RedeploymentPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [];

    /// Identifica nome da superação
    action.superActionName = 'action-activate';

    /// Indica pontos de destino requeridos pela condição 'temível' para escolha do alvo
    action.fearedRequiredFatePoints = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Retorna objeto a representar ação
      return {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        fearedRequiredFatePoints: this.fearedRequiredFatePoints
      };
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { fearedRequiredFatePoints } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( fearedRequiredFatePoints ) && fearedRequiredFatePoints >= 0 );

      // Define pontos de destino requeridos pela condição 'temível' para o acionamento da ação
      this.fearedRequiredFatePoints = fearedRequiredFatePoints;
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-card-slot' );

      // Para caso pontos de destino devam ser gastos devido à condição 'temível'
      if( this.fearedRequiredFatePoints ) {
        // Identificadores
        let { committer } = this,
            committerPlayer = committer.getRelationships().controller,
            isDecidingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == committerPlayer;

        // Caso jogador seja o que escolheu ação, aguarda confirmação sobre o pagamento de pontos de destino
        if( isDecidingPlayer )
          yield new m.ConfirmationModal( {
            text: m.languages.notices.getModalText( 'confirm-fate-point-spent-by-feared', {
              slot: this.target,
              requiredPoints: this.fearedRequiredFatePoints
            } ),
            acceptAction: () => action.currentExecution.next(),
            declineAction: () => action.cancel()
          } ).insert();

        // Gasta pontos de destino requeridos para a escolha do alvo
        committerPlayer.fatePoints.decrease( committer.content.stats.fatePoints.spend( this.fearedRequiredFatePoints ) );
      }

      // Aciona ação
      return this.commit();
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var committerOwner = this.committer.getRelationships().owner;

      // Validação do componente
      if( target instanceof m.Card )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card-slot' ) );

      // Para caso se esteja no período do combate
      if( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) {
        // Validação do tipo de casa
        if( !( target instanceof m.EngagementZone ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-engagement-zone' ) );

        // Validação da ocupação
        if( !target.cards.length )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-occupied-engagement-zone' ) );

        // Validação do limite de cartas
        if( target.cards[ committerOwner.parity ].length >= m.EngagementZone.maxCards )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-player-occupants' ) );

        // Validação da condição 'temível'
        if( !m.ConditionFeared.validateAction( action, target ) ) return false;
      }

      // Para caso não se esteja no período do combate
      else {
        // Validação para zonas de engajamento
        validateEngagementZones: {
          // Encerra bloco caso alvo não seja uma zona de engajamento
          if( !( target instanceof m.EngagementZone ) ) break validateEngagementZones;

          // Captura dados de uso da magia 'O Engodo' em relação ao jogador do acionante
          let theLureUsageData = m.SpellTheLure.triggeringPlayers.find( usageData => usageData.player == committerOwner );

          // Encerra bloco caso jogador de acionante não tenha usado uma magia 'O Engodo' no turno atual
          if( !theLureUsageData?.wasOnThisTurn ) break validateEngagementZones;

          // Validação da raça
          if( this.committer instanceof m.AstralBeing )
            return m.noticeBar.show( m.languages.notices.getInvalidationText( 'engagement-zone-target-must-not-be-with-astral-being' ) );

          // Validação da ocupação
          if( !target.cards.length )
            return m.noticeBar.show( m.languages.notices.getInvalidationText( 'engagement-zone-target-must-be-occupied' ) );

          // Validação do limite de cartas
          if( target.cards[ committerOwner.parity ].length >= m.EngagementZone.maxCards )
            return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-player-occupants' ) );

          // Validação da condição 'temível'
          if( !m.ConditionFeared.validateAction( action, target ) ) return false;

          // Indica que alvo é válido
          return true;
        }

        // Validação do tipo de casa
        if( !( target instanceof m.FieldGridSlot ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-field-grid-slot' ) );

        // Validação da ocupação
        if( target.card )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-unoccupied-card-slot' ) );

        // Validação do jogador
        if( committerOwner != target.owner )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-field-grid' ) );
      }

      // Indica que alvo é válido
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionCommitter: committer, actionTarget: target } = eventData;

      // Ativa acionante em casa alvo
      committer.activate( target.name, { triggeringCard: committer } );
    }

    /// Limpa dados relativos à ação
    action.clearData = function () {
      // Redefine indicação de pontos de destino requeridos pela condição 'temível' para escolha do alvo
      this.fearedRequiredFatePoints = 0;
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionDeploy.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-deploy' );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( !card.isToken );
      m.oAssert( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) || card instanceof m.Goblin );
      m.oAssert( !eventCategory || eventCategory == 'card-activity' );
    }

    // Para caso carta esteja suspensa
    if( card.activity == 'suspended' ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionDeploy( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionDeploy.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionDeploy }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionDeploy );
