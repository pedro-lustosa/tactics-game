// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionChannel = function ( config = {} ) {
  // Iniciação de propriedades
  ActionChannel.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( committer instanceof m.Being );
    m.oAssert( committer.content.stats.combativeness.channeling );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionChannel.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionChannel );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-channel';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.AllocationStep, m.CombatPeriod ];

    /// Componentes a serem animados pela ação
    action.animationTargets = [ 'committer', 'manaToAssign', 'target', 'target.target', 'target.extraTargets', 'adjunctSpells' ];

    /// Arranjo de magias adjuntas
    action.adjunctSpells = [];

    /// Mana a ser atribuído para a canalização da magia
    action.manaToAssign = 0;

    /// Mana a ser atribuído para a canalização das magias adjuntas
    action.adjunctManaToAssign = 0;

    /// Converte ação em um objeto literal
    action.toObject = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.committer );

      // Identificadores
      var actionObject = {
        constructorName: this.constructor.name,
        committer: this.committer.getMatchId(),
        target: this.target.getMatchId(),
        spellTarget: this.target.target?.getMatchId() ?? null,
        spellExtraTargets: this.target.extraTargets.map( target => target.getMatchId() ),
        adjunctSpells: this.adjunctSpells.map( adjunctSpell => adjunctSpell.getMatchId() ),
        manaToAssign: this.manaToAssign,
        adjunctManaToAssign: this.adjunctManaToAssign
      };

      // Caso magia alvo da ação tenha dados próprios a serem passados, passa-os
      if( this.target.addToActionObject ) Object.assign( actionObject, this.target.addToActionObject( this ) );

      // Retorna objeto a representar ação
      return actionObject;
    }

    /// Prepara ação propagada para ser executada
    action.prepareExecution = function ( actionObject = {} ) {
      // Identificadores pré-validação
      var { spellTarget: spellTargetId, spellExtraTargets, adjunctSpells } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !spellTargetId || typeof spellTargetId == 'string' );
        m.oAssert( [ spellExtraTargets, adjunctSpells ].every( value => Array.isArray( value ) && value.every( id => id && typeof id == 'string' ) ) );
        m.oAssert( [ 'manaToAssign', 'adjunctManaToAssign' ].every( key => Number.isInteger( actionObject[ key ] ) && actionObject[ key ] >= 0 ) );
      }

      // Quando existente, atribui à ação o alvo de sua magia
      if( spellTargetId ) this.target.target = m.data.getFromMatchId( spellTargetId );

      // Adiciona ao arranjo de alvos adicionais da magia seus eventuais alvos adicionais
      for( let extraTargetId of spellExtraTargets ) this.target.extraTargets.push( m.data.getFromMatchId( extraTargetId ) );

      // Atribui à ação mana a ser gasto via ela
      for( let key of [ 'manaToAssign', 'adjunctManaToAssign' ] ) this[ key ] = actionObject[ key ];

      // Para caso haja magias adjuntas
      for( let adjunctSpellId of adjunctSpells ) {
        // Identificadores
        let adjunctSpell = m.Card.getFromMatchId( adjunctSpellId );

        // Adiciona magia adjunta ao arranjo de magias adjuntas da ação
        this.adjunctSpells.push( adjunctSpell );

        // Quando existentes, adiciona à ação configurações oriundas da magia adjunta
        adjunctSpell.configChannelAction?.( this );

        // Quando aplicável, aplica modificação da magia adjunta
        adjunctSpell.content.effects.applyModifier?.( this.target );
      }

      // Caso magia alvo da ação tenha dados próprios a serem registrados, registra-os
      this.target.prepareActionExecution?.( action, actionObject );
    }

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer } = this;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Se ainda inexistente, define alvo da ação
      let targetSpell = this.target ??= yield this.selectTarget( 'choose-target-spell' );

      // Quando existentes, adiciona à ação configurações oriundas da magia
      targetSpell.configChannelAction?.( this );

      // Determinação do alvo da magia
      setSpellTarget: {
        // Para caso alvo da magia já tenha sido definido, ou para caso ela não tenha um alvo
        if( targetSpell.target || !targetSpell.content.typeset.attachability ) {
          // Quando aplicável, aplica à magia efeitos do traje 'Manto dos Arcanistas'
          m.GarmentRobeOfTheMagi.checkEffectApplication( this, targetSpell );

          // Encerra bloco
          break setSpellTarget;
        }

        // Para enquanto alvo da ação for uma magia adjunta
        while( targetSpell.isAdjunct ) {
          // Adiciona alvo da ação ao arranjo de magias adjuntas da ação
          this.adjunctSpells.push( targetSpell );

          // Redefine magia alvo da ação, enquanto o alvo da magia adjunta
          targetSpell = this.target = yield this.selectTarget( 'choose-target-for-spell-chain' );

          // Quando existentes, adiciona à ação configurações oriundas da magia
          targetSpell.configChannelAction?.( this );
        }

        // Aplica eventuais modificadores de magias adjuntas ao alvo da ação
        this.adjunctSpells.forEach( adjunctSpell => adjunctSpell.content.effects.applyModifier?.( targetSpell ) );

        // Quando aplicável, aplica à magia efeitos do traje 'Manto dos Arcanistas'
        m.GarmentRobeOfTheMagi.checkEffectApplication( this, targetSpell );

        // Encerra bloco caso magia mirada pela ação não tenha um alvo
        if( !targetSpell.content.typeset.attachability ) break setSpellTarget;

        // Para caso magia tenha função própria para definir alvo
        if( targetSpell.content.effects.setChannelingTarget ) {
          // Identificadores
          let spellEffects = targetSpell.content.effects,
              executionResult = yield setTimeout( () => spellEffects.setChannelingTarget( action ) );

          // Enquanto efeito da magia estiver em execução, atualiza-o sobre resultados que chegarem a esta função
          while( spellEffects.currentExecution && !spellEffects.currentExecution.next( executionResult ).done ) executionResult = yield;

          // Caso alvo da magia não tenha sido definido dentro de sua função de definição própria, define-o como igual ao último resultado dessa função
          targetSpell.target ??= executionResult;
        }

        // Do contrário, executa definição padrão do alvo
        else targetSpell.target = yield this.selectTarget( 'choose-target-for-spell' );
      }

      // Determinação do custo de fluxo
      setFlowCost: {
        // Identificadores
        let spellFlowCost = targetSpell.content.stats.current.flowCost,
            conditionModifiers = committer.conditions.map( condition => condition.getFlowCostModifier?.() ).filter( modifier => modifier ),
            finalModifier = conditionModifiers.reduce( ( accumulator, current ) => accumulator += current, 0 );

        // Para caso haja um modificador de custo de fluxo a ser adicionado
        if( finalModifier ) {
          // Sinaliza início da mudança de custo de fluxo
          m.events.cardContentStart.flowCost.emit( targetSpell, { changeNumber: finalModifier } );

          // Adiciona modificador ao custo de fluxo atual da magia
          spellFlowCost.toChannel = Math.max( spellFlowCost.toChannel + finalModifier, 0 );

          // Sinaliza fim da mudança de custo de fluxo
          m.events.cardContentEnd.flowCost.emit( targetSpell, { changeNumber: finalModifier } );
        }

        // Define custo de fluxo da ação como igual ao do para canalização da magia
        this.flowCost = spellFlowCost.toChannel;
      }

      // Determinação do custo de mana para canalização
      setManaCost: {
        // Não executa bloco caso mana a ser atribuído para a magia já tenha sido definido
        if( this.manaToAssign ) break setManaCost;

        // Não executa bloco caso magia não tenha custo de mana para canalização
        if( !targetSpell.content.stats.current.manaCost.toChannel.min ) break setManaCost;

        // Caso magia tenha função própria para definir custo de mana, executa essa função e aguarda sua resolução
        if( targetSpell.content.effects.setManaToChannel ) this.manaToAssign = yield setTimeout( () => targetSpell.content.effects.setManaToChannel( action ) );

        // Para caso custo de mana ainda não tenha sido definido
        if( !this.manaToAssign ) {
          // Identificadores
          let assignableMana = {
                min: this.getMinAssignableMana(), max: this.getMaxAssignableMana()
              };

          // Define custo de mana da magia, imediatamente caso [seja fixo/canalizador apenas possa pagar custo mínimo] ou, do contrário, via uma modal
          assignableMana.min == assignableMana.max ?
            this.manaToAssign = assignableMana.min :
            yield new m.PromptModal( {
              name: 'mana-cost-modal',
              text: m.languages.notices.getModalText( 'assign-mana-cost', {
                polarityBurden: assignableMana.max != targetSpell.applyPolarityBurden( assignableMana.max, committer ),
                isEnergy: committer instanceof m.AstralBeing
              } ),
              inputConfig: configManaCostModal.bind( this, assignableMana ),
              action: assignMana.bind( this )
            } ).insert();
        }

        // Captura mana a ser gasto após aplicação do ônus de polaridade
        let manaToSpend = targetSpell.applyPolarityBurden( this.manaToAssign, committer );

        // Caso mana a ser atribuído seja modificável pelo ônus de polaridade, gera modal explicitando valor final do mana a ser gasto, e confirmando operação
        if( this.manaToAssign != manaToSpend )
          yield new m.ConfirmationModal( {
            text: m.languages.notices.getModalText( 'confirm-mana-cost-after-polarity-burden', {
              manaPoints: manaToSpend,
              isEnergy: committer instanceof m.AstralBeing
            } ),
            acceptAction: () => action.currentExecution.next(),
            declineAction: () => action.cancel()
          } ).insert();

        // Captura mana a ser gasto por magias adjuntas
        this.adjunctManaToAssign = this.getAdjunctSpellsMana();

        // Caso haja mana a ser gasto por magias adjuntas, gera modal indicando seu valor, e confirmando operação
        if( this.adjunctManaToAssign )
          yield new m.ConfirmationModal( {
            text: m.languages.notices.getModalText( 'confirm-mana-cost-with-adjunct-spells', {
              manaPoints: this.adjunctManaToAssign,
              adjunctSpells: this.adjunctSpells,
              isEnergy: committer instanceof m.AstralBeing
            } ),
            acceptAction: () => action.currentExecution.next(),
            declineAction: () => action.cancel()
          } ).insert();
      }

      // Ativa magias que ainda não estiverem ativas
      [ targetSpell ].concat( this.adjunctSpells ).forEach( spell => spell.activate( committer, { triggeringCard: committer } ) );

      // Aciona ação
      return this.commit();

      // Funções

      /// Atribui o mana para canalização da magia
      function assignMana () {
        // Identificadores
        var promptModal = m.app.pixi.stage.children.find( child => child.name == 'mana-cost-modal' ),
            manaCostInput = promptModal?.content.inputsSet.textInput,
            { htmlInput } = manaCostInput ?? {};

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( 'value' in htmlInput );

        // Encerra função caso campo de inserção esteja inválido
        if( !manaCostInput.isValid ) return false;

        // Atribuição do mana
        this.manaToAssign = Number( htmlInput.value );

        // Prossegue com execução da ação
        return this.currentExecution.next();
      }

      /// Configura custo de mana
      function configManaCostModal( assignableMana, targetModal, targetInput ) {
        // Identificadores
        var { htmlInput } = targetInput,
            isPersistentSpell = this.target.content.typeset.duration == 'persistent',
            { manaCost } = this.target.content.stats.current;

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( targetModal instanceof m.PromptModal );
          m.oAssert( targetInput instanceof PIXI.TextInput );
          m.oAssert( targetInput.name == 'mana-cost-modal-input' );
          m.oAssert( !isPersistentSpell || manaCost.toPersist );
        }

        // Configura propriedades do campo de inserção

        /// Texto de aviso
        targetInput.placeholder = m.languages.notices.getModalText( 'assign-mana-cost-placeholder', {
          isPersistent: isPersistentSpell,
          minMana: assignableMana.min,
          maxMana: assignableMana.max
        } );

        /// Quantidade máxima de caracteres
        targetInput.maxLength = 2;

        // Eventos

        /// Para limitar caracteres usáveis para dígitos
        targetInput.addListener( 'input', () => htmlInput.value = htmlInput.value.replace( /\D/g, '' ) );

        /// Para definir validade do campo de inserção
        targetInput.addListener( 'input', () => targetInput.isValid =
          htmlInput.value >= assignableMana.min && htmlInput.value <= assignableMana.max &&
          ( !isPersistentSpell || htmlInput.value % manaCost.toPersist == 0 )
        );
      }
    }

    /// Valida alvos da ação
    action.validateTarget = function ( target ) {
      // Delega validação em função do alvo a ser definido
      return !this.target ? this.validateSpell( target ) : m.Spell.validateTarget( this, target );
    }

    /// Valida magia da ação
    action.validateSpell = function ( target = this.target ) {
      // Identificadores
      var [ committerOwner, targetOwner ] = [ this.committer.getRelationships().owner, target.getRelationships?.().owner ],
          channelerPaths = this.committer.content.stats.combativeness.channeling.paths;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação do jogador
      if( committerOwner != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-committer-deck' ) );

      // Validação do tipo primitivo
      if( !( target instanceof m.Spell ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-spell' ) );

      // Validação [da atividade/do dono]
      if( target.activity != 'suspended' && target.owner != this.committer )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-spell-suspended-or-owned' ) );

      // Validação do uso
      if( target.isInUse )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-in-disuse' ) );

      // Validação da senda
      if( !channelerPaths[ target.content.typeset.path ].skill )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-in-channeler-paths' ) );

      // Para caso etapa atual seja a da alocação
      if( m.GameMatch.current.flow.step instanceof m.AllocationStep ) {
        // Validação do tipo de magia
        if( target.content.typeset.duration != 'persistent' && !target.isAdjunct )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'allocation-step-spell-targets' ) );
      }

      // Para magias com custo de mana para canalização, validação do custo de mana
      if( target.content.stats.current.manaCost.toChannel.min && !m.Spell.validateManaCost( this, target ) ) return false;

      // Para magias com custo de fluxo para canalização
      if( target.content.stats.current.flowCost.toChannel ) {
        // Identificadores
        let spellFlowCost = target.content.stats.current.flowCost;

        // Validação do custo de fluxo de canalização máximo
        if( spellFlowCost.toChannel > spellFlowCost.maxChanneling )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-over-max-channeling-flow-cost' ) );

        // Para ações ainda não acionadas, validação padrão do custo de fluxo de ações
        if( !this.isCommitted && !m.GameAction.validateFlowCost( this.committer, spellFlowCost.toChannel ) ) return false;
      }

      // Validação do dono da magia segundo seu efeito, se aplicável
      if( target.content.effects.validateOwner && !target.content.effects.validateOwner( this.committer, this ) ) return false;

      // Indica que alvo é válido
      return true;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer, target: targetSpell, adjunctSpells } = this,
          spellTargets = [ targetSpell.target, ...targetSpell.extraTargets ].filter( target => target );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Invalida ação caso magia alvo não esteja ativa
      if( targetSpell.activity != 'active' ) return false;

      // Invalida ação caso acionante não esteja mais em uma casa controlada por seu jogador
      if( committer.slot.getController() != committer.getRelationships().owner ) return false;

      // Identificadores para o prosseguimento da revalidação
      let validationResult = true,
          formerIsQuiet = m.app.isQuiet;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Atualiza resultado da validação com indicação de se magia ainda é válida
      validationResult &&= this.validateSpell();

      // Atualiza resultado da validação com validação do dono ante as magias adjuntas desta magia
      for( let adjunctSpell of adjunctSpells ) validationResult &&= adjunctSpell.content.effects.validateOwner?.( committer, this ) ?? true;

      // Atualiza resultado da validação com validação dos alvos da magia
      for( let target of spellTargets ) validationResult &&= m.Spell.validateTarget( this, target );

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Invalida ação quando isso tiver sido explicitado
      if( !validationResult ) return false;

      // Indica que ação foi revalidada com sucesso
      return true;
    }

    /// Operação a ser executada quando ação terminar sua conclusão
    action.finishExecution = function ( eventData = {} ) {
      // Identificadores
      var { actionTarget: target } = eventData;

      // Usa magia
      target.use( this );
    }

    /// Limpa dados relativos à ação
    action.clearData = function ( eventData = {} ) {
      // Identificadores
      var { eventType, actionTarget: spell } = eventData;

      // Zera estatísticas da ação sobre custo de fluxo e mana a ser atribuído
      for( let key of [ 'flowCost', 'manaToAssign', 'adjunctManaToAssign' ] ) this[ key ] = 0;

      // Itera por arranjo de magias adjuntas
      while( this.adjunctSpells.length ) {
        // Identificadores
        let adjunctSpell = this.adjunctSpells[ 0 ];

        // Quando aplicável, desfaz alteração da magia adjunta
        if( spell && !spell.isAdjunct ) adjunctSpell.content.effects.dropModifier?.( spell );

        // Remove magia adjunta alvo
        this.adjunctSpells.shift();
      }

      // Encerra função caso alvo não tenha sido definido
      if( !spell ) return;

      // Quando aplicável, remove da ação configurações adicionadas por sua magia
      spell.unconfigChannelAction?.( this );

      // Redefine custo de fluxo
      resetFlowCost: {
        // Identificadores
        let [ baseFlowCost, currentFlowCost ] = [ spell.content.stats.base.flowCost, spell.content.stats.current.flowCost ],
            changeNumber = baseFlowCost.toChannel - currentFlowCost.toChannel;

        // Não executa bloco caso não haja valor de custo de fluxo a ser alterado
        if( !changeNumber ) break resetFlowCost;

        // Sinaliza início da mudança de custo de fluxo
        m.events.cardContentStart.flowCost.emit( spell, { changeNumber: changeNumber } );

        // Redefine para valor base custo de fluxo de canalização da magia
        currentFlowCost.toChannel = baseFlowCost.toChannel;

        // Sinaliza fim da mudança de custo de fluxo
        m.events.cardContentEnd.flowCost.emit( spell, { changeNumber: changeNumber } );
      }

      // Encerra função caso ação tenha sido concluída
      if( eventType == 'finish' ) return;

      // Limpa dados da magia alvo da ação
      spell.clearData();

      // Encerra função caso dono da magia não tenha sido definido
      if( !spell.owner ) return;

      // Quando aplicável, provoca efeito da característica 'sharedMagic'
      m.Gnome.checkSharedMagic( spell );
    }

    /// Retorna quantidade mínima de mana permitido para ser atribuído à ação
    action.getMinAssignableMana = function ( spell = this.target ) {
      // Não executa função caso acionante da ação ainda não tenha sido definido
      if( !this.committer ) return 0;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( spell instanceof m.Spell );
        m.oAssert( !this.target || spell == this.target || this.target.isAdjunct );
      }

      // Identificadores
      var spellTargets = [ spell.target, ...spell.extraTargets ].filter( target => target ),
          assignableMana = spell.content.stats.current.manaCost.toChannel.min,
          formerIsQuiet = m.app.isQuiet;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Caso um dos alvos da magia seja um anão, incrementa quantidade mínima até que ela valide característica 'antimagicEssence'
      if( spellTargets.some( target => target instanceof m.Dwarf ) )
        while( !m.Dwarf.validatePotency( this, assignableMana ) ) assignableMana++;

      // Caso magia seja uma persistente, ajusta mana mínimo para que seja um múltiplo do custo de persistência
      if( spell.content.typeset.duration == 'persistent' )
        while( assignableMana % spell.content.stats.current.manaCost.toPersist ) assignableMana++;

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna quantidade mínima de mana atribuível
      return assignableMana;
    }

    /// Retorna quantidade máxima de mana permitido para ser atribuído à ação
    action.getMaxAssignableMana = function ( spell = this.target ) {
      // Não executa função caso acionante da ação ainda não tenha sido definido
      if( !this.committer ) return 0;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( spell instanceof m.Spell );
        m.oAssert( !this.target || spell == this.target || this.target.isAdjunct );
      }

      // Identificadores
      var { committer: channeler, adjunctSpells } = this,
          spellPath = spell.content.typeset.path,
          isAstralBeing = channeler instanceof m.AstralBeing,
          channelerMana = channeler.content.stats.combativeness.channeling.mana,
          totalMana = isAstralBeing ? channeler.content.stats.attributes.current.energy : channelerMana.current.total,
          pathMana = isAstralBeing ? Math.min( totalMana, channelerMana.base[ spellPath ] ) : channelerMana.current[ spellPath ],
          metothMana = isAstralBeing ? Math.min( totalMana, channelerMana.base.metoth ) : channelerMana.current.metoth,
          spellMaxMana = spell.content.stats.current.manaCost.toChannel.max,
          assignableMana = Math.min( pathMana, spellMaxMana );

      // Ajusta mana máximo para canalização de modo que considere o ônus de polaridade
      while( pathMana < spell.applyPolarityBurden( assignableMana, channeler ) ) assignableMana--;

      // Define mana a ser gasto por magias adjuntas
      let adjunctMana = this.getAdjunctSpellsMana( assignableMana );

      // Ajusta mana máximo para canalização e mana de magias adjuntas enquanto forem maiores que o mana total ou de Metoth do canalizador
      while( metothMana < adjunctMana || totalMana < assignableMana + adjunctMana || spellPath == 'metoth' && metothMana < assignableMana + adjunctMana )
        adjunctMana = this.getAdjunctSpellsMana( --assignableMana );

      // Caso magia seja uma persistente, ajusta mana máximo para que seja um múltiplo do custo de persistência
      if( spell.content.typeset.duration == 'persistent' )
        while( assignableMana % spell.content.stats.current.manaCost.toPersist ) assignableMana--;

      // Retorna quantidade máxima de mana atribuível
      return assignableMana;
    }

    /// Retorna mana a ser gasto por magias adjuntas, tomando como referência quantidade de mana passada ou atribuída para a magia principal
    action.getAdjunctSpellsMana = function ( manaToAssign = this.manaToAssign ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !this.manaToAssign || manaToAssign == this.manaToAssign );

      // Identificadores
      var { adjunctSpells } = this;

      // Retorna mana a ser gasto por magias adjuntas
      return Math.round( manaToAssign * adjunctSpells.reduce( ( accumulator, current ) => accumulator += current.adjunctMultiplier ?? 0, 0 ) );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionChannel.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-channel' ),
        cardOwner = card.getRelationships().owner;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( card instanceof m.Being );
      m.oAssert( card.content.stats.combativeness.channeling );
      m.oAssert( !eventCategory || [ 'card-activity', 'field-slot-card-change' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa e em uma casa controlada por seu jogador
    if( card.activity == 'active' && card.slot.getController() == cardOwner ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionChannel( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }

  // Realiza verificações de disponibilidade de ação para suas cartas alvo
  ActionChannel.checkAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: slot, eventCategory } = eventData,
        targetCards = slot.cards.filter(
          card => card.listeners( 'field-slot-card-change-end-enter' ).some( listener => listener == this.controlAvailability )
        );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( slot instanceof m.EngagementZone );
    }

    // Apenas executa função caso período atual seja o do combate
    if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

    // Realiza o controle da ação para as cartas que a podem acionar
    for( let card of targetCards ) this.controlAvailability( { eventTarget: card } );
  }
}

/// Propriedades do protótipo
ActionChannel.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionChannel }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionChannel );
