// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const ActionLeech = function ( config = {} ) {
  // Iniciação de propriedades
  ActionLeech.init( this );

  // Identificadores
  var { committer } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ m.Spirit, m.Demon ].some( constructor => committer instanceof constructor ) );
    m.oAssert( committer.content.stats.attributes.current.energy < committer.content.stats.attributes.base.energy );
    m.oAssert( committer.activity == 'active' );
  }

  // Superconstrutor
  m.GameAction.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ActionLeech.init = function ( action ) {
    // Chama 'init' ascendente
    m.GameAction.init( action );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( action instanceof ActionLeech );

    // Atribuição de propriedades de propriedades iniciais

    /// Nome
    action.name = 'action-leech';

    /// Estágios em que ação pode ser acionada
    action.targetStages = [ m.CombatPeriod ];

    /// Custo de fluxo
    action.flowCost = 3;

    /// Executa operação da ação
    action.execute = function * () {
      // Identificadores
      var { committer } = this,
          committerAttributes = committer.content.stats.attributes;

      // Adiciona esta ação ao arranjo de ações em andamento
      m.GameAction.currents.unshift( this );

      // Validação padrão do custo de fluxo de ações
      if( !m.GameAction.validateFlowCost( committer, this.flowCost ) ) return this.cancel();

      // Se ainda inexistente, define alvo da ação
      this.target ??= yield this.selectTarget( 'choose-target-being' );

      // Aciona ação
      yield this.commit();

      // Enquanto ação puder ser sustentada, realiza sua sustentação
      while( true ) {
        // Restaura a energia de acionante, tendo como referência metade do vigor atual de alvo
        committerAttributes.restore( Math.round( this.target.content.stats.attributes.current.stamina * .5 ), 'energy', { triggeringCard: committer } );

        // Caso energia atual de acionante seja igual à máxima lhe permitida, encerra operação
        if( committerAttributes.current.energy >= committerAttributes.base.energy ) return;

        // Pausa operação
        yield;
      }
    }

    /// Valida alvo da ação
    action.validateTarget = function ( target = this.target ) {
      // Identificadores
      var { committer } = this;

      // Validação do componente
      if( target instanceof m.CardSlot )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-card' ) );

      // Validação da raça
      if( !( target instanceof m.Humanoid ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-humanoid' ) );

      // Validação da atividade
      if( target.activity != 'active' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active' ) );

      // Validação da disponibilidade
      if( !this.isCommitted && m.GameAction.currents.some( action => action instanceof ActionLeech && action.target == target ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'leech-must-target-unleeched-target' ) );

      // Validação da vontade
      if( target.content.stats.attributes.current.will >= committer.content.stats.attributes.current.will )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-will-than-committer' ) );

      // Validação do amparo
      if( target.conditions.some( condition => condition instanceof m.ConditionGuarded ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-guarded' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Revalida ação, para a ratificar ante eventuais mudanças durante seu progresso
    action.revalidate = function () {
      // Identificadores
      var { committer } = this,
          formerIsQuiet = m.app.isQuiet;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameAction.currents.some( action => action == this ) );
        m.oAssert( committer.readiness.status == 'occupied' );
      }

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Captura resultado da validação do alvo da ação
      let validationResult = this.validateTarget();

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Retorna resultado da validação
      return Boolean( validationResult );
    }

    // Atribuição de propriedades de 'commitment'
    let commitment = action.commitment;

    /// Tipo de acionamento
    commitment.type = 'dedicated';

    /// Subtipo de acionamento
    commitment.subtype = 'full';
  }

  // Controla disponibilidade de ação para a carta alvo
  ActionLeech.controlAvailability = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: card, eventCategory } = eventData,
        actionIndex = card.actions.findIndex( action => action.name == 'action-leech' ),
        cardAttributes = card.content.stats.attributes;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.Spirit, m.Demon ].some( constructor => card instanceof constructor ) );
      m.oAssert( !eventCategory || [ 'card-activity', 'card-content' ].includes( eventCategory ) );
    }

    // Para caso carta esteja ativa e sua energia atual seja menor que sua energia original
    if( card.activity == 'active' && cardAttributes.current.energy < cardAttributes.base.energy ) {
      // Adiciona ação, caso ela ainda não exista no arranjo de ações da carta
      if( actionIndex == -1 ) card.actions.push( new ActionLeech( { committer: card } ) );
    }
    // Do contrário
    else {
      // Remove ação, caso ela exista no arranjo de ações da carta
      if( actionIndex != -1 ) card.actions.splice( actionIndex, 1 );
    }
  }
}

/// Propriedades do protótipo
ActionLeech.prototype = Object.create( m.GameAction.prototype, {
  // Construtor
  constructor: { value: ActionLeech }
} );

/// Inclusão de ação no arranjo de ações
m.data.actions.push( ActionLeech );
