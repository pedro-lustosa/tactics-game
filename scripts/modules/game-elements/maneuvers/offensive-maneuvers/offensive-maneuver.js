// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const OffensiveManeuver = function ( config = {} ) {
  // Identificadores
  var { damage: damages } = config,
      damageTypes = [ 'blunt', 'slash', 'pierce', 'fire', 'shock', 'mana', 'ether' ];

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( damages?.constructor == Object );
    m.oAssert( Object.keys( damages ).length );
    m.oAssert( Object.keys( damages ).every( name => damageTypes.includes( name ) && Number.isInteger( damages[ name ] ) && damages[ name ] >= 0 ) );
    m.oAssert( !config.penetration || config.penetration >= 0 && config.penetration <= 5 );
    m.oAssert( [ /^M[0-5]$/, /^R[0-5]$/, /^G[0-2]$/ ].some( regex => regex.test( config.range ?? this.range ) ) );
  }

  // Superconstrutor
  m.GameManeuver.call( this, config );

  // Configurações pós-superconstrutor

  /// Itera por dano, penetração e alcance
  for( let property of [ 'damage', 'penetration', 'range' ] ) {
    // Atribui à manobra valor passado da propriedade alvo
    this[ property ] = config[ property ] ?? this[ property ];

    // Adiciona propriedade alvo ao objeto de alterações
    this.changes[ property ] = property == 'damage' ? {} : [];
  }

  // Para o dano, adiciona suas possíveis subpropriedades ao registro de alterações
  Object.assign( this.changes.damage, Object.fromEntries( damageTypes.map( damageType => [ damageType, [] ] ) ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OffensiveManeuver.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.GameManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof OffensiveManeuver );

    // Atribuição de propriedades iniciais

    /// Dano
    maneuver.damage = {};

    /// Penetração
    maneuver.penetration = 0;

    /// Alcance
    maneuver.range = '';

    /// Tipo de alvo válido
    maneuver.targetType = m.PhysicalBeing;

    /// Defesas válidas
    maneuver.defenses = [];
  }
}

/// Propriedades do protótipo
OffensiveManeuver.prototype = Object.create( m.GameManeuver.prototype, {
  // Construtor
  constructor: { value: OffensiveManeuver }
} );
