// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverShoot = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverShoot.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.range?.startsWith( 'R' ) );

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverShoot.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverShoot );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-shoot';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverCover ];

    /// Valida alvo da manobra
    maneuver.validateTarget = function ( target ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( target instanceof this.targetType );

      // Identificadores
      var targetAttachments = target.getAllCardAttachments();

      // Invalida alvo caso ele esteja vinculado a uma magia 'Casulo Eólico'
      if( targetAttachments.some( attachment => attachment instanceof m.SpellAirCocoon && attachment.content.effects.isEnabled ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-with-air-cocoon' ) );

      // Validação ante paveses
      paviseValidation: {
        // Identificadores
        let bypassingActions = [ m.ActionAttack, m.ActionEngage, m.ActionRetreat ];

        // Encerra bloco caso alvo não esteja com um pavês em uso
        if( !targetAttachments.some( attachment => attachment instanceof m.ImplementPavise && attachment.isInUse ) ) break paviseValidation;

        // Encerra bloco caso alvo esteja em uma zona de engajamento
        if( target.slot instanceof m.EngagementZone ) break paviseValidation;

        // Encerra bloco caso controlador do acionante de "disparar" seja igual ao dono do alvo
        if( this.source.getRelationships().controller == target.getRelationships().owner ) break paviseValidation;

        // Captura ações em progresso de alvo
        let beingActions = m.GameAction.currents.filter( action => action.committer == target );

        // Encerra bloco caso alvo esteja ocupado com certas ações
        if( beingActions.some( action => bypassingActions.some( constructor => action instanceof constructor ) ) ) break paviseValidation;

        // Filtra ações do alvo que sejam 'canalizar' ou 'sustentar'
        let spellActions = beingActions.filter( action => [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );

        // Encerra bloco caso alvo esteja ocupado com 'canalizar' ou 'sustentar' enquanto com um alvo direto que não seja uma magia nem si mesmo
        if( spellActions.some( action => action.target.target && action.target.target != target && !( action.target.target instanceof m.Spell ) ) )
          break paviseValidation;

        // Invalida alvo
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-with-pavise' ) );
      }

      // Indica que alvo da manobra é válido
      return true;
    }
  }
}

/// Propriedades do protótipo
ManeuverShoot.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverShoot }
} );
