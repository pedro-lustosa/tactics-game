// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverBite = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverBite.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.pierce > 0 );
    m.oAssert( !config.range || config.range == 'M0' );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverBite.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverBite );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-bite';

    /// Alcance
    maneuver.range = 'M0';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverBite.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverBite }
} );
