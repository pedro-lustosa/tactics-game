// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverRapture = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverRapture.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ m.Spirit, m.Demon ].some( constructor => config.source instanceof constructor ) );
    m.oAssert( config.damage?.ether > 0 );
    m.oAssert( !config.range || config.range == 'G1' );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverRapture.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverRapture );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-rapture';

    /// Alcance
    maneuver.range = 'G1';

    /// Valida alvo da manobra
    maneuver.validateTarget = function ( target ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( target instanceof this.targetType );

      // Identificadores
      var targetAttributes = target.content.stats.attributes,
          maxHealth = Math.round( targetAttributes.base.health * ( this.source instanceof m.Spirit ? .5 : .75 ) );

      // Invalida alvo da manobra caso sua vida atual seja maior que a máxima permitida
      if( targetAttributes.current.health > maxHealth )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'attack-rapture-too-high-health', { maxHealth: maxHealth } ) );

      // Indica que alvo da manobra é válido
      return true;
    }

    /// Aplica dano colateral da manobra
    maneuver.applyCollateralDamage = function ( spentPoints ) {
      // Identificadores
      var beingAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack && action.attack.maneuver == this );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( spentPoints ) && spentPoints >= 0 );
        m.oAssert( beingAttack );
      }

      // Adiciona evento para reduzir energia de ente astral após término de seu ataque
      m.events.actionChangeEnd.complete.add( beingAttack, () =>
        ( new m.DamageRaw( { totalPoints: spentPoints * 2, maneuver: this, source: this.source } ) ).inflictPoints( this.source ),
      { context: this, once: true } );
    }
  }
}

/// Propriedades do protótipo
ManeuverRapture.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverRapture }
} );
