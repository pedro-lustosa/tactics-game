// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverBurn = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverBurn.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.fire > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverBurn.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverBurn );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-burn';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverBurn.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverBurn }
} );
