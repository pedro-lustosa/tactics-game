// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverRepress = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverRepress.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ m.Spirit, m.Demon ].some( constructor => config.source instanceof constructor ) );
    m.oAssert( config.damage?.ether > 0 );
    m.oAssert( !config.range || config.range == 'G1' );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverRepress.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverRepress );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-repress';

    /// Alcance
    maneuver.range = 'G1';

    /// Tipo de alvo válido
    maneuver.targetType = m.AstralBeing;
  }
}

/// Propriedades do protótipo
ManeuverRepress.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverRepress }
} );
