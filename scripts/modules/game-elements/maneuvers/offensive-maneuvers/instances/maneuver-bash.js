// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverBash = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverBash.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.blunt > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverBash.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverBash );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-bash';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverBlock, m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverBash.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverBash }
} );
