// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverFireBolt = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverFireBolt.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.source instanceof m.ImplementBlazingStaff );
    m.oAssert( !config.range || config.range == 'R1' );
  }

  // Configurações pré-superconstrutor

  /// Força dano a ser o inicial
  config.damage = this.damage;

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverFireBolt.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverFireBolt );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-fire-bolt';

    /// Dano
    maneuver.damage = {
      fire: 4
    };

    /// Alcance
    maneuver.range = 'R1';
  }
}

/// Propriedades do protótipo
ManeuverFireBolt.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverFireBolt }
} );
