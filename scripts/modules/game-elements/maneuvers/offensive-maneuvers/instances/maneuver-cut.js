// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverCut = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverCut.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.slash > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverCut.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverCut );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-cut';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverBlock, m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverCut.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverCut }
} );
