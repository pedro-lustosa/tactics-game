// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverSwarm = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverSwarm.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !config.range || config.range == 'M0' );

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverSwarm.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverSwarm );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-swarm';

    /// Alcance
    maneuver.range = 'M0';
  }
}

/// Propriedades do protótipo
ManeuverSwarm.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverSwarm }
} );
