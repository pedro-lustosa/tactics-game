// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverThrow = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverThrow.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !config.range || config.range == 'R0' );

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverThrow.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverThrow );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-throw';

    /// Alcance
    maneuver.range = 'R0';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverCover, m.ManeuverDodge ];

    /// Valida alvo da manobra
    maneuver.validateTarget = function ( target ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( target instanceof this.targetType );

      // Invalida alvo caso ele esteja vinculado a uma magia 'Casulo Eólico'
      if( target.getAllCardAttachments().some( attachment => attachment instanceof m.SpellAirCocoon && attachment.content.effects.isEnabled ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-with-air-cocoon' ) );

      // Indica que alvo da manobra é válido
      return true;
    }
  }
}

/// Propriedades do protótipo
ManeuverThrow.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverThrow }
} );
