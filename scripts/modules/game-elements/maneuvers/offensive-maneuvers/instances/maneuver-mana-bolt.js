// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverManaBolt = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverManaBolt.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.source instanceof m.Humanoid );
    m.oAssert( config.source.content.stats.combativeness.channeling.paths.powth.skill );
    m.oAssert( !config.range || config.range == 'R1' );
  }

  // Configurações pré-superconstrutor

  /// Força dano a ser o inicial
  config.damage = this.damage;

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverManaBolt.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverManaBolt );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-mana-bolt';

    /// Dano
    maneuver.damage = {
      mana: 5
    };

    /// Alcance
    maneuver.range = 'R1';

    /// Tipo de alvo válido
    maneuver.targetType = m.Being;

    /// Aplica redução dos pontos de mana de seu canalizador
    maneuver.reduceManaPoints = function ( spentPoints ) {
      // Identificadores
      var beingAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack && action.attack.maneuver == this );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( spentPoints ) && spentPoints >= 0 );
        m.oAssert( beingAttack );
      }

      // Subtrai pontos gastos com a manobra dos pontos de mana de seu canalizador
      this.source.content.stats.combativeness.channeling.mana.reduce( spentPoints, 'powth' );
    }
  }
}

/// Propriedades do protótipo
ManeuverManaBolt.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverManaBolt }
} );
