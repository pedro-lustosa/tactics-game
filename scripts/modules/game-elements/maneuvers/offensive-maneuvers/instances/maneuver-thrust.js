// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverThrust = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverThrust.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.pierce > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverThrust.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverThrust );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-thrust';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverRepel, m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverThrust.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverThrust }
} );
