// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverUnarmed = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverUnarmed.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.source instanceof m.PhysicalBeing );
    m.oAssert( config.damage?.blunt > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverUnarmed.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverUnarmed );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-unarmed';

    /// Recuperação
    maneuver.recovery = 3;

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverUnarmed.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverUnarmed }
} );
