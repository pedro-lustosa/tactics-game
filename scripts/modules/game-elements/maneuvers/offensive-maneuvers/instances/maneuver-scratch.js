// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverScratch = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverScratch.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.damage?.slash > 0 );
    m.oAssert( config.range?.startsWith( 'M' ) );
  }

  // Superconstrutor
  m.OffensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverScratch.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.OffensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverScratch );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-scratch';

    /// Defesas válidas
    maneuver.defenses = [ m.ManeuverDodge ];
  }
}

/// Propriedades do protótipo
ManeuverScratch.prototype = Object.create( m.OffensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverScratch }
} );
