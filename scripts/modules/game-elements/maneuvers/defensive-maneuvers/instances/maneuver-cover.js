// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverCover = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverCover.init( this );

  // Superconstrutor
  m.DefensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverCover.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.DefensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverCover );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-cover';

    /// Ataques válidos
    maneuver.attacks = [ m.ManeuverShoot, m.ManeuverThrow ];
  }
}

/// Propriedades do protótipo
ManeuverCover.prototype = Object.create( m.DefensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverCover }
} );
