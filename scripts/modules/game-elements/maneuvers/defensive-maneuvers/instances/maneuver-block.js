// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverBlock = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverBlock.init( this );

  // Superconstrutor
  m.DefensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverBlock.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.DefensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverBlock );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-block';

    /// Ataques válidos
    maneuver.attacks = [ m.ManeuverBash, m.ManeuverCut ];
  }
}

/// Propriedades do protótipo
ManeuverBlock.prototype = Object.create( m.DefensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverBlock }
} );
