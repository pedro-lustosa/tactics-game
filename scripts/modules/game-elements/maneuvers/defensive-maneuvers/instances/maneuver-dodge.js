// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverDodge = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverDodge.init( this );

  // Superconstrutor
  m.DefensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverDodge.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.DefensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverDodge );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-dodge';

    /// Ataques válidos
    maneuver.attacks = [
      m.ManeuverUnarmed, m.ManeuverBash, m.ManeuverCut, m.ManeuverThrust, m.ManeuverThrow, m.ManeuverScratch, m.ManeuverBite, m.ManeuverBurn
    ];
  }
}

/// Propriedades do protótipo
ManeuverDodge.prototype = Object.create( m.DefensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverDodge }
} );
