// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ManeuverRepel = function ( config = {} ) {
  // Iniciação de propriedades
  ManeuverRepel.init( this );

  // Superconstrutor
  m.DefensiveManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ManeuverRepel.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.DefensiveManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof ManeuverRepel );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = 'maneuver-repel';

    /// Ataques válidos
    maneuver.attacks = [ m.ManeuverThrust ];
  }
}

/// Propriedades do protótipo
ManeuverRepel.prototype = Object.create( m.DefensiveManeuver.prototype, {
  // Construtor
  constructor: { value: ManeuverRepel }
} );
