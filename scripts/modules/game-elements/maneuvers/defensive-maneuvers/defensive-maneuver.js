// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DefensiveManeuver = function ( config = {} ) {
  // Superconstrutor
  m.GameManeuver.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DefensiveManeuver.init = function ( maneuver ) {
    // Chama 'init' ascendente
    m.GameManeuver.init( maneuver );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof DefensiveManeuver );

    // Atribuição de propriedades iniciais

    /// Ataques válidos
    maneuver.attacks = [];
  }
}

/// Propriedades do protótipo
DefensiveManeuver.prototype = Object.create( m.GameManeuver.prototype, {
  // Construtor
  constructor: { value: DefensiveManeuver }
} );
