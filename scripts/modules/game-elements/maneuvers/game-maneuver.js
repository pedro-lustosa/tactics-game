// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameManeuver = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( this.name && typeof this.name == 'string' && this.name.startsWith( 'maneuver-' ) );
    m.oAssert( [ m.Being, m.Weapon, m.Implement ].some( constructor => config.source instanceof constructor ) );
    m.oAssert( !config.modifier || config.modifier >= -5 && config.modifier <= 5 );
    m.oAssert( !config.recovery || config.recovery >= 0 && config.recovery <= 5 );
  }

  // Atribui origem da manobra
  this.source = config.source;

  // Atribui à manobra seu modificador e sua recuperação
  for( let key of [ 'modifier', 'recovery' ] ) this[ key ] = config[ key ] ?? this[ key ];

  // Adiciona objeto para registrar mudanças na recuperação
  this.changes.recovery = [];
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GameManeuver.init = function ( maneuver ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( maneuver instanceof GameManeuver );

    // Atribuição de propriedades iniciais

    /// Nome
    maneuver.name = '';

    /// Origem da manobra
    maneuver.source = null;

    /// Modificador
    maneuver.modifier = 0;

    /// Recuperação
    maneuver.recovery = 0;

    /// Para registro de mudanças na manobra
    maneuver.changes = {};

    /// Adiciona a uma estatística da manobra um modificador
    maneuver.applyModifier = function ( statName, number, modifierName ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ statName, modifierName ].every( value => value && typeof value == 'string' ) );
        m.oAssert( statName.split( '-' ).reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes ) );
        m.oAssert( Number.isInteger( number ) );
      }

      // Identificadores
      var statPath = statName.split( '-' ),
          firstKeys = statPath.slice( 0, -1 ),
          lastKey = statPath.slice( -1 ).pop(),
          statHeading = firstKeys.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this ),
          modifiersArray = statPath.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes ),
          currentModifierIndex = modifiersArray.findIndex( modifierObject => modifierObject.name == modifierName ),
          dataToEmit = { maneuver: this, method: 'applyModifier' };

      // Caso modificador passado já exista, remove o antigo
      if( currentModifierIndex != -1 ) this.dropModifier( statName, modifierName );

      // Ajusta modificador de modo que sua adição não exceda delimitações da estatística alvo
      adjustModifier: {
        // Identificadores
        let sourceSize = this.source.content.typeset.size,
            sizeModifier = sourceSize == 'large' ? 3 : sourceSize == 'medium' ? 2 : 1,
            currentValue = statName == 'range' ? Number( this.range.replace( /\D/g, '' ) ) : statHeading[ lastKey ] || 0;

        // Impede que modificador resulte em um valor negativo
        while( currentValue + number < 0 ) number++;

        // Para a recuperação, impede que modificador resulte em um valor maior que 5
        if( statName == 'recovery' )
          while( currentValue + number > 5 ) number--

        // Para a penetração, impede que modificador resulte em um valor maior que 5 vezes o modificador de tamanho
        else if( statName == 'penetration' )
          while( currentValue + number > 5 * sizeModifier ) number--

        // Para o alcance, impede que modificador resulte em um valor maior que sua respectiva delimitação
        else if( statName == 'range' )
          while( !/((M[0-8])|(R[0-5])|(G[0-2]))$/.test( this.range[ 0 ] + ( currentValue + number ) ) ) number--;
      }

      // Não continua função caso valor do modificador seja 0
      if( !number ) return this;

      // Adiciona aos dados a serem emitidos valor do modificador a ser aplicado
      dataToEmit.changeNumber = number;

      // Caso estatística a ser modificada seja o dano, adiciona aos dados a serem emitidos tipo do dano
      if( statName.startsWith( 'damage-' ) ) dataToEmit.damageType = statPath[ 1 ];

      // Sinaliza início da mudança da estatística alvo
      m.events.cardContentStart[ statPath[ 0 ] ].emit( this.source, dataToEmit );

      // Caso estatística alvo seja o alcance, realiza alteração adaptada
      if( statName == 'range' ) statHeading[ lastKey ] = this.range.replace( /\d/g, '' ) + ( Number( this.range.replace( /\D/g, '' ) ) + number )

      // Do contrário, realiza alteração padrão
      else statHeading[ lastKey ] = ( statHeading[ lastKey ] || 0 ) + number;

      // Adiciona modificador passado à manobra
      modifiersArray.push( { name: modifierName, value: number } );

      // Sinaliza fim da mudança da estatística alvo
      m.events.cardContentEnd[ statPath[ 0 ] ].emit( this.source, dataToEmit );

      // Retorna manobra
      return this;
    }

    /// Remove de uma estatística da manobra um modificador
    maneuver.dropModifier = function ( statName, modifierName ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ statName, modifierName ].every( value => value && typeof value == 'string' ) );
        m.oAssert( statName.split( '-' ).reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes ) );
      }

      // Identificadores
      var statPath = statName.split( '-' ),
          firstKeys = statPath.slice( 0, -1 ),
          lastKey = statPath.slice( -1 ).pop(),
          statHeading = firstKeys.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this ),
          modifiersArray = statPath.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes ),
          currentModifierIndex = modifiersArray.findIndex( modifierObject => modifierObject.name == modifierName ),
          number = modifiersArray[ currentModifierIndex ]?.value ?? 0,
          dataToEmit = { maneuver: this, method: 'dropModifier', changeNumber: number };

      // Caso modificador passado não exista, encerra função
      if( currentModifierIndex == -1 ) return this;

      // Caso estatística a ser modificada seja o dano, adiciona aos dados a serem emitidos tipo do dano
      if( statName.startsWith( 'damage-' ) ) dataToEmit.damageType = statPath[ 1 ];

      // Sinaliza início da mudança da estatística alvo
      m.events.cardContentStart[ statPath[ 0 ] ].emit( this.source, dataToEmit );

      // Caso estatística alvo seja o alcance, realiza alteração adaptada
      if( statName == 'range' ) statHeading[ lastKey ] = this.range.replace( /\d/g, '' ) + ( Number( this.range.replace( /\D/g, '' ) ) - number )

      // Do contrário, realiza alteração padrão
      else statHeading[ lastKey ] -= number;

      // Remove modificador passado da manobra
      modifiersArray.splice( currentModifierIndex, 1 );

      // Caso estatística modificada tenha sido o dano, remove seu tipo de dano caso seu valor original seja 0 e não haja mais modificadores o afetando
      if( statName.startsWith( 'damage-' ) && !modifiersArray.length && !statHeading[ lastKey ] ) delete statHeading[ lastKey ];

      // Sinaliza fim da mudança da estatística alvo
      m.events.cardContentEnd[ statPath[ 0 ] ].emit( this.source, dataToEmit );

      // Retorna manobra
      return this;
    }

    /// Retorna valor original de uma estatística da manobra
    maneuver.getBaseValue = function ( statName ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( statName && typeof statName == 'string' );
        m.oAssert( statName.split( '-' ).reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes ) );
      }

      // Identificadores
      var statPath = statName.split( '-' ),
          statValue = statPath.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this ) || 0,
          modifiersArray = statPath.reduce( ( accumulator, current ) => accumulator = accumulator[ current ], this.changes );

      // Subtrai do valor atual da estatística valor de seus modificadores
      for( let modifier of modifiersArray ) statValue -= modifier.value;

      // Retorna valor original da estatística
      return statValue;
    }
  }
}
