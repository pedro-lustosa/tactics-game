// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementPitch = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementPitch.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementPitch.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementPitch );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'pitch';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Weapon;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'ordinary';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida vinculante
    effects.validateAttacher = function ( attacher ) {
      // Identificadores
      var attacherAttacks = attacher?.content?.stats?.effectivity?.current?.attacks;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attacher instanceof typeset.attachability );

      // Invalida vinculante caso não tenha ataque 'disparar'
      if( !attacherAttacks.some( attack => attack instanceof m.ManeuverShoot ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'attacher-invalid-attack', { attack: m.languages.keywords.getManeuver( 'maneuver-shoot' ).toLowerCase() }
        ) );

      // Invalida vinculante caso dano principal de seu ataque 'disparar' não seja de perfuração
      if( Object.keys( attacherAttacks.find( attack => attack instanceof m.ManeuverShoot ).damage )[0] != 'pierce' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'shoot-invalid-damage' ) );

      // Indica que vinculante é válido
      return true;
    }

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var shootManeuver = implement.attacher.content.stats.effectivity.current.attacks.find( attack => attack instanceof m.ManeuverShoot );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Concede 2 pontos de dano de fogo a 'Disparar'
      shootManeuver.applyModifier( 'damage-fire', 2, 'pitch' );

      // Diminui em 1 alcance de 'Disparar'
      shootManeuver.applyModifier( 'range', -1, 'pitch' );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var weaponAttachments = implement.attacher.getAllCardAttachments(),
          shootManeuver = implement.attacher.content.stats.effectivity.current.attacks.find( attack => attack instanceof m.ManeuverShoot );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Caso vinculante não tenha nenhum outro piche em uso, remove da manobra 'Disparar' modificadores do efeito
      if( !weaponAttachments.some( card => card instanceof ImplementPitch && card.isInUse && card != implement ) )
        for( let key of [ 'damage-fire', 'range' ] ) shootManeuver.dropModifier( key, 'pitch' );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 4;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementPitch.gridOrder = m.data.cards.items.push( ImplementPitch ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementPitch.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementPitch }
} );
