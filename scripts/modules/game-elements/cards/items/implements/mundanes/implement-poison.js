// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementPoison = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementPoison.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementPoison.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementPoison );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'poison';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Weapon;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'ordinary';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida vinculante
    effects.validateAttacher = function ( attacher ) {
      // Identificadores
      var attacherAttacks = attacher?.content?.stats?.effectivity?.current?.attacks;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attacher instanceof typeset.attachability );

      // Invalida vinculante caso não tenha um ataque com dano de corte ou de perfuração
      if( !attacherAttacks.some( attack => Object.keys( attack.damage ).some( damage => [ 'slash', 'pierce' ].includes( damage ) ) ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'attacher-must-have-slash-or-pierce-damage' ) );

      // Indica que vinculante é válido
      return true;
    }

    /// Verifica se condição 'Enfermo' deve ser aplicada ao alvo de um ataque
    effects.checkDiseasedGain = function ( combatBreak = {} ) {
      // Identificadores
      var { defender, damages } = combatBreak,
          actionAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( combatBreak.attack.maneuver.source == implement.attacher );
        m.oAssert( actionAttack );
      }

      // Não executa função caso defensor não seja um ente biótico
      if( !( defender instanceof m.BioticBeing ) ) return;

      // Captura danos de corte e perfuração com pontos infligidos
      let targetDamages = damages.filter( damage =>
            [ m.DamageSlash, m.DamagePierce ].some( constructor => damage instanceof constructor ) && damage.totalPoints
          );

      // Não executa função caso ataque usado não tenha um dano de corte ou de perfuração a ser infligido
      if( !targetDamages.length ) return;

      // Define pontos de ataque com dano infligido, enquanto o maior valor entre esses pontos dos danos alvo
      let damagingAttackPoints = targetDamages.reduce( ( accumulator, current ) => Math.max( accumulator, current.damagingAttackPoints ), 0 );

      // Programa aplicação dos marcadores de enfermo
      m.events.actionChangeEnd.complete.add( actionAttack,
        eventData => eventData.eventType == 'finish' ? this.applyDiseasedMarkers( defender, damagingAttackPoints ) : null,
      { context: this, once: true } );
    }

    /// Aplica marcadores de 'Enfermo' relativos ao efeito deste equipamento
    effects.applyDiseasedMarkers = function ( being, markers ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.BioticBeing );
        m.oAssert( Number.isInteger( markers ) && markers > 0 );
      }

      // Identificadores
      var diseasedCondition = being.conditions.find( condition => condition instanceof m.ConditionDiseased ) ?? new m.ConditionDiseased( {source: implement} );

      // Aplica condição de enfermo ao ente alvo
      diseasedCondition.apply( being, null, markers );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 8;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementPoison.gridOrder = m.data.cards.items.push( ImplementPoison ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementPoison.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementPoison }
} );
