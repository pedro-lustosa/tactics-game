// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementHorn = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementHorn.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-horn`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementHorn.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementHorn );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'ordinary';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 14;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 1;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementHorn.gridOrder = m.data.cards.items.push( ImplementHorn ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementHorn.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementHorn }
} );
