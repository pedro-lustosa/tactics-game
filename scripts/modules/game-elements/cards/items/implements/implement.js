// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Implement = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset } } = this,
      sizeModifier = typeset.size == 'large' ? 2 : typeset.size == 'medium' ? 1 : 0;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'non-handed', 'one-handed', 'two-handed', 'multi-handed' ].includes( typeset.wielding ) );
    m.oAssert( typeset.usage == 'dynamic' || typeset.attachability == m.Humanoid );
    m.oAssert( typeset.usage == 'dynamic' || typeset.wielding == 'non-handed' );
    m.oAssert( typeset.wielding == 'non-handed' || typeset.attachability == m.Humanoid );
  }

  // Configurações pré-superconstrutor

  /// Ordem da carta em grades
  this.gridOrder = this.constructor.gridOrder + sizeModifier;

  // Superconstrutor
  m.Item.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Implement.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Item.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof Implement );

    // Atribuição de propriedades iniciais

    /// Usa apetrecho
    implement.use = function() {
      // Identificadores
      var { attacher } = this,
          { wielding } = this.content.typeset;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.activity == 'active' );
        m.oAssert( !this.isToAttach || attacher.isInUse );
      }

      // Encerra função caso equipamento já esteja sendo usado
      if( this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useIn.emit( eventTarget, { useTarget: this } );

      // Para caso equipamento requeira mãos
      if( wielding != 'non-handed' ) {
        // Identificadores
        let attacherManeuvers = attacher.content.stats.effectivity.maneuvers,
            rank = attacherManeuvers.secondary.source && !attacherManeuvers.primary.source ? 'primary' : 'secondary';

        // Adiciona equipamento a mãos do vinculante
        attacher.hands.put( this, rank );
      }

      // Identifica que equipamento está sendo usado
      this.isInUse = true;

      // Habilita efeito do equipamento
      this.content.effects.enable();

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useIn.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    /// Desusa apetrecho
    implement.disuse = function() {
      // Identificadores
      var { attacher } = this,
          { wielding } = this.content.typeset;

      // Encerra função caso equipamento não esteja sendo usado
      if( !this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useOut.emit( eventTarget, { useTarget: this } );

      // Desabilita efeito do equipamento
      this.content.effects.disable();

      // Quando aplicável, limpa dados próprios do equipamento
      this.clearCustomData?.();

      // Caso equipamento requeira mãos, remove-o das mãos do vinculante
      if( wielding != 'non-handed' ) attacher.hands.release( this );

      // Identifica que equipamento não está mais sendo usado
      this.isInUse = false;

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useOut.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = implement.content.typeset;

    /// Equipagem
    typeset.equipage = 'implement';

    /// Empunhadura
    typeset.wielding = '';
  }
}

/// Propriedades do protótipo
Implement.prototype = Object.create( m.Item.prototype, {
  // Construtor
  constructor: { value: Implement }
} );
