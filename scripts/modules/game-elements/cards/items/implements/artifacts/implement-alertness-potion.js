// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementAlertnessPotion = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementAlertnessPotion.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementAlertnessPotion.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementAlertnessPotion );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'alertness-potion';

    /// Indica se apetrecho é vinculável
    implement.isToAttach = false;

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementAlertnessPotion.gridOrder = m.data.cards.items.push( ImplementAlertnessPotion ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementAlertnessPotion.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementAlertnessPotion }
} );
