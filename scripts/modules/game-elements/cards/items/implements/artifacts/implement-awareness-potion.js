// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementAwarenessPotion = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementAwarenessPotion.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo de entes afetados pelo efeito do equipamento
  ImplementAwarenessPotion.affectedBeings = [];

  // Iniciação de propriedades
  ImplementAwarenessPotion.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementAwarenessPotion );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'awareness-potion';

    /// Indica se apetrecho é vinculável
    implement.isToAttach = false;

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida vinculante
    effects.validateAttacher = function ( attacher ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attacher instanceof typeset.attachability );

      // Invalida vinculante caso ele já esteja adiantável
      if( attacher.readiness.isForwardable )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-forwardable' ) );

      // Indica que vinculante é válido
      return true;
    }

    /// Provoca efeito relativo ao uso do equipamento
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == implement );

      // Identificadores pós-validação
      var affectedBeing = implement.owner,
          postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Caso ainda não esteja lá, adiciona ente ao arranjo de entes afetados pelo efeito deste equipamento
      if( !ImplementAwarenessPotion.affectedBeings.includes( affectedBeing ) ) ImplementAwarenessPotion.affectedBeings.push( affectedBeing );

      // Indica que ente pode ser adiantado
      affectedBeing.readiness.isForwardable = true;

      // Adiciona evento para remover dados do efeito do equipamento ao fim do próximo pós-combate
      m.events.flowChangeEnd.finish.add( postCombat, this.clearData.bind( this, affectedBeing ), { context: this, once: true } );
    }

    /// Limpa dados do efeito do equipamento
    effects.clearData = function ( being, eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData,
          { affectedBeings } = ImplementAwarenessPotion;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( affectedBeings.includes( being ) );
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget?.name == 'post-combat-break-period' );
      }

      // Remove ente do arranjo de entes afetados pelo efeito deste equipamento
      affectedBeings.splice( affectedBeings.indexOf( being ), 1 );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 50;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementAwarenessPotion.gridOrder = m.data.cards.items.push( ImplementAwarenessPotion ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementAwarenessPotion.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementAwarenessPotion }
} );
