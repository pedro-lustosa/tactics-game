// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementShadowWand = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementShadowWand.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this,
      manaLimit = size == 'large' ? 8 : size == 'medium' ? 6 : 4;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-shadow-wand`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Mana máximo e atual
  this.attachments.mana = this.maxAttachments.mana = manaLimit;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementShadowWand.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementShadowWand );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'one-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementShadowWand.gridOrder = m.data.cards.items.push( ImplementShadowWand ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementShadowWand.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementShadowWand }
} );
