// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementScepterOfCommand = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementScepterOfCommand.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-scepter-of-command`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementScepterOfCommand.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementScepterOfCommand );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'one-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementScepterOfCommand.gridOrder = m.data.cards.items.push( ImplementScepterOfCommand ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementScepterOfCommand.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementScepterOfCommand }
} );
