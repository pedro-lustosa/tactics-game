// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementFluteOfWolfmancy = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementFluteOfWolfmancy.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-flute-of-wolfmancy`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição da capacidade de convocar
  Object.oAssignByCopy( this, m.mixinSummonable.call( this ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementFluteOfWolfmancy.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementFluteOfWolfmancy );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'two-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementFluteOfWolfmancy.gridOrder = m.data.cards.items.push( ImplementFluteOfWolfmancy ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementFluteOfWolfmancy.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementFluteOfWolfmancy }
} );
