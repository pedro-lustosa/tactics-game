// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementOrichalcumPendant = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementOrichalcumPendant.init( this );

  // Configurações pré-superconstrutor

  /// Atribuição do mana máximo
  this.maxAttachments.mana = 4;

  // Superconstrutor
  m.Implement.call( this, config );

  // Eventos

  /// Adiciona evento para zerar mana vinculado ao apetrecho após sua inativação
  m.events.cardActivityEnd.inactive.add( this, () => this.detach( this.attachments.mana, 'mana' ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementOrichalcumPendant.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementOrichalcumPendant );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'orichalcum-pendant';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: implement } );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: implement } );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Remove todo mana vinculado a si
      implement.detach( implement.attachments.mana, 'mana' );
    }

    /// Converte potência da magia passada em mana para este apetrecho
    effects.weakenSpell = function ( spell ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Enoth, m.Voth ].some( constructor => spell instanceof constructor ) );

      // Identificadores
      var spellDuration = spell.content.typeset.duration,
          spellStats = spell.content.stats.current,
          potencyToReduce = 0;

      // Não executa função caso apetrecho já tenha atingido o limite de mana vinculável
      if( implement.attachments.mana >= implement.maxAttachments.mana ) return;

      // Não executa função caso magia não tenha potência
      if( !spellStats.potency ) return;

      // Converte potência da magia em mana para este apetrecho
      do {
        // Incrementa potência da magia a ser reduzida
        potencyToReduce++;

        // Incrementa mana vinculado a este apetrecho
        implement.attach( 1, 'mana' );
      }
      while( potencyToReduce < spellStats.potency && implement.attachments.mana < implement.maxAttachments.mana );

      // Aplica redução da potência da magia
      spell.setPotency( spellStats.potency - potencyToReduce );

      // Caso potência da magia tenha atingido 0, notifica usuário sobre negação de seu efeito
      if( !spellStats.potency )
        m.noticeBar.show(
          m.languages.notices.getOutcome( 'orichalcum-pendant-spell-prevented', { item: implement, spell: spell } ),
          !m.GameMatch.current.isSimulation && m.GamePlayer.current == spell.getRelationships().opponent ? 'green' : 'red'
        );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 60;
  }

  // Verifica se efeito do equipamento pode ser ativado ante a magia passada
  ImplementOrichalcumPendant.checkEffectActivation = function ( spell ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof m.Spell );

    // Não executa função caso não se esteja no período do combate
    if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

    // Não executa função caso magia não seja de Enoth ou Voth
    if( [ m.Enoth, m.Voth ].every( constructor => !( spell instanceof constructor ) ) ) return;

    // Captura entes que sejam alvos diretos da magia
    let targetBeings = [ ...spell.extraTargets, spell.target ].filter( target => target instanceof m.Being );

    // Caso magia não tenha entes que sejam alvos diretos, encerra função
    if( !targetBeings.length ) return;

    // Captura pingentes de oricalco dos alvos da magia
    let orichalcumPendants = targetBeings.flatMap(
      being => being.getAllCardAttachments().filter( attachment => attachment instanceof this && attachment.content.effects.isEnabled )
    );

    // Ativa efeitos de eventuais pingentes dos alvos desta magia
    for( let orichalcumPendant of orichalcumPendants ) orichalcumPendant.content.effects.weakenSpell( spell );
  }

  // Ordem inicial das variações desta carta na grade
  ImplementOrichalcumPendant.gridOrder = m.data.cards.items.push( ImplementOrichalcumPendant ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementOrichalcumPendant.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementOrichalcumPendant }
} );
