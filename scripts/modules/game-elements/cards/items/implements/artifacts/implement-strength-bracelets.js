// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementStrengthBracelets = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementStrengthBracelets.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-strength-bracelets`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementStrengthBracelets.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementStrengthBracelets );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: implement } );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: implement } );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var { owner } = implement,
          strengthenedCondition = owner.conditions.find( condition => condition instanceof m.ConditionStrengthened ) ?? new m.ConditionStrengthened();

      // Aplica marcadores de 'fortalecido' ao dono do equipamento
      strengthenedCondition.apply( owner, null, 2, { triggeringCard: implement } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementStrengthBracelets.gridOrder = m.data.cards.items.push( ImplementStrengthBracelets ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementStrengthBracelets.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementStrengthBracelets }
} );
