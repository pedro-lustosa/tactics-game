// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementShockGauntlets = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementShockGauntlets.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-shock-gauntlets`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementShockGauntlets.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementShockGauntlets );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement,
          ownerSize = owner.content.typeset.size,
          sizeModifier = ownerSize == 'large' ? 3 : ownerSize == 'medium' ? 2 : 1,
          unarmedManeuver = owner.content.stats.effectivity.maneuvers.natural.current.attacks.find( maneuver => maneuver instanceof m.ManeuverUnarmed );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Concede à manobra 'Desarmado' de dono pontos de dano de choque, em função do tamanho de dono
      unarmedManeuver.applyModifier( 'damage-shock', sizeModifier, 'shock-gauntlets' );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement,
          unarmedManeuver = owner.content.stats.effectivity.maneuvers.natural.current.attacks.find( maneuver => maneuver instanceof m.ManeuverUnarmed );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Remove da manobra 'Desarmado' de dono modificador deste equipamento
      unarmedManeuver.dropModifier( 'damage-shock', 'shock-gauntlets' );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 15;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 1;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementShockGauntlets.gridOrder = m.data.cards.items.push( ImplementShockGauntlets ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementShockGauntlets.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementShockGauntlets }
} );
