// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementVigilAmulet = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementVigilAmulet.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-vigil-amulet`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementVigilAmulet.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementVigilAmulet );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Adiciona evento para criar espírito após afastamento de dono
      m.events.cardActivityStart.withdrawn.add( owner, this.createSpirit, { context: this } );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Remove evento para criar espírito após afastamento de dono
      m.events.cardActivityStart.withdrawn.remove( owner, this.createSpirit, { context: this } );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Cria espírito tendo dono como referência
    effects.createSpirit = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: owner } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-activity' && eventType == 'withdrawn' && owner == implement.owner );

      // Identificadores pós-validação
      var ownerSpells = owner.content.stats.combativeness.channeling?.getOwnedSpells() ?? [];

      // Programa execução desta função para após fim dos eventos relacionados à mudança de atividade
      setTimeout( function () {
        // Não executa função caso dono não esteja afastado
        if( owner.activity != 'withdrawn' ) return;

        // Interrompe ações ou magias que mirem dono
        m.GameAction.preventAllActions( m.GameAction.currents.filter( action =>
          ( action.target == owner || action.target instanceof m.Spell && ( action.target.target == owner || action.target.extraTargets.includes( owner ) ) )
        ) );

        // Alheia dono
        owner.unlink( { triggeringCard: implement } );

        // Gera espírito a substituir dono
        let spirit = new m.Spirit( { source: owner } );

        // Reativa magias outrora de dono, enquanto definindo seu novo dono como o espírito
        ownerSpells.forEach( spell => spell.activate( spirit, { triggeringCard: implement } ) );

        // Suspende espírito
        spirit.inactivate( 'suspended', { triggeringCard: implement } );
      } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementVigilAmulet.gridOrder = m.data.cards.items.push( ImplementVigilAmulet ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementVigilAmulet.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementVigilAmulet }
} );
