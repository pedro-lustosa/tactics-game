// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementEctoplasmUrn = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementEctoplasmUrn.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this,
      manaLimit = size == 'large' ? 12 : size == 'medium' ? 8 : 4;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-ectoplasm-urn`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Mana máximo e atual
  this.attachments.mana = this.maxAttachments.mana = manaLimit;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementEctoplasmUrn.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementEctoplasmUrn );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Adiciona evento para verificar habilitação da ação 'Energizar' após alteração do mana vinculado ao equipamento
      m.events.attachabilityChangeEnd.attachment.add( implement, this.controlEnergizeAvailability, { context: this } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Remove evento para verificar habilitação da ação 'Energizar' após alteração do mana vinculado ao equipamento
      m.events.attachabilityChangeEnd.attachment.remove( implement, this.controlEnergizeAvailability, { context: this } );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Controla disponibilidade de ação 'Energizar'
    effects.controlEnergizeAvailability = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData,
          { owner } = implement;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'attachability-change' && [ 'attachment-in', 'attachment-out' ].includes( eventType ) && eventTarget == implement );

      // Não executa função caso período atual não seja o do combate
      if( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) ) return;

      // Não executa função caso dono do equipamento não esteja preparado
      if( owner.readiness.status != 'prepared' ) return;

      // Verifica se ação 'Energizar' pode ser habilitada
      return m.ActionEnergize.controlAvailability( { eventTarget: owner } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 18;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 1;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementEctoplasmUrn.gridOrder = m.data.cards.items.push( ImplementEctoplasmUrn ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementEctoplasmUrn.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementEctoplasmUrn }
} );
