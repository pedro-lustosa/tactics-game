// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementRodOfAbsorption = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementRodOfAbsorption.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this,
      manaLimit = size == 'large' ? 12 : size == 'medium' ? 8 : 4;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-rod-of-absorption`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Mana máximo e atual
  this.attachments.mana = this.maxAttachments.mana = manaLimit;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementRodOfAbsorption.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementRodOfAbsorption );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'one-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementRodOfAbsorption.gridOrder = m.data.cards.items.push( ImplementRodOfAbsorption ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementRodOfAbsorption.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementRodOfAbsorption }
} );
