// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementBlazingStaff = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementBlazingStaff.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-blazing-staff`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementBlazingStaff.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementBlazingStaff );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverFireBolt( {
          source: implement
        } )
      ],
      // Defesas
      defenses: []
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Define pontos da fonte de manobras deste apetrecho
    effects.setManeuverSourcePoints = function ( maneuverSource ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( maneuverSource.constructor == Object );
        m.oAssert( maneuverSource.source == implement );
      }

      // Identificadores
      var { owner } = implement,
          ownerSize = owner.content.typeset.size,
          basePoints = ownerSize == 'large' ? 8 : ownerSize == 'medium' ? 6 : 4;

      // Retorna pontos de manobra para o ataque do apetrecho na fonte de manobras alvo
      return Math.max( basePoints - maneuverSource.spentPoints, 0 );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 36;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = .8;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementBlazingStaff.gridOrder = m.data.cards.items.push( ImplementBlazingStaff ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementBlazingStaff.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementBlazingStaff }
} );
