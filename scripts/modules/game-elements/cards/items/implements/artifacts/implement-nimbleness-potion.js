// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementNimblenessPotion = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementNimblenessPotion.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo de entes afetados pelo efeito do equipamento
  ImplementNimblenessPotion.affectedBeings = [];

  // Iniciação de propriedades
  ImplementNimblenessPotion.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementNimblenessPotion );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'nimbleness-potion';

    /// Indica se apetrecho é vinculável
    implement.isToAttach = false;

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito do equipamento
    effects.currentExecution = null;

    /// Ente afetado pelo efeito do equipamento
    effects.affectedBeing = null;

    /// Provoca efeito relativo ao uso do equipamento
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == implement );

      // Identificadores pós-validação
      var affectedBeing = implement.owner,
          postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Caso ainda não esteja lá, adiciona ente ao arranjo de entes afetados pelo efeito deste equipamento
      if( !ImplementNimblenessPotion.affectedBeings.includes( affectedBeing ) ) ImplementNimblenessPotion.affectedBeings.push( affectedBeing );

      // Registra ente afetado pelo efeito deste equipamento
      this.affectedBeing = affectedBeing;

      // Prepara efeito a ocorrer no pós-combate
      postCombat.scheduleEffect.call( implement );

      // Adiciona evento para remover dados do efeito do equipamento ao fim do próximo pós-combate
      m.events.flowChangeEnd.finish.add( postCombat, this.clearData, { context: this, once: true } );
    }

    /// Aplica efeito do equipamento a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Identificadores
      var { affectedBeing } = this;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( affectedBeing instanceof m.Being );
        m.oAssert( ImplementNimblenessPotion.affectedBeings.includes( affectedBeing ) );
        m.oAssert( !this.currentExecution );
      }

      // Captura e inicia execução do efeito
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução do efeito
      function * executeEffect() {
        // Identificadores
        var beingAttributes = affectedBeing.content.stats.attributes;

        // Encerra execução caso ente afetado não esteja ativo
        if( affectedBeing.activity != 'active' ) return effects.currentExecution = null;

        // Encerra execução caso vigor de ente afetado seja de 8 pontos ou mais
        if( beingAttributes.current.stamina >= 8 ) return effects.currentExecution = null;

        // Lança parada de destino sobre afastamento do ente
        m.GameMatch.programFateBreak( affectedBeing.getRelationships().owner, {
          name: `${ implement.name }-fate-break-${ affectedBeing.getMatchId() }`,
          playerBeing: affectedBeing,
          opponentBeing: null,
          isContestable: false,
          successAction: () => effects.currentExecution.next( true ),
          failureAction: () => effects.currentExecution.next( false ),
          modalArguments: [ `fate-break-${ implement.name }`, { item: implement, being: affectedBeing } ]
        } );

        // Aguarda resolução da parada de destino
        let wasUsedFatePoint = yield;

        // Caso parada de destino não tenha sido bem-sucedida, afasta alvo
        if( !wasUsedFatePoint ) affectedBeing.inactivate( 'withdrawn', { triggeringCard: implement } );

        // Encerra execução do efeito
        effects.currentExecution = null;
      }
    }

    /// Limpa dados do efeito do equipamento
    effects.clearData = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData,
          { affectedBeing } = this,
          { affectedBeings } = ImplementNimblenessPotion;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( affectedBeing instanceof m.Being );
        m.oAssert( affectedBeings.includes( affectedBeing ) );
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget?.name == 'post-combat-break-period' );
      }

      // Remove ente do arranjo de entes afetados pelo efeito deste equipamento
      affectedBeings.splice( affectedBeings.indexOf( affectedBeing ), 1 );

      // Nulifica ente afetado pelo efeito do equipamento
      this.affectedBeing = null;
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 20;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementNimblenessPotion.gridOrder = m.data.cards.items.push( ImplementNimblenessPotion ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementNimblenessPotion.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementNimblenessPotion }
} );
