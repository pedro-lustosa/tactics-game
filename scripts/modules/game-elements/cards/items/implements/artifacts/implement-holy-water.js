// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementHolyWater = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementHolyWater.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementHolyWater.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementHolyWater );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'holy-water';

    /// Indica se apetrecho é vinculável
    implement.isToAttach = false;

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementHolyWater.gridOrder = m.data.cards.items.push( ImplementHolyWater ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementHolyWater.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementHolyWater }
} );
