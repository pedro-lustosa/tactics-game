// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementRingOfProtection = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementRingOfProtection.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-ring-of-protection`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementRingOfProtection.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementRingOfProtection );

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'non-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !implement.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( implement );

      // Adiciona aos pontos de destino embutidos do dono fonte de pontos deste equipamento
      owner.content.stats.fatePoints.add( { name: 'ring-of-protection', baseValue: 1, currentValue: 1 } );

      // Adiciona eventos para controlar disponibilidade do ponto de destino deste equipamento
      for( let eventType of [ 'combat', 'fate' ] )
        m.events.breakStart[ eventType ].add( m.GameMatch.current, this.controlFatePointAvailability, { context: this } );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    /// Desativa efeito do equipamento
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return implement;

      // Identificadores
      var { owner } = implement;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( implement );

      // Remove dos pontos de destino embutidos do dono fonte de pontos deste equipamento
      owner.content.stats.fatePoints.remove( 'ring-of-protection' );

      // Remove eventos para controlar disponibilidade do ponto de destino deste equipamento
      for( let eventType of [ 'combat', 'fate' ] )
        m.events.breakStart[ eventType ].remove( m.GameMatch.current, this.controlFatePointAvailability, { context: this } );

      // Indica que efeito do equipamento está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( implement );

      // Retorna equipamento
      return implement;
    }

    // Controla disponibilidade do ponto de destino na parada alvo
    effects.controlFatePointAvailability = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'break' && eventPhase == 'start' && [ 'combat', 'fate' ].includes( eventType ) );

      // Identificadores pós-validação
      var { owner } = implement,
          { fatePoints } = owner.content.stats,
          currentFatePoints = fatePoints.sources.find( sourceObject => sourceObject.name == 'ring-of-protection' )?.currentValue;

      // Não executa função caso dono não tenha pontos de destino atuais provenientes desse equipamento
      if( !currentFatePoints ) return;

      // Executa operação em função de se parada atual é uma de combate ou de destino
      return eventType == 'combat' ? controlInCombatBreak() : controlInFateBreak();

      // Funções

      /// Para controle do ponto de destino em paradas de combate
      function controlInCombatBreak() {
        // Identificadores
        var { combatBreak } = eventData,
            { attacker } = combatBreak;

        // Para caso dono do equipamento seja o atacante da parada atual
        if( attacker == owner ) {
          // Retira ponto de destino do equipamento das fontes de dono
          fatePoints.remove( 'ring-of-protection' );

          // Adiciona evento para readicionar pontos de destino do equipamento após conclusão da parada de combate
          m.events.breakEnd.combat.add( m.GameMatch.current, restoreFatePoints, { once: true } );
        }
      }

      /// Para controle do ponto de destino em paradas de destino
      function controlInFateBreak() {
        // Identificadores
        var fateBreak = m.GameMatch.fateBreakExecution,
            { name, playerBeing, opponentBeing } = fateBreak;

        // Encerra função caso dono não esteja envolvido na parada de destino
        if( [ playerBeing, opponentBeing ].every( being => being != owner ) ) return;

        // Trata nome da parada de destino
        name = name.slice( 0, name.search( /-fate-break-/ ) );

        // Encerra função caso dono seja do jogador a resolver parada e ela seja uma parada em que o efeito do equipamento pode ser usado
        if( playerBeing == owner && [ 'action-possess', 'nimbleness-potion', 'spell-consume', 'spell-pain-twist', 'spell-thunder-strike' ].includes( name ) )
          return;

        // Encerra função caso dono seja do jogador a contestar parada e ela seja uma parada em que o efeito do equipamento pode ser usado
        if( opponentBeing == owner && [ 'spell-pain-twist', 'spell-fireball', 'spell-fissure', 'condition-diseased' ].includes( name ) )
          return;

        // Retira ponto de destino do equipamento das fontes de dono
        fatePoints.remove( 'ring-of-protection' );

        // Adiciona evento para readicionar pontos de destino do equipamento após conclusão da parada de destino
        m.events.breakEnd.fate.add( m.GameMatch.current, restoreFatePoints, { once: true } );
      }

      /// Para recolocar pontos de destino retirados em paradas
      function restoreFatePoints() {
        // Não executa função caso efeito do equipamento não esteja mais ativo
        if( !effects.isEnabled ) return;

        // Recoloca fonte de pontos de destino deste equipamente, assumidamente retirada durante uma parada
        fatePoints.add( { name: 'ring-of-protection', baseValue: 1, currentValue: currentFatePoints } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 40;
  }

  // Valida uso do ponto de destino deste equipamento em relação ao ente passado
  ImplementRingOfProtection.validateFatePointUsage = function ( being ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

    // Identificadores
    var ringOfProtection = being.getAllCardAttachments().find( attachment => attachment instanceof this && attachment.content.effects.isEnabled );

    // Invalida uso do ponto de destino caso ente passado não tenha um anel da proteção cujo efeito esteja ativo
    if( !ringOfProtection ) return false;

    // Invalida uso do ponto de destino caso não haja uma parada de destino ou uma parada de combate em andamento
    if( !m.GameMatch.fateBreakExecution && !( m.GameModal.current?.content.body instanceof m.CombatBreakElement ) ) return false;

    // Indica que ponto de destino relativo ao equipamento pode ser usado
    return true;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementRingOfProtection.gridOrder = m.data.cards.items.push( ImplementRingOfProtection ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementRingOfProtection.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementRingOfProtection }
} );
