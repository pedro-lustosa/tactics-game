// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementBellOfRest = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementBellOfRest.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementBellOfRest.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementBellOfRest );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'bell-of-rest';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'one-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementBellOfRest.gridOrder = m.data.cards.items.push( ImplementBellOfRest ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementBellOfRest.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementBellOfRest }
} );
