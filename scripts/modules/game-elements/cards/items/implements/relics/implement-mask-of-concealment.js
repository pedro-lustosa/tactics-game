// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementMaskOfConcealment = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementMaskOfConcealment.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementMaskOfConcealment.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementMaskOfConcealment );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'mask-of-concealment';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'non-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementMaskOfConcealment.gridOrder = m.data.cards.items.push( ImplementMaskOfConcealment ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementMaskOfConcealment.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementMaskOfConcealment }
} );
