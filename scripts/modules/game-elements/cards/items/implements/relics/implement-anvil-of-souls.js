// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementAnvilOfSouls = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementAnvilOfSouls.init( this );

  // Superconstrutor
  m.Implement.call( this, config );

  // Habilita capacidade de convocação a quem usar apetrecho, caso ele ainda não a tenha
  m.events.cardUseEnd.useIn.add( this, this.content.effects.enableSummons, this.content.effects );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementAnvilOfSouls.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementAnvilOfSouls );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'anvil-of-souls';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'heavy';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Habilita capacidade de convocação a dono, caso ele ainda não a tenha
    effects.enableSummons = function ( eventData = {} ) {
      // Identificadores
      var { eventTarget: implement, eventCategory, eventType } = eventData,
          attacher = implement.attacher;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' );

      // Habilitação da capacidade de convocação, se ainda sem
      if( !attacher.summon ) Object.oAssignByCopy( attacher, m.mixinSummonable.call( attacher ) );
    }

    // Atribuição de propriedades de 'coins'
    let coins = implement.coins;

    /// Atual
    coins.current = 100;
  }

  // Ordem inicial das variações desta carta na grade
  ImplementAnvilOfSouls.gridOrder = m.data.cards.items.push( ImplementAnvilOfSouls ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementAnvilOfSouls.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementAnvilOfSouls }
} );
