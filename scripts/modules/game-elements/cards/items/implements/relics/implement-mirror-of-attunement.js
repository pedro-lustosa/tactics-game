// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ImplementMirrorOfAttunement = function ( config = {} ) {
  // Iniciação de propriedades
  ImplementMirrorOfAttunement.init( this );

  // Superconstrutor
  m.Implement.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ImplementMirrorOfAttunement.init = function ( implement ) {
    // Chama 'init' ascendente
    m.Implement.init( implement );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( implement instanceof ImplementMirrorOfAttunement );

    // Atribuição de propriedades iniciais

    /// Nome
    implement.name = 'mirror-of-attunement';

    // Atribuição de propriedades de 'content'
    let content = implement.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'one-handed';
  }

  // Ordem inicial das variações desta carta na grade
  ImplementMirrorOfAttunement.gridOrder = m.data.cards.items.push( ImplementMirrorOfAttunement ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
ImplementMirrorOfAttunement.prototype = Object.create( m.Implement.prototype, {
  // Construtor
  constructor: { value: ImplementMirrorOfAttunement }
} );
