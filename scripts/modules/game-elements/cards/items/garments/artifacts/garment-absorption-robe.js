// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentAbsorptionRobe = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentAbsorptionRobe.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-absorption-robe`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentAbsorptionRobe.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentAbsorptionRobe );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 0,
      // Condutividade
      conductivity: 1,
      // Resistências
      resistances: {
        // Físicas
        blunt: 0, slash: 0, pierce: 0,
        // Elementais
        fire: 0, shock: 0, mana: 0,
        // Astrais
        ether: 0
      }
    }
  }

  // Ordem inicial das variações desta carta na grade
  GarmentAbsorptionRobe.gridOrder = m.data.cards.items.push( GarmentAbsorptionRobe ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentAbsorptionRobe.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentAbsorptionRobe }
} );
