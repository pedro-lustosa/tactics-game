// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentBoneMail = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentBoneMail.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-bone-mail`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentBoneMail.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentBoneMail );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 6,
      // Condutividade
      conductivity: 1,
      // Resistências
      resistances: {
        // Físicas
        blunt: 1, slash: 3, pierce: 0,
        // Elementais
        fire: 0, shock: 0, mana: 0,
        // Astrais
        ether: 1
      }
    };
  }

  // Ordem inicial das variações desta carta na grade
  GarmentBoneMail.gridOrder = m.data.cards.items.push( GarmentBoneMail ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentBoneMail.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentBoneMail }
} );
