// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentManiphileCuirass = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentManiphileCuirass.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-maniphile-cuirass`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentManiphileCuirass.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentManiphileCuirass );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 4,
      // Condutividade
      conductivity: 2,
      // Resistências
      resistances: {
        // Físicas
        blunt: 1, slash: 4, pierce: 3,
        // Elementais
        fire: 0, shock: 0, mana: 1,
        // Astrais
        ether: 0
      }
    };
  }

  // Ordem inicial das variações desta carta na grade
  GarmentManiphileCuirass.gridOrder = m.data.cards.items.push( GarmentManiphileCuirass ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentManiphileCuirass.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentManiphileCuirass }
} );
