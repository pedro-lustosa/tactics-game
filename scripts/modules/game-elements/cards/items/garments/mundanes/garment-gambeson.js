// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentGambeson = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentGambeson.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-gambeson`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentGambeson.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentGambeson );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'ordinary';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 6,
      // Condutividade
      conductivity: 1,
      // Resistências
      resistances: {
        // Físicas
        blunt: 3, slash: 2, pierce: 1,
        // Elementais
        fire: 0, shock: 0, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 20;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentGambeson.gridOrder = m.data.cards.items.push( GarmentGambeson ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentGambeson.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentGambeson }
} );
