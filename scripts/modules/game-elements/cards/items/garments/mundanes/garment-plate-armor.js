// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentPlateArmor = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentPlateArmor.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-plate-armor`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentPlateArmor.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentPlateArmor );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'heavy';

    /// Categoria
    typeset.grade = 'ordinary';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 8,
      // Condutividade
      conductivity: 3,
      // Resistências
      resistances: {
        // Físicas
        blunt: 2, slash: 6, pierce: 5,
        // Elementais
        fire: 2, shock: 2, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 29;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentPlateArmor.gridOrder = m.data.cards.items.push( GarmentPlateArmor ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentPlateArmor.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentPlateArmor }
} );
