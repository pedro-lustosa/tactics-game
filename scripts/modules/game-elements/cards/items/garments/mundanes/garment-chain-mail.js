// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentChainMail = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentChainMail.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-chain-mail`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentChainMail.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentChainMail );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'ordinary';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 6,
      // Condutividade
      conductivity: 3,
      // Resistências
      resistances: {
        // Físicas
        blunt: 0, slash: 4, pierce: 2,
        // Elementais
        fire: 0, shock: 1, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 20;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentChainMail.gridOrder = m.data.cards.items.push( GarmentChainMail ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentChainMail.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentChainMail }
} );
