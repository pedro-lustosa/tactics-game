// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentBrigandine = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentBrigandine.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-brigandine`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentBrigandine.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentBrigandine );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'ordinary';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 4,
      // Condutividade
      conductivity: 2,
      // Resistências
      resistances: {
        // Físicas
        blunt: 2, slash: 5, pierce: 4,
        // Elementais
        fire: 1, shock: 0, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 22;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentBrigandine.gridOrder = m.data.cards.items.push( GarmentBrigandine ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentBrigandine.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentBrigandine }
} );
