// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentScaleArmor = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentScaleArmor.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-scale-armor`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentScaleArmor.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentScaleArmor );

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'ordinary';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 5,
      // Condutividade
      conductivity: 2,
      // Resistências
      resistances: {
        // Físicas
        blunt: 1, slash: 4, pierce: 3,
        // Elementais
        fire: 1, shock: 0, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 22;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentScaleArmor.gridOrder = m.data.cards.items.push( GarmentScaleArmor ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentScaleArmor.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentScaleArmor }
} );
