// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Garment = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats, stats: { effectivity } } } = this,
      sizeModifier = typeset.size == 'large' ? 3 : typeset.size == 'medium' ? 2 : 1;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'small', 'medium', 'large' ].includes( typeset.size ) );
    m.oAssert( effectivity.current.coverage >= 0 && effectivity.current.coverage <= 10 );
    m.oAssert( effectivity.current.coverage > 0 || Object.values( effectivity.current.resistances ).every( value => !value ) );
    m.oAssert( effectivity.current.conductivity >= 0 && effectivity.current.conductivity <= 3 );
  }

  // Configurações pré-superconstrutor

  /// Ajusta estatísticas de resistência segundo tamanho do equipamento
  for( let resistance in effectivity.current.resistances ) effectivity.current.resistances[ resistance ] *= sizeModifier;

  /// Ordem da carta em grades
  this.gridOrder = this.constructor.gridOrder + sizeModifier - 1;

  // Superconstrutor
  m.Item.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Garment.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Item.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof Garment );

    // Atribuição de propriedades iniciais

    /// Usa traje
    garment.use = function () {
      // Identificadores
      var [ attacher, attacherGarments, attacherEffectivity ] = [ this.attacher, this.attacher.garments, this.attacher.content.stats.effectivity ],
          [ garmentWeight, garmentEffectivity ] = [ this.content.typeset.weight, this.content.stats.effectivity.current ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.activity == 'active' );
        m.oAssert( this.attacher.isInUse );
      }

      // Encerra função caso equipamento já esteja sendo usado
      if( this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useIn.emit( eventTarget, { useTarget: this } );

      // Identifica se traje alvo é o primário ou secundário
      var rank = ( function () {
        // Caso vinculante esteja sem trajes ou peso de traje alvo seja pesado, ele é o primário
        if( !attacherGarments.primary.source || garmentWeight == 'heavy' ) return 'primary';

        // Caso peso de traje alvo seja leve, ele é o secundário
        if( garmentWeight == 'light' ) return 'secondary';

        // Caso peso de traje já em vinculante seja leve, o novo é o primário
        if( attacherGarments.primary.source.content.typeset.weight == 'light' ) return 'primary';

        // Caso nenhuma das condições tenha sido atendida, ele é o secundário
        return 'secondary';
      } )();

      // Reajusta posição de traje já em alvo para que ele não seja sobrescrito pelo novo
      if( rank == 'primary' && attacherGarments.primary.source ) attacherGarments.secondary = attacherGarments.primary;

      // Adiciona efetividade do traje aos trajes do vinculante
      attacherGarments[ rank ] = Object.assign( {}, garmentEffectivity );

      // Captura traje, na categoria alvo
      attacherGarments[ rank ].source = this;

      // Atualiza condutividade atual de seu vinculante
      attacherEffectivity.conductivity.add( this );

      // Atualiza resistências atuais de seu vinculante
      attacherEffectivity.resistances.add( this );

      // Identifica que equipamento está sendo usado
      this.isInUse = true;

      // Habilita efeito do equipamento
      this.content.effects.enable();

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useIn.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    /// Desusa traje
    garment.disuse = function() {
      // Identificadores
      var [ attacher, attacherGarments, attacherEffectivity ] = [ this.attacher, this.attacher.garments, this.attacher.content.stats.effectivity ],
          garmentEffectivity = this.content.stats.effectivity.current,
          rank = Object.keys( attacherGarments ).find( rank => attacherGarments[ rank ].source == this );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( rank );

      // Encerra função caso equipamento não esteja sendo usado
      if( !this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useOut.emit( eventTarget, { useTarget: this } );

      // Desabilita efeito do equipamento
      this.content.effects.disable();

      // Quando aplicável, limpa dados próprios do equipamento
      this.clearCustomData?.();

      // Remove equipamento de vinculante
      switch( rank ) {
        case 'primary':
          attacherGarments.primary = attacherGarments.secondary;
          attacherGarments.secondary = {};
          break;
        case 'secondary':
          attacherGarments.secondary = {};
      }

      // Atualiza condutividade atual de seu vinculante
      attacherEffectivity.conductivity.remove( this );

      // Atualiza resistências atuais de seu vinculante
      attacherEffectivity.resistances.remove( this );

      // Identifica que equipamento não está mais sendo usado
      this.isInUse = false;

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useOut.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = garment.content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'static';

    /// Equipagem
    typeset.equipage = 'garment';

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = .75;
  }
}

/// Propriedades do protótipo
Garment.prototype = Object.create( m.Item.prototype, {
  // Construtor
  constructor: { value: Garment }
} );
