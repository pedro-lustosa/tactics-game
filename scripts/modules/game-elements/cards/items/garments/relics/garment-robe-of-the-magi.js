// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentRobeOfTheMagi = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentRobeOfTheMagi.init( this );

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentRobeOfTheMagi.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentRobeOfTheMagi );

    // Atribuição de propriedades iniciais

    /// Nome
    garment.name = 'robe-of-the-magi';

    /// Limpa dados próprios do equipamento
    garment.clearCustomData = function () {
      // Identificadores
      var { effects } = this.content;

      // Remove evento para verificar ativação do efeito do equipamento após entrada ou saída de equipamentos
      m.events.cardContentEnd.item.remove( this.attacher, effects.controlEnablement, { context: effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'relic';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 0,
      // Condutividade
      conductivity: 1,
      // Resistências
      resistances: {
        // Físicas
        blunt: 0, slash: 0, pierce: 0,
        // Elementais
        fire: 0, shock: 0, mana: 0,
        // Astrais
        ether: 0
      }
    }

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Indicação de se alcance da magia atualmente modificada foi alterado
    effects.isWithChangedRange = false;

    /// Provoca efeito relativo ao uso do equipamento
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == garment );

      // Adiciona evento para verificar ativação do efeito do equipamento após entrada ou saída de equipamentos
      m.events.cardContentEnd.item.add( garment.attacher, this.controlEnablement, { context: this } );
    }

    /// Controla ativação do efeito
    effects.controlEnablement = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget, item } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'card-content' && [ 'item-in', 'item-out' ].includes( eventType ) && eventTarget == garment.attacher );

      // Não executa função caso equipamento relativo ao evento não tenha sido um traje
      if( !( item instanceof m.Garment ) ) return;

      // Não executa função caso equipamento relativo ao evento tenha sido este próprio traje
      if( item == garment ) return;

      // Caso um novo traje tenha sido equipado, desativa efeito deste traje; do contrário, ativa-o
      return this[ eventType == 'item-in' ? 'disable' : 'enable' ]();
    }

    /// Ativa efeito do equipamento
    effects.enable = function () {
      // Não executa função caso equipamento não esteja em uso
      if( !garment.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return garment;

      // Identificadores
      var attacherGarments = Object.values( garment.attacher.garments ).filter( card => card.source instanceof m.Garment );

      // Não executa função caso vinculante do equipamento tenha outro traje em uso
      if( attacherGarments.some( card => card.source != garment ) ) return false;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( garment );

      // Indica que efeito do equipamento está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( garment );

      // Retorna equipamento
      return garment;
    }

    /// Quando aplicável, aplica modificadores do traje à magia passada
    effects.applyModifiers = function ( action, spell ) {
      // Não executa função caso efeito do traje não esteja ativo
      if( !this.isEnabled ) return;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( spell instanceof m.Spell );
      }

      // Identificadores
      var spellStats = spell.content.stats.current;

      // Indica que magia deve ser potencializada
      spell.isToEmpower = true;

      // Para caso magia tenha custo de fluxo
      if( spellStats.flowCost.toChannel ) {
        // Sinaliza início da mudança de custo de fluxo
        m.events.cardContentStart.flowCost.emit( spell, { changeNumber: -1 } );

        // Aplica alteração à magia alvo
        spellStats.flowCost.toChannel--;

        // Sinaliza fim da mudança de custo de fluxo
        m.events.cardContentEnd.flowCost.emit( spell, { changeNumber: -1 } );
      }

      // Para caso alcance da magia esteja entre R1 e R4
      if( /^R[1-4]$/.test( spellStats.range ) ) {
        // Sinaliza início da mudança de alcance
        m.events.cardContentStart.range.emit( spell, { changeNumber: 1 } );

        // Aplica alteração à magia alvo
        spellStats.range = 'R' + ( Number( spellStats.range.replace( /\D/g, '' ) ) + 1 );

        // Indica que alteração de alcance foi aplicada
        this.isWithChangedRange = true;

        // Sinaliza fim da mudança de alcance
        m.events.cardContentEnd.range.emit( spell, { changeNumber: 1 } );
      }

      // Após a finalização ou cancelamento da ação, programa remoção dos modificadores adicionados
      for( let eventName of [ 'complete', 'cancel' ] ) m.events.actionChangeEnd[ eventName ].add( action, this.dropModifiers, { context: this } );
    }

    /// Remove modificadores assumidamente aplicados à magia passada
    effects.dropModifiers = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: action, actionTarget: spell } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'action-change' && [ 'finish', 'abort', 'prevent', 'cancel' ].includes( eventType ) && action instanceof m.ActionChannel );
        m.oAssert( spell instanceof m.Spell );
      }

      // Identificadores pós-validação
      var spellStats = spell.content.stats.current;

      // Força indicação de que magia não deve mais ser empoderada
      spell.isToEmpower = false;

      // Para caso tenha havido uma modificação de alcance
      if( this.isWithChangedRange ) {
        // Sinaliza início da mudança de alcance
        m.events.cardContentStart.range.emit( spell, { changeNumber: -1 } );

        // Desfaz alteração aplicada à magia alvo
        spellStats.range = 'R' + ( Number( spellStats.range.replace( /\D/g, '' ) ) - 1 );

        // Indica que alteração de alcance não está mais vigente
        this.isWithChangedRange = false;

        // Sinaliza fim da mudança de alcance
        m.events.cardContentEnd.range.emit( spell, { changeNumber: -1 } );
      }

      // Remove eventos para remoção dos modificadores adicionados à ação
      for( let eventName of [ 'complete', 'cancel' ] ) m.events.actionChangeEnd[ eventName ].remove( action, this.dropModifiers, { context: this } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 80;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 0;
  }

  // Verifica se efeitos deste traje podem ser aplicados ante a ação e a magia passadas
  GarmentRobeOfTheMagi.checkEffectApplication = function ( action, spell ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( action instanceof m.ActionChannel );
      m.oAssert( spell instanceof m.Spell );
    }

    // Identificadores
    var committerGarments = Object.values( action.committer.garments ?? {} ),
        robeOfTheMagi = committerGarments.find( garment => garment.source instanceof m.GarmentRobeOfTheMagi )?.source;

    // Quando existente, aplica efeito do traje 'Manto dos Arcanistas' que o acionante da ação esteja usando
    return robeOfTheMagi?.content.effects.applyModifiers( action, spell );
  }

  // Ordem inicial das variações desta carta na grade
  GarmentRobeOfTheMagi.gridOrder = m.data.cards.items.push( GarmentRobeOfTheMagi ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentRobeOfTheMagi.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentRobeOfTheMagi }
} );
