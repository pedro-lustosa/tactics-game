// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GarmentArmorOfTheHeroes = function ( config = {} ) {
  // Iniciação de propriedades
  GarmentArmorOfTheHeroes.init( this );

  // Superconstrutor
  m.Garment.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GarmentArmorOfTheHeroes.init = function ( garment ) {
    // Chama 'init' ascendente
    m.Garment.init( garment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( garment instanceof GarmentArmorOfTheHeroes );

    // Atribuição de propriedades iniciais

    /// Nome
    garment.name = 'armor-of-the-heroes';

    // Atribuição de propriedades de 'content'
    let content = garment.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'heavy';

    /// Categoria
    typeset.grade = 'relic';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Cobertura
      coverage: 10,
      // Condutividade
      conductivity: 0,
      // Resistências
      resistances: {
        // Físicas
        blunt: 4, slash: 9, pierce: 8,
        // Elementais
        fire: 4, shock: 2, mana: 0,
        // Astrais
        ether: 0
      }
    };

    // Atribuição de propriedades de 'coins'
    let coins = garment.coins;

    /// Atual
    coins.current = 80;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 0;
  }

  // Ordem inicial das variações desta carta na grade
  GarmentArmorOfTheHeroes.gridOrder = m.data.cards.items.push( GarmentArmorOfTheHeroes ) + ( m.data.cards.items.length - 1 ) * 2;
}

/// Propriedades do protótipo
GarmentArmorOfTheHeroes.prototype = Object.create( m.Garment.prototype, {
  // Construtor
  constructor: { value: GarmentArmorOfTheHeroes }
} );
