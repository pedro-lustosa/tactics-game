// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponDesolator = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponDesolator.init( this );

  // Superconstrutor
  m.WeaponAxe.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponDesolator.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponAxe.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponDesolator );

    // Atribuição de propriedades iniciais

    /// Nome
    weapon.name = 'desolator';

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverCut( {
          modifier: 1,
          recovery: 2,
          damage: { slash: 10 },
          penetration: 2,
          range: 'M3',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 1,
          recovery: 1,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 1,
          source: weapon
        } )
      ]
    };
  }

  // Ordem inicial das variações desta carta na grade
  WeaponDesolator.gridOrder = m.data.cards.items.push( WeaponDesolator ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponDesolator.prototype = Object.create( m.WeaponAxe.prototype, {
  // Construtor
  constructor: { value: WeaponDesolator }
} );
