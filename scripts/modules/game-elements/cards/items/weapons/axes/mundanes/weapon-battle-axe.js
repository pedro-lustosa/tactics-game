// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponBattleAxe = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponBattleAxe.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-battle-axe`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponAxe.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponBattleAxe.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponAxe.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponBattleAxe );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverCut( {
          modifier: 0,
          recovery: 1,
          damage: { slash: 5 },
          penetration: 1,
          range: 'M1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: -1,
          recovery: 1,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 11;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponBattleAxe.gridOrder = m.data.cards.items.push( WeaponBattleAxe ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponBattleAxe.prototype = Object.create( m.WeaponAxe.prototype, {
  // Construtor
  constructor: { value: WeaponBattleAxe }
} );
