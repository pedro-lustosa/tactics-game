// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponPoleaxe = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponPoleaxe.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-poleaxe`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponAxe.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponPoleaxe.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponAxe.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponPoleaxe );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverCut( {
          modifier: 1,
          recovery: 3,
          damage: { slash: 4 },
          penetration: 1,
          range: 'M3',
          source: weapon
        } ),
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 3,
          damage: { pierce: 3 },
          penetration: 0,
          range: 'M3',
          source: weapon
        } ),
        new m.ManeuverBash( {
          modifier: 0,
          recovery: 3,
          damage: { blunt: 3 },
          penetration: 0,
          range: 'M3',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 1,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 2,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 15;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponPoleaxe.gridOrder = m.data.cards.items.push( WeaponPoleaxe ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponPoleaxe.prototype = Object.create( m.WeaponAxe.prototype, {
  // Construtor
  constructor: { value: WeaponPoleaxe }
} );
