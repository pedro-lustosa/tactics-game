// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponSmasherMaul = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponSmasherMaul.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-smasher-maul`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.WeaponMace.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponSmasherMaul.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponMace.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponSmasherMaul );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'heavy';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: -2,
          recovery: 0,
          damage: { blunt: 5 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 0,
          recovery: 0,
          source: weapon
        } )
      ]
    };
  }

  // Ordem inicial das variações desta carta na grade
  WeaponSmasherMaul.gridOrder = m.data.cards.items.push( WeaponSmasherMaul ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponSmasherMaul.prototype = Object.create( m.WeaponMace.prototype, {
  // Construtor
  constructor: { value: WeaponSmasherMaul }
} );
