// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponMace = function ( config = {} ) {
  // Superconstrutor
  m.Weapon.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponMace.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Weapon.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponMace );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = weapon.content.typeset.role;

    /// Nome
    role.name = 'mace';

    /// Tipo
    role.type = 'melee';
  }
}

/// Propriedades do protótipo
WeaponMace.prototype = Object.create( m.Weapon.prototype, {
  // Construtor
  constructor: { value: WeaponMace }
} );
