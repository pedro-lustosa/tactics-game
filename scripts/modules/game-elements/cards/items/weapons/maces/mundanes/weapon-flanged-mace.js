// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponFlangedMace = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponFlangedMace.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-flanged-mace`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponMace.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponFlangedMace.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponMace.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponFlangedMace );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: 0,
          recovery: 2,
          damage: { blunt: 3 },
          penetration: 0,
          range: 'M1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 0,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 1,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Realiza configurações da arma em uma parada de combate sua
    effects.configOnCombatBreak = function ( combatBreak ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( [ 'attack', 'defense' ].some( key => combatBreak[ key ]?.maneuver.source == weapon ) );
      }

      // Identificadores
      var { attack, defense, attacker, defender } = combatBreak,
          { maneuver } = attack,
          weaponSize = weapon.content.typeset.size;

      // Não executa função caso esta arma não seja a atacante na parada de combate
      if( maneuver.source != weapon ) return;

      // Não executa função caso manobra do ataque não seja 'Bater'
      if( !( maneuver instanceof m.ManeuverBash ) ) return;

      // Identificadores para a definição da penetração da arma
      let bluntResistance = getBluntResistances(),
          maxPenetration = weaponSize == 'large' ? 6 : weaponSize == 'medium' ? 4 : 2,
          formerPenetration = maneuver.penetration;

      // Ajusta penetração da arma para que equivala ao menor valor entre a máxima e a de fontes pertinentes, e ao maior valor entre esse resultado e a atual
      maneuver.penetration = Math.max( Math.min( maxPenetration, bluntResistance ), formerPenetration );

      // Atualiza visualizador de dano visível da parada de combate
      combatBreak.updateDamagePreview();

      // Adiciona evento para restaurar penetração da arma ao valor original ao fim da parada de combate
      m.events.breakEnd.combat.add( m.GameMatch.current, () => maneuver.penetration = formerPenetration, { once: true } );

      // Funções

      /// Retorna soma das resistências contra concussão relevantes do defensor
      function getBluntResistances() {
        // Identificadores
        var resistanceSum = 0,
            targetGarments = Object.values( defender.garments ?? {} ).filter(
              garment => [ 'medium', 'heavy' ].includes( garment.source?.content.typeset.weight )
            );

        // Adiciona às resistências pertinentes resistências de trajes de peso médio e pesado
        resistanceSum += targetGarments.reduce( ( accumulator, current ) => accumulator += current.resistances?.blunt || 0, 0 );

        // Caso defensor seja um golem, adiciona às resistências pertinentes sua resistência natural
        if( defender instanceof m.Golem ) resistanceSum += defender.content.stats.effectivity.resistances.natural.current.blunt;

        // Retorna soma das resistências contra concussão pertinentes
        return resistanceSum;
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 8;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponFlangedMace.gridOrder = m.data.cards.items.push( WeaponFlangedMace ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponFlangedMace.prototype = Object.create( m.WeaponMace.prototype, {
  // Construtor
  constructor: { value: WeaponFlangedMace }
} );
