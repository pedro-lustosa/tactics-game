// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponGada = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponGada.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-gada`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponMace.call( this, config );

  // Eventos

  /// Para atordoar alvo caso seja infligido um dano crítico via 'Bater'
  m.events.damageChangeEnd.inflict.add( this, this.content.effects.stunTarget, { context: this.content.effects } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponGada.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponMace.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponGada );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'heavy';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: -2,
          recovery: 0,
          damage: { blunt: 5 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 0,
          recovery: 0,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atordoa alvo de ataques 'Bater'
    effects.stunTarget = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget, mainDamage, damageTarget } = eventData,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'damage-change' && eventType == 'inflict' && eventTarget == weapon && mainDamage instanceof m.DamageBlunt );

      // Não executa função caso não se esteja no período do combate
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return;

      // Não executa função caso manobra que provocou o dano não tenha sido 'Bater'
      if( !( mainDamage.maneuver instanceof m.ManeuverBash ) ) return;

      // Não executa função caso dano não tenha sido um crítico
      if( !mainDamage.isCritical ) return;

      // Não executa função caso alvo do dano não seja um ente biótico
      if( !( damageTarget instanceof m.BioticBeing ) ) return;

      // Atordoa alvo do dano
      new m.ConditionStunned().apply( damageTarget, matchFlow.parity );
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 11;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponGada.gridOrder = m.data.cards.items.push( WeaponGada ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponGada.prototype = Object.create( m.WeaponMace.prototype, {
  // Construtor
  constructor: { value: WeaponGada }
} );
