// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponWarHammer = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponWarHammer.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-war-hammer`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponMace.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponWarHammer.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponMace.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponWarHammer );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: -1,
          recovery: 1,
          damage: { blunt: 4 },
          penetration: 0,
          range: 'M1',
          source: weapon
        } ),
        new m.ManeuverThrust( {
          modifier: -2,
          recovery: 0,
          damage: { pierce: 2 },
          penetration: 5,
          range: 'M1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: -1,
          recovery: 1,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 0,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito do equipamento
    effects.currentExecution = null;

    /// Realiza configurações da arma em uma parada de combate sua
    effects.configOnCombatBreak = function ( combatBreak ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( [ 'attack', 'defense' ].some( key => combatBreak[ key ]?.maneuver.source == weapon ) );
      }

      // Identificadores
      var { attack, defense, attacker, defender } = combatBreak,
          { maneuver } = attack,
          weaponSize = weapon.content.typeset.size;

      // Não executa função caso esta arma não seja a atacante na parada de combate
      if( maneuver.source != weapon ) return;

      // Não executa função caso manobra do ataque não seja 'Bater'
      if( !( maneuver instanceof m.ManeuverBash ) ) return;

      // Identificadores para a definição da penetração da arma
      let bluntResistance = getBluntResistances(),
          maxPenetration = weaponSize == 'large' ? 6 : weaponSize == 'medium' ? 4 : 2,
          formerPenetration = maneuver.penetration;

      // Ajusta penetração da arma para que equivala ao menor valor entre a máxima e a de fontes pertinentes, e ao maior valor entre esse resultado e a atual
      maneuver.penetration = Math.max( Math.min( maxPenetration, bluntResistance ), formerPenetration );

      // Atualiza visualizador de dano visível da parada de combate
      combatBreak.updateDamagePreview();

      // Adiciona evento para restaurar penetração da arma ao valor original ao fim da parada de combate
      m.events.breakEnd.combat.add( m.GameMatch.current, () => maneuver.penetration = formerPenetration, { once: true } );

      // Funções

      /// Retorna soma das resistências contra concussão relevantes do defensor
      function getBluntResistances() {
        // Identificadores
        var resistanceSum = 0,
            targetGarments = Object.values( defender.garments ?? {} ).filter(
              garment => [ 'medium', 'heavy' ].includes( garment.source?.content.typeset.weight )
            );

        // Adiciona às resistências pertinentes resistências de trajes de peso médio e pesado
        resistanceSum += targetGarments.reduce( ( accumulator, current ) => accumulator += current.resistances?.blunt || 0, 0 );

        // Caso defensor seja um golem, adiciona às resistências pertinentes sua resistência natural
        if( defender instanceof m.Golem ) resistanceSum += defender.content.stats.effectivity.resistances.natural.current.blunt;

        // Retorna soma das resistências contra concussão pertinentes
        return resistanceSum;
      }
    }

    /// Verifica possibilidade de afastamento de uma arma atacante
    effects.checkWeaponRemoval = function ( combatBreak = {} ) {
      // Identificadores
      var { attack, defender } = combatBreak,
          actionAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( attack.maneuver.source == weapon );
        m.oAssert( actionAttack );
        m.oAssert( !this.currentExecution );
      }

      // Executa efeito
      ( this.currentExecution = executeEffect() ).next();

      // Funções

      /// Para execução deste efeito
      function * executeEffect() {
        // Encerra execução caso ataque usado não tenha sido 'Perfurar'
        if( !( attack.maneuver instanceof m.ManeuverThrust ) ) return effects.currentExecution = null;

        // Encerra execução caso não haja pontos de ataque a serem infligidos
        if( !combatBreak.gaugeInflictedAttack() ) return effects.currentExecution = null;

        // Aguarda fim da ação para prosseguir execução do efeito
        yield m.events.actionChangeEnd.complete.add( actionAttack, () => effects.currentExecution.next(), { once: true } );

        // Encerra execução caso alvo do ataque tenha sido removido
        if( [ 'withdrawn', 'ended', 'unlinked' ].includes( defender.activity ) ) return effects.currentExecution = null;

        // Remove esta arma
        effects.removeWeapon();

        // Encerra execução do efeito
        effects.currentExecution = null;
      }
    }

    /// Remove esta arma, por força de seu efeito
    effects.removeWeapon = function () {
      // Identificadores
      var isOpponent = m.GamePlayer.current == weapon.getRelationships().opponent,
          weaponOwner = weapon.owner;

      // Não executa função caso a arma já tenha sido removida
      if( [ 'withdrawn', 'ended', 'unlinked' ].includes( weapon.activity ) ) return;

      // Afasta a arma
      weapon.inactivate( 'withdrawn' );

      // Notifica usuário sobre afastamento da arma
      m.noticeBar.show(
        m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner } ),
        !m.GameMatch.current.isSimulation && isOpponent ? 'green' : 'red'
      );

      // Adiciona entrada de registro sobre o efeito da arma
      addLogEntry: {
        // Identificadores
        let match = m.GameMatch.current,
            flowChain = match.flow.toChain(),
            logData = {
              name: weapon.name,
              type: 'card-effect',
              description: m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner, isFull: true } )
            };

        // Indica início do registro do evento
        m.events.logChangeStart.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );

        // Atualiza arranjo de eventos do estágio em que se estiver
        match.log.body[ flowChain ].events.push( logData );

        // Indica fim do registro do evento
        m.events.logChangeEnd.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 8;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponWarHammer.gridOrder = m.data.cards.items.push( WeaponWarHammer ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponWarHammer.prototype = Object.create( m.WeaponMace.prototype, {
  // Construtor
  constructor: { value: WeaponWarHammer }
} );
