// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponSpikedClub = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponSpikedClub.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-spiked-club`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponMace.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponSpikedClub.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponMace.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponSpikedClub );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: 0,
          recovery: 2,
          damage: { blunt: 2, pierce: 1 },
          penetration: 0,
          range: 'M1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 0,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 1,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 7;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponSpikedClub.gridOrder = m.data.cards.items.push( WeaponSpikedClub ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponSpikedClub.prototype = Object.create( m.WeaponMace.prototype, {
  // Construtor
  constructor: { value: WeaponSpikedClub }
} );
