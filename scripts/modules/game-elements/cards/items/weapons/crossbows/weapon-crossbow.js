// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponCrossbow = function ( config = {} ) {
  // Superconstrutor
  m.Weapon.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponCrossbow.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Weapon.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponCrossbow );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = weapon.content.typeset.role;

    /// Nome
    role.name = 'crossbow';

    /// Tipo
    role.type = 'ranged';
  }
}

/// Propriedades do protótipo
WeaponCrossbow.prototype = Object.create( m.Weapon.prototype, {
  // Construtor
  constructor: { value: WeaponCrossbow }
} );
