// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponDefaultCrossbow = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponDefaultCrossbow.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-crossbow`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponCrossbow.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponDefaultCrossbow.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponCrossbow.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponDefaultCrossbow );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverShoot( {
          modifier: 0,
          recovery: 0,
          damage: { pierce: 2 },
          penetration: 2,
          range: 'R2',
          source: weapon
        } )
      ],
      // Defesas
      defenses: []
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 16;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponDefaultCrossbow.gridOrder = m.data.cards.items.push( WeaponDefaultCrossbow ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponDefaultCrossbow.prototype = Object.create( m.WeaponCrossbow.prototype, {
  // Construtor
  constructor: { value: WeaponDefaultCrossbow }
} );
