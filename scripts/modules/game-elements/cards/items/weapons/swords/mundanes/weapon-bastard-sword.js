// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponBastardSword = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponBastardSword.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-bastard-sword`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSword.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponBastardSword.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSword.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponBastardSword );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverCut( {
          modifier: 1,
          recovery: 3,
          damage: { slash: 5 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } ),
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 2,
          damage: { pierce: 3 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 1,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 13;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponBastardSword.gridOrder = m.data.cards.items.push( WeaponBastardSword ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponBastardSword.prototype = Object.create( m.WeaponSword.prototype, {
  // Construtor
  constructor: { value: WeaponBastardSword }
} );
