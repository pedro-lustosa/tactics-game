// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponFalchion = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponFalchion.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-falchion`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSword.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponFalchion.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSword.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponFalchion );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverCut( {
          modifier: 0,
          recovery: 5,
          damage: { slash: 3 },
          penetration: 0,
          range: 'M1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 1,
          recovery: 4,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 3,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 9;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponFalchion.gridOrder = m.data.cards.items.push( WeaponFalchion ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponFalchion.prototype = Object.create( m.WeaponSword.prototype, {
  // Construtor
  constructor: { value: WeaponFalchion }
} );
