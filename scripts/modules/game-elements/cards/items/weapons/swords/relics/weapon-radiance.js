// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponRadiance = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponRadiance.init( this );

  // Superconstrutor
  m.WeaponSword.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponRadiance.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSword.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponRadiance );

    // Atribuição de propriedades iniciais

    /// Nome
    weapon.name = 'radiance';

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Tamanho
    typeset.size = 'medium';

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'relic';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
      new m.ManeuverCut( {
          modifier: 1,
          recovery: 3,
          damage: { slash: 5 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } ),
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 2,
          damage: { pierce: 3 },
          penetration: 0,
          range: 'M2',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 0,
          recovery: 1,
          source: weapon
        } )
      ]
    };
  }

  // Ordem inicial das variações desta carta na grade
  WeaponRadiance.gridOrder = m.data.cards.items.push( WeaponRadiance ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponRadiance.prototype = Object.create( m.WeaponSword.prototype, {
  // Construtor
  constructor: { value: WeaponRadiance }
} );
