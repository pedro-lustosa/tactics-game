// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponShortbow = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponShortbow.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-shortbow`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponBow.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponShortbow.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponBow.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponShortbow );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverShoot( {
          modifier: 0,
          recovery: 4,
          damage: { pierce: 1 },
          penetration: 0,
          range: 'R0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: []
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 12;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponShortbow.gridOrder = m.data.cards.items.push( WeaponShortbow ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponShortbow.prototype = Object.create( m.WeaponBow.prototype, {
  // Construtor
  constructor: { value: WeaponShortbow }
} );
