// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponLongbow = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponLongbow.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-longbow`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponBow.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponLongbow.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponBow.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponLongbow );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverShoot( {
          modifier: 0,
          recovery: 3,
          damage: { pierce: 2 },
          penetration: 1,
          range: 'R1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: []
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 16;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponLongbow.gridOrder = m.data.cards.items.push( WeaponLongbow ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponLongbow.prototype = Object.create( m.WeaponBow.prototype, {
  // Construtor
  constructor: { value: WeaponLongbow }
} );
