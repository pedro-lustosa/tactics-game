// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponBow = function ( config = {} ) {
  // Superconstrutor
  m.Weapon.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponBow.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Weapon.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponBow );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = weapon.content.typeset.role;

    /// Nome
    role.name = 'bow';

    /// Tipo
    role.type = 'ranged';
  }
}

/// Propriedades do protótipo
WeaponBow.prototype = Object.create( m.Weapon.prototype, {
  // Construtor
  constructor: { value: WeaponBow }
} );
