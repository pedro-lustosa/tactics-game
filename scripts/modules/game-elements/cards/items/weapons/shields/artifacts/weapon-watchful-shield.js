// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponWatchfulShield = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponWatchfulShield.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-watchful-shield`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.WeaponShield.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponWatchfulShield.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponShield.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponWatchfulShield );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: -1,
          recovery: 0,
          damage: { blunt: 2 },
          penetration: 0,
          range: 'M0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 2,
          recovery: 1,
          source: weapon
        } ),
        new m.ManeuverCover( {
          modifier: -2,
          recovery: 0,
          source: weapon
        } )
      ]
    };
  }

  // Ordem inicial das variações desta carta na grade
  WeaponWatchfulShield.gridOrder = m.data.cards.items.push( WeaponWatchfulShield ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponWatchfulShield.prototype = Object.create( m.WeaponShield.prototype, {
  // Construtor
  constructor: { value: WeaponWatchfulShield }
} );
