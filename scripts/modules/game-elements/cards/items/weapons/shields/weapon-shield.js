// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponShield = function ( config = {} ) {
  // Superconstrutor
  m.Weapon.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponShield.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Weapon.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponShield );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = weapon.content.typeset.role;

    /// Nome
    role.name = 'shield';

    /// Tipo
    role.type = 'melee';
  }
}

/// Propriedades do protótipo
WeaponShield.prototype = Object.create( m.Weapon.prototype, {
  // Construtor
  constructor: { value: WeaponShield }
} );
