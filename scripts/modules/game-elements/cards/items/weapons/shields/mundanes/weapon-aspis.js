// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponAspis = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponAspis.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-aspis`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponShield.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponAspis.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponShield.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponAspis );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'heavy';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: -2,
          recovery: 0,
          damage: { blunt: 2 },
          penetration: 0,
          range: 'M0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 1,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 2,
          recovery: 0,
          source: weapon
        } ),
        new m.ManeuverCover( {
          modifier: -2,
          recovery: 0,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 9;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponAspis.gridOrder = m.data.cards.items.push( WeaponAspis ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponAspis.prototype = Object.create( m.WeaponShield.prototype, {
  // Construtor
  constructor: { value: WeaponAspis }
} );
