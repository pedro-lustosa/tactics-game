// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponBuckler = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponBuckler.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-buckler`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponShield.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponBuckler.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponShield.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponBuckler );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: 0,
          recovery: 0,
          damage: { blunt: 1 },
          penetration: 0,
          range: 'M0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: -1,
          recovery: 3,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: -1,
          recovery: 2,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 3;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponBuckler.gridOrder = m.data.cards.items.push( WeaponBuckler ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponBuckler.prototype = Object.create( m.WeaponShield.prototype, {
  // Construtor
  constructor: { value: WeaponBuckler }
} );
