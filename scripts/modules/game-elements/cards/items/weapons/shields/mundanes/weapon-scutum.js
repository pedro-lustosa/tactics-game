// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponScutum = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponScutum.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-scutum`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponShield.call( this, config );

  // Eventos

  /// Para zerar pontos de defesa efetivos já gastos após arma ser removida
  m.events.cardActivityEnd.removed.add( this, this.clearCustomData );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponScutum.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponShield.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponScutum );

    // Atribuição de propriedades iniciais

    /// Limpa dados próprios da carta
    weapon.clearCustomData = function () {
      // Zera pontos de defesa efetivos já gastos
      this.content.effects.spentDefensePoints = 0;
    }

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Empunhadura
    typeset.wielding = 'one-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverBash( {
          modifier: 0,
          recovery: 0,
          damage: { blunt: 2 },
          penetration: 0,
          range: 'M0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 1,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 2,
          source: weapon
        } ),
        new m.ManeuverCover( {
          modifier: 0,
          recovery: 0,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Registro de pontos efetivos de defesa usados contra ataques cortantes
    effects.spentDefensePoints = 0;

    /// Registra pontos de defesa usados
    effects.withstandDamage = function ( combatBreak ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( combatBreak.defense?.maneuver.source == weapon );
      }

      // Identificadores
      var { defense, damages } = combatBreak,
          mainDamage = damages[ 0 ];

      // Não executa função caso defesa usada não tenha sido 'Bloquear'
      if( !( defense.maneuver instanceof m.ManeuverBlock ) ) return;

      // Não executa função caso dano principal não tenha sido um cortante
      if( !( mainDamage instanceof m.DamageSlash ) ) return;

      // Captura o menor valor entre pontos de defesa e de ataque usados
      let { attack: attackPoints, defense: defensePoints } = combatBreak.getAssignedPoints(),
          effectiveDefensePoints = Math.min( attackPoints, defensePoints );

      // Caso não haja pontos efetivos de defesa, encerra função
      if( effectiveDefensePoints <= 0 ) return;

      // Registra pontos efetivos de defesa
      this.spentDefensePoints += effectiveDefensePoints;

      // Caso pontos efetivos de defesa já usados sejam iguais ou maiores que 10, programa afastamento da arma
      if( this.spentDefensePoints >= 10 ) m.events.breakEnd.combat.add( m.GameMatch.current, this.removeWeapon, { once: true } );
    }

    /// Remove esta arma, por força de seu efeito
    effects.removeWeapon = function () {
      // Identificadores
      var isOpponent = m.GamePlayer.current == weapon.getRelationships().opponent,
          weaponOwner = weapon.owner;

      // Não executa função caso a arma já tenha sido removida
      if( [ 'withdrawn', 'ended', 'unlinked' ].includes( weapon.activity ) ) return;

      // Afasta a arma
      weapon.inactivate( 'withdrawn' );

      // Notifica usuário sobre afastamento da arma
      m.noticeBar.show(
        m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner } ),
        !m.GameMatch.current.isSimulation && isOpponent ? 'green' : 'red'
      );

      // Adiciona entrada de registro sobre o efeito da arma
      addLogEntry: {
        // Identificadores
        let match = m.GameMatch.current,
            flowChain = match.flow.toChain(),
            logData = {
              name: weapon.name,
              type: 'card-effect',
              description: m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner, isFull: true } )
            };

        // Indica início do registro do evento
        m.events.logChangeStart.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );

        // Atualiza arranjo de eventos do estágio em que se estiver
        match.log.body[ flowChain ].events.push( logData );

        // Indica fim do registro do evento
        m.events.logChangeEnd.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 10;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponScutum.gridOrder = m.data.cards.items.push( WeaponScutum ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponScutum.prototype = Object.create( m.WeaponShield.prototype, {
  // Construtor
  constructor: { value: WeaponScutum }
} );
