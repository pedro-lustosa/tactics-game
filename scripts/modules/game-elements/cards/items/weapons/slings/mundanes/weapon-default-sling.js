// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponDefaultSling = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponDefaultSling.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-sling`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponDefaultSling.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSling.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponDefaultSling );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverShoot( {
          modifier: 0,
          recovery: 2,
          damage: { blunt: 2 },
          penetration: 1,
          range: 'R1',
          source: weapon
        } )
      ],
      // Defesas
      defenses: []
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 15;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponDefaultSling.gridOrder = m.data.cards.items.push( WeaponDefaultSling ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponDefaultSling.prototype = Object.create( m.WeaponSling.prototype, {
  // Construtor
  constructor: { value: WeaponDefaultSling }
} );
