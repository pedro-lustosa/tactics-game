// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponSpear = function ( config = {} ) {
  // Superconstrutor
  m.Weapon.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponSpear.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Weapon.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponSpear );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = weapon.content.typeset.role;

    /// Nome
    role.name = 'spear';

    /// Tipo
    role.type = 'melee';
  }
}

/// Propriedades do protótipo
WeaponSpear.prototype = Object.create( m.Weapon.prototype, {
  // Construtor
  constructor: { value: WeaponSpear }
} );
