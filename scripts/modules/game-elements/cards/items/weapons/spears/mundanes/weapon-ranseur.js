// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponRanseur = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponRanseur.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-ranseur`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSpear.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponRanseur.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSpear.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponRanseur );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 4,
          damage: { pierce: 4 },
          penetration: 0,
          range: 'M4',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 0,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 2,
          recovery: 4,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Verifica possibilidade de afastamento de uma arma atacante
    effects.checkWeaponRemoval = function ( combatBreak = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( combatBreak.defense?.maneuver.source == weapon );
      }

      // Identificadores
      var { attack, defense } = combatBreak,
          attackSource = attack.maneuver.source;

      // Não executa função caso defesa usada não tenha sido 'Repelir'
      if( !( defense.maneuver instanceof m.ManeuverRepel ) ) return;

      // Não executa função caso ataque não tenha sido provocado por uma arma
      if( !( attackSource instanceof m.Weapon ) ) return;

      // Captura pontos de ataque a serem infligidos ao fim da parada, desconsiderando seus limites mínimo e máximo
      let attackPoints = combatBreak.gaugeInflictedAttack( true );

      // Caso pontos finais de defesa não tenham sido superiores aos de ataque por ao menos 3 pontos, encerra função
      if( attackPoints > -3 ) return;

      // Programa afastamento da arma atacante
      m.events.breakEnd.combat.add( m.GameMatch.current, () => this.removeWeapon( attackSource ), { context: this, once: true } );
    }

    /// Remove uma arma atacante, por força do efeito desta arma
    effects.removeWeapon = function ( attackingWeapon ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attackingWeapon instanceof m.Weapon );

      // Identificadores
      var isOpponent = m.GamePlayer.current == attackingWeapon.getRelationships().opponent,
          weaponOwner = attackingWeapon.owner;

      // Não executa função caso a arma alvo já tenha sido removida
      if( [ 'withdrawn', 'ended', 'unlinked' ].includes( attackingWeapon.activity ) ) return;

      // Afasta a arma
      attackingWeapon.inactivate( 'withdrawn' );

      // Notifica usuário sobre afastamento da arma
      m.noticeBar.show(
        m.languages.notices.getOutcome( 'weapon-ranseur-effect', { owner: weaponOwner } ),
        !m.GameMatch.current.isSimulation && isOpponent ? 'green' : 'red'
      );

      // Adiciona entrada de registro sobre o efeito da arma
      addLogEntry: {
        // Identificadores
        let match = m.GameMatch.current,
            flowChain = match.flow.toChain(),
            logData = {
              name: weapon.name,
              type: 'card-effect',
              description: m.languages.notices.getOutcome( 'weapon-ranseur-effect', { owner: weaponOwner, isFull: true } )
            };

        // Indica início do registro do evento
        m.events.logChangeStart.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );

        // Atualiza arranjo de eventos do estágio em que se estiver
        match.log.body[ flowChain ].events.push( logData );

        // Indica fim do registro do evento
        m.events.logChangeEnd.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 17;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponRanseur.gridOrder = m.data.cards.items.push( WeaponRanseur ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponRanseur.prototype = Object.create( m.WeaponSpear.prototype, {
  // Construtor
  constructor: { value: WeaponRanseur }
} );
