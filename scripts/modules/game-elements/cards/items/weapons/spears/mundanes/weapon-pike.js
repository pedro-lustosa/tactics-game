// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponPike = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponPike.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-pike`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSpear.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponPike.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSpear.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponPike );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'heavy';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverThrust( {
          modifier: -1,
          recovery: 2,
          damage: { pierce: 5 },
          penetration: 0,
          range: 'M5',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 2,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito do equipamento
    effects.currentExecution = null;

    /// Verifica possibilidade de desferimento de um ataque livre
    effects.checkFreeAttack = function ( combatBreak = {} ) {
      // Identificadores
      var { attack, attacker } = combatBreak,
          actionAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( attack.maneuver.source == weapon );
        m.oAssert( actionAttack );
        m.oAssert( !this.currentExecution );
      }

      // Executa efeito
      ( this.currentExecution = executeEffect() ).next();

      // Funções

      /// Para execução deste efeito
      function * executeEffect() {
        // Encerra execução caso ataque usado não tenha sido 'Perfurar'
        if( !( attack.maneuver instanceof m.ManeuverThrust ) ) return effects.currentExecution = null;

        // Encerra execução caso ataque não seja um dedicado e não tenha sido provocado pelo alcance de uma arma
        if( actionAttack.commitment.type != 'dedicated' && !m.ActionAttack.freeAttackExecution?.name.startsWith( 'melee-range-free-attack' ) )
          return effects.currentExecution = null;

        // Captura dono, controlador e oponente do atacante
        let { owner: attackerOwner, controller: attackerController, opponent: attackerOpponent } = attacker.getRelationships();

        // Encerra execução caso controlador do atacante seja diferente de seu dono
        if( attackerController != attackerOwner ) return effects.currentExecution = null;

        // Aguarda fim da ação para prosseguir execução do efeito, e captura dados do evento de término
        let eventData = yield m.events.actionChangeEnd.complete.add( actionAttack, eventData => effects.currentExecution.next( eventData ), { once: true } );

        // Encerra execução caso ação não tenha sido concluída
        if( eventData.eventType != 'finish' ) return effects.currentExecution = null;

        // Encerra execução caso não haja mais entes do oponente na casa do atacante
        if( !attacker.slot.cards[ attackerOpponent.parity ].length ) return effects.currentExecution = null;

        // Captura ente do controlador do atacante que possa desferir o ataque livre
        let possibleAttacker = getPossibleAttacker();

        // Caso não haja um ente que possa desferir o ataque livre, encerra execução
        if( !possibleAttacker ) return effects.currentExecution = null;

        // Define dados do ataque livre
        let freeAttack = {
          name: `weapon-pike-free-attack-${ possibleAttacker.getMatchId() }`,
          getValidAttacks: ( attack ) => attack.source instanceof WeaponPike && attack instanceof m.ManeuverThrust,
          customValidations: [ {
            validate: ( target ) => target.getRelationships().owner != attackerOwner,
            message: m.languages.notices.getInvalidationText( 'target-must-be-from-opponent' )
          } ],
          modalArguments: [ 'confirm-free-attack-by-pike', { attacker: possibleAttacker } ]
        };

        // Encerra execução do efeito
        effects.currentExecution = null;

        // Programa execução do ataque livre
        return m.ActionAttack.programFreeAttack( possibleAttacker, {
          name: freeAttack.name,
          getValidAttacks: freeAttack.getValidAttacks,
          customValidations: freeAttack.customValidations,
          modalArguments: freeAttack.modalArguments
        } );

        // Funções

        /// Retorna ente que possa desferir o ataque livre
        function getPossibleAttacker() {
          // Identificadores
          var playerBeings = attacker.slot.cards[ attackerController.parity ],
              controlledBeings = playerBeings.filter( card => card.getRelationships().controller == attackerController );

          // Filtra entes que sejam quem desferiu o ataque atual ou que estejam ocupados ou que não tenham um pique em uso
          controlledBeings = controlledBeings.filter( being =>
            being != attacker && being.readiness.status != 'occupied' &&
            being.getAllCardAttachments().some( attachment => attachment instanceof WeaponPike && attachment.isInUse )
          );

          // Ordena entes segundo quantidade de pontos atribuíves para o ataque livre
          controlledBeings.sort( function ( a, b ) {
            // Identificadores
            var maneuverSources = [];

            // Captura fontes de manobras dos entes alvo
            for( let being of [ a, b ] )
              maneuverSources.push( [ 'primary', 'secondary' ].map( rank => being.content.stats.effectivity.maneuvers[ rank ] ).find(
                maneuverSource => maneuverSource.source instanceof WeaponPike
              ) );

            // Ordena entes em ordem decrescente de pontos de ataque ainda disponíveis
            return maneuverSources[ 1 ].currentPoints - maneuverSources[ 0 ].currentPoints;
          } );

          // Quando existente, retorna ente que pode desferir o ataque livre
          return controlledBeings[ 0 ] || null;
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 17;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponPike.gridOrder = m.data.cards.items.push( WeaponPike ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponPike.prototype = Object.create( m.WeaponSpear.prototype, {
  // Construtor
  constructor: { value: WeaponPike }
} );
