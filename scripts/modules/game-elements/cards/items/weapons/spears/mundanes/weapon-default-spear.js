// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponDefaultSpear = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponDefaultSpear.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-spear`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSpear.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponDefaultSpear.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSpear.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponDefaultSpear );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 4,
          damage: { pierce: 4 },
          penetration: 0,
          range: 'M4',
          source: weapon
        } ),
        new m.ManeuverBash( {
          modifier: 2,
          recovery: 1,
          damage: { blunt: 2 },
          penetration: 0,
          range: 'M4',
          source: weapon
        } ),
        new m.ManeuverThrow( {
          modifier: -2,
          recovery: 0,
          damage: { pierce: 4 },
          penetration: 0,
          range: 'R0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 0,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 4,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito do equipamento
    effects.currentExecution = null;

    /// Verifica se esta arma deve ser afastada, por força de seu efeito ante a manobra 'Arremessar'
    effects.checkWeaponRemoval = function ( combatBreak = {} ) {
      // Identificadores
      var { attack, defender } = combatBreak,
          actionAttack = m.GameAction.currents.find( action => action instanceof m.ActionAttack );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( combatBreak instanceof m.CombatBreakElement );
        m.oAssert( combatBreak.currentExecution );
        m.oAssert( attack.maneuver.source == weapon );
        m.oAssert( actionAttack );
        m.oAssert( !this.currentExecution );
      }

      // Executa efeito
      ( this.currentExecution = executeEffect() ).next();

      // Funções

      /// Para execução deste efeito
      function * executeEffect() {
        // Encerra execução caso ataque usado não tenha sido 'Arremessar'
        if( !( attack.maneuver instanceof m.ManeuverThrow ) ) return effects.currentExecution = null;

        // Aguarda fim da ação para prosseguir execução do efeito
        yield m.events.actionChangeEnd.complete.add( actionAttack, () => effects.currentExecution.next(), { once: true } );

        // Encerra execução caso alvo do ataque tenha sido removido
        if( [ 'withdrawn', 'ended', 'unlinked' ].includes( defender.activity ) ) return effects.currentExecution = null;

        // Remove esta arma
        effects.removeWeapon();

        // Encerra execução do efeito
        effects.currentExecution = null;
      }
    }

    /// Remove esta arma, por força de seu efeito
    effects.removeWeapon = function () {
      // Identificadores
      var isOpponent = m.GamePlayer.current == weapon.getRelationships().opponent,
          weaponOwner = weapon.owner;

      // Não executa função caso a arma já tenha sido removida
      if( [ 'withdrawn', 'ended', 'unlinked' ].includes( weapon.activity ) ) return;

      // Afasta a arma
      weapon.inactivate( 'withdrawn' );

      // Notifica usuário sobre afastamento da arma
      m.noticeBar.show(
        m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner } ),
        !m.GameMatch.current.isSimulation && isOpponent ? 'green' : 'red'
      );

      // Adiciona entrada de registro sobre o efeito da arma
      addLogEntry: {
        // Identificadores
        let match = m.GameMatch.current,
            flowChain = match.flow.toChain(),
            logData = {
              name: weapon.name,
              type: 'card-effect',
              description: m.languages.notices.getOutcome( 'weapon-withdrawn-by-its-effect', { owner: weaponOwner, isFull: true } )
            };

        // Indica início do registro do evento
        m.events.logChangeStart.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );

        // Atualiza arranjo de eventos do estágio em que se estiver
        match.log.body[ flowChain ].events.push( logData );

        // Indica fim do registro do evento
        m.events.logChangeEnd.insert.emit( match, { event: weapon, data: logData, flowChain: flowChain } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 16;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponDefaultSpear.gridOrder = m.data.cards.items.push( WeaponDefaultSpear ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponDefaultSpear.prototype = Object.create( m.WeaponSpear.prototype, {
  // Construtor
  constructor: { value: WeaponDefaultSpear }
} );
