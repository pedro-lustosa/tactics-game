// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponHalberd = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponHalberd.init( this );

  // Identificadores
  var { size = 'medium', grade = 'ordinary' } = config,
      { content, content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'ordinary', 'masterpiece' ].includes( grade ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${grade}-${size}-halberd`;

  /// Atribuição do tamanho
  typeset.size = size;

  /// Atribuição da categoria
  typeset.grade = grade;

  // Superconstrutor
  m.WeaponSpear.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponHalberd.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSpear.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponHalberd );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'medium';

    /// Empunhadura
    typeset.wielding = 'two-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 3,
          damage: { pierce: 4 },
          penetration: 0,
          range: 'M4',
          source: weapon
        } ),
        new m.ManeuverCut( {
          modifier: 1,
          recovery: 2,
          damage: { slash: 4 },
          penetration: 1,
          range: 'M4',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 0,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 3,
          source: weapon
        } )
      ]
    };

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Atual
    coins.current = 17;
  }

  // Ordem inicial das variações desta carta na grade
  WeaponHalberd.gridOrder = m.data.cards.items.push( WeaponHalberd ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponHalberd.prototype = Object.create( m.WeaponSpear.prototype, {
  // Construtor
  constructor: { value: WeaponHalberd }
} );
