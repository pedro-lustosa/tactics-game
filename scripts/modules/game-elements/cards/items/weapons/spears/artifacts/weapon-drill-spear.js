// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const WeaponDrillSpear = function ( config = {} ) {
  // Iniciação de propriedades
  WeaponDrillSpear.init( this );

  // Identificadores
  var { size = 'medium' } = config,
      { content, content: { typeset } } = this;

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${size}-drill-spear`;

  /// Atribuição do tamanho
  typeset.size = size;

  // Superconstrutor
  m.WeaponSpear.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  WeaponDrillSpear.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.WeaponSpear.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof WeaponDrillSpear );

    // Atribuição de propriedades de 'content'
    let content = weapon.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Peso
    typeset.weight = 'light';

    /// Categoria
    typeset.grade = 'artifact';

    /// Empunhadura
    typeset.wielding = 'multi-handed';

    // Atribuição de propriedades de 'content.stats.effectivity.current'
    content.stats.effectivity.current = {
      // Ataques
      attacks: [
        new m.ManeuverThrust( {
          modifier: 0,
          recovery: 4,
          damage: { pierce: 4 },
          penetration: 3,
          range: 'M4',
          source: weapon
        } ),
        new m.ManeuverBash( {
          modifier: 2,
          recovery: 1,
          damage: { blunt: 2 },
          penetration: 0,
          range: 'M4',
          source: weapon
        } ),
        new m.ManeuverThrow( {
          modifier: -2,
          recovery: 0,
          damage: { pierce: 4 },
          penetration: 3,
          range: 'R0',
          source: weapon
        } )
      ],
      // Defesas
      defenses: [
        new m.ManeuverBlock( {
          modifier: 2,
          recovery: 0,
          source: weapon
        } ),
        new m.ManeuverRepel( {
          modifier: 1,
          recovery: 4,
          source: weapon
        } )
      ]
    };
  }

  // Ordem inicial das variações desta carta na grade
  WeaponDrillSpear.gridOrder = m.data.cards.items.push( WeaponDrillSpear ) + ( m.data.cards.items.length - 1 ) * 5;
}

/// Propriedades do protótipo
WeaponDrillSpear.prototype = Object.create( m.WeaponSpear.prototype, {
  // Construtor
  constructor: { value: WeaponDrillSpear }
} );
