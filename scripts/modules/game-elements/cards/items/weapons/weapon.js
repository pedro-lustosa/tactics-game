// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Weapon = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats, stats: { effectivity } } } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'one-handed', 'two-handed', 'multi-handed' ].includes( typeset.wielding ) );
    m.oAssert( [ 'small', 'medium', 'large' ].includes( typeset.size ) );
    m.oAssert( [ 'mace', 'sword', 'axe', 'spear', 'shield', 'sling', 'bow', 'crossbow' ].includes( typeset.role.name ) );
    m.oAssert( [ 'melee', 'ranged' ].includes( typeset.role.type ) );
    m.oAssert( typeset.size != 'large' || ![ 'sword', 'bow', 'crossbow' ].includes( typeset.role.name ) );
    m.oAssert( effectivity.current.attacks.every( attack => attack instanceof m.OffensiveManeuver ) );
    m.oAssert( effectivity.current.defenses.every( defense => defense instanceof m.DefensiveManeuver ) );
  }

  // Configurações pré-superconstrutor

  /// Ajuste de estatísticas
  adjustStats: {
    // Identificadores
    let { attacks, defenses } = effectivity.current,
        masterpieceModifier = typeset.grade == 'masterpiece' ? 1 : 0,
        sizeModifier = typeset.size == 'large' ? 3 : typeset.size == 'medium' ? 2 : 1;

    // Itera por arranjos de manobras
    for( let maneuvers of [ attacks, defenses ] ) {
      // Itera por manobras
      for( let maneuver of maneuvers ) {
        // Quando aplicável, adiciona à recuperação modificador de obra-prima
        maneuver.applyModifier( 'recovery', masterpieceModifier, 'masterpiece' );

        // Filtra defesas
        if( maneuver instanceof m.DefensiveManeuver ) continue;

        // Itera por danos
        for( let damageType in maneuver.damage ) {
          // Multiplica dano por modificador de tamanho
          maneuver.damage[ damageType ] *= sizeModifier;

          // Quando aplicável, adiciona a danos físicos modificador de obra-prima
          if( [ 'blunt', 'slash', 'pierce' ].includes( damageType ) ) maneuver.applyModifier( `damage-${ damageType }`, masterpieceModifier, 'masterpiece' );
        }

        // Multiplica penetração pelo modificador de tamanho
        maneuver.penetration *= sizeModifier;

        // Caso manobra alvo não seja "Arremessar", ajusta alcance
        if( !( maneuver instanceof m.ManeuverThrow ) )
          maneuver.range = maneuver.range.replace( /\d/g, '' ) + ( Number( maneuver.range.replace( /\D/g, '' ) ) + sizeModifier );
      }
    }
  }

  /// Ordem da carta em grades
  this.gridOrder = this.constructor.gridOrder + ( function () {
    switch( typeset.size ) {
      case 'small':
        return typeset.grade == 'masterpiece' ? 1 : 0;
      case 'medium':
        return typeset.grade == 'masterpiece' ? 3 : 2;
      case 'large':
        return typeset.grade == 'masterpiece' ? 5 : 4;
    }
  } )();

  // Superconstrutor
  m.Item.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Weapon.init = function ( weapon ) {
    // Chama 'init' ascendente
    m.Item.init( weapon );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( weapon instanceof Weapon );

    // Atribuição de propriedades iniciais

    /// Atribuição da capacidade de usar equipamentos
    Object.oAssignByCopy( weapon, m.mixinEquipable.call( weapon ) );

    /// Usa arma
    weapon.use = function() {
      // Identificadores
      var attacherManeuvers = this.attacher.content.stats.effectivity.maneuvers,
          rank = attacherManeuvers.primary.source && !attacherManeuvers.secondary.source ? 'secondary' : 'primary';

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.activity == 'active' );
        m.oAssert( this.attacher.isInUse );
      }

      // Encerra função caso equipamento já esteja sendo usado
      if( this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useIn.emit( eventTarget, { useTarget: this } );

      // Adiciona equipamento a mãos do vinculante
      this.attacher.hands.put( this, rank );

      // Identifica que equipamento está sendo usado
      this.isInUse = true;

      // Habilita efeito do equipamento
      this.content.effects.enable();

      // Usa equipamentos vinculados
      this.getAllCardAttachments().filter( attachment => attachment instanceof m.Item ).forEach( item => item.use() );

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useIn.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    /// Desusa arma
    weapon.disuse = function() {
      // Encerra função caso equipamento não esteja sendo usado
      if( !this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseStart.useOut.emit( eventTarget, { useTarget: this } );

      // Desusa equipamentos vinculados
      this.getAllCardAttachments().filter( attachment => attachment instanceof m.Item ).forEach( item => item.disuse() );

      // Desabilita efeito do equipamento
      this.content.effects.disable();

      // Quando aplicável, limpa dados próprios do equipamento
      this.clearCustomData?.();

      // Remove equipamento de mãos do vinculante
      this.attacher.hands.release( this );

      // Identifica que equipamento não está mais sendo usado
      this.isInUse = false;

      // Sinaliza fim da mudança de uso
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher ) m.events.cardUseEnd.useOut.emit( eventTarget, { useTarget: this } );

      // Retorna equipamento
      return this;
    }

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = weapon.maxAttachments;

    /// Apetrechos
    maxAttachments.implement = 1;

    /// Magias
    maxAttachments.spell = 1;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = weapon.content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Uso
    typeset.usage = 'dynamic';

    /// Equipagem
    typeset.equipage = 'weapon';

    /// Atuação
    typeset.role = {};

    /// Empunhadura
    typeset.wielding = '';

    // Atribuição de propriedades de 'coins'
    let coins = weapon.coins;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = .75;
  }
}

/// Propriedades do protótipo
Weapon.prototype = Object.create( m.Item.prototype, {
  // Construtor
  constructor: { value: Weapon }
} );
