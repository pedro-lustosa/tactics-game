// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const Item = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats, stats: { effectivity } }, coins } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( this.isToAttach || typeset.usage == 'static' );
    m.oAssert( this.isToAttach || !this.isEmbedded );
    m.oAssert( [ m.Humanoid, m.Weapon ].includes( typeset.attachability ) );
    m.oAssert( [ 'weapon', 'garment', 'implement' ].includes( typeset.equipage ) );
    m.oAssert( [ 'light', 'medium', 'heavy' ].includes( typeset.weight ) );
    m.oAssert( [ 'ordinary', 'masterpiece', 'artifact', 'relic' ].includes( typeset.grade ) );
    m.oAssert( coins.sizeMultiplier >= 0 && coins.sizeMultiplier <= 1 );
  }

  // Configurações pré-superconstrutor

  /// Atribuição da disponibilidade
  typeset.availability = ( function () {
    switch( typeset.grade ) {
      case 'ordinary':
        return 4;
      case 'masterpiece':
        return 3;
      case 'artifact':
        return 2;
      case 'relic':
        return 1;
    }
  } )();

  // Ajusta custo em moedas do equipamento
  adjustCoins: {
    // Apenas executa bloco caso moedas já tenham sido definidas para o equipamento alvo
    if( !coins.current ) break adjustCoins;

    // Identificadores
    let sizeCount = typeset.size == 'large' ? 2 : typeset.size == 'medium' ? 1 : 0,
        sizeIncrement = 0;

    // Define valor adicional de moedas segundo tamanho do equipamento
    for( let i = 0; i < sizeCount; i++ ) sizeIncrement += Math.floor( coins.current * coins.sizeMultiplier );

    // Caso equipamento seja uma arma obra-prima, acrescenta custo adicional pertinente, segundo alcance da arma
    if( this instanceof m.Weapon && typeset.grade == 'masterpiece' ) sizeIncrement += typeset.role.type == 'melee' ? 1 : 2;

    // Efetiva ajuste de moedas
    coins.current += sizeIncrement;
  }

  // Superconstrutor
  m.Card.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição da efetividade, quando aplicável
  if( effectivity ) Object.oAssignByCopy( effectivity.base, effectivity.current, { never: [ 'source' ] } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Item.init = function ( item ) {
    // Chama 'init' ascendente
    m.Card.init( item );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( item instanceof Item );

    // Atribuição de propriedades iniciais

    /// Indica se equipamento deve ser vinculado ao alvo
    item.isToAttach = true;

    /// Indica se equipamento é um vinculado embutido
    item.isEmbedded = false;

    /// Ativa equipamento
    item.activate = function ( activator, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ m.Humanoid, m.Weapon ].some( constructor => activator instanceof constructor ) );
        m.oAssert( this.isToAttach || activator instanceof m.Humanoid );
        m.oAssert( activator.activity == 'active' );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var dataToEmit = {
            formerActivity: this.activity, newActivity: 'active', context: context
          };

      // Para caso equipamento seja vinculável
      if( this.isToAttach ) {
        // Caso equipamento não seja embutido, tenta o equipar ao ativante, ou encerra função caso isso não tenha sido possível
        if( !this.isEmbedded && !activator.equip( this, 'external' ) ) return false;
      }

      // Do contrário, apenas define dono do equipamento como o ativante passado
      else this.owner = activator;

      // Para caso equipamento ainda não esteja ativo
      if( this.activity != 'active' ) {
        // Sinaliza início da mudança de atividade
        m.events.cardActivityStart.active.emit( this, dataToEmit );

        // Altera atividade
        this.activity = 'active';

        // Sinaliza fim da mudança de atividade
        m.events.cardActivityEnd.active.emit( this, dataToEmit );
      }

      // Usa equipamento, se estático
      if( this.content.typeset.usage == 'static' ) this.use()

      // Usa equipamento, se seu ativante for um equipamento em uso
      else if( activator instanceof Item && activator.isInUse ) this.use()

      // Do contrário, avalia se equipamento deve ser usado automaticamente
      else usageCheck: {
        // Encerra avaliação de uso caso seu ativante não tenha mãos disponíveis para o uso do equipamento
        if( !activator.hands?.available ) break usageCheck;

        // Encerra avaliação de uso caso caso haja apenas 1 mão disponível e o equipamento seja bimanual
        if( activator.hands.available == 1 && this.content.typeset.wielding == 'two-handed' ) break usageCheck;

        // Usa equipamento
        this.use();
      }

      // Caso equipamento não seja vinculável, afasta-o
      if( !this.isToAttach ) this.inactivate( 'withdrawn', { triggeringCard: activator } );

      // Retorna equipamento
      return this;
    }

    /// Inativa equipamento
    item.inactivate = function ( newActivity, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'suspended', 'withdrawn', 'ended' ].includes( newActivity ) );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var dataToEmit = {
            formerActivity: this.activity, newActivity: newActivity, context: context
          };

      // Encerra função caso equipamento já esteja com atividade alvo
      if( this.activity == newActivity ) return this;

      // Sinaliza início da mudança de atividade
      m.events.cardActivityStart[ newActivity ].emit( this, dataToEmit );

      // Para caso equipamento seja do baralho
      if( this.deck ) {
        // Quando fora da banca, coloca equipamento na banca
        if( !( this.slot?.environment instanceof m.Table ) ) this.deck.owner.pool.put( this );
      }

      // Para equipamentos ativos, desequipa equipamento, ou apenas o desusa, caso ele seja invinculável ou embutido e a nova atividade seja a suspensa
      if( this.activity == 'active' )
        !this.isToAttach || this.isEmbedded && newActivity == 'suspended' ? this.disuse() : this.attacher.unequip( this );

      // Caso equipamento não seja vinculável, nulifica registro sobre seu dono
      if( !this.isToAttach ) this.owner = null;

      // Altera atividade
      this.activity = newActivity;

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd[ newActivity ].emit( this, dataToEmit );

      // Retorna equipamento
      return this;
    }

    /// Retorna cópia da carta de equipamento
    item.copy = function () {
      // Para armas mundanas
      if( this.content.typeset.equipage == 'weapon' && [ 'ordinary', 'masterpiece' ].includes( this.content.typeset.grade ) )
        return new this.constructor( { size: this.content.typeset.size, grade: this.content.typeset.grade } );

      // Para equipamentos com tamanho
      if( this.content.typeset.size )
        return new this.constructor( { size: this.content.typeset.size } );

      // Para outros casos
      return new this.constructor();
    }

    /// Converte equipamento em um objeto literal
    item.toObject = function () {
      // Identificadores
      var dataObject = {
        constructorName: this.constructor.name, type: 'item'
      };

      // Para armas mundanas, inclui tamanho e categoria
      if( this.content.typeset.equipage == 'weapon' && [ 'ordinary', 'masterpiece' ].includes( this.content.typeset.grade ) )
        Object.assign( dataObject, { size: this.content.typeset.size, grade: this.content.typeset.grade } )

      // Para equipamentos com tamanho, inclui tamanho
      else if( this.content.typeset.size )
        Object.assign( dataObject, { size: this.content.typeset.size } );

      // Retorna objeto literal do equipamento
      return dataObject;
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = item.content.typeset;

    /// Equipagem
    typeset.equipage = '';

    /// Tamanho
    typeset.size = null;

    /// Peso
    typeset.weight = '';

    /// Categoria
    typeset.grade = '';

    // Atribuição de propriedades de 'content.stats'
    let stats = item.content.stats;

    /// Efetividade
    if( [ m.Weapon, m.Garment, m.ImplementBlazingStaff ].some( constructor => item instanceof constructor ) ) stats.effectivity = {};

    // Atribuição de propriedades de 'content.stats.effectivity'
    if( stats.effectivity ) {
      let effectivity = stats.effectivity;

      /// Original
      effectivity.base = {};

      /// Atual
      effectivity.current = {};
    }

    // Atribuição de propriedades de 'coins'
    let coins = item.coins;

    /// Multiplicador devido ao tamanho
    coins.sizeMultiplier = 0;
  }

  // Retorna pontuação de peso do equipamento passado
  Item.getWeightScore = function ( item ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( item instanceof this );

    // Retorna pontuação de peso segundo peso do equipamento
    switch( item.content.typeset.weight ) {
      case 'light':
        return 0;
      case 'medium':
        return 1;
      case 'heavy':
        return 2;
      default:
        return null;
    }
  }

  // Valida vinculante para o equipamento alvo
  Item.validateAttacher = function ( item, attacher ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( item instanceof this );
      m.oAssert( [ m.Humanoid, m.Weapon ].some( constructor => attacher instanceof constructor ) );
    }

    // Identificadores
    var [ itemSize, attacherSize ] = [ item.content.typeset.size, attacher.content.typeset.size ],
        itemAttachability = item.content.typeset.attachability,
        attacherItems = attacher.getAllCardAttachments().filter( attachment => attachment instanceof this && attachment.attacher == attacher );

    // Validação da vinculatividade
    if( !( attacher instanceof itemAttachability ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText(
        'item-invalid-target-type', { targetType: m.languages.keywords.getAttachability( itemAttachability.name, { plural: true } ).toLowerCase() }
      ) );

    // Validação do vinculante
    if( attacherItems.includes( item ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'item-already-attached' ) );

    // Para apetrechos, validação da distinção
    if( item instanceof m.Implement && attacherItems.some( attachment => attachment.content.designation.title == item.content.designation.title ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'implement-with-same-title-already-attached' ) );

    // Validação do tamanho
    if( itemSize && attacherSize != itemSize )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'item-size-must-match' ) );

    // Validação da contagem de peso
    if( attacher.weightCount && attacher.weightCount.current + this.getWeightScore( item ) > attacher.weightCount.max )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'item-over-weight-count' ) );

    // Validação segundo equipagem
    equipageValidation: {
      // Não executa bloco caso equipamento seja invinculável
      if( !item.isToAttach ) break equipageValidation;

      // Identificadores
      let { equipage } = item.content.typeset,
          currentItems = attacherItems.filter( attachment => attachment.content.typeset.equipage == equipage );

      // Impede que equipamento seja equipado caso isso exceda quantidade máxima permitida em alvo
      if( currentItems.length >= attacher.maxAttachments[ equipage ] )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'item-over-max-attachments' ) );

      // Encerra bloco para equipamentos outros que trajes
      if( equipage != 'garment' ) break equipageValidation;

      // Validação entre trajes
      if( currentItems.length ) {
        // Captura traje já equipado
        let currentGarment = currentItems[ 0 ];

        // Impede que novo traje seja equipado caso seu peso seja igual ao de já equipado
        if( currentGarment.content.typeset.weight == item.content.typeset.weight )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'garment-weight-must-be-distinct' ) );
      }
    }

    // Validação própria do equipamento, se aplicável
    if( item.content.effects.validateAttacher && !item.content.effects.validateAttacher( attacher ) ) return false;

    // Indica que vinculante foi validado com sucesso
    return true;
  }
}

/// Propriedades do protótipo
Item.prototype = Object.create( m.Card.prototype, {
  // Construtor
  constructor: { value: Item }
} );
