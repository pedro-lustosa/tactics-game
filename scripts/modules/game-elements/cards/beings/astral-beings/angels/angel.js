// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Angel = function ( config = {} ) {
  // Iniciação de propriedades
  Angel.init( this );

  // Identificadores
  var { summoner } = config,
      channeling = summoner?.content.stats.combativeness.channeling;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( channeling?.paths.voth.skill >= 3 );
    m.oAssert( channeling.paths.voth.polarity == 'positive' );
  }

  // Superconstrutor
  m.AstralBeing.call( this, config );

  // Configurações pós-superconstrutor

  /// Nulifica dono da carta
  this.owner = null;

  /// Definição dos valores originais dos atributos
  Object.oAssignByCopy( this.content.stats.attributes.base, this.content.stats.attributes.current );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Angel.init = function ( astralBeing ) {
    // Chama 'init' ascendente
    m.AstralBeing.init( astralBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( astralBeing instanceof Angel );

    // Atribuição de propriedades iniciais

    /// Nome
    astralBeing.name = 'angel';

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = astralBeing.maxAttachments;

    /// Magias
    maxAttachments.spell = 0;

    // Atribuição de propriedades de 'content'
    let content = astralBeing.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'angel';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Energia
    currentAttributes.energy = Infinity;

    /// Iniciativa
    currentAttributes.initiative = 15;

    /// Poder
    currentAttributes.power = Infinity;

    /// Vontade
    currentAttributes.will = 10;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Elementais
      mana: Infinity,
      // Astrais
      ether: Infinity
    } );
  }

  // Ordem inicial das variações desta carta na grade
  Angel.gridOrder = m.data.cards.creatures.push( Angel ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Angel.prototype = Object.create( m.AstralBeing.prototype, {
  // Construtor
  constructor: { value: Angel }
} );
