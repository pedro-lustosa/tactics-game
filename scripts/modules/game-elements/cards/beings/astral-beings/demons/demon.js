// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Demon = function ( config = {} ) {
  // Iniciação de propriedades
  Demon.init( this );

  // Identificadores
  var { summoner } = config,
      channeling = summoner?.content.stats.combativeness.channeling;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( channeling?.paths.voth.skill >= 2 );
    m.oAssert( channeling.paths.voth.polarity == 'negative' );
  }

  // Superconstrutor
  m.AstralBeing.call( this, config );

  // Configurações pós-superconstrutor
  postConfig: {
    // Identificadores
    let { attributes } = this.content.stats;

    // Definição da vontade
    attributes.base.will = attributes.current.will = summoner.content.stats.attributes.base.will;

    // Definição da energia original
    attributes.base.energy = 20;

    // Definição dos demais valores originais de atributos
    for( let attribute of [ 'initiative', 'power' ] ) attributes.base[ attribute ] = attributes.current[ attribute ];
  }

  // Eventos

  /// Para atualizar habilidades e manobras do ente astral conforme variação de sua energia
  m.events.cardContentEnd.energy.add( this, m.AstralBeing.updateManeuvers, { context: m.AstralBeing } );

  /// Para atualizar danos do ente astral conforme variação de seu poder
  m.events.cardContentEnd.power.add( this, m.AstralBeing.updateDamages, { context: m.AstralBeing } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Demon.init = function ( astralBeing ) {
    // Chama 'init' ascendente
    m.AstralBeing.init( astralBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( astralBeing instanceof Demon );

    // Atribuição de propriedades iniciais

    /// Nome
    astralBeing.name = 'demon';

    // Atribuição de propriedades de 'content'
    let content = astralBeing.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'demon';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Energia
    currentAttributes.energy = 15;

    /// Iniciativa
    currentAttributes.initiative = 10;

    /// Poder
    currentAttributes.power = 5;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Arrebatar
    fightingWeapons.rapture = {
      name: 'rapture',
      raceSkill: 10,
      skill: Math.floor( currentAttributes.energy / 2 ),
      type: 'global'
    };

    /// Reprimir
    fightingWeapons.repress = {
      name: 'repress',
      raceSkill: 5,
      skill: Math.ceil( currentAttributes.energy / 4 ),
      type: 'global'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverRapture( {
        damage: { ether: currentAttributes.power },
        source: astralBeing
      } ),
      new m.ManeuverRepress( {
        damage: { ether: currentAttributes.power },
        source: astralBeing
      } )
    );

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Astrais
      ether: 2
    } );
  }

  // Ordem inicial das variações desta carta na grade
  Demon.gridOrder = m.data.cards.creatures.push( Demon ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Demon.prototype = Object.create( m.AstralBeing.prototype, {
  // Construtor
  constructor: { value: Demon }
} );
