// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Tulpa = function ( config = {} ) {
  // Iniciação de propriedades
  Tulpa.init( this );

  // Identificadores
  var { summoner } = config,
      channeling = summoner?.content.stats.combativeness.channeling,
      matchFlow = m.GameMatch.current.flow;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'metoth', 'voth' ].every( path => channeling?.paths[ path ].skill ) );

  // Superconstrutor
  m.AstralBeing.call( this, config );

  // Configurações pós-superconstrutor
  postConfig: {
    // Identificadores
    let { attributes } = this.content.stats;

    // Definição dos valores originais dos atributos

    /// Energia
    attributes.base.energy = summoner.content.stats.combativeness.channeling.mana.base.metoth;

    /// Outros atributos
    for( let attribute of [ 'initiative', 'power', 'will' ] ) attributes.base[ attribute ] = attributes.current[ attribute ];
  }

  // Eventos

  /// Para restaurar energia em pós-combates
  m.events.flowChangeEnd.finish.add( matchFlow.round.getChild( 'post-combat-break-period' ).getChild( 'being-block' ), this.restoreEnergy, { context: this } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Tulpa.init = function ( astralBeing ) {
    // Chama 'init' ascendente
    m.AstralBeing.init( astralBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( astralBeing instanceof Tulpa );

    // Atribuição de propriedades iniciais

    /// Nome
    astralBeing.name = 'tulpa';

    /// Restaura energia da tulpa
    astralBeing.restoreEnergy = function () {
      // Restaura 2 pontos de energia por vez
      return this.content.stats.attributes.restore( 2, 'energy', { triggeringCard: this } );
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = astralBeing.content.typeset;

    /// Raça
    typeset.race = 'tulpa';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = astralBeing.content.stats.attributes.current;

    /// Energia
    currentAttributes.energy = 2;

    /// Vontade
    currentAttributes.will = 1;
  }

  // Ordem inicial das variações desta carta na grade
  Tulpa.gridOrder = m.data.cards.creatures.push( Tulpa ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Tulpa.prototype = Object.create( m.AstralBeing.prototype, {
  // Construtor
  constructor: { value: Tulpa }
} );
