// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const AstralBeing = function ( config = {} ) {
  // Identificadores
  var { summoner } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( AstralBeing.raceNames.includes( this.content.typeset.race ) );
    m.oAssert( !summoner || summoner instanceof m.Humanoid && summoner.summons );
  }

  // Configurações pré-superconstrutor

  /// Indica se carta é uma ficha
  this.isToken = Boolean( summoner );

  /// Para fichas, indica dono e convocante da carta
  if( summoner ) this.summoner = this.owner = summoner;

  // Superconstrutor
  m.Being.call( this, config );

  // Eventos

  /// Para afastar ente astral caso sua energia ou vontade chegue a 0
  for( let eventName of [ 'energy', 'will' ] )
    m.events.cardContentEnd[ eventName ].add( this, this.removeOnLackOfAttribute );
}

/// Propriedades do construtor
defineProperties: {
  // Nomes das raças
  AstralBeing.raceNames = [ 'tulpa', 'spirit', 'demon', 'angel' ];

  // Iniciação de propriedades
  AstralBeing.init = function ( astralBeing ) {
    // Chama 'init' ascendente
    m.Being.init( astralBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( astralBeing instanceof AstralBeing );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = astralBeing.content.typeset;

    /// Ambiente
    typeset.environment = 'astral';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = astralBeing.content.stats.attributes.current;

    /// Energia
    currentAttributes.energy = 0;

    /// Iniciativa
    currentAttributes.initiative = 0;

    /// Poder
    currentAttributes.power = 0;

    /// Vontade
    currentAttributes.will = 0;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = astralBeing.content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Físicas
      blunt: Infinity, slash: Infinity, pierce: Infinity,
      // Elementais
      fire: Infinity, shock: Infinity
    } );
  }

  // Atualiza habilidades e manobras de ente astral
  AstralBeing.updateManeuvers = function ( eventData = {} ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.Spirit, m.Demon ].some( constructor => eventData.eventTarget instanceof constructor ) );
      m.oAssert( eventData.eventType == 'energy' );
    }

    // Identificadores
    var { eventTarget: astralBeing } = eventData,
        beingWeapons = astralBeing.content.stats.combativeness.fighting.weapons,
        beingManeuvers = astralBeing.content.stats.effectivity.maneuvers;

    // Define novos valores de habilidades

    /// Arrebatar
    beingWeapons.rapture.skill = Math.floor( astralBeing.content.stats.attributes.current.energy / 2 );

    /// Reprimir
    beingWeapons.repress.skill = Math.ceil( astralBeing.content.stats.attributes.current.energy / 4 );

    // Redefine pontos das manobras do ente
    beingManeuvers.setPoints();
  }

  // Atualiza danos de ente astral
  AstralBeing.updateDamages = function ( eventData = {} ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.Spirit, m.Demon ].some( constructor => eventData.eventTarget instanceof constructor ) );
      m.oAssert( eventData.eventType == 'power' );
    }

    // Identificadores
    var { eventTarget: astralBeing } = eventData,
        beingAttacks = astralBeing.content.stats.effectivity.maneuvers.natural.current.attacks;

    // Ajusta dano dos ataques do ente astral
    for( let attack of beingAttacks ) attack.damage.ether = astralBeing.content.stats.attributes.current.power;
  }
}

/// Propriedades do protótipo
AstralBeing.prototype = Object.create( m.Being.prototype, {
  // Construtor
  constructor: { value: AstralBeing }
} );
