// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Spirit = function ( config = {} ) {
  // Iniciação de propriedades
  Spirit.init( this );

  // Identificadores
  var { summoner, source } = config,
      being = summoner || source;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( summoner && !source || !summoner && source );

  // Configurações pré-superconstrutor
  preConfig: {
    // Identificadores
    let spiritAttributes = this.content.stats.attributes.current,
        fightingWeapons = this.content.stats.combativeness.fighting.weapons,
        spiritAttacks = this.content.stats.effectivity.maneuvers.natural.current.attacks;

    // Configura certas propriedades em função de se espírito foi convocado ou não
    summoner ? Spirit.configFromSummoner( this, being ) : Spirit.preConfigFromSource( this, being );

    // Atribuição de habilidades de luta

    /// Arrebatar
    fightingWeapons.rapture = {
      name: 'rapture',
      raceSkill: 10,
      skill: Math.floor( spiritAttributes.energy / 2 ),
      type: 'global'
    };

    /// Reprimir
    fightingWeapons.repress = {
      name: 'repress',
      raceSkill: 5,
      skill: Math.ceil( spiritAttributes.energy / 4 ),
      type: 'global'
    };

    // Atribuição de ataques
    spiritAttacks.push(
      new m.ManeuverRapture( {
        damage: { ether: spiritAttributes.power },
        source: this
      } ),
      new m.ManeuverRepress( {
        damage: { ether: spiritAttributes.power },
        source: this
      } )
    );
  }

  // Superconstrutor
  m.AstralBeing.call( this, config );

  // Configurações pós-superconstrutor
  postConfig: {
    // Identificadores
    let { attributes } = this.content.stats;

    // Definição da energia original
    attributes.base.energy = 20;

    // Definição dos demais valores originais de atributos
    for( let attribute of [ 'initiative', 'power', 'will' ] ) attributes.base[ attribute ] = attributes.current[ attribute ];

    // Caso uma partida esteja em andamento e espírito seja baseado em um ente modelo, executa suas configurações finais
    if( m.GameMatch.current && source ) Spirit.postConfigFromSource( this, source );
  }

  // Eventos

  /// Para atualizar habilidades e manobras do ente astral conforme variação de sua energia
  m.events.cardContentEnd.energy.add( this, m.AstralBeing.updateManeuvers, { context: m.AstralBeing } );

  /// Para atualizar danos do ente astral conforme variação de seu poder
  m.events.cardContentEnd.power.add( this, m.AstralBeing.updateDamages, { context: m.AstralBeing } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Spirit.init = function ( astralBeing ) {
    // Chama 'init' ascendente
    m.AstralBeing.init( astralBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( astralBeing instanceof Spirit );

    // Atribuição de propriedades iniciais

    /// Nome
    astralBeing.name = 'spirit';

    /// Indica ente modelo do espírito
    astralBeing.sourceBeing = null;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = astralBeing.content.typeset;

    /// Raça
    typeset.race = 'spirit';
  }

  // Executa configurações do espírito enquanto tendo sido convocado
  Spirit.configFromSummoner = function ( spirit, summoner ) {
    // Identificadores pré-validação
    var channeling = summoner?.content.stats.combativeness.channeling;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( spirit instanceof this );
      m.oAssert( channeling?.paths.voth.skill );
    }

    // Identificadores pós-validação
    var spiritAttributes = spirit.content.stats.attributes.current;

    // Configurações de atributos

    /// Energia
    spiritAttributes.energy = 10;

    /// Iniciativa
    spiritAttributes.initiative = 8;

    /// Poder
    spiritAttributes.power = summoner.content.stats.combativeness.channeling.paths.voth.skill;

    /// Vontade
    spiritAttributes.will = summoner.content.stats.attributes.base.will;
  }

  // Executa configurações iniciais do espírito enquanto tendo sido baseado em um ente modelo
  Spirit.preConfigFromSource = function ( spirit, source ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( spirit instanceof this );
      m.oAssert( source instanceof m.Humanoid );
      m.oAssert( !source.astralForm );
    }

    // Atribuição de propriedades diversas
    adjustProperties: {
      // Registra no espírito humanoide que representa
      spirit.sourceBeing = source;

      // Registra no humanoide espírito que o representa
      source.astralForm = spirit;

      // Atribuição da ordem da carta em grades
      spirit.gridOrder = source.gridOrder;
    }

    // Configurações de atributos
    setAttributes: {
      // Identificadores
      let spiritAttributes = spirit.content.stats.attributes.current;

      // Energia
      spiritAttributes.energy = source.content.stats.attributes.current.stamina * 2;

      // Poder
      spiritAttributes.power = 1 + source.content.typeset.experience.level;

      // Iniciativa e vontade
      for( let key of [ 'initiative', 'will' ] ) spiritAttributes[ key ] = source.content.stats.attributes.current[ key ];
    }

    // Remoção de fichas
    disposeTokes: {
      // Identificadores
      let sourceSummons = source.summons?.slice() ?? [];

      // Remove convocados do humanoide modelo que sejam pertences
      for( let summon of sourceSummons )
        if( summon.owner ) summon.unlink();
    }

    // Atribuição de estatísticas de canalização
    setChannelingStats: {
      // Identificadores
      let sourceChanneling = source.content.stats.combativeness.channeling;

      // Não executa bloco caso ente modelo não seja um canalizador
      if( !sourceChanneling ) break setChannelingStats;

      // Defini estatísticas de canalização do espírito como iguais a de seu ente modelo
      spirit.content.stats.combativeness.channeling = sourceChanneling;

      // Ajusta dono das magias do ente modelo
      sourceChanneling.getOwnedSpells().forEach( spell => spell.owner = spirit );
    }
  }

  // Executa configurações finais do espírito enquanto tendo sido baseado em um ente modelo
  Spirit.postConfigFromSource = function ( spirit, source ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( spirit instanceof this );
      m.oAssert( spirit.sourceBeing == source );
      m.oAssert( source.astralForm == spirit );
      m.oAssert( m.GameMatch.current );
    }

    // Integração do espírito no baralho do ente modelo
    addToDeck: {
      // Identificadores
      let ownerDeck = source.getRelationships().owner.gameDeck;

      // Adiciona espírito ao baralho do alvo da condição
      ownerDeck.add( [ spirit ], true );

      // Adiciona ao espírito eventos de configuração padrão da partida
      m.GameMatch.current.decks.config( [ spirit ] );

      // Caso ele exista lá, remove ente modelo do arranjo de cartas de sua atividade em seu baralho
      if( ownerDeck.cards[ source.activity ]?.includes( source ) )
        ownerDeck.cards[ source.activity ].splice( ownerDeck.cards[ source.activity ].indexOf( source ), 1 );

      // Caso ele exista lá, remove ente modelo do arranjo de cartas em uso de seu baralho
      if( ownerDeck.cards.inUse.includes( source ) )
        ownerDeck.cards.inUse.splice( ownerDeck.cards.inUse.indexOf( source ), 1 );
    }

    // Programa integração do espírito no fluxo da partida atual
    setTimeout( () => m.GameMatch.current?.configBeing( { summon: spirit } ) );
  }

  // Ordem inicial das variações desta carta na grade
  Spirit.gridOrder = m.data.cards.creatures.push( Spirit ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Spirit.prototype = Object.create( m.AstralBeing.prototype, {
  // Construtor
  constructor: { value: Spirit }
} );
