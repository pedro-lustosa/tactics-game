// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const BioticBeing = function ( config = {} ) {
  // Superconstrutor
  m.PhysicalBeing.call( this, config );

  // Eventos

  /// Para afastar ente biótico caso seu vigor ou vontade chegue a 0
  for( let eventName of [ 'stamina', 'will' ] ) m.events.cardContentEnd[ eventName ].add( this, this.removeOnLackOfAttribute );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BioticBeing.init = function ( bioticBeing ) {
    // Chama 'init' ascendente
    m.PhysicalBeing.init( bioticBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( bioticBeing instanceof BioticBeing );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = bioticBeing.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 0;

    /// Iniciativa
    currentAttributes.initiative = 0;

    /// Vigor
    currentAttributes.stamina = 0;

    /// Agilidade
    currentAttributes.agility = 0;

    /// Vontade
    currentAttributes.will = 0;

    // Atribuição de propriedades de 'content.stats.effectivity.conductivity'
    let conductivity = bioticBeing.content.stats.effectivity.conductivity;

    /// Atual
    conductivity.current = 1;
  }
}

/// Propriedades do protótipo
BioticBeing.prototype = Object.create( m.PhysicalBeing.prototype, {
  // Construtor
  constructor: { value: BioticBeing }
} );
