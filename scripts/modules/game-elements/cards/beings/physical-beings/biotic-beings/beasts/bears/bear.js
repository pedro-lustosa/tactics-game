// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Bear = function ( config = {} ) {
  // Iniciação de propriedades
  Bear.init( this, config.summoner );

  // Superconstrutor
  m.Beast.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Bear.init = function ( bear, summoner ) {
    // Chama 'init' ascendente
    m.Beast.init( bear, summoner );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( bear instanceof Bear );

    // Atribuição de propriedades iniciais

    /// Nome
    bear.name = 'bear';

    // Atribuição de propriedades de 'content'
    let content = bear.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'bear';

    /// Tamanho
    typeset.size = 'medium';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 44;

    /// Iniciativa
    currentAttributes.initiative = 11;

    /// Vigor
    currentAttributes.stamina = 6;

    /// Agilidade
    currentAttributes.agility = 4;

    /// Vontade
    currentAttributes.will = 1;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Arranhar
    fightingWeapons.scratch = {
      name: 'scratch',
      raceSkill: 7,
      skill: 7,
      type: 'melee'
    };

    /// Morder
    fightingWeapons.bite = {
      name: 'bite',
      raceSkill: 5,
      skill: 5,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverScratch( {
        recovery: 3,
        damage: { slash: 6 },
        range: 'M2',
        source: bear
      } ), new m.ManeuverBite( {
        recovery: 1,
        damage: { pierce: 8 },
        source: bear
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: bear
    } ) );
  }

  // Ordem inicial das variações desta carta na grade
  Bear.gridOrder = m.data.cards.creatures.push( Bear ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Bear.prototype = Object.create( m.Beast.prototype, {
  // Construtor
  constructor: { value: Bear }
} );
