// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Wolf = function ( config = {} ) {
  // Iniciação de propriedades
  Wolf.init( this, config.summoner );

  // Superconstrutor
  m.Beast.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Wolf.init = function ( wolf, summoner ) {
    // Chama 'init' ascendente
    m.Beast.init( wolf, summoner );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( wolf instanceof Wolf );

    // Atribuição de propriedades iniciais

    /// Nome
    wolf.name = 'wolf';

    // Atribuição de propriedades de 'content'
    let content = wolf.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'wolf';

    /// Tamanho
    typeset.size = 'small';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 12;

    /// Iniciativa
    currentAttributes.initiative = 9;

    /// Vigor
    currentAttributes.stamina = 5;

    /// Agilidade
    currentAttributes.agility = 8;

    /// Vontade
    currentAttributes.will = 1;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Morder
    fightingWeapons.bite = {
      name: 'bite',
      raceSkill: 5,
      skill: 5,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverBite( {
        recovery: 1,
        damage: { pierce: 6 },
        source: wolf
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: wolf
    } ) );
  }

  // Ordem inicial das variações desta carta na grade
  Wolf.gridOrder = m.data.cards.creatures.push( Wolf ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Wolf.prototype = Object.create( m.Beast.prototype, {
  // Construtor
  constructor: { value: Wolf }
} );
