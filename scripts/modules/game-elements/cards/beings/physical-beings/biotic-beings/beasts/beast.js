// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Beast = function ( config = {} ) {
  // Identificadores
  var { content: { typeset, stats: { attributes } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( Beast.raceNames.includes( typeset.race ) );

  // Superconstrutor
  m.BioticBeing.call( this, config );

  // Configurações pós-superconstrutor

  /// Definição dos valores originais dos atributos
  Object.oAssignByCopy( attributes.base, attributes.current );
}

/// Propriedades do construtor
defineProperties: {
  // Nomes das raças
  Beast.raceNames = [ 'bear', 'wolf', 'swarm' ];

  // Iniciação de propriedades
  Beast.init = function ( beast, summoner ) {
    // Chama 'init' ascendente
    m.BioticBeing.init( beast );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( beast instanceof Beast );
      m.oAssert( summoner instanceof m.Card );
      m.oAssert( summoner.deck instanceof m.MainDeck );
      m.oAssert( summoner.summons );
    }

    // Atribuição de propriedades iniciais

    /// Indica se carta é uma ficha
    beast.isToken = true;

    /// Indica dono da carta
    beast.owner = summoner;

    /// Indica convocante da carta
    beast.summoner = summoner;

    /// Indica se ente não é controlável por seu jogador
    beast.isUncontrollable = true;
  }
}

/// Propriedades do protótipo
Beast.prototype = Object.create( m.BioticBeing.prototype, {
  // Construtor
  constructor: { value: Beast }
} );
