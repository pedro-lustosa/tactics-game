// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Swarm = function ( config = {} ) {
  // Iniciação de propriedades
  Swarm.init( this, config.summoner );

  // Superconstrutor
  m.Beast.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Swarm.init = function ( swarm, summoner ) {
    // Chama 'init' ascendente
    m.Beast.init( swarm, summoner );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( swarm instanceof Swarm );

    // Atribuição de propriedades iniciais

    /// Nome
    swarm.name = 'swarm';

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = swarm.maxAttachments;

    /// Magias
    maxAttachments.spell = 0;

    // Atribuição de propriedades de 'content'
    let content = swarm.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'swarm';

    /// Tamanho
    typeset.size = null;

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 1;

    /// Iniciativa
    currentAttributes.initiative = 14;

    /// Vigor
    currentAttributes.stamina = 1;

    /// Agilidade
    currentAttributes.agility = 0;

    /// Vontade
    currentAttributes.will = 1;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Enxamear
    fightingWeapons.swarm = {
      name: 'swarm',
      raceSkill: 10,
      skill: 10,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverSwarm( {
        damage: { pierce: 1 },
        source: swarm
      } )
    );

    // Atribuição de propriedades de 'content.stats.effectivity.conductivity'
    let conductivity = content.stats.effectivity.conductivity;

    /// Atual
    conductivity.current = 0;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Físicas
      blunt: Infinity, slash: Infinity, pierce: Infinity
    } );
  }

  // Ordem inicial das variações desta carta na grade
  Swarm.gridOrder = m.data.cards.creatures.push( Swarm ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Swarm.prototype = Object.create( m.Beast.prototype, {
  // Construtor
  constructor: { value: Swarm }
} );
