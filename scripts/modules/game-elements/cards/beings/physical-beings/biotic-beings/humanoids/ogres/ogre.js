// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Ogre = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Ogre.init = function ( ogre ) {
    // Chama 'init' ascendente
    m.Humanoid.init( ogre );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( ogre instanceof Ogre );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = ogre.content.typeset;

    /// Raça
    typeset.race = 'ogre';

    /// Tamanho
    typeset.size = 'large';

    // Atribuição de propriedades de 'content.traits'
    let traits = ogre.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Caminhada Lenta – Implementado via parte do código dos arquivos 'action-engage' e 'action-disengage'
      slowPace: {
        name: 'slow-pace'
      },
      // Teimosia Tática – Implementado via método local 'validateRedeploymentActions'
      stubbornTactics: {
       name: 'stubborn-tactics'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = ogre.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 56;

    /// Iniciativa
    currentAttributes.initiative = 7;

    /// Vigor
    currentAttributes.stamina = 5;

    /// Agilidade
    currentAttributes.agility = 3;

    /// Vontade
    currentAttributes.will = 5;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = ogre.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'unarmed', 'mace' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = ogre.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 3 },
        range: 'M3',
        source: ogre
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: ogre
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = ogre.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 15;
  }

  // Validação de ações de ogros em remobilizações ante a característica 'stubbornTactics'
  Ogre.validateRedeploymentActions = function ( ogre ) {
    // Apenas executa função em turnos da batalha
    if( !( m.GameMatch.current.flow.turn instanceof m.BattleTurn ) ) return;

    // Identificadores
    var matchFlow = m.GameMatch.current.flow,
        targetParity = ogre.deck.owner.parity;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( ogre instanceof m.Ogre );

    // Caso paridade da numeração do turno atual não seja igual à paridade do jogador do ogro, indica que ações de remobilizações não podem ser habilitadas
    if( matchFlow.turn.counter % 2 == ( targetParity == 'odd' ? false : true ) ) return false;

    // Indica que ações de remobilizações podem ser habilitadas
    return true;
  }
}

/// Propriedades do protótipo
Ogre.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Ogre }
} );
