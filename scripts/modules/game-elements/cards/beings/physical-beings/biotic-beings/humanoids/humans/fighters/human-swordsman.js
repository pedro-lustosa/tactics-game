// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanSwordsman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanSwordsman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Espada Larga
      this.equip( new m.WeaponLongsword() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 4, 'sword' );

      // Equipamentos

      /// Espada Larga
      this.equip( new m.WeaponLongsword() );

      /// Cota de Malhas
      this.equip( new m.GarmentChainMail() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 5, 'sword' );

      /// Clavas
      fighting.increase( 1, 'mace' );

      // Equipamentos

      /// Espada Larga
      this.equip( new m.WeaponLongsword() );

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Armadura de Placas
      this.equip( new m.GarmentPlateArmor() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 7, 'sword' );

      /// Clavas
      fighting.increase( 1, 'mace' );

      // Equipamentos

      /// Espada Larga
      this.equip( new m.WeaponLongsword( { grade: 'masterpiece' } ) );

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Armadura de Placas
      this.equip( new m.GarmentPlateArmor() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanSwordsman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanSwordsman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'swordsman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanSwordsman.gridOrder = m.data.cards.creatures.push( HumanSwordsman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanSwordsman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanSwordsman }
} );
