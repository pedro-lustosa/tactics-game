// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Human = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Human.init = function ( human ) {
    // Chama 'init' ascendente
    m.Humanoid.init( human );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( human instanceof Human );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = human.content.typeset;

    /// Raça
    typeset.race = 'human';

    /// Tamanho
    typeset.size = 'medium';

    // Atribuição de propriedades de 'content.traits'
    let traits = human.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Integração Racial – Implementado via método local 'triggerRacialMix'
      racialMix: {
        name: 'racial-mix'
      },
      // Adaptabilidade Tática – Implementado via métodos locais 'setupAdaptiveTactics', 'triggerAdaptiveTactics' e 'adaptiveTacticsOnMutualMoves'
      adaptiveTactics: {
       name: 'adaptive-tactics'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = human.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 36;

    /// Iniciativa
    currentAttributes.initiative = 9;

    /// Vigor
    currentAttributes.stamina = 5;

    /// Agilidade
    currentAttributes.agility = 5;

    /// Vontade
    currentAttributes.will = 5;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = human.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'sword', 'shield' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = human.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 2 },
        range: 'M2',
        source: human
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: human
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = human.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 15;
  }

  // Ativação do efeito da característica 'racialMix'
  Human.triggerRacialMix = function ( commander, eventData = {} ) {
    // Identificadores
    var { command } = commander.content.stats,
        { race: raceScope } = command.scopes.creature,
        { eventPhase, eventType, cards } = eventData;

    // Não executa função caso quantidade máxima de humanos seja por padrão nula
    if( !raceScope.human.max ) return false;

    // Redefine quantidade máxima de humanos para o valor original caso o comandante esteja para ser removido do baralho atual
    if( eventPhase == 'start' && eventType == 'remove' && cards?.includes( commander ) ) return raceScope.human.max = raceScope.human.baseMax;

    // Captura incremento a ser adicionado à quantidade máxima de humanos
    let increment = ( function () {
      // Identificadores
      var increment = 0,
          targetRaces = Object.keys( raceScope ).filter( race => raceScope[ race ].max && race != 'human' );

      // Itera por raças passíveis de incrementarem a quantidade máxima de humanos
      for( let race of targetRaces ) {
        // Identificadores
        let raceQuantity = commander.deck?.cards.creatures.filter( card => card.content.typeset.race == race ).length ?? 0,
            quantityAboveMin = raceQuantity - raceScope[ race ].min;

        // Ajusta incremento com base na diferença entre quantidade atual da raça alvo e sua quantidade mínima
        increment += quantityAboveMin >= 5 ? 3 : quantityAboveMin >= 3 ? 2 : quantityAboveMin >= 1 ? 1 : 0;
      }

      // Retorna incremento a ser adicionado
      return increment;
    } ).call( command );

    // Retorna valor ajustado da quantidade máxima de humanos no comando
    return raceScope.human.max = raceScope.human.baseMax + increment;
  }

  // Configura períodos da remobilização para que suportem o efeito da característica 'adaptiveTactics'
  Human.setupAdaptiveTactics = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: redeploymentPeriod } = eventData;

    // Validação
    if( m.app.isInDevelopment )
      m.oAssert( eventCategory == 'flow-change' && [ 'begin', 'finish' ].includes( eventType ) && redeploymentPeriod instanceof m.RedeploymentPeriod );

    // Identificadores pós-validação
    var parities = redeploymentPeriod.children;

    // Ao início e fim do período da remobilização alvo, zera contagem de jogadas de humanos acionadas em suas paridades
    parities.forEach( parity => parity.humanMovesCount = 0 );
  }

  // Ativação do efeito da característica 'adaptiveTactics'
  Human.triggerAdaptiveTactics = function ( eventData = {} ) {
    // Apenas executa função em períodos da remobilização
    if( !( m.GameMatch.current.flow.period instanceof m.RedeploymentPeriod ) ) return;

    // Identificadores
    var { period, parity } = m.GameMatch.current.flow,
        { eventPhase, eventTarget: action, eventCategory } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( parity instanceof m.FlowParity );
      m.oAssert( action instanceof m.GameAction );
      m.oAssert( [ 'start', 'end' ].includes( eventPhase ) );
      m.oAssert( eventCategory == 'action-change' );
    }

    // Não executa função caso contador de jogadas de humanos para a paridade alvo tenha atingido seu limite
    if( parity.humanMovesCount >= 2 ) return;

    // Caso seja início do acionamento da ação
    if( eventPhase == 'start' ) {
      // Altera tipo da ação para livre
      action.commitment.type = 'free';
    }

    // Do contrário
    else {
      // Reajusta tipo da ação para dedicada
      action.commitment.type = 'dedicated';

      // Incrementa contador de jogadas acionadas por humanos
      parity.humanMovesCount++;
    }
  }

  // Retorna quantas ações mútuas serão modificadas por 'adaptiveTactics'
  Human.adaptiveTacticsOnMutualMoves = function ( ...committers ) {
    // Identificadores
    var movesLimit = 2,
        { humanMovesCount = movesLimit } = m.GameMatch.current.flow.parity,
        humansCount = committers.filter( committer => committer instanceof m.Human ).length;

    // Retorna menor valor entre a contagem de humanos acionantes e as jogadas restantes afetadas pela característica para a paridade alvo
    return Math.min( humansCount, movesLimit - humanMovesCount );
  }
}

/// Propriedades do protótipo
Human.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Human }
} );
