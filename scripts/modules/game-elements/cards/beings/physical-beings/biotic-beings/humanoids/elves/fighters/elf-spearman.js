// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ElfSpearman = function ( config = {} ) {
  // Iniciação de propriedades
  ElfSpearman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 2, 'spear' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 3, 'spear' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 4, 'spear' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur() );

      /// Lança
      this.equip( new m.WeaponDefaultSpear() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 5, 'spear' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur( { grade: 'masterpiece' } ) );

      /// Lança
      this.equip( new m.WeaponDefaultSpear( { grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );
  }

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ElfSpearman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Elf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof ElfSpearman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'spearman';
  }

  // Ordem inicial das variações desta carta na grade
  ElfSpearman.gridOrder = m.data.cards.creatures.push( ElfSpearman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
ElfSpearman.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: ElfSpearman }
} );
