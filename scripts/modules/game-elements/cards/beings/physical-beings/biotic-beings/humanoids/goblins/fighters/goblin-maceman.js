// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GoblinMaceman = function ( config = {} ) {
  // Iniciação de propriedades
  GoblinMaceman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 2, 'mace' );

      // Equipamentos

      /// Mangual
      this.equip( new m.WeaponFlail( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 3, 'mace' );

      // Equipamentos

      /// Mangual
      this.equip( new m.WeaponFlail( { size: 'small' } ) );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponFlail ).equip( new m.ImplementPoison() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 4, 'mace' );

      // Equipamentos

      /// Mangual
      this.equip( new m.WeaponFlail( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponFlail ).equip( new m.ImplementPoison() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 5, 'mace' );

      // Equipamentos

      /// Mangual
      this.equip( new m.WeaponFlail( { size: 'small', grade: 'masterpiece' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponFlail ).equip( new m.ImplementPoison() );
  }

  // Superconstrutor
  m.Goblin.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GoblinMaceman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Goblin.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GoblinMaceman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'maceman';
  }

  // Ordem inicial das variações desta carta na grade
  GoblinMaceman.gridOrder = m.data.cards.creatures.push( GoblinMaceman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GoblinMaceman.prototype = Object.create( m.Goblin.prototype, {
  // Construtor
  constructor: { value: GoblinMaceman }
} );
