// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OrcAxeman = function ( config = {} ) {
  // Iniciação de propriedades
  OrcAxeman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Machados
      fighting.increase( 2, 'axe' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe() );

      /// Broquel
      this.equip( new m.WeaponBuckler() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Machados
      fighting.increase( 3, 'axe' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe() );

      /// Broquel
      this.equip( new m.WeaponBuckler() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Machados
      fighting.increase( 4, 'axe' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe() );

      /// Rodela
      this.equip( new m.WeaponRondache() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Machados
      fighting.increase( 6, 'axe' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe( { grade: 'masterpiece' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache() );
  }

  // Superconstrutor
  m.Orc.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OrcAxeman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Orc.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OrcAxeman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'axeman';
  }

  // Ordem inicial das variações desta carta na grade
  OrcAxeman.gridOrder = m.data.cards.creatures.push( OrcAxeman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OrcAxeman.prototype = Object.create( m.Orc.prototype, {
  // Construtor
  constructor: { value: OrcAxeman }
} );
