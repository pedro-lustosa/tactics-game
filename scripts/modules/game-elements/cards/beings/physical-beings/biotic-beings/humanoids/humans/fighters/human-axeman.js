// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanAxeman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanAxeman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Machados
      fighting.increase( 1, 'axe' );

      // Equipamentos

      /// Acha
      this.equip( new m.WeaponPoleaxe() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Machados
      fighting.increase( 2, 'axe' );

      // Equipamentos

      /// Acha
      this.equip( new m.WeaponPoleaxe() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Machados
      fighting.increase( 3, 'axe' );

      // Equipamentos

      /// Acha
      this.equip( new m.WeaponPoleaxe() );

      /// Armadura de Placas
      this.equip( new m.GarmentPlateArmor() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Machados
      fighting.increase( 4, 'axe' );

      /// Bestas
      fighting.increase( 1, 'crossbow' );

      // Equipamentos

      /// Acha
      this.equip( new m.WeaponPoleaxe( { grade: 'masterpiece' } ) );

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      /// Armadura de Placas
      this.equip( new m.GarmentPlateArmor() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanAxeman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanAxeman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'axeman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanAxeman.gridOrder = m.data.cards.creatures.push( HumanAxeman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanAxeman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanAxeman }
} );
