// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderWimis = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderWimis.init( this );

  // Identificadores
  var { command, combativeness: { fighting, channeling } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Espadas
  fighting.increase( 2, 'sword' );

  /// Equipamentos

  //// Gládio
  this.equip( new m.WeaponGladius( { size: 'small' } ) );

  /// Habilidades de canalização

  //// Powth
  channeling.paths.powth.skill = 3;

  //// Voth
  [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 3 ];

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 7, max: 10, constraint: {
        check: ( ...cards ) => cards.every( card =>
          ( card.content.typeset.race == 'orc' ? [ 2, 3 ] : [ 1, 2 ] ).includes( card.content.typeset.experience.level )
        )
      } },
      race: {
        human: { max: 2 },
        orc: { min: 3, max: 4 },
        elf: { max: 2 },
        halfling: { max: 2 },
        ogre: { max: 1 }
      },
      roleType: {
        melee: { min: 3, max: 6 },
        ranged: { max: 3 },
        magical: { max: 3 }
      },
      experienceLevel: {
        1: { min: 3, max: 7 },
        2: { max: 7 },
        3: { max: 4 }
      }
    },
    item: {
      total: { max: 18 },
      grade: {
        ordinary: { max: 18 },
        masterpiece: { max: 18 },
        artifact: { max: 6 }
      }
    },
    spell: {
      total: { min: 4, max: 22 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2000, sideDeck: 660
  } );

  // Superconstrutor
  m.Goblin.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderWimis.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Goblin.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderWimis );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-wimis';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = commander.content.typeset.role;

    /// Nome
    role.name = 'warlock';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = commander.content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 3;

    // Atribuição de propriedades de 'content.stats'
    let stats = commander.content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderWimis.gridOrder = m.data.cards.commanders.push( CommanderWimis );
}

/// Propriedades do protótipo
CommanderWimis.prototype = Object.create( m.Goblin.prototype, {
  // Construtor
  constructor: { value: CommanderWimis }
} );
