// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ElfBowman = function ( config = {} ) {
  // Iniciação de propriedades
  ElfBowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 3, 'bow' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 4, 'bow' );

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 6, 'bow' );

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponShortbow ).equip( new m.ImplementPoison() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 7, 'bow' );

      /// Espadas
      fighting.increase( 3, 'sword' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow( { grade: 'masterpiece' } ) );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponShortbow ).equip( new m.ImplementPoison() );
  }

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ElfBowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Elf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof ElfBowman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'bowman';
  }

  // Ordem inicial das variações desta carta na grade
  ElfBowman.gridOrder = m.data.cards.creatures.push( ElfBowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
ElfBowman.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: ElfBowman }
} );
