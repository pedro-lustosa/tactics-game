// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OgreMaceman = function ( config = {} ) {
  // Iniciação de propriedades
  OgreMaceman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 2, 'mace' );

      // Equipamentos

      /// Porrete Liso
      this.equip( new m.WeaponSmoothClub( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 3, 'mace' );

      // Equipamentos

      /// Porrete Liso
      this.equip( new m.WeaponSmoothClub( { size: 'large', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 4, 'mace' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Porrete Cravado
      this.equip( new m.WeaponSpikedClub( { size: 'large' } ) );

      /// Broquel
      this.equip( new m.WeaponBuckler( { size: 'large' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 5, 'mace' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Porrete Cravado
      this.equip( new m.WeaponSpikedClub( { size: 'large', grade: 'masterpiece' } ) );

      /// Broquel
      this.equip( new m.WeaponBuckler( { size: 'large', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );
  }

  // Superconstrutor
  m.Ogre.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OgreMaceman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Ogre.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OgreMaceman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'maceman';
  }

  // Ordem inicial das variações desta carta na grade
  OgreMaceman.gridOrder = m.data.cards.creatures.push( OgreMaceman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OgreMaceman.prototype = Object.create( m.Ogre.prototype, {
  // Construtor
  constructor: { value: OgreMaceman }
} );
