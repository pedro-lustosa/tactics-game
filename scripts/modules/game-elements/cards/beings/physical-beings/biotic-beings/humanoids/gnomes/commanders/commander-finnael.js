// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderFinnael = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderFinnael.init( this );

  // Identificadores
  var { command, combativeness: { fighting } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Bestas
  fighting.increase( 3, 'crossbow' );

  //// Espadas
  fighting.increase( 1, 'sword' );

  /// Equipamentos

  //// Besta
  this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

  //// Alfange
  this.equip( new m.WeaponFalchion( { size: 'small' } ) );

  //// Jaquetão
  this.equip( new m.GarmentGambeson( { size: 'small' } ) );

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 7, max: 9 },
      race: {
        dwarf: { min: 4, max: 6 },
        gnome: { min: 3, max: 5 }
      },
      roleType: {
        melee: { min: 2, max: 4 },
        ranged: { min: 0, max: 2 },
        magical: { min: 2, max: 4 }
      },
      experienceLevel: {
        1: { max: 9 },
        2: { max: 9 },
        3: { max: 9 },
        4: { max: 9 }
      }
    },
    item: {
      total: { min: 10, max: 12 },
      grade: {
        ordinary: { max: 12 },
        masterpiece: { max: 12 },
        artifact: { max: 12 },
        relic: { min: 0, max: 2 }
      }
    },
    spell: {
      total: { min: 10, max: 12 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2750, sideDeck: 920
  } );

  // Superconstrutor
  m.Gnome.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderFinnael.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Gnome.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderFinnael );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-finnael';

    // Atribuição de propriedades de 'content'
    let content = commander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'crossbowman';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 2;

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderFinnael.gridOrder = m.data.cards.commanders.push( CommanderFinnael );
}

/// Propriedades do protótipo
CommanderFinnael.prototype = Object.create( m.Gnome.prototype, {
  // Construtor
  constructor: { value: CommanderFinnael }
} );
