// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Humanoid = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, typeset: { role, experience }, stats } } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( Humanoid.raceNames.includes( typeset.race ) );
    m.oAssert( [
      'civilian', 'maceman', 'swordsman', 'axeman', 'spearman', 'slingman', 'bowman', 'crossbowman', 'theurge', 'warlock', 'wizard', 'sorcerer'
    ].includes( role.name ) );
    m.oAssert( [ 'commander', 'rookie', 'regular', 'veteran', 'elite', 'novice', 'disciple', 'adept', 'master' ].includes( experience.grade ) );
    m.oAssert( experience.level >= 0 && experience.level <= 4 );
    m.oAssert( Number.isInteger( this.coins.raceIncrement ) && this.coins.raceIncrement >= 0 );
  }

  // Configurações pré-superconstrutor

  /// Atribuição da imagem, se ainda não atribuída
  content.image ||= this.name;

  /// Atribuição da disponibilidade
  typeset.availability = ( function () {
    switch( experience.level ) {
      case 4:
        return 1;
      case 3:
        return 2;
      case 2:
        return 3;
      default:
        return 4;
    }
  } )();

  /// Atribuição da atuação
  role.type = ( function () {
    switch( role.name ) {
      case 'civilian':
      case 'maceman':
      case 'swordsman':
      case 'axeman':
      case 'spearman':
        return 'melee';
      case 'slingman':
      case 'bowman':
      case 'crossbowman':
        return 'ranged';
      case 'theurge':
      case 'warlock':
      case 'wizard':
      case 'sorcerer':
        return 'magical';
    }
  } )();

  /// Atribuição das características
  for( let trait in content.traits ) Object.assign( content.traits[ trait ], m.languages.cards.getTrait( content.traits[ trait ].name ) );

  // Atribuição dos atributos
  Humanoid.setAttributes( this );

  // Atribuição das moedas
  Humanoid.setCoins( this );

  /// Para criaturas, atribuição da ordem da carta em grades
  if( !stats.command ) this.gridOrder = this.constructor.gridOrder + experience.level - 1;

  // Superconstrutor
  m.BioticBeing.call( this, config );

  // Eventos

  /// Ajusta agilidade do humanoide em função de sua contagem de peso
  m.events.cardContentEnd.item.add( this, this.weightCount.adjustAgility, { context: this.weightCount } );

  /// Reduz custos de mana de magias 'A Fera' após remoção do humanoide
  m.events.cardActivityEnd.removed.add( this, m.SpellTheBeast.reduceManaCost, { context: m.SpellTheBeast } );

  /// Verifica ganho de pontos de destino segundo efeito do comandante Ixohch
  m.events.cardActivityEnd.removed.add( this, m.CommanderIxohch.checkFatePointGain, { context: m.CommanderIxohch } );

  /// Atualiza custos de mana de magias 'A Trégua' e 'O Engodo' segundo entrada e saída do humanoide no campo
  for( let constructor of [ m.SpellTheTruce, m.SpellTheLure ] )
    m.events.fieldSlotCardChangeEnd.any.add( this, constructor.updateManaCost, { context: constructor } );
}

/// Propriedades do construtor
defineProperties: {
  // Nomes das raças
  Humanoid.raceNames = [ 'human', 'orc', 'elf', 'dwarf', 'halfling', 'gnome', 'goblin', 'ogre' ];

  // Iniciação de propriedades
  Humanoid.init = function ( humanoid ) {
    // Chama 'init' ascendente
    m.BioticBeing.init( humanoid );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( humanoid instanceof Humanoid );

    // Atribuição de propriedades iniciais

    /// Mãos
    humanoid.hands = {};

    /// Contagem de peso
    humanoid.weightCount = {};

    /// Trajes
    humanoid.garments = {};

    /// Atribuição da capacidade de usar equipamentos
    Object.oAssignByCopy( humanoid, m.mixinEquipable.call( humanoid ) );

    /// Ente astral representando o humanoide
    humanoid.astralForm = null;

    /// Contagem de combates durante os que o humanoide esteve sempre ativo, para uso pela magia 'O Regente'
    humanoid.activeCombatCount = 0;

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = humanoid.maxAttachments;

    /// Armas
    maxAttachments.weapon = 3;

    /// Trajes
    maxAttachments.garment = 2;

    /// Apetrechos
    maxAttachments.implement = 2;

    // Atribuição de propriedades de 'hands'
    let hands = humanoid.hands;

    /// Totais
    hands.total = 2;

    /// Disponíveis
    hands.available = hands.total;

    /// Coloca equipamento em mãos
    hands.put = function ( item, rank ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( humanoid.activity == 'active' );
        m.oAssert( [ m.Weapon, m.Implement ].some( constructor => item instanceof constructor ) );
        m.oAssert( [ 'primary', 'secondary' ].includes( rank ) );
      }

      // Identificadores
      var humanoidManeuvers = humanoid.content.stats.effectivity.maneuvers,
          humanoidItems = Object.fromEntries( [ 'primary', 'secondary' ].map( rank => [ rank, humanoidManeuvers[ rank ].source ] ) ),
          preferredRank = item instanceof m.Weapon ? 'primary' : 'secondary',
          unpreferredRank = preferredRank == 'primary' ? 'secondary' : 'primary',
          recoveryBonusToKeep = humanoidManeuvers[ unpreferredRank ].recoveryBonus,
          wieldingModifier = item.content.typeset.wielding == 'two-handed' ? 2 : item.content.typeset.wielding == 'non-handed' ? 0 : 1,
          dataToEmit = {
            trigger: 'put', maneuverTarget: item
          };

      // Não coloca equipamento em mãos caso ele já esteja lá
      if( Object.values( humanoidItems ).includes( item ) ) return item;

      // Sinaliza início da mudança de fonte de manobras
      m.events.cardContentStart.maneuverSource.emit( humanoid, dataToEmit );

      // Libera mão da categoria alvo
      humanoidItems[ rank ]?.disuse();

      // Libera mão da outra categoria, caso mãos livres atuais ainda não sejam suficientes para colocar em mãos equipamento alvo
      if( this.available - wieldingModifier < 0 ) humanoidItems[ rank == 'primary' ? 'secondary' : 'primary' ].disuse();

      // Ajusta categoria passada para a preferencial do equipamento caso não haja nenhum equipamento lá
      if( !humanoidManeuvers[ preferredRank ].source ) rank = preferredRank;

      // Para caso ajustes ao liberar as mãos tenham levado um equipamento para a fonte de manobras alvo
      if( rank == preferredRank && humanoidManeuvers[ rank ].source ) {
        // Reajusta posição de equipamento na fonte de manobras alvo para que ele não seja sobrescrito pelo novo
        humanoidManeuvers.change( unpreferredRank, humanoidManeuvers[ rank ].source );

        // Restaura bônus de recuperação do equipamento, quando existente
        humanoidManeuvers[ unpreferredRank ].recoveryBonus = recoveryBonusToKeep;
      }

      // Altera fonte de manobras alvo para que contenha manobras do equipamento passado
      humanoidManeuvers.change( rank, item );

      // Ajuste das manobras naturais de humanoide

      /// Para armas passadas, define-as como manobras secundárias, caso não exista um equipamento lá
      if( item instanceof m.Weapon && !humanoidManeuvers.secondary.source ) humanoidManeuvers.change( 'secondary', 'natural' )

      /// Para apetrechos passados, define-as como manobras primárias, caso não exista um equipamento lá
      else if( item instanceof m.Implement && !humanoidManeuvers.primary.source ) humanoidManeuvers.change( 'primary', 'natural' );

      // Atualiza mãos disponíveis de humanoide
      this.available -= wieldingModifier;

      // Sinaliza fim da mudança de fonte de manobras
      m.events.cardContentEnd.maneuverSource.emit( humanoid, dataToEmit );

      // Retorna equipamento
      return item;
    }

    /// Remove equipamento de mãos
    hands.release = function ( item ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Weapon, m.Implement ].some( constructor => item instanceof constructor ) );

      // Identificadores
      var humanoidManeuvers = humanoid.content.stats.effectivity.maneuvers,
          rank = [ 'primary', 'secondary' ].find( rank => humanoidManeuvers[ rank ].source == item ),
          wieldingModifier = item.content.typeset.wielding == 'two-handed' ? 2 : item.content.typeset.wielding == 'non-handed' ? 0 : 1,
          dataToEmit = {
            trigger: 'release', maneuverTarget: item
          };

      // Não executa função caso equipamento não esteja em nenhuma fonte de manobras
      if( !rank ) return item;

      // Sinaliza início da mudança de fonte de manobras
      m.events.cardContentStart.maneuverSource.emit( humanoid, dataToEmit );

      // Remove equipamento da fonte de manobras alvo do humanoide, e ajusta fonte de manobras de suas outras manobras
      switch( rank ) {
        // Para caso fonte de manobras alvo seja a primária
        case 'primary':
          // Identifica o que está na outra fonte de manobras
          switch( humanoidManeuvers.secondary.source?.content.typeset.equipage ) {
            // Para caso uma arma esteja na outra fonte de manobras
            case 'weapon':
              // Atribui à fonte de manobras primária a arma na fonte de manobras secundária
              humanoidManeuvers.change( 'primary', humanoidManeuvers.secondary.source );

              // Atribui à fonte de manobras secundária manobras naturais
              humanoidManeuvers.change( 'secondary', 'natural' );

              // Encerra operação
              break;
            // Para caso um apetrecho esteja na outra fonte de manobras
            case 'implement':
              // Atribui à fonte de manobras primária manobras naturais
              humanoidManeuvers.change( 'primary', 'natural' );

              // Encerra operação
              break;
            // Para caso manobras naturais estejam na outra fonte de manobras
            default:
              // Remove manobras naturais da fonte de manobras secundária
              humanoidManeuvers.remove( 'secondary' );

              // Atribui à fonte de manobras primária manobras naturais
              humanoidManeuvers.change( 'primary', 'natural' );

              // Atribui à fonte de manobras secundária manobras naturais, ou nada, se elas tiverem acabado
              humanoidManeuvers.change( 'secondary', 'natural' );
          }

          // Encerra operação
          break;
        // Para caso fonte de manobras alvo seja a secundária
        case 'secondary':
          // Para caso um apetrecho esteja na fonte de manobras primária
          if( humanoidManeuvers.primary.source instanceof m.Implement ) {
            // Atribui à fonte de manobras secundária o apetrecho na fonte de manobras primária
            humanoidManeuvers.change( 'secondary', humanoidManeuvers.primary.source );

            // Atribui à fonte de manobras primária manobras naturais
            humanoidManeuvers.change( 'primary', 'natural' );
          }
          else {
            // Atribui à fonte de manobras secundária manobras naturais, ou nada, se elas tiverem acabado
            humanoidManeuvers.change( 'secondary', 'natural' );
          }
      }

      // Atualiza mãos disponíveis de humanoide
      this.available += wieldingModifier;

      // Sinaliza fim da mudança de fonte de manobras
      m.events.cardContentEnd.maneuverSource.emit( humanoid, dataToEmit );

      // Retorna equipamento
      return item;
    }

    // Atribuição de propriedades de 'weightCount'
    let weightCount = humanoid.weightCount;

    /// Contagem atual
    weightCount.current = 0;

    /// Contagem máxima
    weightCount.max = 5;

    /// Ajusta agilidade de humanoide para que releve sua contagem de peso
    weightCount.adjustAgility = function ( eventData = {} ) {
      // Identificadores
      var { eventType, item } = eventData,
          weightScore = m.Item.getWeightScore( item ),
          humanoidAttributes = humanoid.content.stats.attributes,
          baseAgility = humanoidAttributes.base.agility;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( item instanceof m.Item );
        m.oAssert( [ 'item-in', 'item-out' ].includes( eventType ) );
      }

      // Não executa função caso equipamento alvo não tenha pontuação de peso
      if( !weightScore ) return;

      // Caso evento seja um de entrada de equipamento, desconta da agilidade atual peso do novo equipamento, até o valor mínimo de 0
      if( eventType == 'item-in' ) return humanoidAttributes.reduce( weightScore, 'agility', { triggeringCard: item } );

      // Não continua função caso contagem de peso atual leve agilidade base para seu valor mínimo
      if( baseAgility - this.current <= 0 ) return;

      // Caso evento seja um de saída de equipamento, aumenta agilidade atual por até toda pontuação de peso retirada, até o valor máximo de 10
      return humanoidAttributes.increase( Math.min( baseAgility - this.current, weightScore ), 'agility', { triggeringCard: item } );
    }

    // Atribuição de propriedades de 'garments'
    let garments = humanoid.garments;

    /// Traje primário
    garments.primary = {};

    /// Traje secundário
    garments.secondary = {};

    // Atribuição de propriedades de 'content.typeset'
    let typeset = humanoid.content.typeset;

    /// Atuação
    typeset.role = {};

    /// Experiência
    typeset.experience = {};

    // Atribuição de propriedades de 'content.typeset.role'
    let role = typeset.role;

    /// Nome
    role.name = '';

    /// Tipo
    role.type = '';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = typeset.experience;

    /// Categoria
    experience.grade = '';

    /// Nível
    experience.level = 0;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = humanoid.content.stats.combativeness.fighting;

    /// Adiciona habilidades de luta de humanoides
    Object.assign( fighting.weapons, {
      unarmed: {
        name: 'unarmed', raceSkill: 5, skill: 5, type: 'melee'
      },
      mace: {
        name: 'mace', raceSkill: 4, skill: 4, type: 'melee'
      },
      sword: {
        name: 'sword', raceSkill: 2, skill: 2, type: 'melee'
      },
      axe: {
        name: 'axe', raceSkill: 3, skill: 3, type: 'melee'
      },
      spear: {
        name: 'spear', raceSkill: 4, skill: 4, type: 'melee'
      },
      shield: {
        name: 'shield', raceSkill: 3, skill: 3, type: 'melee'
      },
      sling: {
        name: 'sling', raceSkill: 1, skill: 1, type: 'ranged'
      },
      bow: {
        name: 'bow', raceSkill: 2, skill: 2, type: 'ranged'
      },
      crossbow: {
        name: 'crossbow', raceSkill: 4, skill: 4, type: 'ranged'
      }
    } );

    // Atribuição de propriedades de 'coins'
    let coins = humanoid.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 0;
  }

  // Define atributos do humanoide alvo
  Humanoid.setAttributes = function ( humanoid ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( humanoid instanceof this );

    // Identificadores
    var { typeset: { race, role, experience }, stats: { attributes } } = humanoid.content,
        tweakAttributes = {
          human: tweakHumanAttributes, orc: tweakOrcAttributes, elf: tweakElfAttributes, dwarf: tweakDwarfAttributes,
          halfling: tweakHalflingAttributes, gnome: tweakGnomeAttributes, goblin: tweakGoblinAttributes, ogre: tweakOgreAttributes
        };

    // Atribui atributos segundo o nível do humanoide
    switch( experience.level ) {
      // Nível 0
      case 0:
        // Grande redução de todos os atributos
        for( let attribute of [ 'health', 'initiative', 'stamina', 'agility', 'will' ] ) attributes.decrease( 2, attribute );

        // Encerra operação
        break;
      // Nível 1
      case 1:
        // Para combatentes próximos, grande redução da iniciativa e vontade
        if( role.type == 'melee' )
          for( let attribute of [ 'initiative', 'will' ] ) attributes.decrease( 2, attribute )

        // Para combatentes distantes, pequena redução de todos os atributos
        else if( role.type == 'ranged' )
          for( let attribute of [ 'health', 'initiative', 'stamina', 'agility', 'will' ] ) attributes.decrease( 1, attribute )

        // Para combatentes mágicos, grande redução de vida, vigor e agilidade
        else if( role.type == 'magical' )
          for( let attribute of [ 'health', 'stamina', 'agility' ] ) attributes.decrease( 2, attribute );

        // Encerra operação
        break;
      // Nível 2
      case 2:
        // Para combatentes próximos, pequeno aumento da vida e pequena redução da iniciativa
        if( role.type == 'melee' ) attributes.increase( 1, 'health' ).decrease( 1, 'initiative' )

        // Para combatentes mágicos, pequena redução da vida e pequeno aumento da iniciativa
        else if( role.type == 'magical' ) attributes.decrease( 1, 'health' ).increase( 1, 'initiative' );

        // Encerra operação
        break;
      // Nível 3
      case 3:
        // Para combatentes próximos
        if( role.type == 'melee' ) {
          // Pequeno aumento da vida, vigor e agilidade
          for( let attribute of [ 'health', 'stamina', 'agility' ] ) attributes.increase( 1, attribute );

          // Pequena redução da vontade
          attributes.decrease( 1, 'will' );
        }

        // Para combatentes distantes, pequeno aumento da iniciativa
        else if( role.type == 'ranged' ) attributes.increase( 1, 'initiative' )

        // Para combatentes mágicos
        else if( role.type == 'magical' ) {
          // Pequena redução da vida, vigor e agilidade
          for( let attribute of [ 'health', 'stamina', 'agility' ] ) attributes.decrease( 1, attribute );

          // Grande aumento da iniciativa, e pequeno aumento da vontade
          attributes.increase( 2, 'initiative' ).increase( 1, 'will' );
        }

        // Encerra operação
        break;
      // Nível 4
      case 4:
        // Para combatentes próximos, grande aumento da vida, vigor e agilidade
        if( role.type == 'melee' )
          for( let attribute of [ 'health', 'stamina', 'agility' ] ) attributes.increase( 2, attribute )

        // Para combatentes distantes, pequeno aumento de todos os atributos
        else if( role.type == 'ranged' )
          for( let attribute of [ 'health', 'initiative', 'stamina', 'agility', 'will' ] ) attributes.increase( 1, attribute )

        // Para combatentes mágicos, grande aumento da iniciativa e vontade
        else if( role.type == 'magical' )
          for( let attribute of [ 'initiative', 'will' ] ) attributes.increase( 2, attribute );
    }

    // Caso nível do humanoide seja o 2, ajusta atributos segundo sua raça
    if( experience.level == 2 ) tweakAttributes[ race ]();

    // Define valores originais dos atributos
    Object.oAssignByCopy( attributes.base, attributes.current );

    // Funções de ajuste de atributos

    /// Para humanos
    function tweakHumanAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'stamina' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'agility' );
        case 'magical':
          return attributes.decrease( 2, 'stamina' ).decrease( 1, 'agility' );
      }
    }

    /// Para orques
    function tweakOrcAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'agility' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'agility' ).decrease( 1, 'will' );
        case 'magical':
          return attributes.decrease( 2, 'stamina' ).decrease( 2, 'agility' ).increase( 1, 'will' );
      }
    }

    /// Para elfos
    function tweakElfAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'agility' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'agility' );
        case 'magical':
          return attributes.decrease( 1, 'stamina' ).decrease( 2, 'agility' );
      }
    }

    /// Para anões
    function tweakDwarfAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.decrease( 1, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'will' );
        case 'magical':
          return attributes.decrease( 1, 'stamina' ).decrease( 2, 'agility' );
      }
    }

    /// Para metadílios
    function tweakHalflingAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.decrease( 1, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'agility' ).decrease( 1, 'will' );
        case 'magical':
          return attributes.decrease( 2, 'stamina' ).decrease( 1, 'agility' );
      }
    }

    /// Para gnomos
    function tweakGnomeAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'stamina' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'will' );
        case 'magical':
          return attributes.decrease( 2, 'stamina' ).decrease( 2, 'agility' ).increase( 1, 'will' );
      }
    }

    /// Para goblines
    function tweakGoblinAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'stamina' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'agility' );
        case 'magical':
          return attributes.decrease( 2, 'stamina' ).decrease( 1, 'agility' );
      }
    }

    /// Para ogros
    function tweakOgreAttributes() {
      // Ajuste dos atributos segundo tipo do cargo do humanoide
      switch( role.type ) {
        case 'melee':
          return attributes.increase( 1, 'agility' ).decrease( 2, 'will' );
        case 'ranged':
          return attributes.decrease( 1, 'stamina' ).decrease( 1, 'agility' );
        case 'magical':
          return attributes.decrease( 1, 'stamina' ).decrease( 2, 'agility' );
      }
    }
  }

  // Define custo em moedas do humanoide alvo
  Humanoid.setCoins = function ( humanoid ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( humanoid instanceof this );

    // Identificadores
    var { content: { stats, stats: { attributes, combativeness: { fighting, channeling } } }, coins } = humanoid;

    // Não executa função caso custo em moedas do humanoide já tenha sido definido
    if( coins.current ) return coins.current;

    // Não executa função caso humanoide seja um comandante
    if( stats.command ) return coins.current;

    // Adiciona ao valor em moedas do humanoide o incremento de sua raça
    coins.current += coins.raceIncrement;

    // Adiciona ao valor em moedas do humanoide valor derivado de seus pontos de vida
    coins.current += Math.floor( attributes.current.health / 3 );

    // Adiciona ao valor em moedas do humanoide valor derivado de sua iniciativa
    coins.current += Math.max( attributes.current.initiative - 4, 0 ) * 2;

    // Adiciona ao valor em moedas do humanoide valor derivado de seus outros atributos
    for( let key of [ 'stamina', 'agility', 'will' ] ) coins.current += attributes.current[ key ] * 2;

    // Adiciona ao valor em moedas do humanoide valor derivado de suas habilidades de luta
    addFightingCoins: {
      // Identificadores
      let fightingSkills = Object.values( fighting.weapons ).map( weapon => weapon.skill - weapon.raceSkill );

      // Ordena decrescentemente habilidades de luta
      fightingSkills.sort( ( a, b ) => b - a );

      // Adição do valor em moedas da habilidade de luta principal
      coins.current += fightingSkills[ 0 ] * 8;

      // Adição do valor em moedas das habilidades de luta secundárias
      fightingSkills.slice( 1 ).forEach( value => coins.current += value * 4 );
    }

    // Adiciona ao valor em moedas do humanoide valor derivado de suas habilidades de canalização
    addChannelingCoins: {
      // Apenas executa bloco caso humanoide tenha habilidades de canalização
      if( !channeling ) break addChannelingCoins;

      // Identificadores
      let channelingSkills = Object.values( channeling.paths ).map( path => path.skill - path.raceSkill );

      // Ordena decrescentemente habilidades de canalização
      channelingSkills.sort( ( a, b ) => b - a );

      // Adição do valor em moedas da habilidade de canalização principal
      coins.current += channelingSkills[ 0 ] * 16;

      // Adição do valor em moedas das habilidades de canalização secundárias
      channelingSkills.slice( 1 ).forEach( value => coins.current += value * 8 );
    }

    // Adiciona ao valor em moedas do humanoide valor derivado de seus vinculados embutidos
    humanoid.getAllCardAttachments( 'embedded' ).forEach( attachment => coins.current += attachment.coins.base );

    // Retorna moedas atuais do humanoide
    return coins.current;
  }
}

/// Propriedades do protótipo
Humanoid.prototype = Object.create( m.BioticBeing.prototype, {
  // Construtor
  constructor: { value: Humanoid }
} );
