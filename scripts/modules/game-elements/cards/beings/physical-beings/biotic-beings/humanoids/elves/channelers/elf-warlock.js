// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ElfWarlock = function ( config = {} ) {
  // Iniciação de propriedades
  ElfWarlock.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de canalização

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 2 ];

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 1;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 3 ];

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 2;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 2 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 4 ];

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 3;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 3 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 5 ];
  }

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ElfWarlock.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Elf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof ElfWarlock );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'warlock';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  ElfWarlock.gridOrder = m.data.cards.creatures.push( ElfWarlock ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
ElfWarlock.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: ElfWarlock }
} );
