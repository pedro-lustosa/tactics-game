// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HalflingBowman = function ( config = {} ) {
  // Iniciação de propriedades
  HalflingBowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 3, 'bow' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 4, 'bow' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 5, 'bow' );

      // Equipamentos

      /// Arco curto
      this.equip( new m.WeaponShortbow( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      /// Piche
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponShortbow ).equip( new m.ImplementPitch() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 6, 'bow' );

      // Equipamentos

      /// Arco longo
      this.equip( new m.WeaponLongbow( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      /// Piche
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponLongbow ).equip( new m.ImplementPitch() );
  }

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HalflingBowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Halfling.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HalflingBowman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'bowman';
  }

  // Ordem inicial das variações desta carta na grade
  HalflingBowman.gridOrder = m.data.cards.creatures.push( HalflingBowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HalflingBowman.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: HalflingBowman }
} );
