// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Goblin = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Goblin.init = function ( goblin ) {
    // Chama 'init' ascendente
    m.Humanoid.init( goblin );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( goblin instanceof Goblin );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = goblin.content.typeset;

    /// Raça
    typeset.race = 'goblin';

    /// Tamanho
    typeset.size = 'small';

    // Atribuição de propriedades de 'content.traits'
    let traits = goblin.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Emboscada Oportuna – Implementado via parte do código dos arquivos 'action-deploy' e 'combat-period'
      timelyAmbush: {
        name: 'timely-ambush'
      },
      // Recuo Matreiro – Implementado via parte do código dos arquivos 'action-retreat' e 'combat-period'
      skitteringRetreat: {
       name: 'skittering-retreat'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = goblin.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 16;

    /// Iniciativa
    currentAttributes.initiative = 12;

    /// Vigor
    currentAttributes.stamina = 4;

    /// Agilidade
    currentAttributes.agility = 6;

    /// Vontade
    currentAttributes.will = 3;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = goblin.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    adjustFighting: {
      // Identificadores
      let rangedWeapons = Object.values( fighting.weapons ).filter( weapon => weapon.type == 'ranged' ).map( weapon => weapon.name );

      // Ajuste das habilidades de combate distante
      for( let weapon of rangedWeapons ) fighting.decrease( 1, weapon, true );
    }

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = goblin.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 1 },
        range: 'M1',
        source: goblin
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: goblin
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = goblin.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 20;
  }
}

/// Propriedades do protótipo
Goblin.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Goblin }
} );
