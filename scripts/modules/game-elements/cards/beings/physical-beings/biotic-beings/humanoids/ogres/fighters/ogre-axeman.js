// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OgreAxeman = function ( config = {} ) {
  // Iniciação de propriedades
  OgreAxeman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Machados
      fighting.increase( 3, 'axe' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Machados
      fighting.increase( 4, 'axe' );

      /// Desarmado
      fighting.increase( 1, 'unarmed' );

      // Equipamentos

      /// Machado de Guerra
      this.equip( new m.WeaponBattleAxe( { size: 'large' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Machados
      fighting.increase( 5, 'axe' );

      /// Desarmado
      fighting.increase( 2, 'unarmed' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'large' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Machados
      fighting.increase( 6, 'axe' );

      /// Desarmado
      fighting.increase( 3, 'unarmed' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'large', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );

      /// Armadura de Placas
      this.equip( new m.GarmentPlateArmor( { size: 'large' } ) );
  }

  // Superconstrutor
  m.Ogre.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OgreAxeman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Ogre.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OgreAxeman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'axeman';
  }

  // Ordem inicial das variações desta carta na grade
  OgreAxeman.gridOrder = m.data.cards.creatures.push( OgreAxeman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OgreAxeman.prototype = Object.create( m.Ogre.prototype, {
  // Construtor
  constructor: { value: OgreAxeman }
} );
