// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HalflingSorcerer = function ( config = {} ) {
  // Iniciação de propriedades
  HalflingSorcerer.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      // Habilidades de canalização

      /// Powth
      channeling.paths.powth.skill = 1;

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );

      // Habilidades de canalização

      /// Powth
      channeling.paths.powth.skill = 2;

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );

      // Habilidades de canalização

      /// Powth
      channeling.paths.powth.skill = 4;

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );

      // Habilidades de canalização

      /// Powth
      channeling.paths.powth.skill = 5;
  }

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HalflingSorcerer.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Halfling.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HalflingSorcerer );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'sorcerer';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  HalflingSorcerer.gridOrder = m.data.cards.creatures.push( HalflingSorcerer ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HalflingSorcerer.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: HalflingSorcerer }
} );
