// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HalflingSpearman = function ( config = {} ) {
  // Iniciação de propriedades
  HalflingSpearman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 1, 'spear' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Lança
      this.equip( new m.WeaponDefaultSpear( { size: 'small' } ) );

      /// Broquel
      this.equip( new m.WeaponBuckler( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 2, 'spear' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Lança
      this.equip( new m.WeaponDefaultSpear( { size: 'small' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache( { size: 'small' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 3, 'spear' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur( { size: 'small' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache( { size: 'small' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 4, 'spear' );

      /// Escudos
      fighting.increase( 4, 'shield' );

      // Equipamentos

      /// Ranseur
      this.equip( new m.WeaponRanseur( { size: 'small' } ) );

      /// Escutum
      this.equip( new m.WeaponScutum( { size: 'small' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );
  }

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HalflingSpearman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Halfling.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HalflingSpearman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'spearman';
  }

  // Ordem inicial das variações desta carta na grade
  HalflingSpearman.gridOrder = m.data.cards.creatures.push( HalflingSpearman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HalflingSpearman.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: HalflingSpearman }
} );
