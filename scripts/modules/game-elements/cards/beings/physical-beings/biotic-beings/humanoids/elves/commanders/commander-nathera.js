// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderNathera = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderNathera.init( this );

  // Identificadores
  var { command, combativeness: { channeling } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de canalização

  //// Enoth
  [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 5 ];

  //// Voth
  [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 5 ];

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 5, max: 8 },
      race: {
        elf: { min: 5, max: 8 },
      },
      roleType: {
        melee: { min: 1, max: 4 },
        ranged: { max: 3 },
        magical: { min: 2, max: 5 }
      },
      experienceLevel: {
        1: { max: 8 },
        2: { max: 8 },
        3: { max: 8 },
        4: { max: 7 }
      }
    },
    spell: {
      total: { min: 6, max: 24 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2000, sideDeck: 660
  } );

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderNathera.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Elf.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderNathera );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-nathera';

    // Atribuição de propriedades de 'content'
    let content = commander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'sorcerer';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 4;

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderNathera.gridOrder = m.data.cards.commanders.push( CommanderNathera );
}

/// Propriedades do protótipo
CommanderNathera.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: CommanderNathera }
} );
