// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderIxohch = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderIxohch.init( this );

  // Identificadores
  var { command, combativeness: { fighting, channeling } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Machados
  fighting.increase( 4, 'axe' );

  /// Equipamentos

  //// Machado de guerra
  this.equip( new m.WeaponBattleAxe( { grade: 'masterpiece' } ) );

  /// Habilidades de canalização

  //// Faoth
  [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 3 ];

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 8, max: 11, constraint: {
        check: ( ...cards ) => cards.every( card => card.content.typeset.experience.level != 4 || card.content.typeset.race == 'orc' )
      } },
      race: {
        orc: { min: 5, max: 11 },
        goblin: { min: 0, max: 3 },
        ogre: { min: 0, max: 3 }
      },
      roleType: {
        melee: { min: 4, max: 9 },
        ranged: { max: 3 },
        magical: { min: 2, max: 5 }
      },
      experienceLevel: {
        1: { max: 11 },
        2: { max: 11 },
        3: { max: 11 },
        4: { max: 6 }
      }
    },
    item: {
      total: { max: 6 },
      grade: {
        ordinary: { max: 6 },
        masterpiece: { max: 6 },
        artifact: { max: 2 },
        relic: { max: 1 }
      }
    },
    spell: {
      total: { min: 10, max: 21 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2500, sideDeck: 840
  } );

  // Superconstrutor
  m.Orc.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderIxohch.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Orc.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderIxohch );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-ixohch';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = commander.content.typeset.role;

    /// Nome
    role.name = 'axeman';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = commander.content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 3;

    // Atribuição de propriedades de 'content.stats'
    let stats = commander.content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( commander ) );
  }

  // Verifica se ponto de destino pode ser ganho via o efeito de Ixohch
  CommanderIxohch.checkFatePointGain = function ( eventData = {} ) {
    // Não executa função caso não se esteja na fase da batalha
    if( !( m.GameMatch.current?.flow.phase instanceof m.BattlePhase ) ) return;

    // Identificadores
    var { eventCategory, eventType, eventTarget: humanoid, formerActivity, context } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-activity' && [ 'withdrawn', 'ended', 'unlinked' ].includes( eventType ) );
      m.oAssert( humanoid instanceof m.Humanoid );
    }

    // Não executa função caso atividade antiga do ente não tenha sido ativa ou suspensa
    if( ![ 'active', 'suspended' ].includes( formerActivity ) ) return;

    // Identificadores pós-validação
    var humanoidOwner = humanoid.getRelationships().owner,
        humanoidOpponent = m.GameMatch.current.players[ humanoidOwner.parity == 'odd' ? 'even' : 'odd' ],
        opponentCommander = humanoidOpponent.gameDeck.cards.commander;

    // Caso o comandante do oponente não seja Ixohch, encerra execução da função
    if( !( opponentCommander instanceof this ) ) return;

    // Captura jogador responsável pela inativação do humanoide
    let triggeringPlayer = context.triggeringCard ? context.triggeringCard.triggeringPlayer ?? context.triggeringCard.getRelationships().controller : null;

    // Caso humanoide não tenha sido removido via seu oponente, encerra execução da função
    if( triggeringPlayer != humanoidOpponent ) return;

    // Identifica se ponto de destino a ser concedido deve ser transitivo
    let isTransitive = [ m.Orc, m.Goblin, m.Ogre ].every( constructor => !( humanoid instanceof constructor ) );

    // Confere ao oponente do humanoide removido 1 ponto de destino
    humanoidOpponent.fatePoints.increase( 1, isTransitive ? 'transitive' : 'intransitive' );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderIxohch.gridOrder = m.data.cards.commanders.push( CommanderIxohch );
}

/// Propriedades do protótipo
CommanderIxohch.prototype = Object.create( m.Orc.prototype, {
  // Construtor
  constructor: { value: CommanderIxohch }
} );
