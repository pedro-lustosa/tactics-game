// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderDummyCommander = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderDummyCommander.init( this );

  // Identificadores
  var { command } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { max: Infinity },
      race: {
        human: { max: Infinity },
        orc: { max: Infinity },
        elf: { max: Infinity },
        dwarf: { max: Infinity },
        halfling: { max: Infinity },
        gnome: { max: Infinity },
        goblin: { max: Infinity },
        ogre: { max: Infinity }
      },
      roleType: {
        melee: { max: Infinity },
        ranged: { max: Infinity },
        magical: { max: Infinity }
      },
      experienceLevel: {
        1: { max: Infinity },
        2: { max: Infinity },
        3: { max: Infinity },
        4: { max: Infinity }
      }
    },
    item: {
      total: { max: Infinity },
      grade: {
        ordinary: { max: Infinity },
        masterpiece: { max: Infinity },
        artifact: { max: Infinity },
        relic: { max: Infinity }
      }
    },
    spell: {
      total: { max: Infinity }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 3000, sideDeck: 1000
  } );

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderDummyCommander.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Human.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderDummyCommander );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-dummy-commander';

    // Atribuição de propriedades de 'content'
    let content = commander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'civilian';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderDummyCommander.gridOrder = m.data.cards.commanders.unshift( CommanderDummyCommander );
}

/// Propriedades do protótipo
CommanderDummyCommander.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: CommanderDummyCommander }
} );
