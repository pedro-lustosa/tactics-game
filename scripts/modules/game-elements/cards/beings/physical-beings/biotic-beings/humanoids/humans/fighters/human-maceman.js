// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanMaceman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanMaceman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 1, 'mace' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Broquel
      this.equip( new m.WeaponBuckler() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 2, 'mace' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Broquel
      this.equip( new m.WeaponBuckler() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 3, 'mace' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Rodela
      this.equip( new m.WeaponRondache() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 4, 'mace' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace( { grade: 'masterpiece' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanMaceman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanMaceman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'maceman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanMaceman.gridOrder = m.data.cards.creatures.push( HumanMaceman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanMaceman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanMaceman }
} );
