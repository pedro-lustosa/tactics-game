// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GnomeSpearman = function ( config = {} ) {
  // Iniciação de propriedades
  GnomeSpearman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 1, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 2, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd( { size: 'small' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 3, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd( { size: 'small' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 4, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd( { size: 'small' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );
  }

  // Superconstrutor
  m.Gnome.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GnomeSpearman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Gnome.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GnomeSpearman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'spearman';
  }

  // Ordem inicial das variações desta carta na grade
  GnomeSpearman.gridOrder = m.data.cards.creatures.push( GnomeSpearman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GnomeSpearman.prototype = Object.create( m.Gnome.prototype, {
  // Construtor
  constructor: { value: GnomeSpearman }
} );
