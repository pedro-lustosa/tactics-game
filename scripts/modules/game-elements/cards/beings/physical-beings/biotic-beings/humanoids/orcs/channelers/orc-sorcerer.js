// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OrcSorcerer = function ( config = {} ) {
  // Iniciação de propriedades
  OrcSorcerer.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 1 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'positive', 1 ];

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'positive', 2 ];

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 3 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'positive', 3 ];

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 4 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 2 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'positive', 3 ];
  }

  // Superconstrutor
  m.Orc.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OrcSorcerer.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Orc.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OrcSorcerer );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'sorcerer';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = creature.content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  OrcSorcerer.gridOrder = m.data.cards.creatures.push( OrcSorcerer ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OrcSorcerer.prototype = Object.create( m.Orc.prototype, {
  // Construtor
  constructor: { value: OrcSorcerer }
} );
