// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanCrossbowman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanCrossbowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 1, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 2, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      /// Pavês
      this.equip( new m.ImplementPavise() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 3, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      /// Pavês
      this.equip( new m.ImplementPavise() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 4, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      /// Pavês
      this.equip( new m.ImplementPavise() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanCrossbowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanCrossbowman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'crossbowman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanCrossbowman.gridOrder = m.data.cards.creatures.push( HumanCrossbowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanCrossbowman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanCrossbowman }
} );
