// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Gnome = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Gnome.init = function ( gnome ) {
    // Chama 'init' ascendente
    m.Humanoid.init( gnome );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( gnome instanceof Gnome );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = gnome.content.typeset;

    /// Raça
    typeset.race = 'gnome';

    /// Tamanho
    typeset.size = 'small';

    // Atribuição de propriedades de 'content.traits'
    let traits = gnome.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Coletividade Mágica – Implementado via método local 'checkSharedMagic'
      sharedMagic: {
        name: 'shared-magic'
      },
      // Inatismo Vôthico – Implementado via método local 'triggerInbredVoth'
      inbredVoth: {
       name: 'inbred-voth'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = gnome.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 21;

    /// Iniciativa
    currentAttributes.initiative = 10;

    /// Vigor
    currentAttributes.stamina = 7;

    /// Agilidade
    currentAttributes.agility = 5;

    /// Vontade
    currentAttributes.will = 7;

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = gnome.content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( gnome ) );

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = combativeness.fighting;

    /// Ajusta habilidades de luta
    adjustFighting: {
      // Identificadores
      let meleeWeapons = Object.values( fighting.weapons ).filter( weapon => weapon.type == 'melee' ).map( weapon => weapon.name );

      // Ajuste das habilidades de combate próximo
      for( let weapon of meleeWeapons ) fighting.decrease( 1, weapon, true );
    }

    // Atribuição de propriedades de 'content.stats.combativeness.channeling'
    let channeling = combativeness.channeling;

    /// Ajusta habilidades de canalização
    adjustChanneling: {
      // Identificadores
      let vothPath = channeling.paths.voth;

      // Define estatísticas iniciais de Voth
      [ vothPath.polarity, vothPath.raceSkill, vothPath.skill ] = [ 'positive', 1, 1 ];
    }

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = gnome.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 1 },
        range: 'M1',
        source: gnome
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: gnome
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = gnome.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 20;
  }

  // Verificação do dono da magia para aplicação do efeito da característica 'sharedMagic'
  Gnome.checkSharedMagic = function ( spell ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( spell instanceof m.Spell );
      m.oAssert( spell.owner );
      m.oAssert( spell.activity == 'active' );
    }

    // Identificadores
    var { owner } = spell;

    // Caso dono da magia não seja um gnomo e não tenha um ente modelo que o seja, indica que efeito da característica não foi aplicado
    if( [ owner, owner.sourceBeing ].every( being => !( being instanceof this ) ) ) return false;

    // Suspende magia passada
    spell.inactivate( 'suspended', { triggeringCard: owner } );

    // Indica que efeito da característica foi aplicado
    return true;
  }

  // Ativação do efeito da característica 'inbredVoth'
  Gnome.triggerInbredVoth = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget.name.startsWith( 'pre-combat' ) );

    // Identificadores pós-validação
    var targetGnomes = m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipTokens: true, skipSideDeck: true } ).filter( card =>
          card instanceof this && [ 'active', 'suspended' ].includes( card.activity )
        );

    // Restaura mana de gnomos alvo
    for( let gnome of targetGnomes ) gnome.content.stats.combativeness.channeling.mana.restore( 2, 'voth' );
  }
}

/// Propriedades do protótipo
Gnome.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Gnome }
} );
