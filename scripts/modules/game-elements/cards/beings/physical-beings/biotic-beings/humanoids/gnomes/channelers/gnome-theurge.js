// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GnomeTheurge = function ( config = {} ) {
  // Iniciação de propriedades
  GnomeTheurge.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de canalização

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'positive', 3 ];

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 2;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'positive', 3 ];

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 2;

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'positive', 3 ];

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 2;

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'positive', 5 ];
  }

  // Superconstrutor
  m.Gnome.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GnomeTheurge.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Gnome.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GnomeTheurge );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'theurge';
  }

  // Ordem inicial das variações desta carta na grade
  GnomeTheurge.gridOrder = m.data.cards.creatures.push( GnomeTheurge ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GnomeTheurge.prototype = Object.create( m.Gnome.prototype, {
  // Construtor
  constructor: { value: GnomeTheurge }
} );
