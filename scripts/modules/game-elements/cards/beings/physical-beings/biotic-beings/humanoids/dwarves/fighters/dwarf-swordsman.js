// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DwarfSwordsman = function ( config = {} ) {
  // Iniciação de propriedades
  DwarfSwordsman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 3, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius() );

      /// Escutum
      this.equip( new m.WeaponScutum() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 3, 'sword' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { grade: 'masterpiece' } ) );

      /// Escutum
      this.equip( new m.WeaponScutum() );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 4, 'sword' );

      /// Escudos
      fighting.increase( 4, 'shield' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { grade: 'masterpiece' } ) );

      /// Escutum
      this.equip( new m.WeaponScutum( { grade: 'masterpiece' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 5, 'sword' );

      /// Escudos
      fighting.increase( 5, 'shield' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { grade: 'masterpiece' } ) );

      /// Escutum
      this.equip( new m.WeaponScutum( { grade: 'masterpiece' } ) );

      /// Armadura de placas
      this.equip( new m.GarmentPlateArmor() );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );
  }

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DwarfSwordsman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Dwarf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof DwarfSwordsman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'swordsman';
  }

  // Ordem inicial das variações desta carta na grade
  DwarfSwordsman.gridOrder = m.data.cards.creatures.push( DwarfSwordsman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
DwarfSwordsman.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: DwarfSwordsman }
} );
