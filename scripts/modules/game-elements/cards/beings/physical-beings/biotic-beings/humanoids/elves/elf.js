// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Elf = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Elf.init = function ( elf ) {
    // Chama 'init' ascendente
    m.Humanoid.init( elf );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( elf instanceof Elf );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = elf.content.typeset;

    /// Raça
    typeset.race = 'elf';

    /// Tamanho
    typeset.size = 'medium';

    // Atribuição de propriedades de 'content.traits'
    let traits = elf.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Maestria Arcana – Implementado via método local 'triggerArcaneMastery'
      arcaneMastery: {
        name: 'arcane-mastery'
      },
      // Empunhadura Exímia – Implementado via parte do código de 'getCombatModifiers', em 'combat-break-element'
      dexterousWielding: {
       name: 'dexterous-wielding'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = elf.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 32;

    /// Iniciativa
    currentAttributes.initiative = 10;

    /// Vigor
    currentAttributes.stamina = 4;

    /// Agilidade
    currentAttributes.agility = 7;

    /// Vontade
    currentAttributes.will = 8;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = elf.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'sword', 'bow' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = elf.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 2 },
        range: 'M2',
        source: elf
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: elf
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = elf.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 10;
  }

  // Ativação do efeito da característica 'arcaneMastery'
  Elf.triggerArcaneMastery = function ( deck ) {
    // Identificadores
    var deckSpells = deck.cards.spells,
        elfChannelers = deck.cards.beings.filter( being => being instanceof Elf && being.content.stats.combativeness.channeling ),
        totalCredits = 0,
        creditsByPath = {};

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deck instanceof m.Deck );

    // Define créditos atuais por senda
    for( let path of [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ] )
      creditsByPath[ path ] = elfChannelers.reduce(
        ( accumulator, current ) => accumulator += current.content.stats.combativeness.channeling.paths[ path ].skill, 0
      ) * 6;

    // Define créditos totais de magias
    totalCredits = Object.values( creditsByPath ).reduce( ( accumulator, current ) => accumulator += current, 0 );

    // Itera por magias do baralho
    for( let spell of deckSpells ) {
      // Ajusta preço atual da magia alvo, enquanto igual a seu preço base menos a quantidade de créditos restantes, até o valor mínimo de 0
      spell.coins.current = Math.max( spell.coins.base - creditsByPath[ spell.content.typeset.path ], 0 );

      // Ajusta quantidade de créditos restantes, enquanto igual a seu valor atual menos o preço base da magia alvo, até o valor mínimo de 0
      creditsByPath[ spell.content.typeset.path ] = Math.max( creditsByPath[ spell.content.typeset.path ] - spell.coins.base, 0 );
    }

    // Registra quantidade restante de créditos de magias
    Object.assign( deck.credits.spells, creditsByPath );

    // Atualiza quantidade de créditos gastos
    deck.credits.spent += totalCredits - Object.values( creditsByPath ).reduce( ( accumulator, current ) => accumulator += current, 0 );
  }
}

/// Propriedades do protótipo
Elf.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Elf }
} );
