// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderTheMagnate = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderTheMagnate.init( this );

  // Identificadores
  var { command } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 5, max: 10 },
      race: {
        human: { max: 2 },
        dwarf: { max: 4 },
        halfling: { max: 2 },
        gnome: { max: 2 },
        goblin: { max: 2 },
        ogre: { max: 1 }
      },
      roleType: {
        melee: { min: 2, max: 6 },
        ranged: { max: 4 },
        magical: { max: 4 }
      },
      experienceLevel: {
        2: { max: 3 },
        3: { max: 10 },
        4: { max: 10 }
      }
    },
    item: {
      total: { min: 6, max: 24 },
      grade: {
        ordinary: { max: 24 },
        masterpiece: { max: 24 },
        artifact: { max: 24 },
        relic: { max: 2 }
      }
    },
    spell: {
      total: { max: 18 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 3000, sideDeck: 0
  } );

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderTheMagnate.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Dwarf.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderTheMagnate );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-the-magnate';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = commander.content.typeset.role;

    /// Nome
    role.name = 'civilian';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = commander.content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    // Atribuição de propriedades de 'content.stats'
    let stats = commander.content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderTheMagnate.gridOrder = m.data.cards.commanders.push( CommanderTheMagnate );
}

/// Propriedades do protótipo
CommanderTheMagnate.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: CommanderTheMagnate }
} );
