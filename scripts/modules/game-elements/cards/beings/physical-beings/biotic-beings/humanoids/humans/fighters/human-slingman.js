// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanSlingman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanSlingman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 2, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling() );

      /// Porrete Liso
      this.equip( new m.WeaponSmoothClub() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 4, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling() );

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 5, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling() );

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Armadura Escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 7, 'sling' );

      /// Clavas
      fighting.increase( 1, 'mace' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling() );

      /// Maça Flangeada
      this.equip( new m.WeaponFlangedMace() );

      /// Armadura Escamada
      this.equip( new m.GarmentScaleArmor() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanSlingman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanSlingman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'slingman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanSlingman.gridOrder = m.data.cards.creatures.push( HumanSlingman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanSlingman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanSlingman }
} );
