// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ElfSwordsman = function ( config = {} ) {
  // Iniciação de propriedades
  ElfSwordsman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 3, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 4, 'sword' );

      // Equipamentos

      /// Espada bastarda
      this.equip( new m.WeaponBastardSword() );

      /// Espada bastarda
      this.equip( new m.WeaponBastardSword() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 6, 'sword' );

      // Equipamentos

      /// Espada bastarda
      this.equip( new m.WeaponBastardSword( { grade: 'masterpiece' } ) );

      /// Espada bastarda
      this.equip( new m.WeaponBastardSword( { grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );
  }

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ElfSwordsman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Elf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof ElfSwordsman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'swordsman';
  }

  // Ordem inicial das variações desta carta na grade
  ElfSwordsman.gridOrder = m.data.cards.creatures.push( ElfSwordsman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
ElfSwordsman.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: ElfSwordsman }
} );
