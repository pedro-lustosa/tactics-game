// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GoblinCrossbowman = function ( config = {} ) {
  // Iniciação de propriedades
  GoblinCrossbowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 1, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 2, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Maça flangeada
      this.equip( new m.WeaponFlangedMace( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 3, 'crossbow' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Maça flangeada
      this.equip( new m.WeaponFlangedMace( { size: 'small' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 4, 'crossbow' );

      /// Clavas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

      /// Maça flangeada
      this.equip( new m.WeaponFlangedMace( { size: 'small' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail( { size: 'small' } ) );
  }

  // Superconstrutor
  m.Goblin.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GoblinCrossbowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Goblin.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GoblinCrossbowman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'crossbowman';
  }

  // Ordem inicial das variações desta carta na grade
  GoblinCrossbowman.gridOrder = m.data.cards.creatures.push( GoblinCrossbowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GoblinCrossbowman.prototype = Object.create( m.Goblin.prototype, {
  // Construtor
  constructor: { value: GoblinCrossbowman }
} );
