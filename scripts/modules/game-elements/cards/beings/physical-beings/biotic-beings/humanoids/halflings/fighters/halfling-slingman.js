// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HalflingSlingman = function ( config = {} ) {
  // Iniciação de propriedades
  HalflingSlingman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 3, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 4, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 5, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'small', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 7, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'small', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      /// Pavês
      this.equip( new m.ImplementPavise( { size: 'small' } ) );
  }

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HalflingSlingman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Halfling.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HalflingSlingman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'slingman';
  }

  // Ordem inicial das variações desta carta na grade
  HalflingSlingman.gridOrder = m.data.cards.creatures.push( HalflingSlingman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HalflingSlingman.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: HalflingSlingman }
} );
