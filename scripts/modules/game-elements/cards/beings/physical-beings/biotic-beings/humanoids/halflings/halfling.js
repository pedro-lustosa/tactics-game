// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Halfling = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );

  // Configurações pós-superconstrutor

  /// Adiciona pontos de destino oriundos de 'Destino Agraciado'
  this.content.stats.fatePoints.add( { name: 'blessed-fate', baseValue: 1, currentValue: 1 } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Halfling.init = function ( halfling ) {
    // Chama 'init' ascendente
    m.Humanoid.init( halfling );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( halfling instanceof Halfling );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = halfling.content.typeset;

    /// Raça
    typeset.race = 'halfling';

    /// Tamanho
    typeset.size = 'small';

    // Atribuição de propriedades de 'content.traits'
    let traits = halfling.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Batedores Irrequietos – Implementado via parte do código dos arquivos 'action-relocate' e 'game-match'
      restlessScouting: {
        name: 'restless-scouting'
      },
      // Destino Agraciado – Implementado via operação em construtor 'Halfling'
      blessedFate: {
       name: 'blessed-fate'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = halfling.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 18;

    /// Iniciativa
    currentAttributes.initiative = 9;

    /// Vigor
    currentAttributes.stamina = 3;

    /// Agilidade
    currentAttributes.agility = 8;

    /// Vontade
    currentAttributes.will = 4;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = halfling.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'sling', 'crossbow' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = halfling.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 1 },
        range: 'M1',
        source: halfling
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: halfling
    } ) );
  }
}

/// Propriedades do protótipo
Halfling.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Halfling }
} );
