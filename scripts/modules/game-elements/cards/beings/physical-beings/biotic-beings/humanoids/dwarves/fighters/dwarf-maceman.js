// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DwarfMaceman = function ( config = {} ) {
  // Iniciação de propriedades
  DwarfMaceman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 1, 'mace' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Martelo de guerra
      this.equip( new m.WeaponWarHammer() );

      /// Áspide
      this.equip( new m.WeaponAspis() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 2, 'mace' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Martelo de guerra
      this.equip( new m.WeaponWarHammer() );

      /// Áspide
      this.equip( new m.WeaponAspis() );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 3, 'mace' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Martelo de guerra
      this.equip( new m.WeaponWarHammer( { grade: 'masterpiece' } ) );

      /// Áspide
      this.equip( new m.WeaponAspis( { grade: 'masterpiece' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 4, 'mace' );

      /// Escudos
      fighting.increase( 4, 'shield' );

      // Equipamentos

      /// Martelo de guerra
      this.equip( new m.WeaponWarHammer( { grade: 'masterpiece' } ) );

      /// Áspide
      this.equip( new m.WeaponAspis( { grade: 'masterpiece' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail() );
  }

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DwarfMaceman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Dwarf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof DwarfMaceman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'maceman';
  }

  // Ordem inicial das variações desta carta na grade
  DwarfMaceman.gridOrder = m.data.cards.creatures.push( DwarfMaceman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
DwarfMaceman.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: DwarfMaceman }
} );
