// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanSpearman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanSpearman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 1, 'spear' );

      // Equipamentos

      /// Lança
      this.equip( new m.WeaponDefaultSpear() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 2, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 3, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd() );

      /// Armadura Escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 4, 'spear' );

      // Equipamentos

      /// Alabarda
      this.equip( new m.WeaponHalberd( { grade: 'masterpiece' } ) );

      /// Armadura Escamada
      this.equip( new m.GarmentScaleArmor() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanSpearman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanSpearman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'spearman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanSpearman.gridOrder = m.data.cards.creatures.push( HumanSpearman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanSpearman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanSpearman }
} );
