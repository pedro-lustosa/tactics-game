// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanBowman = function ( config = {} ) {
  // Iniciação de propriedades
  HumanBowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 3, 'bow' );

      // Equipamentos

      /// Arco Curto
      this.equip( new m.WeaponShortbow() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 4, 'bow' );

      /// Espadas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Arco Curto
      this.equip( new m.WeaponShortbow() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 5, 'bow' );

      /// Espadas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Arco Longo
      this.equip( new m.WeaponLongbow() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Arcos
      fighting.increase( 6, 'bow' );

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Arco Longo
      this.equip( new m.WeaponLongbow() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanBowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanBowman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'bowman';
  }

  // Ordem inicial das variações desta carta na grade
  HumanBowman.gridOrder = m.data.cards.creatures.push( HumanBowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanBowman.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanBowman }
} );
