// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DwarfCrossbowman = function ( config = {} ) {
  // Iniciação de propriedades
  DwarfCrossbowman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 1, 'crossbow' );

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow() );

      /// Gládio
      this.equip( new m.WeaponGladius() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 3, 'crossbow' );

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { grade: 'masterpiece' } ) );

      /// Gládio
      this.equip( new m.WeaponGladius() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 3, 'crossbow' );

      /// Espadas
      fighting.increase( 4, 'sword' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { grade: 'masterpiece' } ) );

      /// Gládio
      this.equip( new m.WeaponGladius() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Bestas
      fighting.increase( 4, 'crossbow' );

      /// Espadas
      fighting.increase( 4, 'sword' );

      // Equipamentos

      /// Besta
      this.equip( new m.WeaponDefaultCrossbow( { grade: 'masterpiece' } ) );

      /// Gládio
      this.equip( new m.WeaponGladius() );

      /// Brigantina
      this.equip( new m.GarmentBrigandine() );

      /// Piche
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponDefaultCrossbow ).equip( new m.ImplementPitch() );
  }

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DwarfCrossbowman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Dwarf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof DwarfCrossbowman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'crossbowman';
  }

  // Ordem inicial das variações desta carta na grade
  DwarfCrossbowman.gridOrder = m.data.cards.creatures.push( DwarfCrossbowman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
DwarfCrossbowman.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: DwarfCrossbowman }
} );
