// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderDofro = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderDofro.init( this );

  // Identificadores
  var { command, combativeness: { fighting } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Espadas
  fighting.increase( 2, 'sword' );

  //// Bestas
  fighting.increase( 1, 'crossbow' );

  /// Equipamentos

  //// Espada bastarda
  this.equip( new m.WeaponBastardSword( { size: 'small' } ) );

  //// Besta
  this.equip( new m.WeaponDefaultCrossbow( { size: 'small' } ) );

  //// Brigantina
  this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

  //// Jaquetão
  this.equip( new m.GarmentGambeson( { size: 'small' } ) );

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 8, max: 8, constraint: {
        check: ( ...cards ) => cards.every( card =>
          ( card.content.typeset.race != 'halfling' || card.content.typeset.role.type != 'magical' ) && card.content.typeset.experience.grade != 'elite'
        )
      } },
      race: {
        human: { max: 1 },
        orc: { max: 1 },
        elf: { max: 1 },
        dwarf: { max: 1 },
        halfling: { min: 3, max: 3 },
        gnome: { max: 1 },
        goblin: { max: 1 },
        ogre: { max: 1 }
      },
      roleType: {
        melee: { min: 2, max: 4 },
        ranged: { min: 1, max: 3 },
        magical: { min: 1, max: 3 }
      },
      experienceLevel: {
        1: { max: 7 },
        2: { max: 7 },
        3: { max: 3 },
        4: { min: 1, max: 1 }
      }
    },
    item: {
      total: { min: 5, max: 21 },
      grade: {
        ordinary: { max: 21 },
        masterpiece: { max: 21 },
        artifact: { max: 21 },
        relic: { min: 1, max: 1 }
      }
    },
    spell: {
      total: { max: 16 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2250, sideDeck: 750
  } );

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderDofro.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Halfling.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderDofro );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-dofro';

    // Atribuição de propriedades de 'content'
    let content = commander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'swordsman';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 1;

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderDofro.gridOrder = m.data.cards.commanders.push( CommanderDofro );
}

/// Propriedades do protótipo
CommanderDofro.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: CommanderDofro }
} );
