// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Dwarf = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Dwarf.init = function ( dwarf ) {
    // Chama 'init' ascendente
    m.Humanoid.init( dwarf );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( dwarf instanceof Dwarf );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = dwarf.content.typeset;

    /// Raça
    typeset.race = 'dwarf';

    /// Tamanho
    typeset.size = 'medium';

    // Atribuição de propriedades de 'content.traits'
    let traits = dwarf.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Produção Engenhosa – Implementado via método local 'triggerIngeniousCraftsmanship'
      ingeniousCraftsmanship: {
        name: 'ingenious-craftsmanship'
      },
      // Essência Antimágica – Implementado via método local 'validatePotency'
      antimagicEssence: {
       name: 'antimagic-essence'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = dwarf.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 40;

    /// Iniciativa
    currentAttributes.initiative = 8;

    /// Vigor
    currentAttributes.stamina = 8;

    /// Agilidade
    currentAttributes.agility = 4;

    /// Vontade
    currentAttributes.will = 6;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = dwarf.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'spear', 'shield' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = dwarf.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 2 },
        range: 'M2',
        source: dwarf
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: dwarf
    } ) );

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = dwarf.content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Elementais
      mana: 3
    } );

    // Atribuição de propriedades de 'coins'
    let coins = dwarf.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 10;
  }

  // Ativação do efeito da característica 'ingeniousCraftsmanship'
  Dwarf.triggerIngeniousCraftsmanship = function ( deck ) {
    // Identificadores
    var deckItems = deck.cards.items,
        dwarvesArray = deck.cards.beings.filter( being => being instanceof Dwarf ),
        totalCredits = dwarvesArray.reduce( ( accumulator, current ) => accumulator += current.content.typeset.experience.level * 15, 0 ),
        currentCredits = totalCredits;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( deck instanceof m.Deck );

    // Itera por equipamentos do baralho
    for( let item of deckItems ) {
      // Ajusta preço atual do equipamento alvo, enquanto igual a seu preço base menos a quantidade de créditos restantes, até o valor mínimo de 0
      item.coins.current = Math.max( item.coins.base - currentCredits, 0 );

      // Ajusta quantidade de créditos restantes, enquanto igual a seu valor atual menos o preço base do equipamento alvo, até o valor mínimo de 0
      currentCredits = Math.max( currentCredits - item.coins.base, 0 );
    }

    // Registra quantidade restante de créditos de equipamentos
    deck.credits.items = currentCredits;

    // Atualiza quantidade de créditos gastos
    deck.credits.spent += totalCredits - currentCredits;
  }

  // Validação de potência da magia de ação passada ante a característica 'antimagicEssence'
  Dwarf.validatePotency = function ( action = {}, manaToAssign = action.manaToAssign ) {
    // Identificadores pré-validação
    var { target: spell } = action;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( action instanceof m.ActionChannel );
      m.oAssert( spell instanceof m.Spell );
    }

    // Identificadores pós-validação
    var spellPath = spell.content.typeset.path,
        allowedMana = manaToAssign || action.getMaxAssignableMana(),
        predictedPotency = spell.gaugePotency( action, allowedMana ),
        minPotency = 5;

    // Caso magia seja de Enoth ou Voth e sua potência prevista seja menor que a mínima permitida pela característica, indica que ela é inválida
    if( [ 'enoth', 'voth' ].includes( spellPath ) && predictedPotency < minPotency )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-invalid-by-antimagic-essence', { minPotency: minPotency } ) );

    // Indica que potência prevista da magia alvo é válida
    return true;
  }
}

/// Propriedades do protótipo
Dwarf.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Dwarf }
} );
