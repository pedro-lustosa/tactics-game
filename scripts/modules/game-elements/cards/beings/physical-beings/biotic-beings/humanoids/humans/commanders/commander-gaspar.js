// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderGaspar = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderGaspar.init( this );

  // Identificadores
  var commander = this,
      { command, combativeness: { fighting } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Arcos
  fighting.increase( 6, 'bow' );

  //// Espadas
  fighting.increase( 4, 'sword' );

  /// Equipamentos

  //// Arco longo
  this.equip( new m.WeaponLongbow( { grade: 'masterpiece' } ) );

  //// Espada bastarda
  this.equip( new m.WeaponBastardSword() );

  //// Brigantina
  this.equip( new m.GarmentBrigandine() );

  //// Jaquetão
  this.equip( new m.GarmentGambeson() );

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 7, max: 10, constraint: {
        check: ( ...cards ) => cards.every( card =>
          card.content.typeset.experience.level != 4 ||
          ( card.content.typeset.race == 'dwarf' && card.content.typeset.role.type == 'melee' && ( !card.deck || !commander.deck?.cards.some(
            deckCard => deckCard != card && deckCard.content.typeset.race == 'dwarf' && deckCard.content.typeset.experience.level == 4
          ) ) ) ||
          ( card.content.typeset.race == 'elf' && card.content.typeset.role.type == 'magical' && ( !card.deck || !commander.deck?.cards.some(
            deckCard => deckCard != card && deckCard.content.typeset.race == 'elf' && deckCard.content.typeset.experience.level == 4
          ) ) )
        )
      } },
      race: {
        human: { min: 1, max: 3 },
        elf: { min: 2, max: 4 },
        dwarf: { min: 2, max: 4 }
      },
      roleType: {
        melee: { min: 2, max: 4 },
        ranged: { min: 1, max: 4 },
        magical: { min: 2, max: 4 }
      },
      experienceLevel: {
        1: { max: 8 },
        2: { max: 8 },
        3: { max: 8 },
        4: { min: 2, max: 2 }
      }
    },
    item: {
      total: { min: 4, max: 18 },
      grade: {
        ordinary: { max: 18 },
        masterpiece: { max: 18 },
        artifact: { max: 18 },
        relic: { max: 2 }
      }
    },
    spell: {
      total: { min: 4, max: 18 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2500, sideDeck: 840
  } );

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderGaspar.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Human.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderGaspar );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-gaspar';

    // Atribuição de propriedades de 'content'
    let content = commander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'bowman';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 4;

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderGaspar.gridOrder = m.data.cards.commanders.push( CommanderGaspar );
}

/// Propriedades do protótipo
CommanderGaspar.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: CommanderGaspar }
} );
