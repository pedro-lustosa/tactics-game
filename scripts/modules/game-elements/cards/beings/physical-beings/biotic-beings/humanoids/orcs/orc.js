// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Orc = function ( config = {} ) {
  // Superconstrutor
  m.Humanoid.call( this, config );

  // Eventos

  /// Para aplicar efeito da característica 'ragingSurge'
  m.events.damageChangeEnd.suffer.add( this, eventData => ( this.ragingSurgeExecution = Orc.triggerRagingSurge( eventData ) ).next() );

  /// Para aplicar efeito da característica 'powerfulShots'
  m.events.cardUseEnd.useInOut.add( this, Orc.triggerPowerfulShots, { context: Orc } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Orc.init = function ( orc ) {
    // Chama 'init' ascendente
    m.Humanoid.init( orc );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( orc instanceof Orc );

    // Atribuição de propriedades iniciais

    /// Execução atual de 'Orc.triggerRagingSurge' tendo como alvo orque atual
    orc.ragingSurgeExecution = null;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = orc.content.typeset;

    /// Raça
    typeset.race = 'orc';

    /// Tamanho
    typeset.size = 'medium';

    // Atribuição de propriedades de 'content.traits'
    let traits = orc.content.traits;

    /// Inserção inicial das características
    Object.assign( traits, {
      // Adrenalina Feroz – Implementado via método local 'triggerRagingSurge'
      fierceAdrenaline: {
        name: 'raging-surge'
      },
      // Disparos Poderosos – Implementado via método local 'triggerPowerfulShots'
      powerfulShots: {
       name: 'powerful-shots'
      }
    } );

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = orc.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 42;

    /// Iniciativa
    currentAttributes.initiative = 11;

    /// Vigor
    currentAttributes.stamina = 6;

    /// Agilidade
    currentAttributes.agility = 4;

    /// Vontade
    currentAttributes.will = 4;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    let fighting = orc.content.stats.combativeness.fighting;

    /// Ajusta habilidades de luta
    for( let weapon of [ 'mace', 'axe' ] ) fighting.increase( 1, weapon, true );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = orc.content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverUnarmed( {
        damage: { blunt: 3 },
        range: 'M2',
        source: orc
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: orc
    } ) );

    // Atribuição de propriedades de 'coins'
    let coins = orc.coins;

    /// Incremento devido à raça do ente
    coins.raceIncrement = 5;
  }

  // Ativação do efeito da característica 'ragingSurge'
  Orc.triggerRagingSurge = function * ( eventData = {} ) {
    // Identificadores
    var { eventCategory, eventType, eventTarget: orc, mainDamage, damagePoints } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'damage-change' && eventType == 'suffer' );
      m.oAssert( orc instanceof this );
      m.oAssert( mainDamage instanceof m.GameDamage );
      m.oAssert( Number.isInteger( damagePoints ) && damagePoints > 0 );
    }

    // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
    while( m.GameMatch.getActiveBreaks().length )
      yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => orc.ragingSurgeExecution?.next() ), { once: true } );

    // Não executa função caso período não seja o do combate
    if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

    // Não executa função caso o orque não esteja ativo
    if( orc.activity != 'active' ) return;

    // Não executa função caso pontos de dano infligidos tenham sido inferiores a 10
    if( damagePoints < 10 ) return;

    // Preparo e atribuição dos marcadores de condições
    setupMarkers: {
      // Identificadores
      let markersToGain = 0,
          maxMarkers = 5;

      // Para cada 2 pontos acima de 10, incrementa marcadores da condição a ser ganha
      for( let i = 12; i <= damagePoints && markersToGain < maxMarkers; i += 2 ) markersToGain++;

      // Encerra bloco caso não haja marcadores a serem adicionados
      if( !markersToGain ) break setupMarkers;

      // Captura eventuais marcadores de encorajado e enfurecido do orque
      let emboldenedMarkers = orc.conditions.find( condition => condition instanceof m.ConditionEmboldened )?.markers ?? 0,
          enragedMarkers = orc.conditions.find( condition => condition instanceof m.ConditionEnraged )?.markers ?? 0;

      // Encerra bloco caso marcadores de encorajado e enfurecido estejam ambos em sua quantidade máxima
      if( [ emboldenedMarkers, enragedMarkers ].every( markers => markers >= maxMarkers ) ) break setupMarkers;

      // Identificadores para a parada de destino
      let { opponent: orcOpponent } = orc.getRelationships(),
          damager = mainDamage.source instanceof m.Being ? mainDamage.source : mainDamage.source?.owner ?? null,
          modalText = 'fate-break-raging-surge-' + (
            emboldenedMarkers >= maxMarkers ? 'gain-enraged' : enragedMarkers >= maxMarkers ? 'prevent-emboldened' : 'gain-enraged-prevent-emboldened'
          ),
          modalArguments = [ modalText, { orc: orc, markers: markersToGain } ];

      // Programa parada de destino para determinar condição a ser aplicada ou atualizada
      let targetCondition = yield m.GameMatch.programFateBreak( orcOpponent, {
        name: `raging-surge-fate-break-${ orc.getMatchId() }`,
        playerBeing: damager,
        opponentBeing: orc,
        isContestable: true,
        successAction: () => orc.ragingSurgeExecution.next( new m.ConditionEnraged() ),
        failureAction: () => orc.ragingSurgeExecution.next( new m.ConditionEmboldened() ),
        modalArguments: modalArguments
      } );

      // Aplica ao orque condição alvo, ou a atualiza, quando já existente
      targetCondition.apply( orc, null, markersToGain );
    }

    // Preparo e iniciação do ataque livre
    setupFreeAttack: {
      // Identificadores
      let modalArguments = [ 'confirm-free-attack-by-raging-surge-trait', { attacker: orc } ];

      // Programa ataque livre, quando aplicável
      return m.ActionAttack.programFreeAttack( orc, {
        name: `raging-surge-free-attack-${ orc.getMatchId() }`,
        finishAction: () => orc.ragingSurgeExecution = null,
        modalArguments: modalArguments
      } );
    }
  }

  // Ativação do efeito da característica 'powerfulShots'
  Orc.triggerPowerfulShots = function ( eventData = {} ) {
    // Identificadores
    var { eventCategory, eventType, eventTarget: orc, useTarget: item } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-use' && [ 'use-in', 'use-out' ].includes( eventType ) );
      m.oAssert( orc instanceof this );
      m.oAssert( item instanceof m.Card );
    }

    // Não executa função caso alvo do uso não seja um arco
    if( !( item instanceof m.WeaponBow ) ) return;

    // Delega operação da característica em função de se arco foi usado ou desusado
    return eventType == 'use-in' ? increaseStats() : decreaseStats();

    // Aumenta estatísticas atinentes de manobra 'disparar'
    function increaseStats() {
      // Identificadores
      var shootAttack = item.content.stats.effectivity.current.attacks.find( attack => attack instanceof m.ManeuverShoot );

      // Não executa função caso manobra 'disparar' não exista
      if( !shootAttack ) return;

      // Quando aplicável, aumenta penetração e alcance da manobra em 1 ponto
      for( let stat of [ 'penetration', 'range' ] ) shootAttack.applyModifier( stat, 1, 'orc-powerful-shots' );
    }

    // Reduz estatísticas atinentes de manobra 'disparar'
    function decreaseStats() {
      // Identificadores
      var shootAttack = item.content.stats.effectivity.current.attacks.find( attack => attack instanceof m.ManeuverShoot );

      // Não executa função caso manobra 'disparar' não exista
      if( !shootAttack ) return;

      // Remove modificadores pertinentes, quando existentes
      for( let stat of [ 'penetration', 'range' ] ) shootAttack.dropModifier( stat, 'orc-powerful-shots' );
    }
  }
}

/// Propriedades do protótipo
Orc.prototype = Object.create( m.Humanoid.prototype, {
  // Construtor
  constructor: { value: Orc }
} );
