// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CommanderGamomba = function ( config = {} ) {
  // Iniciação de propriedades
  CommanderGamomba.init( this );

  // Identificadores
  var { command, combativeness: { fighting } } = this.content.stats;

  // Configurações pré-superconstrutor

  /// Habilidades de luta

  //// Machados
  fighting.increase( 4, 'axe' );

  /// Equipamentos

  //// Bardiche
  this.equip( new m.WeaponBardiche( { size: 'large' } ) );

  //// Jaquetão
  this.equip( new m.GarmentGambeson( { size: 'large' } ) );

  /// Escopos do comando
  command.setScopes( {
    creature: {
      total: { min: 6, max: 9 },
      race: {
        halfling: { min: 3, max: 6 },
        goblin: { min: 3, max: 6 }
      },
      roleType: {
        melee: { min: 2, max: 6 },
        ranged: { max: 3 },
        magical: { max: 3 }
      },
      experienceLevel: {
        1: { max: 7 },
        2: { min: 2, max: 9 },
        3: { max: 7 },
        4: { max: 7 }
      }
    },
    item: {
      total: { max: 20 },
      grade: {
        ordinary: { max: 20 },
        artifact: { max: 20 }
      }
    },
    spell: {
      total: { max: 20 }
    }
  } );

  /// Moedas disponíveis
  Object.assign( command.availableCoins, {
    mainDeck: 2750, sideDeck: 920
  } );

  // Superconstrutor
  m.Ogre.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CommanderGamomba.init = function ( commander ) {
    // Chama 'init' ascendente
    m.Ogre.init( commander );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( commander instanceof CommanderGamomba );

    // Atribuição de propriedades iniciais

    /// Nome
    commander.name = 'commander-gamomba';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = commander.content.typeset.role;

    /// Nome
    role.name = 'axeman';

    // Atribuição de propriedades de 'content.typeset.experience'
    let experience = commander.content.typeset.experience;

    /// Categoria
    experience.grade = 'commander';

    /// Nível
    experience.level = 3;

    // Atribuição de propriedades de 'content.stats'
    let stats = commander.content.stats;

    /// Comando
    stats.command = Object.oAssignByCopy( {}, m.mixinCommand.call( commander ) );
  }

  // Ordem inicial das variações desta carta na grade
  CommanderGamomba.gridOrder = m.data.cards.commanders.push( CommanderGamomba );
}

/// Propriedades do protótipo
CommanderGamomba.prototype = Object.create( m.Ogre.prototype, {
  // Construtor
  constructor: { value: CommanderGamomba }
} );
