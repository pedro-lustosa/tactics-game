// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OrcMaceman = function ( config = {} ) {
  // Iniciação de propriedades
  OrcMaceman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 1, 'mace' );

      // Equipamentos

      /// Porrete Cravado
      this.equip( new m.WeaponSpikedClub() );

      /// Berrante
      this.equip( new m.ImplementHorn() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 2, 'mace' );

      // Equipamentos

      /// Gada
      this.equip( new m.WeaponGada() );

      /// Berrante
      this.equip( new m.ImplementHorn() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 3, 'mace' );

      // Equipamentos

      /// Gada
      this.equip( new m.WeaponGada() );

      /// Berrante
      this.equip( new m.ImplementHorn() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Clavas
      fighting.increase( 4, 'mace' );

      // Equipamentos

      /// Gada
      this.equip( new m.WeaponGada( { grade: 'masterpiece' } ) );

      /// Berrante
      this.equip( new m.ImplementHorn() );
  }

  // Superconstrutor
  m.Orc.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OrcMaceman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Orc.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OrcMaceman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'maceman';
  }

  // Ordem inicial das variações desta carta na grade
  OrcMaceman.gridOrder = m.data.cards.creatures.push( OrcMaceman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OrcMaceman.prototype = Object.create( m.Orc.prototype, {
  // Construtor
  constructor: { value: OrcMaceman }
} );
