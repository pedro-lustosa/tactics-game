// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HumanSorcerer = function ( config = {} ) {
  // Iniciação de propriedades
  HumanSorcerer.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 1 ];

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 1 ];

      /// Powth
      channeling.paths.powth.skill = 1;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 1 ];

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Powth
      channeling.paths.powth.skill = 1;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 1 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 2 ];

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Powth
      channeling.paths.powth.skill = 2;

      /// Voth
      [ channeling.paths.voth.polarity, channeling.paths.voth.skill ] = [ 'negative', 2 ];

      /// Faoth
      [ channeling.paths.faoth.polarity, channeling.paths.faoth.skill ] = [ 'negative', 2 ];
  }

  // Superconstrutor
  m.Human.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HumanSorcerer.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Human.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HumanSorcerer );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'sorcerer';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = creature.content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  HumanSorcerer.gridOrder = m.data.cards.creatures.push( HumanSorcerer ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HumanSorcerer.prototype = Object.create( m.Human.prototype, {
  // Construtor
  constructor: { value: HumanSorcerer }
} );
