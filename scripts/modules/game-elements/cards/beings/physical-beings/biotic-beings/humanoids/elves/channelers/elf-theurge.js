// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ElfTheurge = function ( config = {} ) {
  // Iniciação de propriedades
  ElfTheurge.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting, channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 2 ];

      /// Powth
      channeling.paths.powth.skill = 2;

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 3 ];

      /// Powth
      channeling.paths.powth.skill = 3;

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Alfange
      this.equip( new m.WeaponFalchion() );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 4 ];

      /// Powth
      channeling.paths.powth.skill = 4;

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion( { grade: 'masterpiece' } ) );

      /// Alfange
      this.equip( new m.WeaponFalchion( { grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson() );

      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'positive', 5 ];

      /// Powth
      channeling.paths.powth.skill = 5;
  }

  // Superconstrutor
  m.Elf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ElfTheurge.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Elf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof ElfTheurge );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'theurge';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  ElfTheurge.gridOrder = m.data.cards.creatures.push( ElfTheurge ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
ElfTheurge.prototype = Object.create( m.Elf.prototype, {
  // Construtor
  constructor: { value: ElfTheurge }
} );
