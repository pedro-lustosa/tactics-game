// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DwarfSpearman = function ( config = {} ) {
  // Iniciação de propriedades
  DwarfSpearman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 2, 'spear' );

      // Equipamentos

      /// Pique
      this.equip( new m.WeaponPike() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 3, 'spear' );

      // Equipamentos

      /// Pique
      this.equip( new m.WeaponPike() );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 4, 'spear' );

      // Equipamentos

      /// Pique
      this.equip( new m.WeaponPike( { grade: 'masterpiece' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Lanças
      fighting.increase( 5, 'spear' );

      // Equipamentos

      /// Pique
      this.equip( new m.WeaponPike( { grade: 'masterpiece' } ) );

      /// Armadura escamada
      this.equip( new m.GarmentScaleArmor() );
  }

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DwarfSpearman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Dwarf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof DwarfSpearman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'spearman';
  }

  // Ordem inicial das variações desta carta na grade
  DwarfSpearman.gridOrder = m.data.cards.creatures.push( DwarfSpearman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
DwarfSpearman.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: DwarfSpearman }
} );
