// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GoblinSorcerer = function ( config = {} ) {
  // Iniciação de propriedades
  GoblinSorcerer.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting, channeling } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'novice';
      case 2:
        return 'disciple';
      case 3:
        return 'adept';
      case 4:
        return 'master';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 1, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { size: 'small' } ) );

      // Habilidades de canalização

      /// Powth
      channeling.paths.powth.skill = 2;

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { size: 'small' } ) );

      // Habilidades de canalização

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'negative', 1 ];

      /// Powth
      channeling.paths.powth.skill = 3;

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { size: 'small' } ) );

      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 1;

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'negative', 2 ];

      /// Powth
      channeling.paths.powth.skill = 3;

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 3, 'sword' );

      // Equipamentos

      /// Gládio
      this.equip( new m.WeaponGladius( { size: 'small' } ) );

      // Habilidades de canalização

      /// Metoth
      channeling.paths.metoth.skill = 2;

      /// Enoth
      [ channeling.paths.enoth.polarity, channeling.paths.enoth.skill ] = [ 'negative', 3 ];

      /// Powth
      channeling.paths.powth.skill = 4;
  }

  // Superconstrutor
  m.Goblin.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GoblinSorcerer.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Goblin.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GoblinSorcerer );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'sorcerer';

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = creature.content.stats.combativeness;

    /// Habilidade de canalização
    combativeness.channeling = Object.oAssignByCopy( {}, m.mixinChanneling.call( creature ) );
  }

  // Ordem inicial das variações desta carta na grade
  GoblinSorcerer.gridOrder = m.data.cards.creatures.push( GoblinSorcerer ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GoblinSorcerer.prototype = Object.create( m.Goblin.prototype, {
  // Construtor
  constructor: { value: GoblinSorcerer }
} );
