// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const OgreSlingman = function ( config = {} ) {
  // Iniciação de propriedades
  OgreSlingman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 2, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 4, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'large' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 5, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'large', grade: 'masterpiece' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Fundas
      fighting.increase( 6, 'sling' );

      // Equipamentos

      /// Funda
      this.equip( new m.WeaponDefaultSling( { size: 'large', grade: 'masterpiece' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'large' } ) );
  }

  // Superconstrutor
  m.Ogre.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  OgreSlingman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Ogre.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof OgreSlingman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'slingman';
  }

  // Ordem inicial das variações desta carta na grade
  OgreSlingman.gridOrder = m.data.cards.creatures.push( OgreSlingman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
OgreSlingman.prototype = Object.create( m.Ogre.prototype, {
  // Construtor
  constructor: { value: OgreSlingman }
} );
