// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const HalflingSwordsman = function ( config = {} ) {
  // Iniciação de propriedades
  HalflingSwordsman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 2, 'sword' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion( { size: 'small' } ) );

      /// Broquel
      this.equip( new m.WeaponBuckler( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 3, 'sword' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Alfange
      this.equip( new m.WeaponFalchion( { size: 'small' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 4, 'sword' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos
      this.equip( new m.WeaponBastardSword( { size: 'small' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Espadas
      fighting.increase( 5, 'sword' );

      /// Escudos
      fighting.increase( 4, 'shield' );

      // Equipamentos
      this.equip( new m.WeaponBastardSword( { size: 'small' } ) );

      /// Áspide
      this.equip( new m.WeaponAspis( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Jaquetão
      this.equip( new m.GarmentGambeson( { size: 'small' } ) );
  }

  // Superconstrutor
  m.Halfling.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  HalflingSwordsman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Halfling.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof HalflingSwordsman );

    // Atribuição de propriedades de 'content'
    let content = creature.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset.role'
    let role = content.typeset.role;

    /// Nome
    role.name = 'swordsman';
  }

  // Ordem inicial das variações desta carta na grade
  HalflingSwordsman.gridOrder = m.data.cards.creatures.push( HalflingSwordsman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
HalflingSwordsman.prototype = Object.create( m.Halfling.prototype, {
  // Construtor
  constructor: { value: HalflingSwordsman }
} );
