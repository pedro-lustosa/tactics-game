// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DwarfAxeman = function ( config = {} ) {
  // Iniciação de propriedades
  DwarfAxeman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Machados
      fighting.increase( 2, 'axe' );

      // Equipamentos

      /// Machado de guerra
      this.equip( new m.WeaponBattleAxe() );

      /// Rodela
      this.equip( new m.WeaponRondache() );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Machados
      fighting.increase( 4, 'axe' );

      /// Escudos
      fighting.increase( 1, 'shield' );

      // Equipamentos

      /// Machado de guerra
      this.equip( new m.WeaponBattleAxe( { grade: 'masterpiece' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache() );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Machados
      fighting.increase( 5, 'axe' );

      /// Escudos
      fighting.increase( 2, 'shield' );

      // Equipamentos

      /// Machado de guerra
      this.equip( new m.WeaponBattleAxe( { grade: 'masterpiece' } ) );

      /// Rodela
      this.equip( new m.WeaponRondache( { grade: 'masterpiece' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Machados
      fighting.increase( 6, 'axe' );

      /// Escudos
      fighting.increase( 3, 'shield' );

      // Equipamentos

      /// Acha
      this.equip( new m.WeaponPoleaxe() );

      /// Rodela
      this.equip( new m.WeaponRondache( { grade: 'masterpiece' } ) );

      /// Cota de malhas
      this.equip( new m.GarmentChainMail() );
  }

  // Superconstrutor
  m.Dwarf.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DwarfAxeman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Dwarf.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof DwarfAxeman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'axeman';
  }

  // Ordem inicial das variações desta carta na grade
  DwarfAxeman.gridOrder = m.data.cards.creatures.push( DwarfAxeman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
DwarfAxeman.prototype = Object.create( m.Dwarf.prototype, {
  // Construtor
  constructor: { value: DwarfAxeman }
} );
