// Módulos
import * as m from '../../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GoblinAxeman = function ( config = {} ) {
  // Iniciação de propriedades
  GoblinAxeman.init( this );

  // Identificadores
  var { level = 2 } = config,
      { content, content: { typeset, typeset: { role, experience }, stats, stats: { combativeness: { fighting } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 1, 2, 3, 4 ].includes( level ) );

  // Configurações pré-superconstrutor

  /// Experiência

  //// Categoria
  experience.grade = ( function () {
    switch( level ) {
      case 1:
        return 'rookie';
      case 2:
        return 'regular';
      case 3:
        return 'veteran';
      case 4:
        return 'elite';
    }
  } )();

  //// Nível
  experience.level = level;

  /// Nome
  this.name = `${ typeset.race }-${ experience.grade }-${ role.name }`;

  /// Combatividade
  switch( experience.level ) {
    // Nível 1
    case 1:
      // Habilidades de luta

      /// Machados
      fighting.increase( 2, 'axe' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 2
    case 2:
      // Habilidades de luta

      /// Machados
      fighting.increase( 3, 'axe' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      // Encerra operação
      break;
    // Nível 3
    case 3:
      // Habilidades de luta

      /// Machados
      fighting.increase( 4, 'axe' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'small' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponBardiche ).equip( new m.ImplementPoison() );

      // Encerra operação
      break;
    // Nível 4
    case 4:
      // Habilidades de luta

      /// Machados
      fighting.increase( 5, 'axe' );

      // Equipamentos

      /// Bardiche
      this.equip( new m.WeaponBardiche( { size: 'small', grade: 'masterpiece' } ) );

      /// Brigantina
      this.equip( new m.GarmentBrigandine( { size: 'small' } ) );

      /// Veneno
      this.attachments.embedded.find( attachment => attachment instanceof m.WeaponBardiche ).equip( new m.ImplementPoison() );
  }

  // Superconstrutor
  m.Goblin.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GoblinAxeman.init = function ( creature ) {
    // Chama 'init' ascendente
    m.Goblin.init( creature );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( creature instanceof GoblinAxeman );

    // Atribuição de propriedades de 'content.typeset.role'
    let role = creature.content.typeset.role;

    /// Nome
    role.name = 'axeman';
  }

  // Ordem inicial das variações desta carta na grade
  GoblinAxeman.gridOrder = m.data.cards.creatures.push( GoblinAxeman ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GoblinAxeman.prototype = Object.create( m.Goblin.prototype, {
  // Construtor
  constructor: { value: GoblinAxeman }
} );
