// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Construct = function ( config = {} ) {
  // Identificadores
  var { content: { typeset } } = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( Construct.raceNames.includes( typeset.race ) );

  // Superconstrutor
  m.PhysicalBeing.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Nomes das raças
  Construct.raceNames = [ 'undead', 'salamander', 'golem' ];

  // Iniciação de propriedades
  Construct.init = function ( construct, summoner ) {
    // Chama 'init' ascendente
    m.PhysicalBeing.init( construct );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( construct instanceof Construct );
      m.oAssert( summoner instanceof m.Card );
      m.oAssert( summoner.deck instanceof m.MainDeck );
      m.oAssert( summoner.summons );
    }

    // Atribuição de propriedades iniciais

    /// Indica se carta é uma ficha
    construct.isToken = true;

    /// Indica convocante da carta
    construct.summoner = summoner;

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = construct.content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 0;

    /// Iniciativa
    currentAttributes.initiative = 0;

    /// Agilidade
    currentAttributes.agility = 0;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = construct.content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Elementais
      shock: Infinity,
      // Astrais
      ether: Infinity
    } );
  }
}

/// Propriedades do protótipo
Construct.prototype = Object.create( m.PhysicalBeing.prototype, {
  // Construtor
  constructor: { value: Construct }
} );
