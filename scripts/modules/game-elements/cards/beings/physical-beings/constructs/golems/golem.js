// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Golem = function ( config = {} ) {
  // Iniciação de propriedades
  Golem.init( this, config.summoner );

  // Identificadores
  var { attributes } = this.content.stats;

  // Superconstrutor
  m.Construct.call( this, config );

  // Configurações pós-superconstrutor

  /// Definição dos valores originais dos atributos
  Object.oAssignByCopy( attributes.base, attributes.current );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Golem.init = function ( golem, summoner ) {
    // Chama 'init' ascendente
    m.Construct.init( golem, summoner );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( golem instanceof Golem );

    // Atribuição de propriedades iniciais

    /// Nome
    golem.name = 'golem';

    // Atribuição de propriedades de 'content'
    let content = golem.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'golem';

    /// Tamanho
    typeset.size = 'large';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 60;

    /// Iniciativa
    currentAttributes.initiative = 6;

    /// Agilidade
    currentAttributes.agility = 2;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Bater
    fightingWeapons.bash = {
      name: 'bash',
      raceSkill: 5,
      skill: 5,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverBash( {
        recovery: 2,
        damage: { blunt: 10 },
        range: 'M3',
        source: golem
      } )
    );

    /// Defesas
    currentNaturalManeuvers.defenses.push( new m.ManeuverDodge( {
      source: golem
    } ) );

    // Atribuição de propriedades de 'content.stats.effectivity.conductivity'
    let conductivity = content.stats.effectivity.conductivity;

    /// Atual
    conductivity.current = 3;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Físicas
      blunt: 8, slash: 15, pierce: Infinity,
      // Elementais
      fire: 5
    } );
  }

  // Ordem inicial das variações desta carta na grade
  Golem.gridOrder = m.data.cards.creatures.push( Golem ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Golem.prototype = Object.create( m.Construct.prototype, {
  // Construtor
  constructor: { value: Golem }
} );
