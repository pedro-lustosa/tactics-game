// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Undead = function ( config = {} ) {
  // Iniciação de propriedades
  Undead.init( this, config.summoner );

  // Identificadores
  var { summoner, source: being } = config,
      { content: { typeset, stats, stats: { attributes, effectivity: { maneuvers } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( being instanceof m.Being );
    m.oAssert( being.deck == summoner.deck );
    m.oAssert( being.activity == 'withdrawn' );
    m.oAssert( being.content.stats.attributes.base.stamina );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${ being.content.typeset.race }-undead`;

  /// Atribuição do ente modelo do morto-vivo
  this.sourceBeing = being;

  /// Atribuição do tamanho
  typeset.size = being.content.typeset.size;

  /// Atribuição da vida
  attributes.current.health = being.content.stats.attributes.base.health;

  /// Atribuição de ataques
  Object.oAssignByCopy( maneuvers.natural.current.attacks, being.content.stats.effectivity.maneuvers.natural.base.attacks, { never: [ 'source' ] } );

  // Superconstrutor
  m.Construct.call( this, config );

  // Configurações pós-superconstrutor

  /// Definição dos valores originais dos atributos
  Object.oAssignByCopy( attributes.base, attributes.current );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Undead.init = function ( undead, summoner ) {
    // Chama 'init' ascendente
    m.Construct.init( undead, summoner );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( undead instanceof Undead );

    // Atribuição de propriedades iniciais

    /// Indica dono da carta
    undead.owner = summoner;

    /// Indica se ente não é controlável por seu jogador
    undead.isUncontrollable = true;

    /// Indica ente modelo do morto-vivo
    undead.sourceBeing = null;

    // Atribuição de propriedades de 'content'
    let content = undead.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Raça
    typeset.race = 'undead';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Iniciativa
    currentAttributes.initiative = 1;

    /// Agilidade
    currentAttributes.agility = 1;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = content.stats.combativeness.fighting.weapons;

    /// Desarmado
    fightingWeapons.unarmed = {
      name: 'unarmed',
      raceSkill: 2,
      skill: 2,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.conductivity'
    let conductivity = content.stats.effectivity.conductivity;

    /// Atual
    conductivity.current = 1;

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Físicas
      pierce: 2
    } );
  }

  // Ordem inicial das variações desta carta na grade
  Undead.gridOrder = m.data.cards.creatures.push( Undead ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
Undead.prototype = Object.create( m.Construct.prototype, {
  // Construtor
  constructor: { value: Undead }
} );
