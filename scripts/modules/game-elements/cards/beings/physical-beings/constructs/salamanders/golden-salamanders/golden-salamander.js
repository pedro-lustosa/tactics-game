// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const GoldenSalamander = function ( config = {} ) {
  // Iniciação de propriedades
  GoldenSalamander.init( this, config.summoner );

  // Superconstrutor
  m.Salamander.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GoldenSalamander.init = function ( salamander, summoner ) {
    // Chama 'init' ascendente
    m.Salamander.init( salamander, summoner );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( salamander instanceof GoldenSalamander );
      m.oAssert( summoner.content.stats.combativeness.channeling.paths.voth.skill >= 2 );
    }

    // Atribuição de propriedades iniciais

    /// Nome
    salamander.name = 'golden-salamander';

    // Atribuição de propriedades de 'content'
    let content = salamander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 24;

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverBurn( {
        recovery: 4,
        damage: { fire: 6 },
        range: 'M0',
        source: salamander
      } )
    );
  }

  // Ordem inicial das variações desta carta na grade
  GoldenSalamander.gridOrder = m.data.cards.creatures.push( GoldenSalamander ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
GoldenSalamander.prototype = Object.create( m.Salamander.prototype, {
  // Construtor
  constructor: { value: GoldenSalamander }
} );
