// Módulos
import * as m from '../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Salamander = function ( config = {} ) {
  // Identificadores
  var { attributes } = this.content.stats;

  // Superconstrutor
  m.Construct.call( this, config );

  // Configurações pós-superconstrutor

  /// Definição dos valores originais dos atributos
  Object.oAssignByCopy( attributes.base, attributes.current );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Salamander.init = function ( salamander, summoner ) {
    // Chama 'init' ascendente
    m.Construct.init( salamander, summoner );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( salamander instanceof Salamander );
      m.oAssert( summoner instanceof m.Humanoid );
      m.oAssert( summoner.content.stats.combativeness.channeling.paths.voth.skill );
      m.oAssert( summoner.content.stats.combativeness.channeling.paths.powth.skill );
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = salamander.content.typeset;

    /// Raça
    typeset.race = 'salamander';

    /// Tamanho
    typeset.size = null;

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = salamander.content.stats.attributes.current;

    /// Iniciativa
    currentAttributes.initiative = 12;

    /// Agilidade
    currentAttributes.agility = 7;

    // Atribuição de propriedades de 'content.stats.combativeness.fighting.weapons'
    let fightingWeapons = salamander.content.stats.combativeness.fighting.weapons;

    /// Incendiar
    fightingWeapons.burn = {
      name: 'burn',
      raceSkill: 3,
      skill: 3 + summoner.content.stats.combativeness.channeling.paths.powth.skill,
      type: 'melee'
    };

    // Atribuição de propriedades de 'content.stats.effectivity.resistances.current'
    let currentResistances = salamander.content.stats.effectivity.resistances.current;

    /// Resistências
    Object.assign( currentResistances, {
      // Físicas
      blunt: Infinity, slash: Infinity, pierce: Infinity,
      // Elementais
      fire: Infinity
    } );
  }
}

/// Propriedades do protótipo
Salamander.prototype = Object.create( m.Construct.prototype, {
  // Construtor
  constructor: { value: Salamander }
} );
