// Módulos
import * as m from '../../../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const IndigoSalamander = function ( config = {} ) {
  // Iniciação de propriedades
  IndigoSalamander.init( this, config.summoner );

  // Superconstrutor
  m.Salamander.call( this, config );

  // Configurações pós-superconstrutor

  /// Adiciona evento que torna salamandra incontrolável caso seu convocante seja inativado
  m.events.cardActivityEnd.inactive.add( config.summoner, this.uncontrol, { context: this } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  IndigoSalamander.init = function ( salamander, summoner ) {
    // Chama 'init' ascendente
    m.Salamander.init( salamander, summoner );

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( salamander instanceof IndigoSalamander );
      m.oAssert( summoner.content.stats.combativeness.channeling.paths.voth.polarity == 'negative' );
      m.oAssert( summoner.content.stats.combativeness.channeling.paths.voth.skill >= 2 );
    }

    // Atribuição de propriedades iniciais

    /// Nome
    salamander.name = 'indigo-salamander';

    // Torna salamandra incontrolável
    salamander.uncontrol = function ( eventData = {} ) {
      // Não executa função caso salamandra não esteja ativa
      if( this.activity != 'active' ) return false;

      // Alteração da propriedade sobre seu controle
      return this.isUncontrollable = true;
    }

    // Atribuição de propriedades de 'content'
    let content = salamander.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.stats.attributes.current'
    let currentAttributes = content.stats.attributes.current;

    /// Vida
    currentAttributes.health = 36;

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = content.stats.effectivity.maneuvers.natural.current;

    /// Ataques
    currentNaturalManeuvers.attacks.push(
      new m.ManeuverBurn( {
        recovery: 4,
        damage: { fire: 10 },
        range: 'M0',
        source: salamander
      } )
    );
  }

  // Ordem inicial das variações desta carta na grade
  IndigoSalamander.gridOrder = m.data.cards.creatures.push( IndigoSalamander ) + ( m.data.cards.creatures.length - 1 ) * 3;
}

/// Propriedades do protótipo
IndigoSalamander.prototype = Object.create( m.Salamander.prototype, {
  // Construtor
  constructor: { value: IndigoSalamander }
} );
