// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const PhysicalBeing = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats } } = this;

  // Validação
  if( m.app.isInDevelopment )
    m.oAssert(
      !typeset.size && [ m.Salamander, m.Swarm ].some( constructor => this instanceof constructor ) || [ 'small', 'medium', 'large' ].includes( typeset.size )
    );

  // Superconstrutor
  m.Being.call( this, config );

  // Eventos

  /// Para afastar ente físico caso sua vida chegue a 0
  m.events.cardContentEnd.health.add( this, this.removeOnLackOfAttribute );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  PhysicalBeing.init = function ( physicalBeing ) {
    // Chama 'init' ascendente
    m.Being.init( physicalBeing );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( physicalBeing instanceof PhysicalBeing );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = physicalBeing.content.typeset;

    /// Ambiente
    typeset.environment = 'physical';

    /// Tamanho
    typeset.size = '';
  }

  // Ajusta tamanho de ente biótico levando em consideração magias 'Crescimento' e 'Encolhimento' vinculadas a ele
  PhysicalBeing.adjustSizeByEffect = function ( being ) {
    // Identificadores pré-validação
    var sizeTypes = [ 'small', 'medium', 'large' ];

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( being instanceof this );
      m.oAssert( sizeTypes.includes( being.content.typeset.size ) );
    }

    // Identificadores para identificar para qual tamanho ente deve ser ajustado
    let baseSizeIndex = sizeTypes.indexOf( getBaseSize() ),
        sizeModifier = getSizeModifier(),
        newSizeIndex = baseSizeIndex + sizeModifier,
        targetSize = sizeTypes[ newSizeIndex ];

    // Não executa função caso tamanho do ente já seja o necessário
    if( being.content.typeset.size == targetSize ) return;

    // Identificadores para capturar diferença entre tamanho atual do ente e seu novo tamanho
    let currentSizeIndex = sizeTypes.indexOf( being.content.typeset.size ),
        sizeDifference = newSizeIndex - currentSizeIndex,
        baseSizeDifferenceMultiplier = 1 + ( sizeDifference * ( baseSizeIndex == 0 ? 1 : baseSizeIndex == 1 ? .5 : .33 ) ),
        currentSizeDifferenceMultiplier = 1 + ( sizeDifference * ( currentSizeIndex == 0 ? 1 : currentSizeIndex == 1 ? .5 : .33 ) );

    // Suspende vinculados externos do ente com tamanho
    being.getAllCardAttachments( 'external' ).filter( attachment => attachment.content.typeset.size ).forEach( card => card.inactivate( 'suspended' ) );

    // Identificadores para mudança de tamanho e emissão de evento
    let cardsToChangeSize = [ being ].concat( being.getAllCardAttachments( 'embedded' ).filter( attachment => attachment.content.typeset.size ) ),
        dataToEmit = { formerSize: being.content.typeset.size, newSize: targetSize };

    // Sinaliza início da mudança de tamanho
    cardsToChangeSize.forEach( card => m.events.cardContentStart.size.emit( card, dataToEmit ) );

    // Ajusta vida do ente
    adjustHealth: {
      // Identificadores
      let [ baseAttributes, currentAttributes ] = [ being.content.stats.attributes.base, being.content.stats.attributes.current ],
          healthPercentage = currentAttributes.health / baseAttributes.health;

      // Ajusta vida base do ente
      baseAttributes.health = Math.round( baseAttributes.health * currentSizeDifferenceMultiplier );

      // Ajusta vida atual do ente em proporção à nova vida base
      currentAttributes.health = Math.round( baseAttributes.health * healthPercentage );
    }

    // Ajusta estatísticas de manobras
    adjustManeuvers: {
      // Identificadores
      let maneuverArray = being.content.stats.effectivity.maneuvers.natural.current.attacks;

      // Adiciona ao arranjo de manobras ataques de armas embutidas do ente
      maneuverArray = maneuverArray.concat(
        being.getAllCardAttachments( 'embedded' ).filter( attachment => attachment instanceof m.Weapon && attachment.content.typeset.size ).flatMap(
          weapon => weapon.content.stats.effectivity.current.attacks
        )
      );

      // Itera por manobras do arranjo de manobras
      for( let maneuver of maneuverArray ) {
        // Filtra manobra 'Raio de Mana'
        if( maneuver instanceof m.ManeuverManaBolt ) continue;

        // Captura identificadores relacionados à penetração
        let basePenetration = maneuver.getBaseValue( 'penetration' ),
            penetrationModifier = sizeModifier && Math.round( basePenetration * baseSizeDifferenceMultiplier ) - basePenetration;

        // Altera penetração da manobra
        maneuver.applyModifier( 'penetration', penetrationModifier, 'size-change' );

        // Itera por danos da manobra
        for( let damageName in maneuver.damage ) {
          // Identificadores
          let damageStat = `damage-${ damageName }`,
              baseDamage = maneuver.getBaseValue( damageStat ),
              damageModifier = sizeModifier && Math.round( baseDamage * baseSizeDifferenceMultiplier ) - baseDamage;

          // Altera dano alvo da manobra
          maneuver.applyModifier( damageStat, damageModifier, 'size-change' );
        }

        // Filtra manobra 'Arremessar'
        if( maneuver instanceof m.ManeuverThrow ) continue;

        // Altera alcance de manobras próximas e distantes
        if( [ 'M', 'R' ].some( scope => maneuver.range[ 0 ] == scope ) ) maneuver.applyModifier( 'range', sizeModifier, 'size-change' );
      }
    }

    // Ajusta resistências
    adjustResistances: {
      // Identificadores
      let beingResistances = being.content.stats.effectivity.resistances,
          garments = being.getAllCardAttachments( 'embedded' ).filter( attachment => attachment instanceof m.Garment && attachment.content.typeset.size );

      // Itera por trajes do ente alvo
      for( let garment of garments ) {
        // Identificadores
        let garmentResistances = garment.content.stats.effectivity.current.resistances;

        // Filtra trajes sem nenhuma resistência
        if( Object.values( garmentResistances ).every( value => !value ) ) continue;

        // Sinaliza início da mudança de resistências
        m.events.cardContentStart.resistance.emit( garment, { resistance: 'any' } );

        // Retira do ente resistências do traje
        beingResistances.remove( garment );

        // Itera por resistências do traje
        for( let resistanceName in garmentResistances ) {
          // Identificadores
          let resistanceValue = garmentResistances[ resistanceName ];

          // Ajusta resistência alvo do traje
          garmentResistances[ resistanceName ] = Math.round( resistanceValue * currentSizeDifferenceMultiplier );
        }

        // Readiciona no ente resistências do traje
        beingResistances.add( garment );

        // Sinaliza fim da mudança de resistências
        m.events.cardContentEnd.resistance.emit( garment, { resistance: 'any' } );
      }
    }

    // Itera por cartas a alterar tamanho
    for( let card of cardsToChangeSize ) {
      // Altera tamanho da carta alvo
      card.content.typeset.size = targetSize;

      // Se existente, atualiza descrição do efeito da carta
      card.content.effects.description &&= m.languages.cards.getData( card ).effects ?? '';

      // Sinaliza fim da mudança de tamanho
      m.events.cardContentEnd.size.emit( card, dataToEmit );
    }

    // Funções

    /// Retorna tamanho original do ente alvo
    function getBaseSize() {
      // Retorna tamanho original do ente alvo segundo sua raça
      switch( ( being.sourceBeing ?? being ).content.typeset.race ) {
        case 'ogre':
        case 'golem':
          return 'large';
        case 'halfling':
        case 'gnome':
        case 'goblin':
        case 'wolf':
          return 'small';
        default:
          return 'medium';
      }
    }

    /// Retorna modificador do índice do tamanho original do ente alvo
    function getSizeModifier() {
      // Identificadores
      var sizeModifier = 0,
          attachedSpells = being.attachments.external.filter( attachment => attachment instanceof m.Spell );

      // Caso ente alvo tenha uma magia 'Crescimento' ativa vinculada a si, incrementa modificador de tamanho
      if( attachedSpells.some( spell => spell instanceof m.SpellGrowth && spell.content.effects.isEnabled ) ) sizeModifier++;

      // Caso ente alvo tenha uma magia 'Encolhimento' ativa vinculada a si, decrementa modificador de tamanho
      if( attachedSpells.some( spell => spell instanceof m.SpellShrinkage && spell.content.effects.isEnabled ) ) sizeModifier--;

      // Retorna modificador de tamanho
      return sizeModifier;
    }
  }
}

/// Propriedades do protótipo
PhysicalBeing.prototype = Object.create( m.Being.prototype, {
  // Construtor
  constructor: { value: PhysicalBeing }
} );
