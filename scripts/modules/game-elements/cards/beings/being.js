// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const Being = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats, stats: { attributes, effectivity, effectivity: { maneuvers, conductivity, resistances } } } } = this;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( !this.owner || this.isToken );
    m.oAssert( !typeset.attachability );
    m.oAssert( [ 'physical', 'astral' ].includes( typeset.environment ) );
    m.oAssert( Object.values( this.content.traits ).every( trait => trait.name && trait.title && trait.description ) );
    m.oAssert( [ 'attacks', 'defenses' ].every( key => maneuvers.natural.current[ key ].length <= 2 ) );
    m.oAssert( maneuvers.natural.current.attacks.every( attack => attack instanceof m.OffensiveManeuver ) );
    m.oAssert( maneuvers.natural.current.defenses.every( defense => defense instanceof m.DefensiveManeuver ) );
    m.oAssert( conductivity.current >= 0 && conductivity.current <= 3 );
  }

  // Configurações pré-superconstrutor

  /// Atribuição da imagem, se ainda não atribuída
  content.image ||= this.name + '-card';

  /// Para comandantes
  if( stats.command ) {
    // Atribui disponibilidade
    typeset.availability = 1;

    // Ajusta ordem da carta em grades
    this.gridOrder = this.constructor.gridOrder;
  }

  /// Itera por atributos manipuláveis via modificadores
  for( let attribute of [ 'initiative', 'stamina', 'will' ] ) {
    // Filtra atributos que não existam no ente alvo
    if( !( attribute in attributes.current ) ) continue;

    // Gera arranjo para registro de modificações no atributo alvo
    attributes.changes[ attribute ] = [];
  }

  /// Atribuição de manobras

  //// Primárias e secundárias
  for( let i = 0, rank = [ 'primary', 'secondary' ]; i < rank.length; i++ ) {
    // Caso exista uma manobra natural para o tipo de manobra atual, adiciona-a à fonte de manobras pertinente
    for( let property of [ 'attacks', 'defenses' ] )
      if( maneuvers.natural.current[ property ][ i ] ) maneuvers[ rank[ i ] ][ property ].push( maneuvers.natural.current[ property ][ i ] );
  }

  //// Originais
  Object.oAssignByCopy( maneuvers.natural.base, maneuvers.natural.current, { never: [ 'source' ] } );

  //// Definição inicial dos pontos das fontes de manobras
  maneuvers.setPoints();

  /// Atribuição da condutividade natural
  for( let origin of [ 'base', 'current' ] ) conductivity.natural[ origin ] = conductivity.current;

  /// Atribuição das resistências naturais
  for( let origin of [ 'base', 'current' ] ) Object.oAssignByCopy( resistances.natural[ origin ], resistances.current );

  // Para canalizadores, define configurações finais de canalização
  if( stats.combativeness.channeling ) m.mixinChanneling.close( this );

  // Superconstrutor
  m.Card.call( this, config );

  // Eventos

  /// Para atualizar eventual texto suspenso de ente segundo mudança de sua disponibilidade
  m.events.beingReadinessEnd.any.add( this, () => m.Card.updateHoverText( { currentTarget: this } ) );

  /// Para atualizar eventual conteúdo em molduras de vinculados e pertences de um ente quando ele alterar sua posição no campo
  m.events.fieldSlotCardChangeEnd.enter.add( this, Being.updateRelatedCardsInFrames, { context: Being } );

  /// Para reposicionar jogada de ente cuja iniciativa for alterada
  m.events.cardContentEnd.initiative.add( this, this.relocateMove );

  /// Para limpar dados relacionados à agência do ente
  for( let eventType of [ 'inactive', 'unlinked' ] ) m.events.cardActivityEnd[ eventType ].add( this, this.clearAgencyData );

  /// Para previnir eventuais ações de ente após sofrimento de dano
  m.events.damageChangeEnd.suffer.add( this, this.preventActionsOnDamage );

  /// Para comandantes, adiciona eventos para definir e redefinir quantidade máxima de moedas de seu conjunto de baralhos
  if( stats.command )
    for( let eventType of [ 'add', 'remove' ] ) m.events.deckChangeEnd[ eventType ].add( this, eventData => eventData.deck.setMaxCoins() );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Being.init = function ( being ) {
    // Chama 'init' ascendente
    m.Card.init( being );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( being instanceof Being );

    // Atribuição de propriedades iniciais

    /// Indica se ente não é controlável por seu jogador
    being.isUncontrollable = false;

    /// Arranjo para registro de condições do ente
    being.conditions = [];

    /// Indica preparo de ente para acionar ações
    being.readiness = {};

    /// Ativa ente
    being.activate = function ( slotName, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( slotName && typeof slotName == 'string' );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var formerUsage = this.isInUse,
          dataToEmit = {
            formerActivity: this.activity, newActivity: 'active', context: context
          };

      // Sinaliza início da mudança de atividade, se aplicável
      if( this.activity != 'active' ) m.events.cardActivityStart.active.emit( this, dataToEmit );

      // Sinaliza início da mudança de uso, se aplicável
      if( !formerUsage ) m.events.cardUseStart.useIn.emit( this, { useTarget: this } );

      // Coloca ente no campo, ou encerra função caso isso não tenha sido possível
      if( !m.GameMatch.current.environments.field.put( this, slotName ) ) return false;

      // Encerra função caso ente já esteja ativo
      if( this.activity == 'active' ) return this;

      // Altera atividade para ativa
      this.activity = 'active';

      // Indica que ente está em uso
      this.isInUse = true;

      // Habilita efeito do ente
      this.content.effects.enable();

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd.active.emit( this, dataToEmit );

      // Sinaliza fim da mudança de uso, se aplicável
      if( !formerUsage ) m.events.cardUseEnd.useIn.emit( this, { useTarget: this } );

      // Retorna ente
      return this;
    }

    /// Inativa ente
    being.inactivate = function ( newActivity, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'suspended', 'withdrawn', 'ended', 'unlinked' ].includes( newActivity ) );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var formerUsage = this.isInUse,
          dataToEmit = {
            formerActivity: this.activity, newActivity: newActivity, context: context
          };

      // Caso atividade passada seja a alheia ou ente não seja um de baralho, alheia-o
      if( newActivity == 'unlinked' || !this.deck ) return this.unlink();

      // Encerra função caso ente já esteja com atividade alvo
      if( this.activity == newActivity ) return this;

      // Sinaliza início da mudança de uso, se aplicável
      if( formerUsage ) m.events.cardUseStart.useOut.emit( this, { useTarget: this } );

      // Sinaliza início da mudança de atividade
      m.events.cardActivityStart[ newActivity ].emit( this, dataToEmit );

      // Desabilita efeito do ente
      this.content.effects.disable();

      // Quando fora da banca, coloca ente na banca
      if( !( this.slot?.environment instanceof m.Table ) ) this.deck.owner.pool.put( this );

      // Altera atividade para a passada
      this.activity = newActivity;

      // Indica que ente não está em uso
      this.isInUse = false;

      // Sinaliza fim da mudança de uso, se aplicável
      if( formerUsage ) m.events.cardUseEnd.useOut.emit( this, { useTarget: this } );

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd[ newActivity ].emit( this, dataToEmit );

      // Retorna ente
      return this;
    }

    /// Retorna cópia da carta de ente
    being.copy = function () {
      // Para comandantes
      if( this.content.stats.command ) return new this.constructor();

      // Para humanoides
      if( this instanceof m.Humanoid ) return new this.constructor( { level: this.content.typeset.experience.level } );

      // Lança erro, para casos não previstos
      throw new Error( 'This function does not support copying non-humanoid creatures.' );
    }

    /// Converte ente em um objeto literal
    being.toObject = function () {
      // Identificadores
      var dataObject = {
        constructorName: this.constructor.name
      };

      // Para comandantes, inclui tipo de ente
      if( this.content.stats.command )
        Object.assign( dataObject, { type: 'commander' } )

      // Para humanoides, inclui experiência e tipo de ente
      else if( this instanceof m.Humanoid )
        Object.assign( dataObject, { type: 'humanoid', level: this.content.typeset.experience.level } )

      // Para outros casos, indica que método não suporta conversão de ente alvo
      else throw new Error( 'Being not supported by "toObject".' );

      // Retorna objeto literal do ente
      return dataObject;
    }

    /// Reprepara ente
    being.reprepare = function () {
      // Apenas executa função caso ente esteja ativo, ou caso ele seja um goblin suspenso
      if( this.activity != 'active' && ( !( this instanceof m.Goblin ) || this.activity != 'suspended' ) ) return false;

      // Apenas executa função caso se esteja no período do combate
      if( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) ) return false;

      // Apenas executa função caso ente já tenha uma ou mais jogadas geradas
      if( m.GameMatch.current.flow.moves.every( move => move.source != this ) ) return false;

      // Apenas executa função caso ente esteja sem jogadas a serem desenvolvidas
      if( m.GameMatch.current.flow.moves.some( move => move.source == this && move.development < 1 ) ) return false;

      // Identificadores
      var beingOwner = this.getRelationships().owner,
          matchFlow = m.GameMatch.current.flow,
          segmentParities = matchFlow.segment.children,
          moveParity = segmentParities.find( parityStage => parityStage.parity == beingOwner.parity );

      // Adiciona nova jogada para o ente
      let move = new m.FlowMove( { parent: moveParity, source: this } ).add();

      // Para caso paridade em que jogada foi inserida já tenha se passado
      if( !moveParity.checkIfIsAfter( matchFlow.parity, matchFlow.segment ) ) {
        // Abre jogada
        move.isOpen = true;

        // Atualiza disponibilidade do ente
        this.readiness.update();
      }

      // Retorna ente
      return this;
    }

    /// Altera posição de jogada de ente
    being.relocateMove = function ( eventData = {} ) {
      // Não executa função caso período atual não seja o do combate
      if( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-content' && eventType == 'initiative' );

      // Identificadores pós-validação
      var matchFlow = m.GameMatch.current.flow,
          beingFirstMove = matchFlow.moves.find( move => move.source == this );

      // Não executa função caso nenhuma jogada tenha sido encontrada para o ente alvo
      if( !beingFirstMove ) return;

      // Não executa função caso primeira jogada do ente já esteja aberta
      if( beingFirstMove.isOpen ) return;

      // Não executa função caso primeira jogada do ente já tenha sido desenvolvida
      if( beingFirstMove.development ) return;

      // Para caso jogada esteja em uma paridade
      if( beingFirstMove.parent ) {
        // Retira jogada de sua paridade
        beingFirstMove.parent.children.splice( beingFirstMove.parent.children.indexOf( beingFirstMove ), 1 );

        // Desvincula jogada de sua paridade original
        beingFirstMove.parent = null;
      }

      // Recoloca jogada em uma nova paridade
      setAtNewParity: {
        // Identificadores
        let beingInitiative = this.content.stats.attributes.current.initiative,
            parityName = this.getRelationships().owner.parity + '-parity',
            targetParity = matchFlow.period.getChild( `segment-${ beingInitiative }` )?.getChild( parityName );

        // Para caso nova paridade exista
        if( targetParity ) {
          // Adiciona jogada à nova paridade
          targetParity.children.push( beingFirstMove );

          // Atualiza estágio ascendente da jogada
          beingFirstMove.parent = targetParity;
        }

        // Atualiza abertura da jogada em função de se nova paridade já se passou
        beingFirstMove.isOpen = Boolean( targetParity ) && !targetParity.checkIfIsAfter( matchFlow.parity, matchFlow.period );
      }

      // Atualiza disponibilidade do ente
      this.readiness.update();

      // Retorna ente
      return this;
    }

    /// Remove ente caso algum de seus atributos-chave chege a 0
    being.removeOnLackOfAttribute = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, context = {} } = eventData,
          beingAttributes = this.content.stats.attributes;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-content' && [ 'health', 'energy', 'stamina', 'will' ].includes( eventType ) );
        m.oAssert( eventType in beingAttributes.current );
      }

      // Caso atributo alvo tenha sido ajustado para 0, afasta seu ente
      if( !beingAttributes.current[ eventType ] ) this.inactivate( 'withdrawn', context );
    }

    /// Previne eventuais ações de ente após ele sofrer dano
    being.preventActionsOnDamage = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, mainDamage, damagePoints } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'damage-change' && eventType == 'suffer' );
        m.oAssert( mainDamage instanceof m.GameDamage );
        m.oAssert( Number.isInteger( damagePoints ) && damagePoints > 0 );
      };

      // Identificadores pós-validação
      var beingAttributes = this.content.stats.attributes,
          targetAttribute = 'health' in beingAttributes.current ? 'health' : 'energy',
          beingActions = m.GameAction.currents.filter( action => action.committer == this );

      // Não executa função caso ente não tenha ações em andamento
      if( !beingActions.length ) return;

      // Caso dano tenha sido maior que 1/4 da [vida/energia] base do ente, encerra função impedindo suas ações em andamento
      if( damagePoints > beingAttributes.base[ targetAttribute ] * .25 ) return m.GameAction.preventAllActions( beingActions );

      // Prepara parada de destino sobre mantimento de ações
      setFateBreak: {
        // Identificadores
        let { controller: beingPlayer, opponent: beingOpponent } = this.getRelationships(),
            damager = mainDamage.source instanceof Being ? mainDamage.source : mainDamage.source?.owner ?? null,
            player = damager?.getRelationships().controller == beingPlayer ? beingOpponent : beingPlayer;

        // Programa parada de destino para decidir se ações do ente serão impedidas
        return m.GameMatch.programFateBreak( player, {
          name: `prevent-actions-fate-break-${ this.getMatchId() }`,
          playerBeing: this,
          opponentBeing: damager,
          isContestable: true,
          failureAction: () => m.GameAction.preventAllActions( beingActions ),
          modalArguments: [ 'fate-break-prevent-actions', { committer: this } ]
        } );
      }
    }

    /// Restaura para o valor original estatísticas de entes que são por padrão passíveis dessa restauração
    being.resetDynamicStats = function () {
      // Identificadores
      var beingAttributes = this.content.stats.attributes,
          beingManeuvers = this.content.stats.effectivity.maneuvers,
          beingItems = being.getAllCardAttachments().filter( attachment => attachment instanceof m.Item );

      // Remoção das condições do ente
      while( this.conditions.length ) this.conditions[ 0 ].drop();

      // Redefinição dos atributos
      beingAttributes.reset( ...Object.keys( beingAttributes.current ).filter( attribute => ![ 'health', 'energy' ].includes( attribute ) ) );

      // Reajuste da agilidade de ente para que releve sua contagem de peso, se aplicável
      if( 'weightCount' in this )
        for( let item of beingItems ) this.weightCount.adjustAgility( { item: item, eventType: 'item-in' } );

      // Redefinição dos pontos de manobras
      beingManeuvers.setPoints();

      // Retorna ente
      return this;
    }

    /// Para entes inativados ou alheados, limpa dados relacionados à agência do ente
    being.clearAgencyData = function ( eventData = {} ) {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          { eventCategory } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.activity != 'active' );
        m.oAssert( eventCategory == 'card-activity' );
      }

      // Impede eventuais ações em andamento em que ente seja o acionante
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == this ) );

      // Para caso se esteja no período do combate
      if( matchFlow.period instanceof m.CombatPeriod ) {
        // Para caso ente não esteja suspenso, ou não seja um goblin
        if( this.activity != 'suspended' || !( this instanceof m.Goblin ) ) {
          // Remove jogadas vinculadas ao ente
          matchFlow.moves.filter( move => move.source == this ).forEach( move => move.remove() );

          // Atualiza disponibilidade do ente
          this.readiness.update();
        }
      }

      // Restaura para o valor original estatísticas do ente que são por padrão passíveis dessa restauração
      this.resetDynamicStats();

      // Para canalizadores afastados ou findadados
      if( this.content.stats.combativeness.channeling && [ 'withdrawn', 'ended' ].includes( this.activity ) ) {
        // Identificadores
        let { channeling } = this.content.stats.combativeness;

        // Ajusta atividade de magias em desuso
        channeling.getOwnedSpells().filter( spell => !spell.isInUse ).forEach( spell => spell.adjustDisuseActivity() );

        // Zera mana atual em sendas do canalizador
        for( let pathName in channeling.paths ) channeling.mana.reduce( channeling.mana.current[ pathName ], pathName );
      }
    }

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = being.maxAttachments;

    /// Magias
    maxAttachments.spell = 3;

    // Atribuição de propriedades de 'body'
    let body = being.body;

    /// Símbolo do ente, para sua visualização em zonas de engajamento
    body.symbol = new PIXI.Container();

    /// Montagem do símbolo do ente
    body.buildSymbol = function () {
      // Apenas executa função caso símbolo do ente ainda não tenha conteúdo
      if( this.symbol.children.length ) return this.symbol;

      // Identificadores
      var { symbol } = this;

      // Montagem do símbolo, em função de se seu ente é ou não um humanoide
      being instanceof m.Humanoid ? buildHumanoidSymbol() : buildNonHumanoidSymbol();

      // Por padrão, oculta símbolo
      symbol.visible = false;

      // Inseri símbolo em sua carta
      being.addChild( symbol );

      // Retorna símbolo
      return symbol;

      // Ajusta símbolo de humanoides
      function buildHumanoidSymbol() {
        // Identificadores
        var [ contentContainer, textContainer ] = [ new PIXI.Container(), new PIXI.Container() ],
            background = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.engagementZones[ being.content.typeset.race ] ] ),
            roleIcon = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.engagementZones[ being.content.typeset.role.type ] ] ),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.default.name, fontSize: -13
            },
            levelText = textContainer.addChild( new PIXI.BitmapText( being.content.typeset.experience.level.toString(), textStyle ) ),
            experienceIcon = textContainer.addChild( new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.misc.star ] ) );

        // Redimensionamentos

        /// Do plano de fundo
        background.oScaleByGreaterSize( m.Card.sizes.engagementZone.width );

        /// Do ícone de cargo
        roleIcon.oScaleByGreaterSize( 26 );

        /// Do ícone de experiência
        experienceIcon.oScaleByGreaterSize( Math.abs( textStyle.fontSize ) );

        // Inserções

        /// De conteúdo em seu contedor
        contentContainer.addChild( roleIcon, textContainer );

        /// Do plano de fundo e do conteúdo no símbolo
        symbol.addChild( background, contentContainer );

        // Posicionamentos

        /// Do nível de experiência
        levelText.position.set( 0, textContainer.height * .5 - levelText.height * .5 );

        /// Do ícone de experiência
        experienceIcon.position.set( levelText.x + levelText.width + 1, textContainer.height * .5 - experienceIcon.height * .5 );

        /// Do ícone de cargo
        roleIcon.position.set( contentContainer.width * .5 - roleIcon.width * .5, 0 );

        /// Do contedor do texto
        textContainer.position.set( contentContainer.width * .5 - textContainer.width * .5, roleIcon.y + roleIcon.height + 2 );

        /// Do contedor do conteúdo
        contentContainer.position.set( symbol.width * .5 - contentContainer.width * .5, symbol.height * .5 - contentContainer.height * .5 );
      }

      // Ajusta símbolo de não humanoides
      function buildNonHumanoidSymbol() {
        // Captura o ícone relativo à raça do ente alvo
        var raceIcon = new PIXI.Sprite( PIXI.utils.TextureCache[ m.assets.images.icons.races[ being.content.typeset.race ] ] );

        // Redimensiona tamanho do ícone para o do símbolo
        raceIcon.oScaleByGreaterSize( m.Card.sizes.engagementZone.width );

        // Inseri ícone no símbolo
        symbol.addChild( raceIcon );
      }
    }

    // Atribuição de propriedades de 'content'
    let content = being.content;

    /// Indica características do ente
    content.traits = {};

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Uso
    typeset.usage = 'static';

    /// Raça
    typeset.race = '';

    /// Ambiente
    typeset.environment = '';

    // Atribuição de propriedades de 'content.stats'
    let stats = content.stats;

    /// Atributos
    stats.attributes = {};

    /// Combatividade
    stats.combativeness = {};

    /// Efetividade
    stats.effectivity = {};

    /// Pontos de destino embutidos
    stats.fatePoints = {};

    // Atribuição de propriedades de 'content.stats.attributes'
    Object.oAssignByCopy( stats.attributes, m.mixinAttributes.call( being ) );

    // Atribuição de propriedades de 'content.stats.combativeness'
    let combativeness = content.stats.combativeness;

    /// Habilidade de luta
    combativeness.fighting = {};

    // Atribuição de propriedades de 'content.stats.combativeness.fighting'
    Object.oAssignByCopy( combativeness.fighting, m.mixinFighting.call( being ) );

    // Atribuição de propriedades de 'content.stats.effectivity'
    let effectivity = content.stats.effectivity;

    /// Manobras
    effectivity.maneuvers = {};

    /// Condutividade
    effectivity.conductivity = {};

    /// Resistências
    effectivity.resistances = {};

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers'
    let maneuvers = effectivity.maneuvers;

    /// Primária
    maneuvers.primary = {};

    /// Secundária
    maneuvers.secondary = {};

    /// Natural
    maneuvers.natural = {};

    /// Altera origem de fonte de manobras alvo
    maneuvers.change = function ( rank, source ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'primary', 'secondary' ].includes( rank ) );
        m.oAssert( source == 'natural' || [ m.Weapon, m.Implement ].some( constructor => source instanceof constructor ) );
      }

      // Identificadores
      var beingFighting = being.content.stats.combativeness.fighting,
          sourceManeuvers = source == 'natural' ? setNaturalManeuvers.call( this ) : source.content.stats.effectivity?.current ?? {},
          weaponName = source == 'natural' ? sourceManeuvers.attacks[ 0 ]?.name.replace( /^maneuver-/, '' ) ?? null :
                       source instanceof m.Weapon ? source.content.typeset.role.name : null;

      // Não executa função caso manobra natural alvo seja a mesma na fonte de manobras alvo
      if( source == 'natural' && this[ rank ].attacks[ 0 ] == sourceManeuvers.attacks[ 0 ] ) return this;

      // Não executa função caso equipamento alvo seja o mesmo na fonte de manobras alvo
      if( source != 'natural' && this[ rank ].source == source ) return this;

      // Adiciona manobras à fonte de manobras alvo
      for( let property of [ 'attacks', 'defenses' ] ) this[ rank ][ property ] = sourceManeuvers[ property ] ?? [];

      // Captura nova origem da fonte de manobras
      this[ rank ].source = source == 'natural' ? null : source;

      // Zera bônus de recuperação
      this[ rank ].recoveryBonus = 0;

      // Define pontos atuais da fonte de manobras
      this[ rank ].currentPoints =
        source.content?.effects.setManeuverSourcePoints ? source.content.effects.setManeuverSourcePoints( this[ rank ] ) :
        weaponName == 'mana-bolt' ? Math.max( 0, Math.min( 10 - this[ rank ].spentPoints, being.content.stats.combativeness.channeling.mana.current.powth ) ) :
        weaponName ? Math.max( beingFighting.weapons[ weaponName ].skill - this[ rank ].spentPoints, 0 ) : 0;

      // Retorna manobras
      return this;

      // Funções

      /// Define manobras naturais alvo
      function setNaturalManeuvers() {
        // Identificadores
        var sourceManeuvers = { attacks: [], defenses: [] },
            oppositeRank = rank == 'primary' ? 'secondary' : 'primary';

        // Itera por manobras naturais possíveis
        for( let i = 0; i < 2; i++ ) {
          // Itera por ataques e defesas
          for( let property of [ 'attacks', 'defenses' ] ) {
            // Caso uma manobra do tipo alvo já tenha sido incluída na fonte de manobras, filtra outras
            if( sourceManeuvers[ property ].length >= 1 ) continue;

            // Identificadores
            let maneuver = this.natural.current[ property ][ i ];

            // Filtra manobras que não existam
            if( !maneuver ) continue;

            // Filtra manobras que já existam na outra categoria
            if( this[ oppositeRank ][ property ].includes( maneuver ) ) continue;

            // Adiciona manobra alvo à fonte de manobras
            sourceManeuvers[ property ].push( maneuver );
          }
        }

        // Retorna manobras naturais alvo
        return sourceManeuvers;
      }
    }

    /// Remove conteúdo de fonte de manobras alvo
    maneuvers.remove = function ( rank ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'primary', 'secondary' ].includes( rank ) );

      // Remove manobras da fonte de manobras alvo
      for( let property of [ 'attacks', 'defenses' ] ) this[ rank ][ property ] = [];

      // Nulifica origem da fonte de manobras alvo
      this[ rank ].source = null;

      // Zera pontos atuais e bônus de recuperação da fonte de manobras alvo
      for( let property of [ 'currentPoints', 'recoveryBonus' ] ) this[ rank ][ property ] = 0;

      // Retorna manobras
      return this;
    }

    /// Define pontos das fontes de manobras
    maneuvers.setPoints = function () {
      // Identificadores
      var beingFighting = being.content.stats.combativeness.fighting;

      // Sinaliza início da mudança de pontos de fonte de manobras
      m.events.cardContentStart.maneuverPoints.emit( being, { trigger: 'setPoints' } );

      // Itera por fontes de manobras
      for( let rank of [ 'primary', 'secondary' ] ) {
        // Identificadores
        let source = this[ rank ].source,
            sourceRole = source?.content.typeset.role?.name,
            firstAttack = this[ rank ].attacks[ 0 ];

        // Zera pontos gastos e bônus de recuperação da fonte de manobras alvo
        for( let property of [ 'spentPoints', 'recoveryBonus' ] ) this[ rank ][ property ] = 0;

        // Define pontos atuais da fonte de manobras alvo
        this[ rank ].currentPoints =
          source?.content.effects.setManeuverSourcePoints ? source.content.effects.setManeuverSourcePoints( this[ rank ] ) :
          firstAttack?.name == 'maneuver-mana-bolt' ? Math.min( 10, being.content.stats.combativeness.channeling.mana.current.powth ) :
          firstAttack ? beingFighting.weapons[ sourceRole ?? firstAttack.name.replace(  /^maneuver-/, ''  ) ].skill : 0;
      }

      // Sinaliza fim da mudança de pontos de fonte de manobras
      m.events.cardContentEnd.maneuverPoints.emit( being, { trigger: 'setPoints' } );

      // Retorna manobras
      return this;
    }

    /// Gasta pontos com a manobra alvo
    maneuvers.spendPoints = function ( maneuver, number ) {
      // Identificadores
      var maneuverSource = [ this.primary, this.secondary ].find( source => [ ...source.attacks, ...source.defenses ].includes( maneuver ) );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( maneuver instanceof m.GameManeuver );
        m.oAssert( maneuverSource );
        m.oAssert( Number.isInteger( number ) && number >= 0 );
      }

      // Ajusta pontos a serem gastos para que não sejam maiores que pontos atuais da fonte de manobras passada
      number = Math.min( number, maneuverSource.currentPoints );

      // Apenas executa função caso haja pontos a serem gastos
      if( !number ) return this;

      // Sinaliza início da mudança de pontos de manobra
      m.events.cardContentStart.maneuverPoints.emit( being, { trigger: 'spendPoints' } );

      // Diminui pontos disponíveis da fonte de manobras passada
      maneuverSource.currentPoints -= number;

      // Adiciona pontos diminuídos aos gastos com a fonte de manobras passada
      maneuverSource.spentPoints += number;

      // Define bônus de recuperação da fonte de manobras como igual ao menor valor entre recuperação da manobra passada e pontos gastos
      maneuverSource.recoveryBonus = Math.min( maneuver.recovery, number );

      // Sinaliza fim da mudança de pontos de manobra
      m.events.cardContentEnd.maneuverPoints.emit( being, { trigger: 'spendPoints' } );

      // Retorna manobras
      return this;
    }

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.primary'
    let primaryManeuvers = maneuvers.primary;

    /// Ataques
    primaryManeuvers.attacks = [];

    /// Defesas
    primaryManeuvers.defenses = [];

    /// Origem
    primaryManeuvers.source = null;

    /// Pontos atuais
    primaryManeuvers.currentPoints = 0;

    /// Pontos gastos
    primaryManeuvers.spentPoints = 0;

    /// Bônus de recuperação
    primaryManeuvers.recoveryBonus = 0;

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.secondary'
    Object.oAssignByCopy( maneuvers.secondary, maneuvers.primary );

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural'
    let naturalManeuvers = maneuvers.natural;

    /// Original
    naturalManeuvers.base = {};

    /// Atual
    naturalManeuvers.current = {};

    // Atribuição de propriedades de 'content.stats.effectivity.maneuvers.natural.current'
    let currentNaturalManeuvers = naturalManeuvers.current;

    /// Ataques
    currentNaturalManeuvers.attacks = [];

    /// Defesas
    currentNaturalManeuvers.defenses = [];

    // Atribuição de propriedades de 'content.stats.effectivity.conductivity'
    Object.oAssignByCopy( effectivity.conductivity, m.mixinConductivity.call( being ) );

    // Atribuição de propriedades de 'content.stats.effectivity.resistances'
    Object.oAssignByCopy( effectivity.resistances, m.mixinResistances.call( being ) );

    // Atribuição de propriedades de 'content.stats.fatePoints'
    Object.oAssignByCopy( stats.fatePoints, m.mixinFatePoints.call( being ) );

    // Atribuição de propriedades de 'actions'
    let actions = being.actions;

    /// Custo de fluxo gasto com o acionamento de ações
    actions.spentFlowCost = 0;

    // Atribuição de propriedades de 'readiness'
    let readiness = being.readiness;

    /// Indica estado de preparo do ente
    readiness.status = 'unprepared';

    /// Indica se ente pode ser adiantado no próximo combate
    readiness.isForwardable = false;

    /// Efeitos que estão afetando a disponbilidade
    readiness.relatedEffects = {};

    /// Altera estado de preparo do ente
    readiness.change = function ( newReadiness ) {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          formerReadiness = this.status;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'unprepared', 'prepared', 'occupied', 'exhausted' ].includes( newReadiness ) );
        m.oAssert( matchFlow.period instanceof m.CombatPeriod );
        m.oAssert( newReadiness != 'prepared' || matchFlow.moves.some( move => move.source == being && move.isOpen ) );
        m.oAssert( newReadiness != 'exhausted' || !matchFlow.moves.some( move => move.source == being && move.isOpen ) );
      }

      // Encerra função caso ente já esteja com preparo alvo
      if( formerReadiness == newReadiness ) return being;

      // Sinaliza início da mudança de preparo
      m.events.beingReadinessStart[ newReadiness ].emit( being, { newReadiness: newReadiness } );

      // Altera preparo
      this.status = newReadiness;

      // Sinaliza fim da mudança de preparo
      m.events.beingReadinessEnd[ newReadiness ].emit( being, { formerReadiness: formerReadiness } );

      // Retorna ente
      return being;
    }

    /// Atualiza estado de preparo do ente segundo convenções do combate
    readiness.update = function () {
      // Identificadores
      var beingMoves = m.GameMatch.current.flow.moves.filter( move => move.source == being );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );

      // Caso o ente esteja inativo e não seja um goblin suspenso, desprepara-o
      if( being.activity != 'active' && ( being.activity != 'suspended' || !( being instanceof m.Goblin ) ) ) return this.change( 'unprepared' );

      // Caso o ente esteja com alguma ação em andamento, ocupa-o
      if( m.GameAction.currents.some( action => action.committer == being ) ) return this.change( 'occupied' );

      // Caso o ente esteja sob um efeito que o torna ocupado, ocupa-o
      if( this.relatedEffects.occupied.length ) return this.change( 'occupied' );

      // Caso o ente esteja com jogadas abertas, prepara-o
      if( beingMoves.some( move => move.isOpen ) ) return this.change( 'prepared' );

      // Caso o ente esteja com todas suas jogadas concluídas, esgota-o
      if( beingMoves.length && beingMoves.every( move => move.development >= 1 ) ) return this.change( 'exhausted' );

      // Desprepara ente
      return this.change( 'unprepared' );
    }

    // Atribuição de propriedades de 'readiness.relatedEffects'
    let readinessEffects = readiness.relatedEffects;

    /// Efeitos que tornam ente ocupado
    readinessEffects.occupied = [];
  }

  // Gera símbolos dos entes nos baralhos principais
  Being.buildDeckSymbols = function ( eventData = {} ) {
    // Identificadores
    var { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.BattlePhase );

    // Gera símbolos dos entes nos baralhos principais dos jogadores
    for( let parity of [ 'odd', 'even' ] ) m.GameMatch.current.decks[ parity ].cards.beings.forEach( being => being.body.buildSymbol() );
  }

  // Restaura estatísticas de todos os entes não alheados
  Being.restoreStats = function ( eventData = {} ) {
    // Identificadores
    var matchBeings = m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } ),
        { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget.name.startsWith( 'pre-combat' ) );

    // Restaura estatísticas de entes alvo
    for( let being of matchBeings ) being.resetDynamicStats();
  }

  // Adianta entes que devam ser adiantados
  Being.forwardBeings = function ( eventData = {} ) {
    // Identificadores
    var matchBeings = m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } ),
        { eventCategory, eventType, eventTarget } = eventData,
        { frames } = m.GameScreen.current;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget.name.startsWith( 'pre-combat' ) );

    // Itera por entes da partida
    for( let being of matchBeings ) {
      // Identificadores
      let beingAttributes = being.content.stats.attributes,
          formerForwardable = being.readiness.isForwardable;

      // Caso ente não esteja ativo, indica que ele não pode ser adiantado
      if( being.activity != 'active' ) being.readiness.isForwardable = false;

      // Caso iniciativa de ente já tenha alcançado o limite, indica que ele não pode ser adiantado
      if( beingAttributes.current.initiative >= 15 ) being.readiness.isForwardable = false;

      // Caso ente tenha estado adiantável, mas não esteja mais, transmite essa informação para molduras que estejam o exibindo
      if( formerForwardable != being.readiness.isForwardable )
        frames.filter( frame => frame.content.container?.source == being ).forEach( frame =>
          frame.content.updateCard( { targetComponents: [ 'cardHeading' ] } )
        );

      // Filtra entes que não possam ser adiantados
      if( !being.readiness.isForwardable ) continue;

      // Aplica a ente condição de adiantado
      new m.ConditionForwarded().apply( being );
    }
  }

  // Desprepara entes, e define os a poderem ser adiantados no próximo combate
  Being.resetReadiness = function ( eventData = {} ) {
    // Identificadores
    var matchBeings = m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } ),
        { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget instanceof m.CombatPeriod );

    // Itera por entes da partida
    for( let being of matchBeings ) {
      // Identificadores
      let beingMoves = m.GameMatch.current.flow.moves.filter( move => move.source == being );

      // Desprepara ente
      being.readiness.change( 'unprepared' );

      // Para caso ente tenha jogadas e nenhuma tenha sido desenvolvida
      if( beingMoves.length && beingMoves.every( move => !move.development ) ) {
        // Identificadores
        let framesWithCard = m.GameScreen.current.frames.filter( frame => frame.content.container?.source == being );

        // Define que ente pode ser adiantado no próximo combate
        being.readiness.isForwardable = true;

        // Atualiza molduras que atualmente exibem a carta
        framesWithCard.forEach( frame => frame.content.updateCard( { targetComponents: [ 'cardHeading' ] } ) );
      }
    }
  }

  // Atualiza eventual conteúdo em molduras de vinculados e pertences de um ente quando ele altera sua posição no campo
  Being.updateRelatedCardsInFrames = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: being } = eventData,
        relatedCards = being.getAllCardAttachments().concat( being.content.stats.combativeness?.channeling?.getOwnedSpells() ?? [] );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( being instanceof Being );

    // Itera por cartas relacionadas ao ente alvo
    for( let card of relatedCards ) {
      // Captura molduras atualmente exibindo carta alvo
      let framesWithCard = m.GameScreen.current.frames.filter( frame => frame.content.container?.source == card );

      // Atualiza seção de dados da carta alvo em suas molduras
      framesWithCard.forEach( frame => frame.content.updateCard( { targetComponents: [ 'cardData' ] } ) );
    }
  }
}

/// Propriedades do protótipo
Being.prototype = Object.create( m.Card.prototype, {
  // Construtor
  constructor: { value: Being }
} );
