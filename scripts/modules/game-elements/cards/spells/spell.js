// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const Spell = function ( config = {} ) {
  // Identificadores
  var { content, content: { typeset, stats, effects, stats: { current: { manaCost: currentManaCost } } } } = this;

  // Adiciona propriedades dependentes da duração da magia
  m.mixinDurationable.call( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( !this.isToAttach || typeset.attachability && typeset.duration != 'immediate' );
    m.oAssert( [ 'metoth', 'enoth', 'powth', 'voth', 'faoth' ].includes( typeset.path ) );
    m.oAssert( [ 'positive', 'apolar', 'negative' ].includes( typeset.polarity ) );
    m.oAssert( typeset.polarity == 'apolar' && !this.oppositeSpell || typeset.polarity != 'apolar' && this.oppositeSpell );
    m.oAssert( [ 'immediate', 'sustained', 'persistent', 'permanent' ].includes( typeset.duration ) );
    m.oAssert( [ /^R[0-5]$/, /^G[0-2]$/ ].some( regex => regex.test( stats.current.range ) ) );
    m.oAssert( stats.current.flowCost.toChannel >= 0 && stats.current.flowCost.toChannel <= stats.current.flowCost.maxChanneling );
    m.oAssert( stats.current.manaCost.maxUsage >= 0 && stats.current.manaCost.maxUsage <= 20 );
    m.oAssert( [ 'min', 'max' ].every( value => stats.current.manaCost.toChannel[ value ] >= 0 ) );
    m.oAssert( stats.current.manaCost.toChannel.min <= stats.current.manaCost.toChannel.max );

    // Para magias sustentadas
    if( typeset.duration == 'sustained' ) {
      m.oAssert( [ 'spiking', 'constant' ].includes( typeset.durationSubtype ) );
      m.oAssert( stats.current.flowCost.toSustain > 0 );
    }

    // Para magias persistentes
    else if( typeset.duration == 'persistent' ) {
      m.oAssert( !this.isDynamicPersistence || !stats.current.manaCost.toPersist );
      m.oAssert( stats.current.manaCost.toPersist >= 0 );
    }
  }

  // Configurações pré-superconstrutor

  /// Define como vinculáveis magias persistentes e permanentes com alvo direto
  if( typeset.attachability && [ 'persistent', 'permanent' ].includes( typeset.duration ) ) this.isToAttach = true;

  /// Para magias persistentes, define custo padrão de mana para persistência
  if( typeset.duration == 'persistent' && !this.isDynamicPersistence ) currentManaCost.toPersist ||= currentManaCost.toChannel.min;

  /// Atribuição das estatísticas originais
  Object.oAssignByCopy( stats.base, stats.current );

  /// Ordem da carta em grades
  this.gridOrder = this.constructor.gridOrder;

  // Superconstrutor
  m.Card.call( this, config );

  // Eventos

  /// Caso magia tenha um efeito a ser provocado com sua sustentação, adiciona evento para o provocar
  if( effects.sustainEffect ) m.events.cardUseEnd.sustain.add( this, effects.sustainEffect, { context: effects } );

  /// Caso magia tenha operações pós-uso a serem feitas, adiciona evento para as realizar
  if( this.finishUse ) m.events.cardUseEnd.useIn.add( this, this.finishUse );

  /// Para cartas cuja polaridade seja negativa, adiciona evento para ao fim de seu uso findar 'Apelo Celestial' e eventuais anjos ativos de seu jogador
  if( typeset.polarity == 'negative' )
    m.events.cardUseEnd.useIn.add( this, m.SpellCelestialAppeal.endCardsByNegativeSpell, { context: m.SpellCelestialAppeal } );

  /// Itera por eventos de uso e sustentação
  for( let eventType of [ 'useIn', 'sustain' ] ) {
    // Adiciona evento para reduzir custos de mana de magias 'O Cálice' após uma [canalização/sustentação]
    m.events.cardUseStart[ eventType ].add( this, m.SpellTheChalice.reduceManaCost, { context: m.SpellTheChalice } );

    // Adiciona evento para verificar ganho de pontos de destino oriundos de 'O Cálice'
    m.events.cardUseStart[ eventType ].add( this, m.SpellTheChalice.checkFatePointGain, { context: m.SpellTheChalice } );
  }

  /// Adiciona evento para atualizar atividade de vinculados da magia segundo seu desuso
  m.events.cardUseEnd.useOut.add( this, this.updateCardAttachments );

  /// Adiciona evento para controlar atividade de magias que entrarem em desuso
  m.events.cardUseEnd.useOut.add( this, this.adjustDisuseActivity );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Spell.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Card.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Spell );

    // Atribuição de propriedades iniciais

    /// Indica alvo direto da magia
    spell.target = null;

    /// Indica jogador que provocou magia
    spell.triggeringPlayer = null;

    /// Indica magia que se opõe a atual
    spell.oppositeSpell = null;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = false;

    /// Indica se magia deve gastar mana atribuído
    spell.isToSkipManaSpending = false;

    /// Indica se magia deve ser potencializada
    spell.isToEmpower = false;

    /// Arranjo para alvos adicionais da magia, quando aplicável
    spell.extraTargets = [];

    /// Ativa magia
    spell.activate = function ( channeler, context = {} ) {
      // Identificadores pré-validação
      var channelerSpells = channeler?.content?.stats?.combativeness?.channeling?.paths[ this.content.typeset.path ].spells;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( channeler instanceof m.Being );
        m.oAssert( channeler.content.stats.combativeness.channeling );
        m.oAssert( context.constructor == Object );
        m.oAssert( !this.owner || this.owner == channeler );
        m.oAssert( this.activity == 'active' || !channelerSpells.includes( this ) );
      }

      // Identificadores pós-validação
      var dataToEmit = {
            formerActivity: this.activity, newActivity: 'active', context: context
          };

      // Encerra função caso magia já esteja ativa
      if( this.activity == 'active' ) return this;

      // Sinaliza início da mudança de atividade
      m.events.cardActivityStart.active.emit( this, dataToEmit );

      // Identifica que canalizador passado é o dono da magia
      this.owner = channeler;

      // Adiciona magia alvo à lista de magias de seu canalizador
      channelerSpells.push( this );

      // Altera atividade
      this.activity = 'active';

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd.active.emit( this, dataToEmit );

      // Retorna magia
      return this;
    }

    /// Inativa magia
    spell.inactivate = function ( newActivity, context = {} ) {
      // Identificadores pré-validação
      var channelerSpells = this.owner?.content.stats.combativeness.channeling.paths[ this.content.typeset.path ].spells;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'suspended', 'withdrawn', 'ended' ].includes( newActivity ) );
        m.oAssert( context.constructor == Object );
        m.oAssert( !channelerSpells || channelerSpells.includes( this ) );
      }

      // Identificadores pós-validação
      var dataToEmit = {
            formerActivity: this.activity, newActivity: newActivity, context: context
          };

      // Encerra função caso magia já esteja com atividade alvo
      if( this.activity == newActivity ) return this;

      // Sinaliza início da mudança de atividade
      m.events.cardActivityStart[ newActivity ].emit( this, dataToEmit );

      // Para caso magia seja do baralho
      if( this.deck ) {
        // Quando fora da banca, coloca magia na banca, ou encerra função caso isso não tenha sido possível
        if( !( this.slot?.environment instanceof m.Table ) && !this.deck.owner.pool.put( this ) ) return false;
      }

      // Desusa magia, quando estiver em uso
      this.disuse();

      // Procedimentos para magias com um dono
      if( this.owner ) {
        // Nulifica dono da magia
        this.owner = null;

        // Remove magia alvo da lista de magias do canalizador
        channelerSpells.splice( channelerSpells.indexOf( this ), 1 );
      }

      // Altera atividade
      this.activity = newActivity;

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd[ newActivity ].emit( this, dataToEmit );

      // Retorna magia
      return this;
    }

    /// Usa magia
    spell.use = function ( action ) {
      // Identificadores
      var spellPath = this.content.typeset.path,
          spellStats = this.content.stats;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.owner );
        m.oAssert( this.activity == 'active' );
        m.oAssert( !this.isInUse );
        m.oAssert( !this.content.typeset.attachability || this.target );
        m.oAssert( !this.isToAttach || this.target.maxAttachments?.spell );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Sinaliza início da mudança de uso
      for( let eventTarget of [ this, this.owner ] ) m.events.cardUseStart.useIn.emit( eventTarget, { useTarget: this, action: action } );

      // Identifica que magia está sendo usada
      this.isInUse = true;

      // Captura jogador que provocou magia
      this.triggeringPlayer = this.getRelationships().controller;

      // Para caso haja mana atribuído para a magia
      if( action.manaToAssign ) {
        // Identificadores
        let ownerMana = this.owner.content.stats.combativeness.channeling.mana;

        // Se aplicável, gasta o mana para a canalização da magia
        if( !this.isToSkipManaSpending ) ownerMana.reduce( this.applyPolarityBurden( action.manaToAssign ), spellPath );

        // Quando existente, gasta mana oriundo de magias adjuntas
        ownerMana.reduce( action.adjunctManaToAssign, 'metoth' );

        // Define custo de uso inicial da magia
        spellStats.current.manaCost.usage = action.manaToAssign;

        // Define potência da magia
        this.setPotency( this.gaugePotency( action ) );

        // Para magias persistentes, ajusta custo de persistência da magia, tendo como referência mesmos ajustes feitos à sua potência
        spellStats.current.manaCost.toPersist &&= this.gaugePotency( action, spellStats.current.manaCost.toPersist );
      }

      // Caso magia deva ser vinculada ao alvo, realiza esse processo
      if( this.isToAttach ) this.target.attach( this );

      // Verifica se magia deve ter sua potência reduzida, segundo o efeito de 'Pingente de Oricalco'
      m.ImplementOrichalcumPendant.checkEffectActivation( spell );

      // Resolve resolução de eventuais disputas mágicas, ativando o efeito da magia quando aplicável
      this.handleSpellClash();

      // Sinaliza fim da mudança de uso
      for( let eventTarget of [ this, this.owner ] ) m.events.cardUseEnd.useIn.emit( eventTarget, { useTarget: this, action: action } );

      // Retorna magia
      return this;
    }

    /// Desusa magia
    spell.disuse = function () {
      // Não executa função caso magia não esteja sendo usada
      if( !this.isInUse ) return false;

      // Sinaliza início da mudança de uso
      for( let eventTarget of [ this, this.owner ] )
        if( eventTarget ) m.events.cardUseStart.useOut.emit( eventTarget, { useTarget: this } );

      // Resolve resolução de eventuais disputas mágicas, sempre desativando o efeito desta magia
      this.handleSpellClash( true );

      // Identifica que magia não está mais sendo usada
      this.isInUse = false;

      // Limpa dados diversos da magia
      this.clearData();

      // Sinaliza fim da mudança de uso
      for( let eventTarget of [ this, this.owner ] )
        if( eventTarget ) m.events.cardUseEnd.useOut.emit( eventTarget, { useTarget: this } );

      // Retorna magia
      return this;
    }

    /// Retorna cópia da carta de magia
    spell.copy = function () {
      // Retorna cópia da carta
      return new this.constructor();
    }

    /// Converte magia em um objeto literal
    spell.toObject = function () {
      // Retorna objeto literal da magia
      return { constructorName: this.constructor.name, type: 'spell' };
    }

    /// Define potência da magia
    spell.setPotency = function ( number = 0 ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( number ) && number <= this.content.stats.current.manaCost.maxUsage );

      // Identificadores
      var effectiveNumber = Math.max( number, 0 ),
          changeNumber = effectiveNumber - this.content.stats.current.potency;

      // Encerra função caso potência atual da magia já seja igual à nova
      if( this.content.stats.current.potency == effectiveNumber ) return this;

      // Sinaliza início da mudança de potência
      m.events.cardContentStart.potency.emit( this, { changeNumber: changeNumber } );

      // Define potência atual da magia
      this.content.stats.current.potency = effectiveNumber;

      // Caso potência base ou atual da magia seja 0, ajusta potência base para a atual
      if( !this.content.stats.base.potency || !this.content.stats.current.potency ) this.content.stats.base.potency = this.content.stats.current.potency;

      // Sinaliza fim da mudança de potência
      m.events.cardContentEnd.potency.emit( this, { changeNumber: changeNumber } );

      // Retorna magia
      return this;
    }

    /// Calcula potência prevista da magia
    spell.gaugePotency = function ( action, manaPoints = action?.manaToAssign || action?.getMaxAssignableMana() ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( Number.isInteger( manaPoints ) && manaPoints >= 0 );
      }

      // Caso magia deva ser potencializada, dobra pontos de mana passados
      if( this.isToEmpower ) manaPoints *= 2;

      // Ajusta pontos de mana para que não ultrapassem custo máximo de uso da magia
      manaPoints = Math.min( manaPoints, this.content.stats.current.manaCost.maxUsage );

      // Retorna mana ajustado
      return manaPoints;
    }

    /// Verifica resolução de uma disputa mágica
    spell.handleSpellClash = function ( isExclusive = false ) {
      // Não executa função caso magia não esteja em uso
      if( !this.isInUse ) return this;

      // Identificadores
      var spellTitle = this.content.designation.title,
          clashingSpells = [];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( typeof isExclusive == 'boolean' );

      // Caso magia tenha um vinculante, adiciona ao arranjo de magias disputantes magias de mesmo título vinculadas ao mesmo alvo
      if( this.attacher )
        clashingSpells = clashingSpells.concat( this.attacher.attachments.external.filter( card => card.content.designation.title == spellTitle ) )

      // Do contrário
      else {
        // Identificadores
        let playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

        // Adiciona ao arranjo de magias disputantes magias em uso de mesmo título, ativadas pelo mesmo jogador e sem um alvo direto
        clashingSpells = clashingSpells.concat( playerSpells.filter(
          card => card.content.designation.title == spellTitle && card.isInUse && card.triggeringPlayer == this.triggeringPlayer
        ) );
      }

      // Quando aplicável, adiciona ao arranjo de magias disputantes magias especificadas pela carta alvo
      clashingSpells = clashingSpells.concat( this.addClashingSpells?.() ?? [] );

      // Para caso magia alvo não deva ser incluída na disputa mágica
      if( isExclusive ) {
        // Se existente lá, retira magia alvo do arranjo de magias disputantes
        if( clashingSpells.indexOf( this ) != -1 ) clashingSpells.splice( clashingSpells.indexOf( this ), 1 );

        // Desabilita efeito da magia alvo
        this.content.effects.disable();
      }

      // Filtra magias que estejam vinculadas à magia 'Congelar'
      clashingSpells = clashingSpells.filter( card => !card.attachments.external.some( attachment =>
        attachment instanceof m.SpellFreeze && attachment.content.effects.isEnabled
      ) );

      // Encerra função caso não haja magias disputantes
      if( !clashingSpells.length ) return this;

      // Ordena magias disputantes
      clashingSpells.sort( sortClashingSpells.bind( this ) );

      // Desabilita efeito das magias que não a primeira no arranjo de magias disputantes
      clashingSpells.slice( 1 ).forEach( card => card.content.effects.disable() );

      // Captura primeira magia no arranjo de magias disputantes
      let winnerSpell = clashingSpells[ 0 ];

      // Habilita efeito da magia vencedora da disputa, contanto que ela tenha potência ou que ela não tenha custo de uso
      winnerSpell.content.effects[ winnerSpell.content.stats.current.potency || !winnerSpell.content.stats.current.manaCost.usage ? 'enable' : 'disable' ]();

      // Retorna magia alvo
      return this;

      // Funções

      /// Ordena magias disputantes
      function sortClashingSpells( a, b ) {
        // Identificadores
        var cardValues = [];

        // Atribuição de pontuação comparativa
        for( let card of [ a, b ] ) {
          // Identificadores
          let cardValue = '';

          // Por potência atual
          cardValue += card.content.stats.current.potency;

          // Por ativação do efeito
          cardValue += card.content.effects.isEnabled ? '1' : '0';

          // Por identidade da carta
          cardValue += card == this ? '1' : '0';

          // Captura da pontuação da carta
          cardValues.push( Number( cardValue ) );
        }

        // Compara valor da primeira carta com a segunda, e as ordena decrescentemente
        return cardValues[ 1 ] - cardValues[ 0 ];
      }
    }

    /// Aplica ônus de polaridade a mana passado, quando aplicável
    spell.applyPolarityBurden = function ( manaPoints, channeler = this.owner ) {
      // Identificadores
      var spellPolarity = this.content.typeset.polarity,
          channelerPath = channeler.content.stats.combativeness.channeling.paths[ this.content.typeset.path ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( manaPoints >= 0 );
        m.oAssert( Number.isInteger( manaPoints ) )
        m.oAssert( !this.owner || this.owner == channeler );
      }

      // Não altera o mana se magia for apolar
      if( spellPolarity == 'apolar' ) return manaPoints;

      // Quando necessário, ajusta o mana para que considere o ônus de polaridade
      if( spellPolarity != channelerPath.polarity ) manaPoints = Math.round( manaPoints * ( 1 + .2 * channelerPath.skill ) );

      // Retorna mana ajustado
      return manaPoints;
    }

    /// Ajusta atividade de magias em desuso segundo seu dono
    spell.adjustDisuseActivity = function ( eventData = {} ) {
      // Identificadores
      var { eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !this.isInUse );
        m.oAssert( this.owner );
        m.oAssert( !eventType || eventType == 'use-out' );
      }

      // Caso característica 'sharedMagic' tenha sido aplicada, encerra função
      if( m.Gnome.checkSharedMagic( this ) ) return;

      // Caso dono da magia tenha sido removido, altera atividade da magia para a de seu dono
      if( [ 'withdrawn', 'ended', 'unlinked' ].includes( this.owner.activity ) ) return this.inactivate( this.owner.activity, { triggeringCard: this.owner } );
    }

    /// Limpa dados de magias que já interagiram com a partida
    spell.clearData = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !this.isInUse );

      // Quando aplicável, nulifica execução atual do efeito da magia
      this.content.effects.currentExecution &&= null;

      // Quando aplicável, limpa dados próprios da magia
      this.clearCustomData?.();

      // Nulifica alvo da magia, e jogador que a provocou
      for( let key of [ 'target', 'triggeringPlayer' ] ) this[ key ] = null;

      // Limpa arranjo de alvos adicionais da magia
      while( this.extraTargets.length ) this.extraTargets.shift();

      // Zera custo de uso da magia
      this.content.stats.current.manaCost.usage = 0;

      // Zera potência da magia
      this.setPotency();

      // Desvincula magia de vinculante, quando existente
      this.attacher?.detach( this );

      // Para magias sustentadas
      if( this.content.typeset.duration == 'sustained' ) {
        // Redefine indicação de se magia não deve (mais) ser sustentada
        this.isToStopSustain = false;
      }

      // Para magias persistentes
      else if( this.content.typeset.duration == 'persistent' ) {
        // Nulifica segmento de uso
        this.usageSegment = null;

        // Redefine custo de persistência
        this.content.stats.current.manaCost.toPersist = this.content.stats.base.manaCost.toPersist;
      }
    }

    // Atribuição de propriedades de 'maxAttachments'
    let maxAttachments = spell.maxAttachments;

    /// Magias
    maxAttachments.spell = 1;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Uso
    typeset.usage = 'dynamic';

    /// Senda
    typeset.path = '';

    /// Polaridade
    typeset.polarity = 'apolar';

    /// Duração
    typeset.duration = '';

    // Atribuição de propriedades de 'content.stats'
    let stats = spell.content.stats;

    /// Original
    stats.base = {};

    /// Atual
    stats.current = {};

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = stats.current;

    /// Alcance
    currentStats.range = '';

    /// Custo de fluxo
    currentStats.flowCost = {};

    /// Custo de mana
    currentStats.manaCost = {};

    /// Potência
    currentStats.potency = 0;

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 0;

    /// Custo de fluxo máximo para canalização
    currentFlowCost.maxChanneling = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para canalização
    currentManaCost.toChannel = {};

    // Custo de mana durante um uso da magia
    currentManaCost.usage = 0;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 20;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 0;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 0;
  }

  // Valida custo de mana da magia alvo
  Spell.validateManaCost = function ( action, spell = action?.target ) {
    // Identificadores pré-validação
    var channeler = action?.committer;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
      m.oAssert( spell instanceof this );
      m.oAssert( !action.target || spell == action.target || action.target.isAdjunct );
      m.oAssert( channeler?.content.stats.combativeness.channeling );
    }

    // Identificadores pós-validação
    var { adjunctSpells = [] } = action,
        spellPath = spell.content.typeset.path,
        spellManaCost = spell.content.stats.current.manaCost,
        isAstralBeing = channeler instanceof m.AstralBeing,
        channelerMana = channeler.content.stats.combativeness.channeling.mana,
        totalMana = isAstralBeing ? channeler.content.stats.attributes.current.energy : channelerMana.current.total,
        pathMana = isAstralBeing ? Math.min( totalMana, channelerMana.base[ spellPath ] ) : channelerMana.current[ spellPath ],
        metothMana = isAstralBeing ? Math.min( totalMana, channelerMana.base.metoth ) : channelerMana.current.metoth,
        manaToAssign = action.manaToAssign || spellManaCost.toChannel.min,
        adjustedManaToAssign = spell.applyPolarityBurden( manaToAssign, channeler ),
        adjunctManaToAssign = action.adjunctManaToAssign || action.getAdjunctSpellsMana?.( manaToAssign ) || 0;

    // Invalida custo de mana caso ele esteja acima do mana disponível a seu canalizador
    if( pathMana < adjustedManaToAssign )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-mana-cost-must-be-paid', {
        isEnergy: isAstralBeing,
        isWithAdjunct: !spell.isAdjunct && adjunctSpells.length,
        polarityBurden: manaToAssign != adjustedManaToAssign
      } ) );

    // Invalida custo de mana caso custo de mana das magias adjuntas esteja acima do mana disponível de Metoth de seu canalizador
    if( metothMana < adjunctManaToAssign )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'adjunct-spells-mana-cost-must-be-paid', {
        isEnergy: isAstralBeing
      } ) );

    // Invalida custo de mana caso magia seja de Metoth e seu custo de mana somado ao das magias adjuntas esteja acima do mana disponível de Metoth de seu canalizador
    if( spellPath == 'metoth' && metothMana < adjustedManaToAssign + adjunctManaToAssign )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-metoth-mana', {
        isEnergy: isAstralBeing
      } ) );

    // Invalida custo de mana caso ele somado ao das magias adjuntas esteja acima do mana total de seu canalizador
    if( totalMana < adjustedManaToAssign + adjunctManaToAssign )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'main-spell-and-adjunct-spells-mana-cost-must-be-less-than-total-mana', {
        isEnergy: isAstralBeing
      } ) );

    // Indica que custo de mana é válido
    return true;
  }

  // Valida alvo da magia alvo
  Spell.validateTarget = function ( action, target = action?.target.target ) {
    // Identificadores pré-validação
    var { committer, target: spell } = action;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
      m.oAssert( spell instanceof this );
      m.oAssert( !spell.owner || spell.owner == committer );
      m.oAssert( !spell.target || spell.target == target || spell.extraTargets.includes( target ) );
    }

    // Identificadores pós-validação
    var { path: spellPath, attachability: spellAttachability } = spell.content.typeset,
        committerController = committer.getRelationships().controller,
        isCardSlot = target instanceof m.CardSlot;

    // Invalida alvo caso ele não seja de um tipo válido para qualquer magia
    if( [ m.Swarm, m.Angel ].some( constructor => target instanceof constructor ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'being-cannot-be-direct-target-of-spells' ) );

    // Invalida alvo caso ele não seja de um tipo válido para a magia alvo
    if( !( target instanceof spellAttachability ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText(
         'spell-invalid-target-type', { targetType: m.languages.keywords.getAttachability( spellAttachability.name, { plural: true } ).toLowerCase() }
      ) );

    // Invalida alvo caso ele não seja abarcado pelo alcance da magia
    if( !m.GameAction.validateRange( committer.slot, isCardSlot ? target : target.slot, spell.content.stats.current.range ) )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-invalid-range' ) );

    // Para magias vinculáveis
    if( spell.isToAttach && !Object.values( target.attachments ?? {} ).flat().some( attachment => attachment == spell ) ) {
      // Invalida alvo caso ele não possa ser vinculado a magias
      if( !target.maxAttachments?.spell )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-target-must-be-an-attacher' ) );

      // Invalida alvo caso ele tenha atingido seu limite de magias vinculadas
      if( Object.values( target.attachments ).flat().filter( attachment => attachment instanceof Spell ).length >= target.maxAttachments.spell )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-over-max-attachments' ) );
    }

    // Para caso etapa atual seja a da alocação
    if( m.GameMatch.current.flow.step instanceof m.AllocationStep ) {
      // Invalida alvo caso ele não pertença ao jogador da magia
      if( committerController != ( isCardSlot ? target.owner : target.getRelationships().controller ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-player-component' ) );
    }

    // Para caso alvo esteja sendo amparado
    if( target.conditions?.some( condition => condition instanceof m.ConditionGuarded ) ) {
      // Caso magia seja uma negativa, invalida alvo
      if( spell.content.typeset.polarity == 'negative' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-guarded' ) );

      // Caso acionante seja um ente astral adversário de alvo, invalida alvo
      if( committer instanceof m.AstralBeing && committerController != target.getRelationships().owner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-guarded' ) );
    }

    // Caso ação seja canalizar e alvo seja um anão, valida este ante a característica 'antimagicEssence'
    if( action instanceof m.ActionChannel && target instanceof m.Dwarf && !m.Dwarf.validatePotency( action ) ) return false;

    // Validação do alvo da magia segundo seu efeito, se aplicável
    if( spell.content.effects.validateTarget && !spell.content.effects.validateTarget( target, action ) ) return false;

    // Indica que alvo da magia é válido
    return true;
  }
}

/// Propriedades do protótipo
Spell.prototype = Object.create( m.Card.prototype, {
  // Construtor
  constructor: { value: Spell }
} );
