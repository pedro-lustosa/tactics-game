// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellMacabreCommunion = function ( config = {} ) {
  // Iniciação de propriedades
  SpellMacabreCommunion.init( this );

  // Superconstrutor
  m.Voth.call( this, config );

  // Atribuição de propriedades pós-superconstrutor

  /// Atribuição da capacidade de convocar
  Object.oAssignByCopy( this, m.mixinSummonable.call( this ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellMacabreCommunion.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellMacabreCommunion );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-macabre-communion';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 10;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 10;
  }

  // Ordem inicial das variações desta carta na grade
  SpellMacabreCommunion.gridOrder = m.data.cards.spells.push( SpellMacabreCommunion );
}

/// Propriedades do protótipo
SpellMacabreCommunion.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellMacabreCommunion }
} );
