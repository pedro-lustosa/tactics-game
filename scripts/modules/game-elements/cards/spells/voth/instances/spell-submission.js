// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellSubmission = function ( config = {} ) {
  // Iniciação de propriedades
  SpellSubmission.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellSubmission.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellSubmission );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-submission';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellInspiration;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    /// Configura ação 'canalizar' relativa à magia
    spell.configChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( !this.owner || this.owner == action.committer );
      }

      // Não executa função caso acionante não seja um ente astral
      if( !( action.committer instanceof m.AstralBeing ) ) return;

      // Caso ação seja dedicada, modifica seu acionamento para parcial
      if( action.commitment.type == 'dedicated' ) [ action.commitment.subtype, action.commitment.degree ] = [ 'partial', .5 ];

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( this, { changeNumber: 4 } );

      // Altera alcance da magia para G1
      this.content.stats.current.range = 'G1';

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( this, { changeNumber: 4 } );
    }

    /// Desconfigura ação 'canalizar' relativa à magia
    spell.unconfigChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionChannel );

      // Não executa função caso acionamento da ação não seja dedicado
      if( action.commitment.type != 'dedicated' ) return;

      // Restaura acionamento da ação para pleno
      [ action.commitment.subtype, action.commitment.degree ] = [ 'full', 1 ];
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Redefine alcance da magia
      resetRange: {
        // Não executa bloco caso alcance da magia já seja igual a seu valor original
        if( this.content.stats.current.range == this.content.stats.base.range ) break resetRange;

        // Sinaliza início da mudança de alcance
        m.events.cardContentStart.range.emit( this, { changeNumber: -4 } );

        // Altera alcance da magia para seu valor original
        this.content.stats.current.range = this.content.stats.base.range;

        // Sinaliza fim da mudança de alcance
        m.events.cardContentEnd.range.emit( this, { changeNumber: -4 } );
      }

      // Remove do alvo da magia efeito que o torna ocupado
      removeOccupiedStatus: {
        // Identificadores
        let { target } = spell,
            occupiedIndex = target?.readiness.relatedEffects.occupied.indexOf( this ) ?? -1;

        // Caso magia não esteja no arranjo de efeitos que tornam seu alvo ocupado, não executa bloco
        if( occupiedIndex == -1 ) break removeOccupiedStatus;

        // Remove magia do arranjo de efeitos que tornam o alvo ocupado
        target.readiness.relatedEffects.occupied.splice( occupiedIndex, 1 );

        // Atualiza disponibilidade do alvo
        target.readiness.update();
      }
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
      }

      // Identificadores
      var { adjunctSpells } = action;

      // Invalida dono caso ele seja um ente astral e magia seria modificada por 'estender'
      if( owner instanceof m.AstralBeing && adjunctSpells?.some( adjunctSpell => adjunctSpell instanceof m.SpellExtend ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spirit-spell-cannot-have-extend' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var beingMoves = m.GameMatch.current.flow.moves.filter( move => move.source == target );

      // Invalida alvo caso ele não tenha jogadas
      if( !beingMoves.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-moves' ) );

      // Invalida alvo caso ele esteja esgotado
      if( target.readiness.status == 'exhausted' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-exhausted' ) );

      // Invalida alvo caso ele esteja vinculado à magia 'Purificação Corporal', ou caso magia 'Santuário' esteja ativa em sua grade
      if( [ m.SpellSanctuary, m.SpellBodyCleansing ].some( constructor => !constructor.validateConditionApply( target, 'condition-possessed', true ) ) )
        return false;

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores pós-validação
      var { target } = spell;

      // Impede ações em andamento do alvo
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == target ) );

      // Adiciona magia ao arranjo de efeitos que tornam o alvo ocupado
      target.readiness.relatedEffects.occupied.unshift( spell );

      // Atualiza disponibilidade do alvo
      target.readiness.update();

      // Continua operação na função em comum com 'sustainEffect'
      return this.apply();
    }

    /// Provoca efeito relativo à sustentação da magia
    effects.sustainEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'sustain' && eventTarget == spell );

      // Delega operação para função em comum com 'useEffect'
      return this.apply();
    }

    /// Aplica efeito da magia
    effects.apply = function () {
      // Identificadores
      var { target: being } = spell,
          beingWill = being.content.stats.attributes.current.will,
          spellManaCost = spell.content.stats.current.manaCost,
          sustainedMana = spellManaCost.usage - spellManaCost.toChannel.min;

      // Não executa operação caso vontade de alvo não seja menor ou igual que 1 mais o custo de uso sustentado atual da magia
      if( 1 + sustainedMana < beingWill ) return;

      // Torna alvo possuído
      possessBeing: {
        // Identificadores
        let matchFlow = m.GameMatch.current.flow,
            expireStage = matchFlow.period.getStageFrom( matchFlow.parity, 2 ),
            possessedCondition = new m.ConditionPossessed( { controller: spell.getRelationships().controller } );

        // Remove eventual condição 'possuído' já existente em alvo
        being.conditions.find( condition => condition instanceof m.ConditionPossessed )?.drop();

        // Torna alvo possuído
        possessedCondition.apply( being, expireStage );
      }

      // Indica que magia não deve (mais) ser sustentada
      spell.isToStopSustain = true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellSubmission.gridOrder = m.data.cards.spells.push( SpellSubmission );
}

/// Propriedades do protótipo
SpellSubmission.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellSubmission }
} );
