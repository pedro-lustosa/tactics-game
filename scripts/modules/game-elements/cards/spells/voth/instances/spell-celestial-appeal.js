// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellCelestialAppeal = function ( config = {} ) {
  // Iniciação de propriedades
  SpellCelestialAppeal.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo de jogadores que não podem usar esta magia
  SpellCelestialAppeal.invalidPlayers = [];

  // Iniciação de propriedades
  SpellCelestialAppeal.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellCelestialAppeal );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-celestial-appeal';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellAbyssalEvocation;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var { channeling } = owner.content.stats.combativeness;

      // Invalida dono caso seu controlador não possa usar esta magia
      if( SpellCelestialAppeal.invalidPlayers.includes( owner.getRelationships().controller ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'player-cannot-use-celestial-appeal' ) );

      // Invalida dono caso sua polaridade em Voth não seja positiva
      if( channeling.paths[ spell.content.typeset.path ].polarity != 'positive' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'spell-wrong-voth-polarity-committer', { polarity: m.languages.keywords.getPolarity( 'positive', { feminine: true } ).toLowerCase() }
        ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida convocante
    effects.validateSummoner = function ( summoner ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( summoner instanceof m.Being );
        m.oAssert( summoner.deck == spell.deck );
        m.oAssert( summoner.activity == 'active' );
        m.oAssert( summoner.content.stats.combativeness.channeling );
      }

      // Identificadores
      var summonerPaths = summoner.content.stats.combativeness.channeling.paths;

      // Invalida convocante caso não se esteja no período da remobilização
      if( !( m.GameMatch.current.flow.period instanceof m.RedeploymentPeriod ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'flow-must-be-redeployment' ) );

      // Invalida convocante caso não tenha polaridade positiva em Voth
      if( summonerPaths.voth.polarity != 'positive' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'spell-wrong-voth-polarity-summoner', { polarity: m.languages.keywords.getPolarity( 'positive', { feminine: true } ).toLowerCase() }
        ) );

      // Invalida convocante caso tenha um nível em Voth menor que 3
      if( summonerPaths.voth.skill < 3 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'voth-greater-min-level-summoner', { level: 3 } ) );

      // Invalida convocante caso já tenha 1 anjo ativo
      if( summoner.summons.angel?.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'angels-above-summoner-limit' ) );

      // Indica que convocante é válido
      return true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Desusa cartas desta magia e finda anjos em função do uso de uma magia negativa
  SpellCelestialAppeal.endCardsByNegativeSpell = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: negativeSpell } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && negativeSpell instanceof m.Spell );
      m.oAssert( negativeSpell.content.typeset.polarity == 'negative' );
    }

    // Identificadores pós-validação
    var { controller: spellController, opponent: spellOpponent } = negativeSpell.getRelationships(),
        controllerDeck = m.GameMatch.current.decks[ spellController.parity ],
        opponentDeck = m.GameMatch.current.decks[ spellOpponent.parity ];

    // Caso ainda não esteja lá, adiciona controlador da magia negativa ao arranjo de jogadores que não podem acionar esta magia
    if( !this.invalidPlayers.includes( spellController ) ) this.invalidPlayers.push( spellController );

    // Itera por cartas no baralho do controlador da magia negativa
    for( let card of controllerDeck.cards ) {
      // Identificadores
      let angels = card.summons?.angel?.slice() ?? [];

      // Caso carta seja 'Apelo Celestial' e ela esteja em uso pelo controlador da magia negativa, desusa-a
      if( card instanceof this && card.triggeringPlayer == spellController ) card.disuse();

      // Finda anjos do controlador da magia negativa
      angels.forEach( angel => angel.inactivate( 'ended', { triggeringCard: negativeSpell } ) );
    }

    // Itera por cartas no baralho do oponente do controlador da magia negativa
    for( let card of opponentDeck.cards ) {
      // Caso carta seja 'Apelo Celestial' e ela esteja em uso pelo controlador da magia negativa, desusa-a
      if( card instanceof this && card.triggeringPlayer == spellController ) card.disuse();
    }
  }

  // Ordem inicial das variações desta carta na grade
  SpellCelestialAppeal.gridOrder = m.data.cards.spells.push( SpellCelestialAppeal );
}

/// Propriedades do protótipo
SpellCelestialAppeal.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellCelestialAppeal }
} );
