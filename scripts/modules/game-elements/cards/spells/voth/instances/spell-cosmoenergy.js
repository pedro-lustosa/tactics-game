// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellCosmoenergy = function ( config = {} ) {
  // Iniciação de propriedades
  SpellCosmoenergy.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellCosmoenergy.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellCosmoenergy );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-cosmoenergy';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellBodyTakeover;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.AstralBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 1;
  }

  // Ordem inicial das variações desta carta na grade
  SpellCosmoenergy.gridOrder = m.data.cards.spells.push( SpellCosmoenergy );
}

/// Propriedades do protótipo
SpellCosmoenergy.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellCosmoenergy }
} );
