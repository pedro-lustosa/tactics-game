// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAbyssalEvocation = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAbyssalEvocation.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAbyssalEvocation.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAbyssalEvocation );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-abyssal-evocation';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellCelestialAppeal;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 8;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 8;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var { channeling } = owner.content.stats.combativeness;

      // Invalida dono caso sua polaridade em Voth não seja negativa
      if( channeling.paths[ spell.content.typeset.path ].polarity != 'negative' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'spell-wrong-voth-polarity-committer', { polarity: m.languages.keywords.getPolarity( 'negative', { feminine: true } ).toLowerCase() }
        ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida convocante
    effects.validateSummoner = function ( summoner ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( summoner instanceof m.Being );
        m.oAssert( summoner.deck == spell.deck );
        m.oAssert( summoner.activity == 'active' );
        m.oAssert( summoner.content.stats.combativeness.channeling );
      }

      // Identificadores
      var summonerPaths = summoner.content.stats.combativeness.channeling.paths;

      // Invalida convocante caso não se esteja no período da remobilização
      if( !( m.GameMatch.current.flow.period instanceof m.RedeploymentPeriod ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'flow-must-be-redeployment' ) );

      // Invalida convocante caso não tenha polaridade negativa em Voth
      if( summonerPaths.voth.polarity != 'negative' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'spell-wrong-voth-polarity-summoner', { polarity: m.languages.keywords.getPolarity( 'negative', { feminine: true } ).toLowerCase() }
        ) );

      // Invalida convocante caso tenha um nível em Voth menor que 2
      if( summonerPaths.voth.skill < 2 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'voth-greater-min-level-summoner', { level: 2 } ) );

      // Invalida convocante caso já tenha 1 demônio ativo por 2 níveis seus em Voth
      if( summonerPaths.voth.skill - 2 < ( summoner.summons.demon?.length ?? 0 ) * 2 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'demons-above-summoner-limit' ) );

      // Indica que convocante é válido
      return true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAbyssalEvocation.gridOrder = m.data.cards.spells.push( SpellAbyssalEvocation );
}

/// Propriedades do protótipo
SpellAbyssalEvocation.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellAbyssalEvocation }
} );
