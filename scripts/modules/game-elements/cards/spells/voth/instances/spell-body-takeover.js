// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellBodyTakeover = function ( config = {} ) {
  // Iniciação de propriedades
  SpellBodyTakeover.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellBodyTakeover.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellBodyTakeover );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-body-takeover';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellCosmoenergy;

    /// Indica se magia tem custo de persistência dinâmico
    spell.isDynamicPersistence = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Disponibilidade
    typeset.availability = 2;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellBodyTakeover.gridOrder = m.data.cards.spells.push( SpellBodyTakeover );
}

/// Propriedades do protótipo
SpellBodyTakeover.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellBodyTakeover }
} );
