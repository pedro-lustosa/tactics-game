// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAstralProjection = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAstralProjection.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAstralProjection.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAstralProjection );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-astral-projection';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Invalida dono caso ele seja um ente astral
      if( owner instanceof m.AstralBeing )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-not-be-astral-being' ) );

      // Invalida dono caso ele esteja em uma zona de engajamento
      if( owner.slot instanceof m.EngagementZone )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-not-be-in-engagement-zone' ) );

      // Indica que dono é válido
      return true;
    }

    /// Define o alvo da magia
    effects.setChannelingTarget = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Define o alvo da magia como seu dono
      spell.target = action.committer;

      // Prossegue com execução da ação
      return action.currentExecution.next();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores
      var { target } = spell,
          projectedCondition = new m.ConditionProjected();

      // Torna o alvo da magia projetado
      projectedCondition.apply( target );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAstralProjection.gridOrder = m.data.cards.spells.push( SpellAstralProjection );
}

/// Propriedades do protótipo
SpellAstralProjection.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellAstralProjection }
} );
