// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTelepathy = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTelepathy.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTelepathy.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTelepathy );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-telepathy';

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Being;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'sustained';

    /// Subtipo de duração da magia
    typeset.durationSubtype = 'constant';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R4';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTelepathy.gridOrder = m.data.cards.spells.push( SpellTelepathy );
}

/// Propriedades do protótipo
SpellTelepathy.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellTelepathy }
} );
