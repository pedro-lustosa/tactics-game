// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAwe = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAwe.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAwe.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAwe );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-awe';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellFear;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    /// Adiciona magias adicionas para disputas mágicas
    spell.addClashingSpells = function () {
      // Identificadores
      var { target } = this,
          clashingSpells = [];

      // Adiciona ao arranjo de magias disputantes magias 'Temor' que estejam vinculadas ao mesmo alvo desta magia
      clashingSpells = clashingSpells.concat( target.getAllCardAttachments().filter( card => card instanceof m.SpellFear ) );

      // Retorna magias disputantes
      return clashingSpells;
    }

    /// Configura ação 'canalizar' relativa à magia
    spell.configChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( !this.owner || this.owner == action.committer );
      }

      // Não executa função caso acionante não seja um ente astral
      if( !( action.committer instanceof m.AstralBeing ) ) return;

      // Caso ação seja dedicada, modifica seu acionamento para parcial
      if( action.commitment.type == 'dedicated' ) [ action.commitment.subtype, action.commitment.degree ] = [ 'partial', .5 ];

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( this, { changeNumber: 4 } );

      // Altera alcance da magia para G1
      this.content.stats.current.range = 'G1';

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( this, { changeNumber: 4 } );
    }

    /// Desconfigura ação 'canalizar' relativa à magia
    spell.unconfigChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionChannel );

      // Não executa função caso acionamento da ação não seja dedicado
      if( action.commitment.type != 'dedicated' ) return;

      // Restaura acionamento da ação para pleno
      [ action.commitment.subtype, action.commitment.degree ] = [ 'full', 1 ];
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Zera quantidade de marcadores de 'admirável' adicionada via esta magia
      this.content.effects.addedMarkers = 0;

      // Encerra função caso alcance da magia já seja igual a seu valor original
      if( this.content.stats.current.range == this.content.stats.base.range ) return;

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( this, { changeNumber: -4 } );

      // Altera alcance da magia para seu valor original
      this.content.stats.current.range = this.content.stats.base.range;

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( this, { changeNumber: -4 } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 5;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Marcadores de admirável adicionados via esta magia
    effects.addedMarkers = 0;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var { adjunctSpells } = action;

      // Invalida dono caso ele seja um ente astral e magia seria modificada por 'estender'
      if( owner instanceof m.AstralBeing && adjunctSpells?.some( adjunctSpell => adjunctSpell instanceof m.SpellExtend ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spirit-spell-cannot-have-extend' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var awedCondition = target.conditions.find( condition => condition instanceof m.ConditionAwed );

      // Invalida alvo caso ele esteja com quantidade máxima de marcadores de 'admirável'
      if( awedCondition && awedCondition.markers >= awedCondition.maxMarkers )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-awed-markers' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Caso se esteja na fase da batalha, aplica ao alvo marcadores iniciais de 'admirável'
      if( m.GameMatch.current.flow.phase instanceof m.BattlePhase ) this.applyConditionMarkers();

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Adiciona evento para atualizar quantidade de marcadores em alvo segundo mudança da potência desta magia
      m.events.cardContentEnd.potency.add( spell, this.updateConditionMarkers, { context: this } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Caso se esteja na fase da batalha, remove de alvo marcadores de 'admirável' adicionados via esta magia
      if( m.GameMatch.current.flow.phase instanceof m.BattlePhase ) this.removeConditionMarkers();

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Remove evento para atualizar quantidade de marcadores em alvo segundo mudança da potência desta magia
      m.events.cardContentEnd.potency.remove( spell, this.updateConditionMarkers, { context: this } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Delega operação para função também chamada com a ativação do efeito da magia
      return this.applyConditionMarkers();
    }

    /// Aplica a alvo da magia marcadores de 'admirável'
    effects.applyConditionMarkers = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var { target: being } = spell,
          spellPotency = spell.content.stats.current.potency,
          awedCondition = being.conditions.find( condition => condition instanceof m.ConditionAwed ) ?? new m.ConditionAwed();

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( spellPotency );

      // Adiciona evento para atualizar quantidade de marcadores em alvo via esta magia
      m.events.conditionChangeEnd.increase.add( awedCondition, this.setAddedMarkers, { context: this } );

      // Aplica a alvo condição 'admirável', enquanto com marcadores iguais ao dobro da potência desta magia
      awedCondition.apply( being, null, spellPotency * 2 );

      // Remove evento para atualizar quantidade de marcadores em alvo via esta magia
      m.events.conditionChangeEnd.increase.remove( awedCondition, this.setAddedMarkers, { context: this } );
    }

    /// Aplica a alvo da magia marcadores de 'admirável'
    effects.updateConditionMarkers = function ( eventData = {} ) {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget, changeNumber: changedPotency } = eventData,
          { target: being } = spell,
          awedCondition = being.conditions.find( condition => condition instanceof m.ConditionAwed );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-content' && eventType == 'potency' && eventTarget == spell );
        m.oAssert( changedPotency && Number.isInteger( changedPotency ) );
        m.oAssert( awedCondition );
      }

      // Identificadores pós-validação
      var method = changedPotency > 0 ? 'increase' : 'decrease',
          changedMarkers = changedPotency * 2;

      // Caso marcadores devam ser reduzidos, ajusta redução para que ela não deixe os marcadores desta magia em alvo para um número abaixo de zero
      if( method == 'decrease' )
        while( this.addedMarkers + changedMarkers < 0 ) changedMarkers++

      // Do contrário, ajusta aumento para que ele não deixe os marcadores desta magia em alvo para um número acima do limite de marcadores
      else
        while( this.addedMarkers + changedMarkers > awedCondition.maxMarkers ) changedMarkers--;

      // Atualiza marcadores de condição 'admirável' de alvo oriundos desta magia
      awedCondition[ method ]( Math.abs( changedMarkers ) );

      // Atualiza registro dos marcadores da condição em alvo oriundos desta magia
      this.addedMarkers += changedMarkers;
    }

    /// Remove do alvo da magia marcadores de 'admirável'
    effects.removeConditionMarkers = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Não executa função caso não haja mais marcadores da condição adicionados via a magia
      if( !this.addedMarkers ) return;

      // Identificadores
      var { target: being } = spell,
          awedCondition = being.conditions.find( condition => condition instanceof m.ConditionAwed );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( awedCondition );

      // Remove de condição 'admirável' de alvo marcadores adicionados via esta magia
      awedCondition.decrease( this.addedMarkers );

      // Zera marcadores adicionados via esta magia
      this.addedMarkers = 0;
    }

    /// Define marcadores de 'admirável' adicionados via esta magia
    effects.setAddedMarkers = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget, markers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'increase' && eventTarget instanceof m.ConditionAwed );
        m.oAssert( Number.isInteger( markers ) && markers > 0 );
        m.oAssert( spell.target.conditions.includes( eventTarget ) );
      }

      // Define marcadores da condição em alvo via esta magia
      this.addedMarkers = markers;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAwe.gridOrder = m.data.cards.spells.push( SpellAwe );
}

/// Propriedades do protótipo
SpellAwe.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellAwe }
} );
