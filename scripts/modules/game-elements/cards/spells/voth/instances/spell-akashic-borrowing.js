// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAkashicBorrowing = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAkashicBorrowing.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAkashicBorrowing.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAkashicBorrowing );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-akashic-borrowing';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAkashicBorrowing.gridOrder = m.data.cards.spells.push( SpellAkashicBorrowing );
}

/// Propriedades do protótipo
SpellAkashicBorrowing.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellAkashicBorrowing }
} );
