// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellPyroticAnimation = function ( config = {} ) {
  // Iniciação de propriedades
  SpellPyroticAnimation.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellPyroticAnimation.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellPyroticAnimation );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-pyrotic-animation';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 10;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 10;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var { channeling } = owner.content.stats.combativeness;

      // Invalida dono caso ele não tenha níveis em Powth
      if( !channeling.paths.powth.skill )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'must-be-powthe-committer' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida convocante
    effects.validateSummoner = function ( summoner, salamanderName ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( summoner instanceof m.Being );
        m.oAssert( summoner.deck == spell.deck );
        m.oAssert( summoner.activity == 'active' );
        m.oAssert( summoner.content.stats.combativeness.channeling );
        m.oAssert( [ 'golden', 'indigo' ].includes( salamanderName ) );
      }

      // Identificadores
      var summonerPaths = summoner.content.stats.combativeness.channeling.paths;

      // Invalida convocante caso não se esteja no período da remobilização
      if( !( m.GameMatch.current.flow.period instanceof m.RedeploymentPeriod ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'flow-must-be-redeployment' ) );

      // Invalida convocante caso não tenha algum nível em Powth
      if( !summonerPaths.powth.skill )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'must-be-powthe-summoner' ) );

      // Invalida convocante caso tenha um nível em Voth menor que 2
      if( summonerPaths.voth.skill < 2 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'voth-greater-min-level-summoner', { level: 2 } ) );

      // Invalida convocante caso não tenha polaridade negativa em Voth e salamandra alvo seja a anilada
      if( salamanderName == 'indigo' && summonerPaths.voth.polarity != 'negative' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText(
          'spell-wrong-voth-polarity-summoner', { polarity: m.languages.keywords.getPolarity( 'negative', { feminine: true } ).toLowerCase() }
        ) );

      // Invalida convocante caso já tenha 1 salamandra ativa por 2 níveis seus em Voth
      if( summonerPaths.voth.skill - 2 < ( summoner.summons.salamander?.length ?? 0 ) * 2 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'salamanders-above-summoner-limit' ) );

      // Indica que convocante é válido
      return true;
    }
  }

  // Ordem inicial das variações desta carta na grade
  SpellPyroticAnimation.gridOrder = m.data.cards.spells.push( SpellPyroticAnimation );
}

/// Propriedades do protótipo
SpellPyroticAnimation.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellPyroticAnimation }
} );
