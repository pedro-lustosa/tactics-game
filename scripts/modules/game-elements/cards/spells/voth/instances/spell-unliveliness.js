// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellUnliveliness = function ( config = {} ) {
  // Iniciação de propriedades
  SpellUnliveliness.init( this );

  // Superconstrutor
  m.Voth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellUnliveliness.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Voth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellUnliveliness );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-unliveliness';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellLiveliness;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    /// Configura ação 'canalizar' relativa à magia
    spell.configChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( !this.owner || this.owner == action.committer );
      }

      // Não executa função caso acionante não seja um ente astral
      if( !( action.committer instanceof m.AstralBeing ) ) return;

      // Caso ação seja dedicada, modifica seu acionamento para parcial
      if( action.commitment.type == 'dedicated' ) [ action.commitment.subtype, action.commitment.degree ] = [ 'partial', .5 ];

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( this, { changeNumber: 3 } );

      // Altera alcance da magia para G1
      this.content.stats.current.range = 'G1';

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( this, { changeNumber: 3 } );
    }

    /// Desconfigura ação 'canalizar' relativa à magia
    spell.unconfigChannelAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionChannel );

      // Não executa função caso acionamento da ação não seja dedicado
      if( action.commitment.type != 'dedicated' ) return;

      // Restaura acionamento da ação para pleno
      [ action.commitment.subtype, action.commitment.degree ] = [ 'full', 1 ];
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Não executa função caso alcance da magia já seja igual a seu valor original
      if( this.content.stats.current.range == this.content.stats.base.range ) return;

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( this, { changeNumber: -3 } );

      // Altera alcance da magia para seu valor original
      this.content.stats.current.range = this.content.stats.base.range;

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( this, { changeNumber: -3 } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
      }

      // Identificadores
      var { adjunctSpells } = action;

      // Invalida dono caso ele seja um ente astral e magia seria modificada por 'estender'
      if( owner instanceof m.AstralBeing && adjunctSpells?.some( adjunctSpell => adjunctSpell instanceof m.SpellExtend ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spirit-spell-cannot-have-extend' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var unlivenedCondition = target.conditions.find( condition => condition instanceof m.ConditionUnlivened ),
          enlivenedCondition = target.conditions.find( condition => condition instanceof m.ConditionEnlivened );

      // Invalida alvo caso ele esteja com quantidade máxima de marcadores de desalentado
      if( unlivenedCondition && unlivenedCondition.markers >= unlivenedCondition.maxMarkers )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-unlivened-markers' ) );

      // Valida alvo caso ele esteja com a condição 'alentado'
      if( enlivenedCondition ) return true;

      // Invalida alvo caso ele esteja vinculado à magia 'Purificação Corporal', ou caso magia 'Santuário' esteja ativa em sua grade
      if( [ m.SpellSanctuary, m.SpellBodyCleansing ].some( constructor => !constructor.validateConditionApply( target, 'condition-unlivened', true ) ) )
        return false;

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Delega operação para função em comum com 'sustainEffect'
      return this.apply();
    }

    /// Provoca efeito relativo à sustentação da magia
    effects.sustainEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'sustain' && eventTarget == spell );

      // Delega operação para função em comum com 'useEffect'
      return this.apply();
    }

    /// Aplica efeito da magia
    effects.apply = function () {
      // Identificadores
      var { target } = spell,
          enlivenedCondition = target.conditions.find( condition => condition instanceof m.ConditionEnlivened );

      // Caso alvo tenha condição 'alentado', decrementa em 1 seus marcadores e encerra função
      if( enlivenedCondition ) return enlivenedCondition.decrease( 1, { triggeringCard: spell } );

      // Captura ou gera condição 'desalentado'
      let unlivenedCondition = target.conditions.find( condition => condition instanceof m.ConditionUnlivened ) ?? new m.ConditionUnlivened();

      // Aplica a alvo condição 'desalentado', ou incrementa em 1 seu marcador
      unlivenedCondition.apply( target, null, 1, { triggeringCard: spell } );

      // Caso marcadores de desalentado tenham atingido seu limite, indica que magia não deve (mais) ser sustentada
      if( unlivenedCondition.markers >= unlivenedCondition.maxMarkers ) spell.isToStopSustain = true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellUnliveliness.gridOrder = m.data.cards.spells.push( SpellUnliveliness );
}

/// Propriedades do protótipo
SpellUnliveliness.prototype = Object.create( m.Voth.prototype, {
  // Construtor
  constructor: { value: SpellUnliveliness }
} );
