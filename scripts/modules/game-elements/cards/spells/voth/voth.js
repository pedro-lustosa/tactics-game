// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Voth = function ( config = {} ) {
  // Superconstrutor
  m.Spell.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Voth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Spell.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Voth );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Senda
    typeset.path = 'voth';
  }
}

/// Propriedades do protótipo
Voth.prototype = Object.create( m.Spell.prototype, {
  // Construtor
  constructor: { value: Voth }
} );
