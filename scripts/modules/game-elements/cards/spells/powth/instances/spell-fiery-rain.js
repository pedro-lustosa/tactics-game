// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellFieryRain = function ( config = {} ) {
  // Iniciação de propriedades
  SpellFieryRain.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellFieryRain.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellFieryRain );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-fiery-rain';

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'sustained';

    /// Subtipo de duração da magia
    typeset.durationSubtype = 'constant';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 10;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 10;
  }

  // Ordem inicial das variações desta carta na grade
  SpellFieryRain.gridOrder = m.data.cards.spells.push( SpellFieryRain );
}

/// Propriedades do protótipo
SpellFieryRain.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellFieryRain }
} );
