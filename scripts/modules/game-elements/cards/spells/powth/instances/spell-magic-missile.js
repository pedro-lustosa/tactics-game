// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellMagicMissile = function ( config = {} ) {
  // Iniciação de propriedades
  SpellMagicMissile.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellMagicMissile.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellMagicMissile );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-magic-missile';

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = true;

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Identificadores
      var spellStats = this.content.stats;

      // Não executa função caso custos de mana atuais já sejam iguais aos originais
      if( [ 'min', 'max' ].every( key => spellStats.current.manaCost.toChannel[ key ] == spellStats.base.manaCost.toChannel[ key ] ) ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( this, { type: 'toChannel' } );

      // Redefine para valores originais manas mínimo e máximo para canalização
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] = spellStats.base.manaCost.toChannel[ key ];

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( this, { type: 'toChannel' } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Being;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R5';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Define o alvo da magia
    effects.setChannelingTarget = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
        m.oAssert( !this.currentExecution );
      }

      // Captura e inicia execução da seleção dos alvos da magia
      ( this.currentExecution = executeTargetSelection() ).next();

      // Função para início da execução da seleção dos alvos da magia
      function * executeTargetSelection() {
        // Identificadores
        var { manaCost } = spell.content.stats.current,
            maxAllowedMana = action.getMaxAssignableMana(),
            targetsToChoose = 1;

        // Para caso acionante possa gastar mais de 1 ponto de mana com esta canalização
        if( maxAllowedMana > 1 ) {
          // Inseri modal para seleção da quantidade de ataques da magia
          yield new m.PromptModal( {
            name: 'number-of-attacks-modal',
            text: m.languages.notices.getModalText( 'set-number-of-attacks' ),
            inputConfig: configAttackNumberModal,
            action: assignTargetsToChoose
          } ).insert();
        }

        // Sinaliza início da mudança de custos de mana
        m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel' } );

        // Ajusta custos mínimo e máximo da magia para que sejam iguais à quantidade de ataques
        manaCost.toChannel.min = manaCost.toChannel.max = targetsToChoose;

        // Sinaliza fim da mudança de custos de mana
        m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel' } );

        // Define alvo da magia, para caso haja apenas 1
        if( targetsToChoose == 1 ) spell.target = yield action.selectTarget( 'choose-target-being' )

        // Para caso haja mais de 1 alvo a ser escolhido
        else {
          // Define alvos da magia, adicionando-os ao arranjo de alvos adicionais
          for( let i = 1; i <= targetsToChoose; i++ ) spell.extraTargets.push( yield action.selectTarget( `choose-attack-target-${ i }` ) );

          // Define alvo principal da magia como o último ente no arranjo de alvos adicionais
          spell.target = spell.extraTargets.pop();
        }

        // Nulifica execução atual do efeito da magia
        effects.currentExecution = null;

        // Prossegue com execução da ação
        yield setTimeout( () => action.currentExecution.next() );

        // Funções

        /// Para configuração da modal de escolha da quantidade de ataques
        function configAttackNumberModal( targetModal, targetInput ) {
          // Identificadores
          var { htmlInput } = targetInput;

          // Validação
          if( m.app.isInDevelopment ) {
            m.oAssert( targetModal instanceof m.PromptModal );
            m.oAssert( targetInput instanceof PIXI.TextInput );
            m.oAssert( targetInput.name == 'number-of-attacks-modal-input' );
          }

          // Configura propriedades do campo de inserção

          /// Texto de aviso
          targetInput.placeholder = m.languages.notices.getModalText( 'number-of-attacks-placeholder', { maxNumber: maxAllowedMana } );

          /// Quantidade máxima de caracteres
          targetInput.maxLength = maxAllowedMana.toString().length;

          // Eventos

          /// Para limitar caracteres usáveis para dígitos
          targetInput.addListener( 'input', () => htmlInput.value = htmlInput.value.replace( /\D/g, '' ) );

          /// Para definir validade do campo de inserção
          targetInput.addListener( 'input', () => targetInput.isValid = htmlInput.value >= 1 && htmlInput.value <= maxAllowedMana );
        }

        /// Define quantidade de alvos da magia
        function assignTargetsToChoose() {
          // Identificadores
          var promptModal = m.app.pixi.stage.children.find( child => child.name == 'number-of-attacks-modal' ),
              attackNumberInput = promptModal?.content.inputsSet.textInput,
              { htmlInput } = attackNumberInput ?? {};

          // Validação
          if( m.app.isInDevelopment ) m.oAssert( 'value' in htmlInput );

          // Encerra função caso campo de inserção esteja inválido
          if( !attackNumberInput.isValid ) return false;

          // Atribuição da quantidade de alvos da magia
          targetsToChoose = Number( htmlInput.value );

          // Prossegue com execução da seleção dos alvos
          return effects.currentExecution.next();
        }
      }
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Invalida alvo caso ele não possa ser mirado devido à validação de 'Atrair'
      if( !m.SpellAttract.validateMagicShot( target, action, true ) ) return false;

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var allTargets = [ ...spell.extraTargets, spell.target ],
            manaDamage = new m.DamageMana( { singlePoints: 3, source: spell } ),
            targetsToCheckActionPrevention = [];

        // Adiciona evento para impedir exibição discreta de paradas de destino sobre impedimento de ações devido ao dano sofrido
        m.events.breakStart.fate.add( m.GameMatch.current, validateFateBreakStart );

        // Inflige dano de mana a alvos da magia
        for( let target of allTargets ) manaDamage.setTotalPoints( target, 1 ).inflictPoints( target );

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Remove evento para impedir exibição discreta de paradas de destino sobre impedimento de ações devido ao dano sofrido
        m.events.breakStart.fate.remove( m.GameMatch.current, validateFateBreakStart );

        // Prevenção de ações de entes
        preventActions: {
          // Apenas executa bloco caso haja entes cujas ações podem ser impedidas
          if( !targetsToCheckActionPrevention.length ) break preventActions;

          // Identificadores
          let damager = spell.owner,
              damagerPlayer = damager.getRelationships().controller;

          // Para enquanto houverem entes a verificar paradas de destino para prevenção de ações
          while( targetsToCheckActionPrevention.length ) {
            // Identificadores
            let targetBeing = targetsToCheckActionPrevention[ 0 ],
                { controller: beingPlayer, opponent: beingOpponent } = targetBeing.getRelationships(),
                player = damagerPlayer == beingPlayer ? beingOpponent : beingPlayer,
                requiredPoints = targetsToCheckActionPrevention.filter( being => being == targetBeing ).length,
                beingActions = m.GameAction.currents.filter( action => action.committer == targetBeing );

            // Retira ente alvo do arranjo de entes sobre impedimento de ações
            targetsToCheckActionPrevention = targetsToCheckActionPrevention.filter( being => being != targetBeing );

            // Filtra entes no momento sem ações
            if( !beingActions.length ) continue;

            // Programa parada de destino para decidir se ações do ente serão impedidas
            m.GameMatch.programFateBreak( player, {
              name: `prevent-actions-fate-break-${ targetBeing.getMatchId() }`,
              playerBeing: targetBeing,
              opponentBeing: damager,
              isContestable: true,
              requiredPoints: requiredPoints,
              successAction: () => effects.currentExecution.next( true ),
              failureAction: () => effects.currentExecution.next( false ),
              modalArguments: [ 'fate-break-prevent-actions', { committer: targetBeing } ]
            } );

            // Aguarda resolução da parada de destino
            let wasUsedFatePoint = yield;

            // Caso parada de destino não tenha sido bem-sucedida, impede ações do alvo da magia
            if( !wasUsedFatePoint ) m.GameAction.preventAllActions( beingActions );
          }
        }

        // Funções

        /// Valida início de eventual parada de destino sobre impedimento de ações
        function validateFateBreakStart() {
          // Identificadores
          var fateBreak = m.GameMatch.fateBreakExecution;

          // Apenas executa função caso parada de destino a ser iniciada seja a sobre impedimento de ações
          if( !fateBreak.name.startsWith( 'prevent-actions-fate-break-' ) ) return;

          // Adiciona ente que provocou parada de destino ao arranjo de entes a verificar impedimento de ações
          targetsToCheckActionPrevention.push( fateBreak.playerBeing );

          // Simula parada de destino enquanto tendo sido bem-sucedida
          [ fateBreak.result.player, fateBreak.result.opponent ] = [ true, false ];

          // Indica que parada de destino deve ser desconsiderada
          fateBreak.isToSkip = true;
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellMagicMissile.gridOrder = m.data.cards.spells.push( SpellMagicMissile );
}

/// Propriedades do protótipo
SpellMagicMissile.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellMagicMissile }
} );
