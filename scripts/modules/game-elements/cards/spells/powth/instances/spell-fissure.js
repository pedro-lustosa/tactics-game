// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellFissure = function ( config = {} ) {
  // Iniciação de propriedades
  SpellFissure.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellFissure.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellFissure );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-fissure';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 4;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { target: being } = spell,
            beingAttributes = being.content.stats.attributes;

        // Caso agilidade do alvo seja menor que 2, afasta-o e encerra execução do efeito
        if( beingAttributes.current.agility < 2 ) return being.inactivate( 'withdrawn', { triggeringCard: spell } );

        // Caso alvo esteja indefeso, afasta-o e encerra execução do efeito
        if( being.conditions.some( condition => condition instanceof m.ConditionIncapacitated ) )
          return being.inactivate( 'withdrawn', { triggeringCard: spell } );

        // Caso alvo esteja vinculado a 'Aperto da Terra', afasta-o e encerra execução do efeito
        if( being.getAllCardAttachments().some( attachment => attachment instanceof m.SpellEarthGrip && attachment.content.effects.isEnabled ) )
          return being.inactivate( 'withdrawn', { triggeringCard: spell } );

        // Gasta 2 pontos da agilidade do alvo
        beingAttributes.reduce( 2, 'agility', { triggeringCard: spell } );

        // Caso alvo não esteja esgotado, encerra execução do efeito
        if( being.readiness.status != 'exhausted' ) return;

        // Lança parada de destino sobre afastamento do alvo
        m.GameMatch.programFateBreak( spell.getRelationships().controller, {
          name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
          playerBeing: spell.owner,
          opponentBeing: being,
          isContestable: true,
          successAction: () => effects.currentExecution.next( true ),
          failureAction: () => effects.currentExecution.next( false ),
          modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
        } );

        // Aguarda resolução da parada de destino
        let wasUsedFatePoint = yield;

        // Caso parada de destino tenha sido bem-sucedida, afasta alvo
        if( wasUsedFatePoint ) being.inactivate( 'withdrawn', { triggeringCard: spell } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellFissure.gridOrder = m.data.cards.spells.push( SpellFissure );
}

/// Propriedades do protótipo
SpellFissure.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellFissure }
} );
