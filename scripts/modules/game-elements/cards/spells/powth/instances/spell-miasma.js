// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellMiasma = function ( config = {} ) {
  // Iniciação de propriedades
  SpellMiasma.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellMiasma.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellMiasma );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-miasma';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.CardSlot;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R4';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Intervalo de segmentos até adição do próximo marcador de enfermo
    effects.diseasedInterval = 3;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var combatPeriod = m.GameMatch.current.flow.round.getChild( 'combat-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Programa adição de marcadores de enfermo para todos os entes em alvo
      this.scheduleDiseasedGainForAll();

      // Adiciona ao período do combate evento para em seu início programar adição de marcadores de enfermo a entes em alvo
      m.events.flowChangeEnd.begin.add( combatPeriod, this.scheduleDiseasedGainForAll, { context: this } );

      // Adiciona a alvo evento para programar adição de marcadores de enfermo para entes que o ocuparem
      m.events.fieldSlotCardChangeEnd.enter.add( spell.target, this.scheduleDiseasedGain, { context: this } );

      // Adiciona a alvo evento para remover programação da adição de marcadores de enfermo para entes que o deixarem
      m.events.fieldSlotCardChangeEnd.leave.add( spell.target, this.unscheduleDiseasedGain, { context: this } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var combatPeriod = m.GameMatch.current.flow.round.getChild( 'combat-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Desprograma adição de marcadores de enfermo para todos os entes em alvo
      this.unscheduleDiseasedGainForAll();

      // Remove do período do combate evento para em seu início programar adição de marcadores de enfermo a entes em alvo
      m.events.flowChangeEnd.begin.remove( combatPeriod, this.scheduleDiseasedGainForAll, { context: this } );

      // Remove de alvo evento para programar adição de marcadores de enfermo para entes que o ocuparem
      m.events.fieldSlotCardChangeEnd.enter.remove( spell.target, this.scheduleDiseasedGain, { context: this } );

      // Remove de alvo evento para remover programação da adição de marcadores de enfermo para entes que o deixarem
      m.events.fieldSlotCardChangeEnd.leave.remove( spell.target, this.unscheduleDiseasedGain, { context: this } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Programa adição do próximo marcador de enfermo para todos os entes no alvo da magia
    effects.scheduleDiseasedGainForAll = function () {
      // Identificadores
      var cardSlot = spell.target,
          beingsInTarget = cardSlot.cards ?? [ cardSlot.card ].filter( card => card );

      // Programa adição de marcadores de enfermo a entes em alvo
      for( let being of beingsInTarget ) this.scheduleDiseasedGain( { card: being } );
    }

    /// Desprograma adição do próximo marcador de enfermo para todos os entes no alvo da magia
    effects.unscheduleDiseasedGainForAll = function () {
      // Identificadores
      var cardSlot = spell.target,
          beingsInTarget = cardSlot.cards ?? [ cardSlot.card ].filter( card => card );

      // Remove programação da adição de marcadores de enfermo a entes em alvo
      for( let being of beingsInTarget ) this.unscheduleDiseasedGain( { card: being } );
    }

    /// Programa para o ente alvo adição do próximo marcador de enfermo
    effects.scheduleDiseasedGain = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { card: being } = eventData,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

      // Não executa função caso período atual não seja o do combate
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return;

      // Não executa função caso ente alvo não seja um biótico
      if( !( being instanceof m.BioticBeing ) ) return;

      // Identificadores pós-validação
      var nextSegment = matchFlow.period.getStageFrom( matchFlow.segment, this.diseasedInterval - 1 );

      // Caso haja um próximo segmento válido, adiciona evento para adicionar marcador de enfermo lá
      if( nextSegment ) m.events.flowChangeStart.finish.add( nextSegment, this.addDiseasedMarker, { context: being, once: true } );
    }

    /// Remove do ente alvo programação para adição do próximo marcador de enfermo
    effects.unscheduleDiseasedGain = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { card: being } = eventData,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

      // Não executa função caso período atual não seja o do combate
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return;

      // Itera por segmentos de intervalo até adição do próximo marcador de enfermo
      for( let i = 0; i < this.diseasedInterval; i++ ) {
        // Identificadores
        let currentSegment = matchFlow.period.getStageFrom( matchFlow.segment ?? spell.usageSegment, i );

        // Caso segmento alvo não exista, encerra iteração
        if( !currentSegment ) break;

        // Quando existente, remove de segmento alvo evento para adição do próximo marcador de enfermo
        m.events.flowChangeStart.finish.remove( currentSegment, this.addDiseasedMarker, { context: being } );
      }
    }

    /// Adiciona ao ente alvo marcador de enfermo
    effects.addDiseasedMarker = function () {
      // Não executa função caso efeito esteja inativo
      if( !effects.isEnabled ) return;

      // Identificadores pré-validação
      var being = this,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.BioticBeing );
        m.oAssert( matchFlow.period instanceof m.CombatPeriod );
      }

      // Identificadores pós-validação
      var nextSegment = matchFlow.period.getStageFrom( matchFlow.segment, 1 ),
          diseasedCondition = being.conditions.find( condition => condition instanceof m.ConditionDiseased ) ?? new m.ConditionDiseased( { source: spell } );

      // Aplica condição de enfermo ao ente alvo
      diseasedCondition.apply( being );

      // Caso haja um próximo segmento, adiciona ao fim de seu início evento para programar próxima adição de um marcador de enfermo
      if( nextSegment ) m.events.flowChangeEnd.begin.add( nextSegment, () => effects.scheduleDiseasedGain( { card: being } ), { once: true } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellMiasma.gridOrder = m.data.cards.spells.push( SpellMiasma );
}

/// Propriedades do protótipo
SpellMiasma.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellMiasma }
} );
