// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellFireball = function ( config = {} ) {
  // Iniciação de propriedades
  SpellFireball.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellFireball.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellFireball );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-fireball';

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = true;

    /// Adiciona preparativos para a execução de uma ação propagada
    spell.prepareActionExecution = function ( action, actionObject = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( actionObject.constructor == Object );
      }

      // Caso alvo da magia seja um ente físico, ajusta seu tipo de alvo
      if( this.target instanceof m.PhysicalBeing ) this.content.typeset.attachability = m.PhysicalBeing;
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Redefine tipo de alvo da magia para casas
      this.content.typeset.attachability = m.CardSlot;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.CardSlot;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 4;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Define o alvo da magia
    effects.setChannelingTarget = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
        m.oAssert( !this.currentExecution );
      }

      // Captura e inicia execução da seleção do alvo da magia
      ( this.currentExecution = executeTargetSelection() ).next();

      // Função para início da execução da seleção do alvo da magia
      function * executeTargetSelection() {
        // Inseri modal perguntando se jogador deseja alterar alvo da magia para um ente físico
        new m.ConfirmationModal( {
          text: m.languages.notices.getModalText( 'change-target-of-fireball-spell' ),
          acceptAction: () => effects.currentExecution.next( true ),
          declineAction: () => effects.currentExecution.next( false )
        } ).insert();

        // Captura decisão do jogador
        let isToChangeTarget = yield;

        // Caso alvo deva ser alterado, realiza essa alteração
        if( isToChangeTarget ) spell.content.typeset.attachability = m.PhysicalBeing;

        // Define alvo da magia
        spell.target = yield action.selectTarget( isToChangeTarget ? 'choose-target-being' : 'choose-target-card-slot' );

        // Nulifica execução atual do efeito da magia
        effects.currentExecution = null;

        // Prossegue com execução da ação
        yield setTimeout( () => action.currentExecution.next() );
      }
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Delega execução da validação do alvo em função de se ele é uma casa ou um ente físico
      return target instanceof m.CardSlot ? validateCardSlot() : validatePhysicalBeing();

      // Funções

      /// Para validação de casas
      function validateCardSlot() {
        // Identificadores
        var beingsInTarget = target instanceof m.EngagementZone ? target.cards : [ target.card ].filter( card => card );

        // Invalida alvo caso ele não esteja sendo ocupado por um ente físico
        if( !beingsInTarget.some( being => being instanceof m.PhysicalBeing ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-a-physical-being' ) );

        // Indica que alvo é válido
        return true;
      }

      /// Para validação de entes físicos
      function validatePhysicalBeing() {
        // Invalida alvo caso ele não possa ser mirado devido à validação de 'Atrair'
        if( !m.SpellAttract.validateMagicShot( target, action, true ) ) return false;

        // Indica que alvo é válido
        return true;
      }
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var cardSlot = spell.target instanceof m.CardSlot ? spell.target : spell.target.slot,
            fireDamage = new m.DamageFire( { singlePoints: 5, source: spell } );

        // Define pontos de ataque do dano de fogo como iguais a 2 mais a distância entre a casa do acionante e o alvo da magia
        fireDamage.attackPoints = 2 + m.FieldSlot.getDistanceFrom( spell.owner.slot, cardSlot );

        // Desfere ataque contra um único ente físico
        attackPhysicalBeing: {
          // Não executa bloco caso alvo não seja um ente físico
          if( !( spell.target instanceof m.PhysicalBeing ) ) break attackPhysicalBeing;

          // Inicia parada de destino para consolidação do ataque contra o ente físico escolhido
          m.GameMatch.programFateBreak( spell.getRelationships().controller, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: spell.owner,
            opponentBeing: spell.target,
            isContestable: true,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Encerra bloco caso parada de destino não tenha sido bem-sucedida
          if( !wasUsedFatePoint ) break attackPhysicalBeing;

          // Altera pontos de dano do ataque para 8
          fireDamage.singlePoints = 8;

          // Desfere ataque contra ente físico escolhido, e encerra execução do efeito
          return fireDamage.setTotalPoints( spell.target ).inflictPoints( spell.target );
        }

        // Desfere ataque contra os entes na casa (do) alvo
        attackBeings: {
          // Identificadores
          let attackTargets = cardSlot.cards?.filter( card => card instanceof m.PhysicalBeing ) ?? [ cardSlot.card ];

          // Inflige dano de fogo aos ocupantes da casa alvo
          for( let attackTarget of attackTargets ) fireDamage.setTotalPoints( attackTarget ).inflictPoints( attackTarget );
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellFireball.gridOrder = m.data.cards.spells.push( SpellFireball );
}

/// Propriedades do protótipo
SpellFireball.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellFireball }
} );
