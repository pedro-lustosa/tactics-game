// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellEarthquake = function ( config = {} ) {
  // Iniciação de propriedades
  SpellEarthquake.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellEarthquake.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellEarthquake );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-earthquake';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'sustained';

    /// Subtipo de duração da magia
    typeset.durationSubtype = 'constant';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 3;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 12;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 6;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 6;
  }

  // Ordem inicial das variações desta carta na grade
  SpellEarthquake.gridOrder = m.data.cards.spells.push( SpellEarthquake );
}

/// Propriedades do protótipo
SpellEarthquake.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellEarthquake }
} );
