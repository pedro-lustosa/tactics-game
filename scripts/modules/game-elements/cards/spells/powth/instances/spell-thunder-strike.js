// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellThunderStrike = function ( config = {} ) {
  // Iniciação de propriedades
  SpellThunderStrike.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellThunderStrike.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellThunderStrike );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-thunder-strike';

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.CardSlot;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R4';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 4;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var beingsInTarget = target instanceof m.EngagementZone ? target.cards : [ target.card ].filter( card => card );

      // Invalida alvo caso ele não esteja sendo ocupado por um ente com condutividade
      if( !beingsInTarget.some( being => being.content.stats.effectivity.conductivity.current ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-a-being-with-conductivity' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var cardSlot = spell.target,
            beingsInTarget = cardSlot.cards?.slice() ?? [ cardSlot.card ],
            attackTarget = beingsInTarget.sort( ( a, b ) =>
              b.content.stats.effectivity.conductivity.current - a.content.stats.effectivity.conductivity.current
            )[ 0 ];

        // Executa lógica relativa à negação do ataque desta magia
        negateAttack: {
          // Não executa bloco caso condutividade de alvo seja maior que 1
          if( attackTarget.content.stats.effectivity.conductivity.current > 1 ) break negateAttack;

          // Inicia parada de destino sobre negação do ataque
          m.GameMatch.programFateBreak( attackTarget.getRelationships().controller, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: attackTarget,
            opponentBeing: spell.owner,
            isContestable: false,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: spell, attackTarget: attackTarget } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Encerra função caso parada de destino tenha sido bem-sucedida
          if( wasUsedFatePoint ) return;
        }

        // Aplica dano de choque ao alvo do ataque
        new m.DamageShock( { singlePoints: 8, source: spell } ).setTotalPoints( attackTarget, 5 ).inflictPoints( attackTarget );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellThunderStrike.gridOrder = m.data.cards.spells.push( SpellThunderStrike );
}

/// Propriedades do protótipo
SpellThunderStrike.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellThunderStrike }
} );
