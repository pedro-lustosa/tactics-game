// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAuspiciousWinds = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAuspiciousWinds.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAuspiciousWinds.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAuspiciousWinds );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-auspicious-winds';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'sustained';

    /// Subtipo de duração da magia
    typeset.durationSubtype = 'constant';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 6;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 3;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 3;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAuspiciousWinds.gridOrder = m.data.cards.spells.push( SpellAuspiciousWinds );
}

/// Propriedades do protótipo
SpellAuspiciousWinds.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellAuspiciousWinds }
} );
