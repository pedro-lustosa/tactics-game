// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellManaDisruption = function ( config = {} ) {
  // Iniciação de propriedades
  SpellManaDisruption.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellManaDisruption.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellManaDisruption );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-mana-disruption';

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.CardSlot;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R4';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;
  }

  // Ordem inicial das variações desta carta na grade
  SpellManaDisruption.gridOrder = m.data.cards.spells.push( SpellManaDisruption );
}

/// Propriedades do protótipo
SpellManaDisruption.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellManaDisruption }
} );
