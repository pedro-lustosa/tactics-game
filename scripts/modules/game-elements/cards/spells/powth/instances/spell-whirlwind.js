// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellWhirlwind = function ( config = {} ) {
  // Iniciação de propriedades
  SpellWhirlwind.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellWhirlwind.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellWhirlwind );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-whirlwind';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.EngagementZone;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;
  }

  // Ordem inicial das variações desta carta na grade
  SpellWhirlwind.gridOrder = m.data.cards.spells.push( SpellWhirlwind );
}

/// Propriedades do protótipo
SpellWhirlwind.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellWhirlwind }
} );
