// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellVoltaicOutburst = function ( config = {} ) {
  // Iniciação de propriedades
  SpellVoltaicOutburst.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellVoltaicOutburst.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellVoltaicOutburst );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-voltaic-outburst';

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    /// Configura ação 'sustentar' relativa à magia
    spell.configSustainAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionSustain );
        m.oAssert( action.target == this );
      }

      // Adiciona evento para desferir ataque da magia após sua conclusão
      m.events.actionChangeEnd.finish.add( action, this.content.effects.finish, { context: this.content.effects } );

      // Adiciona evento para revalidar magia ao início de sua aborção
      m.events.actionChangeStart.abort.add( action, this.content.effects.revalidateToFinish, { context: this.content.effects } );
    }

    /// Desconfigura ação 'sustentar' relativa à magia
    spell.unconfigSustainAction = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionSustain );

      // Remove evento para desferir ataque da magia após sua conclusão
      m.events.actionChangeEnd.finish.remove( action, this.content.effects.finish, { context: this.content.effects } );

      // Remove evento para revalidar magia ao início de sua aborção
      m.events.actionChangeStart.abort.remove( action, this.content.effects.revalidateToFinish, { context: this.content.effects } );
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Nulifica dano de choque da magia
      this.content.effects.shockDamage = null;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Dano de choque da magia
    effects.shockDamage = null;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var cardSlot = target.slot,
          beingsInCardSlot = cardSlot instanceof m.EngagementZone ? cardSlot.cards : [ target ];

      // Invalida alvo caso ele não tenha condutividade
      if( !target.content.stats.effectivity.conductivity.current )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-conductivity' ) );

      // Invalida alvo caso ele não esteja com ao menos 1 outro ente que tenha condutividade
      if( beingsInCardSlot.filter( being => being.content.stats.effectivity.conductivity.current ).length < 2 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-with-being-with-conductivity' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Gera e registra dano de choque da magia
      this.shockDamage = new m.DamageShock( { singlePoints: 1, source: spell } );

      // Define pontos de ataque do dano de choque
      this.shockDamage.attackPoints = 6;
    }

    /// Provoca efeito relativo à sustentação da magia
    effects.sustainEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'sustain' && eventTarget == spell );

      // Aumenta em 1 ponto dano de choque da magia
      this.shockDamage.singlePoints++;
    }

    /// Revalida ação, e adiciona evento para sua conclusão caso tenha sido revalidada
    effects.revalidateToFinish = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType, eventTarget: action, actionTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'action-change' && eventPhase == 'start' && eventType == 'abort' && action instanceof m.ActionSustain );
        m.oAssert( actionTarget == spell );
      }

      // Caso ação ainda seja válida, adiciona evento para desferimento do ataque da magia
      if( action.revalidate() ) return m.events.actionChangeEnd.abort.add( action, this.finish, { context: this, once: true } );

      // Notifica que ataque relativo à magia não pode ser desferido
      return m.noticeBar.show( m.languages.notices.getOutcome( `voltaic-outburst-attack-prevented`, { action: action } ),
        !m.GameMatch.current.isSimulation && m.GamePlayer.current == m.GameMatch.current.players.get( 'opponent' ) ? 'green' : 'red'
      );
    }

    /// Conclui efeito relativo à magia
    effects.finish = function ( eventData = {} ) {
      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: action, actionTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'action-change' && [ 'finish', 'abort' ].includes( eventType ) && action instanceof m.ActionSustain );
        m.oAssert( actionTarget == spell );
      }

      // Identificadores pós-validação
      var cardSlot = spell.target.slot,
          beingsInSlot = cardSlot.cards.slice().sort( ( a, b ) =>
            b.content.stats.effectivity.conductivity.current - a.content.stats.effectivity.conductivity.current
          ),
          targetConductivity = beingsInSlot.find( being => being != spell.target ).content.stats.effectivity.conductivity.current,
          attackTargets = beingsInSlot.filter( being => being == spell.target || being.content.stats.effectivity.conductivity.current == targetConductivity );

      // Inflige dano de choque aos alvos pertinentes
      for( let attackTarget of attackTargets ) this.shockDamage.setTotalPoints( attackTarget ).inflictPoints( attackTarget );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellVoltaicOutburst.gridOrder = m.data.cards.spells.push( SpellVoltaicOutburst );
}

/// Propriedades do protótipo
SpellVoltaicOutburst.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellVoltaicOutburst }
} );
