// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellFireRing = function ( config = {} ) {
  // Iniciação de propriedades
  SpellFireRing.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellFireRing.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellFireRing );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-fire-ring';

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'sustained';

    /// Subtipo de duração da magia
    typeset.durationSubtype = 'constant';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 6;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;
  }

  // Ordem inicial das variações desta carta na grade
  SpellFireRing.gridOrder = m.data.cards.spells.push( SpellFireRing );
}

/// Propriedades do protótipo
SpellFireRing.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellFireRing }
} );
