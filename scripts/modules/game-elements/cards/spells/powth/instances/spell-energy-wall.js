// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellEnergyWall = function ( config = {} ) {
  // Iniciação de propriedades
  SpellEnergyWall.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellEnergyWall.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellEnergyWall );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-energy-wall';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 4;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 8;
  }

  // Ordem inicial das variações desta carta na grade
  SpellEnergyWall.gridOrder = m.data.cards.spells.push( SpellEnergyWall );
}

/// Propriedades do protótipo
SpellEnergyWall.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellEnergyWall }
} );
