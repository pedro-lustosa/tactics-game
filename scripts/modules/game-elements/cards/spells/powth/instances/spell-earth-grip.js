// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellEarthGrip = function ( config = {} ) {
  // Iniciação de propriedades
  SpellEarthGrip.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellEarthGrip.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellEarthGrip );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-earth-grip';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ações a serem impedidas e bloqueadas pela magia
    effects.actionsToDeny = [ m.ActionEngage, m.ActionIntercept, m.ActionDisengage, m.ActionRetreat, m.ActionRelocate ];

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var being = spell.target,
          availableActions = being.actions.slice(),
          committingAction = m.GameAction.currents.find( action =>
            action.committer == being && this.actionsToDeny.some( constructor => action instanceof constructor )
          );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Revalida acionamento de ações disponíveis do alvo, de modo que as ações afetadas pelo efeito dessa magia se tornem indisponíveis
      for( let action of availableActions )
        if( this.actionsToDeny.some( constructor => action instanceof constructor ) ) action.constructor.controlAvailability( { eventTarget: being } );

      // Quando existente, impede ação de alvo que deva ser impedida
      committingAction?.complete( 'prevent' );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Re-executa controle de ações do ente alvo
      m.GameAction.refreshActions( spell.target );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellEarthGrip.gridOrder = m.data.cards.spells.push( SpellEarthGrip );
}

/// Propriedades do protótipo
SpellEarthGrip.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellEarthGrip }
} );
