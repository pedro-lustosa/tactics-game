// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellChargedAura = function ( config = {} ) {
  // Iniciação de propriedades
  SpellChargedAura.init( this );

  // Superconstrutor
  m.Powth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellChargedAura.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Powth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellChargedAura );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-charged-aura';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;
  }

  // Ordem inicial das variações desta carta na grade
  SpellChargedAura.gridOrder = m.data.cards.spells.push( SpellChargedAura );
}

/// Propriedades do protótipo
SpellChargedAura.prototype = Object.create( m.Powth.prototype, {
  // Construtor
  constructor: { value: SpellChargedAura }
} );
