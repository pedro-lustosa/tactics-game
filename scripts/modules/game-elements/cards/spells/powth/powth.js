// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Powth = function ( config = {} ) {
  // Superconstrutor
  m.Spell.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Powth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Spell.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Powth );

    // Atribuição de propriedades iniciais

    /// Indica se magia é um disparo mágico
    spell.isMagicShot = false;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Senda
    typeset.path = 'powth';
  }
}

/// Propriedades do protótipo
Powth.prototype = Object.create( m.Spell.prototype, {
  // Construtor
  constructor: { value: Powth }
} );
