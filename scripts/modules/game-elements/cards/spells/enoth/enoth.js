// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Enoth = function ( config = {} ) {
  // Superconstrutor
  m.Spell.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Enoth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Spell.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Enoth );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Senda
    typeset.path = 'enoth';
  }
}

/// Propriedades do protótipo
Enoth.prototype = Object.create( m.Spell.prototype, {
  // Construtor
  constructor: { value: Enoth }
} );
