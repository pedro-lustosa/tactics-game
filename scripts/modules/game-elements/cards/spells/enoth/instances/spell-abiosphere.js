// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAbiosphere = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAbiosphere.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAbiosphere.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAbiosphere );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-abiosphere';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellSanctuary;

    /// Multiplicador de marcadores de enfermo
    spell.diseasedMultiplier = 0;

    /// Grade a sofrer efeito da magia
    spell.targetGrid = null;

    /// Adiciona magias adicionas para disputas mágicas
    spell.addClashingSpells = function () {
      // Identificadores
      var clashingSpells = [],
          targetGrid = this.targetGrid ?? m.GameMatch.current.environments.field.grids[ this.getRelationships().opponent.parity ],
          playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Adiciona ao arranjo de magias disputantes magias 'Santuário' que mirem a mesma grade desta magia
      clashingSpells = clashingSpells.concat( playerSpells.filter( card => card instanceof m.SpellSanctuary && card.targetGrid == targetGrid ) );

      // Retorna magias disputantes
      return clashingSpells;
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Identificadores
      var battleTurn = m.GameMatch.current.flow.round.getChild( 'battle-turn' );

      // Remove evento para incrementar multiplicador de marcadores de enfermo por turno da batalha decorrido
      m.events.flowChangeEnd.finish.remove( battleTurn, this.content.effects.incrementDiseasedMultiplier, { context: this.content.effects } );

      // Zera multiplicador de marcadores de enfermo
      this.diseasedMultiplier = 0;

      // Nulifica grade a sofrer efeito da magia
      this.targetGrid = null;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Identificadores pós-validação
      var battleTurn = m.GameMatch.current.flow.round.getChild( 'battle-turn' );

      // Adiciona evento para incrementar multiplicador de marcadores de enfermo por turno da batalha decorrido
      m.events.flowChangeEnd.finish.add( battleTurn, this.incrementDiseasedMultiplier, { context: this } );

      // Define grade a sofrer o efeito da magia como a que pertencer ao oponente do jogador que controlar a magia
      spell.targetGrid = m.GameMatch.current.environments.field.grids[ spell.getRelationships().opponent.parity ];
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Não executa função caso efeito esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var fieldGrid = spell.targetGrid,
          bioticBeings = fieldGrid.cards.filter( card => card instanceof m.BioticBeing );

      // Itera por entes bióticos na grade alvo
      for( let being of bioticBeings ) {
        // Identificadores
        let diseasedCondition = being.conditions.find( condition => condition instanceof m.ConditionDiseased ) ?? new m.ConditionDiseased( { source: spell } ),
            diseasedMarkers = 2 * spell.diseasedMultiplier;

        // Aplica condição de enfermo ao ente alvo
        diseasedCondition.apply( being, null, diseasedMarkers );
      }
    }

    /// Incrementa contador de turnos da batalha decorridos
    effects.incrementDiseasedMultiplier = function () {
      // Incremento do contador, e retorno de seu valor atualizado
      return ++spell.diseasedMultiplier;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Ordem inicial das variações desta carta na grade
  SpellAbiosphere.gridOrder = m.data.cards.spells.push( SpellAbiosphere );
}

/// Propriedades do protótipo
SpellAbiosphere.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellAbiosphere }
} );
