// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellHypogravity = function ( config = {} ) {
  // Iniciação de propriedades
  SpellHypogravity.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellHypogravity.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellHypogravity );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-hypogravity';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 10;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 10;
  }

  // Ordem inicial das variações desta carta na grade
  SpellHypogravity.gridOrder = m.data.cards.spells.push( SpellHypogravity );
}

/// Propriedades do protótipo
SpellHypogravity.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellHypogravity }
} );
