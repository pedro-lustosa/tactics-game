// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellGrowth = function ( config = {} ) {
  // Iniciação de propriedades
  SpellGrowth.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellGrowth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellGrowth );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-growth';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellShrinkage;

    /// Indica se magia tem custo de persistência dinâmico
    spell.isDynamicPersistence = true;

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Identificadores
      var spellStats = this.content.stats;

      // Não executa função caso custos de mana atuais já sejam iguais aos originais
      if( [ 'min', 'max' ].every( key => spellStats.current.manaCost.toChannel[ key ] == spellStats.base.manaCost.toChannel[ key ] ) ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( this, { type: 'toChannel' } );

      // Redefine para valores originais manas mínimo e máximo para canalização
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] = spellStats.base.manaCost.toChannel[ key ];

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( this, { type: 'toChannel' } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Define quantidade mínima de mana para canalização da magia
    effects.getMinMana = function ( target = spell.target ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( target == spell.target || !spell.target );
      }

      // Define quantidade mínima de mana enquanto 1 mais a quantidade de vinculados embutidos com tamanho de alvo
      return 1 + target.getAllCardAttachments( 'embedded' ).filter( card => card.content.typeset.size ).length;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var spellStats = spell.content.stats,
          formerIsQuiet = m.app.isQuiet;

      // Invalida alvo caso ele não tenha tamanho
      if( !target.content.typeset.size )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-size' ) );

      // Invalida alvo caso seu tamanho seja o grande
      if( target.content.typeset.size == 'large' )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-size-must-be-smaller-than-large' ) );

      // Valida alvo caso já haja um mana a ser atribuído
      if( action.manaToAssign ) return true;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Ajusta provisoriamente custo de mana mínimo da magia
      spellStats.current.manaCost.toChannel.min = this.getMinMana( target );

      // Captura resultado da validação do custo de mana
      let validationResult = m.Spell.validateManaCost( action );

      // Restaura custo de mana mínimo da magia para valor original
      spellStats.current.manaCost.toChannel.min = spellStats.base.manaCost.toChannel.min;

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Invalida alvo caso acionante não possa pagar o custo mínimo de mana previsto tendo-o como referência
      if( !validationResult )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-attachments-with-size-for-spell' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var { manaCost } = spell.content.stats.current,
          minMana = this.getMinMana();

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel' } );

      // Define o custo mínimo de canalização da magia e seu custo de persistência como igual ao mana mínimo definido
      manaCost.toPersist = manaCost.toChannel.min = minMana;

      // Define o custo máximo de canalização da magia como igual a três vezes o custo mínimo, ou seu maior múltiplo que seja menor que o custo máximo de uso
      for( let i = minMana; i <= minMana * 3 && i <= manaCost.maxUsage; i += minMana ) manaCost.toChannel.max = i;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel' } );

      // Prossegue com execução da ação
      return action.currentExecution.next();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Identificadores pós-validação
      var spellStats = spell.content.stats;

      // Não executa função caso custos de mana atuais já sejam iguais aos originais
      if( [ 'min', 'max' ].every( key => spellStats.current.manaCost.toChannel[ key ] == spellStats.base.manaCost.toChannel[ key ] ) ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel' } );

      // Redefine para valores originais manas mínimo e máximo para canalização
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] = spellStats.base.manaCost.toChannel[ key ];

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel' } );
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Ajusta tamanho do alvo da magia
      m.PhysicalBeing.adjustSizeByEffect( spell.target );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Ajusta tamanho do alvo da magia
      m.PhysicalBeing.adjustSizeByEffect( spell.target );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellGrowth.gridOrder = m.data.cards.spells.push( SpellGrowth );
}

/// Propriedades do protótipo
SpellGrowth.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellGrowth }
} );
