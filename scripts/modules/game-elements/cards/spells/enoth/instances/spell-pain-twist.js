// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellPainTwist = function ( config = {} ) {
  // Iniciação de propriedades
  SpellPainTwist.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellPainTwist.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellPainTwist );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-pain-twist';

    /// Ente a transferir dano para o alvo da magia
    spell.damageGiver = null;

    /// Dano a ser transferido do primeiro alvo para o segundo
    spell.damageToAssign = 0;

    /// Adiciona dados a um objeto de ação, a ser propagado para outros usuários na partida
    spell.addToActionObject = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionChannel );

      // Adição de dados próprios
      return { damageGiver: this.damageGiver.getMatchId(), damageToAssign: this.damageToAssign };
    }

    /// Adiciona preparativos para a execução de uma ação propagada
    spell.prepareActionExecution = function ( action, actionObject = {} ) {
      // Identificadores
      var { damageGiver: damageGiverId, damageToAssign } = actionObject;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( actionObject.constructor == Object );
        m.oAssert( damageGiverId && typeof damageGiverId == 'string' );
        m.oAssert( Number.isInteger( damageToAssign ) && damageToAssign > 0 );
      }

      // Registra ente a transferir dano para o alvo da magia
      spell.damageGiver = m.Card.getFromMatchId( damageGiverId );

      // Registra dano a ser infligido ao alvo da magia
      spell.damageToAssign = damageToAssign;
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Nulifica ente a transferir dano para o alvo da magia
      this.damageGiver = null;

      // Zera dano a ser transferido do primeiro alvo para o segundo
      this.damageToAssign = 0;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Retorna dano físico total sofrido pelo ente passado
    effects.getInflictedPhysicalDamage = function ( being = spell.damageGiver ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof typeset.attachability );
        m.oAssert( !spell.damageGiver || spell.damageGiver == being );
      }

      // Identificadores
      var { flow: matchFlow, log: matchLog } = m.GameMatch.current,
          flowChains = Object.keys( matchLog.body ),
          regExp = new RegExp( `\.${ matchFlow.turn.name }-${ matchFlow.turn.counter }\.${ matchFlow.period.name }\.` ),
          physicalDamages = [ 'blunt', 'slash', 'pierce' ],
          sufferedDamage = 0;

      // Itera por cadeias de fluxo já decorridas
      for( let flowChain of flowChains ) {
        // Filtra cadeias de fluxo que não pertençam ao período do combate atual
        if( !regExp.test( flowChain ) ) continue;

        // Identificadores
        let stageEvents = matchLog.body[ flowChain ].events;

        // Itera por arranjo de eventos ocorridos na cadeia de fluxo alvo
        for( let event of stageEvents ) {
          // Filtra eventos que não sejam sobre cura ou danos infligidos
          if( ![ 'damage', 'heal' ].includes( event.type ) ) continue;

          // Para eventos de dano
          if( event.type == 'damage' ) {
            // Filtra eventos cujo alvo seja distinto do ente passado
            if( event.damageTarget != being ) continue;

            // Computa danos físicos sofridos no evento sobre dano infligido atual
            for( let physicalDamage of physicalDamages ) sufferedDamage += event.inflictedDamages[ physicalDamage ] ?? 0;
          }

          // Para eventos de cura
          else {
            // Filtra eventos cujo alvo seja distinto do ente passado
            if( event.healTarget != being ) continue;

            // Subtrai do valor de danos infligidos danos curados
            sufferedDamage -= event.healedDamage;
          }
        }
      }

      // Retorna quantidade de dano físico sofrida pelo ente passado
      return sufferedDamage;
    }

    /// Define o alvo da magia
    effects.setChannelingTarget = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
        m.oAssert( !this.currentExecution );
      }

      // Captura e inicia execução da seleção dos alvos da magia
      ( this.currentExecution = executeTargetSelection() ).next();

      // Função para início da execução da seleção dos alvos da magia
      function * executeTargetSelection() {
        // Aguarda escolha do alvo a ter dano transferido
        spell.damageGiver = yield action.selectTarget( 'choose-target-for-give-damage' );

        // Aguarda escolha do alvo a receber dano transferido
        spell.target = yield action.selectTarget( 'choose-target-for-receive-damage' );

        // Adiciona ao arranjo de alvos adicionais da magia ente a transferir dano para o alvo da magia
        spell.extraTargets.push( spell.damageGiver );

        // Nulifica execução atual do efeito da magia
        effects.currentExecution = null;

        // Prossegue com execução da ação
        yield setTimeout( () => action.currentExecution.next() );
      }
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Delega execução da validação do alvo em função de se ele é o a transferir ou receber dano
      return spell.damageGiver && !spell.target || spell.target == target ? validateDamageReceiver.call( this ) : validateDamageGiver.call( this );

      // Funções

      /// Para validação do alvo a transferir dano
      function validateDamageGiver() {
        // Identificadores
        var inflictedDamage = this.getInflictedPhysicalDamage( target );

        // Invalida alvo caso ele não tenha (mais) nenhum dano físico infligido no combate atual
        if( inflictedDamage <= 0 )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'damage-giver-must-have-suffered-physical-damage' ) );

        // Invalida alvo caso dano físico no momento infligido a ele seja menor que dano a ser transferido
        if( inflictedDamage < this.damageToAssign )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'damage-giver-has-not-enough-damage-to-assign' ) );

        // Indica que alvo é válido
        return true;
      }

      /// Para validação do alvo a receber dano
      function validateDamageReceiver() {
        // Invalida alvo caso ele seja igual ao a transferir dano
        if( spell.damageGiver == target )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'damage-receiver-must-be-different-from-damage-giver' ) );

        // Indica que alvo é válido
        return true;
      }
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var sufferedDamage = spell.damageToAssign = this.getInflictedPhysicalDamage(),
          fifthDamage = Math.round( sufferedDamage * .2 ),
          manaToAssign = 1;

      // Enquanto potência prevista do mana a ser atribuído for menor que 1/5 do dano infligido a alvo, incrementa em 1 o mana
      while( spell.gaugePotency( action, manaToAssign ) < fifthDamage ) manaToAssign++;

      // Ajusta mana a ser atribuído para que ele não seja menor que quantidade mínima de mana a ser atribuído
      manaToAssign = Math.max( manaToAssign, action.getMinAssignableMana() );

      // Informa usuário sobre mana previsto para uso e dano máximo a ser transferido, e aguarda seu consentimento
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-spell-mana-cost-and-inflicted-damage', {
          manaPoints: manaToAssign,
          damagePoints: sufferedDamage,
          isEnergy: action.committer instanceof m.AstralBeing
        } ),
        acceptAction: () => action.currentExecution.next( manaToAssign ),
        declineAction: () => action.cancel()
      } ).replace();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { owner, damageGiver, target: damageReceiver, damageToAssign } = spell;

        // Controle da parada de destino
        fateBreak: {
          // Não executa bloco caso dono seja um dos alvos da magia
          if( [ damageGiver, damageReceiver ].some( being => being == owner ) ) break fateBreak;

          // Não executa bloco caso dono não seja um ente que possa ser mirado por esta magia
          if( !( owner instanceof spell.content.typeset.attachability ) ) break fateBreak;

          // Identificadores
          let ownerOpponent = owner.getRelationships().opponent,
              receiverController = damageReceiver.getRelationships().controller;

          // Lança parada de destino sobre troca do ente a receber dano
          m.GameMatch.programFateBreak( ownerOpponent, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: receiverController == ownerOpponent ? damageReceiver : null,
            opponentBeing: owner,
            isContestable: true,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Para caso parada de destino tenha sido bem-sucedida
          if( wasUsedFatePoint ) {
            // Ajusta ente a receber dano de modo que ele seja igual a dono
            spell.target = damageReceiver = owner;

            // Para impedir revalidação ante 'Pingente de Oricalco', retira ente do qual transferir dano do arranjo de alvos adicionais
            spell.extraTargets.splice( spell.extraTargets.indexOf( damageGiver ), 1 );

            // Verifica se magia deve ter sua potência reduzida, segundo o efeito de 'Pingente de Oricalco'
            m.ImplementOrichalcumPendant.checkEffectActivation( spell );

            // Recoloca ente do qual transferir dano no arranjo de alvos adicionais
            spell.extraTargets.push( damageGiver );

            // Caso magia não tenha mais potência, encerra execução do efeito
            if( !spell.content.stats.current.potency ) return;
          }
        }

        // Atribuição da cura e dano
        healAndDamageAssignment: {
          // Identificadores
          let giverAttributes = damageGiver.content.stats.attributes,
              receiverAttributes = damageReceiver.content.stats.attributes;

          // Ajusta dano a ser infligido de modo que ele não ultrapasse vida atual do destinatário
          damageToAssign = Math.min( damageToAssign, receiverAttributes.current.health );

          // Captura pontos de vida a serem restaurados
          let healedDamage = Math.min( giverAttributes.base.health - giverAttributes.current.health, damageToAssign );

          // Para caso haja pontos de vida a serem restaurados
          if( healedDamage > 0 ) {
            // Restaura vida do ente a transferir dano
            giverAttributes.restore( damageToAssign, 'health', { triggeringCard: spell } );

            // Registra restauração da vida
            m.GameMatch.current.log.addEntry( { eventType: 'heal', eventTarget: healedDamage, healTarget: damageGiver, source: spell } );
          }

          // Aplica dano ao ente a receber dano
          new m.DamageRaw( { totalPoints: damageToAssign, source: spell } ).inflictPoints( damageReceiver );
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellPainTwist.gridOrder = m.data.cards.spells.push( SpellPainTwist );
}

/// Propriedades do protótipo
SpellPainTwist.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellPainTwist }
} );
