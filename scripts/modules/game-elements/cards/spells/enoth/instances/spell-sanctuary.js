// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellSanctuary = function ( config = {} ) {
  // Iniciação de propriedades
  SpellSanctuary.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Condições afetadas pelo efeito da magia
  SpellSanctuary.conditionsToPrevent = [ 'condition-weakened', 'condition-unlivened', 'condition-diseased', 'condition-possessed' ];

  // Iniciação de propriedades
  SpellSanctuary.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellSanctuary );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-sanctuary';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellAbiosphere;

    /// Grade a sofrer efeito da magia
    spell.targetGrid = null;

    /// Adiciona magias adicionas para disputas mágicas
    spell.addClashingSpells = function () {
      // Identificadores
      var clashingSpells = [],
          targetGrid = this.targetGrid ?? m.GameMatch.current.environments.field.grids[ this.getRelationships().controller.parity ],
          playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Adiciona ao arranjo de magias disputantes magias 'Abiosfera' que mirem a mesma grade desta magia
      clashingSpells = clashingSpells.concat( playerSpells.filter( card => card instanceof m.SpellAbiosphere && card.targetGrid == targetGrid ) );

      // Retorna magias disputantes
      return clashingSpells;
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Nulifica grade a sofrer efeito da magia
      this.targetGrid = null;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var { conditionsToPrevent } = SpellSanctuary,
          preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' ),
          fieldGrid = spell.targetGrid ??= m.GameMatch.current.environments.field.grids[ spell.getRelationships().controller.parity ],
          bioticBeings = fieldGrid.cards.filter( card => card instanceof m.BioticBeing );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Itera por entes bióticos na grade alvo
      for( let being of bioticBeings ) {
        // Identificadores
        let beingConditions = being.conditions.filter( condition => conditionsToPrevent.includes( condition.name ) );

        // Remove do ente alvo condições atuais indesejadas
        for( let beingCondition of beingConditions ) beingCondition.drop();
      }

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var fieldGrid = spell.targetGrid,
          bioticBeings = fieldGrid.cards.filter( card => card instanceof m.BioticBeing );

      // Itera por entes bióticos na grade alvo
      for( let being of bioticBeings ) {
        // Identificadores
        let beingAttributes = being.content.stats.attributes,
            healthToRestore = beingAttributes.base.health - beingAttributes.current.health;

        // Encerra função caso não haja vida a ser restaurada
        if( healthToRestore <= 0 ) return;

        // Restaura vida do ente alvo
        beingAttributes.restore( healthToRestore, 'health', { triggeringCard: spell } );

        // Registra restauração da vida
        m.GameMatch.current.log.addEntry( { eventType: 'heal', eventTarget: healthToRestore, healTarget: being, source: spell } );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Valida aplicação de uma condição ante efeito da magia
  SpellSanctuary.validateConditionApply = function( target, conditionName, isBeforeApply = false ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( target instanceof m.Being );
      m.oAssert( conditionName && typeof conditionName == 'string' );
      m.oAssert( typeof isBeforeApply == 'boolean' );
    }

    // Valida aplicação da condição caso ela não seja uma das condições abarcadas por esta magia
    if( !this.conditionsToPrevent.includes( conditionName ) ) return true;

    // Valida aplicação da condição caso ente não seja um biótico
    if( !( target instanceof m.BioticBeing ) ) return true;

    // Valida aplicação da condição caso ente não esteja em uma grade do campo
    if( !( target.slot instanceof m.FieldGridSlot ) ) return true;

    // Identificadores para prosseguir validações
    let targetGrid = target.slot.parent,
        playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Valida aplicação da condição caso não exista uma magia 'Santuário' ativa para a grade do ente alvo
    if( !playerSpells.some( card => card instanceof this && card.targetGrid == targetGrid && card.content.effects.isEnabled ) ) return true;

    // Notifica que aplicação da condição a alvo foi impedida
    return m.noticeBar.show( m.languages.notices.getOutcome(
      'condition-prevented-by-sanctuary', { target: target, conditionName: conditionName, presentTense: isBeforeApply }
    ), isBeforeApply || target.getRelationships().opponent == m.GamePlayer.current ? 'red' : 'green' );
  }

  // Ordem inicial das variações desta carta na grade
  SpellSanctuary.gridOrder = m.data.cards.spells.push( SpellSanctuary );
}

/// Propriedades do protótipo
SpellSanctuary.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellSanctuary }
} );
