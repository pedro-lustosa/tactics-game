// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellRestoration = function ( config = {} ) {
  // Iniciação de propriedades
  SpellRestoration.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellRestoration.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellRestoration );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-restoration';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellRuination;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Item;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;
  }

  // Ordem inicial das variações desta carta na grade
  SpellRestoration.gridOrder = m.data.cards.spells.push( SpellRestoration );
}

/// Propriedades do protótipo
SpellRestoration.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellRestoration }
} );
