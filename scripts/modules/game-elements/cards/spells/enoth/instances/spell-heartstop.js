// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellHeartstop = function ( config = {} ) {
  // Iniciação de propriedades
  SpellHeartstop.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellHeartstop.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellHeartstop );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-heartstop';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellBodyCleansing;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var targetAttributes = target.content.stats.attributes;

      // Invalida alvo caso seu vigor não seja menor que 5
      if( targetAttributes.current.stamina >= 5 )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-less-than-5-stamina' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores pós-validação
      var { target } = spell;

      // Afasta alvo
      target.inactivate( 'withdrawn', { triggeringCard: spell } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellHeartstop.gridOrder = m.data.cards.spells.push( SpellHeartstop );
}

/// Propriedades do protótipo
SpellHeartstop.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellHeartstop }
} );
