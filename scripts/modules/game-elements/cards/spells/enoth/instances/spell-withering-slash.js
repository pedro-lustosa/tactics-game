// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellWitheringSlash = function ( config = {} ) {
  // Iniciação de propriedades
  SpellWitheringSlash.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellWitheringSlash.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellWitheringSlash );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-withering-slash';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Weapon;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;
  }

  // Ordem inicial das variações desta carta na grade
  SpellWitheringSlash.gridOrder = m.data.cards.spells.push( SpellWitheringSlash );
}

/// Propriedades do protótipo
SpellWitheringSlash.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellWitheringSlash }
} );
