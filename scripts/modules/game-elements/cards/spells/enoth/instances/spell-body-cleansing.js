// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellBodyCleansing = function ( config = {} ) {
  // Iniciação de propriedades
  SpellBodyCleansing.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Condições afetadas pelo efeito da magia
  SpellBodyCleansing.conditionsToPrevent = [ 'condition-weakened', 'condition-unlivened', 'condition-diseased', 'condition-possessed' ];

  // Iniciação de propriedades
  SpellBodyCleansing.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellBodyCleansing );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-body-cleansing';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellHeartstop;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var { target: being } = spell,
          { conditionsToPrevent } = SpellBodyCleansing,
          beingConditions = being.conditions.filter( condition => conditionsToPrevent.includes( condition.name ) );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Remove do alvo da magia condições atuais indesejadas
      for( let beingCondition of beingConditions ) beingCondition.drop();

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Valida aplicação de uma condição ante efeito da magia
  SpellBodyCleansing.validateConditionApply = function( target, conditionName, isBeforeApply = false ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( target instanceof m.Being );
      m.oAssert( conditionName && typeof conditionName == 'string' );
      m.oAssert( typeof isBeforeApply == 'boolean' );
    }

    // Valida aplicação da condição caso ela não seja uma das condições abarcadas por esta magia
    if( !this.conditionsToPrevent.includes( conditionName ) ) return true;

    // Valida aplicação da condição caso não exista uma magia 'Purificação Corporal' ativa em alvo
    if( !target.getAllCardAttachments( 'external' ).some( attachment => attachment instanceof this && attachment.content.effects.isEnabled ) )
      return true;

    // Notifica que aplicação da condição a alvo foi impedida
    return m.noticeBar.show( m.languages.notices.getOutcome(
      'condition-prevented-by-body-cleasing', { target: target, conditionName: conditionName, presentTense: isBeforeApply }
    ), isBeforeApply || target.getRelationships().opponent == m.GamePlayer.current ? 'red' : 'green' );
  }

  // Ordem inicial das variações desta carta na grade
  SpellBodyCleansing.gridOrder = m.data.cards.spells.push( SpellBodyCleansing );
}

/// Propriedades do protótipo
SpellBodyCleansing.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellBodyCleansing }
} );
