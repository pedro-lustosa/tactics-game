// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellWeakness = function ( config = {} ) {
  // Iniciação de propriedades
  SpellWeakness.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellWeakness.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellWeakness );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-weakness';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellStrengthness;

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var weakenedCondition = target.conditions.find( condition => condition instanceof m.ConditionWeakened ),
          strengthenedCondition = target.conditions.find( condition => condition instanceof m.ConditionStrengthened );

      // Invalida alvo caso ele esteja com quantidade máxima de marcadores de enfraquecido
      if( weakenedCondition && weakenedCondition.markers >= weakenedCondition.maxMarkers )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-have-max-weakened-markers' ) );

      // Valida alvo caso ele esteja com a condição 'fortalecido'
      if( strengthenedCondition ) return true;

      // Invalida alvo caso ele esteja vinculado à magia 'Purificação Corporal', ou caso magia 'Santuário' esteja ativa em sua grade
      if( [ m.SpellSanctuary, m.SpellBodyCleansing ].some( constructor => !constructor.validateConditionApply( target, 'condition-weakened', true ) ) )
        return false;

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Delega operação para função em comum com 'sustainEffect'
      return this.apply();
    }

    /// Provoca efeito relativo à sustentação da magia
    effects.sustainEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'sustain' && eventTarget == spell );

      // Delega operação para função em comum com 'useEffect'
      return this.apply();
    }

    /// Aplica efeito da magia
    effects.apply = function () {
      // Identificadores
      var { target } = spell,
          strengthenedCondition = target.conditions.find( condition => condition instanceof m.ConditionStrengthened );

      // Caso alvo tenha condição 'fortalecido', decrementa em 1 seus marcadores e encerra função
      if( strengthenedCondition ) return strengthenedCondition.decrease( 1, { triggeringCard: spell } );

      // Captura ou gera condição 'enfraquecido'
      let weakenedCondition = target.conditions.find( condition => condition instanceof m.ConditionWeakened ) ?? new m.ConditionWeakened();

      // Aplica a alvo condição 'enfraquecido', ou incrementa em 1 seu marcador
      weakenedCondition.apply( target, null, 1, { triggeringCard: spell } );

      // Caso marcadores de enfraquecido tenham atingido seu limite, indica que magia não deve (mais) ser sustentada
      if( weakenedCondition.markers >= weakenedCondition.maxMarkers ) spell.isToStopSustain = true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellWeakness.gridOrder = m.data.cards.spells.push( SpellWeakness );
}

/// Propriedades do protótipo
SpellWeakness.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellWeakness }
} );
