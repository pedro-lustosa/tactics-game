// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellRegeneration = function ( config = {} ) {
  // Iniciação de propriedades
  SpellRegeneration.init( this );

  // Superconstrutor
  m.Enoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellRegeneration.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Enoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellRegeneration );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-regeneration';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellDecay;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.BioticBeing;

    /// Disponibilidade
    typeset.availability = 3;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Adiciona evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.add( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.remove( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var { target: being } = spell,
          beingAttributes = being.content.stats.attributes,
          percentage = Math.min( .9, spell.content.stats.current.potency * .1 + beingAttributes.current.stamina * .05 ),
          healthToRestore = Math.min( beingAttributes.base.health - beingAttributes.current.health, Math.round( beingAttributes.base.health * percentage ) );

      // Encerra função caso não haja vida a ser restaurada
      if( healthToRestore <= 0 ) return;

      // Restaura vida do alvo da magia
      beingAttributes.restore( healthToRestore, 'health', { triggeringCard: spell } );

      // Registra restauração da vida
      m.GameMatch.current.log.addEntry( { eventType: 'heal', eventTarget: healthToRestore, healTarget: being, source: spell } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellRegeneration.gridOrder = m.data.cards.spells.push( SpellRegeneration );
}

/// Propriedades do protótipo
SpellRegeneration.prototype = Object.create( m.Enoth.prototype, {
  // Construtor
  constructor: { value: SpellRegeneration }
} );
