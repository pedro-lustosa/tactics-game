// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Metoth = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( !this.adjunctMultiplier || this.isAdjunct );
    m.oAssert( !this.isAdjunct || this.content.typeset.attachability == m.Spell );
    m.oAssert( !this.isAdjunct || this.content.typeset.duration == 'immediate' );
    m.oAssert( !this.isAdjunct || !this.content.stats.current.flowCost.toChannel );
  }

  // Superconstrutor
  m.Spell.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Metoth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Spell.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Metoth );

    // Atribuição de propriedades iniciais

    /// Indica se magia é uma magia adjunta
    spell.isAdjunct = false;

    /// Para magias adjuntas, valor a ser multiplicado pelo custo de mana da magia modificada, a fim de se determinar custo de mana da magia adjunta
    spell.adjunctMultiplier = 0;

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Senda
    typeset.path = 'metoth';
  }
}

/// Propriedades do protótipo
Metoth.prototype = Object.create( m.Spell.prototype, {
  // Construtor
  constructor: { value: Metoth }
} );
