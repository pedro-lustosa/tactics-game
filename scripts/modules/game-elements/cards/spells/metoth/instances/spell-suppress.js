// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellSuppress = function ( config = {} ) {
  // Iniciação de propriedades
  SpellSuppress.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellSuppress.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellSuppress );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-suppress';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Being;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var channelAction = m.GameAction.currents.find( gameAction => gameAction.committer == target && gameAction instanceof m.ActionChannel );

      // Invalida alvo caso ele não esteja canalizando uma magia
      if( !channelAction )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-channeling-spell' ) );

      // Invalida alvo caso magia sendo canalizada por ele não tenha mana atribuído
      if( !channelAction.manaToAssign )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-channeling-spell-with-assigned-mana' ) );

      // Invalida alvo caso potência prevista de sua magia seja maior que potência prevista para esta magia, ou a que ela poderia alcançar
      if( channelAction.target.gaugePotency( channelAction ) > spell.gaugePotency( action ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-must-be-able-to-have-no-less-potency-than-target-spell' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var channelAction = m.GameAction.currents.find( gameAction => gameAction.committer == spell.target && gameAction instanceof m.ActionChannel ),
          manaToAssign = 1;

      // Enquanto potência prevista do mana a ser atribuído for menor que a potência prevista da magia de alvo, incrementa em 1 o mana
      while( spell.gaugePotency( action, manaToAssign ) < channelAction.target.gaugePotency( channelAction ) ) manaToAssign++;

      // Ajusta mana a ser atribuído para que ele não seja menor que quantidade mínima de mana a ser atribuído
      manaToAssign = Math.max( manaToAssign, action.getMinAssignableMana() );

      // Informa usuário sobre mana previsto para uso, e aguarda seu consentimento
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-spell-mana-cost', {
          manaPoints: manaToAssign,
          isEnergy: action.committer instanceof m.AstralBeing
        } ),
        acceptAction: () => action.currentExecution.next( manaToAssign ),
        declineAction: () => action.cancel()
      } ).replace();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { owner, target: targetChanneler } = spell,
            channelAction = m.GameAction.currents.find( gameAction => gameAction.committer == targetChanneler && gameAction instanceof m.ActionChannel ),
            targetSpell = channelAction.target;

        // Impede a ação de alvo
        channelAction.complete( 'prevent' );

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Para caso magia de alvo não esteja findada ou alheada
        if( ![ 'ended', 'unlinked' ].includes( targetSpell.activity ) ) {
          // Lança parada de destino sobre findamento do alvo
          m.GameMatch.programFateBreak( owner.getRelationships().controller, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: owner,
            opponentBeing: targetChanneler,
            isContestable: false,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: targetSpell } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Caso parada de destino tenha sido bem-sucedida, finda alvo da magia; do contrário, afasta-o
          targetSpell.inactivate( wasUsedFatePoint ? 'ended' : 'withdrawn', { triggeringCard: spell } );
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellSuppress.gridOrder = m.data.cards.spells.push( SpellSuppress );
}

/// Propriedades do protótipo
SpellSuppress.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellSuppress }
} );
