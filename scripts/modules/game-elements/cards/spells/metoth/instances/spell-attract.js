// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellAttract = function ( config = {} ) {
  // Iniciação de propriedades
  SpellAttract.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellAttract.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellAttract );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-attract';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.CardSlot;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R3';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Adiciona evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.add( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.remove( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Não executa função caso casa mirada pela magia já tenha alcançado seu limite de mana vinculável
      if( spell.target.attachments.mana >= spell.target.maxAttachments.mana ) return;

      // Identificadores
      var { target: cardSlot } = spell,
          adjacentSlots = cardSlot.getAdjacentSlots().sort( sortCardSlots ),
          transferredMana = {};

      // Itera por casas adjacentes à casa alvo
      for( let slot of adjacentSlots ) {
        // Cria registro de mana transferido da casa alvo
        transferredMana[ slot.name ] = 0;

        // Operação para enquanto casa alvo tiver mana e casa mirada pela magia puder receber mana
        while( slot.attachments.mana && cardSlot.maxAttachments.mana > cardSlot.attachments.mana ) {
          // Desvincula 1 ponto de mana da casa alvo
          slot.detach( 1, 'mana' );

          // Vincula 1 ponto de mana à casa mirada pela magia
          cardSlot.attach( 1, 'mana' );

          // Incrementa registro de mana transferido da casa alvo
          transferredMana[ slot.name ]++;
        }

        // Caso nenhum ponto de mana tenha sido transferido para a casa alvo, apaga registro pertinente
        if( !transferredMana[ slot.name ] ) delete transferredMana[ slot.name ];
      }

      // Caso algum ponto de mana tenha sido transferido, registra uso desta magia
      if( Object.keys( transferredMana ).length ) this.addLogEntry( transferredMana );

      // Funções

      /// Ordena casas a sofrerem o efeito da magia
      function sortCardSlots( a, b ) {
        // Identificadores
        var slotValues = [],
            spellOwner = spell.getRelationships().owner;

        // Atribuição de pontuação comparativa
        for( let slot of [ a, b ] ) {
          // Identificadores
          let slotValue = '',
              slotSuffix = slot.name.replace( /^.+-/g, '' ),
              [ slotLetter, slotNumber ] = [ slotSuffix[ 0 ], slotSuffix[ 1 ] ],
              slotOwner = slot.owner;

          // Por letra
          slotValue += slotLetter == 'A' ? '1' : slotLetter == 'B' ? '2' : '3';

          // Por dono
          slotValue += !slotOwner ? '0' : slotOwner == spellOwner ? '1' : '2';

          // Por coluna
          slotValue += slotNumber;

          // Captura da pontuação da casa
          slotValues.push( Number( slotValue ) );
        }

        // Compara valor da primeira casa com a segunda, e as ordena decrescentemente
        return slotValues[ 1 ] - slotValues[ 0 ];
      }
    }

    /// Adiciona registro sobre ativação do efeito da magia
    effects.addLogEntry = function ( transferredMana ) {
      // Identificadores
      var match = m.GameMatch.current,
          flowChain = match.flow.toChain(),
          logData = {
            name: spell.name,
            type: 'card-effect',
            description: m.languages.notices.getOutcome( `${ spell.name }-effect`, { card: spell, transferredMana: transferredMana } )
          };

      // Indica início do registro do evento
      m.events.logChangeStart.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );

      // Atualiza arranjo de eventos do estágio em que se estiver
      match.log.body[ flowChain ].events.push( logData );

      // Indica fim do registro do evento
      m.events.logChangeEnd.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 20;
  }

  // Valida aplicação de uma magia a um ente em uma casa vinculada a 'Atrair'
  SpellAttract.validateMagicShot = function( target, action, isBeforeApply = false ) {
    // Identificadores
    var { target: spell } = action;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( target instanceof m.Being );
      m.oAssert( action instanceof m.ActionChannel );
      m.oAssert( spell instanceof m.Spell );
      m.oAssert( typeof isBeforeApply == 'boolean' );
    }

    // Valida aplicação da magia caso ela não seja um disparo mágico
    if( !spell.isMagicShot ) return true;

    // Valida aplicação da magia caso casa em que alvo esteja não seja uma do campo
    if( !( target.slot instanceof m.FieldSlot ) ) return true;

    // Captura eventuais magias 'Atrair' em alvo
    let attractSpells = target.slot.getAllCardAttachments().filter( attachment => attachment instanceof this && attachment.content.effects.isEnabled );

    // Valida aplicação da magia caso casa em que alvo esteja não esteja vinculada a nenhuma magia 'Atrair'
    if( !attractSpells.length ) return true;

    // Valida aplicação da magia caso sua potência prevista seja maior que o dobro da de cada magia 'Atrair' em alvo
    if( attractSpells.every( attractSpell => spell.gaugePotency( action ) > attractSpell.content.stats.current.potency * 2 ) ) return true;

    // Notifica que aplicação da condição a alvo foi impedida
    return m.noticeBar.show( m.languages.notices.getOutcome(
      'magic-shot-prevented-by-attract', { target: target, spell: spell, presentTense: isBeforeApply }
    ), isBeforeApply || target.getRelationships().opponent == m.GamePlayer.current ? 'red' : 'green' );
  }

  // Ordem inicial das variações desta carta na grade
  SpellAttract.gridOrder = m.data.cards.spells.push( SpellAttract );
}

/// Propriedades do protótipo
SpellAttract.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellAttract }
} );
