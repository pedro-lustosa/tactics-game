// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellShape = function ( config = {} ) {
  // Iniciação de propriedades
  SpellShape.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellShape.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellShape );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-shape';

    /// Indica se magia deve ser vinculada ao alvo
    spell.isToAttach = true;

    /// Indica que custos de mana da magia não devem ser gastos
    spell.isToSkipManaSpending = true;

    /// Indica nome da senda alvo a sofrer efeito da magia
    spell.targetPath = '';

    /// Adiciona dados a um objeto de ação, a ser propagado para outros usuários na partida
    spell.addToActionObject = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( action instanceof m.ActionChannel );

      // Adição de dados próprios
      return { targetPath: this.targetPath };
    }

    /// Adiciona preparativos para a execução de uma ação propagada
    spell.prepareActionExecution = function ( action, actionObject ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == this );
        m.oAssert( actionObject.constructor == Object && actionObject.targetPath );
      }

      // Registra senda a sofrer efeito da magia
      spell.targetPath = actionObject.targetPath;
    }

    /// Limpa dados próprios da magia
    spell.clearCustomData = function () {
      // Redefine senda a sofrer efeito da magia
      this.targetPath = '';
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Being;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'sustained';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = currentStats.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain = 1;

    /// Custo de mana máximo por uso da magia
    currentManaCost.maxUsage = 8;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentManaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
      }

      // Identificadores
      var { channeling } = owner.content.stats.combativeness,
          allowedPaths = Object.keys( channeling.paths ).filter( path => path != 'metoth' && channeling.paths[ path ].skill );

      // Invalida dono caso ele seja um ente astral
      if( owner instanceof m.AstralBeing )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-not-be-astral-being' ) );

      // Invalida dono caso ele não tenha níveis em nenhuma senda que não Metoth
      if( !allowedPaths.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-have-paths-other-than-metoth' ) );

      // Ajusta sendas permitidas para que apenas sejam consideradas sendas cujo mana atual esteja abaixo do base
      allowedPaths = allowedPaths.filter( path => channeling.mana.current[ path ] < channeling.mana.base[ path ] );

      // Invalida dono caso ele esteja sem sendas com mana atual abaixo do base
      if( !allowedPaths.length )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-have-paths-with-restorable-mana' ) );

      // Indica que dono é válido
      return true;
    }

    /// Define o alvo da magia
    effects.setChannelingTarget = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var channeler = action.committer,
          { channeling } = channeler.content.stats.combativeness,
          allowedPaths = Object.keys( channeling.paths ).filter(
            path => path != 'metoth' && channeling.paths[ path ].skill && channeling.mana.current[ path ] < channeling.mana.base[ path ]
          );

      // Define o alvo da magia como seu dono
      spell.target = action.committer;

      // Para caso haja apenas uma senda a ser escolhida para o efeito
      if( allowedPaths.length == 1 ) {
        // Registra senda a sofrer efeito da magia
        spell.targetPath = allowedPaths[ 0 ];

        // Prossegue com execução da ação
        return action.currentExecution.next();
      }

      // Inseri modal para seleção da senda a sofrer efeito da magia
      return new m.SelectModal( {
        text: m.languages.notices.getModalText( 'select-spell-path-for-spell-effect' ),
        inputsArray: allowedPaths.map( function ( path ) {
          return {
            text: m.languages.keywords.getPath( path ),
            action: function () {
              // Registra senda a sofrer efeito da magia
              spell.targetPath = path;

              // Prossegue com execução da ação
              return action.currentExecution.next();
            }
          };
        } )
      } ).replace();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Delega operação para função em comum com 'sustainEffect'
      return this.apply();
    }

    /// Provoca efeito relativo à sustentação da magia
    effects.sustainEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'sustain' && eventTarget == spell );

      // Delega operação para função em comum com 'useEffect'
      return this.apply();
    }

    /// Aplica efeito da magia
    effects.apply = function () {
      // Identificadores
      var { owner, targetPath } = spell,
          { channeling } = owner.content.stats.combativeness,
          pathsWithMostMana = Object.keys( channeling.paths ).filter( path => channeling.mana.current[ path ] == channeling.mana.current.total );

      // Reduz 1 ponto de mana de Metoth
      channeling.mana.current.metoth = Math.max( 0, channeling.mana.current.metoth - 1 );

      // Caso Metoth seja a senda com mais mana atual, e a única que atende esse critério, reduz também mana total
      if( pathsWithMostMana.length == 1 && pathsWithMostMana[ 0 ] == 'metoth' )
        channeling.mana.current.total = Math.max( 0, channeling.mana.current.total - 1 );

      // Restaura 1 ponto de mana da senda alvo
      channeling.mana.restore( 1, targetPath );

      // Caso mana atual da senda alvo não seja menor que o base, indica que magia não deve (mais) ser sustentada
      if( channeling.mana.current[ targetPath ] >= channeling.mana.base[ targetPath ] ) spell.isToStopSustain = true;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellShape.gridOrder = m.data.cards.spells.push( SpellShape );
}

/// Propriedades do protótipo
SpellShape.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellShape }
} );
