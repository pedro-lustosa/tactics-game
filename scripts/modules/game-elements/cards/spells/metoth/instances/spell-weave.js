// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellWeave = function ( config = {} ) {
  // Iniciação de propriedades
  SpellWeave.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellWeave.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellWeave );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-weave';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.PhysicalBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 4;
  }

  // Ordem inicial das variações desta carta na grade
  SpellWeave.gridOrder = m.data.cards.spells.push( SpellWeave );
}

/// Propriedades do protótipo
SpellWeave.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellWeave }
} );
