// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTear = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTear.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTear.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTear );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-tear';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Spell;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var targetDuration = target.content.typeset.duration;

      // Invalida alvo caso ele não seja uma magia persistente ou permanente
      if( ![ 'persistent', 'permanent' ].includes( targetDuration ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-persistent-or-permanent-spell' ) );

      // Invalida alvo caso ele não esteja em uso
      if( !target.isInUse )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-in-use' ) );

      // Invalida alvo caso sua potência seja maior que potência prevista para esta magia, ou a que ela poderia alcançar
      if( target.content.stats.current.potency > spell.gaugePotency( action ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-must-be-able-to-have-no-less-potency-than-target' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var { target } = spell,
          targetPotency = target.content.stats.current.potency,
          manaToAssign = 1;

      // Enquanto potência prevista do mana a ser atribuído for menor que a potência de alvo, incrementa em 1 o mana
      while( spell.gaugePotency( action, manaToAssign ) < targetPotency ) manaToAssign++;

      // Ajusta mana a ser atribuído para que ele não seja menor que quantidade mínima de mana a ser atribuído
      manaToAssign = Math.max( manaToAssign, action.getMinAssignableMana() );

      // Informa usuário sobre mana previsto para uso, e aguarda seu consentimento
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-spell-mana-cost', {
          manaPoints: manaToAssign,
          isEnergy: action.committer instanceof m.AstralBeing
        } ),
        acceptAction: () => action.currentExecution.next( manaToAssign ),
        declineAction: () => action.cancel()
      } ).replace();
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { owner, target } = spell;

        // Desusa alvo
        target.disuse();

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Para caso alvo não tenha sido removido
        if( [ 'active', 'suspended' ].includes( target.activity ) ) {
          // Lança parada de destino sobre afastamento do alvo
          m.GameMatch.programFateBreak( owner.getRelationships().controller, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: owner,
            opponentBeing: target.owner,
            isContestable: true,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: target } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Caso parada de destino tenha sido bem-sucedida, afasta alvo da magia
          if( wasUsedFatePoint ) target.inactivate( 'withdrawn', { triggeringCard: spell } );
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTear.gridOrder = m.data.cards.spells.push( SpellTear );
}

/// Propriedades do protótipo
SpellTear.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellTear }
} );
