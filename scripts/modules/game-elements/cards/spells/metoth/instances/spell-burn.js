// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellBurn = function ( config = {} ) {
  // Iniciação de propriedades
  SpellBurn.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellBurn.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellBurn );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-burn';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Spell;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellBurn.gridOrder = m.data.cards.spells.push( SpellBurn );
}

/// Propriedades do protótipo
SpellBurn.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellBurn }
} );
