// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellConsume = function ( config = {} ) {
  // Iniciação de propriedades
  SpellConsume.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellConsume.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellConsume );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-consume';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.AstralBeing;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var ownerMana = owner.content.stats.combativeness.channeling.mana;

      // Invalida dono caso ele seja um ente astral
      if( owner instanceof m.AstralBeing )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-not-be-astral-being' ) );

      // Invalida dono caso seu mana atual de Metoth seja igual ou maior que seu mana base
      if( ownerMana.current.metoth >= ownerMana.base.metoth )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'owner-current-metoth-mana-must-be-less-than-base' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var channeler = action.committer;

      // Invalida alvo caso ele não seja uma tulpa ou um espírito
      if( ![ m.Tulpa, m.Spirit ].some( constructor => target instanceof constructor ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-tulpa-or-spirit' ) );

      // Invalida alvo caso sua energia seja maior que o mana base de Metoth de acionante
      if( target.content.stats.attributes.current.energy > channeler.content.stats.combativeness.channeling.mana.base.metoth )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-energy-must-not-be-greater-than-committer-metoth-mana' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { owner, target } = spell,
            ownerMana = owner.content.stats.combativeness.channeling.mana,
            targetEnergy = target.content.stats.attributes.current.energy;

        // Reprepara dono
        owner.reprepare();

        // Restaura mana de Metoth de dono
        ownerMana.restore( targetEnergy, 'metoth' );

        // Finda alvo
        target.inactivate( 'ended', { triggeringCard: spell } );

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Possui dono
        possessOwner: {
          // Não executa bloco caso alvo não seja um espírito
          if( !( target instanceof m.Spirit ) ) break possessOwner;

          // Não executa bloco caso dono esteja vinculado à magia 'Purificação Corporal', ou caso magia 'Santuário' esteja ativa em sua grade
          if( [ m.SpellSanctuary, m.SpellBodyCleansing ].some( constructor => !constructor.validateConditionApply( target, 'condition-possessed' ) ) )
            break possessOwner;

          // Identificadores
          let { controller: ownerController, opponent: ownerOpponent } = owner.getRelationships();

          // Lança parada de destino sobre possessão de dono
          m.GameMatch.programFateBreak( ownerController, {
            name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
            playerBeing: owner,
            opponentBeing: target,
            isContestable: true,
            successAction: () => effects.currentExecution.next( true ),
            failureAction: () => effects.currentExecution.next( false ),
            modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
          } );

          // Aguarda resolução da parada de destino
          let wasUsedFatePoint = yield;

          // Para caso parada de destino não tenha sido bem-sucedida
          if( !wasUsedFatePoint ) {
            // Identificadores
            let possessedCondition = new m.ConditionPossessed( { controller: ownerOpponent } );

            // Remove eventual condição 'possuído' já existente em dono
            owner.conditions.find( condition => condition instanceof m.ConditionPossessed )?.drop();

            // Torna dono possuído
            possessedCondition.apply( owner );
          }
        }
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellConsume.gridOrder = m.data.cards.spells.push( SpellConsume );
}

/// Propriedades do protótipo
SpellConsume.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellConsume }
} );
