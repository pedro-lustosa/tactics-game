// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellPoke = function ( config = {} ) {
  // Iniciação de propriedades
  SpellPoke.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellPoke.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellPoke );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-poke';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Being;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'R5';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 1;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var targetActions = m.GameAction.currents.filter( gameAction => gameAction.committer == target );

      // Invalida alvo caso ele não esteja acionando uma ação "canalizar" ou "sustentar"
      if( !targetActions.some( gameAction => [ m.ActionChannel, m.ActionSustain ].some( constructor => gameAction instanceof constructor ) ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-commiting-channel-or-sustain' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );
        m.oAssert( !this.currentExecution );
      }

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Captura e inicia execução da magia
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução da magia
      function * executeEffect() {
        // Identificadores
        var { owner, target } = spell,
            targetAction = m.GameAction.currents.find(
              action => action.committer == target && [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor )
            );

        // Lança parada de destino sobre negação do efeito da magia
        m.GameMatch.programFateBreak( owner.getRelationships().opponent, {
          name: `${ spell.name }-fate-break-${ spell.getMatchId() }`,
          playerBeing: target,
          opponentBeing: owner,
          isContestable: false,
          successAction: () => effects.currentExecution.next( true ),
          failureAction: () => effects.currentExecution.next( false ),
          modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
        } );

        // Aguarda resolução da parada de destino
        let wasUsedFatePoint = yield;

        // Caso parada de destino tenha sido mal-sucedida, impede ação do alvo da magia
        if( !wasUsedFatePoint ) targetAction.complete( 'prevent' );
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellPoke.gridOrder = m.data.cards.spells.push( SpellPoke );
}

/// Propriedades do protótipo
SpellPoke.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellPoke }
} );
