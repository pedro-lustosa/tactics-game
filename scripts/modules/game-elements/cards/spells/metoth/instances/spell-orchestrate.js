// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellOrchestrate = function ( config = {} ) {
  // Iniciação de propriedades
  SpellOrchestrate.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellOrchestrate.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellOrchestrate );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-orchestrate';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Spell;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';
  }

  // Ordem inicial das variações desta carta na grade
  SpellOrchestrate.gridOrder = m.data.cards.spells.push( SpellOrchestrate );
}

/// Propriedades do protótipo
SpellOrchestrate.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellOrchestrate }
} );
