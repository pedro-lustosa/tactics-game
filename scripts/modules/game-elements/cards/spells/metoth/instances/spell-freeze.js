// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellFreeze = function ( config = {} ) {
  // Iniciação de propriedades
  SpellFreeze.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellFreeze.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellFreeze );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-freeze';

    /// Indica se magia tem custo de persistência dinâmico
    spell.isDynamicPersistence = true;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Spell;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'persistent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 2;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var targetDuration = target.content.typeset.duration;

      // Invalida alvo caso ele não seja uma magia persistente ou permanente
      if( ![ 'persistent', 'permanent' ].includes( targetDuration ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-persistent-or-permanent-spell' ) );

      // Invalida alvo caso ele não esteja em uso
      if( !target.isInUse )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-in-use' ) );

      // Invalida alvo caso seu efeito não esteja ativo
      if( !target.content.effects.isEnabled )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-effect-must-be-enabled' ) );

      // Invalida alvo caso metade de sua potência seja maior que potência prevista para esta magia, ou a que ela poderia alcançar
      if( Math.round( target.content.stats.current.potency * .5 ) > spell.gaugePotency( action ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-must-be-able-to-have-no-less-potency-than-half-target' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var { target } = spell,
          targetHalfPotency = Math.round( target.content.stats.current.potency * .5 ),
          manaToAssign = 1;

      // Enquanto potência prevista do mana a ser atribuído for menor que metade da potência de alvo, incrementa em 1 o mana
      while( spell.gaugePotency( action, manaToAssign ) < targetHalfPotency ) manaToAssign++;

      // Define custo de persistência da magia como igual ao mana a ser atribuído
      spell.content.stats.current.manaCost.toPersist = manaToAssign;

      // Ajusta mana a ser atribuído para que ele não seja menor que quantidade mínima de mana a ser atribuído
      manaToAssign = Math.max( manaToAssign, action.getMinAssignableMana() );

      // Informa usuário sobre mana previsto para uso, e aguarda seu consentimento
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-spell-mana-cost', {
          manaPoints: manaToAssign,
          isEnergy: action.committer instanceof m.AstralBeing
        } ),
        acceptAction: () => action.currentExecution.next( manaToAssign ),
        declineAction: () => action.cancel()
      } ).replace();
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var { target } = spell;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Refaz eventuais disputas mágicas relativas a alvo, sempre desativando seu efeito
      target.handleSpellClash( true );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var { target } = spell;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Refaz eventuais disputas mágicas relativas a alvo, ativando seu efeito quando aplicável
      target.handleSpellClash();

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellFreeze.gridOrder = m.data.cards.spells.push( SpellFreeze );
}

/// Propriedades do protótipo
SpellFreeze.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellFreeze }
} );
