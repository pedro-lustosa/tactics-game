// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellExtend = function ( config = {} ) {
  // Iniciação de propriedades
  SpellExtend.init( this );

  // Superconstrutor
  m.Metoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellExtend.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Metoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellExtend );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-extend';

    /// Indica que magia é uma magia adjunta
    spell.isAdjunct = true;

    /// Valor a ser multiplicado pelo custo de mana da magia modificada, a fim de se determinar custo de mana da magia adjunta
    spell.adjunctMultiplier = .5;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Spell;

    /// Disponibilidade
    typeset.availability = 3;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G2';

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 1;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var { adjunctSpells } = action,
          ownerGarments = Object.values( owner.garments ?? {} );

      // Invalida dono caso uma magia de mesmo nome já esteja no arranjo de magias adjuntas da ação
      if( adjunctSpells.some( adjunctSpell => adjunctSpell.name == spell.name && adjunctSpell != spell ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'adjunct-spell-must-be-distinct' ) );

      // Invalida dono caso ele esteja vinculado ao traje 'Manto dos Arcanistas' e seu efeito esteja ativo
      if( ownerGarments.some( garment => garment.source instanceof m.GarmentRobeOfTheMagi && garment.source.content.effects.isEnabled ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'committer-must-not-be-with-robe-of-the-magi' ) );

      // Indica que dono é válido
      return true;
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.adjunctSpells.includes( spell ) );
      }

      // Identificadores
      var targetStats = target.content.stats.current,
          adjunctSpellIndex = action.adjunctSpells.indexOf( spell ),
          validationResult;

      // Caso alvo seja outra magia adjunta, valida-o em função da validade de sua canalização por acionante
      if( target.isAdjunct ) return action.validateSpell( target );

      // Invalida alvo caso ele não tenha custo de mana para canalização
      if( !targetStats.manaCost.toChannel.min )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-channeling-mana-cost' ) );

      // Invalida alvo caso seu alcance não esteja dentro das delimitações permitidas
      if( !/^R[1-4]$/.test( targetStats.range ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-have-r1-to-r4-range' ) );

      // Para caso magia adjunta não seja a primeira escolhida na cadeia de magias
      if( adjunctSpellIndex ) {
        // Captura resultado da magia adjunta anterior a esta
        validationResult = action.adjunctSpells[ adjunctSpellIndex - 1 ].content.effects.validateTarget( target, action );
      }

      // Do contrário
      else {
        // Aplica à magia alvo todos os ajustes devidos de modificadores
        action.adjunctSpells.forEach( adjunctSpell => adjunctSpell.content.effects.applyModifier?.( target ) );

        // Verifica se alvo é canalizável por acionante, e captura resultado da validação
        validationResult = action.validateSpell( target );

        // Desfaz da magia alvo todos os ajustes devidos de modificadores
        action.adjunctSpells.forEach( adjunctSpell => adjunctSpell.content.effects.dropModifier?.( target ) );
      }

      // Retorna resultado da validação de canalização de alvo
      return validationResult;
    }

    /// Aplica modificação relacionada à magia
    effects.applyModifier = function ( target ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof m.Spell );
        m.oAssert( !target.isAdjunct );
        m.oAssert( /^R[1-4]$/.test( target.content.stats.current.range ) );
      }

      // Identificadores
      var targetStats = target.content.stats.current;

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( target, { changeNumber: 1 } );

      // Aplica alteração à magia alvo
      targetStats.range = 'R' + ( Number( targetStats.range.replace( /\D/g, '' ) ) + 1 );

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( target, { changeNumber: 1 } );

      // Retorna alvo
      return target;
    }

    /// Desfaz modificação relacionada à magia
    effects.dropModifier = function ( target ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof m.Spell );
        m.oAssert( !target.isAdjunct );
        m.oAssert( /^R[2-5]$/.test( target.content.stats.current.range ) );
      }

      // Identificadores
      var targetStats = target.content.stats.current;

      // Sinaliza início da mudança de alcance
      m.events.cardContentStart.range.emit( target, { changeNumber: -1 } );

      // Desfaz alteração assumidamente aplicada à magia alvo
      targetStats.range = 'R' + ( Number( targetStats.range.replace( /\D/g, '' ) ) - 1 );

      // Sinaliza fim da mudança de alcance
      m.events.cardContentEnd.range.emit( target, { changeNumber: -1 } );

      // Retorna alvo
      return target;
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Ordem inicial das variações desta carta na grade
  SpellExtend.gridOrder = m.data.cards.spells.push( SpellExtend );
}

/// Propriedades do protótipo
SpellExtend.prototype = Object.create( m.Metoth.prototype, {
  // Construtor
  constructor: { value: SpellExtend }
} );
