// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheVision = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheVision.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheVision.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheVision );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-vision';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 2;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheVision.gridOrder = m.data.cards.spells.push( SpellTheVision );
}

/// Propriedades do protótipo
SpellTheVision.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheVision }
} );
