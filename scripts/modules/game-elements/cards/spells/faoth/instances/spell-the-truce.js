// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheTruce = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheTruce.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Indica se magia já foi usada na batalha atual
  SpellTheTruce.wasUsed = false;

  // Iniciação de propriedades
  SpellTheTruce.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheTruce );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-truce';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheLure;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function () {
      // Identificadores
      var troopsDifference = getTroopsDifference(),
          spellStats = spell.content.stats,
          minValue = 2,
          manaToReduce = Math.max( troopsDifference, 0 ) * 2,
          newManaCost = Math.max( spellStats.base.manaCost.toChannel.max - manaToReduce, minValue ),
          changeNumber = newManaCost - spellStats.current.manaCost.toChannel.max;

      // Não executa função caso não haja custo de mana a ser ajustado
      if( !changeNumber ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] += changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Funções

      /// Retorna diferença entre quantidade de tropas ativas dos jogadores
      function getTroopsDifference() {
        // Identificadores
        var spellOwner = spell.getRelationships().owner,
            spellOpponent = m.GameMatch.current.players[ spellOwner.parity == 'odd' ? 'even' : 'odd' ],
            troopsCount = {
              owner: spellOwner.gameDeck.cards.active.filter( card => card instanceof m.Humanoid ).length,
              opponent: spellOpponent.gameDeck.cards.active.filter( card => card instanceof m.Humanoid ).length
            };

        // Retorna diferença entre tropas do dono da magia e do oponente
        return troopsCount.owner - troopsCount.opponent;
      }
    }

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var matchFlow = m.GameMatch.current.flow;

      // Invalida dono caso magia já tenha sido usada na batalha
      if( SpellTheTruce.wasUsed )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-already-used-in-battle' ) );

      // Invalida dono caso turno da batalha atual não seja o 3 ou o 4
      if( !( matchFlow.turn instanceof m.BattleTurn ) || ![ 3, 4 ].includes( matchFlow.turn.counter ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-only-usable-in-middle-battle' ) );

      // Indica que dono é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores pós-validação
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica que magia já foi usada na batalha atual
      SpellTheTruce.wasUsed = true;

      // Prepara efeito a ocorrer no pós-combate
      postCombat.scheduleEffect.call( spell );
    }

    /// Aplica efeito da magia a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Programa execução do efeito para após execução do efeito de outras operações na paridade atual
      return setTimeout( () => ( effects.currentExecution = executeEffect() ).next() );

      // Funções

      /// Executa efeito da magia
      function * executeEffect() {
        // Caso haja paradas na tela, aguarda suas conclusões antes de iniciar execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Identificadores
        var { decks: matchDecks, environments: matchEnvironments, flow: matchFlow } = m.GameMatch.current,
            { field } = matchEnvironments,
            setupPhase = matchFlow.round.getChild( 'setup-phase' );

        // Registra uso desta magia
        effects.addLogEntry();

        // Finaliza fase da batalha
        matchFlow.phase.finish();

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Caso rodada não esteja mais ativa, não continua função
        if( !matchFlow.round.isActive ) return effects.currentExecution = null;

        // Suspende cartas no campo, no processo removendo as que forem fichas e limpando o mana de casas
        field.clear();

        // Captura entes e equipamentos suspensos
        let suspendedBeings = [ 'odd', 'even' ].flatMap( parity => matchDecks[ parity ].cards.beings.filter( being => being.activity == 'suspended' ) ),
            suspendedItems = [ 'odd', 'even' ].flatMap( parity => matchDecks[ parity ].cards.items.filter( item => item.activity == 'suspended' ) );

        // Itera por entes suspensos
        for( let being of suspendedBeings ) {
          // Restaura vida ou energia do ente alvo para o valor original
          being.content.stats.attributes.reset( being instanceof m.AstralBeing ? 'energy' : 'health' );

          // Caso ente tenha estatísticas de canalização, restaura o mana de suas sendas para o valor original
          being.content.stats.combativeness.channeling?.mana.reset();

          // Quando aplicável, restaura equipamentos embutidos removidos do ente
          being.resetEmbeddedItems?.();
        }

        // Itera por equipamentos suspensos
        for( let item of suspendedItems ) {
          // Filtra pingentes de oricalcos
          if( item instanceof m.ImplementOrichalcumPendant ) continue;

          // Quando equipamento puder se vincular a mana, restaura seu mana para o valor máximo
          item.attachments.mana = item.maxAttachments.mana;
        }

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => effects.currentExecution.next() ), { once: true } );

        // Limpa programação para avanço do fluxo, caso exista
        matchFlow.advanceTimeout &&= clearTimeout( matchFlow.advanceTimeout ) ?? null;

        // Reinicia fase do preparo, tomando como estágio inicial a etapa da mobilização
        setupPhase.begin( 1 );

        // Indica que fase atualmente ativa da rodada é a fase do preparo
        matchFlow.round.children.active = setupPhase;

        // Nulifica execução atual do efeito da magia
        effects.currentExecution = null;
      }
    }

    /// Adiciona registro sobre ativação do efeito da magia
    effects.addLogEntry = function () {
      // Identificadores
      var match = m.GameMatch.current,
          flowChain = match.flow.toChain(),
          logData = {
            name: spell.name,
            type: 'card-effect',
            description: m.languages.notices.getOutcome( `${ spell.name }-effect`, { card: spell } )
          };

      // Indica início do registro do evento
      m.events.logChangeStart.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );

      // Atualiza arranjo de eventos do estágio em que se estiver
      match.log.body[ flowChain ].events.push( logData );

      // Indica fim do registro do evento
      m.events.logChangeEnd.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Atualiza custos de mana de magias 'A Trégua'
  SpellTheTruce.updateManaCost = function ( eventData = {} ) {
    // Identificadores
    var { eventCategory, eventType, eventTarget: humanoid } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( humanoid instanceof m.Humanoid );
    }

    // Programa atualização do mana para ocorrer após o fim de todos os eventos acionados pela mudança de posicionamento do humanoide alvo
    setTimeout( function () {
      // Não executa função caso não haja uma partida em andamento
      if( !m.GameMatch.current ) return;

      // Identificadores
      var playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Itera por magias dos jogadores
      for( let spell of playerSpells ) {
        // Filtra magias que não sejam 'A Trégua'
        if( !( spell instanceof SpellTheTruce ) ) continue;

        // Atualiza custo de mana da magia alvo
        spell.content.effects.updateManaCost();
      }
    } );
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheTruce.gridOrder = m.data.cards.spells.push( SpellTheTruce );
}

/// Propriedades do protótipo
SpellTheTruce.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheTruce }
} );
