// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheHoard = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheHoard.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheHoard.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheHoard );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-hoard';

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var spellPlayer = spell.getRelationships().controller;

      // Adiciona evento para atualizar custos de mana da magia segundo variação de pontos de destino do jogador desta magia
      m.events.fatePointsChangeEnd.any.add( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var spellPlayer = spell.getRelationships().controller;

      // Remove evento para atualizar custos de mana da magia segundo variação de pontos de destino do jogador desta magia
      m.events.fatePointsChangeEnd.any.remove( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 16;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 16;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var playerFatePoints = spell.triggeringPlayer.fatePoints.current;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Adiciona aos pontos de destino transitivos do jogador seus pontos de destino intransitivos
      playerFatePoints.transitive += playerFatePoints.intransitive;

      // Zera pontos de destino intransitivos do jogador
      playerFatePoints.intransitive = 0;

      // Atualiza seção de pontos de destino na barra de informações do jogador alvo
      m.GameScreen.current.infoBar.updateFateData( { eventTarget: spell.triggeringPlayer } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: player } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'fate-points-change' && [ 'increase', 'decrease' ].includes( eventType ) && player == spell.getRelationships().owner );

      // Identificadores pós-validação
      var spellStats = spell.content.stats,
          manaToReduce = 2 * player.fatePoints.current.get(),
          minValue = 4,
          newManaCost = Math.max( spellStats.base.manaCost.toChannel.max - manaToReduce, minValue ),
          changeNumber = newManaCost - spellStats.current.manaCost.toChannel.max;

      // Não executa função caso não haja custo de mana a ser ajustado
      if( !changeNumber ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] += changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Valida conversão de pontos de destino para transitivos
  SpellTheHoard.validateFatePointConversion = function ( player ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( player instanceof m.GamePlayer );

    // Identificadores
    var playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Valida conversão de pontos de destino caso exista uma magia 'A Riqueza' ativa que afete o jogador passado
    if( playerSpells.some( card => card instanceof this && card.content.effects.isEnabled && card.triggeringPlayer == player ) ) return true;

    // Invalida conversão de pontos de destino
    return false;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheHoard.gridOrder = m.data.cards.spells.push( SpellTheHoard );
}

/// Propriedades do protótipo
SpellTheHoard.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheHoard }
} );
