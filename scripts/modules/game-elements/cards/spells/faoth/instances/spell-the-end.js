// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheEnd = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheEnd.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Indica que condição de vitória de pontos de destino deve ser verificada
  SpellTheEnd.isFateVictory = false;

  // Iniciação de propriedades
  SpellTheEnd.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheEnd );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-end';

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          battleTurn = matchFlow.round.getChild( 'battle-turn' );

      // Adiciona evento para reduzir custos de mana da magia ao início de cada turno da batalha
      m.events.flowChangeEnd.begin.add( battleTurn, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          battleTurn = matchFlow.round.getChild( 'battle-turn' );

      // Remove evento para reduzir custos de mana da magia ao início de cada turno da batalha
      m.events.flowChangeEnd.begin.remove( battleTurn, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 40;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 40;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Sinaliza que vitória de pontos de destino deve ser verificada
      SpellTheEnd.isFateVictory = true;

      // Programa encerramento do turno da batalha atual
      setTimeout( () => m.GameMatch.current.flow.turn.finish() );
    }

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget instanceof m.BattleTurn );

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          changeNumber = Math.min( manaCost.toChannel.min, 4 );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 100;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheEnd.gridOrder = m.data.cards.spells.push( SpellTheEnd );
}

/// Propriedades do protótipo
SpellTheEnd.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheEnd }
} );
