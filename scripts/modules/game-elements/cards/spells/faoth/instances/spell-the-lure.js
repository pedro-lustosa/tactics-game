// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheLure = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheLure.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo de jogadores que já usaram esta magia na batalha atual
  SpellTheLure.triggeringPlayers = [];

  // Iniciação de propriedades
  SpellTheLure.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheLure );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-lure';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheTruce;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'immediate';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 1;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Jogador a sofrer o efeito da magia
    effects.targetPlayer = null;

    /// Jogadas a serem geradas pelo efeito da magia
    effects.movesToAdd = 0;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function () {
      // Identificadores
      var troopsDifference = getTroopsDifference(),
          spellStats = spell.content.stats,
          minValue = 2,
          manaToReduce = Math.max( troopsDifference, 0 ) * 2,
          newManaCost = Math.max( spellStats.base.manaCost.toChannel.max - manaToReduce, minValue ),
          changeNumber = newManaCost - spellStats.current.manaCost.toChannel.max;

      // Não executa função caso não haja custo de mana a ser ajustado
      if( !changeNumber ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] += changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Funções

      /// Retorna diferença entre quantidade de tropas ativas dos jogadores
      function getTroopsDifference() {
        // Identificadores
        var spellOwner = spell.getRelationships().owner,
            spellOpponent = m.GameMatch.current.players[ spellOwner.parity == 'odd' ? 'even' : 'odd' ],
            troopsCount = {
              owner: spellOwner.gameDeck.cards.active.filter( card => card instanceof m.Humanoid ).length,
              opponent: spellOpponent.gameDeck.cards.active.filter( card => card instanceof m.Humanoid ).length
            };

        // Retorna diferença entre tropas do oponente e do dono da magia
        return troopsCount.opponent - troopsCount.owner;
      }
    }

    /// Valida o dono da magia
    effects.validateOwner = function ( owner, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( owner instanceof m.Being );
        m.oAssert( owner.content.stats.combativeness?.channeling );
        m.oAssert( !spell.owner || spell.owner == owner );
        m.oAssert( action instanceof m.ActionChannel );
      }

      // Identificadores
      var ownerController = owner.getRelationships().controller;

      // Invalida dono caso seu controlador já tenha usado a magia na batalha
      if( SpellTheLure.triggeringPlayers.some( usageData => usageData.player == ownerController ) )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'spell-already-used-by-you-in-battle' ) );

      // Indica que dono é válido
      return true;
    }

    /// Provoca efeito relativo ao uso da magia
    effects.useEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && eventType == 'use-in' && eventTarget == spell );

      // Caso efeito da magia esteja inativo, não executa função
      if( !this.isEnabled ) return;

      // Identificadores pós-validação
      var matchFlow = m.GameMatch.current.flow,
          postCombat = matchFlow.turn.getChild( 'post-combat-break-period' ),
          redeploymentPeriod = matchFlow.turn.getChild( 'redeployment-period' );

      // Adiciona jogador que usou magia ao arranjo de jogadores que a usaram
      SpellTheLure.triggeringPlayers.push( { player: spell.triggeringPlayer, wasOnThisTurn: true } );

      // Captura jogador a sofrer o efeito da magia
      this.targetPlayer = spell.triggeringPlayer;

      // Prepara efeito a ocorrer no pós-combate
      postCombat.scheduleEffect.call( spell );

      // Adiciona evento para aplicação do efeito a ocorrer na remobilização
      m.events.flowChangeEnd.begin.add( redeploymentPeriod, this.applyRedeploymentPeriodEffect, { context: this } );

      // Prepara evento para limpar dados da magia ao fim do turno atual
      m.events.flowChangeEnd.finish.add( matchFlow.turn, this.clearData, { context: this, once: true } );
    }

    /// Aplica efeito da magia a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Identificadores
      var { targetPlayer } = this,
          playerCommander = targetPlayer.gameDeck.cards.commander,
          { field } = m.GameMatch.current.environments,
          targetEngagementZones = field.divider.slots.filter( slot => slot.getController() != targetPlayer ),
          targetBeings = targetEngagementZones.flatMap( slot => slot.cards[ targetPlayer.parity ].filter( card =>
            card instanceof m.Humanoid && playerCommander != card
          ) );

      // Caso comandante do jogador alvo não esteja ativo, remove do arranjo de entes alvo eventuais entes vinculados a "O Regente"
      if( playerCommander.activity != 'active' )
        targetBeings = targetBeings.filter( being =>
          !being.getAllCardAttachments().some( card => card instanceof m.SpellTheRuler && card.content.effects.isEnabled )
        );

      // Define contador de jogadas a serem geradas
      this.movesToAdd = targetBeings.length;

      // Registra uso desta magia
      this.addLogEntry( targetBeings );

      // Suspende entes alvo
      targetBeings.forEach( being => being.inactivate( 'suspended', { triggeringCard: spell } ) );
    }

    /// Aplica efeito da magia a ser resolvido na remobilização
    effects.applyRedeploymentPeriodEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: redeploymentPeriod } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && redeploymentPeriod instanceof m.RedeploymentPeriod );

      // Identificadores pós-validação
      var { targetPlayer } = this,
          targetParity = redeploymentPeriod.getChild( `${ targetPlayer.parity }-parity` );

      // Gera novas jogadas, e as adiciona à paridade do jogador pertinente
      for( let i = 0; i < this.movesToAdd; i++ ) new m.FlowMove( { parent: targetParity, source: targetPlayer } ).add();
    }

    /// Adiciona registro sobre ativação do efeito da magia
    effects.addLogEntry = function ( targetBeings ) {
      // Identificadores
      var match = m.GameMatch.current,
          flowChain = match.flow.toChain(),
          logData = {
            name: spell.name,
            type: 'card-effect',
            description: m.languages.notices.getOutcome( `${ spell.name }-effect`, { card: spell, beings: targetBeings } )
          };

      // Indica início do registro do evento
      m.events.logChangeStart.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );

      // Atualiza arranjo de eventos do estágio em que se estiver
      match.log.body[ flowChain ].events.push( logData );

      // Indica fim do registro do evento
      m.events.logChangeEnd.insert.emit( match, { event: spell, data: logData, flowChain: flowChain } );
    }

    /// Limpa dados do efeito da magia
    effects.clearData = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget instanceof m.BattleTurn );

      // Identificadores pós-validação
      var { targetPlayer } = this,
          redeploymentPeriod = m.GameMatch.current.flow.round.getChild( 'redeployment-period' );

      // Nulifica jogador a sofrer o efeito da magia
      this.targetPlayer = null;

      // Zera jogadas a serem geradas pelo efeito da magia
      this.movesToAdd = 0;

      // Indica que, para o jogador alvo, o efeito desta magia não foi mais usado no turno atual
      SpellTheLure.triggeringPlayers.find( usageData => usageData.player == targetPlayer ).wasOnThisTurn = false;

      // Remove evento para aplicar efeito da magia relativo à remobilização
      m.events.flowChangeEnd.begin.remove( redeploymentPeriod, this.applyRedeploymentPeriodEffect, { context: this } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 40;
  }

  // Atualiza custos de mana de magias 'O Engodo'
  SpellTheLure.updateManaCost = function ( eventData = {} ) {
    // Identificadores
    var { eventCategory, eventType, eventTarget: humanoid } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'field-slot-card-change' );
      m.oAssert( humanoid instanceof m.Humanoid );
    }

    // Programa atualização do mana para ocorrer após o fim de todos os eventos acionados pela mudança de posicionamento do humanoide alvo
    setTimeout( function () {
      // Não executa função caso não haja uma partida em andamento
      if( !m.GameMatch.current ) return;

      // Identificadores
      var playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Itera por magias dos jogadores
      for( let spell of playerSpells ) {
        // Filtra magias que não sejam 'O Engodo'
        if( !( spell instanceof SpellTheLure ) ) continue;

        // Atualiza custo de mana da magia alvo
        spell.content.effects.updateManaCost();
      }
    } );
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheLure.gridOrder = m.data.cards.spells.push( SpellTheLure );
}

/// Propriedades do protótipo
SpellTheLure.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheLure }
} );
