// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheChampion = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheChampion.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo que aponta jogadores que devem perder na rodada atual
  SpellTheChampion.playersToLose = [];

  // Iniciação de propriedades
  SpellTheChampion.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheChampion );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-champion';

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          battleTurn = matchFlow.round.getChild( 'battle-turn' );

      // Adiciona evento para aumentar custos de mana da magia ao fim de cada turno da batalha
      m.events.flowChangeEnd.finish.add( battleTurn, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          battleTurn = matchFlow.round.getChild( 'battle-turn' );

      // Remove evento para aumentar custos de mana da magia ao fim de cada turno da batalha
      m.events.flowChangeEnd.finish.remove( battleTurn, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 2;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 2;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget instanceof m.BattleTurn );

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          maxValue = 8,
          changeNumber = Math.min( maxValue - manaCost.toChannel.min, 2 );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Aumenta custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] += changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var committerController = action.committer.getRelationships().controller,
          targetOwner = target.getRelationships().owner;

      // Invalida alvo caso seu dono seja diferente do controlador de acionante
      if( committerController != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-player' ) );

      // Invalida alvo caso ele seja um comandante
      if( target.content.stats.command )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );

      // Indica que alvo é válido
      return true;
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var being = spell.target;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Adiciona aos pontos de destino embutidos do alvo fonte de pontos desta magia
      being.content.stats.fatePoints.add( { name: spell.name, baseValue: Infinity, currentValue: Infinity } );

      // Adiciona evento para programar término da partida com a remoção do alvo
      m.events.cardActivityStart.removed.add( being, this.addToPlayersToLose, { context: this } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var being = spell.target;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Remove dos pontos de destino embutidos do alvo fonte de pontos desta magia
      being.content.stats.fatePoints.remove( spell.name );

      // Remove evento para programar término da partida com a remoção do alvo
      m.events.cardActivityStart.removed.remove( being, this.addToPlayersToLose, { context: this } );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Adiciona jogador que ativou esta magia ao arranjo de jogadores que devem perder a partida
    effects.addToPlayersToLose = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'card-activity' && [ 'withdrawn', 'ended', 'unlinked' ].includes( eventType ) && eventTarget == spell.target );

      // Quando ainda não estiver lá, adiciona jogador que usou a magia ao arranjo de jogadores que devem perder a rodada
      if( !SpellTheChampion.playersToLose.includes( spell.triggeringPlayer ) ) SpellTheChampion.playersToLose.push( spell.triggeringPlayer );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheChampion.gridOrder = m.data.cards.spells.push( SpellTheChampion );
}

/// Propriedades do protótipo
SpellTheChampion.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheChampion }
} );
