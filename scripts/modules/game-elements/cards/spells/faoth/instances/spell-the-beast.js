// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheBeast = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheBeast.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheBeast.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheBeast );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-beast';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheFeast;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Execução atual do efeito da magia
    effects.currentExecution = null;

    /// Alvo do efeito da parada de destino da magia
    effects.fateBreakTarget = null;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function () {
      // Identificadores
      var { manaCost } = spell.content.stats.current,
          minValue = 2,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, 2 );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Adiciona evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.add( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.remove( postCombat, postCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pós-combate
    effects.applyPostCombatEffect = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var fateBreakName = `${ spell.name }-fate-break-${ spell.getMatchId() }`,
          { triggeringPlayer } = spell;

      // Lança parada de destino sobre afastamento de uma criatura biótica
      return m.GameMatch.programFateBreak( triggeringPlayer, {
        name: fateBreakName,
        playerBeing: null,
        opponentBeing: null,
        isContestable: null,
        middleAction: () => ( effects.currentExecution = chooseTarget() ).next(),
        successAction: withdrawTarget.bind( this ),
        failureAction: clearData.bind( this ),
        modalArguments: [ `fate-break-${ spell.name }`, { spell: spell } ]
      } );

      // Funções

      /// Escolhe via a parada de destino um alvo a ser afastado
      async function * chooseTarget() {
        // Identificadores pré-validação
        var { fateBreakExecution } = m.GameMatch;

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( fateBreakExecution?.name == fateBreakName );

        // Identificadores pós-validação
        var playerCards = m.GameMatch.current.decks.getAllCards(),
            isDecidingPlayer = m.GameMatch.current.isSimulation || m.GamePlayer.current == triggeringPlayer,
            choiceComponent, targetBeing;

        // Para caso jogador seja o a escolher o alvo
        if( isDecidingPlayer ) {
          // Adiciona às cartas dos jogadores evento para validar o alvo
          for( let card of playerCards ) card.addListener( 'dblclick', effects.selectFateBreakTarget );

          // Avisa jogador sobre escolha do alvo a ser afastado
          choiceComponent = new m.StaticBar( {
            text: m.languages.notices.getChoiceText( 'choose-target-being' ),
            isRemovable: true,
            removeHoverText: m.languages.snippets.getUserInterfaceAction( 'cancel' ),
            removeAction: () => effects.currentExecution.next( null )
          } ).insert();

          // Aguarda escolha do alvo, e o captura
          targetBeing = yield;

          // Remove das cartas dos jogadores evento para validar o alvo
          for( let card of playerCards ) card.removeListener( 'dblclick', effects.selectFateBreakTarget );

          // Caso partida não seja uma simulação, propaga decisão sobre escolha do alvo
          if( !m.GameMatch.current.isSimulation ) {
            m.sockets.current.emit( 'progress-effect', {
              matchName: m.GameMatch.current.name,
              data: {
                cardId: spell.getMatchId(),
                value: playerCards.indexOf( targetBeing )
              }
            } );
          }
        }

        // Do contrário
        else {
          // Avisa usuário sobre escolha do alvo a ser afastado
          choiceComponent = new m.StaticBar( {
            text: m.languages.notices.getChoiceText( 'choosing-fate-break-target', { playerParity: triggeringPlayer.parity } )
          } ).insert();

          // Aguarda escolha do alvo, e o captura
          targetBeing = playerCards[ yield ];
        }

        // Remove barra sobre escolha do alvo
        choiceComponent.remove();

        // Para caso um alvo não tenha sido escolhido
        if( !targetBeing ) {
          // Caso usuário não seja o jogador que cancelou escolha do alvo, notifica-o sobre esse cancelamento
          if( !isDecidingPlayer )
            m.noticeBar.show(
              m.languages.notices.getChoiceText( 'canceling-fate-break-target', { playerParity: triggeringPlayer.parity } ),
              m.GamePlayer.current ? 'green' : 'red'
            );

          // Indica que parada de destino deve ser cancelada
          fateBreakExecution.isToSkip = true;
        }

        // Do contrário
        else {
          // Notifica que ente alvo foi escolhido para ser removido
          choiceComponent = new m.StaticBar( {
            text: m.languages.notices.getChoiceText( 'chose-fate-break-target', { target: targetBeing } )
          } ).insert();

          // Aguarda animação de destaque do alvo
          await m.animations.highlightCard( targetBeing, { color: 'lightYellow' } );

          // Remove barra sobre notificação do alvo
          choiceComponent.remove();

          // Captura alvo da parada de destino, e o define como ente do oponente na parada de destino
          fateBreakExecution.opponentBeing = effects.fateBreakTarget = targetBeing;

          // Define contestabilidade da parada de destino em função de se alvo escolhido é um humanoide
          fateBreakExecution.isContestable = targetBeing instanceof m.Humanoid;
        }

        // Progride parada de destino
        fateBreakExecution.next();
      }

      /// Afasta alvo escolhido via a parada de destino
      function withdrawTarget() {
        // Afasta alvo escolhido para a parada de destino
        effects.fateBreakTarget.inactivate( 'withdrawn', { triggeringCard: spell } );

        // Limpa dados relativos à parada de destino
        clearData();
      }

      /// Limpa dados relativos à parada de destino
      function clearData() {
        // Nulifica execução atual do efeito da magia
        effects.currentExecution = null;

        // Nulifica alvo escolhido da parada de destino
        effects.fateBreakTarget = null;
      }
    }

    /// Seleciona o alvo da parada de destino
    effects.selectFateBreakTarget = function ( eventData ) {
      // Identificadores
      var { currentTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( effects.currentExecution );

      // Valida o alvo, e captura resultado da validação
      let validationResult = validateTarget();

      // Emite som de desfecho da validação
      m.assets.audios.soundEffects[ validationResult ? 'click-next' : 'click-deny' ].play();

      // Caso validação do alvo tenha sido bem-sucedida, continua execução do efeito da magia
      if( validationResult ) return effects.currentExecution.next( target );

      // Funções

      /// Valida alvo selecionado
      function validateTarget() {
        // Invalida alvo caso ele não seja um ente biótico
        if( !( target instanceof m.BioticBeing ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-biotic-being' ) );

        // Invalida alvo caso ele seja um comandante
        if( target.content.stats.command )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );

        // Invalida alvo caso ele não esteja ativo ou suspenso
        if( ![ 'active', 'suspended' ].includes( target.activity ) )
          return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-active-or-suspended' ) );

        // Indica que alvo é válido
        return true;
      }
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Reduz custos de mana de magias 'A Fera'
  SpellTheBeast.reduceManaCost = function ( eventData = {} ) {
    // Não executa função caso não se esteja na fase da batalha
    if( !( m.GameMatch.current?.flow.phase instanceof m.BattlePhase ) ) return;

    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: humanoid, formerActivity } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-activity' && [ 'withdrawn', 'ended', 'unlinked' ].includes( eventType ) );
      m.oAssert( humanoid instanceof m.Humanoid );
    }

    // Não executa função caso atividade antiga do ente não tenha sido ativa ou suspensa
    if( ![ 'active', 'suspended' ].includes( formerActivity ) ) return;

    // Identificadores pós-validação
    var humanoidOwner = humanoid.getRelationships().owner,
        humanoidOpponent = m.GameMatch.current.players[ humanoidOwner.parity == 'odd' ? 'even' : 'odd' ],
        opponentSpells = humanoidOpponent.gameDeck.cards.spells;

    // Itera por magias do oponente do humanoide removido
    for( let spell of opponentSpells ) {
      // Filtra magias que não sejam 'A Fera'
      if( !( spell instanceof this ) ) continue;

      // Atualiza custo de mana da magia alvo
      spell.content.effects.updateManaCost();
    }
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheBeast.gridOrder = m.data.cards.spells.push( SpellTheBeast );
}

/// Propriedades do protótipo
SpellTheBeast.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheBeast }
} );
