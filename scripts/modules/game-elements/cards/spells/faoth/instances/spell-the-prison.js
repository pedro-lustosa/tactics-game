// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellThePrison = function ( config = {} ) {
  // Iniciação de propriedades
  SpellThePrison.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellThePrison.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellThePrison );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-prison';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheCastle;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 20;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellThePrison.gridOrder = m.data.cards.spells.push( SpellThePrison );
}

/// Propriedades do protótipo
SpellThePrison.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellThePrison }
} );
