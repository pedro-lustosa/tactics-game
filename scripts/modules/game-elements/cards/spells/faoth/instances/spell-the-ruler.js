// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheRuler = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheRuler.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Arranjo para registrar humanoides ativos ao longo de um combate
  SpellTheRuler.combatActiveHumanoids = [];

  // Arranjo que aponta jogadores que não devem perder caso comandante seja inativado
  SpellTheRuler.playersWithRuler = [];

  // Iniciação de propriedades
  SpellTheRuler.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheRuler );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-ruler';

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' );

      // Adiciona evento para reduzir custo de mana mínimo da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.add( combatPeriod, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' );

      // Remove evento para reduzir custo de mana mínimo da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.remove( combatPeriod, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = m.Humanoid;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G1';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 3;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 8;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 8;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget instanceof m.CombatPeriod );

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          minValue = 2,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, 2 );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel.min', changeNumber: -changeNumber } );

      // Reduz custo de mana mínimo da magia
      manaCost.toChannel.min -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel.min', changeNumber: -changeNumber } );
    }

    /// Define quantidade mínima de mana para canalização da magia
    effects.getMinMana = function ( target = spell.target ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( target == spell.target || !spell.target );
      }

      // Define quantidade mínima de mana enquanto o maior valor entre 2 e o custo de mana máximo menos duas vezes a quantidade de combates em que o alvo esteve sempre ativo
      return Math.max( 2, spell.content.stats.current.manaCost.toChannel.max - target.activeCombatCount * 2 );
    }

    /// Valida o alvo da magia
    effects.validateTarget = function ( target, action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( target instanceof typeset.attachability );
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var { manaCost } = spell.content.stats.current,
          committerController = action.committer.getRelationships().controller,
          targetOwner = target.getRelationships().owner,
          [ formerIsQuiet, formerMinMana ] = [ m.app.isQuiet, manaCost.toChannel.min ];

      // Invalida alvo caso seu dono seja diferente do controlador de acionante
      if( committerController != targetOwner )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-be-from-player' ) );

      // Invalida alvo caso ele seja um comandante
      if( target.content.stats.command )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'target-must-not-be-commander' ) );

      // Valida alvo caso já haja um mana a ser atribuído
      if( action.manaToAssign ) return true;

      // Força para ativada configuração 'isQuiet'
      m.app.isQuiet = true;

      // Ajusta provisoriamente custo de mana mínimo da magia
      let adjustedMinMana = manaCost.toChannel.min = this.getMinMana( target );

      // Captura resultado da validação do custo de mana
      let validationResult = m.Spell.validateManaCost( action );

      // Restaura custo de mana mínimo da magia para valor original
      manaCost.toChannel.min = formerMinMana;

      // Restaura configuração 'isQuiet' para seu valor original
      m.app.isQuiet = formerIsQuiet;

      // Invalida alvo caso acionante não possa pagar o custo mínimo de mana previsto tendo-o como referência
      if( !validationResult )
        return m.noticeBar.show( m.languages.notices.getInvalidationText( 'not-enough-mana-for-given-target', { manaPoints: adjustedMinMana } ) );

      // Indica que alvo é válido
      return true;
    }

    /// Define o mana a ser usado pela magia
    effects.setManaToChannel = function ( action ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionChannel );
        m.oAssert( action.target == spell );
      }

      // Identificadores
      var manaToAssign = this.getMinMana();

      // Ajusta mana a ser atribuído para que ele não seja menor que quantidade mínima de mana a ser atribuído
      manaToAssign = Math.max( manaToAssign, action.getMinAssignableMana() );

      // Informa usuário sobre mana previsto para uso, e aguarda seu consentimento
      return new m.ConfirmationModal( {
        text: m.languages.notices.getModalText( 'confirm-spell-mana-cost', {
          manaPoints: manaToAssign,
          isEnergy: action.committer instanceof m.AstralBeing
        } ),
        acceptAction: () => action.currentExecution.next( manaToAssign ),
        declineAction: () => action.cancel()
      } ).replace();
    }

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var humanoid = spell.target,
          spellPlayer = spell.triggeringPlayer,
          playerCommander = spellPlayer.gameDeck.cards.commander;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Para caso humanoide esteja ativo
      if( humanoid.activity == 'active' ) {
        // Adiciona jogador do humanoide ao arranjo de jogadores que não devem perder com a inativação do comandante
        SpellTheRuler.playersWithRuler.push( spellPlayer );

        // Re-executa controle de ações do comandante
        m.GameAction.refreshActions( playerCommander );
      }

      // Para caso comandante do jogador não esteja ativo
      if( playerCommander.activity != 'active' ) {
        // Se aplicável, revalida acionamento de ação 'recuar' de alvo, de modo que ela se torne indisponível
        if( humanoid.actions.some( action => action instanceof m.ActionRetreat ) ) m.ActionRetreat.controlAvailability( { eventTarget: humanoid } );

        // Quando existente, impede ação 'recuar' de alvo
        m.GameAction.currents.find( action => action.committer == humanoid && action instanceof m.ActionRetreat )?.complete( 'prevent' );
      }

      // Adiciona evento para controlar presença do jogador do humanoide no arranjo de jogadores que não devem perder com a inativação do comandante
      m.events.cardActivityStart.any.add( humanoid, this.updatePlayersWithRuler, { context: this } );

      // Adiciona eventos para controlar habilitação da ação 'Recuar' do humanoide e do comandante segundo atividade do outro
      for( let being of [ playerCommander, humanoid ] ) m.events.cardActivityEnd.any.add( being, this.updateRetreatAvailability, { context: this } );

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var humanoid = spell.target,
          spellPlayer = spell.triggeringPlayer,
          playerCommander = spellPlayer.gameDeck.cards.commander;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Caso jogador do humanoide esteja ativo e no arranjo de jogadores que não devem perder com a inativação do comandante, retira-o de lá
      if( humanoid.activity == 'active' && SpellTheRuler.playersWithRuler.includes( spellPlayer ) )
        SpellTheRuler.playersWithRuler.splice( SpellTheRuler.playersWithRuler.indexOf( spellPlayer ), 1 );

      // Remove evento para controlar presença do jogador do humanoide no arranjo de jogadores que não devem perder com a inativação do comandante
      m.events.cardActivityStart.any.remove( humanoid, this.updatePlayersWithRuler, { context: this } );

      // Itera por comandante e por alvo da magia
      for( let being of [ playerCommander, humanoid ] ) {
        // Remove evento para controlar habilitação da ação 'Recuar' do ente alvo
        m.events.cardActivityEnd.any.remove( being, this.updateRetreatAvailability, { context: this } );

        // Re-executa controle de ações do ente alvo
        m.GameAction.refreshActions( being );
      }

      // Caso jogador alvo não esteja mais no arranjo de jogadores afetados por esta magia, impede eventual ação recuar em andamento do comandante
      if( !SpellTheRuler.playersWithRuler.includes( spellPlayer ) )
        m.GameAction.currents.find( action => action.committer == playerCommander && action instanceof m.ActionRetreat )?.complete( 'prevent' );

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Atualiza arranjo de jogadores que não devem perder com a inativação do comandante
    effects.updatePlayersWithRuler = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData,
          spellPlayer = spell.triggeringPlayer;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-activity' && eventTarget == spell.target );
        m.oAssert( eventType == 'active' || SpellTheRuler.playersWithRuler.includes( spellPlayer ) );
      }

      // Caso humanoide tenha sido ativado, adiciona-o ao arranjo; do contrário, retira-o de lá
      eventType == 'active' ? SpellTheRuler.playersWithRuler.push( spellPlayer ) :
                              SpellTheRuler.playersWithRuler.splice( SpellTheRuler.playersWithRuler.indexOf( spellPlayer ), 1 );
    }

    /// Atualiza habilitação da ação 'Recuar' do alvo da magia ou de seu comandante segundo a atividade do outro
    effects.updateRetreatAvailability = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: being } = eventData,
          spellPlayer = being.getRelationships().owner,
          playerCommander = spellPlayer.gameDeck.cards.commander,
          isCommander = being == playerCommander;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-activity' );

      // Re-executa controle de ações do alvo
      m.GameAction.refreshActions( isCommander ? spell.target : playerCommander );

      // Caso comandante tenha sido inativado, impede eventual ação recuar em andamento do alvo da magia
      if( isCommander && eventType != 'active' )
        m.GameAction.currents.find( action => action.committer == spell.target && action instanceof m.ActionRetreat )?.complete( 'prevent' );

      // Caso jogador alvo não esteja mais no arranjo de jogadores afetados por esta magia, impede eventual ação recuar em andamento do comandante
      if( !SpellTheRuler.playersWithRuler.includes( spellPlayer ) )
        m.GameAction.currents.find( action => action.committer == playerCommander && action instanceof m.ActionRetreat )?.complete( 'prevent' );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Controla registro de humanoides ativos ao longo de um combate
  SpellTheRuler.controlActiveHumanoidsRecord = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: stage } = eventData;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && [ 'begin', 'finish' ].includes( eventType ) && stage instanceof m.CombatPeriod );

    // Identificadores pós-validação
    var { combatActiveHumanoids } = this;

    // Executa operação em função de se período do combate começou ou se encerrou
    return eventType == 'begin' ? recordActiveHumanoids.call( this ) : increaseActiveCount.call( this );

    // Funções

    /// Registra humanoides ativos
    function recordActiveHumanoids() {
      // Identificadores
      var playerBeings = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.beings );

      // Itera por entes da partida
      for( let being of playerBeings ) {
        // Filtra entes que não sejam humanoides ativos
        if( !( being instanceof m.Humanoid ) || being.activity != 'active' ) continue;

        // Adiciona ente ao arranjo de humanoides ativos
        combatActiveHumanoids.push( being );

        // Adiciona evento para remover humanoide do arranjo de humanoides ativos caso sua atividade seja alterada
        m.events.cardActivityEnd.any.add( being, this.removeHumanoidRecord, { context: this, once: true } );
      }
    }

    /// Incrementa contagem de combates durante os que um humanoide esteve sempre ativo
    function increaseActiveCount() {
      // Para enquanto houverem humanoides no registro de humanoides ativos
      while( combatActiveHumanoids.length ) {
        // Identificadores
        let humanoid = combatActiveHumanoids[ 0 ];

        // Incrementa contagem de combates durante os que o humanoide esteve sempre ativo
        humanoid.activeCombatCount++;

        // Remove evento para remover humanoide do arranjo de humanoides ativos caso sua atividade seja alterada
        m.events.cardActivityEnd.any.remove( humanoid, this.removeHumanoidRecord, { context: this } );

        // Retira humanoide atual do registro de humanoides ativos
        combatActiveHumanoids.shift();
      }
    }
  }

  // Remove do arranjo de humanoides ativos registro do humanoide passado
  SpellTheRuler.removeHumanoidRecord = function ( eventData = {} ) {
    // Identificadores
    var { eventTarget: humanoid } = eventData,
        humanoidIndex = this.combatActiveHumanoids.indexOf( humanoid );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( humanoid instanceof m.Humanoid && humanoidIndex != -1 );

    // Remove humanoide passado do arranjo de humanoides ativos
    this.combatActiveHumanoids.splice( humanoidIndex, 1 );
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheRuler.gridOrder = m.data.cards.spells.push( SpellTheRuler );
}

/// Propriedades do protótipo
SpellTheRuler.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheRuler }
} );
