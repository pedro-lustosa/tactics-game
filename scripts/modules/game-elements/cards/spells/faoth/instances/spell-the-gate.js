// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheGate = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheGate.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheGate.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheGate );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-gate';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 20;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 20;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheGate.gridOrder = m.data.cards.spells.push( SpellTheGate );
}

/// Propriedades do protótipo
SpellTheGate.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheGate }
} );
