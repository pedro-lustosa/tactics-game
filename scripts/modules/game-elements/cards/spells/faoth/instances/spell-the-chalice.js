// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheChalice = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheChalice.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheChalice.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheChalice );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-chalice';

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 5;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 25;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 25;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( manaSpent ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( manaSpent ) && manaSpent >= 0 );

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          minValue = 3,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, Math.floor( manaSpent * .5 ) );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Reduz custos de mana de magias 'O Cálice'
  SpellTheChalice.reduceManaCost = function ( eventData = {} ) {
    // Não executa função caso não se esteja no período do combate
    if( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) ) return;

    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: spell, action } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-use' && [ 'use-in', 'sustain' ].includes( eventType ) );
      m.oAssert( spell instanceof m.Spell && [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
    }

    // Identificadores pós-validação
    var manaSpent = ( spell.isToSkipManaSpending ? 0 : action.manaToAssign ) + ( action.adjunctManaToAssign ?? 0 ),
        playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Invalida redução de mana caso menos de 2 pontos de mana tenham sido gastos
    if( manaSpent < 2 ) return;

    // Itera por magias de jogadores
    for( let spell of playerSpells ) {
      // Filtra magias que não sejam 'O Cálice'
      if( !( spell instanceof this ) ) continue;

      // Atualiza custo de mana da magia alvo
      spell.content.effects.updateManaCost( manaSpent );
    }
  }

  // Verifica ganho de pontos de destino
  SpellTheChalice.checkFatePointGain = function ( eventData = {} ) {
    // Não executa função caso não se esteja no período do combate
    if( !( m.GameMatch.current?.flow.period instanceof m.CombatPeriod ) ) return;

    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget: spell, action } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'card-use' && [ 'use-in', 'sustain' ].includes( eventType ) );
      m.oAssert( spell instanceof m.Spell && [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );
    }

    // Identificadores pós-validação
    var spellController = spell.getRelationships().controller,
        manaSpent = ( spell.isToSkipManaSpending ? 0 : action.manaToAssign ) + ( action.adjunctManaToAssign ?? 0 ),
        gainedFatePoints = Math.floor( manaSpent * .5 ),
        playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Não executa função caso não haja pontos de destino a serem ganhos
    if( !gainedFatePoints ) return;

    // Invalida ganho de pontos de destino caso não exista uma magia 'O Cálice' ativa que seja do jogador que canalizou a magia passada
    if( !playerSpells.some( card => card instanceof this && card.content.effects.isEnabled && card.triggeringPlayer == spellController ) ) return;

    // Confere ao jogador alvo pontos de destino intransitivos
    spellController.fatePoints.increase( gainedFatePoints );
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheChalice.gridOrder = m.data.cards.spells.push( SpellTheChalice );
}

/// Propriedades do protótipo
SpellTheChalice.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheChalice }
} );
