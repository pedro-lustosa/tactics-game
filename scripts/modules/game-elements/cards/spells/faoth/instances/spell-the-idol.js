// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheIdol = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheIdol.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheIdol.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheIdol );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-idol';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheHost;

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 16;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 16;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheIdol.gridOrder = m.data.cards.spells.push( SpellTheIdol );
}

/// Propriedades do protótipo
SpellTheIdol.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheIdol }
} );
