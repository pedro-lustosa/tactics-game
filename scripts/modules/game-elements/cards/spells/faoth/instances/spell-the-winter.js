// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheWinter = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheWinter.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheWinter.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheWinter );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-winter';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheSpring;

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' ),
          spellPlayer = spell.getRelationships().opponent;

      // Adiciona evento para reduzir custos de mana da magia segundo o uso de pontos de destino do oponente do jogador desta magia
      m.events.fatePointsChangeEnd.decrease.add( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );

      // Adiciona evento para redefinir custos de mana da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.add( combatPeriod, spell.content.effects.resetManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' ),
          spellPlayer = spell.getRelationships().opponent;

      // Remove evento para reduzir custos de mana da magia segundo o uso de pontos de destino do oponente do jogador desta magia
      m.events.fatePointsChangeEnd.decrease.remove( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );

      // Remove evento para redefinir custos de mana da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.remove( combatPeriod, spell.content.effects.resetManaCost, { context: spell.content.effects } );
    }

    /// Adiciona magias adicionas para disputas mágicas
    spell.addClashingSpells = function () {
      // Identificadores
      var { triggeringPlayer } = this,
          clashingSpells = [],
          playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Adiciona ao arranjo de magias disputantes magias 'Santuário' que mirem a mesma grade desta magia
      clashingSpells = clashingSpells.concat( playerSpells.filter(
        card => card instanceof m.SpellTheSpring && card.isInUse && card.triggeringPlayer != triggeringPlayer
      ) );

      // Retorna magias disputantes
      return clashingSpells;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'negative';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 18;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 18;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Não executa função caso não se esteja no período do combate
      if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: player, changeNumber: decreasedFatePoints } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'fate-points-change' && eventType == 'decrease' && player != spell.getRelationships().owner );
        m.oAssert( Number.isInteger( decreasedFatePoints ) && decreasedFatePoints < 0 );
      }

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          minValue = 3,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, Math.abs( 3 * decreasedFatePoints ) );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    /// Redefine custos de mana desta magia para seus valores originais
    effects.resetManaCost = function () {
      // Identificadores
      var spellStats = spell.content.stats,
          changeNumber = spellStats.base.manaCost.toChannel.min - spellStats.current.manaCost.toChannel.min;

      // Não executa função caso não haja custo de mana a ser ajustado
      if( !changeNumber ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Redefine custos de mana da magia
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] = spellStats.base.manaCost.toChannel[ key ];

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Valida ganho de pontos de destino
  SpellTheWinter.validateFatePointGain = function ( player ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( player instanceof m.GamePlayer );

    // Identificadores
    var playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Invalida ganho de pontos de destino caso exista uma magia 'O Inverno' ativa que afete o oponente do jogador passado
    if( playerSpells.some( card => card instanceof this && card.content.effects.isEnabled && card.triggeringPlayer != player ) ) return false;

    // Valida ganho de pontos de destino
    return true;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheWinter.gridOrder = m.data.cards.spells.push( SpellTheWinter );
}

/// Propriedades do protótipo
SpellTheWinter.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheWinter }
} );
