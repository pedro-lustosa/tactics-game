// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheSpring = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheSpring.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheSpring.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheSpring );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-spring';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheWinter;

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' ),
          spellPlayer = spell.getRelationships().controller;

      // Adiciona evento para reduzir custos de mana da magia segundo o uso de pontos de destino do jogador desta magia
      m.events.fatePointsChangeEnd.decrease.add( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );

      // Adiciona evento para redefinir custos de mana da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.add( combatPeriod, spell.content.effects.resetManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          combatPeriod = matchFlow.round.getChild( 'combat-period' ),
          spellPlayer = spell.getRelationships().controller;

      // Remove evento para reduzir custos de mana da magia segundo o uso de pontos de destino do jogador desta magia
      m.events.fatePointsChangeEnd.decrease.remove( spellPlayer, spell.content.effects.updateManaCost, { context: spell.content.effects } );

      // Remove evento para redefinir custos de mana da magia ao fim de cada combate
      m.events.flowChangeEnd.finish.remove( combatPeriod, spell.content.effects.resetManaCost, { context: spell.content.effects } );
    }

    /// Adiciona magias adicionas para disputas mágicas
    spell.addClashingSpells = function () {
      // Identificadores
      var { triggeringPlayer } = this,
          clashingSpells = [],
          playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

      // Adiciona ao arranjo de magias disputantes magias 'Santuário' que mirem a mesma grade desta magia
      clashingSpells = clashingSpells.concat( playerSpells.filter(
        card => card instanceof m.SpellTheWinter && card.isInUse && card.triggeringPlayer != triggeringPlayer
      ) );

      // Retorna magias disputantes
      return clashingSpells;
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 18;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 18;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Ativa efeito da magia
    effects.enable = function () {
      // Não executa função caso magia não esteja em uso
      if( !spell.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( spell );

      // Adiciona evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.add( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Desativa efeito da magia
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return spell;

      // Identificadores
      var preCombat = m.GameMatch.current.flow.round.getChild( 'pre-combat-break-period' );

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( spell );

      // Remove evento para preparar efeito a ocorrer no pré-combate
      m.events.flowChangeEnd.begin.remove( preCombat, preCombat.scheduleEffect, { context: spell } );

      // Indica que efeito da magia está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( spell );

      // Retorna magia
      return spell;
    }

    /// Aplica efeito da magia a ser resolvido no pré-combate
    effects.applyPreCombatEffect = function () {
      // Não executa função caso efeito esteja inativo
      if( !this.isEnabled ) return;

      // Identificadores
      var { triggeringPlayer } = spell;

      // Confere 2 pontos de destino intransitivos ao jogador que usou a magia
      triggeringPlayer.fatePoints.increase( 2 );
    }

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Não executa função caso não se esteja no período do combate
      if( !( m.GameMatch.current.flow.period instanceof m.CombatPeriod ) ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: player, changeNumber: decreasedFatePoints } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'fate-points-change' && eventType == 'decrease' && player == spell.getRelationships().owner );
        m.oAssert( Number.isInteger( decreasedFatePoints ) && decreasedFatePoints < 0 );
      }

      // Identificadores pós-validação
      var { manaCost } = spell.content.stats.current,
          minValue = 3,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, Math.abs( 3 * decreasedFatePoints ) );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    /// Redefine custos de mana desta magia para seus valores originais
    effects.resetManaCost = function () {
      // Identificadores
      var spellStats = spell.content.stats,
          changeNumber = spellStats.base.manaCost.toChannel.min - spellStats.current.manaCost.toChannel.min;

      // Não executa função caso não haja custo de mana a ser ajustado
      if( !changeNumber ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );

      // Redefine custos de mana da magia
      for( let key of [ 'min', 'max' ] ) spellStats.current.manaCost.toChannel[ key ] = spellStats.base.manaCost.toChannel[ key ];

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 80;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheSpring.gridOrder = m.data.cards.spells.push( SpellTheSpring );
}

/// Propriedades do protótipo
SpellTheSpring.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheSpring }
} );
