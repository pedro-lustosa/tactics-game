// Módulos
import * as m from '../../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SpellTheFeast = function ( config = {} ) {
  // Iniciação de propriedades
  SpellTheFeast.init( this );

  // Superconstrutor
  m.Faoth.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SpellTheFeast.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Faoth.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof SpellTheFeast );

    // Atribuição de propriedades iniciais

    /// Nome
    spell.name = 'spell-the-feast';

    /// Magia Oposta
    spell.oppositeSpell = m.SpellTheBeast;

    /// Adiciona à partida em que se estiver configurações próprias da carta
    spell.configMatch = function () {
      // Adiciona evento para atualizar custos de mana da magia ao fim de paradas de destino
      m.events.breakEnd.fate.add( m.GameMatch.current, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    /// Remove da partida em que se estiver configurações próprias da carta
    spell.unconfigMatch = function () {
      // Remove evento para atualizar custos de mana da magia ao fim de paradas de destino
      m.events.breakEnd.fate.remove( m.GameMatch.current, spell.content.effects.updateManaCost, { context: spell.content.effects } );
    }

    // Atribuição de propriedades de 'content'
    let content = spell.content;

    /// Imagem
    content.image = 'card-front';

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Disponibilidade
    typeset.availability = 1;

    /// Polaridade
    typeset.polarity = 'positive';

    /// Duração
    typeset.duration = 'permanent';

    // Atribuição de propriedades de 'content.stats.current'
    let currentStats = content.stats.current;

    /// Alcance
    currentStats.range = 'G0';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = currentStats.flowCost;

    /// Custo de fluxo para canalização
    currentFlowCost.toChannel = 4;

    // Atribuição de propriedades de 'content.stats.current.manaCost.toChannel'
    let currentManaCostToChannel = currentStats.manaCost.toChannel;

    /// Custo de mana mínimo para canalização
    currentManaCostToChannel.min = 12;

    /// Custo de mana máximo para canalização
    currentManaCostToChannel.max = 12;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Atualiza custos de mana desta magia
    effects.updateManaCost = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, player, isContestable, wasSkipped, wasSuccess } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'break' && eventType == 'fate' );

      // Não executa função caso parada de destino não tenha sido contestável
      if( !isContestable ) return;

      // Não executa função caso parada de destino não tenha sido conduzida
      if( wasSkipped ) return;

      // Não executa função caso parada de destino não tenha sido bem-sucedida
      if( !wasSuccess ) return;

      // Não executa função caso jogador da parada de destino não tenha sido o dono desta magia
      if( spell.getRelationships().owner != player ) return;

      // Identificadores
      var { manaCost } = spell.content.stats.current,
          minValue = 2,
          changeNumber = Math.min( manaCost.toChannel.min -  minValue, 2 );

      // Não executa função caso não haja custo de mana a ser ajustado
      if( changeNumber <= 0 ) return;

      // Sinaliza início da mudança de custos de mana
      m.events.cardContentStart.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );

      // Reduz custos de mana da magia
      for( let key of [ 'min', 'max' ] ) manaCost.toChannel[ key ] -= changeNumber;

      // Sinaliza fim da mudança de custos de mana
      m.events.cardContentEnd.manaCost.emit( spell, { type: 'toChannel', changeNumber: -changeNumber } );
    }

    // Atribuição de propriedades de 'coins'
    let coins = spell.coins;

    /// Atual
    coins.current = 60;
  }

  // Verifica se uma parada de destino pode ser recontestada
  SpellTheFeast.checkCounterContestation = function ( player, playerBeing ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( player instanceof m.GamePlayer );
      m.oAssert( m.GameMatch.fateBreakExecution );
      m.oAssert( !playerBeing || [ m.GameMatch.fateBreakExecution.playerBeing, m.GameMatch.fateBreakExecution.opponentBeing ].includes( playerBeing ) );
    }

    // Identificadores para validação de pontos de destino
    let { requiredPoints } = m.GameMatch.fateBreakExecution,
        playerTotalFatePoints = player.fatePoints.current.get() + ( playerBeing?.content.stats.fatePoints.current ?? 0 );

    // Indica que efeito desta magia não é aplicável caso jogador alvo e seu eventual ente não tenham mais os pontos de destino requeridos
    if( playerTotalFatePoints < requiredPoints ) return false;

    // Captura magias dos jogadores
    let playerSpells = [ 'odd', 'even' ].flatMap( parity => m.GameMatch.current.decks[ parity ].cards.spells );

    // Indica que efeito desta magia não é aplicável caso jogador alvo não tenha nenhuma carta desta magia cujo efeito esteja habilitado
    if( !playerSpells.some( card => card instanceof this && card.content.effects.isEnabled && card.triggeringPlayer == player ) ) return false;

    // Indica que efeito desta magia é aplicável
    return true;
  }

  // Ordem inicial das variações desta carta na grade
  SpellTheFeast.gridOrder = m.data.cards.spells.push( SpellTheFeast );
}

/// Propriedades do protótipo
SpellTheFeast.prototype = Object.create( m.Faoth.prototype, {
  // Construtor
  constructor: { value: SpellTheFeast }
} );
