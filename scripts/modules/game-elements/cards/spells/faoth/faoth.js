// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const Faoth = function ( config = {} ) {
  // Superconstrutor
  m.Spell.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  Faoth.init = function ( spell ) {
    // Chama 'init' ascendente
    m.Spell.init( spell );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( spell instanceof Faoth );

    // Atribuição de propriedades de 'content.typeset'
    let typeset = spell.content.typeset;

    /// Senda
    typeset.path = 'faoth';
  }
}

/// Propriedades do protótipo
Faoth.prototype = Object.create( m.Spell.prototype, {
  // Construtor
  constructor: { value: Faoth }
} );
