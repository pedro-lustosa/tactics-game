// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const Card = function ( config = {} ) {
  // Identificadores
  var { content, content: { designation, typeset, stats, effects }, coins } = this;

  // Atribuição dos dados de localização
  Card.setLocationData( this );

  // Até que quantidade de moedas de uma carta seja definida, torna-a um valor que a impossibilita ser usada
  if( !stats.command && !this.isToken && !this.sourceBeing ) coins.current ||= 9999;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ this.name, designation.full, designation.title, designation.subtitle, content.image, content.epigraph ].every(
      value => value && typeof value == 'string'
    ) );
    m.oAssert( this.isToken || this.gridOrder );
    m.oAssert( !this.isToken || this.summoner );
    m.oAssert( ( stats.command || this.isToken || this.sourceBeing ) && !coins.current || Number.isInteger( coins.current ) && coins.current >= 1 );
    m.oAssert( [ 'static', 'dynamic' ].includes( typeset.usage ) );
    m.oAssert( ( this.isToken || this.sourceBeing ) && !typeset.availability || typeset.availability >= 1 && typeset.availability <= 4 );
  }

  // Superconstrutor
  PIXI.Container.call( this );

  // Configurações pós-superconstrutor

  /// Define moedas originais
  coins.base = coins.current;

  /// Define corpo
  this.body.build();

  /// Torna carta interativa
  this.buttonMode = this.interactive = true;

  /// Inseri no contedor da carta sua frente
  this.addChild( this.body.front );

  // Eventos

  /// Para exibir conteúdo da carta na moldura ativa
  this.addListener( 'click', m.GameFrame.showCard );

  /// Para selecionar carta como alvo de uma ação, quando aplicável
  this.addListener( 'dblclick', m.GameAction.selectTarget );

  /// Para mostrar menu de ações da carta, quando aplicável
  this.addListener( 'rightclick', m.ActionsMenu.build );

  /// Para controlar menu de contexto do navegador de modo a não conflitar com o do jogo
  for( let eventName of [ 'mouseover', 'mouseout' ] ) this.addListener( eventName, m.app.controlBrowserContextMenu );

  /// Para atualizar texto suspenso sobre carta
  this.addListener( 'mouseover', Card.updateHoverText, Card );

  /// Para aplicar filtro de tons de cinza à carta quando seu efeito tiver sido inativado, ou quando ela tiver entrada em uso
  for( let eventPath of [ m.events.cardEffectEnd.disable, m.events.cardUseStart.useIn ] )
    eventPath.add( this, this.body.applyGrayScale, { context: this.body } );

  /// Para remover filtro de tons de cinza da carta quando seu efeito tiver sido ativado, ou quando ela tiver entrada em desuso
  for( let eventPath of [ m.events.cardEffectEnd.enable, m.events.cardUseEnd.useOut ] )
    eventPath.add( this, this.body.dropGrayScale, { context: this.body } );

  /// Para atualizar atividade de vinculados de carta segundo mudanças na atividade dessa carta
  m.events.cardActivityEnd.any.add( this, this.updateCardAttachments );

  /// Para, quando carta for inativada ou alheada, impedir eventuais ações em andamento em que ela seja o alvo
  for( let eventName of [ 'inactive', 'unlinked' ] )
    m.events.cardActivityEnd[ eventName ].add( this, () => m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.target == this ) ) );

  /// Caso carta tenha um efeito a ser provocado com seu uso, adiciona evento para o provocar
  if( effects.useEffect ) m.events.cardUseEnd.useIn.add( this, effects.useEffect, { context: effects } );
}

/// Propriedades do construtor
defineProperties: {
  // Tamanhos de cartas
  Card.sizes = {
    base: {
      width: 5, height: 7
    },
    engagementZone: {
      width: 60, height: 60
    },
    poolSlot: {
      width: 75, height: 105
    },
    fieldSlot: {
      width: 110, height: 154
    },
    poolFrame: {
      width: 160, height: 224
    },
    fieldFrame: {
      width: 240, height: 336
    }
  };

  // Controla exibição das designações de cartas
  Card.designationVisibility = ( function () {
    // Identificadores
    var designationVisibility = {};

    // Define se por padrão designações de cartas são visíveis em campos
    designationVisibility.field = false;

    // Define se por padrão designações de cartas são visíveis em róis
    designationVisibility.pool = screen.availWidth >= 1900 && screen.availHeight >= 950 ? true : false;

    // Define se por padrão designações de cartas são visíveis em molduras
    designationVisibility.frame = false;

    // Define se por padrão designações de cartas são visíveis em listas
    designationVisibility.list = true;

    // Retorna objeto de controle da exibição de designação de cartas
    return designationVisibility;
  } )();

  // Iniciação de propriedades
  Card.init = function ( card ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( card instanceof Card );

    // Atribuição de propriedades iniciais

    /// Nome
    card.name = '';

    /// Texto suspenso a exibir informações sobre carta
    card.hoverText = '';

    /// Nome do texto suspenso a exibir informações sobre carta
    card.hoverTextName = '';

    /// Atividade da carta
    card.activity = 'unlinked';

    /// Ordem da carta em grades
    card.gridOrder = 0;

    /// Indica se carta está sendo usada
    card.isInUse = false;

    /// Indica se carta é uma ficha
    card.isToken = false;

    /// Casa em que carta está
    card.slot = null;

    /// Para pertences, indica dono da carta
    card.owner = null;

    /// Para fichas, indica convocante da carta
    card.summoner = null;

    /// Baralho da carta
    card.deck = null;

    /// Arranjo para registro de ações que carta pode acionar
    card.actions = [];

    /// Estrutura visual
    card.body = {};

    /// Conteúdo
    card.content = {};

    /// Moedas
    card.coins = {};

    /// Vinculatividade
    Object.oAssignByCopy( card, m.mixinAttachable.call( card ) );

    /// Alheia carta
    card.unlink = function ( context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( context.constructor == Object );
        m.oAssert( this.isToken || this.sourceBeing || this.activity != 'active' );
        m.oAssert( !this.attachments.external.some( attachment => attachment instanceof m.Item ) );
      }

      // Identificadores
      var formerUsage = this.isInUse,
          dataToEmit = {
            formerActivity: this.activity, newActivity: 'unlinked', context: context
          };

      // Encerra função caso carta já esteja alheia
      if( this.activity == 'unlinked' ) return this;

      // Sinaliza início da mudança de atividade
      m.events.cardActivityStart.unlinked.emit( this, dataToEmit );

      // Sinaliza início da mudança de uso, se aplicável
      if( formerUsage ) m.events.cardUseStart.useOut.emit( this, { useTarget: this } );

      // Desabilita efeito da carta
      this.content.effects.disable();

      // Se carta estiver em um ambiente, retira-a de lá
      if( this.slot ) this.slot.environment instanceof m.Field ? this.slot.environment.remove( this ) : this.deck.owner.pool.remove( this, true );

      // Quando aplicável, nulifica ente modelo da carta
      this.sourceBeing &&= null;

      // Alheia carta
      this.activity = 'unlinked';

      // Indica que carta não está mais em uso
      this.isInUse = false;

      // Sinaliza fim da mudança de uso, se aplicável
      if( formerUsage ) m.events.cardUseEnd.useOut.emit( this, { useTarget: this } );

      // Sinaliza fim da mudança de atividade
      m.events.cardActivityEnd.unlinked.emit( this, dataToEmit );

      // Retorna carta
      return this;
    }

    /// Adapta carta para o tipo de tamanho alvo
    card.toSize = function ( sizeType ) {
      // Identificadores
      var cardFront = this.body.front;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( sizeType in Card.sizes );
        m.oAssert( sizeType != 'base' );
      }

      // Encerra função caso carta já esteja sob o tamanho alvo
      if( this.width == Card.sizes[ sizeType ].width && this.height == Card.sizes[ sizeType ].height ) return false;

      // Redimensiona carta
      for( let size of [ 'width', 'height' ] ) this[ size ] = Card.sizes[ sizeType ][ size ];

      // Alça rodapé da carta, para que não aumente o tamanho desta quando for redimensionado
      cardFront.footer.y = 0;

      // Ajusta designações da carta segundo tipo do tamanho
      switch( sizeType ) {
        case 'poolSlot':
          for( let section of [ 'heading', 'footer' ] ) {
            // Exibição da seção de designação
            cardFront[ section ].visible = Card.designationVisibility.pool;

            // Ajuste da seção de designação
            adjustDesignationSection( section, 2.1 );
          }
          break;
        case 'fieldSlot':
          for( let section of [ 'heading', 'footer' ] ) {
            // Exibição da seção de designação
            cardFront[ section ].visible = Card.designationVisibility.field;

            // Ajuste da seção de designação
            adjustDesignationSection( section, 1.8 );
          }
          break;
        case 'poolFrame':
          for( let section of [ 'heading', 'footer' ] ) {
            // Exibição da seção de designação
            cardFront[ section ].visible = Card.designationVisibility.frame;

            // Ajuste da seção de designação
            adjustDesignationSection( section, 1.3 );
          }
          break;
        case 'fieldFrame': {
          for( let section of [ 'heading', 'footer' ] ) {
            // Exibição da seção de designação
            cardFront[ section ].visible = Card.designationVisibility.frame;

            // Ajuste da seção de designação
            adjustDesignationSection( section, 1 );
          }
        }
      }

      // Recoloca rodapé da carta em sua posição padrão
      cardFront.footer.y = this.getLocalBounds().height - cardFront.footer.height;

      // Retorna alvo
      return this;

      // Ajusta uma seção de designação da carta
      function adjustDesignationSection( section, resizeRatio ) {
        // Redimensionamento da seção
        cardFront[ section ].scale.set( 1, resizeRatio );

        // Redimensionamento do conteúdo da seção
        cardFront[ section ].content.scale.set( resizeRatio, 1 );

        // Centralização do conteúdo da seção
        cardFront[ section ].content.x = cardFront.background.width * .5 - cardFront[ section ].content.width * .5;
      }
    }

    /// Retorna relações da carta com [usuários/jogadores]
    card.getRelationships = function () {
      // Identificadores
      var relationships = {},
          cardDeck = this.deck ?? this.summoner?.deck;

      // Identifica dono da carta, enquanto sendo o usuário ou jogador que a tem em seu baralho
      relationships.owner = cardDeck?.owner ?? null;

      // Identifica usuário que tem carta em seu baralho
      relationships.user = relationships.owner instanceof m.GameUser ? relationships.owner : relationships.owner?.user ?? null;

      // Identifica jogador que tem carta em seu baralho
      relationships.player = relationships.owner instanceof m.GamePlayer ? relationships.owner : null;

      // Para caso carta tenha um jogador lhe vinculado
      if( relationships.player ) {
        // Identifica jogador que atualmente controla a carta
        relationships.controller = ( this instanceof m.Being ?
          this.conditions.find( condition => condition instanceof m.ConditionPossessed )?.controller :
          this.owner?.conditions.find( condition => condition instanceof m.ConditionPossessed )?.controller
        ) ?? relationships.player;

        // Identifica jogador adversário ao que controla a carta
        relationships.opponent = m.GameMatch.current.players.get( 'all' ).find( player => player != relationships.controller );
      }

      // Do contrário
      else {
        // Identifica quem atualmente controla a carta
        relationships.controller = relationships.owner;

        // Identifica jogador adversário ao que controla a carta
        relationships.opponent = null;
      }

      // Retorna relações da carta
      return relationships;
    }

    /// Retorna um identificador único para a carta em relação a uma partida
    card.getMatchId = function () {
      // Identificadores pré-validação
      var cardDeck = this.deck ?? this.summoner?.deck,
          cardPlayer = cardDeck?.owner;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( m.GameMatch.current );
        m.oAssert( cardPlayer instanceof m.GamePlayer );
      }

      // Identificadores pós-validação
      var matchId = '';

      // Adiciona ao identificador da partida tipo de baralho em que carta está
      matchId += cardDeck instanceof m.MainDeck ? 'M' : 'S';

      // Adiciona ao identificador da partida paridade do jogador da carta
      matchId += cardPlayer.parity == 'odd' ? '1' : '2';

      // Adiciona ao identificador da partida um separador
      matchId += '-';

      // Adiciona ao identificador da partida índice da carta em seu baralho alvo
      matchId += cardDeck.cards.indexOf( this.summoner ?? this );

      // Caso carta não tenha sido convocada, encerra função
      if( !this.summoner ) return matchId;

      // Adiciona ao identificador da partida indicador de que carta é uma ficha ou um equipamento embutido
      matchId += this.isToken ? 'S' : this.isEmbedded ? 'E' : '?';

      // Identifica como proceder em função do indicador adicionado à carta
      switch( matchId.slice( -1 ) ) {
        // Para fichas
        case 'S':
          // Adiciona ao identificador da partida posição de carta no arranjo de convocados de seu convocante
          matchId += this.summoner.summons.indexOf( this );

          // Encerra operação
          break;
        // Para equipamentos embutidos
        case 'E': {
          // Identifica se equipamento está diretamente vinculado a seu dono, ou se está antes vinculado a outro equipamento
          let isDirectAttachment = this.attacher == this.summoner;

          // Adiciona ao identificador da partida posição de carta no arranjo de equipamentos embutidos de seu convocante
          matchId += this.summoner.attachments.embedded.indexOf( isDirectAttachment ? this : this.attacher );

          // Caso equipamento esteja vinculado a outro equipamento, adiciona sua posição no índice de vinculados de seu vinculante
          if( !isDirectAttachment ) matchId += '+' + this.attacher.attachments.embedded.indexOf( this );

          // Encerra operação
          break;
        }
        // Para outros casos, lança um erro
        default:
          throw new Error( `Unexpected card for getting the match id.` );
      }

      // Retorna identificador da partida de carta
      return matchId;
    }

    // Atribuição de propriedades de 'body'
    let body = card.body;

    /// Frente da carta
    body.front = null;

    /// Verso da carta
    body.back = null;

    /// Monta corpo da carta
    body.build = function ( sizeType = 'fieldFrame', isOnlyFront = false ) {
      // Identificadores
      var [ cardWidth, cardHeight ] = [ Card.sizes[ sizeType ].width, Card.sizes[ sizeType ].height ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( sizeType in Card.sizes );
        m.oAssert( sizeType != 'base' );
      }

      // Monta frente da carta
      buildFront.call( this );

      // Monta verso da carta
      if( !isOnlyFront ) buildBack.call( this );

      // Retorna corpo da carta
      return this;

      // Monta frente da carta
      function buildFront() {
        // Identificadores
        var front = this.front = new PIXI.Container(),
            heading = this.front.heading = new PIXI.Graphics(),
            footer = this.front.footer = new PIXI.Graphics(),
            textStyle = {
              fontName: m.assets.fonts.sourceSansPro.large.name, fontSize: -11, tint: m.data.colors.black
            };

        // Plano de fundo

        /// Geração
        front.background = new PIXI.Sprite( PIXI.utils.TextureCache[ card.content.image ] );

        /// Medidas
        [ front.background.width, front.background.height ] = [ cardWidth, cardHeight ];

        /// Iniciação do arranjo de filtros
        front.background.filters = [];

        // Configurações do cabeçalho e do rodapé
        for( let section of [ heading, footer ] ) {
          // Geração do texto
          section.content = new PIXI.BitmapText( section == heading ? card.content.designation.title : card.content.designation.subtitle, textStyle );

          // Inserção do texto
          section.addChild( section.content );

          // Confecção da seção

          /// Preenchimento
          section.beginFill( m.data.colors.white );

          /// Renderização
          section.drawRect( 0, 0, cardWidth, section.content.height + 2 );

          /// Fim do preenchimento
          section.endFill();

          // Posicionamento do texto
          section.content.position.set( section.width * .5 - section.content.width * .5, section.height * .5 - section.content.height * .5 );

          // Exibição da seção
          section.visible = Card.designationVisibility.frame;
        }

        // Posicionamento do rodapé
        footer.y = cardHeight - footer.height;

        // Inserção dos componentes
        front.addChild( front.background, footer, heading );
      }

      // Monta verso da carta
      function buildBack() {
        // Identificadores
        var back = this.back = new PIXI.Container();

        // Plano de fundo
        back.background = new PIXI.Sprite( PIXI.utils.TextureCache[ 'card-back' ] );

        // Inserção dos componentes
        back.addChild( back.background );
      }
    }

    /// Aplica à frente da carta filtro de tons de cinza
    body.applyGrayScale = function ( eventData = {} ) {
      // Para eventos de uso, não executa função caso alvo do evento e alvo do uso sejam distintos
      if( eventData.useTarget && eventData.useTarget != eventData.eventTarget ) return;

      // Não executa função caso já exista um filtro de tons de cinza
      if( this.front.background.filters.some( filter => filter.name == 'gray-scale-filter' ) ) return this;

      // Identificadores pós-validação
      var frontBackground = this.front.background,
          colorFilter = new PIXI.filters.ColorMatrixFilter();

      // Configura filtro de cor

      /// Nomeação do filtro
      colorFilter.name = 'gray-scale-filter';

      /// Aplicação de escala de cinza
      colorFilter.greyscale( .5 );

      // Adiciona filtro de cor ao arranjo de filtros da frente da carta
      frontBackground.filters.push( colorFilter );

      // Retorna corpo da carta
      return this;
    }

    /// Remove da frente da carta filtro de tons de cinza
    body.dropGrayScale = function ( eventData = {} ) {
      // Para eventos de uso, não executa função caso alvo do evento e alvo do uso sejam distintos
      if( eventData.useTarget && eventData.useTarget != eventData.eventTarget ) return;

      // Identificadores
      var frontBackground = this.front.background,
          filterIndex = frontBackground.filters.findIndex( filter => filter.name == 'gray-scale-filter' );

      // Caso filtro de tons de cinza exista, remove-o do arranjo de filtros
      if( filterIndex != -1 ) frontBackground.filters.splice( filterIndex, 1 ).pop().destroy( { children: true } );

      // Retorna corpo da carta
      return this;
    }

    /// Atualiza exibição das seções de designação de carta
    body.updateDesignationVisibility = function( scope ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( scope in Card.designationVisibility );

      // Atualização da exibição das seções de designação
      for( let section of [ 'heading', 'footer' ] ) this.front[ section ].visible = Card.designationVisibility[ scope ];
    }

    // Atribuição de propriedades de 'content'
    let content = card.content;

    /// Designação
    content.designation = {};

    /// Imagem
    content.image = '';

    /// Tipagem
    content.typeset = {};

    /// Estatísticas
    content.stats = {};

    /// Efeitos
    content.effects = {};

    /// Epígrafe
    content.epigraph = '';

    // Atribuição de propriedades de 'content.designation'
    let designation = content.designation;

    /// Título
    designation.title = '';

    /// Subtítulo
    designation.subtitle = '';

    /// Designação completa
    designation.full = '';

    /// Retorna um número que aponta posição da carta em seu baralho em relação a outras cartas com a mesma designação
    designation.getIndex = function ( isAsText = false, targetDeck = null ) {
      // Identificadores pré-validação
      var deck = ( card.deck ?? card.summoner?.deck ?? targetDeck );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( deck instanceof m.Deck );

      // Identificadores pós-validação
      var sameDesignationCards =
            card.isEmbedded ? card.attacher.attachments.embedded.filter( attachment => card.content.designation.full == attachment.content.designation.full ) :
            card.isToken ?
              deck.cards.reduce( ( accumulator, current ) =>
                accumulator.concat( current.summons?.filter( token => card.content.designation.title == token.content.designation.title ) ?? [] ), []
              ) :
            deck.cards.filter( deckCard => card.content.designation.full == deckCard.content.designation.full ),
          cardIndex = !card.isEmbedded && deck == targetDeck ? sameDesignationCards.length : sameDesignationCards.indexOf( card );

      // Caso índice deva ser retornado enquanto um íntegro, encerra função
      if( !isAsText ) return cardIndex;

      // Caso índice deva ser retornado enquanto um texto, omite-o para caso ele seja o primeiro, e retorna-o incrementado nos demais casos
      return cardIndex ? ' ' + ++cardIndex : '';
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = content.typeset;

    /// Vinculação
    typeset.attachability = null;

    /// Uso
    typeset.usage = '';

    /// Disponibilidade
    typeset.availability = 0;

    // Atribuição de propriedades de 'content.effects'
    let effects = content.effects;

    /// Descrição
    effects.description = '';

    /// Indica se efeito da carta está ativo
    effects.isEnabled = false;

    /// Ativa efeito da carta
    effects.enable = function () {
      // Não executa função caso carta não esteja em uso
      if( !card.isInUse ) return false;

      // Não executa função caso efeito já esteja ativo
      if( this.isEnabled ) return card;

      // Indica início da ativação do efeito
      m.events.cardEffectStart.enable.emit( card );

      // Indica que efeito da carta está ativo
      this.isEnabled = true;

      // Indica fim da ativação do efeito
      m.events.cardEffectEnd.enable.emit( card );

      // Retorna carta
      return card;
    }

    /// Desativa efeito da carta
    effects.disable = function () {
      // Não executa função caso efeito já esteja inativo
      if( !this.isEnabled ) return card;

      // Indica início da inativação do efeito
      m.events.cardEffectStart.disable.emit( card );

      // Indica que efeito da carta está inativo
      this.isEnabled = false;

      // Indica fim da inativação do efeito
      m.events.cardEffectEnd.disable.emit( card );

      // Retorna carta
      return card;
    }

    // Atribuição de propriedades de 'coins'
    let coins = card.coins;

    /// Original
    coins.base = 0;

    /// Atual
    coins.current = 0;

    // Atribuição de propriedades de 'actions'
    let actions = card.actions;

    /// Arranjo de ações singulares acionadas
    actions.singleCommitments = [];
  }

  // Reproduz uma carta, através de seus dados de registro
  Card.load = function ( cardData = {} ) {
    // Identificadores pré-validação
    var { constructorName, type } = cardData,
        cardConstructors = m.data.cards;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( cardData.constructor == Object );
      m.oAssert( constructorName && typeof constructorName == 'string' );
      m.oAssert( [ 'commander', 'humanoid', 'item', 'spell' ].includes( type ) );
    }

    // Reproduz carta em função de seu tipo
    switch( type ) {
      case 'commander':
        return loadCommander();
      case 'humanoid':
        return loadHumanoid();
      case 'item':
        return loadItem();
      case 'spell':
        return loadSpell();
    }

    // Reproduz um comandante
    function loadCommander() {
      // Identificadores
      var targetConstructor = cardConstructors.commanders.find( constructor => constructor.name == constructorName );

      // Retorna comandante apontado
      return new targetConstructor();
    }

    // Reproduz um humanoide
    function loadHumanoid() {
      // Identificadores
      var targetConstructor = cardConstructors.creatures.find( constructor => constructor.name == constructorName ),
          config = {};

      // Se existente, atribui nível do humanoide para as configurações de geração
      if( 'level' in cardData ) config.level = cardData.level;

      // Retorna humanoide apontado
      return new targetConstructor( config );
    }

    // Reproduz um equipamento
    function loadItem() {
      // Identificadores
      var targetConstructor = cardConstructors.items.find( constructor => constructor.name == constructorName ),
          config = {};

      // Quando existentes, atribui propriedades configuráveis do equipamento
      for( let key of [ 'size', 'grade' ] )
        if( key in cardData ) config[ key ] = cardData[ key ];

      // Retorna equipamento apontado
      return new targetConstructor( config );
    }

    // Reproduz uma magia
    function loadSpell() {
      // Identificadores
      var targetConstructor = cardConstructors.spells.find( constructor => constructor.name == constructorName );

      // Retorna comandante apontado
      return new targetConstructor();
    }
  }

  // Retorna uma carta a partir de seu identificador na partida atual
  Card.getFromMatchId = function ( matchId ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( m.GameMatch.current );
      m.oAssert( typeof matchId == 'string' && matchId.length >= 4 );
      m.oAssert( [ 'M', 'S' ].includes( matchId[ 0 ] ) );
      m.oAssert( [ '1', '2' ].includes( matchId[ 1 ] ) );
      m.oAssert( matchId[ 2 ] == '-' );
      m.oAssert( /\d/.test( matchId[ 3 ] ) );
    }

    // Identificadores
    var gameMatch = m.GameMatch.current,
        targetParity = matchId[ 1 ] == '1' ? 'odd' : 'even',
        targetDeck = matchId[ 0 ] == 'M' ? gameMatch.decks[ targetParity ] : gameMatch.decks[ targetParity ].sideDeck,
        idSuffix = matchId.slice( 3 ),
        idSuffixIndexes = idSuffix.split( /\D/ ),
        deckIndex = idSuffixIndexes[ 0 ],
        deckCard = targetDeck.cards[ deckIndex ];

    // Caso identificador de partida aponte para uma carta de baralho, retorna-a
    if( idSuffixIndexes.length == 1 ) return deckCard ?? false;

    // Identifica carta segundo letra indicadora de seu tipo
    switch( idSuffix[ idSuffix.search( /\D/ ) ] ) {
      // Para fichas
      case 'S':
        // Retorna ficha que corresponder ao índice apontado pelo identificador
        return deckCard.summons[ idSuffixIndexes[ 1 ] ] ?? false;
      // Para equipamentos embutidos
      case 'E': {
        // Identificadores
        let attachment = deckCard.attachments.embedded[ idSuffixIndexes[ 1 ] ] ?? false,
            subIndex = idSuffixIndexes[ 2 ];

        // Retorna equipamento em função de se ele está diretamente vinculado a seu dono ou vinculado antes a outro equipamento
        return !subIndex ? attachment : attachment?.attachments.embedded[ subIndex ] ?? false;
      }
      // Para outros casos, lança um erro
      default:
        throw new Error( `"${ matchId }" is an unexpected match id.` );
    }
  }

  // Atribui à carta passada seus dados de localização
  Card.setLocationData = function ( card ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( card instanceof this );

    // Identificadores
    var cardData = m.languages.cards.getData( card );

    // Atribuição do título e do subtítulo
    for( let property of [ 'title', 'subtitle' ] ) card.content.designation[ property ] = cardData[ property ];

    // Atribuição da designação completa
    card.content.designation.full = cardData.fullDesignation;

    // Atribuição da epígrafe
    card.content.epigraph = cardData.epigraph;

    // Atribuição da descrição do efeito, quando existente
    card.content.effects.description = cardData.effects ?? '';

    // Para comandantes, caso existam restrições especiais, atribui sua descrição
    if( cardData.creaturesConstraint ) card.content.stats.command.scopes.creature.total.constraint.description = cardData.creaturesConstraint;

    // Para caso carta seja um ente de baralho baseado em outro
    if( card instanceof m.Being && card.sourceBeing && !card.isToken ) {
      // Ajusta designação completa para que contemple nome do ente a que carta se baseia
      card.content.designation.full = m.languages.snippets.getSomethingOf( card.sourceBeing.content.designation.title, card.content.designation.title );

      // Ajusta título da carta para que seja igual ao do ente em que ela se baseia
      card.content.designation.title = card.sourceBeing.content.designation.title;
    }
  }

  // Controla textos suspensos de cartas
  Card.controlHoverText = function ( eventData = {} ) {
    // Identificadores pré-validação
    var { eventCategory, eventType, eventTarget } = eventData;

    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( eventCategory == 'flow-change' && [ 'begin', 'finish' ].includes( eventType ) );
      m.oAssert( [ m.FlowStep, m.FlowPeriod ].some( constructor => eventTarget instanceof constructor ) );
    }

    // Identificadores pós-validação
    var matchCards = m.GameMatch.current.decks.getAllCards();

    // Delega operação em função de tipo do evento
    return eventType == 'begin' ? controlOnBegin() : controlOnFinish();

    // Funções

    /// Para o controle de textos suspensos ao se entrar em novo estágio
    function controlOnBegin() {
      // Identificadores
      var targetCard = matchCards.find( card => card.oCheckCursorOver() );

      // Caso cursor esteja sobre uma determinada carta, programa exibição de seu texto suspenso
      if( targetCard ) setTimeout( Card.updateHoverText.bind( Card, { currentTarget: targetCard } ) );
    }

    /// Para o controle de textos suspensos ao se sair de um estágio alvo
    function controlOnFinish() {
      // Remove texto suspenso de cartas, e dados relativos a ele
      for( let card of matchCards ) Card.removeHoverText( card );
    }
  }

  // Atualiza texto suspenso da carta passada
  Card.updateHoverText = function ( event = {} ) {
    // Identificadores
    var { currentTarget: card } = event;

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( card instanceof this );

    // Não executa função caso cursor não esteja sobre carta alvo
    if( !card.oCheckCursorOver() ) return;

    // Remove texto suspenso antigo da carta, caso exista
    this.removeHoverText( card );

    // Apenas continua função caso haja uma partida atual
    if( !m.GameMatch.current ) return;

    // Atualiza conteúdo do texto suspenso da carta em função do estágio em que se estiver
    m.GameMatch.current.flow.step instanceof m.ArrangementStep ? updateArrangementStepHoverText() :
    m.GameMatch.current.flow.step instanceof m.AllocationStep ? updateAllocationStepHoverText() :
    m.GameMatch.current.flow.period instanceof m.CombatPeriod ? updateCombatPeriodHoverText() : null;

    // Quando existente, exibe texto suspenso da carta
    return m.app.showHoverText.call( card );

    // Funções

    /// Para atualização do texto suspenso da etapa do arranjo
    function updateArrangementStepHoverText() {
      // Identificadores
      var isCommander = card.content.stats.command;

      // Não executa função caso carta não tenha um preço em moedas e não seja um comandante
      if( !card.coins.base && !isCommander ) return;

      // Define texto suspenso da carta
      card.hoverText = ( function () {
        // Caso carta não seja um comandante, retorna seu valor em moedas
        if( !isCommander ) return m.languages.snippets.coins + `: ${ card.coins.base }`;

        // Identificadores para definição do texto suspenso para comandantes
        var text = '',
            mainDeck = card.getRelationships().owner.gameDeck,
            { sideDeck } = mainDeck,
            deckTexts = m.languages.components.deckFrame();

        // Caso não haja um baralho coadjuvante com cartas, encerra função
        if( !sideDeck?.cards.length ) return text;

        // Inicia texto sobre moedas disponíveis
        text += `${ deckTexts.availableCoins }:\n`;

        // Adiciona moedas disponíveis de cada baralho
        for( let deck of [ mainDeck, sideDeck ] ) text += `  ${ m.languages.keywords.getDeck( deck.type ) }: ${ deck.coins.max - deck.coins.spent }\n`;

        // Retorna texto sobre moedas
        return text.trimEnd();
      } )();

      // Atualiza nome do texto suspenso sobre carta
      card.hoverTextName = card.hoverText ? 'card-current-coins-hover-text' : '';
    }

    /// Para atualização do texto suspenso da etapa da alocação
    function updateAllocationStepHoverText() {
      // Apenas executa função caso carta seja um ente
      if( !( card instanceof m.Being ) ) return;

      // Apenas executa função caso tenham sido computadas ações com custo de fluxo gastas
      if( !card.actions.spentFlowCost ) return;

      // Define texto sobre o custo de fluxo de ações gastas por carta
      card.hoverText = m.languages.snippets.spentFlowCost + `: ${ card.actions.spentFlowCost }`;

      // Atualiza nome do texto suspenso sobre carta
      card.hoverTextName = 'card-spent-flow-cost-hover-text';
    }

    /// Para atualização do texto suspenso do período do combate
    function updateCombatPeriodHoverText() {
      // Apenas executa função caso carta seja um ente
      if( !( card instanceof m.Being ) ) return;

      // Não executa função caso ente esteja atualmente despreparado e esteja no rol
      if( card.readiness.status == 'unprepared' && card.slot instanceof m.PoolSlot ) return;

      // Define texto sobre o estado de preparo do ente
      card.hoverText = m.languages.keywords.getReadiness( card.readiness.status );

      // Caso ente esteja ocupado, ajusta seu texto suspenso em função da eventual ação que esteja acionando
      if( card.readiness.status == 'occupied' )
        card.hoverText = m.languages.snippets.getBeingCurrentAction( m.GameAction.currents.find( action => action.committer == card ) ) || card.hoverText;

      // Para caso ente esteja com a condição 'possuído'
      if( card.conditions.some( condition => condition instanceof m.ConditionPossessed ) ) {
        // Identificadores
        let { owner: cardOwner, controller: cardController } = card.getRelationships();

        // Caso dono e controlador de ente sejam distintos, adiciona informação sobre seu controlador
        if( cardOwner != cardController ) card.hoverText += '\n' + m.languages.snippets.getController( card );
      }

      // Atualiza nome do texto suspenso sobre carta
      card.hoverTextName = 'being-readiness-hover-text';
    }
  }

  // Remove eventual texto suspenso sobre carta passada
  Card.removeHoverText = function ( card ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( card instanceof this );

    // Para remoção efetiva do texto suspenso
    removeHoverText: {
      // Não executa bloco caso cursor não esteja sobre carta alvo
      if( !card.oCheckCursorOver() ) break removeHoverText;

      // Cancela eventual exibição programada do texto suspenso
      m.app.hoverTextTimeout &&= clearTimeout( m.app.hoverTextTimeout ) ?? null;

      // Encerra bloco caso carta não tenha um nome para seu texto suspenso
      if( !card.hoverTextName ) break removeHoverText;

      // Captura texto suspenso da carta
      let hoverText = m.GameScreen.current.children.find( child => child.name == card.hoverTextName );

      // Remove texto suspenso da carta, caso exista
      if( hoverText ) m.app.removeHoverText( hoverText );
    }

    // Limpa dados do texto suspenso da carta
    card.hoverText = card.hoverTextName = '';
  }
}

/// Propriedades do protótipo
Card.prototype = Object.create( PIXI.Container.prototype, {
  // Construtor
  constructor: { value: Card }
} );
