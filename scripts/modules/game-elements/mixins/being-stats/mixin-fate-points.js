// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinFatePoints = function () {
  // Identificadores
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Pontos de destino atuais
    current: 0,
    // Fontes de pontos de destino
    sources: [],
    // Adiciona fonte de pontos de destino
    add: function ( source = {} ) {
      // Identificadores pré-validação
      var { name: sourceName } = source;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( source.constructor == Object );
        m.oAssert( sourceName && typeof sourceName == 'string' );
        m.oAssert( [ 'baseValue', 'currentValue' ].every( key => key in source && ( source[ key ] == Infinity || Number.isInteger( source[ key ] ) ) ) );
        m.oAssert( source.baseValue > 0 && source.currentValue >= 0 );
      }

      // Quando já existir lá, remove do arranjo de fontes fonte de nome passado
      this.remove( sourceName );

      // Sinaliza início da adição da fonte de pontos de destino
      m.events.cardContentStart.fateIn.emit( sourceCard, { source: source } );

      // Adiciona fonte alvo ao arranjo de fontes
      this.sources.push( source );

      // Adiciona valor atual da nova fonte aos pontos de destino embutidos
      this.current += source.currentValue;

      // Sinaliza fim da adição da fonte de pontos de destino
      m.events.cardContentEnd.fateIn.emit( sourceCard, { source: source } );

      // Retorna objeto de pontos de destino
      return this;
    },
    // Gasta pontos de destino de uma fonte
    spend: function ( number, sourceName ) {
      // Não executa função caso não haja pontos de destino embutidos atuais
      if( !this.current ) return number;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number > 0 );
        m.oAssert( !sourceName || typeof sourceName == 'string' && this.sources.some( sourceObject => sourceObject.name == sourceName ) );
      }

      // Identificadores
      var sourcePreferences = sourceName ? [ this.sources.find( sourceObject => sourceObject.name == sourceName  ) ] : getSourcePreferences.call( this );

      // Sinaliza início do uso de pontos de destino
      m.events.cardContentStart.fateSpent.emit( sourceCard );

      // Remove pontos de destino embutidos, até que valor a ser removido tenha sido completado ou tenham se acabado os pontos embutidos
      for( let source of sourcePreferences )
        while( this.current && number && source.currentValue ) this.current--, number--, source.currentValue--;

      // Sinaliza fim do uso de pontos de destino
      m.events.cardContentEnd.fateSpent.emit( sourceCard );

      // Retorna a quantidade de pontos de destino que não foi removida
      return number;

      // Funções

      /// Retorna arranjo com fontes de pontos de destino na ordem de preferência para o uso
      function getSourcePreferences() {
        // Identificadores
        var sources = this.sources.slice();

        // Caso haja uma fonte proveniente de 'Anel da Proteção' e seu efeito não seja válido nesse caso, retira-a
        if( sources.some( sourceObject => sourceObject.name == 'ring-of-protection' ) && !m.ImplementRingOfProtection.validateFatePointUsage( sourceCard ) )
          sources.splice( sources.findIndex( sourceObject => sourceObject.name == 'ring-of-protection' ), 1 );

        // Retorna fontes de pontos de destino, em ordem decrescente de quantidade de pontos atuais
        return sources.sort( ( a, b ) => b.currentValue - a.currentValue );
      }
    },
    // Remove fonte de pontos de destino
    remove: function ( sourceName ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( sourceName && typeof sourceName == 'string' );

      // Identificadores
      var sourceIndex = this.sources.findIndex( sourceObject => sourceObject.name == sourceName ),
          source = this.sources[ sourceIndex ];

      // Caso fonte de nome passado não exista, encerra função
      if( sourceIndex == -1 ) return this;

      // Sinaliza início da remoção da fonte de pontos de destino
      m.events.cardContentStart.fateOut.emit( sourceCard, { source: source } );

      // Remove fonte alvo do arranjo de fontes
      this.sources.splice( sourceIndex, 1 );

      // Atualiza valor atual de pontos de destino embutidos
      this.current = this.sources.reduce( ( accumulator, current ) => accumulator += current.currentValue, 0 );

      // Sinaliza fim da remoção da fonte de pontos de destino
      m.events.cardContentEnd.fateOut.emit( sourceCard, { source: source } );

      // Retorna objeto de pontos de destino
      return this;
    },
    // Redefine pontos de destino atuais
    reset: function () {
      // Não executa função caso não haja fontes de pontos de destino
      if( !this.sources.length ) return this;

      // Sinaliza início da redefinição das fontes de pontos de destino
      m.events.cardContentStart.fateReset.emit( sourceCard );

      // Zera pontos de destino embutidos atuais
      this.current = 0;

      // Itera por fontes de pontos de destino
      for( let source of this.sources ) {
        // Define valor atual da fonte alvo como igual a seu original
        source.currentValue = source.baseValue;

        // Acrescenta pontos da fonte alvo aos pontos de destino embutidos
        this.current += source.currentValue;
      }

      // Sinaliza fim da redefinição das fontes de pontos de destino
      m.events.cardContentEnd.fateReset.emit( sourceCard );

      // Retorna objeto de pontos de destino
      return this;
    }
  }
};

// Métodos não configuráveis de 'mixinFatePoints'
Object.defineProperties( mixinFatePoints, {
  // Redefine pontos de destino embutidos de todos os entes
  resetAllFatePoints: {
    value: function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget instanceof m.BattleTurn );

      // Identificadores pós-validação
      var matchBeings = m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } );

      // Redefine pontos de destino embutidos dos entes na partida
      for( let being of matchBeings ) being.content.stats.fatePoints.reset();
    }
  }
} );
