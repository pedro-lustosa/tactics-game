// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinResistances = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Resistências naturais
    natural: {
      base: {}, current: {}
    },
    // Resistências atuais
    current: {
      // Físicas
      blunt: 0, slash: 0, pierce: 0,
      // Elementais
      fire: 0, shock: 0, mana: 0,
      // Astrais
      ether: 0
    },
    // Fontes de resistência vinculadas
    sources: [],
    // Adiciona fonte de resistências
    add: function ( source, value = source?.content?.stats?.effectivity?.current?.resistances ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ m.Garment, m.Spell ].some( constructor => source instanceof constructor ) );
        m.oAssert( Object.keys( value ).every( resistanceType => resistanceType in sourceCard.content.stats.effectivity.resistances.current ) );
      }

      // Não executa função caso fonte passada não tenha resistências
      if( !Object.values( value ).some( resistanceValue => resistanceValue ) ) return false;

      // Não executa função caso fonte passada já esteja no arranjo de fontes
      if( this.sources.map( object => object.source ).includes( source ) ) return false;

      // Adiciona fonte de resistências ao arranjo
      this.sources.push( { source: source, value: value } );

      // Atualiza resistências atuais de alvo
      return mixinResistances.updateResistances( sourceCard, source, value );
    },
    // Remove fonte de resistências
    remove: function ( source ) {
      // Identificadores
      var sourceIndex = this.sources.map( object => object.source ).indexOf( source );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Garment, m.Spell ].some( constructor => source instanceof constructor ) );

      // Não executa função caso fonte passada não esteja no arranjo de fontes
      if( sourceIndex == -1 ) return false;

      // Captura valor da fonte de resistências alvo
      let value = this.sources[ sourceIndex ].value;

      // Remove fonte de resistências do arranjo
      this.sources.splice( sourceIndex, 1 );

      // Atualiza resistências atuais de alvo
      return mixinResistances.updateResistances( sourceCard, source, value, true );
    }
  }
};

// Métodos não configuráveis de 'mixinResistances'
Object.defineProperties( mixinResistances, {
  // Atualiza resistências de alvo
  updateResistances: {
    value: function ( sourceCard, targetSource, targetResistances, isReduction = false ) {
      // Identificadores
      var cardResistances = sourceCard.content.stats.effectivity.resistances,
          isGarment = targetSource instanceof m.Garment;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( sourceCard instanceof m.Being );

      // Sinaliza início da mudança de resistências
      m.events.cardContentStart.resistance.emit( sourceCard, { resistance: 'any' } );

      // Itera por resistências passadas
      for( let resistance in targetResistances ) {
        // Captura valor da resistência alvo
        let number = targetResistances[ resistance ];

        // Filtra resistências nulas
        if( !number ) continue;

        // Caso operação seja de subtração, inverte operador da quantidade alvo
        if( isReduction ) number *= -1;

        // Sinaliza início da mudança da resistência alvo
        m.events.cardContentStart.resistance.emit( sourceCard, { resistance: resistance, number: number } );

        // Atualiza resistência alvo
        cardResistances.current[ resistance ] += number;

        // Se fonte alvo não for um traje, atualiza resistência natural alvo
        if( !isGarment ) cardResistances.natural.current[ resistance ] += number;

        // Sinaliza fim da mudança da resistência alvo
        m.events.cardContentEnd.resistance.emit( sourceCard, { resistance: resistance, number: number } );
      }

      // Sinaliza fim da mudança de resistências
      m.events.cardContentEnd.resistance.emit( sourceCard, { resistance: 'any' } );

      // Retorna resistências atuais
      return cardResistances.current;
    }
  }
} );
