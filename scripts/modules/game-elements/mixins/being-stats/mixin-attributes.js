// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinAttributes = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Atributos originais
    base: {},
    // Atributos atuais
    current: {},
    /// Para registro de mudanças nos atributos
    changes: {},
    // Aumenta valor atual de atributo segundo íntegro passado
    increase: function ( number, attribute, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( attribute in this.current );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var attributeCeil = attribute == 'health' ? Infinity : attribute == 'energy' ? 20 : attribute == 'initiative' ? 15 : 10,
          currentAttribute = this.current[ attribute ],
          changeNumber = attribute == 'health' ?
            Math.round( currentAttribute * ( 1 + ( .05 * number ) ) ) - currentAttribute :
            Math.min( attributeCeil - currentAttribute, number ),
          dataToEmit = {
            method: 'increase', attribute: attribute, changeNumber: changeNumber, context: context
          };

      // Não executa função caso não haja um valor a ser adicionado
      if( changeNumber <= 0 ) return this;

      // Sinaliza início da mudança de atributo
      m.events.cardContentStart[ attribute ].emit( sourceCard, dataToEmit );

      // Atualiza valor do atributo alvo
      this.current[ attribute ] += changeNumber;

      // Sinaliza fim da mudança de atributo
      m.events.cardContentEnd[ attribute ].emit( sourceCard, dataToEmit );

      // Retorna atributos
      return this;
    },
    // Diminui valor atual de atributo segundo íntegro passado, até o valor mínimo de 1
    decrease: function ( number, attribute, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( attribute in this.current );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var attributeFloor = 1,
          currentAttribute = this.current[ attribute ],
          changeNumber = attribute == 'health' ?
            currentAttribute - Math.max( Math.round( currentAttribute * ( 1 - ( .05 * number ) ) ), attributeFloor ) :
            Math.min( currentAttribute - attributeFloor, number ),
          dataToEmit = {
            method: 'decrease', attribute: attribute, changeNumber: -changeNumber, context: context
          };

      // Não executa função caso não haja um valor a ser diminuído
      if( changeNumber <= 0 ) return this;

      // Sinaliza início da mudança de atributo
      m.events.cardContentStart[ attribute ].emit( sourceCard, dataToEmit );

      // Atualiza valor do atributo alvo
      this.current[ attribute ] -= changeNumber;

      // Sinaliza fim da mudança de atributo
      m.events.cardContentEnd[ attribute ].emit( sourceCard, dataToEmit );

      // Retorna atributos
      return this;
    },
    // Aumenta valor atual de atributo, mas sem que ele ultrapasse seu valor original
    restore: function ( number, attribute, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( attribute in this.current );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var attributeCeil = this.base[ attribute ],
          currentAttribute = this.current[ attribute ],
          changeNumber = Math.min( attributeCeil - currentAttribute, number ),
          dataToEmit = {
            method: 'restore', attribute: attribute, changeNumber: changeNumber, context: context
          };

      // Não executa função caso não haja um valor a ser restaurado
      if( changeNumber <= 0 ) return this;

      // Sinaliza início da mudança de atributo
      m.events.cardContentStart[ attribute ].emit( sourceCard, dataToEmit );

      // Atualiza valor do atributo alvo
      this.current[ attribute ] += changeNumber;

      // Sinaliza fim da mudança de atributo
      m.events.cardContentEnd[ attribute ].emit( sourceCard, dataToEmit );

      // Retorna atributos
      return this;
    },
    // Diminui valor atual de atributo segundo íntegro passado, até o valor mínimo de 0
    reduce: function ( number, attribute, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( attribute in this.current );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var attributeFloor = 0,
          currentAttribute = this.current[ attribute ],
          changeNumber = Math.min( currentAttribute - attributeFloor, number ),
          dataToEmit = {
            method: 'reduce', attribute: attribute, changeNumber: -changeNumber, context: context
          };

      // Não executa função caso não haja um valor a ser reduzido
      if( changeNumber <= 0 ) return this;

      // Sinaliza início da mudança de atributo
      m.events.cardContentStart[ attribute ].emit( sourceCard, dataToEmit );

      // Atualiza valor do atributo alvo
      this.current[ attribute ] -= changeNumber;

      // Sinaliza fim da mudança de atributo
      m.events.cardContentEnd[ attribute ].emit( sourceCard, dataToEmit );

      // Retorna atributos
      return this;
    },
    // Redefine valor atual de atributos para seu valor original
    reset: function ( ...attributes ) {
      // Por padrão, define atributos como todos os atuais
      if( !attributes.length ) attributes = Object.keys( this.current );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attributes.every( attribute => attribute in this.current ) );

      // Redefinição dos atributos alvo
      for( let attribute of attributes ) {
        // Identificadores
        let baseAttribute = this.base[ attribute ],
            currentAttribute = this.current[ attribute ],
            changeNumber = baseAttribute - currentAttribute,
            dataToEmit = {
              method: 'reset', attribute: attribute, changeNumber: changeNumber
            };

        // Filtra atributos já em seu valor original
        if( !changeNumber ) continue;

        // Sinaliza início da mudança de atributo
        m.events.cardContentStart[ attribute ].emit( sourceCard, dataToEmit );

        // Redefine atributo alvo
        this.current[ attribute ] += changeNumber;

        // Limpa modificadores de atributo alvo
        while( this.changes[ attribute ]?.length ) this.changes[ attribute ].pop();

        // Sinaliza fim da mudança de atributo
        m.events.cardContentEnd[ attribute ].emit( sourceCard, dataToEmit );
      }

      // Retorna atributos
      return this;
    },
    // Controla o modificador de um atributo que possa ser atualizado via a conjunção de seus modificadores
    controlModifier: function ( attribute, number, modifierName, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( attribute in this.changes );
        m.oAssert( Number.isInteger( number ) );
        m.oAssert( modifierName && typeof modifierName == 'string' );
      }

      // Identificadores
      var attributeModifiers = this.changes[ attribute ],
          modifierIndex = attributeModifiers.findIndex( modifierObject => modifierObject.name == modifierName );

      // Caso modificador passado já exista, remove o antigo
      if( modifierIndex != -1 ) attributeModifiers.splice( modifierIndex, 1 );

      // Caso haja um valor associado ao modificador passado, adiciona modificador
      if( number ) attributeModifiers.push( { name: modifierName, value: number  } );

      // Atualiza valor do atributo alvo
      this.update( attribute, context );

      // Retorna atributos
      return this;
    },
    // Atualiza um atributo que possa ser atualizado via a conjunção de seus modificadores
    update: function ( attribute, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( attribute in this.changes );

      // Identificadores
      var [ baseValue, currentValue ] = [ this.base[ attribute ], this.current[ attribute ] ],
          attributeModifiers = this.changes[ attribute ],
          modifiersSum = attributeModifiers.reduce( ( accumulator, current ) => accumulator += current.value, 0 ),
          attributeCeil = attribute == 'initiative' ? 15 : 10,
          finalResult = Math.max( 0, Math.min( attributeCeil, baseValue + modifiersSum ) ),
          valueToApply = currentValue - finalResult;

      // Realiza atualização do valor do atributo alvo
      valueToApply < 0 ? this.increase( Math.abs( valueToApply ), attribute, context ) : this.reduce( valueToApply, attribute, context );

      // Retorna atributos
      return this;
    }
  }
};
