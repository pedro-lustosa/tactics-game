// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinFighting = function () {
  // Identificadores
  var sourceCard = this,
      fightingObject = {};

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Armas
  fightingObject.weapons = {};

  // Aumenta habilidade em arma segundo íntegro passado, até valor máximo de 10
  fightingObject.increase = function ( number, weapon, isRaceSkill = false ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( Number.isInteger( number ) && number >= 0 );
      m.oAssert( weapon in this.weapons );
    }

    // Identificadores
    var properties = [ 'skill' ].concat( isRaceSkill ? [ 'raceSkill' ] : [] );

    // Aumento da habilidade nas propriedades pertinentes
    for( let property of properties ) this.weapons[ weapon ][ property ] = Math.min( this.weapons[ weapon ][ property ] + number, 10 );

    // Retorna habilidades de luta
    return this;
  }

  // Diminui habilidade em arma segundo íntegro passado, até valor mínimo de 1
  fightingObject.decrease = function ( number, weapon, isRaceSkill = false ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( Number.isInteger( number ) && number >= 0 );
      m.oAssert( weapon in this.weapons );
    }

    // Identificadores
    var properties = [ 'skill' ].concat( isRaceSkill ? [ 'raceSkill' ] : [] );

    // Redução da habilidade nas propriedades pertinentes
    for( let property of properties ) this.weapons[ weapon ][ property ] = Math.max( this.weapons[ weapon ][ property ] - number, 1 );

    // Retorna habilidades de luta
    return this;
  }

  // Retorna objeto de luta
  return fightingObject;
};
