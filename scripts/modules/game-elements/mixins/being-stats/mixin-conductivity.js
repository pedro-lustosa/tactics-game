// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinConductivity = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Condutividade natural
    natural: {
      base: 0, current: 0
    },
    // Condutividade atual
    current: 0,
    // Fonte de condutividade atualmente vigente
    source: null,
    // Fontes de condutividade vinculadas
    sources: [],
    // Adiciona fonte de condutividade
    add: function ( source, value = source?.content?.stats?.effectivity?.current?.conductivity ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ m.Garment, m.Spell ].some( constructor => source instanceof constructor ) );
        m.oAssert( value >= 0 && value <= 3 );
        m.oAssert( Number.isInteger( value ) );
      }

      // Não executa função caso fonte passada já esteja no arranjo de fontes
      if( this.sources.map( object => object.source ).includes( source ) ) return false;

      // Adiciona fonte de condutividade ao arranjo
      this.sources.push( { source: source, value: value } );

      // Atualiza condutividade atual de alvo
      return mixinConductivity.updateConductivity( sourceCard );
    },
    // Remove fonte de condutividade
    remove: function ( source ) {
      // Identificadores
      var sourceIndex = this.sources.map( object => object.source ).indexOf( source );

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ m.Garment, m.Spell ].some( constructor => source instanceof constructor ) );

      // Não executa função caso fonte passada não esteja no arranjo de fontes
      if( sourceIndex == -1 ) return false;

      // Remove fonte de condutividade do arranjo
      this.sources.splice( sourceIndex, 1 );

      // Atualiza condutividade atual de alvo
      return mixinConductivity.updateConductivity( sourceCard );
    }
  }
};

// Métodos não configuráveis de 'mixinConductivity'
Object.defineProperties( mixinConductivity, {
  // Atualiza condutividade de alvo
  updateConductivity: {
    value: function ( sourceCard ) {
      // Identificadores
      var conductivity = sourceCard.content.stats.effectivity.conductivity,
          newSource = null,
          [ formerValue, newValue ] = [ conductivity.current, conductivity.natural.base ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( sourceCard instanceof m.Being );

      // Redefine condutividade natural atual
      conductivity.natural.current = conductivity.natural.base;

      // Itera por fontes de condutividade do alvo
      for( let sourceObject of conductivity.sources ) {
        // Identificadores
        let { source, value } = sourceObject;

        // Para caso fonte seja um traje
        if( source instanceof m.Garment ) {
          // Identificadores
          let garmentRank = Object.entries( sourceCard.garments ).find( entry => entry[1].source == source )[0],
              garmentEffectivity = source.content.stats.effectivity.current;

          // Para caso traje seja primário e cubra todo o vinculante
          if( garmentRank == 'primary' && garmentEffectivity.coverage == 10 ) {
            // Altera nova fonte e valor da condutividade
            [ newSource, newValue ] = [ source, value ];

            // Encerra iteração
            break;
          }

          // Atualiza condutividade atual para a alvo caso valor desta seja maior, ou igual e com seu traje sendo o primário
          if( value > newValue || ( value == newValue && garmentRank == 'primary' ) ) [ newSource, newValue ] = [ source, value ];
        }
        // Para caso fonte seja uma magia
        else if( source instanceof m.Spell ) {
          // Atualiza condutividade natural atual
          conductivity.natural.current = value;

          // Filtra casos em que condutividade de um traje é maior ou igual do que a da magia
          if( Object.values( sourceCard.garments ).some( rank => rank.source?.content.stats.effectivity.current.conductivity >= value ) ) continue;

          // Altera nova fonte e valor da condutividade
          [ newSource, newValue ] = [ source, value ];
        }
      }

      // Altera fonte da condutividade
      conductivity.source = newSource;

      // Encerra função caso condutividade de alvo não precise ser alterada
      if( formerValue == newValue ) return conductivity.current;

      // Sinaliza início da mudança da condutividade
      m.events.cardContentStart.conductivity.emit( sourceCard, { newValue: newValue } );

      // Altera valor da condutividade
      conductivity.current = newValue;

      // Sinaliza fim da mudança da condutividade
      m.events.cardContentEnd.conductivity.emit( sourceCard, { formerValue: formerValue } );

      // Retorna condutividade atual do alvo
      return conductivity.current;
    }
  }
} );
