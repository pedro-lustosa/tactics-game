// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinChanneling = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Sendas
    paths: {
      metoth: {
        name: 'metoth', raceSkill: 0, skill: 0, polarity: 'apolar', spells: []
      },
      enoth: {
        name: 'enoth', raceSkill: 0, skill: 0, polarity: '', spells: []
      },
      powth: {
        name: 'powth', raceSkill: 0, skill: 0, polarity: 'apolar', spells: []
      },
      voth: {
        name: 'voth', raceSkill: 0, skill: 0, polarity: '', spells: []
      },
      faoth: {
        name: 'faoth', raceSkill: 0, skill: 0, polarity: '', spells: []
      }
    },
    // Mana
    mana: {
      // Mana original
      base: {},
      // Mana atual
      current: {},
      // Aumenta valor atual de mana de uma senda, mas sem que ele ultrapasse seu valor original
      restore: function ( number, path ) {
        // Caso ente seja um ser astral ou esteja sendo representado por um ser astral, converte operação em uma restauração de energia
        if( sourceCard instanceof m.AstralBeing || sourceCard.astralForm )
          return setTimeout( () =>
            ( sourceCard.astralForm ?? sourceCard ).content.stats.attributes.restore(number, 'energy', { triggeringCard: sourceCard.astralForm ?? sourceCard })
          );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( Number.isInteger( number ) && number >= 0 );
          m.oAssert( path in this.current && path != 'total' );
        }

        // Identificadores
        var manaCeil = this.base[ path ],
            currentMana = this.current[ path ],
            changeNumber = Math.min( manaCeil - currentMana, number ),
            dataToEmit = {
              method: 'restore', path: path, changeNumber: changeNumber
            };

        // Não executa função caso não haja um valor a ser restaurado
        if( changeNumber <= 0 ) return this;

        // Sinaliza início da mudança de mana
        m.events.cardContentStart.mana.emit( sourceCard, dataToEmit );

        // Atualiza valor do mana alvo
        this.current[ path ] += changeNumber;

        // Atualiza valor do mana total
        this.current.total = Object.values( this.current ).reduce( ( accumulator, current ) => Math.max( accumulator, current ) );

        // Sinaliza fim da mudança de mana
        m.events.cardContentEnd.mana.emit( sourceCard, dataToEmit );

        // Retorna mana
        return this;
      },
      // Diminui valor atual de mana de uma senda segundo íntegro passado, até o valor mínimo de 0
      reduce: function ( number, path ) {
        // Caso ente seja um ser astral ou esteja sendo representado por um ser astral, converte operação em uma redução de energia
        if( sourceCard instanceof m.AstralBeing || sourceCard.astralForm )
          return setTimeout( () =>
            ( sourceCard.astralForm ?? sourceCard ).content.stats.attributes.reduce( number, 'energy', { triggeringCard: sourceCard.astralForm ?? sourceCard } )
          );

        // Validação
        if( m.app.isInDevelopment ) {
          m.oAssert( Number.isInteger( number ) && number >= 0 );
          m.oAssert( path in this.current && path != 'total' );
        }

        // Identificadores
        var manaFloor = 0,
            currentMana = this.current[ path ],
            changeNumber = Math.min( currentMana - manaFloor, number ),
            dataToEmit = {
              method: 'reduce', path: path, changeNumber: -changeNumber
            };

        // Não executa função caso não haja um valor a ser reduzido
        if( changeNumber <= 0 ) return this;

        // Sinaliza início da mudança de mana
        m.events.cardContentStart.mana.emit( sourceCard, dataToEmit );

        // Atualiza valor do mana alvo
        this.current[ path ] -= changeNumber;

        // Atualiza valor do mana total
        this.current.total -= changeNumber;

        // Atualiza mana disponível das sendas
        for( let manaPath in this.current ) this.current[ manaPath ] = Math.min( this.current[ manaPath ], this.current.total );

        // Sinaliza fim da mudança de mana
        m.events.cardContentEnd.mana.emit( sourceCard, dataToEmit );

        // Retorna mana
        return this;
      },
      // Redefine valor atual de mana das sendas alvos para seu valor original
      reset: function ( ...paths ) {
        // Caso ente seja um ser astral ou esteja sendo representado por um ser astral, converte operação em uma redefinição de sua energia
        if( sourceCard instanceof m.AstralBeing || sourceCard.astralForm )
          return setTimeout( () => ( sourceCard.astralForm ?? sourceCard ).content.stats.attributes.reset( 'energy' ) );

        // Por padrão, define sendas como todas as atuais
        if( !paths.length ) paths = Object.keys( this.current ).filter( path => path != 'total' );

        // Validação
        if( m.app.isInDevelopment ) m.oAssert( paths.every( path => path in this.current && path != 'total' ) );

        // Redefinição do mana das sendas alvo
        for( let path of paths ) {
          // Identificadores
          let baseMana = this.base[ path ],
              currentMana = this.current[ path ],
              changeNumber = baseMana - currentMana,
              dataToEmit = {
                method: 'reset', path: path, changeNumber: changeNumber
              };

          // Filtra sendas cujo mana já está em seu valor original
          if( !changeNumber ) continue;

          // Sinaliza início da mudança de mana
          m.events.cardContentStart.mana.emit( sourceCard, dataToEmit );

          // Redefine mana alvo
          this.current[ path ] += changeNumber;

          // Atualiza mana total
          this.current.total = Math.max( this.current[ path ], this.current.total );

          // Sinaliza fim da mudança de mana
          m.events.cardContentEnd.mana.emit( sourceCard, dataToEmit );
        }

        // Retorna mana
        return this;
      }
    },
    // Retorna todas as magias pertencidas pela carta alvo
    getOwnedSpells: function () {
      return Object.values( this.paths ).flatMap( path => path.spells );
    }
  }
};

// Métodos não configuráveis de 'mixinChanneling'
Object.defineProperties( mixinChanneling, {
  // Define configurações finais de canalização para o canalizador passado
  close: {
    value: function ( channeler ) {
      // Identificadores
      var { paths, mana } = channeler.content.stats.combativeness.channeling;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( channeler instanceof m.Being );
        m.oAssert( Object.values( paths ).every( path => path.skill >= 0 && path.skill <= 5 ) );
        m.oAssert( Object.values( paths ).every( path => !path.skill || [ 'positive', 'apolar', 'negative' ].includes( path.polarity ) ) );
      }

      // Não executa função caso um mana total já tenha sido definido
      if( mana.base.total ) return;

      // Determinação do mana das sendas
      for( let path in paths ) mana.base[ path ] = mana.current[ path ] = paths[ path ].skill * 4;

      // Determinação do mana total
      mana.base.total = mana.current.total = Object.values( mana.base ).reduce( ( accumulator, current ) => Math.max( accumulator, current ) );

      // Encerra função caso canalizador não seja um humanoide
      if( !( channeler instanceof m.Humanoid ) ) return;

      // Caso canalizador seja um vothe, atribui capacidade de convocar
      if( paths.voth.skill ) Object.oAssignByCopy( channeler, m.mixinSummonable.call( channeler ) );

      // Para caso canalizador seja um powthe
      if( paths.powth.skill ) {
        // Identificadores
        let channelerManeuvers = channeler.content.stats.effectivity.maneuvers;

        // Atribuição de manobra 'Raio de Mana' às manobras naturais do canalizador
        for( let key of [ 'base', 'current' ] ) channelerManeuvers.natural[ key ].attacks.unshift( new m.ManeuverManaBolt( { source: channeler } ) );

        // Atualização das fontes de manobras do canalizador, para que possivelmente contemplem manobra adicionada
        for( let rank of [ 'primary', 'secondary' ] ) {
          // Filtra fontes de manobras com equipamentos
          if( channelerManeuvers[ rank ].source ) continue;

          // Atualiza fonte de manobras alvo
          channelerManeuvers.change( rank, 'natural' );
        }

        // Adição de evento para atualização dos pontos de "Raio de Mana" após mudança de pontos de mana
        setTimeout( () => m.events.cardContentEnd.mana.add( channeler, mixinChanneling.updateManaBoltPoints, { context: mixinChanneling } ) );
      }
    }
  },
  // Atualiza pontos disponíveis de ataque "Raio de Mana" após redução do mana de Powth
  updateManaBoltPoints: {
    value: function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget: channeler } = eventData,
          channelerMana = channeler?.content.stats.combativeness?.channeling?.mana.current;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'card-content' && eventType == 'mana' && channeler instanceof m.Being );
        m.oAssert( channelerMana );
      }

      // Captura fonte de manobras com raio de mana
      let channelerManeuvers = channeler.content.stats.effectivity.maneuvers,
          manaBoltManeuverSource = [ 'primary', 'secondary' ].map( rank => channelerManeuvers[ rank ] ).find(
            maneuverSource => maneuverSource.attacks.some( attack => attack instanceof m.ManeuverManaBolt )
          );

      // Não executa função caso canalizador não esteja com 'Raio de Mana' disponível
      if( !manaBoltManeuverSource ) return;

      // Ajusta pontos disponíveis para a manobra raio de mana
      manaBoltManeuverSource.currentPoints = Math.max( 0, Math.min( 10 - manaBoltManeuverSource.spentPoints, channelerMana.powth ) );
    }
  }
} );
