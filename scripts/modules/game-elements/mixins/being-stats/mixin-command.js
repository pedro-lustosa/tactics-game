// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinCommand = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Being );

  // Objeto da mistura
  return {
    // Escopos do comando
    scopes: ( function () {
      // Identificadores
      var scopes = {
        // Escopo de criaturas
        creature: {
          // Escopo de raças
          race: {
            human: {},
            orc: {},
            elf: {},
            dwarf: {},
            halfling: {},
            gnome: {},
            goblin: {},
            ogre: {}
          },
          // Escopo tipos de atuação
          roleType: {
            melee: {},
            ranged: {},
            magical: {}
          },
          // Escopo de níveis de experiência
          experienceLevel: {
            1: {},
            2: {},
            3: {},
            4: {}
          }
        },
        // Escopo de equipamentos
        item: {
          // Escopo de categorias
          grade: {
            ordinary: {},
            masterpiece: {},
            artifact: {},
            relic: {}
          }
        },
        // Escopo de magias
        spell: {}
      },
      scopeStats = {
        min: 0, max: 0
      };

      // Itera por superescopos
      for( let superScope in scopes ) {
        // Itera por subescopos
        for( let subScope in scopes[ superScope ] ) {
          // Atribui a propriedades de subescopos as estatísticas de escopo
          Object.keys( scopes[ superScope ][ subScope ] ).forEach( key => scopes[ superScope ][ subScope ][ key ] = scopeStats );
        }

        // Atribui a superescopo as estatísticas de escopo
        scopes[ superScope ].total = scopeStats;

        // Adiciona a estatísticas de escopo de superescopos propriedade para restrições especiais
        scopes[ superScope ].total.constraint = {
          check: ( ...cards ) => true, description: ''
        };
      }

      // Salva valor original do escopo de quantidade máxima de humanos
      scopes.creature.race.human.baseMax = scopes.creature.race.human.max;

      // Retorna escopos
      return scopes;
    } )(),
    // Moedas disponíveis para o conjunto de baralhos do comandante alvo
    availableCoins: {
      mainDeck: 0, sideDeck: 0
    },
    // Atribui valores aos escopos de comando
    setScopes: function ( scopes ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( scopes.constructor == Object );
        m.oAssert( Object.keys( scopes ).every( superScope => Object.keys( this.scopes ).includes( superScope ) ) );
        m.oAssert( Object.keys( scopes.creature ?? {} ).every( subScope => Object.keys( this.scopes.creature ).includes( subScope ) ) );
        m.oAssert( Object.keys( scopes.item ?? {} ).every( subScope => Object.keys( this.scopes.item ).includes( subScope ) ) );
        m.oAssert( Object.keys( scopes.spell ?? {} ).every( subScope => Object.keys( this.scopes.spell ).includes( subScope ) ) );

        // Para caso haja escopos de criaturas
        if( scopes.creature ) {
          m.oAssert(
            Object.keys( scopes.creature.race ?? {} ).every( value => Object.keys( this.scopes.creature.race ).includes( value ) )
          );
          m.oAssert(
            Object.keys( scopes.creature.roleType ?? {} ).every( value => Object.keys( this.scopes.creature.roleType ).includes( value ) )
          );
          m.oAssert(
            Object.keys( scopes.creature.experienceLevel ?? {} ).every( value => Object.keys( this.scopes.creature.experienceLevel ).includes( value ) )
          );
        }

        // Para caso haja escopos de equipamentos
        if( scopes.item ) {
          m.oAssert(
            Object.keys( scopes.item.grade ?? {} ).every( value => Object.keys( this.scopes.item.grade ).includes( value ) )
          );
        }
      }

      // Itera por escopos passados
      for( let superScope in scopes ) {
        // Atribuição de propriedades de superescopos
        if( scopes[ superScope ].total ) {
          // Identificadores
          let scopeTotal = this.scopes[ superScope ].total;

          // Valores mínimo e máximo
          for( let property of [ 'min', 'max' ] ) scopeTotal[ property ] = scopes[ superScope ].total[ property ] ?? scopeTotal[ property ];

          // Validação da restrição especial, quando existente
          scopeTotal.constraint.check = scopes[ superScope ].total.constraint?.check ?? scopeTotal.constraint.check;
        }

        // Itera por subescopos
        for( let subScope in scopes[ superScope ] ) {
          // Filtra escopo do total de cartas
          if( subScope == 'total' ) continue;

          // Atribuição de propriedades de subescopos
          Object.keys( scopes[ superScope ][ subScope ] ).forEach(
            key => Object.assign( this.scopes[ superScope ][ subScope ][ key ], scopes[ superScope ][ subScope ][ key ] )
          );
        }
      }

      // Ajusta valor original do escopo de quantidade máxima de humanos
      this.scopes.creature.race.human.baseMax = this.scopes.creature.race.human.max;
    }
  }
};
