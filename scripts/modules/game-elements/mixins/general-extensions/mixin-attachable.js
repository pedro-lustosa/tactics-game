// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinAttachable = function () {
  // Captura componente alvo
  var sourceCard = this;

  // Objeto da mistura
  return {
    // Vinculante do componente
    attacher: null,
    // Vinculados do componente
    attachments: {
      embedded: [], external: [], mana: 0
    },
    // Limite de vinculados do componente
    maxAttachments: {
      weapon: 0, garment: 0, implement: 0, spell: 0, mana: 0
    },
    // Vincula vinculado ao componente
    attach: function ( attachment, attachmentType = 'external' ) {
      // Identificadores
      var formerAttacher = attachment.attacher;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'embedded', 'external', 'mana' ].includes( attachmentType ) );
        m.oAssert(
          [ 'external', 'embedded' ].includes( attachmentType ) && attachment instanceof m.Card && this instanceof attachment.content.typeset.attachability ||
          attachmentType == 'mana' && Number.isInteger( attachment ) && attachment >= 0
        );
      }

      // Delega operação de vinculação em função de se vinculado é uma carta ou não
      attachment instanceof m.Card ? attachCard.call( this ) : attachMana.call( this );

      // Retorna componente
      return this;

      // Vincula carta ao componente
      function attachCard() {
        // Encerra função se vinculado já estiver no componente
        if( this.attachments[ attachmentType ].includes( attachment ) ) return;

        // Sinaliza início da mudança de vinculante
        m.events.attachabilityChangeStart.attacherIn.emit( attachment, { newAttacher: this } );

        // Sinaliza início da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeStart.attachmentIn.emit( eventTarget, { attacher: this, attachment: attachment } );

        // Relaciona vinculado ao componente
        attachment.attacher = this;

        // Adiciona vinculado ao componente
        this.attachments[ attachmentType ].push( attachment );

        // Sinaliza fim da mudança de vinculante
        m.events.attachabilityChangeEnd.attacherIn.emit( attachment, { formerAttacher: formerAttacher } );

        // Sinaliza fim da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeEnd.attachmentIn.emit( eventTarget, { attacher: this, attachment: attachment } );
      }

      // Vincula mana ao componente
      function attachMana() {
        // Encerra função se não houver mana a ser vinculado
        if( !attachment ) return;

        // Encerra função caso casa já esteja com quantidade máxima de mana lhe vinculável
        if( this.attachments.mana >= this.maxAttachments.mana ) return;

        // Sinaliza início da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeStart.attachmentIn.emit( eventTarget, { attacher: this, attachment: attachment } );

        // Adiciona mana ao componente, até um máximo igual ao máximo de mana lhe vinculável
        this.attachments.mana = Math.min( this.attachments.mana + attachment, this.maxAttachments.mana );

        // Sinaliza fim da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeEnd.attachmentIn.emit( eventTarget, { attacher: this, attachment: attachment } );
      }
    },
    // Desvincula vinculado do componente
    detach: function ( attachment, attachmentType = 'external' ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ 'embedded', 'external', 'mana' ].includes( attachmentType ) );
        m.oAssert(
          [ 'external', 'embedded' ].includes( attachmentType ) && attachment instanceof m.Card ||
          attachmentType == 'mana' && Number.isInteger( attachment ) && attachment >= 0
        );
      }

      // Delega operação de desvinculação em função de se vinculado é uma carta ou não
      attachment instanceof m.Card ? detachCard.call( this ) : detachMana.call( this );

      // Retorna componente
      return this;

      // Desvincula carta do componente
      function detachCard() {
        // Identificadores
        var targetIndex = this.attachments[ attachmentType ].indexOf( attachment );

        // Encerra função caso vinculado não esteja no componente
        if( targetIndex == -1 ) return;

        // Sinaliza início da mudança de vinculante
        m.events.attachabilityChangeStart.attacherOut.emit( attachment );

        // Sinaliza início da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeStart.attachmentOut.emit( eventTarget, { attacher: this, attachment: attachment } );

        // Desrelaciona vinculado do componente
        attachment.attacher = null;

        // Remove vinculado do componente
        this.attachments[ attachmentType ].splice( targetIndex, 1 );

        // Sinaliza fim da mudança de vinculante
        m.events.attachabilityChangeEnd.attacherOut.emit( attachment, { formerAttacher: this } );

        // Sinaliza fim da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeEnd.attachmentOut.emit( eventTarget, { attacher: this, attachment: attachment } );
      }

      // Desvincula mana do componente
      function detachMana() {
        // Encerra função se não houver mana a ser desvinculado
        if( !attachment ) return;

        // Encerra função caso casa esteja sem mana
        if( !this.attachments.mana ) return;

        // Sinaliza início da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeStart.attachmentOut.emit( eventTarget, { attacher: this, attachment: attachment } );

        // Subtrai mana do componente, até um mínimo de 0
        this.attachments.mana = Math.max( this.attachments.mana - attachment, 0 );

        // Sinaliza fim da mudança de vinculado
        for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
          m.events.attachabilityChangeEnd.attachmentOut.emit( eventTarget, { attacher: this, attachment: attachment } );
      }
    },
    // Retorna todas as cartas vinculadas ao componente, diretamente ou não
    getAllCardAttachments: function ( attachmentType = 'all' ) {
      // Identificadores
      var attachmentsArray = [],
          attachmentTypes = attachmentType == 'embedded' ? [ 'embedded' ] : [ 'embedded', 'external' ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( [ 'all', 'embedded', 'external' ].includes( attachmentType ) );

      // Itera por tipos de vinculação
      for( let type of attachmentTypes ) {
        // Itera por vinculados do tipo de vinculação alvo
        for( let attachment of this.attachments[ type ] ) {
          // Adiciona vinculado ao arranjo de vinculados, quando aplicável
          switch( attachmentType ) {
            case 'embedded':
              if( attachment.isEmbedded ) attachmentsArray.push( attachment );
              break;
            case 'external':
              if( !attachment.isEmbedded ) attachmentsArray.push( attachment );
              break;
            default:
              attachmentsArray.push( attachment );
          }

          // Adiciona ao arranjo de vinculados os vinculados do vinculado alvo, quando aplicável
          attachmentsArray = attachmentsArray.concat( attachment.getAllCardAttachments?.( attachmentType ) ?? [] );
        }
      }

      // Retorna arranjo de vinculados
      return attachmentsArray;
    },
    // Atualiza estado de cartas vinculadas ao componente
    updateCardAttachments: function () {
      // Delega atualização de cartas vinculadas segundo tipo primitivo do vinculante
      return this instanceof m.Spell ? updateSpellAttachments.call( this ) : updateAttachments.call( this );

      // Funções

      /// Atualiza vinculados de entes e equipamentos
      function updateAttachments() {
        // Define função para controlar vinculados que sejam equipamentos
        var updateItem = ( function () {
              // Define função de atualização em função de atividade do componente
              switch( this.activity ) {
                case 'unlinked':
                  return ( attachment ) => attachment.unlink();
                case 'active':
                  return ( attachment ) => attachment.activate( this, { triggeringCard: this } );
                default:
                  return ( attachment ) => attachment.inactivate( this.activity, { triggeringCard: this } );
              }
            } ).call( this );

        // Itera por tipos de vinculação
        for( let type of [ 'embedded', 'external' ] ) {
          // Cria arranjo de cartas atualmente vinculadas ao componente
          let attachments = this.attachments[ type ].slice();

          // Itera por vinculados
          for( let attachment of attachments ) {
            // Caso vinculado seja um equipamento, atualiza seu estado
            if( attachment instanceof m.Item ) updateItem( attachment )

            // Do contrário, caso vinculado seja uma magia
            else if( attachment instanceof m.Spell ) {
              // Filtra magias cujo vinculante esteja ativo
              if( this.activity == 'active' ) continue;

              // Desusa magia, desde que o vinculante não esteja suspenso ou a magia não seja permanente
              if( this.activity != 'suspended' || attachment.content.typeset.duration != 'permanent' ) attachment.disuse();
            }
          }
        }
      }

      /// Atualiza vinculados de magias
      function updateSpellAttachments() {
        // Desconsidera operação caso magia esteja em uso
        if( this.isInUse ) return;

        // Cria arranjo de cartas atualmente vinculadas à magia
        let attachments = this.attachments.external.slice();

        // Desusa vinculados
        attachments.forEach( attachment => attachment.disuse() );
      }
    }
  }
};
