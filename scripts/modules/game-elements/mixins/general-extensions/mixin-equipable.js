// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinEquipable = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Objeto da mistura
  return {
    // Equipa equipamento passado
    equip: function ( item, attachmentType = 'embedded' ) {
      // Identificadores
      var itemOwner = ( function () {
            // Identificadores
            var itemOwner = this;

            // Busca dono do equipamento
            while( itemOwner && !( itemOwner instanceof m.Being ) ) itemOwner = itemOwner.attacher;

            // Retorna dono do equipamento
            return itemOwner;
          } ).call( this );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( item instanceof m.Item );
        m.oAssert( !item.isEmbedded );
        m.oAssert( [ 'embedded', 'external' ].includes( attachmentType ) );
        m.oAssert( itemOwner instanceof m.Being );

        // Para vinculados externos
        if( attachmentType == 'external' ) {
          m.oAssert( this.activity == 'active' );
          m.oAssert( ( this.deck ?? this.summoner.deck ) == item.deck );
        }

        // Para vinculados embutidos
        else if( attachmentType == 'embedded' ) {
          m.oAssert( item.activity == 'unlinked' );
          m.oAssert( !item.deck );
        }
      }

      // Valida vinculante a receber equipamento alvo, e encerra função caso ele tenha sido invalidado
      if( !m.Item.validateAttacher( item, this ) ) return false;

      // Sinaliza início da mudança de equipamento
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
        m.events.cardContentStart.itemIn.emit( eventTarget, { attacher: this, item: item } );

      // Desequipa equipamento de vinculante atual, se existente
      item.attacher?.unequip( item );

      // Registra dono do equipamento
      item.owner = itemOwner;

      // Vincula equipamento a novo vinculante
      this.attach( item, attachmentType );

      // Adiciona pontuação de peso do equipamento à contagem de peso de seu dono
      item.owner.weightCount.current += m.Item.getWeightScore( item );

      // Para caso equipamento seja um vinculado embutido
      if( attachmentType == 'embedded' ) {
        // Identifica equipamento como um vinculado embutido
        item.isEmbedded = true;

        // Define convocante enquanto ente a que equipamento se vincula, ou, se aplicável, a seu convocante
        item.summoner = item.owner.summoner ?? item.owner;
      }

      // Sinaliza fim da mudança de equipamento
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
        m.events.cardContentEnd.itemIn.emit( eventTarget, { attacher: this, item: item } );

      // Retorna equipamento vinculado
      return item;
    },
    // Desequipa equipamento passado
    unequip: function ( item ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( item instanceof m.Item );

      // Não executa função caso equipamento não esteja em vinculante alvo
      if( item.attacher != this ) return item;

      // Sinaliza início da mudança de equipamento
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
        m.events.cardContentStart.itemOut.emit( eventTarget, { attacher: this, item: item } );

      // Desusa equipamento
      item.disuse();

      // Desvincula equipamento
      this.detach( item, item.isEmbedded ? 'embedded' : 'external' );

      // Retira pontuação de peso do equipamento da contagem de peso de seu dono
      item.owner.weightCount.current -= m.Item.getWeightScore( item );

      // Força indicação de que equipamento não está embutido
      item.isEmbedded = false;

      // Nulifica dono do equipamento, e seu eventual convocante
      item.summoner = item.owner = null;

      // Sinaliza fim da mudança de equipamento
      for( let eventTarget = this; eventTarget; eventTarget = eventTarget.attacher )
        m.events.cardContentEnd.itemOut.emit( eventTarget, { attacher: this, item: item } );

      // Retorna equipamento desvinculado
      return item;
    },
    // Redefine equipamentos embutidos
    resetEmbeddedItems: function () {
      // Identificadores
      var copiedBeing = this.copy(),
          baseEmbeddedAttachments = copiedBeing.attachments.embedded,
          currentEmbeddedAttachments = this.attachments.embedded;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( baseEmbeddedAttachments.length >= currentEmbeddedAttachments.length );
        m.oAssert(
          baseEmbeddedAttachments.filter( card => !( card instanceof m.Item ) ).length ==
          currentEmbeddedAttachments.filter( card => !( card instanceof m.Item ) ).length
        );
      }

      // Itera por vinculados embutidos originais do ente
      for( let x = 0; x < baseEmbeddedAttachments.length; x++ ) {
        // Identificadores
        let baseAttachment = baseEmbeddedAttachments[ x ],
            subBaseAttachments = baseAttachment.attachments.embedded;

        // Filtra vinculados que não sejam equipamentos
        if( !( baseAttachment instanceof m.Item ) ) continue;

        // Para caso não haja mais um equipamento como o original no índice esperado de vinculados embutidos do ente
        if( currentEmbeddedAttachments[ x ]?.name != baseAttachment.name ) {
          // Adiciona equipamento original novamente ao arranjo de vinculados do ente, e o captura
          let restoredItem = this.equip( baseAttachment.copy() );

          // Ajusta posição do equipamento adicionado no arranjo de vinculados embutidos do ente
          currentEmbeddedAttachments.splice( x, 0, currentEmbeddedAttachments.pop() );

          // Ajusta atividade de equipamento adicionado, para que corresponda à de seu ente
          this.activity == 'active' ? restoredItem.activate( this ) : this.activity != 'unlinked' ? restoredItem.inactivate( this.activity ) : null;
        }

        // Itera por vinculados do equipamento alvo
        for( let y = 0; y < subBaseAttachments.length; y++ ) {
          // Identificadores
          let subBaseAttachment = subBaseAttachments[ y ],
              currentAttacher = currentEmbeddedAttachments[ x ];

          // Filtra vinculados que não sejam equipamentos
          if( !( subBaseAttachment instanceof m.Item ) ) continue;

          // Filtra vinculados que o equipamento embutido atual do ente já tenha no índice previsto
          if( currentAttacher.attachments.embedded[ y ]?.name == subBaseAttachment.name ) continue;

          // Adiciona equipamento original novamente ao arranjo de vinculados do equipamento alvo, e o captura
          let restoredItem = currentAttacher.equip( subBaseAttachment.copy() );

          // Ajusta posição do equipamento adicionado no arranjo de vinculados embutidos do equipamento alvo
          currentAttacher.attachments.embedded.splice( y, 0, currentAttacher.attachments.embedded.pop() );

          // Ajusta atividade de equipamento adicionado, para que corresponda à de seu vinculante
          currentAttacher.activity == 'active' ? restoredItem.activate( currentAttacher ) :
          currentAttacher.activity != 'unlinked' ? restoredItem.inactivate( currentAttacher.activity ) : null;
        }
      }
    }
  }
};
