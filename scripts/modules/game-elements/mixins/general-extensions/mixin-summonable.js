// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinSummonable = function () {
  // Captura carta alvo
  var sourceCard = this;

  // Objeto da mistura
  return {
    // Convocações
    summons: [],
    // Convoca criatura
    summon: function ( creature, slotName ) {
      // Identificadores
      var { summons } = this,
          creatureRace = creature?.content.typeset.race;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( creature instanceof m.Being );
        m.oAssert( creature.isToken );
        m.oAssert( creature.summoner == this );
        m.oAssert( !creature.owner || creature.owner == this );
        m.oAssert( this.activity == 'active' && creature.activity == 'unlinked' );
        m.oAssert( !summons[ creatureRace ]?.includes( creature ) );
      }

      // Sinaliza início da mudança de convocação
      for( let eventTarget of [ this, creature ] )
        m.events.summonChangeStart.summonIn.emit( eventTarget, { summoner: this, summon: creature } );

      // Ativa criatura, ou encerra função caso isso não tenha sido possível
      if( !creature.activate( slotName, { triggeringCard: this } ) ) return false;

      // Adiciona criatura às convocadas de seu convocante
      summons.push( creature );

      // Cria arranjo de convocações para raça da criatura, se ainda não existente
      summons[ creatureRace ] ??= [];

      // Adiciona criatura ao arranjo de convocações para sua raça
      summons[ creatureRace ].push( creature );

      // Se criatura for um pertence, adiciona evento para a alhear quando seu dono for inativado
      if( creature.owner ) m.events.cardActivityEnd.inactive.add( this, () => creature.unlink(), { once: true } );

      // Adiciona evento para desconvocar criatura ao ser alheada
      m.events.cardActivityEnd.unlinked.add( creature, this.unsummon, { context: this, once: true } );

      // Sinaliza fim da mudança de convocação
      for( let eventTarget of [ this, creature ] )
        m.events.summonChangeEnd.summonIn.emit( eventTarget, { summoner: this, summon: creature } );

      // Retorna convocações do convocante
      return summons;
    },
    // Desconvoca criatura convocada
    unsummon: function ( eventData = {} ) {
      // Identificadores
      var { summons } = this,
          creature = eventData.eventTarget,
          creatureRace = creature?.content.typeset.race;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( creature instanceof m.Being );
        m.oAssert( creature.isToken );
        m.oAssert( summons[ creatureRace ].includes( creature ) );
      }

      // Sinaliza início da mudança de convocação
      for( let eventTarget of [ this, creature ] )
        m.events.summonChangeStart.summonOut.emit( eventTarget, { summoner: this, summon: creature } );

      // Remove criatura das convocações de seu convocante
      summons.splice( summons.indexOf( creature ), 1 );

      // Remove criatura das convocações de seu convocante para sua raça
      summons[ creatureRace ].splice( summons[ creatureRace ].indexOf( creature ), 1 );

      // Sinaliza fim da mudança de convocação
      for( let eventTarget of [ this, creature ] )
        m.events.summonChangeEnd.summonOut.emit( eventTarget, { summoner: this, summon: creature } );

      // Retorna convocações do convocante
      return summons;
    }
  }
};
