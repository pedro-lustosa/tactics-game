// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const mixinDurationable = function () {
  // Identificadores
  var spell = this;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( this instanceof m.Spell );

  // Atribui propriedades segundo duração da magia alvo
  switch( this.content.typeset.duration ) {
    case 'immediate':
      return getImmediateProperties.call( this );
    case 'sustained':
      return getSustainedProperties.call( this );
    case 'persistent':
      return getPersistentProperties.call( this );
    case 'permanent':
      return getPermanentProperties.call( this );
    default:
      return {};
  }

  // Retorna propriedades de magias imediatas
  function getImmediateProperties() {
    // Atribuição de propriedades iniciais

    /// Encerra uso da magia
    this.finishUse = function () {
      // Caso haja paradas na tela, aguarda suas conclusões antes de executar função
      while( m.GameMatch.getActiveBreaks().length )
        return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => spell.finishUse() ), { once: true } );

      // Programa desuso da magia
      return setTimeout( () => spell.disuse() );
    }
  }

  // Retorna propriedades de magias sustentadas
  function getSustainedProperties() {
    // Atribuição de propriedades iniciais

    /// Indica se magia não deve (mais) ser sustentada
    this.isToStopSustain = false;

    /// Encerra uso da magia
    this.finishUse = function () {
      // Atrasa execução da função, para que todos os eventos de uso sejam completados
      return setTimeout( ( function () {
        // Caso haja paradas na tela, aguarda suas conclusões antes de executar função
        while( m.GameMatch.getActiveBreaks().length )
          return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => spell.finishUse() ), { once: true } );

        // Desusa magia caso dono não esteja mais ativo
        if( this.owner.activity != 'active' ) return this.disuse();

        // Desusa magia caso seu efeito não esteja ativo
        if( !this.content.effects.isEnabled ) return this.disuse();

        // Desusa magia caso ela não deva ser sustentada
        if( this.isToStopSustain ) return this.disuse();

        // Desusa magia caso dono esteja com ao menos 3 marcadores de enfurecido
        if( this.owner.conditions.find( condition => condition instanceof m.ConditionEnraged )?.markers >= 3 ) return this.disuse();

        // Gera ação 'sustentar'
        let actionSustain = new m.ActionSustain( { committer: this.owner, target: this, isPropagated: true } );

        /// Quando existentes, adiciona à ação configurações oriundas da magia
        this.configSustainAction?.( actionSustain );

        // Executa ação 'sustentar'
        ( actionSustain.currentExecution = actionSustain.execute() ).next();

        // Atualiza texto suspenso de dono, quando aplicável
        m.Card.updateHoverText( { currentTarget: this.owner } );
      } ).bind( this ) );
    }

    /// Sustenta magia
    this.sustain = function ( action ) {
      // Não executa função caso magia não esteja sendo usada
      if( !this.isInUse ) return false;

      // Identificadores
      var { path, durationSubtype } = this.content.typeset,
          spellMana = this.content.stats.current.manaCost;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( action instanceof m.ActionSustain );
        m.oAssert( m.GameAction.currents.some( gameAction => gameAction == action && gameAction.committer == this.owner ) );
      }

      // Para caso magia tenha custo de mana para sustentação
      if( action.manaToAssign ) {
        // Para magias constantes, conclui ação de sustentação caso custo de uso da magia exceda seu limite após a adição do custo de mana desta sustentação
        if( durationSubtype == 'constant' && spellMana.usage + action.manaToAssign > spellMana.maxUsage ) return false;
      }

      // Sinaliza início da sustentação
      for( let eventTarget of [ this, this.owner ] ) m.events.cardUseStart.sustain.emit( eventTarget, { sustainTarget: this, action: action } );

      // Se aplicável, gasta o mana para a sustentação da magia
      if( !this.isToSkipManaSpending )
        this.owner.content.stats.combativeness.channeling.mana.reduce( this.applyPolarityBurden( action.manaToAssign ), path );

      // Incrementa custo de uso da magia
      spellMana.usage += action.manaToAssign;

      // Sinaliza fim da sustentação
      for( let eventTarget of [ this, this.owner ] ) m.events.cardUseEnd.sustain.emit( eventTarget, { sustainTarget: this, action: action } );

      // Conclui ação de sustentação caso magia não deva mais ser sustentada
      if( this.isToStopSustain ) return false;

      // Para caso magia tenha custo de mana para sustentação
      if( action.manaToAssign ) {
        // Para magias de pico, conclui ação de sustentação caso custo de uso da magia exceda seu limite após a adição do custo de mana da próxima sustentação
        if( durationSubtype == 'spiking' && spellMana.usage + action.manaToAssign > spellMana.maxUsage ) return false;
      }

      // Indica que próxima sustentação é prevista para ocorrer
      return true;
    }

    // Atribuição de propriedades de 'content.typeset'
    let typeset = this.content.typeset;

    /// Subtipo de duração da magia
    typeset.durationSubtype ||= 'spiking';

    // Atribuição de propriedades de 'content.stats.current.flowCost'
    let currentFlowCost = this.content.stats.current.flowCost;

    /// Custo de fluxo para sustentação
    currentFlowCost.toSustain ||= 0;

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = this.content.stats.current.manaCost;

    /// Custo de mana para sustentação
    currentManaCost.toSustain ||= 0;
  }

  // Retorna propriedades de magias persistentes
  function getPersistentProperties() {
    // Atribuição de propriedades iniciais

    /// Indica se magia tem custo de persistência dinâmico
    this.isDynamicPersistence ||= false;

    /// Indica segmento de uso da magia
    this.usageSegment = null;

    /// Encerra uso da magia
    this.finishUse = function () {
      // Caso haja paradas na tela, aguarda suas conclusões antes de executar função
      while( m.GameMatch.getActiveBreaks().length )
        return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => spell.finishUse() ), { once: true } );

      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          spellStats = this.content.stats,
          framesWithCard = m.GameScreen.current.frames.filter( frame => frame.content.container?.source == this );

      // Programa desuso da magia caso ela não tenha mais potência
      if( !spellStats.current.potency ) return setTimeout( () => spell.disuse() );

      // Registra segmento de uso
      this.usageSegment = matchFlow.step instanceof m.BattleStep ?
        matchFlow.segment : matchFlow.round.getChild( 'post-combat-break-period' ).getChild( 'spell-block' ).children.find(
          segment => segment.name.endsWith( '-' + spellStats.current.potency )
        );

      // Atualiza segmento de uso nas molduras que atualmente exibem a carta
      framesWithCard.forEach( frame => frame.content.updateCard( { targetComponents: [ 'cardData' ] } ) );

      // Programa evento de redução da potência da magia via seu custo de persistência
      m.events.flowChangeEnd.transit.add( matchFlow.round.getChild( 'battle-turn' ),
        () => this.usageSegment ? m.events.flowChangeEnd.finish.add( this.usageSegment, this.applyToPersistCost, { context: this, once: true } ) : null,
      { context: this, once: true } );
    }

    /// Aplica custo de persistência da magia
    this.applyToPersistCost = function ( eventData = {} ) {
      // Apenas executa função caso um segmento de uso ainda exista
      if( !this.usageSegment ) return;

      // Identificadores
      var { usageSegment } = this,
          spellStats = this.content.stats,
          { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == usageSegment );

      // Caso magia não esteja vinculada a 'Congelar', ajusta sua potência para que de seu valor atual seja reduzido seu custo de persistência
      if( !this.attachments.external.some( attachment => attachment instanceof m.SpellFreeze && attachment.content.effects.isEnabled ) )
        this.setPotency( spellStats.current.potency - spellStats.current.manaCost.toPersist );

      // Caso potência atual da magia tenha chegado a 0, desusa-a
      if( !spellStats.current.potency ) return this.disuse();

      // Refaz eventuais disputas mágicas relativas à magia, também controlando habilitação de seu efeito
      this.handleSpellClash();

      // Caso período atual seja um intersticial, atualiza segmento de uso ao seu fim
      if( m.GameMatch.current.flow.period instanceof m.BreakPeriod )
        return m.events.flowChangeEnd.finish.add( m.GameMatch.current.flow.period, updateUsageSegment, { context: this, once: true } );

      // Do contrário, apenas programa novo evento para redução da potência da magia via seu custo de persistência
      return m.events.flowChangeEnd.finish.add( this.usageSegment, this.applyToPersistCost, { context: this, once: true } );

      // Atualiza segmento de uso de um período intersticial
      function updateUsageSegment( eventData = {} ) {
        // Apenas executa função caso um segmento de uso ainda exista
        if( !this.usageSegment ) return;

        // Identificadores
        var framesWithCard = m.GameScreen.current.frames.filter( frame => frame.content.container?.source == this );

        // Atualização do segmento de uso
        this.usageSegment = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' ).getChild( 'spell-block' ).children.find(
          segment => segment.name.endsWith( '-' + this.content.stats.current.potency )
        );

        // Atualização do segmento de uso nas molduras que atualmente exibem a carta
        framesWithCard.forEach( frame => frame.content.updateCard( { targetComponents: [ 'cardData' ] } ) );

        // Atualização do evento para redução da potência da magia via seu custo de persistência
        m.events.flowChangeEnd.finish.add( this.usageSegment, this.applyToPersistCost, { context: this, once: true } );
      }
    }

    // Atribuição de propriedades de 'content.stats.current.manaCost'
    let currentManaCost = this.content.stats.current.manaCost;

    /// Custo de mana para persistência
    currentManaCost.toPersist ||= 0;
  }

  // Retorna propriedades de magias permanentes
  function getPermanentProperties() {
    // Atribuição de propriedades iniciais

    /// Encerra uso da magia
    this.finishUse = function () {
      // Caso haja paradas na tela, aguarda suas conclusões antes de executar função
      while( m.GameMatch.getActiveBreaks().length )
        return m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => spell.finishUse() ), { once: true } );

      // Programa desuso da magia caso ela não tenha mais potência
      if( !this.content.stats.current.potency ) return setTimeout( () => spell.disuse() );
    }
  }
};
