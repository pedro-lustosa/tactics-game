// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameDamage = function ( config = {} ) {
  // Identificadores
  var { singlePoints, totalPoints, source, maneuver } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( this.name && typeof this.name == 'string' && this.name.startsWith( 'damage-' ) );
    m.oAssert( [ 'physical', 'elemental', 'astral', 'raw' ].includes( this.type ) );
    m.oAssert( [ singlePoints, totalPoints ].every( value => !value || Number.isInteger( value ) && value >= 0 ) );
    m.oAssert( !singlePoints && totalPoints >= 0 || singlePoints >= 0 && !totalPoints );
    m.oAssert( !source || source instanceof m.Card );
    m.oAssert( !maneuver || maneuver instanceof m.OffensiveManeuver );
  }

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição de propriedades
  for( let key of [ 'singlePoints', 'totalPoints', 'source', 'maneuver' ] ) this[ key ] = config[ key ] || this[ key ];

  /// Evento para registrar danos infligidos
  m.events.damageChangeStart.inflict.add( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GameDamage.init = function ( damage ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof GameDamage );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = '';

    /// Tipo de dano
    damage.type = '';

    /// Pontos de ataque que resultaram no dano
    damage.attackPoints = 0;

    /// Pontos de ataque que infligiram ao menos 1 ponto de dano
    damage.damagingAttackPoints = 0;

    /// Pontos de dano por ponto de ataque
    damage.singlePoints = 0;

    /// Pontos de dano totais
    damage.totalPoints = 0;

    /// Indica se dano é um crítico
    damage.isCritical = false;

    /// Indica se dano é uma escoriação
    damage.isGraze = false;

    /// Danos colaterais a serem infligidos com este
    damage.collateralDamages = [];

    /// Origem do dano
    damage.source = null;

    /// Manobra que resultou no dano
    damage.maneuver = null;

    /// Determina pontos de dano a serem infligidos a um ente
    damage.setTotalPoints = function ( being, attackPoints = this.attackPoints, penetration = 0, fateModifier = 0 ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( Number.isInteger( attackPoints ) && attackPoints >= 0 && attackPoints <= 10 );
        m.oAssert( Number.isInteger( penetration ) && penetration >= 0 );
        m.oAssert( Number.isInteger( fateModifier ) );
      }

      // Identificadores
      var damageName = this.name.replace( /^damage-/, '' ),
          beingNaturalResistances = being.content.stats.effectivity.resistances.natural.current,
          beingGarments = Object.values( being.garments ?? {} ).filter( garment => garment.resistances );

      // Define pontos de ataque do dano como iguais aos pontos de ataque passados
      this.attackPoints = attackPoints;

      // Zera dano total atual, e pontos de ataque que infligiram danos
      this.damagingAttackPoints = this.totalPoints = 0;

      // Redefine indicadores de se dano é um crítico ou escoriação
      this.isGraze = this.isCritical = false;

      // Itera por pontos de ataque
      for( let i = 1; i <= attackPoints; i++ ) {
        // Identificadores
        let resistanceValue = 0;

        // Adiciona aos pontos de resistência valor da resistência natural alvo
        resistanceValue += beingNaturalResistances[ damageName ];

        // Itera por trajes do alvo
        for( let garment of beingGarments ) {
          // Filtra trajes cuja cobertura é excedida por ponto de ataque atual
          if( garment.coverage < i ) continue;

          // Adiciona aos pontos de resistência valor do traje alvo
          resistanceValue += garment.resistances[ damageName ];
        }

        // Subtrai da resistência valor passado de penetração, sob o valor mínimo de 0
        resistanceValue = Math.max( resistanceValue - penetration, 0 );

        // Define pontos de dano infligidos
        let inflictedDamage = Math.max( this.singlePoints - resistanceValue, 0 );

        // Incrementa pontos de dano totais com pontos de dano infligidos atuais
        this.totalPoints += inflictedDamage;

        // Caso haja pontos de dano infligidos, incrementa registro pertinente
        if( inflictedDamage ) this.damagingAttackPoints++;
      }

      // Quando aplicável, modifica dano segundo o uso de pontos de destino
      this.setFateModifier( fateModifier );

      // Retorna dano
      return this;
    }

    /// Modifica pontos de dano totais segundo o uso de pontos de destino
    damage.setFateModifier = function ( fateModifier = 0 ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( fateModifier ) );

      // Não executa função caso pontos de dano totais a serem infligidos sejam 0
      if( !this.totalPoints ) return this;

      // Não executa função caso modificador de destino passado seja 0
      if( !fateModifier ) return this;

      // Não executa função caso dano já seja um crítico, ou uma escoriação
      if( this.isCritical || this.isGraze ) return this;

      // Para caso manobra relativa ao dano seja 'Arrebatar' e sua origem seja um espírito
      if( this.maneuver instanceof m.ManeuverRapture && this.source instanceof m.Spirit ) {
        // Caso modificador de destino penda para o defensor, zera dano total
        if( fateModifier < 0 ) this.totalPoints = 0;

        // Retorna dano
        return this;
      }

      // Para caso modificador de destino penda para o atacante
      if( fateModifier > 0 ) {
        // Dobra dano total
        this.totalPoints *= 2;

        // Indica que dano é um crítico
        this.isCritical = true;
      }

      // Para caso modificador de destino penda para o defensor
      else {
        // Reduz pela metade dano total
        this.totalPoints = Math.round( this.totalPoints * .5 );

        // Indica que dano é uma escoriação
        this.isGraze = true;
      }

      // Retorna dano
      return this;
    }

    /// Inflige pontos de dano a um ente
    damage.inflictPoints = function ( being ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

      // Identificadores
      var allDamages = [ this ].concat( this.collateralDamages ),
          damagesToInflict = allDamages.filter( damage => damage.totalPoints && ( !damage.validateInfliction || damage.validateInfliction( being ) ) ),
          damagePoints = damagesToInflict.reduce( ( accumulator, current ) => accumulator += current.totalPoints, 0 ),
          fireDamages = allDamages.filter( damage => this instanceof m.DamageFire ),
          beingAttributes = being.content.stats.attributes,
          targetAttribute = 'health' in beingAttributes.current ? 'health' : 'energy',
          dataToEmit = {
            mainDamage: this,
            validDamages: damagesToInflict,
            damageTarget: being,
            damagePoints: damagePoints,
            isToBeRemoved: damagePoints >= beingAttributes.current[ targetAttribute ]
          };

      // Não executa função caso ente não esteja ativo
      if( being.activity != 'active' ) return false;

      // Para danos de fogo que não tenham sido validados, executa função de infligimento própria
      fireDamages.filter( damage => !damage.validateInfliction( being ) ).forEach( damage => damage.burn( being ) );

      // Não executa função caso não haja danos a serem infligidos
      if( !damagesToInflict.length ) return false;

      // Se aplicável, sinaliza para infligidor início do infligimento do dano
      if( this.source ) m.events.damageChangeStart.inflict.emit( this.source, dataToEmit );

      // Sinaliza para sofredor início do sofrimento do dano
      m.events.damageChangeStart.suffer.emit( being, dataToEmit );

      // Itera por danos a serem infligidos
      for( let damage of damagesToInflict ) {
        // Encerra iteração caso alvo não esteja mais ativo
        if( being.activity != 'active' ) break;

        // Sinaliza para dano início de seu infligimento
        m.events.damageChangeStart.inflict.emit( damage, dataToEmit );

        // Reduz pontos de [vida/energia] do alvo
        beingAttributes.reduce( damage.totalPoints, targetAttribute, { triggeringCard: this.source } );

        // Sinaliza para dano fim de seu infligimento
        m.events.damageChangeEnd.inflict.emit( damage, dataToEmit );
      }

      // Sinaliza para sofredor fim do sofrimento do dano
      m.events.damageChangeEnd.suffer.emit( being, dataToEmit );

      // Se aplicável, sinaliza para infligidor fim do infligimento do dano
      if( this.source ) m.events.damageChangeEnd.inflict.emit( this.source, dataToEmit );

      // Retorna dano
      return this;
    }
  }
}

/// Propriedades do protótipo
GameDamage.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: GameDamage }
} );
