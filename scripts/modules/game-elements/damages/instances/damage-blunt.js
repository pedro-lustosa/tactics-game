// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageBlunt = function ( config = {} ) {
  // Iniciação de propriedades
  DamageBlunt.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageBlunt.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageBlunt );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-blunt';

    /// Tipo de dano
    damage.type = 'physical';
  }
}

/// Propriedades do protótipo
DamageBlunt.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageBlunt }
} );
