// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageEther = function ( config = {} ) {
  // Iniciação de propriedades
  DamageEther.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageEther.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageEther );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-ether';

    /// Tipo de dano
    damage.type = 'astral';

    /// Valida infligimento de dano
    damage.validateInfliction = function ( being, damagePoints = this.totalPoints ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( Number.isInteger( damagePoints ) && damagePoints >= 0 );
      }

      // Caso alvo seja um ente astral, sempre valida infligimento do dano
      if( being instanceof m.AstralBeing ) return true;

      // Caso dano não reduziria vida de alvo para 0 ou menos, invalida seu infligimento
      if( being.content.stats.attributes.current.health - damagePoints > 0 ) return false;

      // Indica que infligimento do dano é válido
      return true;
    }
  }
}

/// Propriedades do protótipo
DamageEther.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageEther }
} );
