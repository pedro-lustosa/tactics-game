// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageSlash = function ( config = {} ) {
  // Iniciação de propriedades
  DamageSlash.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageSlash.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageSlash );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-slash';

    /// Tipo de dano
    damage.type = 'physical';
  }
}

/// Propriedades do protótipo
DamageSlash.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageSlash }
} );
