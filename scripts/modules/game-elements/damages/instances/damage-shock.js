// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageShock = function ( config = {} ) {
  // Iniciação de propriedades
  DamageShock.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );

  // Eventos

  /// Para atordoar alvo do dano, quando atendidas as condições para tal
  m.events.damageChangeEnd.inflict.add( this, this.stunTarget );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageShock.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageShock );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-shock';

    /// Tipo de dano
    damage.type = 'elemental';

    /// Atordoa alvo do dano
    damage.stunTarget = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, damageTarget: being } = eventData,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'damage-change' && eventType == 'inflict' );
        m.oAssert( being instanceof m.Being );
      }

      // Não executa função caso ente alvo não seja biótico
      if( !( being instanceof m.BioticBeing ) ) return false;

      // Não executa função caso não se esteja no período do combate
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return false;

      // Não executa função caso dano não seja maior que 1/4 da vida base do ente alvo
      if( this.totalPoints <= being.content.stats.attributes.current.health * .25 ) return false;

      // Atordoa ente alvo
      new m.ConditionStunned().apply( being, matchFlow.parity );
    };
  }
}

/// Propriedades do protótipo
DamageShock.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageShock }
} );
