// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamagePierce = function ( config = {} ) {
  // Iniciação de propriedades
  DamagePierce.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamagePierce.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamagePierce );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-pierce';

    /// Tipo de dano
    damage.type = 'physical';
  }
}

/// Propriedades do protótipo
DamagePierce.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamagePierce }
} );
