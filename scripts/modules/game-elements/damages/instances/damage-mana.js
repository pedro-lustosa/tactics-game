// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageMana = function ( config = {} ) {
  // Iniciação de propriedades
  DamageMana.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );

  // Eventos

  /// Para vincular parte do mana infligido à casa em que o alvo estiver
  m.events.damageChangeStart.inflict.add( this, this.attachMana );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageMana.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageMana );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-mana';

    /// Tipo de dano
    damage.type = 'elemental';

    /// Vincula parte do mana infligido à casa em que o alvo estiver
    damage.attachMana = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, damageTarget: being } = eventData,
          manaToAttach = this.attackPoints;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'damage-change' && eventType == 'inflict' );
        m.oAssert( being instanceof m.Being );
        m.oAssert( Number.isInteger( manaToAttach ) && manaToAttach > 0 );
      }

      // Vincula mana oriundo do dano à casa em que alvo estiver
      being.slot.attach( manaToAttach, 'mana' );
    };
  }
}

/// Propriedades do protótipo
DamageMana.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageMana }
} );
