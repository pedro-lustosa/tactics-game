// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageFire = function ( config = {} ) {
  // Iniciação de propriedades
  DamageFire.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageFire.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageFire );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-fire';

    /// Tipo de dano
    damage.type = 'elemental';

    /// Valida infligimento de dano
    damage.validateInfliction = function ( being, damagePoints = this.totalPoints ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( Number.isInteger( damagePoints ) && damagePoints >= 0 );
      }

      // Caso alvo seja um ente astral, sempre valida infligimento do dano
      if( being instanceof m.AstralBeing ) return true;

      // Caso dano a ser infligido seja maior que o dobro da agilidade atual de alvo, invalida seu infligimento direto
      if( damagePoints > being.content.stats.attributes.current.agility * 2 ) return false;

      // Indica que infligimento do dano é válido
      return true;
    }

    /// Incendeia alvo
    damage.burn = function ( being ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( being instanceof m.Being );

      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          ignitedCondition = being.conditions.find( condition => condition instanceof m.ConditionIgnited ) ?? new m.ConditionIgnited( { source: this.source } );

      // Não executa função caso não haja dano a ser infligido
      if( !this.totalPoints ) return false;

      // Não executa função caso ente alvo não seja físico
      if( !( being instanceof m.PhysicalBeing ) ) return false;

      // Não executa função caso não se esteja no período do combate
      if( !( matchFlow.period instanceof m.CombatPeriod ) ) return false;

      // Caso dano de fogo a ser infligido via esta operação seja maior que o dano de fogo já previsto para ser infligido, atualiza a origem da condição
      if( ignitedCondition.damageToInflict < this.totalPoints ) ignitedCondition.source = this.source;

      // Incrementa dano total a ser infligido pela condição
      ignitedCondition.damageToInflict += this.totalPoints;

      // Incendeia ente alvo, ou atualiza estado dessa condição
      ignitedCondition.apply( being );
    };
  }
}

/// Propriedades do protótipo
DamageFire.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageFire }
} );
