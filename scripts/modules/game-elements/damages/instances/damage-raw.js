// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const DamageRaw = function ( config = {} ) {
  // Iniciação de propriedades
  DamageRaw.init( this );

  // Superconstrutor
  m.GameDamage.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DamageRaw.init = function ( damage ) {
    // Chama 'init' ascendente
    m.GameDamage.init( damage );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( damage instanceof DamageRaw );

    // Atribuição de propriedades iniciais

    /// Nome
    damage.name = 'damage-raw';

    /// Tipo de dano
    damage.type = 'raw';
  }
}

/// Propriedades do protótipo
DamageRaw.prototype = Object.create( m.GameDamage.prototype, {
  // Construtor
  constructor: { value: DamageRaw }
} );
