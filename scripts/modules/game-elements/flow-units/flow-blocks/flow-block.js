// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowBlock = function ( config = {} ) {
  // Iniciação de propriedades
  FlowBlock.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.name );
    m.oAssert( config.parent instanceof m.BreakPeriod );
    m.oAssert( Number.isInteger( config.segments ) && config.segments > 0 );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = config.name;

  /// Atribuição do nome de exibição
  this.displayName = m.languages.keywords.getFlow( this.name );

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição de estágios descendentes
  for( let i = config.segments; i; i-- )
    this.children.push( new m.FlowSegment( {
      name: `segment-${ i }`, displayName: `${ m.languages.keywords.getFlow( 'segment' ) } ${ i }`, parent: this
    } ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowBlock.init = function ( block ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( block );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( block instanceof FlowBlock );

    // Atribuição de propriedades iniciais

    /// Tipo de fluxo
    block.flowType = 'block';
  }
}

/// Propriedades do protótipo
FlowBlock.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowBlock }
} );
