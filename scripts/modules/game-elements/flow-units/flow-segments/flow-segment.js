// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowSegment = function ( config = {} ) {
  // Iniciação de propriedades
  FlowSegment.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.name && config.displayName );
    m.oAssert( [ m.FlowBlock, m.CombatPeriod ].some( constructor => config.parent instanceof constructor ) );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = config.name;

  /// Atribuição do nome de exibição
  this.displayName = config.displayName;

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.FlowParity( { parent: this, parity: 'odd' } ),
    new m.FlowParity( { parent: this, parity: 'even' } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowSegment.init = function ( segment ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( segment );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( segment instanceof FlowSegment );

    // Atribuição de propriedades iniciais

    /// Tipo de fluxo
    segment.flowType = 'segment';
  }
}

/// Propriedades do protótipo
FlowSegment.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowSegment }
} );
