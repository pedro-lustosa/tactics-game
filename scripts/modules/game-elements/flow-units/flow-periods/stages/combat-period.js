// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const CombatPeriod = function ( config = {} ) {
  // Iniciação de propriedades
  CombatPeriod.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.BattleTurn );

  // Superconstrutor
  m.FlowPeriod.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição de estágios descendentes
  for( let i = 15; i; i-- )
    this.children.push( new m.FlowSegment( {
      name: `segment-${ i }`, displayName: `${ m.languages.keywords.getFlow( 'segment' ) } ${ i }`, parent: this
    } ) );

  // Eventos

  /// Para geração de ações
  m.events.flowChangeStart.begin.add( this, this.setActions );

  /// Para remoção de ações
  m.events.flowChangeEnd.finish.add( this, this.unsetActions );

  /// Para despreparo dos entes, e definição de entes adiantados
  m.events.flowChangeStart.finish.add( this, m.Being.resetReadiness, { context: m.Being } );

  /// Para registro de humanoides ativos ao longo de um combate, para uso com a magia 'O Regente'
  m.events.flowChangeEnd.transit.add( this, m.SpellTheRuler.controlActiveHumanoidsRecord, { context: m.SpellTheRuler } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  CombatPeriod.init = function ( period ) {
    // Chama 'init' ascendente
    m.FlowPeriod.init( period );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( period instanceof CombatPeriod );

    // Atribuição de propriedades iniciais

    /// Nome
    period.name = 'combat-period';

    /// Nome de exibição
    period.displayName = m.languages.keywords.getFlow( period.name );

    /// Gera as jogadas do período
    period.setMoves = function ( eventData = {} ) {
      // Identificadores
      var beings = {},
          matchDecks = m.GameMatch.current.decks,
          { eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' );

      // Captura entes a receberem jogadas
      for( let parity of [ 'odd', 'even' ] )
        beings[ parity ] = matchDecks[ parity ].cards.beings.concat( matchDecks[ parity ].cards.flatMap( card => card.summons ?? [] ) ).filter(
          card => card.activity == 'active' || card instanceof m.Goblin && card.activity == 'suspended'
        );

      // Itera por segmentos do período
      for( let segment of this.children ) {
        // Captura número do segmento alvo
        let segmentNumber = segment.name.replace( /\D/g, '' );

        // Itera por paridades do segmento alvo
        for( let parityStage of segment.children ) {
          // Captura entes da paridade alvo cuja iniciativa corresponda ao número do segmento
          let segmentBeings = beings[ parityStage.parity ].filter( being => being.content.stats.attributes.current.initiative == segmentNumber );

          // Gera jogada por ente válido, e a adiciona ao palco atual
          for( let being of segmentBeings ) new m.FlowMove( { parent: parityStage, source: being } ).add();
        }
      }

      // Retorna período
      return this;
    }

    /// Remove as jogadas do período, quando concluído
    period.unsetMoves = function ( eventData = {} ) {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          { eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' );

      // Remoção das jogadas
      while( matchFlow.moves.length ) matchFlow.moves[ 0 ].remove();

      // Retorna período
      return this;
    }

    /// Configura geração das ações do período
    period.setActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, targetCards: targetBeings } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'begin' );
        m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );
      }

      // Define entes a terem suas ações configuradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } );

      // Adiciona evento para controlar ações do ente alvo em função de sua disponibilidade
      for( let being of targetBeings ) m.events.beingReadinessEnd.any.add( being, this.controlActions, { context: this } );
    }

    /// Desconfigura geração das ações do período
    period.unsetActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, targetCards: targetBeings } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'finish' );
        m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );
      }

      // Define entes a terem suas ações desconfiguradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } );

      // Remove evento para controlar ações do ente alvo em função de sua disponibilidade
      for( let being of targetBeings ) m.events.beingReadinessEnd.any.remove( being, this.controlActions, { context: this } );
    }

    /// Controla ações do período
    period.controlActions = function ( eventData = {} ) {
      // Identificadores
      var { eventTarget: being, eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( being instanceof m.Being );
        m.oAssert( !eventCategory || eventCategory == 'being-readiness' );
        m.oAssert( !eventType || eventType == being.readiness.status );
      }

      // Limpa ações atuais de ente
      clearCurrentActions: {
        // Remove ações atuais de ente
        while( being.actions.length ) being.actions.shift();

        // Remove eventuais eventos atuais de ente

        /// De mudança de atividade
        for( let action of [
          m.ActionAttack, m.ActionEngage, m.ActionIntercept, m.ActionDisengage, m.ActionHandle, m.ActionChannel, m.ActionExpel, m.ActionLeech, m.ActionPossess,
          m.ActionBanish, m.ActionGuard, m.ActionDeploy, m.ActionRetreat
        ] )
          m.events.cardActivityEnd.any.remove( being, action.controlAvailability, { context: action } );

        /// De mudança de manobra
        for( let action of [ m.ActionAttack ] )
          m.events.cardContentEnd.maneuver.remove( being, action.controlAvailability, { context: action } );

        /// De mudança de equipamento
        for( let action of [ m.ActionHandle ] )
          m.events.cardContentEnd.item.remove( being, action.controlAvailability, { context: action } );

        /// De mudança de uso
        for( let action of [ m.ActionPrepare, m.ActionEnergize ] )
          m.events.cardUseEnd.useInOut.remove( being, action.controlAvailability, { context: action } );

        /// De entrada em casa do campo
        for( let action of [ m.ActionEngage, m.ActionIntercept, m.ActionDisengage, m.ActionChannel, m.ActionExpel, m.ActionRetreat ] )
          m.events.fieldSlotCardChangeEnd.enter.remove( being, action.controlAvailability, { context: action } );

        /// De mudança de mana
        for( let action of [ m.ActionExpel ] )
          m.events.cardContentEnd.mana.remove( being, action.controlAvailability, { context: action } );

        /// De mudança de energia
        for( let action of [ m.ActionLeech ] )
          m.events.cardContentEnd.energy.remove( being, action.controlAvailability, { context: action } );
      }

      // Identifica estado de preparo atual do ente alvo, e quando aplicável executa função lhe atinente
      switch( being.readiness.status ) {
        case 'prepared':
          return controlPreparedActions();
        case 'occupied':
          return controlOccupiedActions();
        case 'exhausted':
          return controlExhaustedActions();
        default:
          return;
      }

      // Controla ações de entes preparados
      function controlPreparedActions() {
        // Controle de 'atacar'
        attackControl: {
          // Executa controle de 'Atacar'
          m.ActionAttack.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Atacar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionAttack.controlAvailability, { context: m.ActionAttack } );

          /// De mudança de manobra
          m.events.cardContentEnd.maneuver.add( being, m.ActionAttack.controlAvailability, { context: m.ActionAttack } );
        }

        // Controle de 'engajar'
        engageControl: {
          // Caso alvo não seja um ente físico, encerra bloco
          if( !( being instanceof m.PhysicalBeing ) ) break engageControl;

          // Executa controle de 'Engajar'
          m.ActionEngage.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Engajar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionEngage.controlAvailability, { context: m.ActionEngage } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionEngage.controlAvailability, { context: m.ActionEngage } );
        }

        // Controle de 'interceptar'
        interceptControl: {
          // Caso alvo não seja um ente físico, encerra bloco
          if( !( being instanceof m.PhysicalBeing ) ) break interceptControl;

          // Executa controle de 'Interceptar'
          m.ActionIntercept.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Interceptar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionIntercept.controlAvailability, { context: m.ActionIntercept } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionIntercept.controlAvailability, { context: m.ActionIntercept } );
        }

        // Controle de 'desengajar'
        disengageControl: {
          // Caso alvo não seja um ente físico, encerra bloco
          if( !( being instanceof m.PhysicalBeing ) ) break disengageControl;

          // Executa controle de 'Desengajar'
          m.ActionDisengage.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Desengajar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionDisengage.controlAvailability, { context: m.ActionDisengage } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionDisengage.controlAvailability, { context: m.ActionDisengage } );
        }

        // Controle de 'manejar'
        handleControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break handleControl;

          // Executa controle de 'Manejar'
          m.ActionHandle.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Manejar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );

          /// De mudança de equipamento
          m.events.cardContentEnd.item.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );
        }

        // Controle de 'canalizar'
        channelControl: {
          // Caso ente alvo não seja um canalizador, encerra bloco
          if( !being.content.stats.combativeness.channeling ) break channelControl;

          // Executa controle de 'Canalizar'
          m.ActionChannel.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Canalizar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionChannel.controlAvailability, { context: m.ActionChannel } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionChannel.controlAvailability, { context: m.ActionChannel } );
        }

        // Controle de 'expelir'
        expelControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break expelControl;

          // Caso ente alvo não seja um metothe, encerra bloco
          if( !being.content.stats.combativeness.channeling?.paths.metoth.skill ) break expelControl;

          // Executa controle de 'Expelir'
          m.ActionExpel.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Expelir'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionExpel.controlAvailability, { context: m.ActionExpel } );

          /// De mudança de mana
          m.events.cardContentEnd.mana.add( being, m.ActionExpel.controlAvailability, { context: m.ActionExpel } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionExpel.controlAvailability, { context: m.ActionExpel } );
        }

        // Controle de 'vampirizar'
        leechControl: {
          // Caso ente alvo não seja um espírito ou demônio, encerra bloco
          if( ![ m.Spirit, m.Demon ].some( constructor => being instanceof constructor ) ) break leechControl;

          // Executa controle de 'Vampirizar'
          m.ActionLeech.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Vampirizar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionLeech.controlAvailability, { context: m.ActionLeech } );

          /// De mudança de energia
          m.events.cardContentEnd.energy.add( being, m.ActionLeech.controlAvailability, { context: m.ActionLeech } );
        }

        // Controle de 'possuir'
        possessControl: {
          // Caso ente alvo não seja um espírito ou demônio, encerra bloco
          if( ![ m.Spirit, m.Demon ].some( constructor => being instanceof constructor ) ) break possessControl;

          // Executa controle de 'Possuir'
          m.ActionPossess.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Possuir'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionPossess.controlAvailability, { context: m.ActionPossess } );
        }

        // Controle de 'banir'
        banishControl: {
          // Caso ente alvo não seja um anjo, encerra bloco
          if( !( being instanceof m.Angel ) ) break banishControl;

          // Executa controle de 'Banir'
          m.ActionBanish.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Banir'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionBanish.controlAvailability, { context: m.ActionBanish } );
        }

        // Controle de 'amparar'
        guardControl: {
          // Caso ente alvo não seja um anjo, encerra bloco
          if( !( being instanceof m.Angel ) ) break guardControl;

          // Executa controle de 'Amparar'
          m.ActionGuard.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Amparar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionGuard.controlAvailability, { context: m.ActionGuard } );
        }

        // Controle de 'mobilizar'
        deployControl: {
          // Caso ente alvo não seja um goblin, encerra bloco
          if( !( being instanceof m.Goblin ) ) break deployControl;

          // Executa controle de 'Mobilizar'
          m.ActionDeploy.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Mobilizar'
          m.events.cardActivityEnd.any.add( being, m.ActionDeploy.controlAvailability, { context: m.ActionDeploy } );
        }

        // Controle de 'recuar'
        retreatControl: {
          // Caso ente alvo não seja um goblin, encerra bloco
          if( !( being instanceof m.Goblin ) ) break retreatControl;

          // Executa controle de 'Recuar'
          m.ActionRetreat.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Recuar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionRetreat.controlAvailability, { context: m.ActionRetreat } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionRetreat.controlAvailability, { context: m.ActionRetreat } );
        }

        // Controle de 'preparar'
        prepareControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break prepareControl;

          // Executa controle de 'Preparar'
          m.ActionPrepare.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Preparar'

          /// De mudança de uso
          m.events.cardUseEnd.useInOut.add( being, m.ActionPrepare.controlAvailability, { context: m.ActionPrepare } );
        }

        // Controle de 'energizar'
        energizeControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break energizeControl;

          // Executa controle de 'Energizar'
          m.ActionEnergize.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Energizar'

          /// De mudança de uso
          m.events.cardUseEnd.useInOut.add( being, m.ActionEnergize.controlAvailability, { context: m.ActionEnergize } );
        }
      }

      // Controla ações de entes ocupados
      function controlOccupiedActions() {
        // Controle de 'abortar'
        abortControl: {
          // Caso ente alvo não esteja acionando nenhuma ação, encerra bloco
          if( !m.GameAction.currents.some( action => action.committer == being ) ) break abortControl;

          // Executa controle de 'Abortar'
          m.ActionAbort.controlAvailability( { eventTarget: being } );
        }
      }

      // Controla ações de entes esgotados
      function controlExhaustedActions() {
        // Controle de 'interceptar'
        interceptControl: {
          // Caso alvo não seja um ente físico, encerra bloco
          if( !( being instanceof m.PhysicalBeing ) ) break interceptControl;

          // Executa controle de 'Interceptar'
          m.ActionIntercept.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Interceptar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionIntercept.controlAvailability, { context: m.ActionIntercept } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionIntercept.controlAvailability, { context: m.ActionIntercept } );
        }
      }
    }

    /// Retorna paridades do período
    period.getParities = function () {
      // Identificadores
      var paritiesArray = [];

      // Itera por segmentos do período, e captura suas paridades
      for( let segment of this.children ) paritiesArray.push( segment.children );

      // Retorna arranjo de paridades
      return paritiesArray;
    }

    /// Retorna um estágio do período tendo como referência desvio a partir de outro estágio do período, de mesma unidade do fluxo
    period.getStageFrom = function ( referenceStage, deviation ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ m.FlowSegment, m.FlowParity ].some( constructor => referenceStage instanceof constructor ) );
        m.oAssert( referenceStage.getParent( 'period' ) == this );
        m.oAssert( Number.isInteger( deviation ) );
      }

      // Identificadores
      var targetStages = getTargetStages.call( this ),
          referenceStageIndex = targetStages.indexOf( referenceStage ),
          targetStage = targetStages[ referenceStageIndex + deviation ] ?? null;

      // Retorna estágio que corresponder ao desvio passado
      return targetStage;

      // Funções

      /// Retorna arranjo com todos os estágios do período que tenham a mesma unidade de fluxo que o de referência
      function getTargetStages() {
        // Para caso estágio de referência seja um segmento
        if( referenceStage instanceof m.FlowSegment ) return this.children;

        // Para caso estágio de referência seja uma paridade
        return this.getParities().flat().filter( parityStage => parityStage.parity == referenceStage.parity );
      }
    }
  }
}

/// Propriedades do protótipo
CombatPeriod.prototype = Object.create( m.FlowPeriod.prototype, {
  // Construtor
  constructor: { value: CombatPeriod }
} );
