// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const BreakPeriod = function ( config = {} ) {
  // Iniciação de propriedades
  BreakPeriod.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( config.name );
    m.oAssert( config.parent instanceof m.BattleTurn );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${ config.name }-${ this.name }`;

  /// Atribuição do nome de exibição
  this.displayName = m.languages.keywords.getFlow( this.name );

  // Superconstrutor
  m.FlowPeriod.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.FlowBlock( { name: 'being-block', parent: this, segments: 15 } ),
    new m.FlowBlock( { name: 'item-block', parent: this, segments: 15 } ),
    new m.FlowBlock( { name: 'spell-block', parent: this, segments: 20 } )
  );

  // Eventos

  /// Para o pré-combate
  if( this.name.startsWith( 'pre-combat' ) ) {
    // Restauração de estatísticas de entes
    m.events.flowChangeEnd.begin.add( this, m.Being.restoreStats, { context: m.Being } );

    // Aplicação do efeito da característica 'inbredVoth', de gnomos
    m.events.flowChangeEnd.begin.add( this, m.Gnome.triggerInbredVoth, { context: m.Gnome } );

    // Adição dos pontos de destino intransitivos pré-definidos
    m.events.flowChangeEnd.begin.add( this, eventData => m.GameMatch.current.players.addDefaultFatePoints( eventData ) );

    // Determinação de entes adiantados
    m.events.flowChangeStart.finish.add( this, m.Being.forwardBeings, { context: m.Being } );

    // Aplicação dos posicionamentos de ajuste
    m.events.flowChangeEnd.finish.add( this, () => m.GameMatch.current.environments.field.applyAdjustmentPlacements() );
  }

  /// Para o pós-combate
  else if( this.name.startsWith( 'post-combat' ) ) {
    // Verificação das condições para o fim da etapa atual
    m.events.flowChangeStart.finish.add( this, eventData => m.GameMatch.current.flow.step.checkEndConditions( eventData ) );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BreakPeriod.init = function ( period ) {
    // Chama 'init' ascendente
    m.FlowPeriod.init( period );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( period instanceof BreakPeriod );

    // Atribuição de propriedades iniciais

    /// Nome
    period.name = 'break-period';

    /// Retorna paridades do período
    period.getParities = function () {
      // Identificadores
      var paritiesArray = [];

      // Itera por blocos do período
      for( let block of this.children ) {
        // Itera por segmentos do período, e captura suas paridades
        for( let segment of block.children ) paritiesArray.push( segment.children );
      }

      // Retorna arranjo de paridades
      return paritiesArray;
    }

    /// Programa um efeito para ocorrer em um estágio do período
    period.scheduleEffect = function ( eventData = {} ) {
      // Identificadores pré-validação
      var source = this,
          { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ m.Card, m.GameCondition ].some( constructor => source instanceof constructor ) );
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == period );
      }

      // Identificadores pós-validação
      var sourceCard = source instanceof m.Card ? source : source.source,
          effectStage = period.getStageForEffect( sourceCard ),
          method = period.name.startsWith( 'pre-combat' ) ? 'applyPreCombatEffect' : 'applyPostCombatEffect',
          methodPath = source instanceof m.Card ? source.content.effects : source;

      // Caso não tenha sido encontrado um estágio para o efeito, não executa função
      if( !effectStage ) return;

      // Eventos

      /// Para provocar efeito no início do estágio pertinente
      m.events.flowChangeEnd.begin.add( effectStage, methodPath[ method ], { context: methodPath, once: true } );

      /// Para, ao fim do período intersticial, remover evento para provocar efeito no início do estágio pertinente, para caso não tenha sido provocado
      m.events.flowChangeEnd.finish.add( period,
        () => m.events.flowChangeEnd.begin.remove( effectStage, methodPath[ method ], { context: methodPath } ),
      { once: true } );
    }

    /// Retorna estágio em que efeito da carta de referência deve ocorrer
    period.getStageForEffect = function ( card ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( card instanceof m.Card );

      // Identificadores
      var cardOwner = card.getRelationships().owner;

      // Delega operação em função do tipo primitivo da carta passada
      return card instanceof m.Being ? getBeingStage() : card instanceof m.Item ? getItemStage() : card instanceof m.Spell ? getSpellStage() : null;

      // Funções

      /// Retorna estágio em que efeito do ente passado deve ocorrer
      function getBeingStage() {
        // Não executa função caso ente não tenha iniciativa
        if( !card.content.stats.attributes.current.initiative ) return null;

        // Identificadores
        var beingBlock = period.getChild( 'being-block' ),
            effectSegment = beingBlock.getChild( `segment-${ card.content.stats.attributes.current.initiative }` ),
            effectParity = effectSegment.getChild( `${ cardOwner.parity }-parity` );

        // Retorna paridade em que efeito deve ocorrer
        return effectParity;
      }

      /// Retorna estágio em que efeito do equipamento passado deve ocorrer
      function getItemStage() {
        // Não executa função caso equipamento não tenha um dono, ou esse dono não tenha iniciativa
        if( !card.owner?.content.stats.attributes.current.initiative ) return null;

        // Identificadores
        var itemBlock = period.getChild( 'item-block' ),
            effectSegment = itemBlock.getChild( `segment-${ card.owner.content.stats.attributes.current.initiative }` ),
            effectParity = effectSegment.getChild( `${ cardOwner.parity }-parity` );

        // Retorna paridade em que efeito deve ocorrer
        return effectParity;
      }

      /// Retorna estágio em que efeito da magia passada deve ocorrer
      function getSpellStage() {
        // Não executa função caso magia não tenha potência
        if( !card.content.stats.current.potency ) return null;

        // Identificadores
        var spellBlock = period.getChild( 'spell-block' ),
            effectSegment = spellBlock.getChild( `segment-${ card.content.stats.current.potency }` ),
            effectParity = effectSegment.getChild( `${ cardOwner.parity }-parity` );

        // Retorna paridade em que efeito deve ocorrer
        return effectParity;
      }
    }
  }
}

/// Propriedades do protótipo
BreakPeriod.prototype = Object.create( m.FlowPeriod.prototype, {
  // Construtor
  constructor: { value: BreakPeriod }
} );
