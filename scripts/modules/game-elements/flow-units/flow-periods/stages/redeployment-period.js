// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const RedeploymentPeriod = function ( config = {} ) {
  // Iniciação de propriedades
  RedeploymentPeriod.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.BattleTurn );

  // Superconstrutor
  m.FlowPeriod.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.FlowParity( { parent: this, parity: 'odd' } ),
    new m.FlowParity( { parent: this, parity: 'even' } )
  );

  // Eventos

  /// Para geração de ações
  m.events.flowChangeStart.begin.add( this, this.setActions );

  /// Para remoção de ações
  m.events.flowChangeEnd.finish.add( this, this.unsetActions );

  /// Para conformidade com característica 'adaptiveTactics', de humanos
  m.events.flowChangeEnd.transit.add( this, m.Human.setupAdaptiveTactics, { context: m.Human } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  RedeploymentPeriod.init = function ( period ) {
    // Chama 'init' ascendente
    m.FlowPeriod.init( period );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( period instanceof RedeploymentPeriod );

    // Atribuição de propriedades iniciais

    /// Nome
    period.name = 'redeployment-period';

    /// Nome de exibição
    period.displayName = m.languages.keywords.getFlow( period.name );

    /// Gera as jogadas do período
    period.setMoves = function ( eventData = {} ) {
      // Identificadores
      var parities = this.children,
          { eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' );

      // Geração e captura das jogadas
      for( let parityStage of parities ) {
        // Gera jogadas
        let currentPlayer = m.GameMatch.current.players[ parityStage.parity ],
            moves = [
              new m.FlowMove( { parent: parityStage, source: currentPlayer } ),
              new m.FlowMove( { parent: parityStage, source: currentPlayer } )
            ];

        // Caso jogador atual tenha ao menos 1 ponto de destino, gera 1 jogada adicional
        if( currentPlayer.fatePoints.current.get() ) moves.push( new m.FlowMove( { parent: parityStage, source: currentPlayer } ) );

        // Inseri jogadas no palco
        for( let move of moves ) move.add();
      }

      // Retorna período
      return this;
    }

    /// Remove as jogadas do período, quando concluído
    period.unsetMoves = function ( eventData = {} ) {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          { eventCategory, eventType } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' );

      // Remoção das jogadas
      while( matchFlow.moves.length ) matchFlow.moves[ 0 ].remove();

      // Retorna período
      return this;
    }

    /// Gera as ações do período
    period.setActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, targetCards: targetBeings } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'begin' );
        m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );
      }

      // Define entes a terem suas ações configuradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } );

      // Itera por entes a se conceder ações
      for( let being of targetBeings ) {
        // Filtra entes em zonas de engajamento
        if( being.slot instanceof m.EngagementZone ) continue;

        // Filtra entes incontroláveis
        if( being.isUncontrollable ) continue;

        // Filtra ogros, segundo a característica 'stubbornTactics'
        if( being instanceof m.Ogre && !m.Ogre.validateRedeploymentActions( being ) ) continue;

        // Controle de 'posicionar'
        relocateControl: {
          // Executa controle da ação 'Posicionar'
          m.ActionRelocate.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Posicionar'
          m.events.cardActivityEnd.any.add( being, m.ActionRelocate.controlAvailability, { context: m.ActionRelocate } );
        }

        // Controle de 'desconvocar'
        unsummonControl: {
          // Caso ente alvo não seja uma ficha, encerra bloco
          if( !being.isToken ) break unsummonControl;

          // Executa controle da ação 'Desconvocar'
          m.ActionUnsummon.controlAvailability( { eventTarget: being } );
        }

        // Encerra definição de ações para fichas
        if( being.isToken ) continue;

        // Controle de 'mobilizar'
        deployControl: {
          // Executa controle da ação 'Mobilizar'
          m.ActionDeploy.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Mobilizar'
          m.events.cardActivityEnd.any.add( being, m.ActionDeploy.controlAvailability, { context: m.ActionDeploy } );
        }

        // Controle de 'recuar'
        retreatControl: {
          // Executa controle da ação 'Recuar'
          m.ActionRetreat.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Recuar'
          m.events.cardActivityEnd.any.add( being, m.ActionRetreat.controlAvailability, { context: m.ActionRetreat } );
        }

        // Encerra definição de ações para não humanoides
        if( !( being instanceof m.Humanoid ) ) continue;

        // Controle de 'convocar'
        summonControl: {
          // Executa controle da ação 'Convocar'
          m.ActionSummon.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Convocar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionSummon.controlAvailability, { context: m.ActionSummon } );

          /// De mudança de uso
          m.events.cardUseEnd.useInOut.add( being, m.ActionSummon.controlAvailability, { context: m.ActionSummon } );

          /// De mudança de condição
          m.events.conditionChangeEnd.markers.add( being, m.ActionSummon.controlAvailability, { context: m.ActionSummon } );
        }

        // Controle de 'absorver'
        absorbControl: {
          // Executa controle da ação 'Absorver'
          m.ActionAbsorb.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Absorver'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionAbsorb.controlAvailability, { context: m.ActionAbsorb } );

          /// De mudança de uso
          m.events.cardUseEnd.useInOut.add( being, m.ActionAbsorb.controlAvailability, { context: m.ActionAbsorb } );

          /// De entrada em casa do campo
          m.events.fieldSlotCardChangeEnd.enter.add( being, m.ActionAbsorb.controlAvailability, { context: m.ActionAbsorb } );
        }

        // Controle de 'manejar'
        handleControl: {
          // Executa controle da ação 'Manejar'
          m.ActionHandle.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Manejar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );

          /// De mudança de equipamento
          m.events.cardContentEnd.item.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );
        }

        // Encerra definição de ações para entes cujo baralho não tenha equipamentos
        if( !m.GameMatch.current.decks[ being.deck.owner.parity ].cards.items.length ) continue;

        // Controle de 'equipar'
        equipControl: {
          // Executa controle da ação 'Equipar'
          m.ActionEquip.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Equipar'
          m.events.cardActivityEnd.any.add( being, m.ActionEquip.controlAvailability, { context: m.ActionEquip } );
        }

        // Controle de 'desequipar'
        unequipControl: {
          // Executa controle da ação 'Desequipar'
          m.ActionUnequip.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Desequipar'
          m.events.cardContentEnd.item.add( being, m.ActionUnequip.controlAvailability, { context: m.ActionUnequip } );
        }

        // Controle de 'transferir'
        transferControl: {
          // Executa controle da ação 'Transferir'
          m.ActionTransfer.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Transferir'
          m.events.cardContentEnd.item.add( being, m.ActionTransfer.controlAvailability, { context: m.ActionTransfer } );
        }
      }

      // Retorna período
      return this;
    }

    /// Remove as ações do período
    period.unsetActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, targetCards: targetBeings } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'finish' );
        m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );
      }

      // Define entes a terem suas ações desconfiguradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipSideDeck: true } );

      // Itera por entes a se remover ações
      for( let being of targetBeings ) {
        // Remove ações do ente alvo
        while( being.actions.length ) being.actions.shift();

        // Quando existentes, remove eventos de atividade para o controle de ações
        for( let action of [ m.ActionDeploy, m.ActionRetreat, m.ActionSummon, m.ActionEquip, m.ActionRelocate, m.ActionHandle, m.ActionAbsorb ] )
          m.events.cardActivityEnd.any.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de mudança de equipamentos para o controle de ações
        for( let action of [ m.ActionUnequip, m.ActionTransfer, m.ActionHandle ] )
          m.events.cardContentEnd.item.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de uso para o controle de ações
        for( let action of [ m.ActionSummon, m.ActionAbsorb ] )
          m.events.cardUseEnd.useInOut.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de entrada em casa do campo para o controle de ações
        for( let action of [ m.ActionAbsorb ] )
          m.events.fieldSlotCardChangeEnd.enter.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de mudança de condições para o controle de ações
        for( let action of [ m.ActionSummon ] )
          m.events.conditionChangeEnd.markers.remove( being, action.controlAvailability, { context: action } );
      }

      // Retorna período
      return this;
    }

    /// Retorna paridades do período
    period.getParities = function () {
      // Retorna arranjo com as paridades
      return [ this.children ];
    }
  }
}

/// Propriedades do protótipo
RedeploymentPeriod.prototype = Object.create( m.FlowPeriod.prototype, {
  // Construtor
  constructor: { value: RedeploymentPeriod }
} );
