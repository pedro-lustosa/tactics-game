// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowPeriod = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( typeof this.getParities == 'function' );

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Eventos

  /// Para limpeza de informações relativas a ações de cartas
  m.events.flowChangeStart.finish.add( this, m.GameAction.clearCardsData, { context: m.GameAction } );

  /// Para controle dos textos suspensos de cartas
  m.events.flowChangeStart.transit.add( this, m.Card.controlHoverText, { context: m.Card } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowPeriod.init = function ( period ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( period );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( period instanceof FlowPeriod );

    // Atribuição de propriedades iniciais

    /// Tipo de fluxo
    period.flowType = 'period';
  }
}

/// Propriedades do protótipo
FlowPeriod.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowPeriod }
} );
