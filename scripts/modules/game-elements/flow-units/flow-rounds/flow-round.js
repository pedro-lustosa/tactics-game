// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowRound = function ( config = {} ) {
  // Iniciação de propriedades
  FlowRound.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( !config.parent );

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.SetupPhase( { parent: this } ),
    new m.BattlePhase( { parent: this } )
  );

  // Eventos

  /// Para limpeza de dados diversos definidos ao longo da rodada
  m.events.flowChangeEnd.finish.add( this, this.clearMiscData );

  /// Para determinar vencedor da rodada
  m.events.flowChangeEnd.finish.add( this, this.setWinner );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowRound.init = function ( round ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( round );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( round instanceof FlowRound );

    // Atribuição de propriedades iniciais

    /// Nome
    round.name = 'round';

    /// Nome de exibição
    round.displayName = m.languages.keywords.getFlow( round.name );

    /// Tipo de fluxo
    round.flowType = 'round';

    /// Contador de rodadas
    round.counter = 0;

    /// Atualiza contagem da rodada
    round.incrementCounter = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );

      // Incremento do contador, e atualização do nome
      this.displayName = `${ m.languages.keywords.getFlow( this.flowType ) } ${ ++this.counter }`;

      // Retorna rodada
      return this;
    }

    /// Determina vencedor da rodada
    round.setWinner = function ( eventData = {} ) {
      // Não executa função caso partida não tenha sido encerrada com um desfecho previsto
      if( !m.GameMatch.current.stats.isFinished ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData,
          match = m.GameMatch.current;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == this );
        m.oAssert( match instanceof m.GameMatch );
      }

      // Identificadores pós-validação
      var matchPlayers = match.players.get( 'all' ),
          winResult = 0;

      // Itera por jogadores
      for( let player of matchPlayers ) {
        // Identificadores
        let userDeck = player.user.decks.find( deck => deck.name == player.gameDeck.name );

        // Caso partida atual não seja uma simulação, computa partida como concluída
        if( !match.isSimulation ) player.user.matches.unfinished--, userDeck.matches.unfinished--;

        // Caso um dos jogadores deva vencer, ajusta resultado da vitória
        if( player.isToWin ) player.parity == 'odd' ? winResult++ : winResult--;

        // Redefine indicador de se jogador alvo deve vencer
        player.isToWin = false;
      }

      // Executa operações segundo resultado da vitória
      return winResult ? setWin() : setDraw();

      // Define vitória
      function setWin() {
        // Identificadores
        var winner = m.GameMatch.current.players[ winResult == 1 ? 'odd' : 'even' ],
            loser = m.GameMatch.current.players[ winResult == 1 ? 'even' : 'odd' ],
            modalText = 'notify-round-winner-' + (
              match.isSimulation || !m.GamePlayer.current ? 'other' : m.GamePlayer.current == winner ? 'self' : 'opponent'
            );

        // Registra na partida nome do usuário que a venceu
        match.stats.winner = winner.user.name;

        // Atualiza dados sobre jogadores
        updatePlayerStats: {
          // Apenas executa bloco caso partida não seja uma simulação
          if( match.isSimulation ) break updatePlayerStats;

          // Computa vitória do vencedor
          winner.user.matches.wins++;

          // Computa vitória no baralho usado do vencedor
          winner.user.decks.find( deck => deck.name == winner.gameDeck.name ).matches.wins++;

          // Computa derrota do perdedor
          loser.user.matches.losses++;

          // Computa derrota no baralho usado do perdedor
          loser.user.decks.find( deck => deck.name == loser.gameDeck.name ).matches.losses++;
        }

        // Caso jogador atual seja o perdedor, atualiza servidor sobre resultado da partida
        if( m.GamePlayer.current == loser ) match.updateServer();

        // Notifica usuários sobre vencedor da partida
        return new m.AlertModal( {
          text: m.languages.notices.getModalText( modalText, { winner: winner } ),
          action: () => m.GameScreen.change( new m.OpeningScreen() )
        } ).insert();
      }

      // Define empate
      function setDraw() {
        // Atualiza dados sobre jogadores
        updatePlayerStats: {
          // Apenas executa bloco caso partida não seja uma simulação
          if( match.isSimulation ) break updatePlayerStats;

          // Itera por jogadores
          for( let player of matchPlayers ) {
            // Computa empate para o jogador alvo
            player.user.matches.draws++;

            // Computa empate no baralho usado do jogador alvo
            player.user.decks.find( deck => deck.name == player.gameDeck.name ).matches.draws++;
          }
        }

        // Caso jogador atual seja o ímpar, atualiza servidor sobre resultado da partida
        if( m.GamePlayer.current?.parity == 'odd' ) match.updateServer();

        // Notifica usuários sobre empate
        return new m.AlertModal( {
          text: m.languages.notices.getModalText( 'notify-round-draw' ),
          action: () => m.GameScreen.change( new m.OpeningScreen() )
        } ).insert();
      }
    }

    /// Limpa dados diversos eventualmente definidos ao longo de uma rodada
    round.clearMiscData = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData,
          match = m.GameMatch.current;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == this );
        m.oAssert( match instanceof m.GameMatch );
      }

      // Zera pontos de destino dos jogadores
      match.players.clearFatePoints( eventData );

      // Para nulificação de paradas e da execução da progressão de ações
      m.GameMatch.fateBreakExecution = m.ActionAttack.freeAttackExecution = m.GameAction.progressExecution = null;

      // Limpa arranjos com ações a serem executadas ou progredidas
      for( let key of [ 'executionQueue', 'progressQueue' ] )
        while( m.GameAction[ key ].length ) m.GameAction[ key ].shift();

      // Limpa arranjo de entes afetados de cartas que os têm
      for( let constructor of [ m.ImplementAwarenessPotion, m.ImplementNimblenessPotion ] )
        while( constructor.affectedBeings.length ) constructor.affectedBeings.shift();

      // Limpa arranjo de jogadores que não podem usar 'Apelo Celestial'
      while( m.SpellCelestialAppeal.invalidPlayers.length ) m.SpellCelestialAppeal.invalidPlayers.shift();

      // Limpa arranjo de jogadores que devem perder, via o efeito de 'O Campeão'
      while( m.SpellTheChampion.playersToLose.length ) m.SpellTheChampion.playersToLose.shift();

      // Limpa arranjo de jogadores que já usaram a magia 'O Engodo' na batalha atual
      while( m.SpellTheLure.triggeringPlayers.length ) m.SpellTheLure.triggeringPlayers.shift();

      // Redefine indicador de se magia 'A Trégua' já foi usada na batalha atual
      m.SpellTheTruce.wasUsed = false;

      // Redefine indicador de que vitória de pontos de destino deve ser verificada
      m.SpellTheEnd.isFateVictory = false;
    }
  }
}

/// Propriedades do protótipo
FlowRound.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowRound }
} );
