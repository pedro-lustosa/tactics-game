// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowMove = function ( config = {} ) {
  // Iniciação de propriedades
  FlowMove.init( this );

  // Identificadores
  var { parent, source } = config;

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( parent instanceof m.FlowParity );
    m.oAssert( [ m.GamePlayer, m.Being ].some( constructor => source instanceof constructor ) );
    m.oAssert( !config.requiredMessage || config.isRequired );
    m.oAssert( !this.children.length );
  }

  // Configurações pré-superconstrutor

  /// Atribuição dos nomes
  assignNames: {
    // Identificadores
    let moveParity = parent.parity;

    // Para caso jogada esteja vinculada a um jogador
    if( source instanceof m.GamePlayer ) {
      // Atribuição do nome
      this.name = moveParity + '-' + this.name;

      // Atribuição do nome de exibição
      this.displayName = m.languages.keywords.getFlow( this.flowType );
    }

    // Para caso jogada esteja vinculada a um ente
    else {
      // Identificadores
      let matchFlow = m.GameMatch.current.flow,
          moveId = matchFlow.moves[ moveParity ].filter( move => move.source == source ).length + 1,
          cardId = source.content.designation.getIndex() + 1;

      // Atribuição do nome
      this.name = `${ moveParity }-${ this.name }-${ moveId }-${ source.name }-${ cardId }`;

      // Atribuição do nome de exibição
      this.displayName = m.languages.snippets.getMoveName( moveId, source, cardId );
    }
  }

  /// Atribuição da origem, obrigatoriedade e mensagem de obrigatoriedade
  for( let property of [ 'source', 'isRequired', 'requiredMessage' ] ) this[ property ] = config[ property ] || this[ property ];

  // Superconstrutor
  m.FlowUnit.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowMove.init = function ( move ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( move );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( move instanceof FlowMove );

    // Atribuição de propriedades iniciais

    /// Nome
    move.name = 'move';

    /// Tipo de fluxo
    move.flowType = 'move';

    /// Origem
    move.source = null;

    /// Indica se jogada está aberta
    move.isOpen = false;

    /// Indica grau de desenvolvimento da jogada
    move.development = 0;

    /// Indica se jogada é obrigatória
    move.isRequired = false;

    /// Para uma jogada obrigatória, indica mensagem a ser emitida caso se tente avançar o fluxo sem a concluir
    move.requiredMessage = '';

    /// Adiciona jogada ao palco atual
    move.add = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          parityStage = move.parent,
          flowRostrum = parityStage.getParent( 'period' ) ?? parityStage.getParent( 'turn' ),
          eventTargets = [ this, flowRostrum ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( parityStage instanceof m.FlowParity );
        m.oAssert( [ 'setMoves', 'unsetMoves' ].every( key => typeof flowRostrum[ key ] == 'function' ) );
      }

      // Sinaliza início da adição da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeStart.add.emit( eventTarget, { move: this } );

      // Inseri jogada em arranjos pertinentes
      for( let movesArray of [ matchFlow.moves, matchFlow.moves[ parityStage.parity ], parityStage.children ] ) movesArray.push( move );

      // Para caso se esteja em um turno do preparo, ou no período da remobilização
      if( [ m.SetupTurn, m.RedeploymentPeriod ].some( constructor => flowRostrum instanceof constructor ) ) {
        // Identificadores
        let moveNumber = matchFlow.moves[ parityStage.parity ].length;

        // Adiciona número da jogada a seu nome
        move.name += '-' + moveNumber;

        // Adiciona número da jogada a seu nome de exibição
        move.displayName += ' ' + moveNumber;
      }

      // Sinaliza fim da adição da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeEnd.add.emit( eventTarget, { move: this } );

      // Retorna jogada
      return this;
    }

    /// Desenvolve jogada
    move.develop = function ( degree ) {
      // Identificadores
      var flowRostrum = this.getParent( 'period' ) ?? this.getParent( 'turn' ),
          eventTargets = [ this, flowRostrum ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( degree >= 0 && degree <= 1 );
        m.oAssert( this.isOpen );
        m.oAssert( [ 'setMoves', 'unsetMoves' ].every( key => typeof flowRostrum[ key ] == 'function' ) );
      }

      // Sinaliza início do desenvolvimento da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeStart.develop.emit( eventTarget, { move: this } );

      // Desenvolve jogada
      this.development += Math.min( this.development + degree, 1 );

      // Caso desenvolvimento da jogada seja [igual a/maior que] 1, indica que ela está fechada
      if( this.development >= 1 ) this.isOpen = false;

      // Sinaliza fim do desenvolvimento da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeEnd.develop.emit( eventTarget, { move: this } );

      // Retorna jogada
      return this;
    }

    /// Remove jogada do palco atual
    move.remove = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          moveParity = move.source.parity ?? move.source.getRelationships().owner.parity,
          moveGeneralIndex = matchFlow.moves.indexOf( move ),
          moveParityIndex = matchFlow.moves[ moveParity ].indexOf( move ),
          flowRostrum = this.getParent( 'period' ) ?? this.getParent( 'turn' ),
          eventTargets = [ this, flowRostrum ].filter( stage => stage );

      // Sinaliza início da remoção da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeStart.remove.emit( eventTarget, { move: this } );

      // Caso jogada atualmente exista no arranjo de jogadas, remove-a de lá
      if( moveGeneralIndex != -1 ) matchFlow.moves.splice( moveGeneralIndex, 1 );

      // Caso jogada atualmente exista no arranjo de jogadas por paridade, remove-a de lá
      if( moveParityIndex != -1 ) matchFlow.moves[ moveParity ].splice( moveParityIndex, 1 );

      // Remove jogada de estágio ascendente, quando ainda lá
      move.parent?.children.splice( move.parent.children.indexOf( move ), 1 );

      // Desvincula jogada de estágio ascendente
      move.parent = null;

      // Sinaliza fim da remoção da jogada
      for( let eventTarget of eventTargets ) m.events.moveChangeEnd.remove.emit( eventTarget, { move: this } );

      // Retorna jogada
      return this;
    }
  }
}

/// Propriedades do protótipo
FlowMove.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowMove }
} );
