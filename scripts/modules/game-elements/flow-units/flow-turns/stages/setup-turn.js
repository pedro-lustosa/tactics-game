// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SetupTurn = function ( config = {} ) {
  // Iniciação de propriedades
  SetupTurn.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ m.ArrangementStep, m.DeploymentStep, m.AllocationStep ].some( flowUnit => config.parent instanceof flowUnit ) );

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = config.parent.name.replace( /-.+/g, '' ) + '-' + this.name;

  // Superconstrutor
  m.FlowTurn.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.FlowParity( { parent: this, parity: 'odd' } ),
    new m.FlowParity( { parent: this, parity: 'even' } )
  );

  // Eventos

  /// Para geração de ações
  m.events.flowChangeStart.begin.add( this, this.setActions );

  /// Para remoção de ações
  m.events.flowChangeEnd.finish.add( this, this.unsetActions );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SetupTurn.init = function ( turn ) {
    // Chama 'init' ascendente
    m.FlowTurn.init( turn );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( turn instanceof SetupTurn );

    // Atribuição de propriedades iniciais

    /// Nome
    turn.name = 'setup-turn';

    /// Gera as jogadas do turno
    turn.setMoves = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );

      // Identificadores pós-validação
      var match = m.GameMatch.current,
          parityStages = this.children,
          isRequired = this.parent instanceof m.DeploymentStep && this.counter == 1;

      // Itera por estágios de paridade
      for( let parityStage of parityStages ) {
        // Identificadores
        let requiredMessage = isRequired ? getRequiredMessage( parityStage ) : '';

        // Gera jogada para o estágio alvo, e a adiciona ao fluxo
        new m.FlowMove( {
          parent: parityStage,
          source: match.players[ parityStage.parity ],
          isRequired: isRequired,
          requiredMessage: requiredMessage
        } ).add();
      }

      // Retorna turno
      return this;

      // Funções

      /// Retorna mensagem de jogada obrigatória a ser enviada
      function getRequiredMessage( parityStage ) {
        // Validação
        if( m.app.isInDevelopment ) m.oAssert( parityStage instanceof m.FlowParity );

        // Identificadores
        var messageName = 'must-deploy-commander',
            parityPlayer = match.players[ parityStage.parity ],
            playerSpells = [ 'odd', 'even' ].flatMap( parity => match.decks[ parity ].cards.spells );

        // Caso haja uma magia 'O Regente' em uso para o jogador da paridade alvo, ajusta mensagem de jogada obrigatória
        if( playerSpells.some( spell => spell instanceof m.SpellTheRuler && spell.content.effects.isEnabled && spell.triggeringPlayer == parityPlayer ) )
          messageName = 'must-deploy-commander-or-ruler';

        // Retorna mensagem de jogada obrigatória
        return m.languages.notices.getFlowText( messageName );
      }
    }

    /// Remove as jogadas do turno, quando concluído
    turn.unsetMoves = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == this );

      // Remoção das jogadas
      while( m.GameMatch.current.flow.moves.length ) m.GameMatch.current.flow.moves[ 0 ].remove();

      // Retorna turno
      return this;
    }

    /// Gera as ações do turno, segundo sua etapa
    turn.setActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget, targetCards } = eventData,
          currentStep = this.getParent( 'step' );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );
        m.oAssert( !targetCards || Array.isArray( targetCards ) );
        m.oAssert( currentStep instanceof m.FlowStep );
      }

      // Delega configuração das ações para a etapa atual
      currentStep.setActions( targetCards );

      // Retorna turno
      return this;
    }

    /// Remove as ações do turno, segundo sua etapa
    turn.unsetActions = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget, targetCards } = eventData,
          currentStep = this.getParent( 'step' );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == this );
        m.oAssert( !targetCards || Array.isArray( targetCards ) );
        m.oAssert( currentStep instanceof m.FlowStep );
      }

      // Delega configuração das ações para a etapa atual
      currentStep.unsetActions( targetCards );

      // Retorna turno
      return this;
    }

    /// Retorna paridades do turno
    turn.getParities = function () {
      // Retorna arranjo com as paridades
      return [ this.children ];
    }
  }
}

/// Propriedades do protótipo
SetupTurn.prototype = Object.create( m.FlowTurn.prototype, {
  // Construtor
  constructor: { value: SetupTurn }
} );
