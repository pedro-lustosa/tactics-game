// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const BattleTurn = function ( config = {} ) {
  // Iniciação de propriedades
  BattleTurn.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.BattleStep );

  // Superconstrutor
  m.FlowTurn.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.BreakPeriod( { name: 'pre-combat', parent: this } ),
    new m.CombatPeriod( { parent: this } ),
    new m.BreakPeriod( { name: 'post-combat', parent: this } ),
    new m.RedeploymentPeriod( { parent: this } )
  );

  // Eventos

  /// Para ordenação das paridades no novo turno
  m.events.flowChangeStart.begin.add( this, this.setParityOrder );

  /// Para limpeza dos pontos de destino dos jogadores
  m.events.flowChangeEnd.finish.add( this, eventData => m.GameMatch.current.players.clearFatePoints( eventData ) );

  /// Para redefinição dos pontos de destino embutidos
  m.events.flowChangeEnd.finish.add( this, m.mixinFatePoints.resetAllFatePoints, { context: m.mixinFatePoints } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BattleTurn.init = function ( turn ) {
    // Chama 'init' ascendente
    m.FlowTurn.init( turn );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( turn instanceof BattleTurn );

    // Atribuição de propriedades iniciais

    /// Nome
    turn.name = 'battle-turn';

    // Retorna paridades do turno
    turn.getParities = function () {
      // Retorna arranjo com as paridades de todos os períodos
      return this.children.flatMap( period => period.getParities() );
    }

    // Define ordem das paridades segundo turno atual
    turn.setParityOrder = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !eventCategory || eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );

      // Identificadores pós-validação
      var paritiesArray = this.getParities(),
          isOddFirst = !this.counter || Boolean( this.counter % 2 );

      // Itera por paridades do turno
      for( let parities of paritiesArray ) {
        // Identificadores
        let firstParity = parities[ 0 ].parity;

        // Quando aplicável, inverte ordem das paridades do conjunto alvo
        if( isOddFirst && firstParity == 'even' || !isOddFirst && firstParity == 'odd' ) parities.push( parities.shift() );
      }
    }
  }
}

/// Propriedades do protótipo
BattleTurn.prototype = Object.create( m.FlowTurn.prototype, {
  // Construtor
  constructor: { value: BattleTurn }
} );
