// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowTurn = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( typeof this.getParities == 'function' );

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Identificadores
  var parentStep = this.getParent( 'step' );

  // Eventos

  /// Para verificar condições para o fim da etapa atual
  m.events.flowChangeStart.finish.add( this, parentStep.checkEndConditions, { context: parentStep } );

  /// Para redefinição do contador do turno ao fim da progressão de sua etapa
  m.events.flowChangeEnd.progress.add( parentStep, this.resetCounter, { context: this } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowTurn.init = function ( turn ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( turn );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( turn instanceof FlowTurn );

    // Atribuição de propriedades iniciais

    /// Nome de exibição
    turn.displayName = m.languages.keywords.getFlow( turn.name );

    /// Tipo de fluxo
    turn.flowType = 'turn';

    /// Contador de turnos
    turn.counter = 0;

    /// Prepara novo turno
    turn.setNewTurn = function () {
      // Apenas executa função se turno estiver ativo
      if( !this.isActive ) return false;

      // Sinaliza início da mudança de fluxo
      m.events.flowChangeStart.finish.emit( this );

      // Sinaliza fim da mudança de fluxo
      m.events.flowChangeEnd.finish.emit( this );

      // Encerra função caso turno não esteja mais ativo
      if( !this.isActive ) return this;

      // Sinaliza início da mudança de fluxo
      m.events.flowChangeStart.begin.emit( this );

      // Redefine estágio descendente ativo do turno
      this.children.active = this.children[ 0 ];

      // Sinaliza fim da mudança de fluxo
      m.events.flowChangeEnd.begin.emit( this );

      // Retorna turno
      return this;
    }

    /// Atualiza contagem do turno
    turn.incrementCounter = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );

      // Incremento do contador, e atualização do nome
      this.displayName = `${ m.languages.keywords.getFlow( this.flowType ) } ${ ++this.counter }`;

      // Retorna turno
      return this;
    }

    /// Redefine contador do turno
    turn.resetCounter = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'progress' && eventTarget == this.getParent( 'step' ) );

      // Redefine nome de exibição
      this.displayName = m.languages.keywords.getFlow( this.flowType );

      // Zera contador
      this.counter = 0;

      // Quando aplicável, redefine ordem das paridades
      this.setParityOrder?.();

      // Retorna turno
      return this;
    }
  }
}

/// Propriedades do protótipo
FlowTurn.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowTurn }
} );
