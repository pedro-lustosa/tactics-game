// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowParity = function ( config = {} ) {
  // Iniciação de propriedades
  FlowParity.init( this );

  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ 'odd', 'even' ].includes( config.parity ) );
    m.oAssert( [ m.SetupTurn, m.FlowSegment, m.RedeploymentPeriod ].some( flowUnit => config.parent instanceof flowUnit ) );
  }

  // Configurações pré-superconstrutor

  /// Atribuição do nome
  this.name = `${config.parity}-${this.name}`;

  /// Atribuição do nome de exibição
  this.displayName = m.languages.keywords.getFlow( this.name );

  /// Atribuição da paridade
  this.parity = config.parity;

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Eventos

  /// Para partidas simuladas, alterna jogador atual, para que ele seja sempre igual ao controlador do fluxo
  m.events.flowChangeEnd.begin.add( this, eventData => m.GameMatch.current.players.toggleCurrent( eventData ) );

  /// Abertura de jogadas, e preparo dos entes
  m.events.flowChangeEnd.begin.add( this, eventData => m.GameMatch.current.flow.openMoves( eventData ) );

  /// Registro da cadeia de fluxo atual
  m.events.flowChangeEnd.begin.add( this, eventData => m.GameMatch.current.log.addFlowChain( eventData ) );

  /// Sincronização do fluxo entre sockets de uma partida
  m.events.flowChangeEnd.begin.add( this, eventData => m.GameMatch.current.flow.updateSynchronization( eventData ) );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowParity.init = function ( parity ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( parity );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( parity instanceof FlowParity );

    // Atribuição de propriedades iniciais

    /// Nome
    parity.name = 'parity';

    /// Tipo de fluxo
    parity.flowType = 'parity';

    /// Paridade
    parity.parity = '';
  }
}

/// Propriedades do protótipo
FlowParity.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowParity }
} );
