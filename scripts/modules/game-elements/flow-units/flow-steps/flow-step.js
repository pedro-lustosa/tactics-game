// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowStep = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( typeof this.checkEndConditions == 'function' );

  // Superconstrutor
  m.FlowUnit.call( this, config );

  // Eventos

  /// Para limpeza de informações relativas a ações de cartas
  m.events.flowChangeStart.finish.add( this, m.GameAction.clearCardsData, { context: m.GameAction } );

  /// Para controle dos textos suspensos de cartas
  m.events.flowChangeStart.transit.add( this, m.Card.controlHoverText, { context: m.Card } );

  /// Para redefinição do contador da etapa ao fim de sua rodada
  m.events.flowChangeEnd.finish.add( this.getParent( 'round' ), this.resetCounter, { context: this } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowStep.init = function ( step ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( step );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( step instanceof FlowStep );

    // Atribuição de propriedades iniciais

    /// Tipo de fluxo
    step.flowType = 'step';

    /// Contador de etapas
    step.counter = 0;

    /// Atualiza contagem de etapas
    step.incrementCounter = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'begin' && eventTarget == this );

      // Incrementa contador
      ++this.counter;

      // Retorna etapa
      return this;
    }

    /// Redefine contador da etapa
    step.resetCounter = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'flow-change' && eventType == 'finish' && eventTarget == this.getParent( 'round' ) );

      // Zera contador
      this.counter = 0;

      // Retorna etapa
      return this;
    }
  }
}

/// Propriedades do protótipo
FlowStep.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowStep }
} );
