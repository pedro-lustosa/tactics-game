// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const AllocationStep = function ( config = {} ) {
  // Iniciação de propriedades
  AllocationStep.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.SetupPhase );

  // Superconstrutor
  m.FlowStep.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.SetupTurn( { parent: this } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  AllocationStep.init = function ( step ) {
    // Chama 'init' ascendente
    m.FlowStep.init( step );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( step instanceof AllocationStep );

    // Atribuição de propriedades iniciais

    /// Nome
    step.name = 'allocation-step';

    /// Nome de exibição
    step.displayName = m.languages.keywords.getFlow( step.name );

    /// Gera as ações da etapa, por turno do preparo
    step.setActions = function ( targetBeings ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );

      // Define entes a terem suas ações configuradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipTokens: true, skipSideDeck: true } );

      // Itera por entes a se conceder ações
      for( let being of targetBeings ) {
        // Controle de 'equipar'
        equipControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break equipControl;

          // Caso baralho de ente não tenha equipamentos, encerra bloco
          if( !m.GameMatch.current.decks[ being.deck.owner.parity ].cards.items.length ) break equipControl;

          // Executa controle da ação 'Equipar'
          m.ActionEquip.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Equipar'
          m.events.cardActivityEnd.any.add( being, m.ActionEquip.controlAvailability, { context: m.ActionEquip } );
        }

        // Controle de 'manejar'
        handleControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break handleControl;

          // Executa controle da ação 'Manejar'
          m.ActionHandle.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Manejar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );

          /// De mudança de equipamento
          m.events.cardContentEnd.item.add( being, m.ActionHandle.controlAvailability, { context: m.ActionHandle } );
        }

        // Controle de 'canalizar'
        channelControl: {
          // Caso ente alvo não seja um canalizador, encerra bloco
          if( !being.content.stats.combativeness.channeling ) break channelControl;

          // Caso baralho de ente não tenha magias persistentes, encerra bloco
          if( !m.GameMatch.current.decks[ being.deck.owner.parity ].cards.spells.some( spell => spell.content.typeset.duration == 'persistent' ) )
            break channelControl;

          // Executa controle da ação 'Canalizar'
          m.ActionChannel.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Canalizar'
          m.events.cardActivityEnd.any.add( being, m.ActionChannel.controlAvailability, { context: m.ActionChannel } );
        }
      }

      // Retorna etapa
      return this;
    }

    /// Remove as ações da etapa, por turno do preparo
    step.unsetActions = function ( targetBeings ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );

      // Define entes a terem suas ações desconfiguradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipTokens: true, skipSideDeck: true } );

      // Itera por entes a se remover ações
      for( let being of targetBeings ) {
        // Remove ações do ente alvo
        while( being.actions.length ) being.actions.shift();

        // Quando existentes, remove eventos de atividade para o controle de ações
        for( let action of [ m.ActionEquip, m.ActionHandle, m.ActionChannel ] )
          m.events.cardActivityEnd.any.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de mudança de equipamentos para o controle de ações
        for( let action of [ m.ActionHandle ] )
          m.events.cardContentEnd.item.remove( being, action.controlAvailability, { context: action } );
      }

      // Retorna etapa
      return this;
    }

    /// Verifica condições para o fim da etapa
    step.checkEndConditions = function ( eventData = {} ) {
      // Não executa função caso partida já tenha sido concluída
      if( m.GameMatch.current?.stats.isFinished ) return;

      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventPhase == 'start' && eventType == 'finish' && eventTarget instanceof m.SetupTurn );

      // Identificadores pós-validação
      var matchFlow = m.GameMatch.current.flow,
          finishStep = () => m.events.flowChangeEnd.finish.add( matchFlow.turn, matchFlow.step.progress, { context: matchFlow.step, once: true } );

      // Avança fluxo caso seu turno tenha se encerrado sem que ao menos 1 de suas jogadas tenha sido concluída
      if( matchFlow.moves.every( move => move.development < 1 ) ) return finishStep();
    }
  }
}

/// Propriedades do protótipo
AllocationStep.prototype = Object.create( m.FlowStep.prototype, {
  // Construtor
  constructor: { value: AllocationStep }
} );
