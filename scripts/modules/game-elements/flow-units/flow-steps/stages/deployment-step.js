// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const DeploymentStep = function ( config = {} ) {
  // Iniciação de propriedades
  DeploymentStep.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.SetupPhase );

  // Superconstrutor
  m.FlowStep.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.SetupTurn( { parent: this } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  DeploymentStep.init = function ( step ) {
    // Chama 'init' ascendente
    m.FlowStep.init( step );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( step instanceof DeploymentStep );

    // Atribuição de propriedades iniciais

    /// Nome
    step.name = 'deployment-step';

    /// Nome de exibição
    step.displayName = m.languages.keywords.getFlow( step.name );

    /// Gera as ações da etapa, por turno do preparo
    step.setActions = function ( targetBeings ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );

      // Define entes a terem suas ações configuradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipTokens: true, skipSideDeck: true } );

      // Itera por entes a se conceder ações
      for( let being of targetBeings ) {
        // Caso turno da mobilização seja o primeiro, filtra todos os entes que não sejam comandantes e não estejam vinculados a "O Regente"
        if( this.getChild( 'deployment-setup-turn' ).counter == 1 )
          if( !being.content.stats.command && !being.getAllCardAttachments().some( card => card instanceof m.SpellTheRuler && card.content.effects.isEnabled ) )
            continue;

        // Controle de 'mobilizar'
        deployControl: {
          // Executa controle da ação 'Mobilizar'
          m.ActionDeploy.controlAvailability( { eventTarget: being } );

         // Adiciona evento para o controle da disponibilidade de ação 'Mobilizar'
         m.events.cardActivityEnd.any.add( being, m.ActionDeploy.controlAvailability, { context: m.ActionDeploy } );
        }

        // Controle de 'convocar'
        summonControl: {
          // Caso ente alvo não seja um humanoide, encerra bloco
          if( !( being instanceof m.Humanoid ) ) break summonControl;

          // Executa controle da ação 'Convocar'
          m.ActionSummon.controlAvailability( { eventTarget: being } );

          // Adiciona eventos para o controle da disponibilidade de ação 'Convocar'

          /// De mudança de atividade
          m.events.cardActivityEnd.any.add( being, m.ActionSummon.controlAvailability, { context: m.ActionSummon } );

          /// De mudança de uso
          m.events.cardUseEnd.useInOut.add( being, m.ActionSummon.controlAvailability, { context: m.ActionSummon } );
        }

        // Controle de 'posicionar'
        relocateControl: {
          // Caso ente alvo não seja um metadílio, encerra bloco
          if( !( being instanceof m.Halfling ) ) break relocateControl;

          // Executa controle da ação 'Posicionar'
          m.ActionRelocate.controlAvailability( { eventTarget: being } );

          // Adiciona evento para o controle da disponibilidade de ação 'Posicionar'
          m.events.cardActivityEnd.any.add( being, m.ActionRelocate.controlAvailability, { context: m.ActionRelocate } );
        }
      }

      // Retorna etapa
      return this;
    }

    /// Remove as ações da etapa, por turno do preparo
    step.unsetActions = function ( targetBeings ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetBeings || Array.isArray( targetBeings ) && targetBeings.every( being => being instanceof m.Being ) );

      // Define entes a terem suas ações desconfiguradas, caso entes específicos não tenham sido passados
      targetBeings ||= m.GameMatch.current.decks.getAllCards( { skipAssets: true, skipTokens: true, skipSideDeck: true } );

      // Itera por entes a se remover ações
      for( let being of targetBeings ) {
        // Remove ações do ente alvo
        while( being.actions.length ) being.actions.shift();

        // Quando existentes, remove eventos de atividade para o controle de ações
        for( let action of [ m.ActionDeploy, m.ActionSummon, m.ActionRelocate ] )
          m.events.cardActivityEnd.any.remove( being, action.controlAvailability, { context: action } );

        // Quando existentes, remove eventos de uso para o controle de ações
        for( let action of [ m.ActionSummon ] )
          m.events.cardUseEnd.useInOut.remove( being, action.controlAvailability, { context: action } );
      }

      // Retorna etapa
      return this;
    }

    /// Verifica condições para o fim da etapa
    step.checkEndConditions = function ( eventData = {} ) {
      // Não executa função caso partida já tenha sido concluída
      if( m.GameMatch.current?.stats.isFinished ) return;

      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventPhase == 'start' && eventType == 'finish' && eventTarget instanceof m.SetupTurn );

      // Identificadores pós-validação
      var matchFlow = m.GameMatch.current.flow,
          finishStep = () => m.events.flowChangeEnd.finish.add( matchFlow.turn, matchFlow.step.progress, { context: matchFlow.step, once: true } );

      // Avança fluxo caso seu turno tenha se encerrado sem que ao menos 1 de suas jogadas tenha sido concluída
      if( matchFlow.moves.every( move => move.development < 1 ) ) return finishStep();
    }
  }
}

/// Propriedades do protótipo
DeploymentStep.prototype = Object.create( m.FlowStep.prototype, {
  // Construtor
  constructor: { value: DeploymentStep }
} );
