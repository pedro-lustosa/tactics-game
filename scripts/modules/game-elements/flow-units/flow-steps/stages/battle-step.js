// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const BattleStep = function ( config = {} ) {
  // Iniciação de propriedades
  BattleStep.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.BattlePhase );

  // Superconstrutor
  m.FlowStep.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.BattleTurn( { parent: this } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BattleStep.init = function ( step ) {
    // Chama 'init' ascendente
    m.FlowStep.init( step );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( step instanceof BattleStep );

    // Atribuição de propriedades iniciais

    /// Nome
    step.name = 'battle-step';

    /// Nome de exibição
    step.displayName = m.languages.keywords.getFlow( step.name );

    /// Verifica condições para o fim da etapa
    step.checkEndConditions = function ( eventData = {} ) {
      // Não executa função caso partida já tenha sido concluída
      if( m.GameMatch.current?.stats.isFinished ) return;

      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'flow-change' && eventPhase == 'start' && eventType == 'finish' );
        m.oAssert( [ m.BattleTurn, m.BreakPeriod ].some( constructor => eventTarget instanceof constructor ) );
      }

      // Identificadores pós-validação
      var { stats: matchStats, players: matchPlayers, environments: matchEnvironments, flow: matchFlow } = m.GameMatch.current;

      // Verifica quantidade de pontos de destino dos jogadores
      checkFatePoints: {
        // Não executa bloco caso vitória de pontos de destino não tenha sido provocada
        if( !m.SpellTheEnd.isFateVictory ) break checkFatePoints;

        // Identificadores
        let fatePoints = Object.fromEntries( matchPlayers.get( 'all' ).map( player => [ player.parity, player.fatePoints.current.get() ] ) );

        // Caso o ímpar tenha mais pontos de destino que o par, sinaliza-o como vencedor
        if( fatePoints.odd > fatePoints.even ) matchPlayers.odd.isToWin = true

        // Caso o par tenha mais pontos de destino que o ímpar, sinaliza-o como vencedor
        else if( fatePoints.even > fatePoints.odd ) matchPlayers.even.isToWin = true;

        // Avança fluxo
        return finishStep();
      }

      // Verifica comandantes inativos
      checkInactiveCommanders: {
        // Itera por paridades
        for( let parity of [ 'odd', 'even' ] ) {
          // Identificadores
          let { commander, beings: playerBeings } = matchPlayers[ parity ].gameDeck.cards,
              oppositeParity = parity == 'odd' ? 'even' : 'odd';

          // Para caso magia 'O Campeão' sinalize que o jogador alvo deve perder
          if( m.SpellTheChampion.playersToLose.includes( matchPlayers[ parity ] ) ) {
            // Habilita vitória para jogador oposto
            matchPlayers[ oppositeParity ].isToWin = true;

            // Avança iteração
            continue;
          }

          // Caso comandante do jogador alvo esteja ativo, avança iteração
          if( commander.activity == 'active' ) continue;

          // Caso magia 'O Regente' impeça que jogador alvo seja afastado devido à inativação do comandante, avança iteração
          if( m.SpellTheRuler.playersWithRuler.includes( matchPlayers[ parity ] ) ) continue;

          // Caso comandante do jogador seja o Gaspar e ainda haja ao menos 1 tropa de nível 4, avança iteração
          if( commander instanceof m.CommanderGaspar && playerBeings.some( card => card.activity == 'active' && card.content.typeset.experience?.level == 4 ) )
            continue;

          // Habilita vitória para jogador oposto
          matchPlayers[ oppositeParity ].isToWin = true;
        }
      }

      // Avança fluxo caso ao menos um dos jogadores tenha alcançado uma condição de vitória
      if( [ 'odd', 'even' ].some( parity => matchPlayers[ parity ].isToWin ) ) return finishStep();

      // Verifica quantidade de zonas de engajamento controladas
      checkEngagementZonesControl: {
        // Apenas executa bloco caso turno concluído tenha sido o 6
        if( matchFlow.turn.counter < 6 ) break checkEngagementZonesControl;

        // Identificadores
        let engagementZones = matchEnvironments.field.divider.slots,
            oddZones = engagementZones.filter( slot => slot.getController() == matchPlayers.odd ).length,
            evenZones = engagementZones.filter( slot => slot.getController() == matchPlayers.even ).length;

        // Caso o ímpar controle mais zonas que o par, sinaliza-o como vencedor
        if( oddZones > evenZones ) matchPlayers.odd.isToWin = true

        // Caso o par controle mais zonas que o ímpar, sinaliza-o como vencedor
        else if( evenZones > oddZones ) matchPlayers.even.isToWin = true;

        // Avança fluxo
        return finishStep();
      }

      // Funções

      /// Conclui etapa atual
      function finishStep() {
        // Indica que partida foi concluída
        matchStats.isFinished = true;

        // Adiciona evento para concluir etapa após conclusão de turno ou período intersticial atual
        m.events.flowChangeEnd.finish.add( eventTarget, matchFlow.step.progress, { context: matchFlow.step, once: true } )
      }
    }
  }
}

/// Propriedades do protótipo
BattleStep.prototype = Object.create( m.FlowStep.prototype, {
  // Construtor
  constructor: { value: BattleStep }
} );
