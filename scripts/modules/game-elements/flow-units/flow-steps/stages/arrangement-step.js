// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ArrangementStep = function ( config = {} ) {
  // Iniciação de propriedades
  ArrangementStep.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.SetupPhase );

  // Superconstrutor
  m.FlowStep.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.SetupTurn( { parent: this } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ArrangementStep.init = function ( step ) {
    // Chama 'init' ascendente
    m.FlowStep.init( step );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( step instanceof ArrangementStep );

    // Atribuição de propriedades iniciais

    /// Nome
    step.name = 'arrangement-step';

    /// Nome de exibição
    step.displayName = m.languages.keywords.getFlow( step.name );

    /// Gera as ações da etapa, por turno do preparo
    step.setActions = function ( targetCards ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetCards || Array.isArray( targetCards ) && targetCards.every( card => card instanceof m.Card ) );

      // Define cartas a terem suas ações configuradas, caso cartas específicas não tenham sido passadas
      targetCards ||= m.GameMatch.current.decks.getAllCards( { skipTokens: true } );

      // Itera por cartas a se conceder ações
      for( let card of targetCards ) {
        // Filtra comandantes
        if( card.content.stats.command ) continue;

        // Controle de 'trocar'
        swapControl: {
          // Executa controle da ação 'Trocar'
          m.ActionSwap.controlAvailability( { eventTarget: card } );

         // Adiciona evento para o controle da disponibilidade de ação 'Trocar'
         m.events.deckChangeEnd.any.add( card, m.ActionSwap.controlAvailability, { context: m.ActionSwap } );
        }
      }

      // Retorna etapa
      return this;
    }

    /// Remove as ações da etapa, por turno do preparo
    step.unsetActions = function ( targetCards ) {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !targetCards || Array.isArray( targetCards ) && targetCards.every( card => card instanceof m.Card ) );

      // Define cartas a terem suas ações desconfiguradas, caso cartas específicas não tenham sido passadas
      targetCards ||= m.GameMatch.current.decks.getAllCards( { skipTokens: true } );

      // Itera por cartas a se remover ações
      for( let card of targetCards ) {
        // Remove ações da carta alvo
        while( card.actions.length ) card.actions.shift();

        // Quando existente, remove evento para o controle da disponibilidade de ação 'Trocar'
        m.events.deckChangeEnd.any.remove( card, m.ActionSwap.controlAvailability, { context: m.ActionSwap } );
      }

      // Retorna etapa
      return this;
    }

    /// Verifica condições para o fim da etapa
    step.checkEndConditions = function ( eventData = {} ) {
      // Não executa função caso partida já tenha sido concluída
      if( m.GameMatch.current?.stats.isFinished ) return;

      // Identificadores pré-validação
      var { eventCategory, eventPhase, eventType, eventTarget } = eventData;

      // Validação
      if( m.app.isInDevelopment )
        m.oAssert( eventCategory == 'flow-change' && eventPhase == 'start' && eventType == 'finish' && eventTarget instanceof m.SetupTurn );

      // Identificadores pós-validação
      var matchFlow = m.GameMatch.current.flow,
          finishStep = () => m.events.flowChangeEnd.finish.add( matchFlow.turn, matchFlow.step.progress, { context: matchFlow.step, once: true } );

      // Avança fluxo caso seu turno tenha se encerrado sem que ao menos 1 de suas jogadas tenha sido concluída
      if( matchFlow.moves.every( move => move.development < 1 ) ) return finishStep();

      // Avança fluxo caso turno concluído tenha sido o 5
      if( matchFlow.turn.counter >= 5 ) return finishStep();
    }
  }
}

/// Propriedades do protótipo
ArrangementStep.prototype = Object.create( m.FlowStep.prototype, {
  // Construtor
  constructor: { value: ArrangementStep }
} );
