// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const SetupPhase = function ( config = {} ) {
  // Iniciação de propriedades
  SetupPhase.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.FlowRound );

  // Superconstrutor
  m.FlowPhase.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.ArrangementStep( { parent: this } ),
    new m.DeploymentStep( { parent: this } ),
    new m.AllocationStep( { parent: this } )
  );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  SetupPhase.init = function ( phase ) {
    // Chama 'init' ascendente
    m.FlowPhase.init( phase );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( phase instanceof SetupPhase );

    // Atribuição de propriedades iniciais

    /// Nome
    phase.name = 'setup-phase';

    /// Nome de exibição
    phase.displayName = m.languages.keywords.getFlow( phase.name );
  }
}

/// Propriedades do protótipo
SetupPhase.prototype = Object.create( m.FlowPhase.prototype, {
  // Construtor
  constructor: { value: SetupPhase }
} );
