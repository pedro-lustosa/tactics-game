// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const BattlePhase = function ( config = {} ) {
  // Iniciação de propriedades
  BattlePhase.init( this );

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( config.parent instanceof m.FlowRound );

  // Superconstrutor
  m.FlowPhase.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição dos estágios descendentes
  this.children.push(
    new m.BattleStep( { parent: this } )
  );

  // Eventos

  /// Para geração dos símbolos dos entes nos baralhos principais
  m.events.flowChangeEnd.begin.add( this, m.Being.buildDeckSymbols, { context: m.Being } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BattlePhase.init = function ( phase ) {
    // Chama 'init' ascendente
    m.FlowPhase.init( phase );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( phase instanceof BattlePhase );

    // Atribuição de propriedades iniciais

    /// Nome
    phase.name = 'battle-phase';

    /// Nome de exibição
    phase.displayName = m.languages.keywords.getFlow( phase.name );
  }
}

/// Propriedades do protótipo
BattlePhase.prototype = Object.create( m.FlowPhase.prototype, {
  // Construtor
  constructor: { value: BattlePhase }
} );
