// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowPhase = function ( config = {} ) {
  // Superconstrutor
  m.FlowUnit.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowPhase.init = function ( phase ) {
    // Chama 'init' ascendente
    m.FlowUnit.init( phase );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( phase instanceof FlowPhase );

    // Atribuição de propriedades iniciais

    /// Tipo de fluxo
    phase.flowType = 'phase';
  }
}

/// Propriedades do protótipo
FlowPhase.prototype = Object.create( m.FlowUnit.prototype, {
  // Construtor
  constructor: { value: FlowPhase }
} );
