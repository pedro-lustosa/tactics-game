// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const FlowUnit = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( [ this.name, this.displayName, this.flowType ].every( value => value && typeof value == 'string' ) );
    for( let set of [ [ 'setMoves', 'unsetMoves' ], [ 'setActions', 'unsetActions' ] ] )
      m.oAssert( set.every( key => !( key in this ) ) || set.every( key => typeof this[ key ] == 'function' ) );
  }

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Configurações pós-superconstrutor

  /// Atribuição do estágio ascendente
  this.parent = config.parent || this.parent;

  // Eventos

  /// Para estágios com contadores, adiciona evento para incrementar contador e atualizar nome de exibição
  if( this.incrementCounter ) m.events.flowChangeStart.begin.add( this, this.incrementCounter );

  /// Para palcos do fluxo
  if( this.setMoves ) {
    // Evento para geração de jogadas
    m.events.flowChangeStart.begin.add( this, this.setMoves );

    // Evento para remoção de jogadas
    m.events.flowChangeEnd.finish.add( this, this.unsetMoves );
  }
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  FlowUnit.init = function ( flowUnit ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( flowUnit instanceof FlowUnit );

    // Atribuição de propriedades iniciais

    /// Nome
    flowUnit.name = '';

    /// Nome de exibição
    flowUnit.displayName = '';

    /// Tipo de fluxo
    flowUnit.flowType = '';

    /// Estágio ascendente
    flowUnit.parent = null;

    /// Estágios descendentes
    flowUnit.children = [];

    /// Indica se estágio está atualmente ativo
    flowUnit.isActive = false;

    /// Inicia estágio
    flowUnit.begin = function ( childToBegin = 0 ) {
      // Não executa função se estágio já estiver ativo
      if( this.isActive ) return false;

      // Identificadores
      var matchFlow = m.GameMatch.current?.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( matchFlow );
        m.oAssert( !this.parent || this.parent.isActive );
        m.oAssert( [ this, this.getParent( 'round' ) ].includes( matchFlow.round ) );
        m.oAssert( !matchFlow[ this.flowType ]?.isActive );
      }

      // Para estágios outros que jogadas, sinaliza início da mudança de fluxo
      if( !( this instanceof m.FlowMove ) ) m.events.flowChangeStart.begin.emit( this );

      // Ativa estágio
      this.isActive = true;

      // Na partida, atualiza unidade de fluxo correspondente ao estágio
      matchFlow[ this.flowType ] = this;

      // Se com estágios descendentes, ativa o primeiro, ou o passado para a função
      if( this.children.length ) this.children.active = this.children[ childToBegin ].begin();

      // Para estágios outros que jogadas, sinaliza fim da mudança de fluxo
      if( !( this instanceof m.FlowMove ) ) m.events.flowChangeEnd.begin.emit( this );

      // Retorna estágio
      return this;
    }

    /// Progride por estágio
    flowUnit.progress = function () {
      // Apenas executa função se estágio estiver ativo
      if( !this.isActive ) return false;

      // Apenas executa função se estágio tiver estágios descendentes
      if( !this.children.length ) return false;

      // Sinaliza início da mudança de fluxo
      m.events.flowChangeStart.progress.emit( this );

      // Encerra estágio descendente atualmente ativo
      this.children.active.finish();

      // Não continua função caso estágio não esteja mais ativo
      if( !this.isActive ) return this;

      // Atualiza estágio descendente atualmente ativo
      this.children.active = this.children[ this.children.indexOf( this.children.active ) + 1 ] || null;

      // Se estágio for um turno e não houver mais um estágio descendente ativo, prepara novo turno
      if( this instanceof m.FlowTurn && !this.children.active ) this.setNewTurn();

      // Para caso não exista mais um estágio descendente ativo
      if( !this.children.active ) {
        // Desativa a si mesmo
        this.finish();

        // Se existente, progride estágio ascendente
        this.parent?.progress();
      }
      // Para caso exista um estágio descendente ativo
      else {
        // Inicia novo estágio descendente ativo
        this.children.active.begin();
      }

      // Sinaliza fim da mudança de fluxo
      m.events.flowChangeEnd.progress.emit( this );

      // Retorna estágio
      return this;
    }

    /// Conclui estágio
    flowUnit.finish = function () {
      // Apenas executa função se estágio estiver ativo
      if( !this.isActive ) return false;

      // Para estágios outros que jogadas, sinaliza início da mudança de fluxo
      if( !( this instanceof m.FlowMove ) ) m.events.flowChangeStart.finish.emit( this );

      // Para caso haja um estágio descendente ativo
      if( this.children.active ) {
        // Finaliza estágio descendente ativo
        this.children.active.finish();

        // Nulifica estágio descendente ativo
        this.children.active = null;
      }

      // Desativa estágio
      this.isActive = false;

      // Na partida, caso estágio não seja uma rodada, deixa de o apontar como o ativo de sua unidade de fluxo
      if( !( this instanceof m.FlowRound ) ) m.GameMatch.current.flow[ this.flowType ] = null;

      // Para estágios outros que jogadas, sinaliza fim da mudança de fluxo
      if( !( this instanceof m.FlowMove ) ) m.events.flowChangeEnd.finish.emit( this );

      // Retorna estágio
      return this;
    }

    /// Retorna estágio ascendente
    flowUnit.getParent = function ( parentType ) {
      // Identificadores
      var parent = this;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( parentType && typeof parentType == 'string' );

      // Itera por ascendentes de alvo até encontrar um que seja do tipo de fluxo passado
      while( parent = parent.parent )
        if( parent.flowType == parentType ) return parent;

      // Se nenhum estágio do tipo de fluxo passado for um ascendente de alvo, retorna nulo
      return null;
    }

    /// Retorna estágio descendente
    flowUnit.getChild = function ( childName ) {
      // Identificadores
      var targetChild = null;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( childName && typeof childName == 'string' );

      // Itera por descendentes diretos
      for( let child of this.children ) {
        // Caso descendente tenha sido encontrado, retorna-o
        if( child.name == childName ) return child;

        // Tenta encontrar estágio alvo de modo recursivo
        targetChild = child.getChild( childName ) || targetChild;

        // Caso estágio alvo tenha sido encontrado, encerra iteração
        if( targetChild ) break;
      }

      // Retorna estágio alvo
      return targetChild;
    }

    /// Indica se estágio está após estágio passado
    flowUnit.checkIfIsAfter = function ( stage, onset = m.GameMatch.current.flow.round ) {
      // Identificadores
      var matchStages = FlowUnit.getAllStages( [ onset ] ),
          [ firstIndex, secondIndex ] = [ matchStages.indexOf( this ), matchStages.indexOf( stage ) ];

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( [ stage, onset ].every( value => value instanceof FlowUnit ) );
        m.oAssert( [ firstIndex, secondIndex ].every( index => index != -1 ) );
      }

      // Retorna se estágio alvo está após o passado ou não
      return firstIndex > secondIndex;
    }

    // Atribuição de propriedades de 'children'
    let children = flowUnit.children;

    /// Estágio descendente ativo
    children.active = null;
  }

  // Retorna todos os estágios de uma partida
  FlowUnit.getAllStages = function ( currentStages = [ m.GameMatch.current.flow.round ], isToIncludeMoves = false ) {
    // Identificadores
    var stagesArray = [];

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( currentStages.every( stage => stage instanceof this ) );

    // Itera por estágios atuais
    for( let stage of currentStages ) {
      // Adiciona ao arranjo de estágios estágio atual
      stagesArray.push( stage );

      // Caso estágio atual tenha descendentes, adiciona-os também ao arranjo de estágios
      if( stage.children.length && ( isToIncludeMoves || !( stage.children[ 0 ] instanceof m.FlowMove ) ) )
        stagesArray = stagesArray.concat( this.getAllStages( stage.children, isToIncludeMoves ) );
    }

    // Retorna estágios capturados
    return stagesArray;
  }

  // Adapta cadeia de fluxo passada para um formato textual
  FlowUnit.displayFlowChain = function ( flowChain ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( flowChain && typeof flowChain == 'string' );

    // Identificadores
    var flowUnits = {
          'round': 'R*', 'phase': '*P', 'step': '*S', 'turn': 'T*', 'period': '*P', 'block': '*B', 'segment': 'S*', 'parity': 'P*'
        },
        flowChainArray = flowChain.split( '.' ),
        flowText = '';

    // Itera por unidades de fluxo
    for( let flowUnit in flowUnits ) {
      // Identificadores
      let flowId = flowUnits[ flowUnit ],
          regExp = new RegExp( `(^|-)${ flowUnit }(-|$)` ),
          stageName = flowChainArray.find( stage => regExp.test( stage ) )?.replace( regExp, '' ) ?? '',
          charIndex = stageName.startsWith( 'pre-combat' ) ? 2 : [ 'allocation', 'post-combat' ].some( name => stageName.startsWith( name ) ) ? 1 : 0;

      // Filtra unidades de fluxo sem estágio encontrado
      if( !stageName ) continue;

      // Para turnos, ajusta nome do estágio de modo que apenas releve seu número
      if( flowUnit == 'turn' ) stageName = stageName.replace( /\D/g, '' )

      // Para paridades, substitui palavra indicadora da paridade por um número a lhe indicar
      else if( flowUnit == 'parity' ) stageName = stageName == 'odd' ? '1' : '2';

      // Elabora identificador do estágio alvo, em função de se ele aponta para um número ou palavra
      let stageId = /^\d+/.test( stageName ) ? stageName.slice( 0, stageName.search( /\D|$/ ) ) : stageName[ charIndex ].toUpperCase();

      // Para etapas cujo contador esteja acima de um, adiciona contador
      if( flowUnit == 'step' && stageName.replace( /\D/g, '' ) > 1 ) stageId += stageName.replace( /\D/g, '' );

      // Cria trecho abreviado relativo ao estágio alvo
      flowText += flowId.replace( '*', stageId ) + '-';
    }

    // Retorna texto sobre cadeia de fluxo, com separador final removido
    return flowText.replace( /-$/, '' );
  }
}

/// Propriedades do protótipo
FlowUnit.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: FlowUnit }
} );
