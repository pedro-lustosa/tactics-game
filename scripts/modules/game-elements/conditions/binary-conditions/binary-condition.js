// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const BinaryCondition = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( [ 'increase', 'decrease' ].every( key => !this[ key ] ) );

  // Superconstrutor
  m.GameCondition.call( this, config );

  // Configurações pós-superconstrutor

  /// Evento para registrar ganho da condição
  m.events.conditionChangeEnd.apply.add( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );

  /// Evento para registrar perda da condição
  m.events.conditionChangeEnd.drop.add( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  BinaryCondition.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GameCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof BinaryCondition );
  }
}

/// Propriedades do protótipo
BinaryCondition.prototype = Object.create( m.GameCondition.prototype, {
  // Construtor
  constructor: { value: BinaryCondition }
} );
