// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionPossessed = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionPossessed.init( this );

  // Identificadores
  var { controller } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( controller instanceof m.GamePlayer );

  // Superconstrutor
  m.BinaryCondition.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição do jogador a controlar o alvo da condição
  this.controller = controller;
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionPossessed.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionPossessed );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-possessed';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Jogador que controla alvo da condição
    condition.controller = null;

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
      }

      // Identificadores pós-validação
      var { controller: beingController, owner: beingOwner } = target.getRelationships();

      // Impede ações em andamento do alvo da condição
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == target ) );

      // Caso controlador do alvo seja diferente de seu dono, atualiza eventual texto suspenso do alvo da condição
      if( beingController != beingOwner ) m.Card.updateHoverText( { currentTarget: target } );

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );

      // Identificadores pós-validação
      var { controller: beingController, owner: beingOwner } = target.getRelationships();

      // Impede ações em andamento do alvo da condição
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == target ) );

      // Caso controlador do alvo seja diferente de seu dono, atualiza eventual texto suspenso do alvo da condição
      if( beingController != beingOwner ) m.Card.updateHoverText( { currentTarget: target } );

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionPossessed.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionPossessed }
} );
