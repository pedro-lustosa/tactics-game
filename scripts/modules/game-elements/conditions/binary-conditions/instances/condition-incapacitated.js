// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionIncapacitated = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionIncapacitated.init( this );

  // Superconstrutor
  m.BinaryCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionIncapacitated.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionIncapacitated );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-incapacitated';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
      }

      // Impede ações em andamento do ente
      m.GameAction.preventAllActions( m.GameAction.currents.filter( action => action.committer == target ) );

      // Adiciona condição ao arranjo de efeitos que tornam o ente ocupado
      target.readiness.relatedEffects.occupied.unshift( this );

      // Atualiza disponibilidade do ente
      target.readiness.update();

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData,
          conditionIndex = target.readiness.relatedEffects.occupied.indexOf( this );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );
        m.oAssert( conditionIndex > -1 );
      }

      // Remove condição do arranjo de efeitos que tornam ente ocupado
      target.readiness.relatedEffects.occupied.splice( conditionIndex, 1 );

      // Caso ente ainda esteja ocupado, atualiza sua disponibilidade
      if( target.readiness.status == 'occupied' ) target.readiness.update();

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionIncapacitated.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionIncapacitated }
} );
