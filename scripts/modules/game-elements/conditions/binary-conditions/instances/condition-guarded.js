// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionGuarded = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionGuarded.init( this );

  // Superconstrutor
  m.BinaryCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionGuarded.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionGuarded );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-guarded';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
      }

      // Impede ações de entes astrais
      preventAstralBeingActions: {
        // Identificadores
        let targetOwner = target.getRelationships().owner,
            astralBeingActions = m.GameAction.currents.filter( action => action.committer instanceof m.AstralBeing );

        // Itera por ações de entes astrais
        for( let action of astralBeingActions ) {
          // Filtra ações que não mirem o alvo diretamente
          if( action.target != target ) continue;

          // Filtra ações que sejam do mesmo jogador do alvo
          if( action.committer.getRelationships().controller == targetOwner ) continue;

          // Impede ação
          action.complete( 'prevent' );
        }
      }

      // Impede ações de magias negativas
      preventNegativeSpellActions: {
        // Identificadores
        let spellActions = m.GameAction.currents.filter( action => [ m.ActionChannel, m.ActionSustain ].some( constructor => action instanceof constructor ) );

        // Itera por magias em andamento
        for( let action of spellActions ) {
          // Identificadores
          let spell = action.target;

          // Filtra magias que não mirem o alvo diretamente
          if( spell.target != target && !spell.extraTargets.includes( target ) ) continue;

          // Filtra magias cuja polaridade não seja negativa
          if( spell.content.typeset.polarity != 'negative' ) continue;

          // Impede ação
          action.complete( 'prevent' );
        }
      }

      // Desusa magias negativas vinculadas ao alvo
      target.getAllCardAttachments().filter( card => card instanceof m.Spell && card.content.typeset.polarity == 'negative' ).forEach( card => card.disuse() );

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionGuarded.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionGuarded }
} );
