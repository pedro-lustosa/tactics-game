// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionForwarded = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionForwarded.init( this );

  // Superconstrutor
  m.BinaryCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionForwarded.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionForwarded );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-forwarded';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: being } = eventData,
          beingAttributes = being.content.stats.attributes,
          changeNumber = 15 - beingAttributes.current.initiative;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && being instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period.name.startsWith( 'pre-combat' ) );
        m.oAssert( being.readiness.isForwardable );
        m.oAssert( changeNumber > 0 );
        m.oAssert( !beingAttributes.changes.initiative.some( modifier => modifier.name == 'forwarded' ) );
      }

      // Ajusta iniciativa do ente
      beingAttributes.controlModifier( 'initiative', changeNumber, 'forwarded' );

      // Indica que ente não pode mais ser adiantado
      being.readiness.isForwardable = false;

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: being } = eventData,
          beingAttributes = being.content.stats.attributes;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && being instanceof this.targetType );

      // Ajusta iniciativa do ente
      beingAttributes.controlModifier( 'initiative', 0, 'forwarded' );

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionForwarded.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionForwarded }
} );
