// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionProjected = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionProjected.init( this );

  // Superconstrutor
  m.BinaryCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionProjected.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionProjected );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-projected';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.Humanoid;

    /// Execução atual do efeito desta condição
    condition.currentExecution = null;

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
        m.oAssert( target.slot instanceof m.FieldGridSlot );
        m.oAssert( !this.currentExecution );
      }

      // Captura e inicia execução do efeito
      ( this.currentExecution = executeEffect() ).next();

      // Retorna condição
      return this;

      // Função para execução do efeito da condição
      function * executeEffect() {
        // Interrompe ações ou magias que mirem o alvo desta condição, afora a que ele esteja concluindo
        m.GameAction.preventAllActions( m.GameAction.currents.filter( action =>
          action.committer != target &&
          ( action.target == target || action.target instanceof m.Spell && ( action.target.target == target || action.target.extraTargets.includes( target ) ) )
        ) );

        // Desabilita os efeitos de cartas vinculadas ao alvo desta condição
        target.getAllCardAttachments().forEach( card => card.content.effects.disable() );

        // Caso haja paradas na tela, aguarda suas conclusões antes de prosseguir execução da função
        while( m.GameMatch.getActiveBreaks().length )
          yield m.events.breakEnd.any.add( m.GameMatch.current, () => setTimeout( () => condition.currentExecution.next() ), { once: true } );

        // Caso esta condição não esteja mais em seu alvo, encerra execução da função
        if( condition.target != target ) return;

        // Caso casa atual do alvo da condição não esteja em uma grade do campo, programa remoção da condição
        if( !( target.slot instanceof m.FieldGridSlot ) ) return setTimeout( () => condition.drop.bind( condition ) );

        // Captura casa atual do alvo da condição
        let cardFieldSlot = target.slot;

        // Remove do campo alvo da condição
        cardFieldSlot.environment.remove( target );

        // Gera espírito a representar alvo da condição
        let spirit = new m.Spirit( { source: target } );

        // Ativa espírito na casa do campo onde estava o alvo da condição
        spirit.activate( cardFieldSlot.name, { triggeringCard: target } );

        // Adiciona evento para remover condição caso seu espírito seja inativado
        m.events.cardActivityEnd.inactive.add( spirit, () => setTimeout( condition.drop.bind( condition ) ) );

        // Encerra execução do efeito
        condition.currentExecution = null;
      }
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );
        m.oAssert( target.astralForm instanceof m.Spirit );
      }

      // Identificadores pós-validação
      var ownerDeck = target.getRelationships().owner.gameDeck,
          spirit = target.astralForm,
          { activity: spiritActivity, slot: cardSlot } = spirit;

      // Desvincula alvo da condição da carta de espírito o representando
      target.astralForm = null;

      // Para caso espírito possa canalizar
      if( spirit.content.stats.combativeness.channeling ) {
        // Ajusta dono das magias do espírito
        spirit.content.stats.combativeness.channeling.getOwnedSpells().forEach( spell => spell.owner = target );

        // Remove do espírito propriedade sobre canalização
        delete spirit.content.stats.combativeness.channeling;
      }

      // Alheia espírito
      spirit.unlink();

      // Remove de espírito eventos de configuração padrão da partida
      m.GameMatch.current.decks.unconfig( [ spirit ] );

      // Remove espírito do baralho do alvo da condição
      ownerDeck.remove( [ spirit ] );

      // Sincroniza atividade do alvo da condição com a última do espírito
      spiritActivity == 'active' ? target.activate( cardSlot.name, { triggeringCard: spirit } ) :
                                   target.inactivate( spiritActivity, { triggeringCard: spirit } );

      // Para caso alvo da condição ainda esteja ativo
      if( target.activity == 'active' ) {
        // Quando preciso, atualiza arranjo sobre cartas do baralho de alvo com sua atividade
        if( !ownerDeck.cards[ target.activity ]?.includes( target ) ) ownerDeck.cards[ target.activity ]?.push( target );

        // Quando preciso, atualiza arranjo sobre cartas em uso do baralho de alvo
        if( target.isInUse && !ownerDeck.cards.inUse.includes( target ) ) ownerDeck.cards.inUse.push( target );

        // Na barra de informações da tela de partidas, força atualização da seção de atividade de cartas do baralho alvo
        m.GameScreen.current.infoBar.updateDeckData( { eventCategory: 'card-use', eventTarget: target, useTarget: target } );
      }

      // Reabilita os efeitos de cartas vinculadas ao alvo da condição
      target.getAllCardAttachments().forEach( card => card instanceof m.Spell ? card.handleSpellClash() : card.content.effects.enable() );

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionProjected.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionProjected }
} );
