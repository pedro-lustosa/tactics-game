// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionStunned = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionStunned.init( this );

  // Superconstrutor
  m.BinaryCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionStunned.init = function ( condition ) {
    // Chama 'init' ascendente
    m.BinaryCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionStunned );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-stunned';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData,
          targetMoves = m.GameMatch.current.flow.moves.filter( move => move.source == target );

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( m.GameMatch.current.flow.period instanceof m.CombatPeriod );
      }

      // Adiciona a ente condição de indefeso
      new m.ConditionIncapacitated().apply( target, this.expireStage );

      // Itera por jogadas do ente
      for( let move of targetMoves ) {
        // Força jogada para estado aberto
        move.isOpen = true;

        // Conclui jogada
        move.develop( 1 );
      }

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
ConditionStunned.prototype = Object.create( m.BinaryCondition.prototype, {
  // Construtor
  constructor: { value: ConditionStunned }
} );
