// Módulos
import * as m from '../../../modules.js';

// Identificadores

/// Base do módulo
export const GameCondition = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) {
    m.oAssert( this.name && typeof this.name == 'string' && this.name.startsWith( 'condition-' ) );
    m.oAssert( [ 'pre-combat-break-period', 'combat-period' ].includes( this.maxExpireStageName ) );
  }

  // Superconstrutor
  PIXI.utils.EventEmitter.call( this );

  // Eventos

  /// Se aplicável, para executar efeitos de entrada da condição
  if( this.begin ) m.events.conditionChangeEnd.apply.add( this, this.begin );

  /// Se aplicável, para executar efeitos de saída da condição
  if( this.finish ) m.events.conditionChangeEnd.drop.add( this, this.finish );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GameCondition.init = function ( condition ) {
    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof GameCondition );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = '';

    /// Ente alvo da condição
    condition.target = null;

    /// Estágio em que condição é antecipadamente removida
    condition.expireStage = null;

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = '';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.Being;

    // Aplica condição a um ente
    condition.apply = function ( target, expireStage, gainedMarkers = 1, context = {} ) {
      // Identificadores pré-validação
      var matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( matchFlow.phase instanceof m.BattlePhase );
        m.oAssert( !this.target || this.target == target );
        m.oAssert( target instanceof this.targetType );
        m.oAssert( !expireStage || expireStage instanceof m.FlowUnit && [ 'period', 'block', 'segment', 'parity' ].includes( expireStage.flowType ) );
      }

      // Identificadores pós-validação
      var currentCondition = target.conditions.find( condition => condition.name == this.name ),
          eventTargets = [ this, target ];

      // Não executa função caso ente alvo não esteja ativo
      if( target.activity != 'active' ) return false;

      // Valida aplicação da condição ante magias 'Santuário' e 'Purificação Corporal'
      spellValidation: {
        // Identificadores
        let formerIsQuiet = m.app.isQuiet;

        // Força para ativada configuração 'isQuiet'
        m.app.isQuiet = true;

        // Captura resultado da validação
        let validationResult = [ m.SpellSanctuary, m.SpellBodyCleansing ].every( constructor => constructor.validateConditionApply( target, this.name ) );

        // Restaura configuração 'isQuiet' para seu valor original
        m.app.isQuiet = formerIsQuiet;

        // Encerra função caso condição alvo não possa ser aplicada ao ente
        if( !validationResult ) return false;
      }

      // Caso ente alvo já tenha condição, apenas atualiza seus dados
      if( currentCondition ) return updateData.call( currentCondition );

      // Sinaliza aos alvos pertinentes início da aplicação da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeStart.apply.emit( eventTarget, { condition: this, conditionTarget: target } );

      // Inseri condição no arranjo de condições do alvo passado
      target.conditions.push( this );

      // Registra alvo da condição
      this.target = target;

      // Se condição deve ser removida até o fim do período do combate, adiciona evento para sua remoção nesse momento
      if( this.maxExpireStageName == 'combat-period' )
        m.events.flowChangeEnd.finish.add( matchFlow.turn.getChild( 'combat-period' ), this.drop, { context: this, once: true } );

      // Atualiza dados da condição
      updateData.call( this );

      // Sinaliza aos alvos pertinentes fim da aplicação da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeEnd.apply.emit( eventTarget, { condition: this, conditionTarget: target } );

      // Retorna condição
      return this;

      // Atualiza dados da condição
      function updateData() {
        // Atualiza marcadores da condição, caso ela seja uma condição gradativa
        this.increase?.( gainedMarkers, context );

        // Atualiza estágio de término da condição
        return this.updateStage( expireStage );
      }
    }

    // Atualiza estágio de término da condição
    condition.updateStage = function ( expireStage ) {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.target instanceof this.targetType );
        m.oAssert( !expireStage || expireStage instanceof m.FlowUnit && [ 'period', 'block', 'segment', 'parity' ].includes( expireStage.flowType ) );
      }

      // Para caso ainda não exista um estágio de término atual e tenha sido passado um estágio de término
      if( !this.expireStage && expireStage ) {
        // Define estágio de término
        this.expireStage = expireStage;

        // Programa remoção da condição para o fim desse estágio
        m.events.flowChangeEnd.finish.add( expireStage, this.drop, { context: this, once: true } );
      }

      // Para caso exista um estágio de término atual e não tenha sido passado um novo estágio de término
      else if( this.expireStage && !expireStage ) {
        // Remove evento para remoção da condição no estágio de término atual
        m.events.flowChangeEnd.finish.remove( this.expireStage, this.drop, { context: this } );

        // Nulifica estágio de término da condição
        this.expireStage = null;
      }

      // Para caso exista tanto um estágio de término atual quanto tenha sido passado um novo estágio de término
      else if( this.expireStage && expireStage ) {
        // Identifica se estágio de término atual deve ou não ser sobrescrito pelo novo
        let isToOverride = this.expireStage.getChild( expireStage.name ) != expireStage && expireStage.checkIfIsAfter( this.expireStage, matchFlow.phase );

        // Para caso estágio atual deva ser sobrescrito pelo novo
        if( isToOverride ) {
          // Remove evento para remoção da condição no estágio de término atual
          m.events.flowChangeEnd.finish.remove( this.expireStage, this.drop, { context: this } );

          // Atualiza estágio de término
          this.expireStage = expireStage;

          // Programa remoção da condição para o fim desse estágio
          m.events.flowChangeEnd.finish.add( expireStage, this.drop, { context: this, once: true } );
        }
      }

      // Retorna condição
      return this;
    }

    // Remove condição de seu ente
    condition.drop = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          { target } = this,
          conditionIndex = target?.conditions.indexOf( this ),
          eventTargets = [ this, target ];

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !target || conditionIndex > -1 );

      // Não executa função caso condição não tenha um alvo
      if( !target ) return this;

      // Caso condição ainda tenha marcadores, retira-os primeiro
      if( this.markers ) return this.decrease( this.markers );

      // Sinaliza aos alvos pertinentes início da remoção da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeStart.drop.emit( eventTarget, { condition: this, conditionTarget: target } );

      // Remove condição do arranjo de condições de seu alvo
      target.conditions.splice( conditionIndex, 1 );

      // Nulifica alvo da condição e estágio de expiração
      this.expireStage = this.target = null;

      // Quando aplicável, nulifica execução atual da condição
      this.currentExecution &&= null;

      // Se condição seria removida até o fim do período do combate, remove evento para sua remoção nesse momento
      if( this.maxExpireStageName == 'combat-period' )
        m.events.flowChangeEnd.finish.remove( matchFlow.turn.getChild( 'combat-period' ), this.drop, { context: this } );

      // Sinaliza aos alvos pertinentes fim da remoção da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeEnd.drop.emit( eventTarget, { condition: this, conditionTarget: target } );

      // Retorna condição
      return this;
    }
  }
}

/// Propriedades do protótipo
GameCondition.prototype = Object.create( PIXI.utils.EventEmitter.prototype, {
  // Construtor
  constructor: { value: GameCondition }
} );
