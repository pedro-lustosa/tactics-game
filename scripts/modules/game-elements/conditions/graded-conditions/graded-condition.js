// Módulos
import * as m from '../../../../modules.js';

// Identificadores

/// Base do módulo
export const GradedCondition = function ( config = {} ) {
  // Validação
  if( m.app.isInDevelopment ) m.oAssert( Number.isInteger( this.maxMarkers ) && this.maxMarkers >= 1 && this.maxMarkers <= 10 );

  // Superconstrutor
  m.GameCondition.call( this, config );

  // Configurações pós-superconstrutor

  /// Evento para registrar ganho de marcadores da condição
  m.events.conditionChangeEnd.increase.add( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );

  /// Evento para registrar perda de marcadores da condição
  m.events.conditionChangeEnd.decrease.add( this, m.GameMatch.current.log.addEntry, { context: m.GameMatch.current.log } );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  GradedCondition.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GameCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof GradedCondition );

    // Atribuição de propriedades iniciais

    /// Quantidade atual de marcadores
    condition.markers = 0;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 0;

    /// Aumenta marcadores atuais da condição
    condition.increase = function ( number = 1, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.target instanceof m.Being );
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var eventTargets = [ this, this.target ],
          effectiveNumber = Math.min( this.maxMarkers - this.markers, number ),
          dataToEmit = {
            condition: this, markers: effectiveNumber, context: context
          };

      // Não executa função caso não haja marcadores a serem adicionados
      if( effectiveNumber <= 0 ) return this;

      // Sinaliza aos alvos pertinentes início da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeStart.increase.emit( eventTarget, dataToEmit );

      // Atualiza quantidade de marcadores da condição
      this.markers += effectiveNumber;

      // Sinaliza aos alvos pertinentes fim da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeEnd.increase.emit( eventTarget, dataToEmit );

      // Retorna condição
      return this;
    };

    /// Diminui marcadores atuais da condição
    condition.decrease = function ( number = 1, context = {} ) {
      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( this.target instanceof m.Being );
        m.oAssert( Number.isInteger( number ) && number >= 0 );
        m.oAssert( context.constructor == Object );
      }

      // Identificadores
      var eventTargets = [ this, this.target ],
          effectiveNumber = Math.min( this.markers, number ),
          dataToEmit = {
            condition: this, markers: effectiveNumber, context: context
          };

      // Não executa função caso não haja marcadores a serem removidos
      if( effectiveNumber <= 0 ) return this;

      // Sinaliza aos alvos pertinentes início da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeStart.decrease.emit( eventTarget, dataToEmit );

      // Atualiza quantidade de marcadores da condição
      this.markers -= effectiveNumber;

      // Sinaliza aos alvos pertinentes fim da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeEnd.decrease.emit( eventTarget, dataToEmit );

      // Caso condição tenha ficado sem nenhum marcador, remove-a do ente alvo
      if( !this.markers ) this.drop();

      // Retorna condição
      return this;
    };
  }
}

/// Propriedades do protótipo
GradedCondition.prototype = Object.create( m.GameCondition.prototype, {
  // Construtor
  constructor: { value: GradedCondition }
} );
