// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionEnlivened = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionEnlivened.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Eventos

  /// Para atualizar vontade do alvo da condição
  m.events.conditionChangeEnd.markers.add( this, this.updateWill );

  /// Para atualizar iniciativa do alvo da condição
  m.events.conditionChangeEnd.markers.add( this, this.updateInitiative );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionEnlivened.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionEnlivened );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-enlivened';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 4;

    /// Atualiza vontade do alvo da condição
    condition.updateWill = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { target: being } = this,
          { eventCategory, eventType, context = {} } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) );

      // Identificadores pós-validação
      var beingAttributes = being.content.stats.attributes;

      // Aplica atualização da vontade do ente, de modo assíncrono para garantir que condição será completamente aplicada antes de ser potencialmente removida
      setTimeout( () => beingAttributes.controlModifier( 'will', condition.markers, condition.name, context ) );
    }

    /// Atualiza iniciativa do alvo da condição
    condition.updateInitiative = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { target: being } = this,
          { eventCategory, eventType, context = {} } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) );

      // Identificadores pós-validação
      var beingAttributes = being.content.stats.attributes;

      // Aplica atualização da iniciativa do ente
      beingAttributes.controlModifier( 'initiative', condition.getInitiativeModifier(), condition.name, context );
    }

    /// Retorna modificador de iniciativa da condição
    condition.getInitiativeModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 3, modifier = 2; modifier; i -= 2, modifier-- )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }
  }
}

/// Propriedades do protótipo
ConditionEnlivened.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionEnlivened }
} );
