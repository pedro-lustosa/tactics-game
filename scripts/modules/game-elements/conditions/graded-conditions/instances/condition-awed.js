// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionAwed = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionAwed.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionAwed.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionAwed );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-awed';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.PhysicalBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 10;
  }

  // Valida uma ação ante essa condição
  ConditionAwed.validateAction = function ( action, target ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( action instanceof m.ActionAttack );
      m.oAssert( target instanceof m.Being );
      m.oAssert( !action.target || action.target == target );
    }

    // Identificadores
    var { committer } = action,
        committerWill = committer.content.stats.attributes.current.will;

    // Indica que ação é válida caso acionante não seja um ente biótico
    if( !( committer instanceof m.BioticBeing ) ) return true;

    // Indica que ação é válida caso o acionante não esteja na mesma casa do alvo
    if( committer.slot != target.slot ) return true;

    // Captura marcadores de 'admirável' no alvo
    let awedMarkers = target.conditions.find( condition => condition instanceof ConditionAwed )?.markers;

    // Indica que ação é válida caso alvo não tenha marcadores de 'admirável'
    if( !awedMarkers ) return true;

    // Indica que ação é válida caso vontade de acionante seja maior que marcadores de 'admirável' em alvo por mais de 1 ponto
    if( committerWill > awedMarkers + 1 ) return true;

    // Invalida ação caso vontade do acionante seja menor que marcadores de 'admirável' do alvo
    if( committerWill < awedMarkers )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'awed-prevents-attack-target' ) );

    // Indica que ação é válida caso ela já tenha sido acionada
    if( action.isCommitted ) return true;

    // Identificadores relativos a pontos de destino requeridos devido à condição
    let requiredPoints = 1,
        playerFatePoints = committer.getRelationships().controller.fatePoints.current.get(),
        committerFatePoints = committer.content.stats.fatePoints.current;

    // Subtrai de pontos de destino do acionante eventual ponto de destino oriundo de anel da proteção
    committerFatePoints -= committer.content.stats.fatePoints.sources.find( sourceObject => sourceObject.name == 'ring-of-protection' )?.currentValue ?? 0;

    // Invalida ação caso jogador não tenha os pontos de destino requeridos para escolha do alvo
    if( playerFatePoints + committerFatePoints < requiredPoints )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'no-fate-points-to-attack-awed-target' ) );

    // Define quantidade de pontos de destino que devem ser gastos para a escolha do alvo passado
    action.aweRequiredFatePoints = requiredPoints;

    // Indica que ação é válida
    return true;
  }
}

/// Propriedades do protótipo
ConditionAwed.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionAwed }
} );
