// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionEmboldened = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionEmboldened.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Eventos

  /// Para ajustar subtipo de acionamento das ações 'atacar' do alvo
  m.events.conditionChangeEnd.markers.add( this, this.updateAttackCommitment );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionEmboldened.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionEmboldened );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-emboldened';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 5;

    /// Retorna modificador de ataque da condição
    condition.getAttackModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 5, modifier = 3; modifier; i -= 2, modifier-- )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }

    /// Ajusta subtipo de acionamento das ações 'atacar' do alvo
    condition.updateAttackCommitment = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { target: being } = this,
          { eventCategory, eventType, markers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) && markers > 0 );

      // Identificadores pós-validação
      var attackAction = being.actions.find( action => action instanceof m.ActionAttack );

      // Não executa função caso alvo não tenha uma ação 'atacar' habilitada
      if( !attackAction ) return;

      // Não executa função caso acionamento da ação 'atacar' de alvo não seja dedicado
      if( attackAction.commitment.type != 'dedicated' ) return;

      // Ajusta subtipo e grau de acionamento da ação 'atacar' em função da quantidade de marcadores de encorajado
      [ attackAction.commitment.subtype, attackAction.commitment.degree ] = this.markers >= 3 ? [ 'partial', .5 ] : [ 'full', 1 ];
    }
  }
}

/// Propriedades do protótipo
ConditionEmboldened.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionEmboldened }
} );
