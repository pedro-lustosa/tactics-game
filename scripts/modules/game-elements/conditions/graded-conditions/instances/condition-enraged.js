// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionEnraged = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionEnraged.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Eventos

  /// Para atualizar custo de fluxo de eventual ação 'canalizar' em andamento do alvo
  m.events.conditionChangeEnd.markers.add( this, this.updateSpellFlowCost );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionEnraged.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionEnraged );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-enraged';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 5;

    /// Retorna modificador de defesa da condição
    condition.getDefenseModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 5, modifier = -3; modifier; i -= 2, modifier++ )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }

    /// Retorna modificador de custo de fluxo de magias
    condition.getFlowCostModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 4, modifier = 2; modifier; i -= 2, modifier-- )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }

    /// Atualiza custo de fluxo de eventual ação 'canalizar' do acionante
    condition.updateSpellFlowCost = function ( eventData = {} ) {
      // Não executa função caso alvo da condição não seja um canalizador
      if( !this.target?.content.stats.combativeness.channeling ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, markers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) && markers > 0 );

      // Captura magia sendo atualmente canalizada pelo alvo da condição
      let channelAction = m.GameAction.currents.find( action => action.committer == this.target && action instanceof m.ActionChannel );

      // Encerra função caso não haja uma ação 'canalizar' em andamento
      if( !channelAction ) return;

      // Captura modificador de custo de fluxo a ser adicionado
      let signedMarkers = eventType == 'increase' ? markers : -markers,
          flowCostModifier = this.getFlowCostModifier() - this.getFlowCostModifier( this.markers - signedMarkers );

      // Encerra função caso não haja um modificador de custo de fluxo a ser adicionado
      if( !flowCostModifier ) return;

      // Captura de dados sobre magia da ação
      let spell = channelAction.target,
          spellFlowCost = spell.content.stats.current.flowCost;

      // Sinaliza início da mudança de custo de fluxo
      m.events.cardContentStart.flowCost.emit( spell, { changeNumber: flowCostModifier } );

      // Atualiza custo de fluxo da magia relativa à ação
      spellFlowCost.toChannel = Math.max( spellFlowCost.toChannel + flowCostModifier, 0 );

      // Atualiza custo de fluxo da ação em andamento
      channelAction.flowCost = spellFlowCost.toChannel;

      // Sinaliza fim da mudança de custo de fluxo
      m.events.cardContentEnd.flowCost.emit( spell, { changeNumber: flowCostModifier } );
    }
  }
}

/// Propriedades do protótipo
ConditionEnraged.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionEnraged }
} );
