// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionDiseased = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionDiseased.init( this );

  // Identificadores
  var { source } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( source instanceof m.Card );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição da carta que provocou a condição
  this.source = source;

  // Eventos

  /// Para atualizar custo de fluxo de eventual ação 'canalizar' em andamento do alvo
  m.events.conditionChangeEnd.markers.add( this, this.updateSpellFlowCost );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionDiseased.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionDiseased );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-diseased';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 10;

    /// Indica carta que provocou a condição
    condition.source = null;

    /// Execução atual do efeito de pós-combate da condição
    condition.currentExecution = null;

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );

      // Identificadores pós-validação
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Adiciona evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.add( postCombat, postCombat.scheduleEffect, { context: this } );

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );

      // Identificadores pós-validação
      var postCombat = m.GameMatch.current.flow.round.getChild( 'post-combat-break-period' );

      // Remove evento para preparar efeito a ocorrer no pós-combate
      m.events.flowChangeEnd.begin.remove( postCombat, postCombat.scheduleEffect, { context: this } );

      // Retorna condição
      return this;
    }

    /// Retorna modificador de manobras da condição
    condition.getManeuverModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 9, modifier = -5; modifier; i -= 2, modifier++ )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }

    /// Retorna modificador de custo de fluxo de magias
    condition.getFlowCostModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 10, modifier = 5; modifier; i -= 2, modifier-- )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }

    /// Atualiza custo de fluxo de eventual ação 'canalizar' do acionante
    condition.updateSpellFlowCost = function ( eventData = {} ) {
      // Não executa função caso alvo da condição não seja um canalizador
      if( !this.target?.content.stats.combativeness.channeling ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, markers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) && markers > 0 );

      // Captura magia sendo atualmente canalizada pelo alvo da condição
      let channelAction = m.GameAction.currents.find( action => action.committer == this.target && action instanceof m.ActionChannel );

      // Encerra função caso não haja uma ação 'canalizar' em andamento
      if( !channelAction ) return;

      // Captura modificador de custo de fluxo a ser adicionado
      let signedMarkers = eventType == 'increase' ? markers : -markers,
          flowCostModifier = this.getFlowCostModifier() - this.getFlowCostModifier( this.markers - signedMarkers );

      // Encerra função caso não haja um modificador de custo de fluxo a ser adicionado
      if( !flowCostModifier ) return;

      // Captura de dados sobre magia da ação
      let spell = channelAction.target,
          spellFlowCost = spell.content.stats.current.flowCost;

      // Sinaliza início da mudança de custo de fluxo
      m.events.cardContentStart.flowCost.emit( spell, { changeNumber: flowCostModifier } );

      // Atualiza custo de fluxo da magia relativa à ação
      spellFlowCost.toChannel = Math.max( spellFlowCost.toChannel + flowCostModifier, 0 );

      // Atualiza custo de fluxo da ação em andamento
      channelAction.flowCost = spellFlowCost.toChannel;

      // Sinaliza fim da mudança de custo de fluxo
      m.events.cardContentEnd.flowCost.emit( spell, { changeNumber: flowCostModifier } );
    }

    /// Aplica efeito da condição a ser resolvido no pós-combate
    condition.applyPostCombatEffect = function () {
      // Não executa função caso condição não tenha mais um alvo
      if( !this.target ) return;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( !this.currentExecution );

      // Captura e inicia execução do efeito
      ( this.currentExecution = executeEffect() ).next();

      // Função para início da execução do efeito
      function * executeEffect() {
        // Identificadores
        var being = condition.target,
            beingStamina = being.content.stats.attributes.current.stamina,
            remainingStamina = beingStamina - condition.markers;

        // Caso vigor do alvo da condição seja maior que quantidade de marcadores por mais de 1 ponto, encerra função
        if( remainingStamina > 1 ) return condition.currentExecution = null;

        // Caso vigor do alvo da condição seja menor que quantidade de marcadores, afasta o alvo da condição
        if( remainingStamina < 0 ) return being.inactivate( 'withdrawn', { triggeringCard: condition.source } );

        // Lança parada de destino sobre afastamento do ente
        m.GameMatch.programFateBreak( being.getRelationships().opponent, {
          name: `${ condition.name }-fate-break-${ being.getMatchId() }`,
          playerBeing: null,
          opponentBeing: being,
          isContestable: Boolean( remainingStamina ),
          successAction: () => condition.currentExecution.next( true ),
          failureAction: () => condition.currentExecution.next( false ),
          modalArguments: [ `fate-break-${ condition.name }`, { being: being } ]
        } );

        // Aguarda resolução da parada de destino
        let wasUsedFatePoint = yield;

        // Caso parada de destino tenha sido bem-sucedida, afasta alvo
        if( wasUsedFatePoint ) return being.inactivate( 'withdrawn', { triggeringCard: condition.source } );

        // Encerra execução do efeito
        condition.currentExecution = null;
      }
    }
  }
}

/// Propriedades do protótipo
ConditionDiseased.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionDiseased }
} );
