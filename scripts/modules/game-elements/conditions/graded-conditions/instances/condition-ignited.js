// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionIgnited = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionIgnited.init( this );

  // Identificadores
  var { source } = config;

  // Validação
  if( m.app.isInDevelopment ) m.oAssert( source instanceof m.Card );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Configurações pós-superconstrutor

  /// Atribuição da carta que provocou a condição
  this.source = source;

  // Eventos

  /// Para infligir dano rateado de fogo após remoção de marcadores
  m.events.conditionChangeEnd.decrease.add( this, this.inflictDamage );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionIgnited.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionIgnited );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-ignited';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'combat-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.PhysicalBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 3;

    /// Indica carta que provocou a condição
    condition.source = null;

    /// Dano total a ser infligido
    condition.damageToInflict = 0;

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData,
          matchFlow = m.GameMatch.current.flow;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );
        m.oAssert( matchFlow.period instanceof m.CombatPeriod );
      }

      // Para entes bióticos, adiciona a ente condição de indefeso
      if( target instanceof m.BioticBeing ) new m.ConditionIncapacitated().apply( target, this.expireStage );

      // Adiciona evento para diminuir 1 marcador da condição ao fim do segmento atual
      m.events.flowChangeStart.finish.add( matchFlow.segment, () => this.target ? this.decrease() : null, { context: this, once: true } );

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );

      // Zera dano total a ser infligido
      this.damageToInflict = 0;

      // Retorna condição
      return this;
    }

    /// Define ou redefine quantidade de marcadores da condição
    condition.increase = function () {
      // Validação
      if( m.app.isInDevelopment ) m.oAssert( this.target instanceof m.Being );

      // Identificadores
      var beingSize = this.target.content.typeset.size,
          sizeScore = beingSize == 'large' ? 3 : beingSize == 'medium' ? 2 : 1,
          eventTargets = [ this, this.target ];

      // Sinaliza aos alvos pertinentes início da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeStart.increase.emit( eventTarget, { condition: this, markers: sizeScore } );

      // Atualiza quantidade de marcadores da condição
      this.markers = sizeScore;

      // Sinaliza aos alvos pertinentes fim da atualização de marcadores da condição
      for( let eventTarget of eventTargets ) m.events.conditionChangeEnd.increase.emit( eventTarget, { condition: this, markers: sizeScore } );

      // Retorna condição
      return this;
    }

    /// Atualiza estágio de término da condição
    condition.updateStage = function () {
      // Identificadores
      var matchFlow = m.GameMatch.current.flow,
          newExpireStage = matchFlow.period.getStageFrom( matchFlow.segment, this.markers - 1 );

      // Caso haja um estágio de término atual, remove evento para remoção da condição nesse estágio
      if( this.expireStage ) m.events.flowChangeEnd.finish.remove( this.expireStage, this.drop, { context: this } );

      // Registra novo estágio de término
      this.expireStage = newExpireStage ?? null;

      // Programa remoção da condição para o fim do novo estágio de término, caso exista
      if( newExpireStage ) m.events.flowChangeEnd.finish.add( newExpireStage, this.drop, { context: this, once: true } );

      // Atualiza estágio de término da condição de indefeso
      updateIncapacitatedStage: {
        // Identificadores
        let incapacitatedCondition = this.target.conditions.find( condition => condition instanceof m.ConditionIncapacitated ),
            incapacitatedStage = incapacitatedCondition?.expireStage;

        // Encerra bloco caso alvo da condição de incendiado não seja um ente biótico
        if( !( this.target instanceof m.BioticBeing ) ) break updateIncapacitatedStage;

        // Encerra bloco caso condição de indefeso ainda não exista ou não tenha um estágio de término
        if( !incapacitatedStage ) break updateIncapacitatedStage;

        // Para caso exista um estágio de término atual
        if( newExpireStage ) {
          // Encerra bloco caso estágio de término atual de incendiado não seja posterior ao atual da condição de indefeso
          if( incapacitatedStage.getChild( newExpireStage.name ) == newExpireStage || !newExpireStage.checkIfIsAfter( incapacitatedStage, matchFlow.phase ) )
            break updateIncapacitatedStage;
        }

        // Remove evento para remoção da condição de indefeso em seu estágio de término atual
        m.events.flowChangeEnd.finish.remove( incapacitatedStage, incapacitatedCondition.drop, { context: incapacitatedCondition } );

        // Atualiza novo estágio de término da condição de indefeso
        incapacitatedCondition.expireStage = newExpireStage ?? null;

        // Programa remoção da condição de indefeso para o fim do novo estágio de término de incendiado, caso exista
        if( newExpireStage ) m.events.flowChangeEnd.finish.add( newExpireStage, incapacitatedCondition.drop, { context: incapacitatedCondition, once: true } );
      }

      // Retorna condição
      return this;
    }

    /// Inflige dano rateado de fogo após remoção de marcadores
    condition.inflictDamage = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, markers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'decrease' && markers > 0 );

      // Itera por marcadores
      for( let i = markers; i; i-- ) {
        // Identificadores
        let damageSum = Math.round( this.damageToInflict / ( this.markers + i ) );

        // Filtra danos zerados
        if( !damageSum ) continue;

        // Inflige parte do dano de fogo total
        inflictDamage: {
          // Identificadores
          let targetBeing = this.target,
              beingAttributes = targetBeing.content.stats.attributes,
              fireDamage = new m.DamageFire( { totalPoints: damageSum, source: this.source } ),
              dataToEmit = {
                mainDamage: fireDamage,
                validDamages: [ fireDamage ],
                damageTarget: targetBeing,
                damagePoints: fireDamage.totalPoints,
                isToBeRemoved: fireDamage.totalPoints >= beingAttributes.current.health
              };

          // Sinaliza início do sofrimento do dano
          m.events.damageChangeStart.suffer.emit( targetBeing, dataToEmit );

          // Sinaliza início do infligimento do dano
          m.events.damageChangeStart.inflict.emit( fireDamage, dataToEmit );

          // Reduz pontos de vida do alvo da condição
          beingAttributes.reduce( fireDamage.totalPoints, 'health', { triggeringCard: this.source } );

          // Sinaliza fim do infligimento do dano
          m.events.damageChangeEnd.inflict.emit( fireDamage, dataToEmit );

          // Sinaliza fim do sofrimento do dano
          m.events.damageChangeEnd.suffer.emit( targetBeing, dataToEmit );
        }

        // Subtrai dano infligido de dano de fogo total a ser infligido
        this.damageToInflict = Math.max( 0, this.damageToInflict - damageSum );

        // Caso condição não esteja mais em alvo, encerra função
        if( !this.target ) return;
      }

      // Programação da próxima condição
      setNewDecrease: {
        // Não executa bloco caso período do combate tenha se encerrado
        if( !m.GameMatch.current.flow.period ) break setNewDecrease;

        // Identificadores
        let matchFlow = m.GameMatch.current.flow,
            nextSegment = matchFlow.period.getStageFrom( matchFlow.segment, 1 );

        // Caso haja um próximo segmento, programa próxima remoção de marcador para fim desse segmento
        if( nextSegment ) m.events.flowChangeStart.finish.add( nextSegment, () => this.target ? this.decrease() : null, { context: this, once: true } );
      }
    }
  }
}

/// Propriedades do protótipo
ConditionIgnited.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionIgnited }
} );
