// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionFeared = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionFeared.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionFeared.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionFeared );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-feared';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.PhysicalBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 10;
  }

  // Valida uma ação ante essa condição
  ConditionFeared.validateAction = function ( action, cardSlot ) {
    // Validação
    if( m.app.isInDevelopment ) {
      m.oAssert( [ m.ActionDeploy, m.ActionEngage, m.ActionIntercept ].some( constructor => action instanceof constructor ) );
      m.oAssert( cardSlot instanceof m.EngagementZone );
    }

    // Identificadores
    var { committer } = action;

    // Indica que ação é válida caso acionante não seja um ente biótico
    if( !( committer instanceof m.BioticBeing ) ) return true;

    // Indica que ação é válida caso acionante esteja possuído
    if( committer.conditions.some( condition => condition instanceof m.ConditionPossessed ) ) return true;

    // Filtra entes em alvo com a condição 'temível', e captura marcadores do ente que tiver mais
    let beingsWithFeared = getBeingsWithFeared(),
        fearedMarkers = beingsWithFeared.reduce(
          ( accumulator, current ) => Math.max( accumulator, current.conditions.find( condition => condition instanceof this ).markers ), 0
        );

    // Indica que ação é válida caso não haja em alvo um ente do oponente com marcadores de 'temível'
    if( !fearedMarkers ) return true;

    // Captura marcadores de 'encorajado' do acionante
    let emboldenedMarkers = committer.conditions.some( condition => condition instanceof m.ConditionEmboldened )?.markers ?? 0;

    // Ajusta marcadores de 'temível' para que relevem marcadores de 'encorajado' de acionante
    fearedMarkers = Math.max( fearedMarkers - emboldenedMarkers * 2, 0 );

    // Indica que ação é válida caso não haja mais marcadores de 'temível' a serem relevados
    if( !fearedMarkers ) return true;

    // Identificadores para o prosseguimento da validação
    let committerWill = committer.content.stats.attributes.current.will,
        fearedSlot = beingsWithFeared[ 0 ].slot;

    // Indica que ação é válida caso vontade de acionante seja maior que marcadores de 'temível' por mais de 1 ponto
    if( committerWill > fearedMarkers + 1 ) return true;

    // Invalida ação caso vontade do acionante seja menor que marcadores de 'temível'
    if( committerWill < fearedMarkers )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'feared-prevents-slot-choice', { slot: fearedSlot } ) );

    // Indica que ação é válida caso ela já tenha sido acionada
    if( action.isCommitted ) return true;

    // Identificadores relativos a pontos de destino requeridos devido à condição
    let requiredPoints = 1,
        playerFatePoints = committer.getRelationships().controller.fatePoints.current.get(),
        committerFatePoints = committer.content.stats.fatePoints.current;

    // Subtrai de pontos de destino do acionante eventual ponto de destino oriundo de anel da proteção
    committerFatePoints -= committer.content.stats.fatePoints.sources.find( sourceObject => sourceObject.name == 'ring-of-protection' )?.currentValue ?? 0;

    // Invalida ação caso jogador não tenha os pontos de destino requeridos para escolha do alvo
    if( playerFatePoints + committerFatePoints < requiredPoints )
      return m.noticeBar.show( m.languages.notices.getInvalidationText( 'no-fate-points-to-move-to-target-with-feared', { slot: fearedSlot } ) );

    // Define quantidade de pontos de destino que devem ser gastos para a escolha do alvo passado
    action.fearedRequiredFatePoints = requiredPoints;

    // Indica que ação é válida
    return true;

    // Funções

    /// Retorna entes válidos com condição 'temível'
    function getBeingsWithFeared() {
      // Identificadores
      var opponentParity = committer.getRelationships().opponent.parity,
          opponentBeings = cardSlot.cards[ opponentParity ];

      // Caso ação não seja "mobilizar" e não haja entes do oponente na zona de engajamento alvo, define ente do oponente como o que seria deslocado para lá
      if( !( action instanceof m.ActionDeploy ) && !opponentBeings.length )
        opponentBeings = opponentBeings.concat( cardSlot.getAdjacentGridSlots( opponentParity ).card ?? [] );

      // Retorna entes do oponente com marcadores de 'temível'
      return opponentBeings.filter( being => being.conditions.some( condition => condition instanceof ConditionFeared ) );
    }
  }
}

/// Propriedades do protótipo
ConditionFeared.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionFeared }
} );
