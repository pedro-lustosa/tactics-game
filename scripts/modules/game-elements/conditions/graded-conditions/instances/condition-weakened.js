// Módulos
import * as m from '../../../../../modules.js';

// Identificadores

/// Base do módulo
export const ConditionWeakened = function ( config = {} ) {
  // Iniciação de propriedades
  ConditionWeakened.init( this );

  // Superconstrutor
  m.GradedCondition.call( this, config );

  // Eventos

  /// Para atualizar vigor do alvo da condição
  m.events.conditionChangeEnd.markers.add( this, this.updateStamina );

  /// Para atualizar dano de manobras do alvo da condição
  m.events.conditionChangeEnd.markers.add( this, this.updateDamageModifier );
}

/// Propriedades do construtor
defineProperties: {
  // Iniciação de propriedades
  ConditionWeakened.init = function ( condition ) {
    // Chama 'init' ascendente
    m.GradedCondition.init( condition );

    // Validação
    if( m.app.isInDevelopment ) m.oAssert( condition instanceof ConditionWeakened );

    // Atribuição de propriedades iniciais

    /// Nome
    condition.name = 'condition-weakened';

    /// Nome do estágio limite para remoção da condição
    condition.maxExpireStageName = 'pre-combat-break-period';

    /// Tipo de alvo a que condição se aplica
    condition.targetType = m.BioticBeing;

    /// Quantidade máxima de marcadores
    condition.maxMarkers = 4;

    /// Manobras a que modificador de dano da condição pode ser aplicado, e seus respectivos danos
    condition.allowedManeuvers = {
      unarmed: 'blunt', bash: 'blunt', scratch: 'slash', bite: 'pierce'
    };

    /// Inicia efeitos da condição
    condition.begin = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'apply' && target instanceof this.targetType );

      // Adiciona evento para controlar eventual modificador de dano a armas de alvo da condição ante mudanças de uso
      m.events.cardUseEnd.useInOut.add( target, this.controlDamageModifier, { context: this } );

      // Retorna condição
      return this;
    }

    /// Encerra efeitos da condição
    condition.finish = function ( eventData = {} ) {
      // Identificadores
      var { eventCategory, eventType, conditionTarget: target } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && eventType == 'drop' && target instanceof this.targetType );

      // Remove evento para controlar eventual modificador de dano a armas de alvo da condição ante mudanças de uso
      m.events.cardUseEnd.useInOut.remove( target, this.controlDamageModifier, { context: this } );

      // Retorna condição
      return this;
    }

    /// Atualiza vigor do alvo da condição
    condition.updateStamina = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { target: being } = this,
          { eventCategory, eventType, context = {} } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) );

      // Identificadores pós-validação
      var beingAttributes = being.content.stats.attributes;

      // Aplica atualização do vigor do ente, de modo assíncrono para garantir que condição será completamente aplicada antes de ser potencialmente removida
      setTimeout( () => beingAttributes.controlModifier( 'stamina', -condition.markers, condition.name, context ) );
    }

    /// Atualiza modificador de dano da condição em manobras de seu alvo
    condition.updateDamageModifier = function ( eventData = {} ) {
      // Identificadores pré-validação
      var { target: being, allowedManeuvers } = this,
          { eventCategory, eventType, markers: changedMarkers } = eventData;

      // Validação
      if( m.app.isInDevelopment ) {
        m.oAssert( eventCategory == 'condition-change' && [ 'increase', 'decrease' ].includes( eventType ) );
        m.oAssert( Number.isInteger( changedMarkers ) && changedMarkers > 0 );
      }

      // Identificadores pós-validação
      var beingManeuvers = being.content.stats.effectivity.maneuvers,
          damageModifier = this.getDamageModifier(),
          formerDamageModifier = this.getDamageModifier( this.markers + ( eventType == 'increase' ? -changedMarkers : changedMarkers ) ),
          damageToChange = damageModifier - formerDamageModifier,
          maneuverNames = Object.keys( allowedManeuvers );

      // Não executa função caso não haja dano a ser alterado
      if( !damageToChange ) return;

      // Itera por fontes de manobras
      for( let key of [ 'primary', 'secondary', 'natural' ] ) {
        // Identificadores
        let maneuverSource = beingManeuvers[ key ];

        // Para manobras naturais, ajusta fonte de manobras
        if( key == 'natural' ) maneuverSource = maneuverSource.current

        // Do contrário, filtra manobras naturais
        else
          if( !maneuverSource.source ) continue;

        // Itera por ataques da fonte de manobras alvo
        for( let attack of maneuverSource.attacks ) {
          // Identificadores
          let maneuverName = maneuverNames.find( maneuverName => attack.name.endsWith( '-' + maneuverName ) ),
              damageName = allowedManeuvers[ maneuverName ];

          // Filtra ataques que não sejam de uma das manobras aplicáveis
          if( !maneuverName ) continue;

          // Aplica ao dano da manobra alvo modificador da condição
          attack.applyModifier( `damage-${ damageName }`, damageModifier, this.name );
        }
      }
    }

    /// Controla modificador de dano da condição para armas que entrarem em uso ou desuso
    condition.controlDamageModifier = function ( eventData = {} ) {
      // Não executa função caso componente usado ou desusado não seja uma arma
      if( !( eventData.useTarget instanceof m.Weapon ) ) return;

      // Identificadores pré-validação
      var { eventCategory, eventType, eventTarget: being, useTarget: weapon } = eventData;

      // Validação
      if( m.app.isInDevelopment ) m.oAssert( eventCategory == 'card-use' && [ 'use-in', 'use-out' ].includes( eventType ) && being == this.target );

      // Identificadores pós-validação
      var weaponAttacks = weapon.content.stats.effectivity.current.attacks,
          damageModifier = this.getDamageModifier(),
          { allowedManeuvers } = this,
          maneuverNames = Object.keys( allowedManeuvers );

      // Não executa função caso não haja um modificador de dano a ser aplicado ou retirado
      if( !damageModifier ) return;

      // Caso evento seja o de desuso, multiplica por -1 modificador de dano
      if( eventType == 'use-out' ) damageModifier *= -1;

      // Itera por ataques da arma alvo
      for( let attack of weaponAttacks ) {
        // Identificadores
        let maneuverName = maneuverNames.find( maneuverName => attack.name.endsWith( '-' + maneuverName ) ),
            damageName = allowedManeuvers[ maneuverName ];

        // Filtra ataques que não sejam de uma das manobras aplicáveis
        if( !maneuverName ) continue;

        // Controla entrada ou saída de modificador de dano à manobra alvo
        eventType == 'use-in' ?
          attack.applyModifier( `damage-${ damageName }`, damageModifier, this.name ) : attack.dropModifier( `damage-${ damageName }`, this.name );
      }
    }

    /// Retorna modificador de dano da condição
    condition.getDamageModifier = function ( markers = this.markers ) {
      // Retorna modificador segundo quantidade atual de marcadores da condição
      for( let i = 3, modifier = -2; modifier; i -= 2, modifier++ )
        if( i <= markers ) return modifier;

      // Retorna ausência de modificador
      return 0;
    }
  }
}

/// Propriedades do protótipo
ConditionWeakened.prototype = Object.create( m.GradedCondition.prototype, {
  // Construtor
  constructor: { value: ConditionWeakened }
} );
